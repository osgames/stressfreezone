#include "lugre_prefix.h"
#include "lugre_fifo.h"
#include "lugre_gfx3D.h"
#include "lugre_ogrewrapper.h"
#include "lugre_ode.h"
#include "resyncreceiver.h"
#include "objectcontroller.h"
#include "physicform.h"
#include "object.h"
#include "location.h"

using namespace Lugre;

cObject::cObject() : 
	mpParentLocation(0), 
	mpPhysicForm(0), 
	mpController(0),
	miID(0),
	miResyncCounterHigh(0),
	miResyncCounterLow(0),
	miLastChangeTime(0), 
	mbResynced(true),
	mfMaxSpeed(0), 
	mfBoundingRad(0), 
	mfDistToLocationCenter(0),
	mpGfx3D(0),
	mvPos(		Ogre::Vector3::ZERO),
	mvVel(		Ogre::Vector3::ZERO),
	mvAccel(	Ogre::Vector3::ZERO),
	mqRot(		Ogre::Quaternion::IDENTITY),
	mqTurn(		Ogre::Quaternion::IDENTITY)	{}
		
cObject::~cObject() {
	SetParentLocation(0);
	SetController(0); // WARNING ! don't delete mpController, not owned by object
	#ifdef ENABLE_ODE
	if (mpPhysicForm) { delete mpPhysicForm; mpPhysicForm = 0; }
	#endif
	SetID(0);
}

void	cObject::SetController	(cObjectController* pObjectController) {
	if (mpController) mpController->Release();
	mpController = pObjectController;
	if (mpController) mpController->Lock();
}


// project 1 onto 2 
Ogre::Vector3 Vector_project_on_vector (Ogre::Vector3 v1, Ogre::Vector3 v2){
	return v2 * (v1.dotProduct(v2) / v2.dotProduct(v2));
	//Vector.scalarmult(x2,y2,z2, Vector.dot(x1,y1,z1,x2,y2,z2) / Vector.dot(x2,y2,z2,x2,y2,z2))
}

// project x,y,z on the plane with normal nx,ny,nz
Ogre::Vector3 Vector_project_on_plane (Ogre::Vector3 v, Ogre::Vector3 n){
	return v - Vector_project_on_vector(v,n);
}


/// fPhysStepTime is used for simple physics
/// iCurTime is intended to be used for setting miLastChangeTime on collision/physical interaction
void	cObject::Step	(const int iCurTime,const float fPhysStepTime,const bool bIncResyncCounterLow) {
	// apply controller
	if (mpController) {
		mpController->Step(this);
		miLastChangeTime = iCurTime;
	}
	
	if (mfMaxSpeed > 0) { 
		float fCurSpeed = mvVel.length();
		if (fCurSpeed > mfMaxSpeed) mvVel *= mfMaxSpeed / fCurSpeed;
	}
	
	// TODO : complex phys using mpPhysicForm, and collision stuff using mpParentLocation  (only on server?)
	
	// simple phys
	if (mpPhysicForm == 0) {
		mvVel += mvAccel * fPhysStepTime;
		mvPos += mvVel * fPhysStepTime;
		
		// apply turn
		static	Ogre::Radian	angle;
		static	Vector3			axis;
		mqTurn.ToAngleAxis(angle,axis);
		mqRot = mqRot * Quaternion(angle*fPhysStepTime,axis);
		mqRot.normalise();
	} else {
		#ifdef ENABLE_ODE
		float fspeed = 100.0f;
		mpPhysicForm->AddForce(mvAccel.x * fspeed, mvAccel.y * fspeed, mvAccel.z * fspeed);
		
		if(0 && mvAccel.length() > 0.0f){
			mpPhysicForm->AddForceAtRelPos(10000,10000,10000, 1,1,0);
		}
		
		// get position/rotation
		mpPhysicForm->GetPosition(mvPos.x, mvPos.y, mvPos.z);
		mpPhysicForm->GetLinearVelocity(mvVel.x, mvVel.y, mvVel.z);
		mpPhysicForm->GetRotation(mqRot.x, mqRot.y, mqRot.z, mqRot.w);
		mqRot.normalise();
		#endif
	}
	
	mfDistToLocationCenter = mvPos.length();
	if (mpParentLocation) {
		float fNewBound = mfDistToLocationCenter + mfBoundingRad;
		if (mpParentLocation->mfBoundingRad < fNewBound)
			mpParentLocation->mfBoundingRad = fNewBound;
	}
	
	if (mpGfx3D) {
		mpGfx3D->SetPosition(mvPos);
		mpGfx3D->SetOrientation(mqRot);
		mpGfx3D->mfCustomBoundingRadius = mfBoundingRad; ///< for 2d Tracking
	}
	
	if (bIncResyncCounterLow) {
		++miResyncCounterLow; // don't increment on client !
		assert(miResyncCounterLow > 0 && "todo : handle overflow in miResyncCounterLow");
	}
}

/// executed in constant time (thanks to keeping insert position)
/// server should increase miResyncCounterHigh and inform client using reliable (tcp) connection
void	cObject::SetParentLocation	(cLocation* pParentLocation) {
	if (mpParentLocation == pParentLocation) return;
	if (mpParentLocation) mpParentLocation->mlObjects.erase(_mLocationInsertPos);
	mpParentLocation = pParentLocation;
	if (mpParentLocation) _mLocationInsertPos = mpParentLocation->mlObjects.insert(mpParentLocation->mlObjects.begin(),this);
}

/// registers object in cResyncReceiver* gpResyncReceiver if available
void	cObject::SetID	(const int iID) {
	if (miID != 0 && gpResyncReceiver) gpResyncReceiver->RemoveObject(miID);
	miID = iID;
	if (miID != 0 && gpResyncReceiver) gpResyncReceiver->AddObject(miID,this);
}

/// server only
void	cObject::SaveResyncData	(cFIFO& pFIFO) {
	pFIFO.PushUint32(miID);
	pFIFO.PushUint16((uint16)miResyncCounterHigh);
	pFIFO.PushUint32(miResyncCounterLow);
	pFIFO.Push(mvPos);
	pFIFO.Push(mvVel);
	pFIFO.Push(mvAccel);
	pFIFO.Push(mqRot);
	pFIFO.Push(mqTurn);
}

/// client only, ignored if resync considered old (or too new in case locaction change has not arrived over tcp yet
void	cObject::LoadResyncData	(cFIFO& pFIFO) {
	pFIFO.Skip(4);
	if (pFIFO.PopUint16() != (uint16)miResyncCounterHigh) { pFIFO.Skip(kResyncMessageLen-(4+2)); return; }
	uint32 newlow = pFIFO.PopUint32();
	if (newlow < miResyncCounterLow) { pFIFO.Skip(kResyncMessageLen-(4+2+4)); return; }
	miResyncCounterLow = newlow;
	pFIFO.Pop(mvPos);
	pFIFO.Pop(mvVel);
	pFIFO.Pop(mvAccel);
	pFIFO.Pop(mqRot);
	pFIFO.Pop(mqTurn);
}

void	cObject::SkipResyncData	(cFIFO& pFIFO) {
	pFIFO.Skip(kResyncMessageLen);
}
