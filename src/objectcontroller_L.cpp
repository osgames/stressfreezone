#include "lugre_prefix.h"
#include "lugre_luabind.h"
#include "object.h"
#include "objectcontroller.h"

using namespace Lugre;

/*
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include "gfx3D.h"
#include "net.h"
#include "fifo.h"
#include "player.h"
#include "game.h"
#include "client.h"
#include "server.h"
#include "smartptr.h"
//#include "config.h"
#include "scripting.h"
#include "input.h"
#include "sound.h"
#include "robstring1.2.h"
#include "location.h"
*/

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

class cObjectController_L : public cLuaBind<cObjectController> { public:
		/// called by Register(), registers object-methods (see cLuaBind constructor for examples)
		virtual void RegisterMethods	(lua_State *L) { PROFILE
			lua_register(L,"CreateObjectController",		&cObjectController_L::CreateObjectController);

			#define REGISTER_METHOD(methodname) mlMethod.push_back(make_luaL_reg(#methodname,&cObjectController_L::methodname));
			REGISTER_METHOD(Destroy);
			REGISTER_METHOD(SetApproachObject);
			REGISTER_METHOD(SetApproachPosition);
			REGISTER_METHOD(IsTargetAlive);
			REGISTER_METHOD(DistanceToTarget);			
		}

		/// called by Register(), registers object-member-vars (see cLuaBind::RegisterMembers() for examples)
		virtual void RegisterMembers 	() { PROFILE
			cObjectController* prototype = new cObjectController(); // memory leak : never deleted, but better than side effects
			cMemberVar_REGISTER(prototype,	kVarType_Float,			mfMaxAccel,			0);
			cMemberVar_REGISTER(prototype,	kVarType_Float,			mfApproachMinDist,	0);
			cMemberVar_REGISTER(prototype,  kVarType_bool,			mbStupid,			0);
			cMemberVar_REGISTER(prototype,  kVarType_Float,			mfMaxSpeed,			0);
			//~ cMemberVar_REGISTER(prototype,	kVarType_int,			miLastChangeTime,		0);
			//~ cMemberVar_REGISTER(prototype,	kVarType_bool,			mbResynced,		0);
			//~ cMemberVar_REGISTER(prototype,	kVarType_Vector3,		mvPos,			0);
			//~ cMemberVar_REGISTER(prototype,	kVarType_Quaternion,	mqTurn,			0);
		}

	// static methods exported to lua

		/// for lua : cObjectController*	CreateObjectController		();
		static int							CreateObjectController		(lua_State *L) { PROFILE 
			return CreateUData(L,new cObjectController()); 
		}

	// object methods exported to lua
		
		static int	Destroy				(lua_State *L) { PROFILE 
			delete checkudata_alive(L);
			return 0; 
		}
		
		/// for lua : void	SetApproachObject	(obj)
		static int			SetApproachObject	(lua_State *L) { PROFILE 
			cObject* pTarget = (lua_gettop(L) >= 2 && !lua_isnil(L,2)) ? cLuaBind<cObject>::checkudata(L,2) : 0;
			checkudata_alive(L)->SetTarget(pTarget ? (new cObjectContollerTargetObject(pTarget)) : 0);
			return 0; 
		}
		
		/// for lua : void	o:SetApproachPosition	(x,y,z)
		static int			SetApproachPosition	(lua_State *L) { PROFILE 
			checkudata_alive(L)->SetTarget(new cObjectContollerTargetPosition(
					Ogre::Vector3(
						luaL_checknumber(L,2),
						luaL_checknumber(L,3),
						luaL_checknumber(L,4)
					)
			));
			return 0; 
		}

		/// for lua : number	DistanceToTarget	()
		// only call if target is alive
		static int			DistanceToTarget	(lua_State *L) { PROFILE 
			cObjectController*p = checkudata_alive(L);
			if(p->mpApproachTarget && p->mpApproachTarget->IsAlive()){
				Vector3 other(
						luaL_checknumber(L,2),
						luaL_checknumber(L,3),
						luaL_checknumber(L,4));
				lua_pushnumber(L,p->mpApproachTarget->GetPosition().distance(other));
			} else {
				lua_pushnumber(L,0);
			}
			return 1;
		}
		
		/// for lua : bool	IsTargetAlive	()
		static int			IsTargetAlive	(lua_State *L) { PROFILE 
			cObjectController*p = checkudata_alive(L);
			if(p->mpApproachTarget){
				lua_pushboolean(L,p->mpApproachTarget->IsAlive());
			} else {
				lua_pushboolean(L,false);
			}
			return 1;
		}
		
		virtual const char* GetLuaTypeName () { return "sfz.objcontroller"; }
};


/// lua binding
void	cObjectController::LuaRegister 	(lua_State *L) { PROFILE
	cLuaBind<cObjectController>::GetSingletonPtr(new cObjectController_L())->LuaRegister(L);
}




#if 0
using namespace Ogre;

class cObject;
class lua_State;

class cObject_L : public cLuaBind<cObject> { public:
		/// called by Register(), registers object-methods (see cLuaBind constructor for examples)
		virtual void RegisterMethods	(lua_State *L) { PROFILE
			lua_register(L,"RayQuery",			&cObject_L::RayQuery);
			lua_register(L,"GetIntersecting",	&cObject_L::GetIntersecting);
		}

	// object methods exported to lua
		
	// static methods exported to lua

		/// returns list : objID1, hitDist1, objID2, hitDist2, ...
		/// hit-list	RayQuery		(x,y,z,dx,dy,dz) hit=objid,dist
		static int		RayQuery		(lua_State *L) { PROFILE
			// TODO : repair (location system)... currently unused though
			/*
			std::vector<std::pair<Real,cObject*> > resultset;
			cGame::GetSingleton().RayQuery(luaSFZ_checkVector3(L,1),luaSFZ_checkVector3(L,4),resultset);

			// push 2 values for each hit : objid,dist
			int nres = 0;
			for (std::vector<std::pair<Real,cObject*> >::iterator itor=resultset.begin();itor!=resultset.end();++itor) {
				lua_pushnumber(L,(*itor).second->miID);
				lua_pushnumber(L,(*itor).first);
				luaL_checkstack(L, 2, "too many results");
				nres += 2;
			}
			return nres;
			*/
		}

		/// returns list : objID1, objID2, ...
		/// objid-list	GetIntersecting	(x,y,z,rad)
		static int		GetIntersecting	(lua_State *L) { PROFILE
			std::set<size_t> resultset;
			
			// TODO : repair (location system)... currently unused though
			// cGame::GetSingleton().GetIntersectingIDs(luaSFZ_checkVector3(L,1),luaL_checknumber(L,4),resultset);

			// push 1 value for each hit : objid
			int nres = 0;
			/*
			for (std::set<size_t>::iterator itor=resultset.begin();itor!=resultset.end();++itor) {
				lua_pushnumber(L,(*itor));
				luaL_checkstack(L, 1, "too many results");
				++nres;
			}*/
			return nres;
		}

		virtual const char* GetLuaTypeName () { return "sfz.objcontroller"; }
};
#endif
