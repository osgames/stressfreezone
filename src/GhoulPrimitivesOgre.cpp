#include "lugre_prefix.h"
#include <Ogre.h>
#include "GhoulPrimitives.h"

using namespace GhoulPrimitive;
using namespace Ogre;
using namespace Lugre;


#define MY_VERTEX_BINDING_SOURCE 0



/*
TODO : useful ?
# Split your vertex buffers up if you find that your usage patterns for different elements of the vertex are different.
No point having one huge updateable buffer with all the vertex data in it, if all you need to update is the texture coordinates.
Split that part out into it's own buffer and make the rest HBU_STATIC_WRITE_ONLY.

see also
vertex buffer :	file:///usr/src/ogrenew/Docs/manual/manual_42.html#SEC178
index buffer :	file:///usr/src/ogrenew/Docs/manual/manual_47.html#SEC186
*/


/// construction
OgreRenderableDrawer::OgreRenderableDrawer	() { PROFILE
	setMaterial("BaseWhiteNoLighting");
	mReady = false;
	mRenderOp.vertexData = new VertexData;
	mIndexBufferInitialized = false;
	mVertexDecInitialized = false;
	mVertexBufferCapacity = 0;
	mIndexBufferCapacity = 0;
}

OgreRenderableDrawer::~OgreRenderableDrawer() { PROFILE
	delete mRenderOp.vertexData;
	if (mIndexBufferInitialized)
		delete mRenderOp.indexData;
}

// Drawer interface
void	OgreRenderableDrawer::Prepare		(eOpType opType,eGeometryChange gcHint,size_t vertexCount,size_t indexCount,bool hasNormals,bool hasTexCoords) { PROFILE
	assert(!mReady && "Prepare called twice");

	// Create vertex declaration
	if (!mVertexDecInitialized || hasNormals != mHasNormals || mHasTexCoords != hasTexCoords) {
		mHasNormals = hasNormals;
		mHasTexCoords = hasTexCoords;
		VertexDeclaration *decl = mRenderOp.vertexData->vertexDeclaration;
		if (mVertexDecInitialized) {
			while (decl->getVertexSize(MY_VERTEX_BINDING_SOURCE) > 0) {
				//printf("Clearing old vertexdecl\n");
				decl->removeElement(0); // clear would have been nice
			}
		}

		//printf("create vertex decl\n");
		size_t offset = 0;
		offset += decl->addElement(MY_VERTEX_BINDING_SOURCE, 0, VET_FLOAT3, VES_POSITION).getSize();
		if (mHasNormals)	offset += decl->addElement(MY_VERTEX_BINDING_SOURCE,offset,VET_FLOAT3,VES_NORMAL).getSize();
		if (mHasTexCoords)	offset += decl->addElement(MY_VERTEX_BINDING_SOURCE,offset,VET_FLOAT2,VES_TEXTURE_COORDINATES).getSize();

		mVertexDecInitialized = true;

		/*
		NOTES :
		decl->addElement(source,offset,theType,semantic,index=0);
		// VES_DIFFUSE,VES_SPECULAR
		// VES_BLEND_WEIGHTS,VES_BLEND_INDICES,VES_BINORMAL,VES_TANGENT
		// VET_FLOAT1-4,VET_COLOUR,
		// source=POSITION_BINDING
		source 	The binding index of HardwareVertexBuffer which will provide the source for this element. See VertexBufferBindingState for full information.
		offset 	The offset in bytes where this element is located in the buffer
		theType 	The data format of the element (3 floats, a colour etc)
		semantic 	The meaning of the data (position, normal, diffuse colour etc)
		index 	Optional index for multi-input elements like texture coordinates
		//size_t offset = decl->getVertexSize(MY_VERTEX_BINDING_SOURCE);
		*/
	}

	// initialize RenderOp
	mRenderOp.operationType = GetOgreOpType(opType);
	mRenderOp.useIndexes = indexCount > 0;
	if (mRenderOp.useIndexes && !mIndexBufferInitialized) {
		mRenderOp.indexData = new IndexData;
		mIndexBufferInitialized = true;
	}
	mRenderOp.vertexData->vertexCount = vertexCount;
	if (mRenderOp.useIndexes) mRenderOp.indexData->indexCount = indexCount;

	// init membervars for receiving vertices
	mReady = true;
	miVerticesLeft = vertexCount;
	miIndicesLeft = indexCount;
	mBoundingBoxEmpty = true;
	mvAABMin = Vector3::ZERO;
	mvAABMax = Vector3::ZERO;

	// if there are no vertices, there is no need for a buffer
	if (vertexCount <= 0) return;

	// Create new vertex buffer
	if (mVertexBufferCapacity != vertexCount) {
		mVertexBufferCapacity  = vertexCount;
		//printf("alloc vertexbuffer for max %d vertices\n",mVertexBufferCapacity);
		mHWVBuf = HardwareBufferManager::getSingleton().createVertexBuffer(
				mRenderOp.vertexData->vertexDeclaration->getVertexSize(MY_VERTEX_BINDING_SOURCE),
				vertexCount,
				HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY);
		// Bind buffer
		mRenderOp.vertexData->vertexBufferBinding->setBinding(MY_VERTEX_BINDING_SOURCE, mHWVBuf);

		// TODO : memory leak if reassigned ??
	}
	mRenderOp.vertexData->vertexCount = vertexCount;


	// Create new index buffer
	//printf("preindex\n");
	if (mIndexBufferCapacity != indexCount && mRenderOp.useIndexes) {
		mIndexBufferCapacity  = indexCount;
		//printf("alloc index for max %d indices\n",mIndexBufferCapacity);
		mHWIBuf = HardwareBufferManager::getSingleton().createIndexBuffer(
				HardwareIndexBuffer::IT_16BIT,
				indexCount,
				HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY);
		mRenderOp.indexData->indexBuffer = mHWIBuf;
		// TODO : memory leak if reassigned ??
		//HardwareIndexBufferSharedPtr iBuf = mRenderOp.indexData->indexBuffer;
	}
	if (mRenderOp.useIndexes) mRenderOp.indexData->indexCount = indexCount;

	//mHWVBuf = mRenderOp.vertexData->vertexBufferBinding->getBuffer(MY_VERTEX_BINDING_SOURCE);
	mWritePtr = (vertexCount > 0) ? (static_cast<Real*>(mHWVBuf->lock(HardwareBuffer::HBL_DISCARD))) : 0;
	mIndexWritePtr = (indexCount > 0) ? (static_cast<unsigned short*>(mHWIBuf->lock(HardwareBuffer::HBL_DISCARD))) : 0;
}

void	OgreRenderableDrawer::AddVertex		(kReal x,kReal y,kReal z,kReal nx,kReal ny,kReal nz,kReal u,kReal v) { PROFILE
	//printf("AddVertex(%0.1f,%0.1f,%0.1f, %0.1f,%0.1f,%0.1f, %0.1f,%0.1f)\n",x,y,z,nx,ny,nz,u,v);
	assert(mReady && "Prepare not called");
	assert(miVerticesLeft > 0 && "vertex buffer overflow");
	if (miVerticesLeft <= 0) return;
	--miVerticesLeft;
	*mWritePtr++ = x;
	*mWritePtr++ = y;
	*mWritePtr++ = z;
	if (mHasNormals) {
		*mWritePtr++ = nx;
		*mWritePtr++ = ny;
		*mWritePtr++ = nz;
	}
	if (mHasTexCoords) {
		*mWritePtr++ = u;
		*mWritePtr++ = v;
	}
	if (mBoundingBoxEmpty || mvAABMin.x > x) mvAABMin.x = x;
	if (mBoundingBoxEmpty || mvAABMin.y > y) mvAABMin.y = y;
	if (mBoundingBoxEmpty || mvAABMin.z > z) mvAABMin.z = z;
	if (mBoundingBoxEmpty || mvAABMax.x < x) mvAABMax.x = x;
	if (mBoundingBoxEmpty || mvAABMax.y < y) mvAABMax.y = y;
	if (mBoundingBoxEmpty || mvAABMax.z < z) mvAABMax.z = z;
	mBoundingBoxEmpty = false;
}

void	OgreRenderableDrawer::AddIndex	(IndexInt i) { PROFILE
	//printf("AddIndex(%d)\n",i);
	assert(mReady && "Prepare not called");
	assert(miIndicesLeft > 0 && "index buffer overflow");
	if (miIndicesLeft <= 0) return;
	--miIndicesLeft;
	*mIndexWritePtr++ = i;
}

void	OgreRenderableDrawer::Finish		() { PROFILE
	assert(mReady && "Prepare not called");
	assert(miVerticesLeft == 0 && "not enough vertices written");
	assert(miIndicesLeft == 0 && "not enough indices written");
	mBox.setExtents(mvAABMin,mvAABMax);
	mReady = false;
	if (mVertexBufferCapacity > 0) mHWVBuf->unlock();
	if (mIndexBufferCapacity > 0) mHWIBuf->unlock();
}

Ogre::Real	OgreRenderableDrawer::getBoundingRadius(void) const { PROFILE
	return Math::Sqrt(std::max(mBox.getMaximum().squaredLength(), mBox.getMinimum().squaredLength()));
}

Ogre::Real	OgreRenderableDrawer::getSquaredViewDepth(const Ogre::Camera* cam) const { PROFILE
	Vector3 vMin, vMax, vMid, vDist;
	vMin = mBox.getMinimum();
	vMax = mBox.getMaximum();
	vMid = ((vMin - vMax) * 0.5) + vMin;
	vDist = cam->getDerivedPosition() - vMid;
	return vDist.squaredLength();
}


/// Set the type of operation to draw with.
Ogre::RenderOperation::OperationType	OgreRenderableDrawer::GetOgreOpType	(const eOpType opType) { PROFILE
	switch (opType) {
		default:
		case kOpType_POINT_LIST		: return Ogre::RenderOperation::OT_POINT_LIST;
		case kOpType_LINE_LIST		: return Ogre::RenderOperation::OT_LINE_LIST;
		case kOpType_LINE_STRIP		: return Ogre::RenderOperation::OT_LINE_STRIP;
		case kOpType_TRIANGLE_LIST	: return Ogre::RenderOperation::OT_TRIANGLE_LIST;
		case kOpType_TRIANGLE_STRIP	: return Ogre::RenderOperation::OT_TRIANGLE_STRIP;
		case kOpType_TRIANGLE_FAN	: return Ogre::RenderOperation::OT_TRIANGLE_FAN;
	}
}

/// a hint at how often the geometry will change (and will need to be resend to the 3d-card)
Ogre::HardwareBuffer::Usage				OgreRenderableDrawer::GetOgreHWBUsage	(const eGeometryChange geometryChangeHint) { PROFILE
	switch (geometryChangeHint) {
		default:
		case kGeometryChange_Seldom				: return Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY;
		case kGeometryChange_Often				: return Ogre::HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY;
		case kGeometryChange_AlmostEveryFrame	: return Ogre::HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE;
	}
	/*
	from  file:///usr/src/ogrenew/Docs/manual/manual_38.html :
	HBU_STATIC
		SLOW because READ ENABLED
		This means you do not need to update the buffer very often, but you might occasionally want to read from it.

	HBU_STATIC_WRITE_ONLY
		This means you do not need to update the buffer very often, and you do not need to read from it. However, you may read from it's shadow buffer if you set one up (See section 5.3 Shadow Buffers). This is the optimal buffer usage setting.

	HBU_DYNAMIC
		SLOW because READ ENABLED
		This means you expect to update the buffer often, and that you may wish to read from it. This is the least optimal buffer setting.

	HBU_DYNAMIC_WRITE_ONLY
		This means you expect to update the buffer often, but that you never want to read from it. However, you may read from it's shadow buffer if you set one up (See section 5.3 Shadow Buffers). If you use this option, and replace the entire contents of the buffer every frame, then you should use HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE instead, since that has better performance characteristics on some platforms.

	HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE
		This means that you expect to replace the entire contents of the buffer on an extremely regular basis, most likely every frame. By selecting this option, you free the system up from having to be concerned about losing the existing contents of the buffer at any time, because if it does lose them, you will be replacing them next frame anyway. On some platforms this can make a significant performance difference, so you should try to use this whenever you have a buffer you need to update regularly. Note that if you create a buffer this way, you should use the HBL_DISCARD flag when locking the contents of it for writing.
	*/
}
