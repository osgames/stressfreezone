#include "lugre_prefix.h"
#include "lugre_robstring.h"
#include "lugre_main.h"
#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#define PATH_MAIN_LUA		strprintf("%s/main.lua",(LUA_DIR)).c_str()
#define PATH_LUGRE_LUA		strprintf("%s/lua",(LUGRE_DIR)).c_str()

using namespace Lugre;

void	SFZ_RegisterLuaPlugin	();

// #define USE_WINMAIN

#ifdef USE_WINMAIN
INT WINAPI	WinMain		(HINSTANCE hInst, HINSTANCE hPrevInstance, LPSTR strCmdLine,int nCmdShow) { // nCmdShow : show state of window
#else
int			main		(int argc, char* argv[]) {
#endif
	// create a unix-like commandline and show console in win
	#ifdef USE_WINMAIN
	int	argc = 0;
	char **argv = Lugre_ParseWinCommandLine(argc);
	Lugre_ShowWin32Console();
	#endif
	
	// register plugins
	
	SFZ_RegisterLuaPlugin();
	
	// start mainloop
	
	Lugre_Run(argc,argv,PATH_MAIN_LUA,PATH_LUGRE_LUA);
	
	return 0;
}
