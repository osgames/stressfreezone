#if 0  // OBSOLETE FILE
#include "prefix.h"
#include "client.h"
#include "object.h"
#include "timer.h"
#include <math.h>
#include "HUDElement2D.h"
#include "input.h"
#include "scripting.h"
#include "ogrewrapper.h"
#include <Ogre.h>

#define kHUDOverlayZOrder 400

#ifndef fmax
    #define fmax(a,b) (a<b?b:a)
#endif

#ifndef fmin
    #define fmin(a,b) (a>b?b:a)
#endif

using namespace Ogre;

Ogre::Overlay*		cHUDElement2D::mpHUDOverlay; // one global overlay

enum {
	kListenerBinding_EveryFrame,
	kListenerBinding_EveryHalfSecond,
	kListenerBinding_ObjectDeath,
};

cHUDElement2D::cHUDElement2D() : 
		mbVisible(false), mbObjHasBeenKilled(false), mbPosModeObject_UpdateSize(false), mbWatchMouse(false), mbMouseIsOver(false),
		mvBase(0,0), mvParam(0,0), mvParam2(0,0), mvSizeParam(1.0,1.0), mfCurRotate(0),
		mpGfx2D(0) { PROFILE
	miPosMode = kPosMode_None;
	miTurnMode = kTurnMode_None;
	miTextMode = kTextMode_None;
	static size_t miLastUID = 0;
	miUID = ++miLastUID;
	// warning ! avoid side effects here, use Init() instead, one instance is used as prototype for scripting
}

cHUDElement2D::~cHUDElement2D() { PROFILE
	if (mpGfx2D) { delete mpGfx2D; mpGfx2D = 0; }
}

/// TODO : call me !
void cHUDElement2D::Init () {
	// init the static mpHUDOverlay (one overlay for all hudelements)
	if (!mpHUDOverlay) mpHUDOverlay = cGfx2D::CreateOverlay("HUDOverlay",kHUDOverlayZOrder);
		
	mpGfx2D = new cGfx2D(mpHUDOverlay);
	
	// register listeners
	mpObj1.RegisterListener(this,(int)kListenerBinding_ObjectDeath);
	cTimer::GetSingletonPtr()->RegisterFrameIntervalListener(	this,0,	 (int)kListenerBinding_EveryFrame);
	cTimer::GetSingletonPtr()->RegisterIntervalListener(		this,500,(int)kListenerBinding_EveryHalfSecond);	
}


void cHUDElement2D::Listener_Notify (cListenable* pTarget,const size_t eventcode,void* param,void* userdata) { PROFILE
	static char textbuf[255];
	switch (userdata) {
		case kListenerBinding_ObjectDeath: {
			// die when the target dies
			mbVisible = false;
			VarUpdate();
			mbObjHasBeenKilled = true; // don't delete directly, as this would destroy the smartpointer during iteration
		} break;
		case kListenerBinding_EveryHalfSecond: {
			if (!mbVisible) break;
			switch (miTextMode) {
				case kTextMode_AbsSpeed: {
						cObject* pObj1 = *mpObj1;
						if(!pObj1) pObj1 = *cClient::mpPlayerShip;
						if (pObj1) {
								Real dist = (pObj1->mvVel).length();
								if (dist < 10000) // output in m/s
										sprintf(textbuf,"%0.0fm/s",dist);
								else	sprintf(textbuf,"%0.0fkm/s",dist/1000);
								mpGfx2D->SetText(textbuf);
						} else {
								mpGfx2D->SetText("");
						}
				} break;
				case kTextMode_Dist: {
					cObject* pObj1 = *mpObj1;
					cObject* pObj2 = *cClient::mpPlayerShip;
					if (pObj1 && pObj2) {
						Real dist = (pObj1->mvPos - pObj2->mvPos).length();
						if (dist < 10000) // output in m
								sprintf(textbuf,"%0.0fm",dist);
						else	sprintf(textbuf,"%0.0fkm",dist/1000);
						mpGfx2D->SetText(textbuf);
					} else {
						mpGfx2D->SetText("");
					}
				} break;
				case kTextMode_FPS: {
					static size_t iLastFPS_Frames = 0;
					static float fLastFPS = 0;
					size_t curframes = cTimer::miCurFrameNum;
					if (iLastFPS_Frames != curframes) {
						fLastFPS = (curframes - iLastFPS_Frames) * 2.0;
						iLastFPS_Frames = curframes;
					}
					sprintf(textbuf,"%3.0f FPS",fLastFPS);
					mpGfx2D->SetText(textbuf);
				} break;
				case kTextMode_RelSpeed:
					// TODO...
				break;
			}
		} break;
		case kListenerBinding_EveryFrame: {
			
			if (mbObjHasBeenKilled) {
				delete this;
				return;
			}
			
			if (mbWatchMouse) {
				bool bNewMouseIsOver = mpGfx2D->IsPointWithin(cInput::iMouse[0],cInput::iMouse[1]);
				if (bNewMouseIsOver && !mbMouseIsOver) cScripting::GetSingletonPtr()->LuaCall("MouseEnterHUDElement","i",(int)miUID);
				if (!bNewMouseIsOver && mbMouseIsOver) cScripting::GetSingletonPtr()->LuaCall("MouseLeaveHUDElement","i",(int)miUID);
				mbMouseIsOver = bNewMouseIsOver;
			}
			
			TrackingStep();
		} break;
	}
}

/// called when a member-variable is changed via lua-script
void	cHUDElement2D::VarUpdate		() { PROFILE
	mpGfx2D->SetRotate(mfCurRotate);
	TrackingStep();
}

/// updates position and rotation every frame based on miPosMode and miTurnMode
void	cHUDElement2D::TrackingStep		() { PROFILE
	bool bShow = mbVisible;
	if (!(miPosMode == kPosMode_None && miTurnMode == kTurnMode_None) && bShow) {
		
		cObject* pObj1 = *mpObj1;
		cObject* pObj2 = *cClient::mpPlayerShip;
		static Real x,y,cx,cy,sw,sh;
		sw = Real(cGfx2D::GetViewportWidth());
		sh = Real(cGfx2D::GetViewportHeight());
		bool bIsOnSreen = false;

		switch (miPosMode) {
			case kPosMode_Object:
				if (pObj1 && pObj1->IsInScene()) {
					bIsOnSreen = GetScreenPos(pObj1->mvPos,x,y,pObj1->mfRad,cx,cy);
					mpGfx2D->SetPos(mvBase.x + (x + 1.0 + mvParam.x * cx)*0.5*sw,mvBase.y + (-y + 1.0 + mvParam.y * cy)*0.5*sh);
					if (mbPosModeObject_UpdateSize) mpGfx2D->SetDimensions(cx*sw+mvSizeParam.x,cy*sh+mvSizeParam.y);
					if (!bIsOnSreen) bShow = false;
				} else { bShow = false; }
			break;
			case kPosMode_Aim:
				if (pObj1 && pObj2 && pObj1->IsInScene()) {
					Real dist = (pObj1->mvPos - pObj2->mvPos).length();
					Real fVorhaltTime = (mfParam > 0.0) ? (dist / mfParam) : 0.0;
					bIsOnSreen = GetScreenPos(pObj1->mvPos + (pObj1->mvVel - pObj2->mvVel)*fVorhaltTime,x,y,pObj1->mfRad,cx,cy);
					mpGfx2D->SetPos(mvBase.x + (x + 1.0 + mvParam.x * cx)*0.5*sw,mvBase.y + (-y + 1.0 + mvParam.y * cy)*0.5*sh);
					if (mbPosModeObject_UpdateSize) mpGfx2D->SetDimensions(cx*sw+mvSizeParam.x,cy*sh+mvSizeParam.y);
					if (!bIsOnSreen) bShow = false;
				} else { bShow = false; }
			break; /// pos=where to aim for playership shooting at mpObj1 with bulletvelocity=mfParam
			case kPosMode_ObjectDir:
				if (pObj1 && pObj1->IsInScene()) {
					bIsOnSreen = GetScreenPos(pObj1->mvPos,x,y,pObj1->mfRad,cx,cy);
					bShow = !bIsOnSreen;
					if (bShow) {
						/// transform mvParam and mvParam from pixelcoords to relative coords [0;1]
						if (mvParam2.x <= 0.0) mvParam2.x = sw-mvParam2.x;
						if (mvParam2.y <= 0.0) mvParam2.y = sh-mvParam2.y;
						mpGfx2D->SetPos(	fmax(mvParam.x,fmin(mvParam2.x,mvBase.x + 0.5*(x+1.0)*sw)),
											fmax(mvParam.y,fmin(mvParam2.y,mvBase.y + 0.5*(-y+1.0)*sh)) );
					}
				} else { bShow = false; }
			break; /// only visible if target is not on screen, and then always on the border of the screen, min=mvParam max=mvParam2
		}
		switch (miTurnMode) {
			case kTurnMode_Constant:	mpGfx2D->SetRotate(mfCurRotate + mfTurnBase); break;/// turnspeed = mfTurnBase
			case kTurnMode_ObjectDist:	{
				Real dist = (pObj1 && pObj2) ? ((pObj1->mvPos - pObj2->mvPos).length()) : 0.0;
				/// dist to obj = x, turnspeed = min(mfTurnBase + mfTurnParam*x + mfTurnParam2*x^2,mfTurnMax)
				mpGfx2D->SetRotate(mfCurRotate + fmax(mfTurnMin,fmin(mfTurnMax,mfTurnBase + mfTurnParam*dist + mfTurnParam2*dist*dist)));
			} break;
			case kTurnMode_ObjectDir:
				if (miPosMode == kPosMode_ObjectDir) {
					if (pObj1) {
						mpGfx2D->SetRotate(atan2(y-0.5,x-0.5));
					}
				}
			break;/// rotation is set to point to object direction + mfTurnBase, useful together with kPosMode_ObjectDir
		}
	}
	mpGfx2D->SetVisible(bShow);
}


/// returns true if in front of cam, and fills x,y with clamped screencoords in [-1;1]
/// and fills cx,cy with projected size on screen in [0;1]
bool	cHUDElement2D::GetScreenPos	(const Vector3& pos,Real& x,Real& y,const Real rad,Real& cx,Real& cy) { PROFILE
	Camera* cam = cOgreWrapper::GetSingleton().mCamera;
	Vector3 eyeSpacePos = cam->getViewMatrix(true) * pos;
	// z < 0 means in front of cam
	if (eyeSpacePos.z < 0) {
		Vector3 screenSpacePos = cam->getProjectionMatrix() * eyeSpacePos;
		x = screenSpacePos.x;
		y = screenSpacePos.y;
		bool bIsOnSreen = true;
		if (x < -1.0) { x = -1.0; bIsOnSreen = false; } if (x > 1.0) { x = 1.0; bIsOnSreen = false; }
		if (y < -1.0) { y = -1.0; bIsOnSreen = false; } if (y > 1.0) { y = 1.0; bIsOnSreen = false; }
		if (bIsOnSreen) {
			Vector3 spheresize(rad, rad, eyeSpacePos.z);
			spheresize = cam->getProjectionMatrix() * spheresize;
			cx = spheresize.x;
			cy = spheresize.y;
		} else {
			cx = 0;
			cy = 0;
		}
		return bIsOnSreen;
	} else {
		cx = 0;
		cy = 0;
		x = (-eyeSpacePos.x > 0) ? -1 : 1;
		y = (-eyeSpacePos.y > 0) ? -1 : 1;
		return false;
	}
}
#endif
