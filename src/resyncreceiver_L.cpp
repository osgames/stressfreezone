#include "lugre_prefix.h"
#include "lugre_luabind.h"
#include "lugre_net.h"
#include "resyncreceiver.h"

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}
using namespace Lugre;

class cResyncReceiver_L : public cLuaBind<cResyncReceiver> { public:
		/// called by Register(), registers object-methods (see cLuaBind constructor for examples)
		virtual void RegisterMethods	(lua_State *L) { PROFILE
			lua_register(L,"CreateResyncReceiver",		&cResyncReceiver_L::CreateResyncReceiver);
			lua_register(L,"SetGlobalResyncReceiver",	&cResyncReceiver_L::SetGlobalResyncReceiver);

			#define REGISTER_METHOD(methodname) mlMethod.push_back(make_luaL_reg(#methodname,&cResyncReceiver_L::methodname));
			REGISTER_METHOD(Destroy);
			REGISTER_METHOD(ReceiveResyncs);
			REGISTER_METHOD(ReceiveResyncsFromFIFO);
		}

	// static methods exported to lua

		/// for lua : cResyncReceiver*	CreateResyncReceiver		();
		static int						CreateResyncReceiver		(lua_State *L) { PROFILE 
			return CreateUData(L,new cResyncReceiver()); 
		}
		
		/// for lua : void	SetGlobalResyncReceiver	(cResyncReceiver*)
		static int			SetGlobalResyncReceiver	(lua_State *L) { PROFILE 
			gpResyncReceiver = (lua_gettop(L) >= 1 && !lua_isnil(L,1)) ? checkudata(L) : 0;
			return 0; 
		}

	// object methods exported to lua
		
		static int	Destroy				(lua_State *L) { PROFILE 
			delete checkudata_alive(L);
			return 0; 
		}
		
		/// iServerAddr is a light userdata representing the ip address, e.g. from net_L.cpp : cConnection_L::GetRemoteAddress
		/// for lua : int	ReceiveResyncs	(cUDP_ReceiveSocket*,iServerAddr)
		static int			ReceiveResyncs	(lua_State *L) { PROFILE 
			cUDP_ReceiveSocket*	pUDP_ReceiveSocket	= cLuaBind<cUDP_ReceiveSocket>::checkudata_alive(L,2);
			
			uint32 				iServerAddr			= (uint32)(long)(lua_touserdata(L,3));
			// iAddr is not really a pointer, but lua has problems encoding full 32bit integers in it's number type (float)
			
			lua_pushnumber(L,checkudata_alive(L)->ReceiveResyncs(*pUDP_ReceiveSocket,iServerAddr));
			return 1; 
		}
		
		/// for lua : void	ReceiveResyncsFromFIFO	(data)
		static int			ReceiveResyncsFromFIFO	(lua_State *L) { PROFILE 
			cFIFO*			pFIFO	= cLuaBind<cFIFO>::checkudata_alive(L,2);
			checkudata_alive(L)->ReceiveResyncsFromFIFO(*pFIFO);
			return 0; 
		}

		virtual const char* GetLuaTypeName () { return "sfz.ResyncReceiver"; }
};

/// lua binding
void	cResyncReceiver::LuaRegister 	(lua_State *L) { PROFILE
	cLuaBind<cResyncReceiver>::GetSingletonPtr(new cResyncReceiver_L())->LuaRegister(L);
}
