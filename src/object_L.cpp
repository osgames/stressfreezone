#include "lugre_prefix.h"
#include "lugre_luabind.h"
#include "lugre_ode.h"
#include "object.h"

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}
using namespace Lugre;


class cObject_L : public cLuaBind<cObject> { public:
		/// called by Register(), registers object-methods (see cLuaBind constructor for examples)
		virtual void RegisterMethods	(lua_State *L) { PROFILE
			lua_register(L,"CreateObject",		&cObject_L::CreateObject);

			#define REGISTER_METHOD(methodname) mlMethod.push_back(make_luaL_reg(#methodname,&cObject_L::methodname));
			REGISTER_METHOD(Destroy);
			REGISTER_METHOD(GetID);
			REGISTER_METHOD(SetID);
			REGISTER_METHOD(ResetResyncCounterLow);
			REGISTER_METHOD(SetParentLocation);
			REGISTER_METHOD(SetPhysicForm);
			REGISTER_METHOD(SetController);
			REGISTER_METHOD(SetGfx);
		}

		/// called by Register(), registers object-member-vars (see cLuaBind::RegisterMembers() for examples)
		virtual void RegisterMembers 	() { PROFILE
			cObject* prototype = new cObject(); // memory leak : never deleted, but better than side effects
			cMemberVar_REGISTER(prototype,	kVarType_int,			miLastChangeTime,		0);
			cMemberVar_REGISTER(prototype,	kVarType_int,			miResyncCounterHigh,	0);
			cMemberVar_REGISTER(prototype,	kVarType_Float,			mfBoundingRad,			0);
			cMemberVar_REGISTER(prototype,	kVarType_Float,			mfDistToLocationCenter,	0);
			cMemberVar_REGISTER(prototype,	kVarType_Float,			mfMaxSpeed,				0);
			cMemberVar_REGISTER(prototype,	kVarType_bool,			mbResynced,		0);
			cMemberVar_REGISTER(prototype,	kVarType_Vector3,		mvPos,			0);
			cMemberVar_REGISTER(prototype,	kVarType_Vector3,		mvVel,			0);
			cMemberVar_REGISTER(prototype,	kVarType_Vector3,		mvAccel,		0);
			cMemberVar_REGISTER(prototype,	kVarType_Quaternion,	mqRot,			0);
			cMemberVar_REGISTER(prototype,	kVarType_Quaternion,	mqTurn,			0);
		}

	// static methods exported to lua

		/// for lua : cObject*	CreateObject		();
		static int				CreateObject		(lua_State *L) { PROFILE 
			return CreateUData(L,new cObject()); 
		}

	// object methods exported to lua
		
		static int	Destroy				(lua_State *L) { PROFILE 
			delete checkudata_alive(L);
			return 0; 
		}
		
		/// for lua : int	GetID	()
		static int			GetID	(lua_State *L) { PROFILE 
			lua_pushnumber(L,checkudata_alive(L)->GetID());
			return 1; 
		}
		
		/// for lua : void	SetID	()
		static int			SetID	(lua_State *L) { PROFILE 
			checkudata_alive(L)->SetID(luaL_checkint(L,2));
			return 0; 
		}
		
		/// for lua : void	SetGfx	(gfx3d)
		static int			SetGfx	(lua_State *L) { PROFILE 
			checkudata_alive(L)->mpGfx3D = (lua_gettop(L) >= 2 && !lua_isnil(L,2)) ? cLuaBind<cGfx3D>::checkudata(L,2) : 0;
			return 0; 
		}
		
		/// for lua : void	ResetResyncCounterLow	()
		static int			ResetResyncCounterLow	(lua_State *L) { PROFILE 
			checkudata_alive(L)->miResyncCounterLow = 0;
			return 0; 
		}
		
		/// for lua : void	SetParentLocation	(location)
		static int			SetParentLocation	(lua_State *L) { PROFILE 
			cLocation* pLocation = cLuaBind<cLocation>::checkudata(L,2);
			checkudata_alive(L)->SetParentLocation(pLocation);
			return 0; 
		}
		
		
		/// for lua : void	SetPhysicForm	(odeobject)
		static int			SetPhysicForm	(lua_State *L) { PROFILE 
			#ifdef ENABLE_ODE
			cOdeObject *o = checkudata_alive(L)->mpPhysicForm;
			if(o){
				delete o;
			}
			checkudata_alive(L)->mpPhysicForm = cLuaBind<cOdeObject>::checkudata_alive(L,2);
			#endif
			return 0; 
		}
		
		/// for lua : void	SetController	(objectcontroller)
		static int			SetController	(lua_State *L) { PROFILE 
			cObjectController* p = (lua_gettop(L) >= 2 && !lua_isnil(L,2)) ? cLuaBind<cObjectController>::checkudata(L,2) : 0;
			checkudata_alive(L)->SetController(p);
			return 0; 
		}

		virtual const char* GetLuaTypeName () { return "sfz.obj"; }
};

/// lua binding
void	cObject::LuaRegister 	(lua_State *L) { PROFILE
	cLuaBind<cObject>::GetSingletonPtr(new cObject_L())->LuaRegister(L);
}
