#include "lugre_prefix.h"
#include "lugre_net.h"
#include "lugre_fifo.h"
#include "location.h"
#include "object.h"
#include <math.h> // random

using namespace Lugre;

cLocation::cLocation	() : mfBoundingRad(0) {}

cLocation::~cLocation	() {
	assert(mlObjects.size() == 0 && "cLocation should be empty on destruction");
}

void	cLocation::StepAllObjects	(const int iCurTime,const float fPhysStepTime,const bool bIncResyncCounterLow) {
	mfBoundingRad = 0;
	for (std::list<cObject*>::iterator itor=mlObjects.begin();itor!=mlObjects.end();++itor) (*itor)->Step(iCurTime,fPhysStepTime,bIncResyncCounterLow);
}

/// efficient mass delete
void	cLocation::DeleteAllObjects	() {
	cObject* pObj;
	for (std::list<cObject*>::iterator itor=mlObjects.begin();itor!=mlObjects.end();++itor) {
		pObj = (*itor);
		pObj->_HackForgetParentLocation(); // prevent object from unregistring itself normally
		delete pObj;
	}
	mlObjects.clear();
}

/// only resyncs objects with mbResynced = true AND :
///   (obj.miLastChangeTime >= iMinLastChangeTime or random(fRandomResyncProb))
/// returns how many resync packets have been sent
int		cLocation::SendResyncs		(cUDP_SendSocket& pUDPSocket,const uint32 iAddr,const int iPort,const int iMinLastChangeTime,const float fRandomResyncProb) {
	static cFIFO myFIFO;
	cObject* pObj;
	int packetcounter = 0;
	float fiRandProb = fRandomResyncProb * float(RAND_MAX);
	for (std::list<cObject*>::iterator itor=mlObjects.begin();itor!=mlObjects.end();++itor) {
		pObj = (*itor);
		if (pObj->mbResynced && (pObj->miLastChangeTime >= iMinLastChangeTime || (fRandomResyncProb > 0 && float(rand()) < fiRandProb))) {
			myFIFO.Clear();
			pObj->SaveResyncData(myFIFO);
			pUDPSocket.Send(iAddr,iPort,myFIFO);
			++packetcounter;
		}
	}
	return packetcounter;
}

int		cLocation::StoreResyncs		(cFIFO& pFIFO,const int iMinLastChangeTime,const float fRandomResyncProb) {
	cObject* pObj;
	int packetcounter = 0;
	float fiRandProb = fRandomResyncProb * float(RAND_MAX);
	for (std::list<cObject*>::iterator itor=mlObjects.begin();itor!=mlObjects.end();++itor) {
		pObj = (*itor);
		if (pObj->mbResynced && (pObj->miLastChangeTime >= iMinLastChangeTime || (fRandomResyncProb > 0 && float(rand()) < fiRandProb))) {
			pObj->SaveResyncData(pFIFO);
			++packetcounter;
		}
	}
	return packetcounter;	
}
