#include "lugre_prefix.h"
#include "lugre_net.h"
#include "lugre_fifo.h"
#include "object.h"
#include "resyncreceiver.h"

using namespace Lugre;

cResyncReceiver* gpResyncReceiver = 0;

cResyncReceiver::cResyncReceiver	() {}
cResyncReceiver::~cResyncReceiver	() { if (this == gpResyncReceiver) gpResyncReceiver = 0; }

/// only accepts resyncs from the given remote address to prevent other cheaters sending false positions
/// returns the last resultcode from cUDP_ReceiveSocket::Receive, negative indicates error
int		cResyncReceiver::ReceiveResyncs	(cUDP_ReceiveSocket& pUDPSocket,const uint32 iServerAddr) {
	static cFIFO 	myFIFO; 
	uint32			iRemoteAddr;
	cObject* 		pObj;
	int				res;
	
	// clear fifo just to be sure in case of errors
	myFIFO.Clear(); 
	
	// receive all available packets
	while ((res = pUDPSocket.Receive(myFIFO,iRemoteAddr)) > 0) {
		if (iRemoteAddr == iServerAddr) {
			if (myFIFO.size() >= kResyncMessageLen) {
				uint32 iObjID = myFIFO.PeekUint32(0);
				pObj = mlObjects[iObjID];
				//printf("cResyncReceiver::ReceiveResyncs objid=%d obj=0x%08x\n",iObjID,(int)pObj);
				if (pObj) pObj->LoadResyncData(myFIFO); else cObject::SkipResyncData(myFIFO);
			} else {
				// todo : protocol version mismatch ? broken message ?
			}
		} else {
			// todo : report hacking attempt to server ?
		}
		myFIFO.Clear();
	}
	return res;
}

void	cResyncReceiver::ReceiveResyncsFromFIFO	(cFIFO& pFIFO) {
	cObject* 		pObj;
	while (pFIFO.size() >= kResyncMessageLen) {
		uint32 iObjID = pFIFO.PeekUint32(0);
		pObj = mlObjects[iObjID];
		//printf("cResyncReceiver::ReceiveResyncs objid=%d obj=0x%08x\n",iObjID,(int)pObj);
		if (pObj) pObj->LoadResyncData(pFIFO); else cObject::SkipResyncData(pFIFO);
	}
}
