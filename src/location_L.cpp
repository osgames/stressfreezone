#include "lugre_prefix.h"
#include "lugre_luabind.h"
#include "lugre_ogrewrapper.h"
#include "object.h"
#include "location.h"
#include <Ogre.h>

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

using namespace Lugre;



class cLocation_L : public cLuaBind<cLocation> { public:
		/// called by Register(), registers object-methods (see cLuaBind constructor for examples)
		virtual void RegisterMethods	(lua_State *L) { PROFILE
			lua_register(L,"CreateLocation",		&cLocation_L::CreateLocation);

			#define REGISTER_METHOD(methodname) mlMethod.push_back(make_luaL_reg(#methodname,&cLocation_L::methodname));
			REGISTER_METHOD(Destroy);
			REGISTER_METHOD(GetBoundRad);
			REGISTER_METHOD(SetBoundRad);
			REGISTER_METHOD(StepAllObjects);
			REGISTER_METHOD(DeleteAllObjects);
			REGISTER_METHOD(SendResyncs);
			REGISTER_METHOD(StoreResyncs);
			REGISTER_METHOD(IntersectSphere);
			REGISTER_METHOD(Intersect2DRect); 
		}

	// static methods exported to lua

		/// for lua : cLocation*	CreateLocation		();
		static int					CreateLocation		(lua_State *L) { PROFILE 
			return CreateUData(L,new cLocation()); 
		}

	// object methods exported to lua
		
		static int	Destroy				(lua_State *L) { PROFILE 
			delete checkudata_alive(L);
			return 0; 
		}
		
		/// for lua : float	GetBoundRad	()
		static int			GetBoundRad	(lua_State *L) { PROFILE 
			lua_pushnumber(L,checkudata_alive(L)->mfBoundingRad);
			return 1; 
		}
		
		/// for lua : void	SetBoundRad	(float)
		static int			SetBoundRad	(lua_State *L) { PROFILE 
			checkudata_alive(L)->mfBoundingRad = luaL_checknumber(L,2);
			return 0; 
		}
		
		/// for lua : void	DeleteAllObjects	()
		static int			DeleteAllObjects	(lua_State *L) { PROFILE 
			checkudata_alive(L)->DeleteAllObjects();
			return 0; 
		}
		
		
		/// for lua : void	StepAllObjects	(iCurTime,fPhysStepTime,bIncResyncCounterLow=false)
		static int			StepAllObjects	(lua_State *L) { PROFILE 
			bool bIncResyncCounterLow = (lua_gettop(L) >= 4 && !lua_isnil(L,4)) ? (lua_isboolean(L,4) ? lua_toboolean(L,4) : luaL_checkint(L,4)) : false;
			checkudata_alive(L)->StepAllObjects(luaL_checkint(L,2),luaL_checknumber(L,3),bIncResyncCounterLow);
			return 0; 
		}
		
		
		/// iAddr is a light userdata representing the ip address, e.g. from net_L.cpp : cConnection_L::GetRemoteAddress
		/// for lua : int	SendResyncs		(cUDP_SendSocket*,iAddr,iPort,iMinLastChangeTime)
		/// returns the number of resyncs sent
		static int			SendResyncs		(lua_State *L) { PROFILE 
			cUDP_SendSocket*	pUDP_SendSocket		= cLuaBind<cUDP_SendSocket>::checkudata_alive(L,2);
			
			uint32 				iAddr				= (uint32)(long)(lua_touserdata(L,3)); 
			// iAddr is not really a pointer, but lua has problems encoding full 32bit integers in it's number type (float)
			
			int 				iPort				= luaL_checkint(L,4);
			int 				iMinLastChangeTime	= luaL_checkint(L,5);
			float 				fRandomResyncProb	= luaL_checknumber(L,6);
			lua_pushnumber(L,checkudata_alive(L)->SendResyncs(*pUDP_SendSocket,iAddr,iPort,iMinLastChangeTime,fRandomResyncProb));
			return 1; 
		}
		
		/// stores resync packets in a fifo for transmissin other than udp, see also SendResyncs
		/// for lua : int	StoreResyncs		(cFIFO*,iMinLastChangeTime)
		/// returns the number of resyncs stored
		static int			StoreResyncs		(lua_State *L) { PROFILE 
			cFIFO*		pFIFO				= cLuaBind<cFIFO>::checkudata_alive(L,2);
			int 		iMinLastChangeTime	= luaL_checkint(L,3);
			float 		fRandomResyncProb	= luaL_checknumber(L,4);
			lua_pushnumber(L,checkudata_alive(L)->StoreResyncs(*pFIFO,iMinLastChangeTime,fRandomResyncProb));
			return 1; 
		}
		
		/// lists all objects intersecting the given sphere as table with one-based indices
		/// earlyoutrad using location GetBoundRad() only works correctly after physstep and before any other movement
		/// leave earlyoutrad nil to disable early out check
		/// set earlyoutrad = -1 (or < 0) to use location mfBoundingRad
		/// for lua	:	idtable		IntersectSphere		(x,y,z,rad,earlyoutrad=nil)
		static int					IntersectSphere		(lua_State *L) { PROFILE 
			cLocation*		pLocation = checkudata_alive(L);
			Ogre::Vector3	vPos = luaSFZ_checkVector3(L,2);
			float			fRad = luaL_checknumber(L,5);
			
			// check early out
			if (lua_gettop(L) >= 6 && !lua_isnil(L,6)) {
				float fEarlyOutRad = luaL_checknumber(L,6);
				if (fEarlyOutRad < 0) fEarlyOutRad = pLocation->mfBoundingRad;
				if (vPos.squaredLength() > mysquare(fRad + fEarlyOutRad)) return 0;
			}
			
			// construct result table
			lua_newtable(L);
			int i=0;
			cObject* pObj;
			for (std::list<cObject*>::iterator itor=pLocation->mlObjects.begin();itor!=pLocation->mlObjects.end();++itor) {
				pObj = (*itor);
				if ((vPos - pObj->mvPos).squaredLength() <= mysquare(fRad + pObj->mfBoundingRad)) {
					lua_pushnumber( L, pObj->GetID() );
					lua_rawseti( L, -2, ++i );
				}
			}
			return 1;
		}
		
		/// lists all objects whose projected gfx3d boundrad intersects the given 2d rect as table with one-based indices
		/// for lua	:	idtable		Intersect2DRect		(x0,y0,x1,y1,maxdist=0)
		/// idlist[id] = distance
		static int					Intersect2DRect		(lua_State *L) { PROFILE 
			// mpGfx3D 
			cLocation*		pLocation = checkudata_alive(L);
			float			x0 = luaL_checknumber(L,2);
			float			y0 = luaL_checknumber(L,3);
			float			x1 = luaL_checknumber(L,4);
			float			y1 = luaL_checknumber(L,5);
			float			fMaxDist = (lua_gettop(L) >= 6 && !lua_isnil(L,6)) ? luaL_checknumber(L,6) : 0;
			
			// construct result table
			lua_newtable(L);
			cObject* 		pObj;
			cOgreWrapper& 	ogrewrapper = cOgreWrapper::GetSingleton();
			Ogre::Real		x,y,cx,cy;
			Ogre::Real sw = Ogre::Real(ogrewrapper.GetViewportWidth());
			Ogre::Real sh = Ogre::Real(ogrewrapper.GetViewportHeight());
			for (std::list<cObject*>::iterator itor=pLocation->mlObjects.begin();itor!=pLocation->mlObjects.end();++itor) {
				pObj = (*itor);
				if (ogrewrapper.ProjectSizeAndPos(pObj->mvPos,x,y,pObj->mfBoundingRad,cx,cy)) { // if bIsOnScreen
					x = (x + 1.0) * 0.5 * sw;
					y = (-y + 1.0) * 0.5 * sh;
					cx *= sw;
					cy *= sh;
					float fDist = mymax(0.0f,	mymax(	mymax(-(x0 - (x-cx/2)),-(y0 - (y-cy/2))),
														mymax( (x0 - (x+cx/2)), (y0 - (y+cy/2))) ));
					
					//if (x >= x0-cx/2 && x <= x1+cx/2 && y >= y0-cy/2 && y <= y1+cy/2) {
					if (fDist <= fMaxDist) {
						lua_pushnumber( L, fDist );
						lua_rawseti( L, -2, pObj->GetID() );
					}
				}
			}
			return 1;
		}

		virtual const char* GetLuaTypeName () { return "sfz.location"; }
};

/// lua binding
void	cLocation::LuaRegister 	(lua_State *L) { PROFILE
	cLuaBind<cLocation>::GetSingletonPtr(new cLocation_L())->LuaRegister(L);
}
