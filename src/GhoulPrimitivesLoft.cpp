#include "lugre_prefix.h"
#include <math.h>
//#include <cmath> ??
#include "GhoulPrimitives.h"

using namespace GhoulPrimitive;
using namespace Lugre;




// default implementation empty
Loft::Loft		() { PROFILE
	mBase_bAutoNormal = true;
	mUseScale = true;
	mUseRot = true;
	mAutoBackSide = false;
	mSphericalAutoNormals = false;
	mSphericalAutoNormals_Center = Vector3::ZERO;
}

Loft::~Loft		() {
	
}

void	Loft::Update			(Drawer& drawer,bool bHasNormals,bool bHasTexCoords) { PROFILE
	size_t basePointCount = mlBasePoint.size();
	size_t pathPointCount = mlPathPoint.size();
	size_t vertexCount = basePointCount * pathPointCount;
	size_t indexCount = (basePointCount-1) * 6 * (pathPointCount - 1);
	size_t i,j,k,startoff,autobackoffset;
	
	if (mAutoBackSide) {
		autobackoffset = vertexCount;
		indexCount *= 2;
		vertexCount *= 2;
	}
	
	// kOpType_TRIANGLE_STRIP kOpType_LINE_STRIP
	drawer.Prepare(kOpType_TRIANGLE_LIST,kGeometryChange_Seldom,vertexCount,indexCount,bHasNormals,bHasTexCoords);
	
	// vertices
	BasePoint*	base;
	PathPoint*	path;
	Vector3		p,n;
	for (k=0;k<(mAutoBackSide?2:1);++k)
	for (i=0;i<pathPointCount;++i) { path = &mlPathPoint[i];
		for (j=0;j<basePointCount;++j) { base = &mlBasePoint[j];
			
			if (mUseRot) {
				if (mUseScale)
						p = path->p + path->rot * base->p * path->scale;
				else	p = path->p + path->rot * base->p;
			} else {
				if (mUseScale)
						p = path->p + base->p * path->scale;
				else	p = path->p + base->p;
			}
			
			if (mSphericalAutoNormals)
					n = (p - mSphericalAutoNormals_Center).normalisedCopy();
			else	n = (mUseRot)?(path->rot * base->n):(base->n);
			
			if (k==0)					drawer.AddVertex( p , n , base->u + path->uBias, path->v );
			if (mAutoBackSide && k==1)	drawer.AddVertex( p , -n , base->u + path->uBias, path->v );
			// base : p n u
			// path : p v uBias scale rot;
		}
	}
	
	// indices
	for (i=1,startoff=0;i<pathPointCount;++i,startoff+=basePointCount) {
		for (j=0;j<basePointCount-1;++j) {
			drawer.AddIndex(startoff + j+0);
			drawer.AddIndex(startoff + j+0 + basePointCount);
			drawer.AddIndex(startoff + j+1 + basePointCount);
			drawer.AddIndex(startoff + j+0);
			drawer.AddIndex(startoff + j+1 + basePointCount);
			drawer.AddIndex(startoff + j+1);
			
			if (mAutoBackSide) {
				drawer.AddIndex(startoff+autobackoffset + j+0);
				drawer.AddIndex(startoff+autobackoffset + j+1 + basePointCount);
				drawer.AddIndex(startoff+autobackoffset + j+0 + basePointCount);
				drawer.AddIndex(startoff+autobackoffset + j+0);
				drawer.AddIndex(startoff+autobackoffset + j+1);
				drawer.AddIndex(startoff+autobackoffset + j+1 + basePointCount);
			}
		}
	}
	
	drawer.Finish();
}
	
void	Loft::ClearBasePoints	() { PROFILE
	mlBasePoint.clear();
}

void	Loft::AddBasePoint		(kVector3 p,kVector3 n,kReal u) { PROFILE
	SetBasePoint(mlBasePoint.size(),p,n,u);
}

void	Loft::SetBasePoint		(size_t i,kVector3 p,kVector3 n,kReal u) { PROFILE
	if (mlBasePoint.size() < i+1)
		mlBasePoint.resize(i+1);
	if (mBase_bAutoNormal && n.x == 0.0 && n.y == 0.0 && n.z == 0.0)
			mlBasePoint[i] = BasePoint(p,p.normalisedCopy(),u);
	else	mlBasePoint[i] = BasePoint(p,n,u);
}			

void	Loft::ClearPathPoints	() { PROFILE
	mlPathPoint.clear();
}

void	Loft::AddPathPoint		(kVector3 p,kReal v,kReal uBias,kVector3 scale,kQuaternion rot) { PROFILE
	SetPathPoint(mlPathPoint.size(),p,v,uBias,scale,rot);
}

void	Loft::SetPathPoint		(size_t i,kVector3 p,kReal v,kReal uBias,kVector3 scale,kQuaternion rot) { PROFILE
	if (mlPathPoint.size() < i+1)
		mlPathPoint.resize(i+1);
	mlPathPoint[i] = PathPoint(p,v,uBias,scale,rot);
}
