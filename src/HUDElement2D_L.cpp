#if 0  // OBSOLETE FILE
#include "lugre_prefix.h"
#include "lugre_scripting.h"
#include "lugre_luabind.h"
#include "HUDElement2D.h"

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}


using namespace Lugre;

class cHUDElement2D_L : public cLuaBind<cHUDElement2D> { public:
		/// called by Register(), registers object-methods (see cLuaBind constructor for examples)
		virtual void RegisterMethods	(lua_State *L) { PROFILE
			// mlMethod.push_back((struct luaL_reg){"Meemba",		cHUDElement2D_L::Get});
			// lua_register(L,"MyGlobalFun",	MyGlobalFun);
			// lua_register(L,"MyStaticMethod",	&cSomeClass::MyStaticMethod);

			lua_register(L,"CreateHUDElement2D",	&cHUDElement2D_L::CreateHUDElement2D);

			#define REGISTER_METHOD(methodname) mlMethod.push_back(make_luaL_reg(#methodname,&cHUDElement2D_L::methodname));
			REGISTER_METHOD(Destroy);

			// synced with include/HUDElement2D.h
			cScripting* scripting = cScripting::GetSingletonPtr();
			#define RegisterClassConstant(name) scripting->SetGlobal(#name,cHUDElement2D::name)
			RegisterClassConstant(kPosMode_None);
			RegisterClassConstant(kPosMode_Object);
			RegisterClassConstant(kPosMode_Aim);
			RegisterClassConstant(kPosMode_ObjectDir);
			RegisterClassConstant(kTurnMode_None);
			RegisterClassConstant(kTurnMode_Constant);
			RegisterClassConstant(kTurnMode_ObjectDist);
			RegisterClassConstant(kTurnMode_ObjectDir);
			RegisterClassConstant(kTextMode_None);
			RegisterClassConstant(kTextMode_Dist);
			RegisterClassConstant(kTextMode_RelSpeed);
			RegisterClassConstant(kTextMode_FPS);
			RegisterClassConstant(kTextMode_AbsSpeed);
		}

		/// called by Register(), registers object-member-vars (see cLuaBind::RegisterMembers() for examples)
		virtual void RegisterMembers 	() { PROFILE
			cHUDElement2D* prototype = new cHUDElement2D(); // memory leak : never deleted, but better than side effekts
			cMemberVar_REGISTER(prototype,	kVarType_bool,			mbVisible,	kVarFlag_NotifyChange);
			cMemberVar_REGISTER(prototype,	kVarType_bool,			mbPosModeObject_UpdateSize,	kVarFlag_NotifyChange);
			cMemberVar_REGISTER(prototype,	kVarType_bool,			mbWatchMouse,	0);
			cMemberVar_REGISTER(prototype,	kVarType_bool,			mbMouseIsOver,	0);

			cMemberVar_REGISTER(prototype,	kVarType_size_t,		miUID,			kVarFlag_Readonly);
			cMemberVar_REGISTER(prototype,	kVarType_size_t,		miPosMode,		kVarFlag_NotifyChange);
			cMemberVar_REGISTER(prototype,	kVarType_size_t,		miTurnMode,		kVarFlag_NotifyChange);
			cMemberVar_REGISTER(prototype,	kVarType_size_t,		miTextMode,		kVarFlag_NotifyChange);

			cMemberVar_REGISTER(prototype,	kVarType_Vector2,		mvBase,			kVarFlag_NotifyChange);
			cMemberVar_REGISTER(prototype,	kVarType_Vector2,		mvParam,		kVarFlag_NotifyChange);
			cMemberVar_REGISTER(prototype,	kVarType_Vector2,		mvParam2,		kVarFlag_NotifyChange);
			cMemberVar_REGISTER(prototype,	kVarType_Vector2,		mvSizeParam,	kVarFlag_NotifyChange);

			cMemberVar_REGISTER(prototype,	kVarType_Real,			mfParam,		kVarFlag_NotifyChange);
			cMemberVar_REGISTER(prototype,	kVarType_Real,			mfCurRotate,	kVarFlag_NotifyChange);
			cMemberVar_REGISTER(prototype,	kVarType_Real,			mfTurnBase,		kVarFlag_NotifyChange);
			cMemberVar_REGISTER(prototype,	kVarType_Real,			mfTurnParam,	kVarFlag_NotifyChange);
			cMemberVar_REGISTER(prototype,	kVarType_Real,			mfTurnParam2,	kVarFlag_NotifyChange);
			cMemberVar_REGISTER(prototype,	kVarType_Real,			mfTurnMin,		kVarFlag_NotifyChange);
			cMemberVar_REGISTER(prototype,	kVarType_Real,			mfTurnMax,		kVarFlag_NotifyChange);

			cMemberVar_REGISTER(prototype,	kVarType_ObjSmartPtr,	mpObj1,			kVarFlag_NotifyChange);
			// cMemberVar_REGISTER(prototype,	kVarType_ObjSmartPtr,	mpObj2,		kVarFlag_NotifyChange);
			//cMemberVar_REGISTER(prototype,	kVarType_Colour,		mvColour,	kVarFlag_NotifyChange);
			cMemberVar_REGISTER(prototype,	kVarType_Gfx2D,			mpGfx2D,		kVarFlag_Readonly);
		}

		virtual void NotifyChange (cHUDElement2D* x,const char* sMemberVarName) { PROFILE
			assert(x);
			x->VarUpdate();
		}

	/// static methods exported to lua


		/// hudelem myhudelem = CreateHUDElement2D()
		/// call one of the Init* methods afterwards
		static int	CreateHUDElement2D		(lua_State *L) { PROFILE
			cHUDElement2D* target = new cHUDElement2D();
			target->Init();
			return CreateUData(L,target);
		}

		/// hudelem:Destroy()
		static int	Destroy			(lua_State *L) { PROFILE
			//printf("Destroy start\n");
			delete checkudata_alive(L);
			//printf("Destroy end\n");
			return 0;
		}
		
		virtual const char* GetLuaTypeName () { return "sfz.HUDElement2D"; }
};

/// lua binding
void	cHUDElement2D::LuaRegister 	(lua_State *L) { PROFILE
	cLuaBind<cHUDElement2D>::GetSingletonPtr(new cHUDElement2D_L())->LuaRegister(L);
}

#endif
