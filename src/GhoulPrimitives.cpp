#include "lugre_prefix.h"
#include <math.h>
//#include <cmath> ??
#include "GhoulPrimitives.h"
#include "Ogre.h"

using namespace GhoulPrimitive;
using namespace Lugre;


inline int max (const int a, const int b) { return (a>b)?a:b; }


// default implementation empty
Drawer::Drawer		() {}
Drawer::~Drawer		() {}
void	Drawer::Prepare		(eOpType opType,eGeometryChange gcHint,size_t vertexCount,size_t indexCount,bool hasNormals,bool hasTexCoords) {}
void	Drawer::AddVertex	(kVector3 p,kVector3 n,kReal u,kReal v) { AddVertex(p.x,p.y,p.z,n.x,n.y,n.z,u,v); }
void	Drawer::AddVertex	(kReal x,kReal y,kReal z,kReal nx,kReal ny,kReal nz,kReal u,kReal v) {}
void	Drawer::AddIndex	(IndexInt i) {}
void	Drawer::Finish		() {}

Primitive::Primitive		() {}
Primitive::~Primitive		() {}
void	Primitive::Update	(Drawer& drawer,bool bHasNormals,bool bHasTexCoords) {}


/** writes the coordinates for points on a 2D circle into an array
@remarks 			TODO : optimize me using sin,cos tables ?
@param a			The array to write to, must hold at least  blocks * components
@param blocks		number of blocks(e.g. vectors) in the array
@param components	each "block" consists of this many Reals, only the first 2 receive circle coords
@param startang		for partial circle, 0.0 results in the first point being on (0,1),...,(1,0)... (clockwise)
@param endang		for partial circle
@param setOtherComponentsToZero		initializes components after the second to zero
*/
void	Primitive::Ellipse	(Real* a,size_t blocks,size_t components,kReal radiusx,kReal radiusy,kReal startang,kReal endang,bool setOtherComponentsToZero) { PROFILE
	assert(blocks > 1 && "min one block");
	int i,j;
	if (components <= 0) return;
	Real ang,angstep=(endang-startang)/((Real)(blocks-1));
	for (i=0,ang=startang;i<blocks;++i,ang+=angstep) {
		if (i==blocks-1) ang = endang; // more exact end
		a[i*components+0] = radiusx * sin(ang);
		if (components > 1) {
			a[i*components+1] = radiusy * cos(ang);
			if (setOtherComponentsToZero) for (j=2;j<components;++j) a[i*components+j] = 0.0;
		}
	}
}

void	Primitive::Circle	(Real* a,size_t blocks,size_t components,kReal radius,kReal startang,kReal endang,bool setOtherComponentsToZero) { PROFILE
	Primitive::Ellipse(a,blocks,components,radius,radius,startang,endang,setOtherComponentsToZero);
}


/** n float interpolation (vectors,colors,..)
@param source		array with source values, must have at least space for "bufsize" Reals
@param dest			result is written here, must have at least space for "num" Reals
@param t			"time" for interpolation, [0.0;1.0]
@param num			number of Reals to synchronously interpolate, e.g. 3 for 3dvectors x,y,z
@param startoff		the offset (in Reals) of the first coordinate at t=0.0,  the entries this one are also considered for quadric mode, if they are inside the buffer
@param bufsize		the number of floats in the buffer
@param mode	see eInterpolationMode
@see eInterpolationMode
@param stride		number of "unused" (e.g. not interpolated) Reals after a "num" block, useful if quaternions etc are in the same buffer, and are interpolated differently
*/
void	Primitive::InterpolatateFloatV 	(kReal* source,Real* dest,kReal t,size_t num,size_t startoff,size_t bufsize,eInterpolationMode mode,size_t stride) { PROFILE
	// use enum, choose from buffer, extend dynamically, stride : this function interpolates floats, stride enables using it for buffers containing vectors...
	ASSERT(startoff >= 0 && "negative startoff");
	ASSERT(stride >= 0 && "negative stride");
	ASSERT(source && "source buffer null");
	ASSERT(dest && "dest buffer null");
	ASSERT(bufsize >= (num+stride) + startoff && "source buffer too small");  // source must at least have one complete block !
	int i,j;
	switch (mode) {
		case kInterpolate_Constant:
			for (i=0;i<num;++i) {
				dest[i] = source[startoff+i];
			}
		break;
		case kInterpolate_SmoothQuadric:
		{
			//assert(bufsize >= (num+stride)*3 + startoff && "source buffer too small");
			//assert(0 <= startoff - (num+stride) && "source buffer too small");
			Real t2 = t * t;
			Real t3 = t2 * t;
			Real t3m2 = t3 - t2;
			Real p1,p2,q1,q2,r1,r2;

			//Q(t) = P1*(2t^3-3t^2+1) + R1*(t^3-2t^2+t) + P2*(-2t^3+3t^2) + R2*(t^3-t^2)
			//Q(0) = P1*(1) + R1*(0) + P2*(0) + R2*(0)
			//Q(1) = P1*(0) + R1*(0) + P2*(1) + R2*(0)
			// hermit spline or something like that
			// R1,R2 are tangent-vectors at P1,P2

			for (i=0;i<num;++i) {
				// the order is  q1  p1  p2  q2   , interpolation between p1 and p2
				p1 = source[startoff+i];
				j = startoff+i + (num+stride);
				p2 = (j<bufsize)?source[j]:p1;
				j += (num+stride);
				q2 = (j<bufsize)?source[j]:p2;
				j = startoff+i - (num+stride);
				q1 = (j>=0)?source[j]:p1;

				r1 = (p2 - q1) * 0.5;
				r2 = (q2 - p1) * 0.5;
				dest[i] = (t<0.0)?p1:((t>1.0)?p2:( p1*(t3m2+t3m2-t2+1.0) + r1*(t3m2-t2+t) + p2*(-t3m2-t3m2+t2) + r2*(t3m2) ));
			}
		}
		break;
		case kInterpolate_Linear:
		default :
		{
			Real p1,p2;
			for (i=0;i<num;++i) {
				//assert(bufsize >= (num+stride)*2 + startoff && "source buffer too small");
				p1 = source[startoff+i];
				j = startoff+i + (num+stride);
				p2 = (j<bufsize)?source[j]:p1;
				dest[i] = (t<0.0)?p1:((t>1.0)?p2:(p1+(p2-p1)*t));
			}
		}
	}
}

Real	Primitive::InterpolatateFloat 	(kReal* source,kReal t,size_t startoff,size_t bufsize,eInterpolationMode mode,size_t stride) { PROFILE
	Real res;
	Primitive::InterpolatateFloatV(source,&res,t,1,startoff,bufsize,mode,stride);
	return res;
}


Real		InterpolatateFloat_Linear	(kReal p1,kReal p2,kReal t) { PROFILE
	Real source[] = { p1, p2 };
	return Primitive::InterpolatateFloat(source,t,0,2,kInterpolate_Linear,0);
}
Real		InterpolatateFloat_Smooth	(kReal q1,kReal p1,kReal p2,kReal q2,kReal t) { PROFILE
	Real source[] = { q1, p1, p2, q2 };
	return Primitive::InterpolatateFloat(source,t,1,4,kInterpolate_SmoothQuadric,0);
}


/// ellipsoid
Ellipsoid::~Ellipsoid() {}
Ellipsoid::Ellipsoid				(kVector3 pos,kVector3 rad,size_t rings,size_t segments) : Loft() { PROFILE
	SetParams(pos,rad,rings,segments);
}
Ellipsoid&	Ellipsoid::SetParams	(kVector3 pos,kVector3 rad,size_t rings,size_t segments) { PROFILE
	size_t i;
	Real t;
	Real *circle;
	circle = new Real[segments*2];
	Primitive::Ellipse(circle,segments,2,rad.x,rad.y);
	for (i=0;i<segments;++i)
		AddBasePoint(Vector3(circle[i*2+0],circle[i*2+1],0.0),Vector3::ZERO,(Real)i/(Real)(max(2,segments)-1));

	for (i=0;i<rings;++i) {
		t = (Real)i/(Real)(max(2,rings)-1);
		AddPathPoint(Vector3(pos.x,pos.y,pos.z-rad.z*cos(t*pi)),t,0,Vector3::UNIT_SCALE*sin(t*pi));
	}
	mSphericalAutoNormals = true;
	mSphericalAutoNormals_Center = pos;
	delete(circle);
	return *this;
}

OgreEllipsoid::~OgreEllipsoid() {}
OgreEllipsoid::OgreEllipsoid		(kVector3 pos,kVector3 rad,size_t rings,size_t segments)
	: Ellipsoid(pos,rad,rings,segments), OgreRenderableDrawer() { PROFILE
	Update(*this);
}



/// cone
Cone::~Cone() {}
Cone::Cone					(kVector3 pos1,kVector3 pos2,kReal rad1X,kReal rad1Y,kReal endscale,size_t segments,size_t height_segments) : Loft() { PROFILE
	SetParams(pos1,pos2,rad1X,rad1Y,endscale,segments,height_segments);
}
Cone&	Cone::SetParams		(kVector3 pos1,kVector3 pos2,kReal rad1X,kReal rad1Y,kReal endscale,size_t segments,size_t height_segments) { PROFILE
	size_t i;
	Vector3 v = pos2 - pos1;
	Vector3 vn = v.normalisedCopy();
	Vector3 x,y;
	if (fabs(vn.x) > fabs(vn.y))
			x = vn.crossProduct(Vector3::UNIT_Y); // x or z was max
	else	x = vn.crossProduct(Vector3::UNIT_X); // y or z was max
	y = vn.crossProduct(x);

	Real t;
	Real *circle;
	circle = new Real[segments*2];
	Primitive::Ellipse(circle,segments,2,rad1X,rad1Y);
	for (i=0;i<segments;++i) {
		AddBasePoint(x*circle[i*2+0] + y*circle[i*2+1],Vector3::ZERO,(Real)i/(Real)(max(2,segments)-1));
	}

	height_segments = max(2,height_segments);
	for (i=0;i<height_segments;++i) {
		t = (Real)i/(Real)(max(2,height_segments)-1);
		AddPathPoint(pos1+t*v,t,0,Vector3::UNIT_SCALE*(1.0 - t*(1.0-endscale)));
	}
	delete(circle);
	return *this;
}

OgreCone::~OgreCone() {}
OgreCone::OgreCone			(kVector3 pos1,kVector3 pos2,kReal rad1X,kReal rad1Y,kReal endscale,size_t segments,size_t height_segments)
	: Cone(pos1,pos2,rad1X,rad1Y,endscale,segments,height_segments), OgreRenderableDrawer() { PROFILE
	Update(*this);
}
