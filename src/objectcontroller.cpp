#include "lugre_prefix.h"
#include "object.h"
#include "objectcontroller.h"
#include <stdio.h>
#include <assert.h>

using namespace Lugre;

// targets
cObjectContollerTarget::~cObjectContollerTarget () { PROFILE }

Vector3 cObjectContollerTargetObject::GetPosition() { PROFILE
	return *mpApproachObject ? (*mpApproachObject)->mvPos : Vector3(0,0,0);
}
Vector3 cObjectContollerTargetObject::GetVelocity() { PROFILE
	return *mpApproachObject ? (*mpApproachObject)->mvVel : Vector3(0,0,0);
}

bool	cObjectContollerTargetObject::IsAlive () { PROFILE
	return (*mpApproachObject) != 0;
}

cObjectContollerTargetObject::cObjectContollerTargetObject (cObject* pObject) : mpApproachObject(pObject) { PROFILE }
cObjectContollerTargetObject::~cObjectContollerTargetObject (){ PROFILE }


Vector3 cObjectContollerTargetPosition::GetPosition () { PROFILE return mvPosition; }
bool cObjectContollerTargetPosition::IsAlive () { PROFILE return true; }
Vector3 cObjectContollerTargetPosition::GetVelocity () { PROFILE return Vector3(0,0,0); }

cObjectContollerTargetPosition::cObjectContollerTargetPosition (Vector3 vPosition) : mvPosition(vPosition) { PROFILE }
cObjectContollerTargetPosition::~cObjectContollerTargetPosition () { PROFILE }


cObjectController::cObjectController () : mpApproachTarget(0), miControlledObjectCount(0), mfMaxAccel(1), mfApproachMinDist(0), mbStupid(false) {}

cObjectController::~cObjectController () {
	assert(miControlledObjectCount == 0 && "WARNING ! destroyed cObjectController still in use");
	if (miControlledObjectCount != 0) printf("WARNING ! destroyed cObjectController still in use\n");
	if(mpApproachTarget) { delete mpApproachTarget; mpApproachTarget = 0; }
}

void	cObjectController::Lock		() { miControlledObjectCount++; }
void	cObjectController::Release	() { miControlledObjectCount--; assert(miControlledObjectCount >= 0); } 

void	cObjectController::SetTarget	(cObjectContollerTarget *pTarget) { PROFILE
	if(mpApproachTarget) delete mpApproachTarget;
	mpApproachTarget = pTarget;
}

void	cObjectController::Step	(cObject* pControlled) {
	using namespace Ogre;
	
	if (mpApproachTarget && mpApproachTarget->IsAlive()) {
		// approach autopilot
		
		Vector3 relpos = (pControlled->mvPos - mpApproachTarget->GetPosition());
		Vector3 relvel = (pControlled->mvVel - mpApproachTarget->GetVelocity());
		Vector3 dir = -relpos.normalisedCopy();
		Real relspeed = relvel.dotProduct(dir);
		Real reldist = relpos.length();
		
		// Real maxacc = (mvAccelParam-mvAccelParam2).length()*0.5;
		
		bool accel = true;
		if (reldist < mfApproachMinDist) accel = false;
		
		if (relspeed > 0.0) {
			Real breaktime = relspeed / mfMaxAccel;
			Real breakdist = relspeed * breaktime + mfMaxAccel * breaktime * breaktime;
			if (breakdist > reldist-mfApproachMinDist) accel = false; // hit the breaks
			//printf("apr=%d rd=%0.3f rs=%0.3f breaktime=%f breakdist=%f accell=%d\n",miID,reldist,relspeed,breaktime,breakdist,accel?1:0);	
		} else {
			//printf("apr=%d rd=%0.3f rs=%0.3f \n",miID,reldist,relspeed);	
		}
		
		// lotv prevents orbiting around the target if the current speed is orthogonal to the direction to the target
		Vector3 lotp = relspeed * dir;
		Vector3 lotv = (lotp - relvel).normalisedCopy();
		Real veldot = relvel.dotProduct(dir);
		if (veldot < 0.0) veldot = -veldot;
		
		if (accel || mbStupid) {
			//mvAccel = mfMaxAccel * dir;
			pControlled->mvAccel = mfMaxAccel * (( 1.0*dir + lotv).normalisedCopy());  // full speed ahead
		} else {
			//mvAccel = (-mfMaxAccel) * dir;
			pControlled->mvAccel = mfMaxAccel * ((-1.0*dir + lotv).normalisedCopy());  // hit the brakes !
		}
	}
	
	/*
	SLOWDOWN : 
		//accelerate inverse to current velocity
		mbNeedResync = true;
		Ogre::Vector3 v = -mvVel;
		v.normalise();
		v *= std::min(mfAccelParam1,mvVel.length());
		mvAccel = v;
		//set accel and speed to zero if is almost zero
		if(mvVel.isZeroLength()){
				mvVel = Ogre::Vector3::ZERO;
				mvAccel = Ogre::Vector3::ZERO;
		}
	*/
}


#if 0
// old code from object.cpp

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

/// server & client
void	cObject::Step		(Ogre::Real time,bool think) { PROFILE
	//#define NO_CLIENT_PRECALC	
	// server only : autopilot stuff
	if (miTrackingMode == kTrackMode_Object || miTrackingMode == kTrackMode_ObjectAdvAim) {
		cObject* obj = *mTrackObject;
		mvTrackParam = obj ? (obj->mvPos - mvPos) : Vector3::UNIT_Z;

		// TODO : use GetAbsolutePos or GetVectorTo or something when coordinate-hierarchy system is finished
		// TODO : this is unfair (turn is set directly, instead of turnaccel), rewrite me

		static Vector3		v;
		static Vector3		fwd(0,0,1.0);
		static Vector3		axis;
		static Radian		angle;
		static Quaternion	rot;
		
		v = mqRot.Inverse() * mvTrackParam; // TODO : getDerivedOrientation instead of mqRot directly
		v.normalise();
		mqTurn = fwd.getRotationTo(v);
		mqTurn.ToAngleAxis(angle,axis);
		angle = fmax(-mfMaxTurnAccel,fmin(mfMaxTurnAccel,angle.valueRadians() / fmax(0.0001,time) ));
		mqTurn = Quaternion(angle,axis);

		/*
		DIDN'T work :
		fwd = mqRot * Vector3::UNIT_Z;
		fwd.normalise();
		rot = fwd.getRotationTo(mvTrackParam);
		rot.ToAngleAxis(angle,axis);
		//angle = fmax(-mfMaxTurnAccel,fmin(mfMaxTurnAccel,angle.valueRadians() / fmax(0.0001,time) ));
		mqTurn = Quaternion(angle,axis);
		//mqRot = rot;
		//mqTurn = Quaternion::IDENTITY;
		*/
	}
}



#endif
