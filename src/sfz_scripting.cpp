#include "lugre_prefix.h"
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "lugre_scripting.h"
#include "lugre_luabind.h"

#include "object.h"
#include "location.h"
#include "resyncreceiver.h"
#include "objectcontroller.h"

using namespace Lugre;

static int l_GetDataDir			(lua_State *L) { PROFILE lua_pushstring(L,(DATA_DIR)	); return 1; }
static int l_GetLuaDir			(lua_State *L) { PROFILE lua_pushstring(L,(LUA_DIR)		); return 1; }
static int l_GetLugreDir		(lua_State *L) { PROFILE lua_pushstring(L,(LUGRE_DIR)	); return 1; }
static int l_UseHomeWritable	(lua_State *L) { PROFILE 
	#ifdef USE_HOME_WRITABLE
	lua_pushboolean(L,true); 
	#else
	lua_pushboolean(L,false); 
	#endif
	return 1; 
}

void	SFZ_RegisterLuaPlugin	() {
	
	class cSFZ_ScriptingPlugin : public cScriptingPlugin { public:
		void	RegisterLua_GlobalFunctions	(lua_State*	L) {
			lua_register(L,"GetDataDir",		l_GetDataDir); 
			lua_register(L,"GetLuaDir",			l_GetLuaDir); 
			lua_register(L,"GetLugreDir",		l_GetLugreDir); 
			lua_register(L,"UseHomeWritable",	l_UseHomeWritable); 
		}
		
		void	RegisterLua_Classes			(lua_State*	L) {
			cObject::LuaRegister(L);
			cLocation::LuaRegister(L);
			cObjectController::LuaRegister(L);
			cResyncReceiver::LuaRegister(L);
		}
	};
	
	cScripting::RegisterPlugin(new cSFZ_ScriptingPlugin());
}
