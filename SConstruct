# -*- Python -*-

sfz_sources = ''

env_cache_keys = ['CCFLAGS',
                  'CPPDEFINES',
                  'CPPFLAGS',
                  'CPPPATH',
                  'CXXFLAGS',
                  'LIBPATH',
                  'LIBS',
                  'LINKFLAGS']

#python dirlist : http://mail.python.org/pipermail/python-list/2003-March/196612.html
import dircache  
import os   
import sys   
import time
import math
import pickle
from build_support import *
from build_config import *

env = Environment()
# Do not put a .sconsign file into every directory
env.SConsignFile()

### create options ###
options = Options('options.cache')
options.Add(EnumOption('SOUND_SYSTEM', 'switch between openal and fmod', 'fmod', allowed_values=('openal', 'fmod')))

### build vars ###
preconfigured = False
force_configure = False
command_line_args = sys.argv[1:]
if 'configure' in command_line_args:
    force_configure = True
elif ('-h' in command_line_args) or ('--help' in command_line_args):
    preconfigured = True # this is just to ensure config gets skipped when help is requested

# config cache
if not force_configure:
    try:
        f = open('config.cache', 'r')
        up = pickle.Unpickler(f)
        pickled_values = up.load()
        for key, value in pickled_values.items():
            env[key] = value
        preconfigured = True
        if ('-h' not in command_line_args) and ('--help' not in command_line_args):
            print 'Using previous successful configuration; if you want to re-run the configuration step, run "scons configure".'
    except Exception:
        None

options.Update(env)

Help(options.GenerateHelpText(env))
options.Save('options.cache', env)

## helper
def CheckPkgConfig(context, version):
  context.Message( 'Checking for pkg-config... ' )
  ret = context.TryAction('pkg-config --atleast-pkgconfig-version=%s' % version)[0]
  context.Result( ret )
  return ret

def CheckPkg(context, name):
  context.Message( 'Checking for %s... ' % name )
  ret = context.TryAction('pkg-config --exists \'%s\'' % name)[0]
  context.Result( ret )
  return ret 
def CheckBoost(context, version):
    # Boost versions are in format major.minor.subminor
    v_arr = version.split(".")
    version_n = 0
    if len(v_arr) > 0:
        version_n += int(v_arr[0])*100000
    if len(v_arr) > 1:
        version_n += int(v_arr[1])*100
    if len(v_arr) > 2:
        version_n += int(v_arr[2])
        
    context.Message('Checking for Boost version >= %s... ' % (version))
    ret = context.TryRun("""
    #include <boost/version.hpp>

    int main() 
    {
        return BOOST_VERSION >= %d ? 0 : 1;
    }
    """ % version_n, '.cpp')[0]
    context.Result(ret)
    return ret

def roblistfiles(dirpath,ext,skipfile):
	ret = ''
	for f in dircache.listdir(dirpath):
		filepath = os.path.join(dirpath,f)
		if os.path.isfile(filepath) :
			if f != skipfile:
				(name, myext) = os.path.splitext(f)
				if myext == ext:
					ret += filepath + '\n'
	return ret 

# build environment 
libpath		= '/usr/lib /usr/local/lib'
includes	= '/usr/include /usr/local/include include src lugre/include lugre/src'
pkglibs		= 'OGRE OIS lua50 lualib50'
libs		= ''
cppdefines	= ''

# CPPDEFINES and other useful stuff for Environment : http://www.scons.org/doc/0.97/HTML/scons-user/a3414.html#CV-CCFLAGS 
#env = Environment(CCFLAGS = Split('-O3 -mtune=generic'), CPPDEFINES=Split(cppdefines), LIBPATH = Split(libpath), LIBS = Split(libs), CPPPATH = Split(includes)) 
# CPPDEFINES=[('B', 2), 'A']
# CPPDEFINES={'B':2, 'A':None}

# working dir override
cppdefines 		+= ' MAIN_WORKING_DIR=\\".\\" '
cppdefines 		+= ' LUA_DIR=\\"./lua\\" '
cppdefines 		+= ' LUGRE_DIR=\\"./lugre\\" '
cppdefines 		+= ' DATA_DIR=\\"./data\\" '
#~ cppdefines 		+= ' USE_HOME_WRITABLE'
#cppdefines 		+= ' MAIN_WORKING_DIR="some/where/" '

# mylugre local override
if os.path.exists("mylugre"):
	includes = includes.replace('lugre/','mylugre/')
	pkglibs = pkglibs.replace('lugre/','mylugre/')
	

# sound system differences
CCFLAGS_sound = ''  # used by lugre_sound.cpp
print 'SOUND_SYSTEM = ' + env['SOUND_SYSTEM']
if (env['SOUND_SYSTEM'] == 'fmod'):
	skipfile 		= 'lugre_sound_openal.cpp'
	cppdefines 		+= ' USE_FMOD'
	includes 		+= ' /usr/local/include/fmodex'
	libs 			+= ' fmodex'
else:
	skipfile 		= 'lugre_sound_fmod.cpp'
	cppdefines 		+= ' USE_OPENAL'
	pkglibs 		+= ' openal freealut vorbis vorbisfile'

# activate thread support
cppdefines 		+= ' ENABLE_THREADS'
libs				+= ' boost_thread-mt'
#~ libs				+= ' boost_thread'

# activate ode support, see also CheckODE in build_support.py , and CheckODE and ode_config below
cppdefines 		+= ' ENABLE_ODE'

# search for sourcefiles to compile
sfz_sources += roblistfiles('src/','.cpp',skipfile)
if os.path.exists('mylugre/'):
	sfz_sources += roblistfiles('mylugre/src/','.cpp',skipfile)
else:
	sfz_sources += roblistfiles('lugre/src/','.cpp',skipfile)

## see also https://freeorion.svn.sourceforge.net/svnroot/freeorion/trunk/FreeOrion/SConstruct
## check configuration
if not env.GetOption('clean'):
	if not preconfigured:
		conf = env.Configure(custom_tests = {
			'CheckPkgConfig' : CheckPkgConfig,
			'CheckPkg' : CheckPkg,
			'CheckVersionHeader' : CheckVersionHeader,
			'CheckODE' : CheckODE,
			'CheckConfigSuccess' : CheckConfigSuccess,
			'CheckBoost' : CheckBoost
			})

		if not conf.CheckPkgConfig('0.15.0'):
			print 'pkg-config >= 0.15.0 not found.'
			Exit(1)
		
		for x in Split(pkglibs): 
			if not conf.CheckPkg(x):
				print x,'not found.'
				Exit(1)
		
		for x in Split(libs):
			if x != "boost_thread-mt":
				if not conf.CheckLib(x):
					print "Couldn't find " + x + " library. Exiting."
					Exit(1) 
		
		for x in Split(pkglibs): 
			env.ParseConfig('pkg-config --cflags --libs ' + x) 
		
		if not (conf.CheckBoost('1.33')):
			print 'Boost version >= 1.33 needed'
			Exit(1)
		
		ode_config = WhereIs('ode-config')
		if not conf.CheckODE(options, conf, ode_config):
			Exit(1)	
		
		conf.Finish();
		f = open('config.cache', 'w')
		p = pickle.Pickler(f)
		cache_dict = {}
		for i in env_cache_keys:
			cache_dict[i] = env.has_key(i) and env.Dictionary(i) or []
		p.dump(cache_dict)
		if 'configure' in command_line_args:
			Exit(0)

# OPENAL_TONE_TEST = 'false'
# if a file with this name exists, then activate openal

# store the current buildtime in the version file
if os.path.exists("VERSION_WRITE_ENABLE"):
	ts = int(math.floor(time.time()))
	print "writing build timestamp: " + `ts`
	f = open("data/VERSION","w")
	f.write(`ts` + "\n")
	f.close()


# if OPENAL_TONE_TEST == 'true':
#	sfz_sources = 'src/openal_tonetest.cpp'


# registering build environment
env.AppendUnique(CCFLAGS = Split('-g'))
env.AppendUnique(CPPDEFINES = Split(cppdefines))
env.AppendUnique(LIBPATH = Split(libpath))
env.AppendUnique(LIBS = Split(libs))
env.AppendUnique(CPPPATH = Split(includes))

options.Update(env)

TargetSignatures('content')
#SetOption('implicit_cache', 1)

env.Program('sfz',Split(sfz_sources))

