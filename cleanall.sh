#!/bin/sh

rm -f configure
rm -f Makefile.in
rm -f config.log
rm -f depcomp
rm -f config.guess
rm -f config.sub
rm -f ltmain.sh
rm -f AutoMakefile
rm -f mkinstalldirs
rm -f config.status
rm -rf autom4te.cache
rm -f libtool
rm -f missing
rm -f CEGUI.log
rm -f Ogre.log
rm -f aclocal.m4
rm -f install-sh
rm -f include/Makefile.in
rm -f include/stamp-h1
#rm -f include/config.h.in
rm -f include/Makefile
rm -f include/config.h
rm -f src/Makefile.in
rm -f src/*.o
rm -rf src/.libs
rm -rf src/.deps
rm -f src/Makefile
rm -f olm

#sh bootstrap && sh configure && make
