-- central gui code
-- see also lib.guimaker.lua

gMouseCorrectionX = 5
gMouseCorrectionY = 5

-- returns x,y
function GetMousePos ()
	local mx,my = PollInput()
	return mx+gMouseCorrectionX,my+gMouseCorrectionY
end

function GetMouseRay() 
	local x,y = GetMousePos()
	local vw,vh = GetViewportSize()
	return GetScreenRay(x/vw,y/vh) 
end

gWidgetList = {} -- for finding widget via uid
gDialogList = {} -- for finding dialog via uid
gLastMouseDownWidget = nil
gWidgetUnderMouse = nil
gbWidgetLeave_Since_LastMouseDown = false

function GUIStep ()
	-- detect mouse enter and mouse leave
	local widget = GetWidgetUnderMouse()
	if (gWidgetUnderMouse ~= widget) then
		if (widget ~= gLastMouseDownWidget) then gbWidgetLeave_Since_LastMouseDown = true end
		if (gWidgetUnderMouse) then GUIEvent("mouse_leave",gWidgetUnderMouse) end
		gWidgetUnderMouse = widget
		if (gWidgetUnderMouse) then GUIEvent("mouse_enter",gWidgetUnderMouse) end
	end
end

function GUI_TriggerWidgetCallback (widget,callback_name,dialogparam,...)
	local dialog = widget.dialog
	local bConsumed = false -- the dialog callback is not called if the widget callback returns true
	if (widget[callback_name]) then bConsumed = widget[callback_name](widget,unpack(arg)) end
	if ((not bConsumed) and dialog:IsAlive() and dialog[callback_name]) then dialog[callback_name](dialogparam,unpack(arg)) end
end

function GUI_TriggerWidgetCallback_BackwardComp (widget,callback_name,dialogparam,...)
	local dialog = widget.dialog
	local bConsumed = false -- the dialog callback is not called if the widget callback returns true
	if (widget[callback_name]) then 
		widget[callback_name](widget,unpack(arg)) 
	else 
		if (dialog:IsAlive() and dialog[callback_name]) then dialog[callback_name](dialogparam,unpack(arg)) end
	end
end

-- if the parameter widget is nil, the gLastMouseDownWidget is used
-- called from lib.input.lua, eventname is one of 
-- mouse_left_down,mouse_left_up
-- mouse_right_down,mouse_right_up
-- mouse_left_click,mouse_left_click_double,mouse_left_click_single,
-- mouse_left_drag_start,mouse_left_drag_step,mouse_left_drag_stop
-- called from GUIStep, eventname is one of
-- mouse_enter,mouse_leave
-- generates button_click event from mouse_left_click when the mouse is still inside gLastMouseDownWidget
function GUIEvent (sEventName,widget)
	local callback_name = "on_"..sEventName  -- e.g. widget.on_mouse_left_down
	if (sEventName == "mouse_left_down" or 
		sEventName == "mouse_right_down" or 
		sEventName == "mouse_middle_down") then 
		gLastMouseDownWidget = GetWidgetUnderMouse() 
		gbWidgetLeave_Since_LastMouseDown = false 
	end
	widget = widget or gLastMouseDownWidget
	if (sEventName == "mouse_right_up") then widget = GetWidgetUnderMouse() end -- for context/right-click-menu
	
	--print("GUIEvent",callback_name,widget)
	
	
	if (widget and widget:IsAlive()) then
		GUI_TriggerWidgetCallback(widget,callback_name,widget.dialog)
		
		-- backwards compatibility for iris
		if (true) then 
			local mousebutton = nil
			if (sEventName == "mouse_left_down") then mousebutton = 1 end
			if (sEventName == "mouse_right_down") then mousebutton = 2 end
			if (sEventName == "mouse_middle_down") then mousebutton = 3 end
			if (mousebutton) then GUI_TriggerWidgetCallback_BackwardComp(widget,"onMouseDown",widget,mousebutton) end
			
			local mousebutton = nil
			if (sEventName == "mouse_left_up") then mousebutton = 1 end
			if (sEventName == "mouse_right_up") then mousebutton = 2 end
			if (sEventName == "mouse_middle_up") then mousebutton = 3 end
			if (mousebutton) then GUI_TriggerWidgetCallback_BackwardComp(widget,"onMouseUp",widget,mousebutton) end
		end
	end
	
	if (sEventName == "mouse_left_click") then
		if (gLastMouseDownWidget == GetWidgetUnderMouse()) then
			GUIEvent("button_click") -- special event for gui stuff, only triggered if mouse is release on the same widget on which it was pressed
			if (gLastMouseDownWidget) then
				GUI_TriggerWidgetCallback_BackwardComp(gLastMouseDownWidget,"onLeftClick",gLastMouseDownWidget) 
			end
		end
	end
end


-- returns true if the gui consumes all input events (ie. to skip keypressed state input handling)
function GuiConsumesInput()
	-- TODO probably remove the dependancy to lib.edittext.lua?
	if not gActiveEditText then
		return false
	else
		return true
	end
end

-- returns true if the event was consumed/handled
function GuiKeyDown(key,char)
	local bConsumed = EditTextKeyDown(key,char)
	-- todo : stuff like return to trigger default button in modal dialog, tab to change cycle input elements (edit-texts)
	return bConsumed
end

function GetLastMouseDownWidget()
	return gLastMouseDownWidget
end

function GetWidgetUnderMouse () 
	local mx,my = GetMousePos()
	local id = GetWidgetUnderPos(mx,my)
	return id and gWidgetList[id] 
end

function GetDialogUnderMouse () 
	local widget = GetWidgetUnderMouse()
	return widget and widget.dialog
end
