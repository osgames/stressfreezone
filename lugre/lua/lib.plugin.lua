-- handles plugin system

-- lists all lua files in pluginDir and executes them
function LoadPlugins (pluginDir)
	local arr_files = dirlist(pluginDir,false,true)
	local sortedfilenames = {}
	for k,filename in pairs(arr_files) do table.insert(sortedfilenames,filename) end
	table.sort(sortedfilenames)
	
	for k,filename in pairs(sortedfilenames) do if fileextension(filename) == "lua" then
		local path = pluginDir..filename
		print("loading plugin ",path)
		dofile(path)
	end end
end

