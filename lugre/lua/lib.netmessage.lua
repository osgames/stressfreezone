-- defines message types used for networking

gSendFifo = CreateFIFO()
gUDPRecvFifo = CreateFIFO()
giNextNetMessageTypeID = 1
gNetMessageParamFormat = {}
gNetMessageTypeName = {}
gNetMessageFormatWrapper_ByCustom = {}

kNetFirstByte = hex2num("0x85") -- 7334 for GS, don't ask

function RegisterNetMessageType (name,paramformat,forcedid)
	if (giNextNetMessageTypeID == kNetFirstByte) then giNextNetMessageTypeID = giNextNetMessageTypeID + 1 end
	local msgtype = forcedid or giNextNetMessageTypeID
	if (not forcedid) then giNextNetMessageTypeID = giNextNetMessageTypeID + 1 end
	assert(not _G[name],"RegisterNetMessageType : type with this name already known !")
	_G[name] = msgtype
	gNetMessageTypeName[msgtype] = name
	gNetMessageParamFormat[msgtype] = paramformat
end

-- use FALSE instead of NIL here, nil in the middle confuses the variable argument syntax
function RegisterNetMessageFormatWrapper (letter_custom,letter_real,custom2real,real2custom)
	gNetMessageFormatWrapper_ByCustom[	letter_custom] = {	letter_custom	=letter_custom,
															letter_real		=letter_real,
															custom2real		=custom2real,
															real2custom		=real2custom }
end

--[[
unused so far...
kNetDataTypes = {}
kNetDataTypes["b"] = 1	-- b byte(uint8) 
kNetDataTypes["i"] = 2	-- i int(uint32) 
kNetDataTypes["f"] = 3	-- f float(4 byte) 
kNetDataTypes["s"] = 4	-- s string(len_uint32,text_ascii) 
kNetDataTypes["#"] = 5	-- # fifo(len_uint32,data_raw) 
kNetDataTypes["_"] = 6	-- _ variable argument count, each prefixed with 1 byte type
]]--

-- formattet pop
-- paramformat is something like "iiifffffff"
-- extracts data from fifo, and returns it as an array
--  b byte(uint8) 
--  i int(uint32) 
--  f float(4 byte) 
--  s string(len_uint32,text_ascii) 
function FPop (fifo,paramformat) 
	local res = {}
		
	--print("FPop",paramformat,fifo:Size()) 
	for c in string.gfind(paramformat,".") do
		
		local wrapper = gNetMessageFormatWrapper_ByCustom[c]
		local realc = wrapper and wrapper.letter_real or c
		local resultpart
		
			if (realc == "b") then resultpart = fifo:PopNetUint8() 	
		elseif (realc == "i") then resultpart = fifo:PopNetUint32()
		elseif (realc == "f") then resultpart = fifo:PopF()
		elseif (realc == "s") then resultpart = fifo:PopS()	 	
		elseif (realc == "#") then  -- fifo : datalen,data
			local datalen = fifo:PopNetUint32()
			resultpart = CreateFIFO()
			fifo:PopFIFO(resultpart,datalen)
		elseif (realc == ".") then -- variable argument count : datalen,fmtlen,fmtstring,data
			local datalen = fifo:PopNetUint32()
			local fmt = fifo:PopS()
			local data = CreateFIFO()
			fifo:PopFIFO(data,datalen)
			local varargs = {FPop(data,fmt)} -- only floats and strings
			data:Destroy()
			for k,v in pairs(varargs) do table.insert(res,v) end
		else assert(false,"illegal paramformat") end
		
		if (resultpart) then 
			if (wrapper) then resultpart = wrapper.real2custom(resultpart) end
			table.insert(res,resultpart) 
		end
		
	end
	return unpack(res)
end

-- see also FPop
function FPush (fifo,paramformat,...) 
	local i = 1
	for c in string.gfind(paramformat,".") do
		local x = arg[i]
		
		local wrapper = gNetMessageFormatWrapper_ByCustom[c]
		if (wrapper) then
			c = wrapper.letter_real
			x = wrapper.custom2real(x)
		end
		
		if (c ~= ".") then assert(x,"not enough params for format "..paramformat.." missing=#"..i) end
			if (c == "b") then assert(type(x) == "number","byte :number expected")	fifo:PushNetUint8(x)	
		elseif (c == "i") then assert(type(x) == "number","int  :number expected")	fifo:PushNetUint32(x)	
		elseif (c == "f") then assert(type(x) == "number","float:number expected")	fifo:PushF(x)
		elseif (c == "s") then assert(type(x) == "string","string expected")		fifo:PushS(x)	
		elseif (c == "#") then assert(type(x) == "table","fifo expected")			fifo:PushNetUint32(x:Size()) fifo:PushFIFO(x)
		elseif (c == ".") then 
			local fmt = ""
			local data = CreateFIFO()
			while (x) do
					if (type(x) == "number") then fmt = fmt.."f" data:PushF(x) 
				elseif (type(x) == "string") then fmt = fmt.."s" data:PushS(x)	
				else assert(false,"illegal var-arg-paramformat") end
				i = i + 1
				x = arg[i]
			end
			fifo:PushNetUint32(data:Size()) 
			fifo:PushS(fmt) 
			fifo:PushFIFO(data)
		else assert(false,"illegal paramformat") end
		i = i + 1
	end
end

-- WARNING ! returns nil for variable message length, e.g. string..., see IsNetMessageComplete for those
function CalcNetMessageParamLength (paramformat) 
	local res = 0
	for c in string.gfind(paramformat,".") do
		local wrapper = gNetMessageFormatWrapper_ByCustom[c]
		if (wrapper) then c = wrapper.letter_real end
			if (c == "b") then res = res + 1
		elseif (c == "i") then res = res + 4
		elseif (c == "f") then res = res + 4
		elseif (c == "s") then return nil -- variable length not calculatable without peek
		elseif (c == "#") then return nil -- variable length not calculatable without peek
		elseif (c == ".") then return nil -- variable length not calculatable without peek
		else assert(false,"illegal paramformat") end
	end
	return res
end

-- true if string or vararg or other data with not fixed length is contained
function IsNetMessageVariableLength (paramformat) return CalcNetMessageParamLength(paramformat) == nil end

function IsNetMessageComplete (fifo,paramformat,startoffset) 
	local len = CalcNetMessageParamLength(paramformat)  -- only works for fixed length messages
	if (len) then return fifo:Size() - startoffset >= len end
	
	-- variable length message
	local fifosize = fifo:Size()
	local curpos = startoffset
	local partsize
	for c in string.gfind(paramformat,".") do
		local wrapper = gNetMessageFormatWrapper_ByCustom[c]
		if (wrapper) then c = wrapper.letter_real end
		--print(" IsNetMessageComplete c=",c,"curpos=",curpos)
			if (c == "b") then partsize = 1
		elseif (c == "i") then partsize = 4
		elseif (c == "f") then partsize = 4
		elseif (c == "s") then partsize = 4 
			if (partsize > fifosize-curpos) then return false end
			--print(" stringlen=",fifo:PeekNetUint32(curpos),"atpos",curpos)
			partsize = partsize + fifo:PeekNetUint32(curpos) -- peek stringlength
		elseif (c == "#") then partsize = 4 
			if (partsize > fifosize-curpos) then return false end
			--print(" stringlen=",fifo:PeekNetUint32(curpos),"atpos",curpos)
			partsize = partsize + fifo:PeekNetUint32(curpos) -- peek fifolength
		elseif (c == ".") then partsize = 8 
			if (partsize > fifosize-curpos) then return false end
			partsize = partsize + fifo:PeekNetUint32(curpos) + fifo:PeekNetUint32(curpos+4) -- peek fifolength + fmt-stringlen
		else assert(false,"illegal paramformat") end
		if (partsize > fifosize-curpos) then return false end
		curpos = curpos + partsize
	end
	return true
end

-- msgtype is an id like kNetMessage_Chat
function SendNetMessage (con,msgtype,...)
	if not con then return end
	gSendFifo:Clear()
	gSendFifo:PushUint8(msgtype)
	FPush(gSendFifo,gNetMessageParamFormat[msgtype],unpack(arg))
	--gSendFifo:HexDump()
	con:Push(gSendFifo)
end


-- pops all complete messages from the fifo and calls callback for each of them
-- fifo should be individual for this connection, in case half messages arrive
-- usually mycon:Pop(fifo) is called right before this
-- callback(msgtype,args...)
function RecvNetMessages (fifo,callback)
	while (fifo:Size() > 0) do
		local msgtype = fifo:PeekNetUint8(0)
		local paramformat = gNetMessageParamFormat[msgtype]
		local packetinfo = sprintf("RecvNetMessages %s[%d] format=%s fifosize=%d",gNetMessageTypeName[msgtype] or "unknown",msgtype,paramformat or "?",fifo:Size())
		if (not paramformat) then FatalErrorMessage("unknown netmessagetype "..packetinfo) end
		
		-- check if complete, and activate callback if it is
		if (IsNetMessageComplete(fifo,paramformat,1)) then
			fifo:PopRaw(1)
			local params = {FPop(fifo,paramformat)}
			-- log
			if (not in_array(msgtype,gNoLogNetMessages)) then
				printdebug("net",packetinfo.." : "..arrdump(params))
				--fifo:HexDump()
			end
			local success,errormsg = lugrepcall(callback,msgtype,unpack(params))
			if (not success) then NotifyListener("lugre_error","error in RecvNetMessages",packetinfo,"\n",errormsg) end
		else
			--print("incomplete : ",packetinfo)
			break
		end
	end
end

