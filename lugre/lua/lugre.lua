gMyFrameCounter = 0

function lugre_detect_ogre_plugin_path () 
	if (gOgrePluginPath) then return gOgrePluginPath end -- override via config
	if (WIN32) then return end -- autodetect(and popen) not possible on win, but libs are in working dir there
	local p = io.popen("pkg-config --variable=plugindir OGRE")
	local res = p and p:read() -- read line
	if (p) then p:close() end
	return res
end

function lugre_include_libs (basepath)
	local libpath = basepath
		
		-- test
	dofile(libpath .. "lib.util.lua")
	dofile(libpath .. "lib.listener.lua")
	dofile(libpath .. "lib.contextmenu.lua")
	dofile(libpath .. "lib.cursor.lua")
	dofile(libpath .. "lib.tooltip.lua")
	dofile(libpath .. "lib.sound.lua")
	dofile(libpath .. "lib.box.lua")
	dofile(libpath .. "lib.prism.lua")
	dofile(libpath .. "lib.primitive.lua")
	dofile(libpath .. "lib.voxel.lua")
	dofile(libpath .. "lib.plugin.lua")
	dofile(libpath .. "lib.cam.lua")
	dofile(libpath .. "lib.beam.lua")
	dofile(libpath .. "lib.material.lua")
	dofile(libpath .. "lib.types.lua")
	dofile(libpath .. "lib.time.lua")
	dofile(libpath .. "lib.input.lua")
	dofile(libpath .. "lib.netmessage.lua")
	dofile(libpath .. "lib.gui.lua")
	dofile(libpath .. "lib.guiutils.lua")
	dofile(libpath .. "lib.guimaker.lua")
	dofile(libpath .. "lib.fadelines.lua")
	dofile(libpath .. "lib.edittext.lua")
	dofile(libpath .. "lib.chatline.lua")
	dofile(libpath .. "lib.plaingui.lua")
	dofile(libpath .. "lib.movedialog.lua")
	dofile(libpath .. "lib.preview.lua")
	dofile(libpath .. "lib.http.lua")
	dofile(libpath .. "lib.irc.lua")
	dofile(libpath .. "lib.filebrowser.lua")
	dofile(libpath .. "lib.thread.lua")
	dofile(libpath .. "lib.ode.lua")
	--dofile(libpath .. "lib.xml.lua")
	--dofile(libpath .. "lib.net.lua")
	--dofile(libpath .. "lib.mousepick.lua")

	RegisterListener("lugre_error",function (...) print("lugre_error",unpack(arg)) end)
	
	-- register pixel format constants, like PF_FLOAT16_R
	if (OgrePixelFormatList) then
		local mylist = OgrePixelFormatList()
		for name,id in pairs(mylist) do _G[name] = id end
	end
	
end

-- test if a name is following the convention for global variables and constants (e.g. giMyInt or gSomething or kBlub)
function LugreIsGlobalVarName (name) return string.match(name,"^[gk][iblsfv]*[A-Z]") ~= nil end

function LugreActivateGlobalVarChecking ()
	-- install metatable to enforce naming convention gUppercaseletter for global vars
	setmetatable(_G,{
		__newindex=function (t,k,v) 
			if ((type(v) ~= "function") and (not LugreIsGlobalVarName(k))) then 
				print("warning, illegal global var naming",k,v,_TRACEBACK()) 
			end 
			rawset(t, k, v)
			end
		} )
end

-- call this in your mainloop
function LugreStep ()
	SoundStep()
	local curticks = Client_GetTicks()
	gSecondsSinceLastFrame = (curticks - gMyTicks) / 1000.0
	gMyFrameCounter = gMyFrameCounter + 1
	gMyTicks = curticks
	--UpdateFPS()
	UpdateStats()
	StepFadeLines()
	gui.StepMoveDialog()
	NotifyListener("LugreStep")
end
