-- obsolete, or at least unused
-- voxel-like mesh analysis, used for collision/interesection detection in shipeditor
-- fine grids generated from raw geometry
-- superseeded by shipvoxelgrid.lua

-- TODO : 2d voxel : 1,3, 1,5, 3,1, 3,7, 5,1, 5,7, 7,3, 7,5,  *e , e=1/8  -- a circle of points that is not hit by lines on a 1/2 grid

-- returns 3 dimensional array, zero based,  [xi][yi][zi] = 0 means outside, = 1 means inside
-- gfx must start at 0,0,0 and not go into negative coords
-- WARNING ! only works on simple (convex) forms
function CalcVoxelGrid (gfx,cx,cy,cz,gridsize)
	local g = gridsize or 1/4 -- gridsize
	local m = math.max(cx,cy,cz)
	local voxelgrid = {}
	for xi = 0,cx/g-1 do
		voxelgrid[xi] = {}
		for yi = 0,cy/g-1 do
			voxelgrid[xi][yi] = {}
			for zi = 0,cz/g-1 do
				-- ray-escape test along all axes
				-- if there one ray that does not hit any poly, the point must be outside
				local x,y,z = g/2 + g*xi,g/2 + g*yi,g/2 + g*zi
				voxelgrid[xi][yi][zi] = (	(not gfx:RayPick(x,y,z, m,0,0)) or 
											(not gfx:RayPick(x,y,z,-m,0,0)) or
											(not gfx:RayPick(x,y,z,0, m,0)) or
											(not gfx:RayPick(x,y,z,0,-m,0)) or
											(not gfx:RayPick(x,y,z,0,0, m)) or
											(not gfx:RayPick(x,y,z,0,0,-m)))  and  0  or   1
			end
		end
	end
	return voxelgrid
end

-- calls fun(voxelstate,x,y,z) with every voxel
-- the process is aborted and returns the first value of what fun returns if it is something other than nil
function ForEachVoxel (voxelgrid,fun)
	for xi,yarr in pairs(voxelgrid) do 
		for yi,zarr in pairs(yarr) do 
			for zi,state in pairs(zarr) do
				local res = fun(state,xi,yi,zi)
				if (res ~= nil) then return res end
			end
		end
	end
end

-- returns a new voxel grid
-- sx,sy,sz should be integers (e.g. mirrors, or scale by two)
-- qw,qx,qy,qz should be an orthogonal rotation (e.g. 90 degrees to the right,...)
function TransformVoxelGrid (voxelgrid, px,py,pz, qw,qx,qy,qz, sx,sy,sz, gridsize)
	local g = gridsize or 1/4 -- gridsize
	local res = {}
	ForEachVoxel(voxelgrid,function (state,x,y,z) 
			x,y,z = x*sx,y*sy,z*sz
			x,y,z = Quaternion.ApplyToVector(x,y,z,qw,qx,qy,qz) 
			x,y,z = px/g + x,py/g + y,pz/g + z
			res[x] = res[x] or {}
			res[x][y] = res[x][y] or {}
			res[x][y][z] = state
		end)
	return res	
end

-- compares two voxel grids and returns true if they intersect
function VoxelGridIntersection (voxelgrid1,voxelgrid2)
	return ForEachVoxel(voxelgrid1,function (state,x,y,z) 
		if (state == 1 and voxelgrid2[x] and voxelgrid2[x][y] and voxelgrid2[x][y][z] == 1) then return true end
	end)
end 

-- creates and returns returns array of (green) billboard gfx for every voxel "inside"
function DrawVoxelGrid (voxelgrid,parentgfx,gridsize)
	local g = gridsize or 1/4 -- gridsize
	local res = {}
	ForEachVoxel(voxelgrid,function (state,xi,yi,zi) 
		if (state == 1) then
			local gfx = parentgfx and parentgfx:CreateChild() or CreateRootGfx3D()
			local x,y,z = xi*g+g/2,yi*g+g/2,zi*g+g/2
			gfx:SetBillboard(x,y,z,g/4,GetPlainColourMat(0,1,0)) 
			table.insert(res,gfx)
		end
		end)
	return res
end


--[[
OBSOLETE, did not work correctly, to many rounding errors on RayPickList
-- ray must be padded with gridsize at the start and at the end to avoid rounding errors
-- returns voxel-cells along ray, arr[zi] (where 0 <= zi < numcells) (2 means border, 0 means outside, 1 means inside)
function GfxVoxelLine (gfx, gridsize,numcells, rx,ry,rz,rvx,rvy,rvz)
	local raylen = Vector.len(rvx,rvy,rvz)
	local hits = gfx:RayPickList(rx,ry,rz,rvx,rvy,rvz) -- table{facenum=dist,...}
	
	-- round hitlist to cells
	local cellhits = {}
	local mincell = 0
	local maxcell = numcells-1
	for facenum,dist in pairs(hits) do 
		local cell = math.floor((dist*raylen - gridsize) / gridsize)
		cellhits[cell] = (cellhits[cell] or 0) + 1
		if (cell < mincell) then mincell = cell end
		if (cell > maxcell) then maxcell = cell end
	end
	
	-- determine inside and outside
	local res = {}
	local outside = true
	for i = mincell,maxcell do
		local border = (cellhits[i] or 0) ~= 0
		if (math.mod(cellhits[i] or 0,2) == 1) then outside = (not outside) end
		--print("cell",i,border,outside)
		if (i >= 0 and i < numcells) then res[i] = border and 2 or (outside and 0 or 1) end
	end
	
	return res
end
]]--
