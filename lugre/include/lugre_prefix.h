/*
http://www.opensource.org/licenses/mit-license.php  (MIT-License)

Copyright (c) 2007 Lugre-Team

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef LUGRE_PREFIX_H
#define LUGRE_PREFIX_H

#include <assert.h>
#define ASSERT(x) assert(x)
#include "lugre_profile.h"

#ifndef WIN32
	#include <stdint.h>
#endif

namespace Lugre {
	
int mystricmp (const char *str1, const char *str2); // defined in shell.cpp

template<typename T1, typename T2> inline T1 myabs(T1 a){return (a>0?a:-a);}
template<typename T1, typename T2> inline T1 mymax(T1 a,T2 b){return (a<b?b:a);}
template<typename T1, typename T2> inline T1 mymin(T1 a,T2 b){return (a>b?b:a);}
template<typename T1> inline T1 mysquare(T1 a){return a*a;}

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

#ifndef MAIN_WORKING_DIR
	/// MAIN_WORKING_DIR is usually set as compiler option , e.g. -DMAIN_WORKING_DIR=some/where/
	//#pragma warning("warning : MAIN_WORKING_DIR not defined as compiler-option, defaulting to \"\"")
    #define MAIN_WORKING_DIR "."
    #define MWD "."
#else
    #define MWD ((MAIN_WORKING_DIR))
#endif
inline const char* GetMainWorkingDir () { return MWD; }
/// needs lugre_robstring.h
#define LUGRE_PATH(filename) strprintf("%s/%s",GetMainWorkingDir(),filename).c_str() 

#ifdef WIN32
#ifndef snprintf
#ifndef MINGW
	int snprintf (char *str, int n, char *fmt, ...);
	#define DEFINE_SNPRINTF
#endif
#endif
#endif


#ifdef WIN32
	typedef unsigned long	uint32; 
	typedef unsigned short	uint16; 
	typedef unsigned char	uint8; 
	typedef long			int32; 
	typedef short			int16; 
	typedef char			int8; 
#else
	typedef uint32_t	uint32;
	typedef uint16_t	uint16;
	typedef uint8_t	uint8;
	typedef int32_t	int32;
	typedef int16_t	int16;
	typedef int8_t	int8;
#endif

/// defined in scripting.cpp
void	printdebug	(const char *szCategory, const char *szFormat = "", ...);

}

#endif
