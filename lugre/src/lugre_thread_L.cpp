#include "lugre_prefix.h"
#include "lugre_thread.h"
#include "lugre_fifo.h"
#include "lugre_luabind.h"

namespace Lugre {
	
class cThread_NetRequest_L : public cLuaBind<cThread_NetRequest> { public:
	// implementation of cLuaBind

		/// called by Register(), registers object-methods (see cLuaBind constructor for examples)
		virtual void RegisterMethods	(lua_State *L) { PROFILE
			#define REGISTER_METHOD(methodname) mlMethod.push_back(make_luaL_reg(#methodname,&cThread_NetRequest_L::methodname));

			REGISTER_METHOD(Destroy);
			REGISTER_METHOD(IsFinished);
			REGISTER_METHOD(HasError);
			
			#undef REGISTER_METHOD
			
			lua_register(L,"CreateThread_NetRequest",	&cThread_NetRequest_L::CreateThread_NetRequest);
		}
		
	// static methods exported to lua
		 
		/// pSendData		is used by the thread, DO NOT USE OR RELEASE IT UNTIL THE THREAD IS FINISHED
		/// pAnswerBuffer	is used by the thread, DO NOT USE OR RELEASE IT UNTIL THE THREAD IS FINISHED
		/// thread_netr		CreateThread_NetRequest	(sHost,iPort,fifo_SendData=nil,fifo_pAnswerBuffer=nil)
		static int			CreateThread_NetRequest	(lua_State *L) { PROFILE
			std::string	sHost			= luaL_checkstring(L,1);
			int			iPort			= luaL_checkint(L,2);
			cFIFO* 		pSendData		= (lua_gettop(L) >= 3 && !lua_isnil(L,3)) ? cLuaBind<cFIFO>::checkudata(L,3) : 0;
			cFIFO*		pAnswerBuffer	= (lua_gettop(L) >= 4 && !lua_isnil(L,4)) ? cLuaBind<cFIFO>::checkudata(L,4) : 0;
			return CreateUData(L,new cThread_NetRequest(sHost,iPort,pSendData,pAnswerBuffer));
		}
			
	// object methods exported to lua

		/// Destroy()
		static int	Destroy			(lua_State *L) { PROFILE
			delete checkudata_alive(L);
			return 0;
		}
		
		/// bool	IsFinished	()
		static int	IsFinished	(lua_State *L) { PROFILE
			lua_pushboolean(L,checkudata_alive(L)->IsFinished());
			return 1; 
		}
		
		/// bool	HasError	()
		static int	HasError	(lua_State *L) { PROFILE
			lua_pushboolean(L,checkudata_alive(L)->HasError());
			return 1; 
		}

		virtual const char* GetLuaTypeName () { return "lugre.thread_netrequest"; }
};


	
class cThread_LoadFile_L : public cLuaBind<cThread_LoadFile> { public:
	// implementation of cLuaBind

		/// called by Register(), registers object-methods (see cLuaBind constructor for examples)
		virtual void RegisterMethods	(lua_State *L) { PROFILE
			#define REGISTER_METHOD(methodname) mlMethod.push_back(make_luaL_reg(#methodname,&cThread_LoadFile_L::methodname));

			REGISTER_METHOD(Destroy);
			REGISTER_METHOD(IsFinished);
			REGISTER_METHOD(HasError);
			
			#undef REGISTER_METHOD
			
			lua_register(L,"CreateThread_LoadFile",	&cThread_LoadFile_L::CreateThread_LoadFile);
		}
		
	// static methods exported to lua
		 
		/// pAnswerBuffer is used by the thread, DO NOT USE OR RELEASE IT UNTIL THE THREAD IS FINISHED
		/// thread_loadf	CreateThread_LoadFile	(sFilePath,fifo_answerbuffer,iStart=0,iLength=-1)
		static int			CreateThread_LoadFile	(lua_State *L) { PROFILE
			std::string	sFilePath		= luaL_checkstring(L,1);
			cFIFO* 		pAnswerBuffer	= cLuaBind<cFIFO>::checkudata_alive(L,2);
			int			iStart			= (lua_gettop(L) >= 3 && !lua_isnil(L,3)) ? luaL_checkint(L,3) : 0;
			int			iLength			= (lua_gettop(L) >= 4 && !lua_isnil(L,4)) ? luaL_checkint(L,4) : -1;
			return CreateUData(L,new cThread_LoadFile(sFilePath,pAnswerBuffer,iStart,iLength));
		}
			
	// object methods exported to lua

		/// Destroy()
		static int	Destroy			(lua_State *L) { PROFILE
			delete checkudata_alive(L);
			return 0;
		}
		
		/// bool	IsFinished	()
		static int	IsFinished	(lua_State *L) { PROFILE
			lua_pushboolean(L,checkudata_alive(L)->IsFinished());
			return 1; 
		}
		
		/// bool	HasError	()
		static int	HasError	(lua_State *L) { PROFILE
			lua_pushboolean(L,checkudata_alive(L)->HasError());
			return 1; 
		}

		virtual const char* GetLuaTypeName () { return "lugre.thread_loadfile"; }
};

	
/// lua binding
void	cThread_NetRequest::LuaRegister 	(lua_State *L) { PROFILE
	cLuaBind<cThread_NetRequest>::GetSingletonPtr(new cThread_NetRequest_L())->LuaRegister(L);
}
void	cThread_LoadFile::LuaRegister 	(lua_State *L) { PROFILE
	cLuaBind<cThread_LoadFile>::GetSingletonPtr(new cThread_LoadFile_L())->LuaRegister(L);
}

};
