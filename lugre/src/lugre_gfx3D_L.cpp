#include "lugre_prefix.h"
#include "lugre_gfx3D.h"
#include "lugre_camera.h"
#include "lugre_scripting.h"
#include "lugre_game.h"
#include "lugre_ogrewrapper.h"
#include "lugre_input.h"
#include "lugre_robrenderable.h"
#include "lugre_robstring.h"
#include "lugre_luabind.h"
#include "lugre_gfx2D.h"
#include "lugre_beam.h"
#include <Ogre.h>

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

using namespace Ogre;

namespace Lugre {

class cGfx3D_L : public cLuaBind<cGfx3D> { public:
		/// called by Register(), registers object-methods (see cLuaBind constructor for examples)
		virtual void RegisterMethods	(lua_State *L) { PROFILE
			// mlMethod.push_back((struct luaL_reg){"Meemba",		cGfx3D_L::Get});
			// lua_register(L,"MyGlobalFun",	MyGlobalFun);
			// lua_register(L,"MyStaticMethod",	&cSomeClass::MyStaticMethod);

			lua_register(L,"CreateGfx3D",			&cGfx3D_L::CreateGfx3D);
			lua_register(L,"CreateRootGfx3D",		&cGfx3D_L::CreateRootGfx3D);
			lua_register(L,"CreateCamPosGfx3D",		&cGfx3D_L::CreateCamPosGfx3D);
			lua_register(L,"CreateCockpitGfx3D",	&cGfx3D_L::CreateCockpitGfx3D);
			
			#define REGISTER_METHOD(methodname) mlMethod.push_back(make_luaL_reg(#methodname,&cGfx3D_L::methodname));
			
			REGISTER_METHOD(Destroy);
			REGISTER_METHOD(CreateChild);
			REGISTER_METHOD(CreateTagPoint);
			REGISTER_METHOD(GetWorldAABB);
			REGISTER_METHOD(GetEntityBounds);
			REGISTER_METHOD(GetEntityBoundRad);
			REGISTER_METHOD(GetEntityIndexCount);
			REGISTER_METHOD(GetEntityVertex);
			REGISTER_METHOD(GetCustomBoundRad); /// mfBoundingRadius
			REGISTER_METHOD(SetCustomBoundRad);
			REGISTER_METHOD(RayPick);
			REGISTER_METHOD(RayPickList);
			REGISTER_METHOD(SetParent);
			REGISTER_METHOD(SetRootAsParent);
			REGISTER_METHOD(SetRenderingDistance);
			REGISTER_METHOD(SetVisible);
			REGISTER_METHOD(SetDisplaySkeleton);
			REGISTER_METHOD(SetPrepareFrameStep);
			REGISTER_METHOD(SetMaterial);
			REGISTER_METHOD(GetScale);
			REGISTER_METHOD(GetPosition);
			REGISTER_METHOD(GetDerivedPosition);
			REGISTER_METHOD(GetOrientation);
			REGISTER_METHOD(GetDerivedOrientation);
			REGISTER_METHOD(SetPosition);
			REGISTER_METHOD(SetScale);
			REGISTER_METHOD(SetNormaliseNormals);
			REGISTER_METHOD(SetOrientation);
			REGISTER_METHOD(SetMesh);
			REGISTER_METHOD(GetMeshSubEntityCount);
			REGISTER_METHOD(GetMeshSubEntityMaterial);
			REGISTER_METHOD(SetMeshSubEntityMaterial);
			REGISTER_METHOD(SetMeshSubEntityCustomParameter);
			REGISTER_METHOD(SetAnim);
			REGISTER_METHOD(IsAnimLooped);
			REGISTER_METHOD(HasBone);
			REGISTER_METHOD(GetAnimLength);
			REGISTER_METHOD(GetAnimTimePos);
			REGISTER_METHOD(SetAnimTimePos);
			REGISTER_METHOD(AnimAddTime);
			REGISTER_METHOD(HasSkeleton);
			REGISTER_METHOD(SetStarfield);
			REGISTER_METHOD(SetExplosion);
			REGISTER_METHOD(SetTargetTracker);
			REGISTER_METHOD(SetBillboard);
			REGISTER_METHOD(SetTrail);
			REGISTER_METHOD(SetRadar);
			REGISTER_METHOD(SetRadialGrid);
			REGISTER_METHOD(SetWireBoundingBoxGfx3D);
			REGISTER_METHOD(SetWireBoundingBoxMinMax);
			//~ REGISTER_METHOD(SetWireBoundingBoxMeshEntity);
			REGISTER_METHOD(SetSimpleRenderable);
			REGISTER_METHOD(RenderableBegin);
			REGISTER_METHOD(RenderableVertex);
			REGISTER_METHOD(RenderableIndex);
			REGISTER_METHOD(RenderableIndex3);
			REGISTER_METHOD(RenderableIndex2);
			REGISTER_METHOD(RenderableEnd);
			REGISTER_METHOD(RenderableConvertToMesh);
			REGISTER_METHOD(RenderableAddToMesh);
			REGISTER_METHOD(RenderableSkipVertices);
			REGISTER_METHOD(RenderableSkipIndices);
			REGISTER_METHOD(SetTextFont);
			REGISTER_METHOD(SetText);
			REGISTER_METHOD(SetCastShadows);
			
			REGISTER_METHOD(CreateMergedMesh);
			
			REGISTER_METHOD(SetParticleSystem);
			REGISTER_METHOD(ParticleSystem_FastForward);
			REGISTER_METHOD(ParticleSystem_SetNonVisibleUpdateTimeout);
			REGISTER_METHOD(ParticleSystem_SetSpeedFactor);
			REGISTER_METHOD(ParticleSystem_GetNumParticles);
			REGISTER_METHOD(ParticleSystem_RemoveAllEmitters);
			REGISTER_METHOD(ParticleSystem_SetEmitterRate);
			REGISTER_METHOD(ParticleSystem_SetDefaultParticleSize);
			REGISTER_METHOD(ParticleSystem_SetEmitterVelocityMinMax);
			
			REGISTER_METHOD(SetBeam);
			REGISTER_METHOD(BeamCountLines);
			REGISTER_METHOD(BeamClearLines);
			REGISTER_METHOD(BeamAddLine);
			REGISTER_METHOD(BeamClearLine);
			REGISTER_METHOD(BeamDeleteLine);
			REGISTER_METHOD(BeamAddPoint);
			REGISTER_METHOD(BeamSetPoint);
			REGISTER_METHOD(BeamPopFront);
			REGISTER_METHOD(BeamPopBack);
			REGISTER_METHOD(BeamUpdateBounds);
			
			REGISTER_METHOD(SetForcePosCam);
			REGISTER_METHOD(SetForceRotCam);
			REGISTER_METHOD(SetForceLookat);
			
			// for use with RenderableBegin()
			cScripting* scripting = cScripting::GetSingletonPtr();
			#define RegisterClassConstant(name) scripting->SetGlobal(#name,Ogre::RenderOperation::name)
			RegisterClassConstant(OT_POINT_LIST);
			RegisterClassConstant(OT_LINE_LIST);
			RegisterClassConstant(OT_LINE_STRIP);
			RegisterClassConstant(OT_TRIANGLE_LIST);
			RegisterClassConstant(OT_TRIANGLE_STRIP);
			RegisterClassConstant(OT_TRIANGLE_FAN);
			#undef RegisterClassConstant
		}

	/// static methods exported to lua


		static int	CreateGfx3D		(lua_State *L) { PROFILE
			std::string sSceneMgrName 	= (lua_gettop(L) >= 1 && !lua_isnil(L,1)) ? luaL_checkstring(L,1) : "main";
			Ogre::SceneManager*	pSceneMgr = cOgreWrapper::GetSingleton().GetSceneManager(sSceneMgrName.c_str());
			cGfx3D* target = pSceneMgr ? cGfx3D::NewFree(pSceneMgr) : 0;
			return CreateUData(L,target);
		}

		/// gfx3D:CreateChild()
		static int	CreateChild		(lua_State *L) { PROFILE
			cGfx3D* target = cGfx3D::NewChildOfGfx3D(checkudata_alive(L));
			return CreateUData(L,target);
		}

		/// gfx3D:CreateTagPoint(sBoneName,x,y,z,qw,qx,qy,qz)
		static int	CreateTagPoint		(lua_State *L) { PROFILE
			std::string sBoneName = luaL_checkstring(L,2);
			Ogre::Vector3		vOffsetPosition		= (lua_gettop(L) >= 5 && !lua_isnil(L,5)) ? Ogre::Vector3(		luaL_checknumber(L,3),luaL_checknumber(L,4),luaL_checknumber(L,5)) : Ogre::Vector3::ZERO;
			Ogre::Quaternion	qOffsetOrientation	= (lua_gettop(L) >= 8 && !lua_isnil(L,8)) ? Ogre::Quaternion(	luaL_checknumber(L,6),luaL_checknumber(L,7),luaL_checknumber(L,8),luaL_checknumber(L,9)) : Ogre::Quaternion::IDENTITY;
			cGfx3D* target = cGfx3D::NewTagPoint(checkudata_alive(L),sBoneName.c_str(),vOffsetPosition,qOffsetOrientation);
			return CreateUData(L,target);
		}
		
		/// for lua : gfx3D CreateRootGfx3D (szSceneMgrName="main")
		static int	CreateRootGfx3D		(lua_State *L) { PROFILE
			std::string sSceneMgrName 	= (lua_gettop(L) >= 1 && !lua_isnil(L,1)) ? luaL_checkstring(L,1) : "main";
			Ogre::SceneManager*	pSceneMgr = cOgreWrapper::GetSingleton().GetSceneManager(sSceneMgrName.c_str());
			cGfx3D* target = pSceneMgr ? cGfx3D::NewChildOfRoot(pSceneMgr) : 0;
			return CreateUData(L,target);
		}

		static int	CreateCamPosGfx3D		(lua_State *L) { PROFILE
			cGfx3D* target = cGfx3D::NewChildOfSceneNode(cOgreWrapper::GetSingleton().mpCamPosSceneNode);
			return CreateUData(L,target);
		}

		static int	CreateCockpitGfx3D		(lua_State *L) { PROFILE
			cGfx3D* target = cGfx3D::NewChildOfSceneNode(cOgreWrapper::GetSingleton().mpCamHolderSceneNode);
			return CreateUData(L,target);
		}

		/// gfx3D:SetForcePosCam(cam or nil)
		static int	SetForcePosCam			(lua_State *L) { PROFILE
			Ogre::Camera* cam = (lua_gettop(L) > 1 && !lua_isnil(L,2))?cLuaBind<cCamera>::checkudata_alive(L,2)->mpCam:0;
			checkudata_alive(L)->mpForcePosCam = cam;
			if (cam) checkudata_alive(L)->SetPrepareFrameStep(true);
			return 0;
		}
		
		/// gfx3D:SetForceRotCam(cam or nil)
		static int	SetForceRotCam			(lua_State *L) { PROFILE
			Ogre::Camera* cam = (lua_gettop(L) > 1 && !lua_isnil(L,2))?cLuaBind<cCamera>::checkudata_alive(L,2)->mpCam:0;
			checkudata_alive(L)->mpForceRotCam = cam;
			if (cam) checkudata_alive(L)->SetPrepareFrameStep(true);
			return 0;
		}
		
		/// gfx3D:SetForceLookat(gfx3d or nil)
		static int	SetForceLookat			(lua_State *L) { PROFILE
			cGfx3D* target =        (lua_gettop(L) > 1 && !lua_isnil(L,2))?checkudata_alive(L,2):0;
			checkudata_alive(L)->mpForceLookatTarget = target;
			if (target) checkudata_alive(L)->SetPrepareFrameStep(true);
			return 0;
		}
		
		/// gfx3D:Destroy()
		static int	Destroy			(lua_State *L) { PROFILE
			//printf("cGfx3D_L::Destroy start\n");
			delete checkudata_alive(L);
			//printf("cGfx3D_L::Destroy end\n");
			return 0;
		}
		
		
		/// determines the scenenode world bbox
		/// x1,y1,z1, x2,y2,z2	GetWorldAABB		()
		static int				GetWorldAABB		(lua_State *L) { PROFILE
			cGfx3D* mygfx = checkudata_alive(L);
			if (!mygfx->mpSceneNode) return 0;
			mygfx->mpSceneNode->_updateBounds();
			const Ogre::AxisAlignedBox& mybounds = mygfx->mpSceneNode->_getWorldAABB();
			lua_pushnumber(L,mybounds.getMinimum().x);
			lua_pushnumber(L,mybounds.getMinimum().y);
			lua_pushnumber(L,mybounds.getMinimum().z);
			lua_pushnumber(L,mybounds.getMaximum().x);
			lua_pushnumber(L,mybounds.getMaximum().y);
			lua_pushnumber(L,mybounds.getMaximum().z);
			return 6;
		}
		
		/// x1,y1,z1, x2,y2,z2	GetEntityBounds		()
		static int				GetEntityBounds		(lua_State *L) { PROFILE
			cGfx3D* mygfx = checkudata_alive(L);
			if (!mygfx->mpEntity) return 0;
			Ogre::MeshPtr pMesh = mygfx->mpEntity->getMesh();
			if (pMesh.isNull()) return 0;
			const Ogre::AxisAlignedBox& mybounds = pMesh->getBounds();
			lua_pushnumber(L,mybounds.getMinimum().x);
			lua_pushnumber(L,mybounds.getMinimum().y);
			lua_pushnumber(L,mybounds.getMinimum().z);
			lua_pushnumber(L,mybounds.getMaximum().x);
			lua_pushnumber(L,mybounds.getMaximum().y);
			lua_pushnumber(L,mybounds.getMaximum().z);
			return 6;
		}
		
		/// r		GetEntityBoundRad		()
		static int	GetEntityBoundRad		(lua_State *L) { PROFILE
			cGfx3D* mygfx = checkudata_alive(L);
			if (!mygfx->mpEntity) return 0;
			Ogre::MeshPtr pMesh = mygfx->mpEntity->getMesh();
			if (pMesh.isNull()) return 0;
			lua_pushnumber(L,pMesh->getBoundingSphereRadius());
			return 1;
		}
		
		/// int		GetEntityIndexCount		()
		static int	GetEntityIndexCount		(lua_State *L) { PROFILE
			cGfx3D* mygfx = checkudata_alive(L);
			lua_pushnumber(L,cOgreWrapper::GetSingleton().GetEntityIndexCount(mygfx->mpEntity));
			return 1;
		}
		
		/// x,y,z	GetEntityVertex		(iIndexIndex)
		static int	GetEntityVertex		(lua_State *L) { PROFILE
			cGfx3D* mygfx = checkudata_alive(L);
			Ogre::Vector3 p = cOgreWrapper::GetSingleton().GetEntityVertex(mygfx->mpEntity,luaL_checkint(L,2));
			lua_pushnumber(L,p.x);
			lua_pushnumber(L,p.y);
			lua_pushnumber(L,p.z);
			return 3;
		}
		
		/// r		GetCustomBoundRad		()
		static int	GetCustomBoundRad		(lua_State *L) { PROFILE
			lua_pushnumber(L,checkudata_alive(L)->mfCustomBoundingRadius);
			return 1;
		}
		
		/// 		SetCustomBoundRad		(float r)
		static int	SetCustomBoundRad		(lua_State *L) { PROFILE
			checkudata_alive(L)->mfCustomBoundingRadius = luaL_checknumber(L,2);
			return 0;
		}
		
		/// bhit,bhitdist,facenum,aabbhitfacenoirmalx,aabbhitfacenormaly = gfx3D:RayPick(rx,ry,rz,rvx,rvy,rvz) -- mainly for mousepicking
		static int	RayPick			(lua_State *L) { PROFILE
			cGfx3D* mygfx = checkudata_alive(L);
			float fHitDist = 0.0;
			bool bHit = false;
			int iFaceNum = -1;
			int fAABBHitFaceNormalX = -1;
			int fAABBHitFaceNormalY = -1;
			int fAABBHitFaceNormalZ = -1;
			
			// don't use ++i or something here, the compiler might mix the order
			Ogre::Vector3		vRayPos(luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4));
			Ogre::Vector3		vRayDir(luaL_checknumber(L,5),luaL_checknumber(L,6),luaL_checknumber(L,7));
			if (mygfx->mpEntity) {
				iFaceNum = cOgreWrapper::GetSingleton().RayEntityQuery(vRayPos,vRayDir,mygfx->mpEntity,&fHitDist);
				bHit = iFaceNum != -1;
			}
			if (mygfx->mbHasAABB) {
				bHit = cOgreWrapper::GetSingleton().RayAABBQuery(vRayPos - mygfx->GetPosition(),vRayDir,mygfx->mAABB,&fHitDist,&fAABBHitFaceNormalX,&fAABBHitFaceNormalY,&fAABBHitFaceNormalZ);
			}
			lua_pushboolean(L,bHit);
			lua_pushnumber(L,fHitDist);
			lua_pushnumber(L,iFaceNum);
			lua_pushnumber(L,fAABBHitFaceNormalX);
			lua_pushnumber(L,fAABBHitFaceNormalY);
			lua_pushnumber(L,fAABBHitFaceNormalZ);
			return 6;
		}
		
		/// returns a list of all hits
		/// table{facenum=dist,...} = gfx3D:RayPickList(rx,ry,rz,rvx,rvy,rvz) -- mainly for mousepicking
		static int	RayPickList			(lua_State *L) { PROFILE
			cGfx3D* mygfx = checkudata_alive(L);
			Ogre::Vector3 vRayPos(luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4));
			Ogre::Vector3 vRayDir(luaL_checknumber(L,5),luaL_checknumber(L,6),luaL_checknumber(L,7));
			
			if (!mygfx->mpEntity) return 0;
				
			std::vector<std::pair<float,int> > myHitList;
			cOgreWrapper::GetSingleton().RayEntityQuery(vRayPos,vRayDir,mygfx->mpEntity,myHitList);
			
			// construct result table
			lua_newtable(L);
			for (unsigned int i=0;i<myHitList.size();++i) {
				lua_pushnumber( L, myHitList[i].first );
				lua_rawseti( L, -2, myHitList[i].second );
			}
			return 1;
		}
		
		/// gfx3D:SetParent(gfx3D_or_nil)
		static int	SetParent			(lua_State *L) { PROFILE
			checkudata_alive(L)->SetParent((lua_gettop(L) > 1 && !lua_isnil(L,2))?checkudata(L,2):0);
			return 0;
		}
		
		/// gfx3D:SetRootAsParent(szSceneMgrName)
		static int	SetRootAsParent			(lua_State *L) { PROFILE
			std::string sSceneMgrName = (lua_gettop(L) >= 2 && !lua_isnil(L,2)) ? luaL_checkstring(L,2) : "main";
			Ogre::SceneManager*	pSceneMgr = cOgreWrapper::GetSingleton().GetSceneManager(sSceneMgrName.c_str());
			checkudata_alive(L)->SetParent(pSceneMgr?pSceneMgr->getRootSceneNode():0);
			return 0;
		}
		
		/// gfx3D:SetVisible( bool)
		static int	SetVisible				(lua_State *L) { PROFILE
			checkudata_alive(L)->SetVisible(lua_toboolean(L,2));
			return 0;
		}
		
		/// gfx3D:SetDisplaySkeleton( bool)
		static int	SetDisplaySkeleton				(lua_State *L) { PROFILE
			Ogre::Entity* myentity = checkudata_alive(L)->mpEntity;
			if (myentity) myentity->setDisplaySkeleton(lua_toboolean(L,2));
			return 0;
		}
		
		/// gfx3D:SetRenderingDistance( float dist)
		static int	SetRenderingDistance				(lua_State *L) { PROFILE
			Ogre::Entity* myentity = checkudata_alive(L)->mpEntity;
			if (myentity) myentity->setRenderingDistance(luaL_checknumber(L,2));
			return 0;
		}
		
		/// gfx3D:SetPrepareFrameStep( bool)
		static int	SetPrepareFrameStep				(lua_State *L) { PROFILE
			checkudata_alive(L)->SetPrepareFrameStep(lua_toboolean(L,2));
			return 0;
		}
		
		/// gfx3D:SetMaterial( string)
		static int	SetMaterial		(lua_State *L) { PROFILE /*(const char* szMat); */
			checkudata_alive(L)->SetMaterial(luaL_checkstring(L, 2));
			return 0;
		}
		
		/// x,y,z	GetScale	()
		static int	GetScale	(lua_State *L) { PROFILE
			Ogre::Vector3 p = checkudata_alive(L)->GetScale();
			lua_pushnumber(L,p.x);
			lua_pushnumber(L,p.y);
			lua_pushnumber(L,p.z);
			return 3;
		}
		
		/// x,y,z	GetPosition		()
		static int	GetPosition		(lua_State *L) { PROFILE
			Ogre::Vector3 p = checkudata_alive(L)->GetPosition();
			lua_pushnumber(L,p.x);
			lua_pushnumber(L,p.y);
			lua_pushnumber(L,p.z);
			return 3;
		}
		
		/// x,y,z	GetDerivedPosition	()
		static int	GetDerivedPosition	(lua_State *L) { PROFILE
			Ogre::Vector3 p = checkudata_alive(L)->GetDerivedPosition();
			lua_pushnumber(L,p.x);
			lua_pushnumber(L,p.y);
			lua_pushnumber(L,p.z);
			return 3;
		}
		
		/// w,x,y,z		GetOrientation	()
		static int		GetOrientation	(lua_State *L) { PROFILE
			Ogre::Quaternion q = checkudata_alive(L)->GetOrientation();
			lua_pushnumber(L,q.w);
			lua_pushnumber(L,q.x);
			lua_pushnumber(L,q.y);
			lua_pushnumber(L,q.z);
			return 4;
		}
		
		/// w,x,y,z		GetDerivedOrientation	()
		static int		GetDerivedOrientation	(lua_State *L) { PROFILE
			Ogre::Quaternion q = checkudata_alive(L)->GetDerivedOrientation();
			lua_pushnumber(L,q.w);
			lua_pushnumber(L,q.x);
			lua_pushnumber(L,q.y);
			lua_pushnumber(L,q.z);
			return 4;
		}
		
		/// gfx3D:SetPosition( float x, float y, float z)
		static int	SetPosition				(lua_State *L) { PROFILE
			checkudata_alive(L)->SetPosition(Ogre::Vector3(luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4)));
			return 0;
		}
		
		/// gfx3D:SetScale( float x, float y, float z)
		static int	SetScale				(lua_State *L) { PROFILE
			checkudata_alive(L)->SetScale(Ogre::Vector3(luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4)));
			return 0;
		}
		
		
		/// gfx3D:SetNormaliseNormals(bool bNormalise)
		static int	SetNormaliseNormals				(lua_State *L) { PROFILE
			bool bOn = (lua_isboolean(L,2) ? lua_toboolean(L,2) : luaL_checkint(L,2));
			checkudata_alive(L)->SetNormaliseNormals(bOn);
			return 0;
		}
		
		
		
		/// gfx3D:SetOrientation( float w, float x, float y, float z)
		static int	SetOrientation				(lua_State *L) { PROFILE
			checkudata_alive(L)->SetOrientation(Ogre::Quaternion(luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4),luaL_checknumber(L,5)));
			return 0;
		}

		/// void		SetBillboard		(udata_obj obj,vPos,Real radius,string matname="explosion")
		static int		SetBillboard		(lua_State *L) { PROFILE
			checkudata_alive(L)->SetBillboard(
				luaSFZ_checkVector3(L,2),
				luaL_checknumber(L,5),	// radius
				luaL_checkstring(L,6) 	// matname
				);
			return 0;
		}

		/// void		SetRadar		()
		static int		SetRadar		(lua_State *L) { PROFILE
			checkudata_alive(L)->SetRadar();
			return 0;
		}

		/// void		SetRadialGrid		()
		static int		SetRadialGrid		(lua_State *L) { PROFILE
			checkudata_alive(L)->SetRadialGrid();
			return 0;
		}

		/// void		SetWireBoundingBoxGfx3D		()
		static int		SetWireBoundingBoxGfx3D		(lua_State *L) { PROFILE
			checkudata_alive(L)->SetWireBoundingBox(*checkudata_alive(L,2));
			return 0;
		}
		/// void		SetWireBoundingBoxMinMax		()
		static int		SetWireBoundingBoxMinMax		(lua_State *L) { PROFILE
			checkudata_alive(L)->SetWireBoundingBox(luaSFZ_checkVector3(L,2),luaSFZ_checkVector3(L,5));
			return 0;
		}
		//~ /// void		SetWireBoundingBoxMeshEntity		()
		//~ static int		SetWireBoundingBoxMeshEntity		(lua_State *L) { PROFILE
			//~ Ogre::Entity* pEntity = cLuaBind<cMeshEntity>::checkudata_alive(L,2)->mpOgreEntity;
			//~ if (pEntity) checkudata_alive(L)->SetWireBoundingBox(*pEntity); else printf("SetWireBoundingBoxMeshEntity failed, no entity\n");
			//~ return 0;
		//~ }

		/// void		SetTrail		(udata_obj obj,vPos,Real length,Real elements, string matname="explosion")
		static int		SetTrail		(lua_State *L) { PROFILE
			checkudata_alive(L)->SetTrail(
				luaSFZ_checkVector3(L,2),
				luaL_checknumber(L,5),	// length
				luaL_checkint(L,6),	// elements
				luaL_checkstring(L,7), 	// matname
				luaL_checknumber(L,8),	// r
				luaL_checknumber(L,9),	// g
				luaL_checknumber(L,10),	// b
				luaL_checknumber(L,11),	// a
				luaL_checknumber(L,12),	// delta r
				luaL_checknumber(L,13),	// delta g
				luaL_checknumber(L,14),	// delta b
				luaL_checknumber(L,15),	// delta a
				luaL_checknumber(L,16),	// width
				luaL_checknumber(L,17)	// delta width
				);
			return 0;
		}

		/// void		SetExplosion		(udata_obj obj,Real radius,string matname="explosion")
		static int		SetExplosion		(lua_State *L) { PROFILE
			checkudata_alive(L)->SetExplosion(luaL_checknumber(L,2),luaL_checkstring(L,3));
			return 0;
		}

		/// OBSOLETE
		/// void		SetTargetTracker	(udata_obj obj,Real dist,Real size,colr,colg,colb,string matname="explosion")
		static int		SetTargetTracker	(lua_State *L) { PROFILE
			size_t index = 4;
			checkudata_alive(L)->SetTargetTracker(
				luaL_checknumber(L,2),	// fDist
				luaL_checknumber(L,3),	// fSize
				luaSFZ_checkColour3(L,index),	// color
				luaL_checkstring(L,7)	// matname
				);
			return 0;
		}

		// ***** ***** ***** ***** ***** Beam System
		
		/// void		SetBeam		(bUseVertexColour)
		static int		SetBeam		(lua_State *L) { PROFILE
			checkudata_alive(L)->SetBeam(lua_isboolean(L,2) ? lua_toboolean(L,2) : luaL_checkint(L,2));
			return 0;
		}
		
		/// int			BeamCountLines		()
		static int		BeamCountLines		(lua_State *L) { PROFILE lua_pushnumber(L,checkudata_alive(L)->mpBeam->CountLines()); return 1; }
		
		/// removes all lines
		/// void		BeamClearLines		()
		static int		BeamClearLines		(lua_State *L) { PROFILE checkudata_alive(L)->mpBeam->ClearLines(); return 0; }
		
		/// int			BeamAddLine			()
		static int		BeamAddLine			(lua_State *L) { PROFILE lua_pushnumber(L,checkudata_alive(L)->mpBeam->AddLine()); return 1; }
		
		/// void		BeamDeleteLine		(iLine)
		static int		BeamDeleteLine		(lua_State *L) { PROFILE checkudata_alive(L)->mpBeam->DeleteLine(luaL_checkint(L,2)); return 0; }
		
		/// removes all points on this line
		/// void		BeamClearLine		(iLine)
		static int		BeamClearLine		(lua_State *L) { PROFILE checkudata_alive(L)->mpBeam->ClearLine(luaL_checkint(L,2)); return 0; }
		
		/// void		BeamPopFront		(iLine)
		static int		BeamPopFront		(lua_State *L) { PROFILE checkudata_alive(L)->mpBeam->PopFront(luaL_checkint(L,2)); return 0; }
		
		/// void		BeamPopBack			(iLine)
		static int		BeamPopBack			(lua_State *L) { PROFILE checkudata_alive(L)->mpBeam->PopBack(luaL_checkint(L,2)); return 0; }
		
		/// void		BeamAddPoint		(iLine,x,y,z,h1,h2,u1,u2,v1,v2,r1=1,g1=1,b1=1,r2=1,g2=1,b2=1)
		static int		BeamAddPoint		(lua_State *L) { PROFILE
			int i=3;
			checkudata_alive(L)->mpBeam->AddPoint(luaL_checkint(L,2),cBeamPoint(
				Ogre::Vector3(luaL_checknumber(L,i+0),luaL_checknumber(L,i+1),luaL_checknumber(L,i+2)), // x,y,z
				luaL_checknumber(L,i+3),luaL_checknumber(L,i+4), // h1,h2
				luaL_checknumber(L,i+5),luaL_checknumber(L,i+6), // u1,u2
				luaL_checknumber(L,i+7),luaL_checknumber(L,i+8), // v1,v2
				lua_gettop(L) >= i+12 ? Ogre::ColourValue(luaL_checknumber(L,i+ 9),luaL_checknumber(L,i+10),luaL_checknumber(L,i+11),luaL_checknumber(L,i+12)) : Ogre::ColourValue::White, // col1
				lua_gettop(L) >= i+16 ? Ogre::ColourValue(luaL_checknumber(L,i+13),luaL_checknumber(L,i+14),luaL_checknumber(L,i+15),luaL_checknumber(L,i+16)) : Ogre::ColourValue::White  // col2
				)); 
			return 0; 
		}
		
		/// void		BeamSetPoint		(iLine,iPoint,x,y,z,h1,h2,u1,u2,v1,v2,r1=1,g1=1,b1=1,r2=1,g2=1,b2=1)
		static int		BeamSetPoint		(lua_State *L) { PROFILE 
			cBeamPoint* p = checkudata_alive(L)->mpBeam->GetPoint(luaL_checkint(L,2),luaL_checkint(L,3)); 
			int i=4;
			if (p) *p = cBeamPoint(
				Ogre::Vector3(luaL_checknumber(L,i+0),luaL_checknumber(L,i+1),luaL_checknumber(L,i+2)), // x,y,z
				luaL_checknumber(L,i+3),luaL_checknumber(L,i+4), // h1,h2
				luaL_checknumber(L,i+5),luaL_checknumber(L,i+6), // u1,u2
				luaL_checknumber(L,i+7),luaL_checknumber(L,i+8), // v1,v2
				lua_gettop(L) >= i+12 ? Ogre::ColourValue(luaL_checknumber(L,i+ 9),luaL_checknumber(L,i+10),luaL_checknumber(L,i+11),luaL_checknumber(L,i+12)) : Ogre::ColourValue::White, // col1
				lua_gettop(L) >= i+16 ? Ogre::ColourValue(luaL_checknumber(L,i+13),luaL_checknumber(L,i+14),luaL_checknumber(L,i+15),luaL_checknumber(L,i+16)) : Ogre::ColourValue::White  // col2
				);
			return 0; 
		}
		
		/// call this after changing geometry
		/// void		BeamUpdateBounds	()
		static int		BeamUpdateBounds	(lua_State *L) { PROFILE checkudata_alive(L)->mpBeam->UpdateBounds(); return 0; }
		
		// ***** ***** ***** ***** ***** ParticleSystem
		
		/// void		SetParticleSystem			(udata_obj obj, string templatename="Examples/Fireworks")
		static int		SetParticleSystem			(lua_State *L) { PROFILE
			checkudata_alive(L)->SetParticleSystem(luaL_checkstring(L, 2));
			return 0;
		}
		
		/// void		ParticleSystem_FastForward			(udata_obj obj, float time, float interval)
		static int		ParticleSystem_FastForward			(lua_State *L) { PROFILE
			Ogre::ParticleSystem* target = checkudata_alive(L)->mpParticleSystem;
			if (target) target->fastForward(luaL_checknumber(L, 2),luaL_checknumber(L, 3));
			return 0;
		}
		/// void		ParticleSystem_SetNonVisibleUpdateTimeout			(float time)
		static int		ParticleSystem_SetNonVisibleUpdateTimeout			(lua_State *L) { PROFILE
			Ogre::ParticleSystem* target = checkudata_alive(L)->mpParticleSystem;
			if (target) target->setNonVisibleUpdateTimeout(luaL_checknumber(L, 2));
			return 0;
		}
		/// void		ParticleSystem_SetSpeedFactor			(float time)
		static int		ParticleSystem_SetSpeedFactor			(lua_State *L) { PROFILE
			Ogre::ParticleSystem* target = checkudata_alive(L)->mpParticleSystem;
			if (target) target->setSpeedFactor(luaL_checknumber(L, 2));
			return 0;
		}
		
		/// int			ParticleSystem_GetNumParticles			()
		static int		ParticleSystem_GetNumParticles			(lua_State *L) { PROFILE
			lua_pushnumber(L,checkudata_alive(L)->GetNumParticles());
			return 1;
		}
		/// void		ParticleSystem_RemoveAllEmitters			()
		static int		ParticleSystem_RemoveAllEmitters			(lua_State *L) { PROFILE
			Ogre::ParticleSystem* target = checkudata_alive(L)->mpParticleSystem;
			if (target) target->removeAllEmitters();
			return 0;
		}
		/// void		ParticleSystem_SetEmitterRate			(iEmitterIndex,fRate)
		static int		ParticleSystem_SetEmitterRate			(lua_State *L) { PROFILE
			Ogre::ParticleSystem* target = checkudata_alive(L)->mpParticleSystem;
			if (!target) return 0;
			Ogre::ParticleEmitter* emitter = target->getEmitter(luaL_checkint(L, 2));
			if (emitter) emitter->setEmissionRate(luaL_checknumber(L,3));
			return 0;
		}
		/// void		ParticleSystem_SetDefaultParticleSize	(fW,fH)
		static int		ParticleSystem_SetDefaultParticleSize	(lua_State *L) { PROFILE
			Ogre::ParticleSystem* target = checkudata_alive(L)->mpParticleSystem;
			if (target) target->setDefaultDimensions(luaL_checknumber(L,2),luaL_checknumber(L,3));
			return 0;
		}
		/// void		ParticleSystem_SetEmitterVelocityMinMax	(fMin,fMax)
		static int		ParticleSystem_SetEmitterVelocityMinMax	(lua_State *L) { PROFILE
			Ogre::ParticleSystem* target = checkudata_alive(L)->mpParticleSystem;
			if (!target) return 0;
			Ogre::ParticleEmitter* emitter = target->getEmitter(luaL_checkint(L, 2));
			if (emitter) emitter->setParticleVelocity(luaL_checknumber(L,3),luaL_checknumber(L,4));
			return 0;
		}
		
		// ***** ***** ***** ***** ***** Mesh
		
		/// void		SetMesh			(udata_obj obj, string meshname="razor.mesh")
		static int		SetMesh			(lua_State *L) { PROFILE
			checkudata_alive(L)->SetMesh(luaL_checkstring(L, 2));
			return 0;
		}
		
		/// void		CreateMergedMesh			(udata_obj obj, string meshname)
		static int		CreateMergedMesh			(lua_State *L) { PROFILE
			checkudata_alive(L)->CreateMergedMesh(luaL_checkstring(L, 2));
			return 0;
		}

		/// int			GetMeshSubEntityCount	()
		static int		GetMeshSubEntityCount	(lua_State *L) { PROFILE
			cGfx3D* mygfx = checkudata_alive(L);
			if (!mygfx->mpEntity) return 0;
			lua_pushnumber(L,mygfx->mpEntity->getNumSubEntities());
			return 1;
		}
		
		
		/// string		GetMeshSubEntityMaterial	(iSubEntityIndexZeroBased)
		static int		GetMeshSubEntityMaterial	(lua_State *L) { PROFILE
			cGfx3D* mygfx = checkudata_alive(L);
			if (!mygfx->mpEntity) return 0;
			Ogre::SubEntity* pSub = mygfx->mpEntity->getSubEntity(luaL_checkint(L,2));
			if (!pSub) return 0;
			lua_pushstring(L,pSub->getMaterialName().c_str());
			return 1;
		}
		
		/// void		SetMeshSubEntityMaterial	(iSubEntityIndexZeroBased,matname)
		static int		SetMeshSubEntityMaterial	(lua_State *L) { PROFILE
			cGfx3D* mygfx = checkudata_alive(L);
			if (!mygfx->mpEntity) return 0;
			Ogre::SubEntity* pSub = mygfx->mpEntity->getSubEntity(luaL_checkint(L,2));
			if (pSub) pSub->setMaterialName(luaL_checkstring(L,3));
			return 0;
		}
		
		/// void		SetMeshSubEntityCustomParameter	(iSubEntityIndexZeroBased,iParam,x,y,z,w)
		static int		SetMeshSubEntityCustomParameter	(lua_State *L) { PROFILE
			cGfx3D* mygfx = checkudata_alive(L);
			if (!mygfx->mpEntity) return 0;
			Ogre::SubEntity* pSub = mygfx->mpEntity->getSubEntity(luaL_checkint(L,2));
			int iParam = luaL_checkint(L,3);
			float x = luaL_checknumber(L,4);
			float y = luaL_checknumber(L,5);
			float z = luaL_checknumber(L,6);
			float w = luaL_checknumber(L,7);
			if (pSub) pSub->setCustomParameter(iParam,Ogre::Vector4(x,y,z,w));
			return 0;
		}
		
		/// void		SetAnim			(sAnimName,bLoop)
		static int		SetAnim			(lua_State *L) { PROFILE
			checkudata_alive(L)->SetAnim(luaL_checkstring(L, 2),lua_isboolean(L,3) ? lua_toboolean(L,3) : luaL_checkint(L,3));
			return 0;
		}
		
		/// float		GetAnimLength			(sAnimName)
		static int		GetAnimLength			(lua_State *L) { PROFILE
			lua_pushnumber(L,checkudata_alive(L)->GetAnimLength(luaL_checkstring(L, 2)));
			return 1;
		}
		
		/// float		GetAnimTimePos			()
		static int		GetAnimTimePos			(lua_State *L) { PROFILE
			lua_pushnumber(L,checkudata_alive(L)->GetAnimTimePos());
			return 1;
		}
		
		/// void		SetAnimTimePos			(float fTimeInSeconds)
		static int		SetAnimTimePos			(lua_State *L) { PROFILE
			checkudata_alive(L)->SetAnimTimePos(luaL_checknumber(L,2));
			return 0;
		}
		
		/// void		AnimAddTime		(fTime)  (loops automatically)
		static int		AnimAddTime		(lua_State *L) { PROFILE
			Ogre::AnimationState* pAnimState = checkudata_alive(L)->mpAnimState;
			if (pAnimState) pAnimState->addTime(luaL_checknumber(L,2));
			return 0;
		}
		
		/// bool		IsAnimLooped			()
		static int		IsAnimLooped			(lua_State *L) { PROFILE
			lua_pushboolean(L,checkudata_alive(L)->IsAnimLooped());
			return 1;
		}
		
		/// bool		HasBone					(sBoneName)
		static int		HasBone					(lua_State *L) { PROFILE
			lua_pushboolean(L,checkudata_alive(L)->HasBone(luaL_checkstring(L, 2)));
			return 1;
		}
		
		/// bool		HasSkeleton			()
		static int		HasSkeleton			(lua_State *L) { PROFILE
			cGfx3D* mygfx = checkudata_alive(L);
			if (!mygfx->mpEntity) return 0;
			lua_pushboolean(L,mygfx->mpEntity->hasSkeleton());
			return 1;
		}
		
		// ***** ***** ***** ***** ***** SimpleRenderable

		/// void		SetSimpleRenderable		()
		static int		SetSimpleRenderable		(lua_State *L) { PROFILE
			checkudata_alive(L)->SetSimpleRenderable();
			return 0;
		}

		/// void		RenderableBegin		(iVertexCount,iIndexCount,bDynamic,bKeepOldIndices,opType)
		/// optype like OT_TRIANGLE_LIST
		static int		RenderableBegin		(lua_State *L) { PROFILE
			// void	Begin	(size_t iVertexCount,size_t iIndexCount,bool bDynamic,bool bKeepOldIndices,RenderOperation::OperationType opType);
			checkudata_alive(L)->mpSimpleRenderable->Begin(
				luaL_checkint(L,2),
				luaL_checkint(L,3),
				lua_isboolean(L,4) ? lua_toboolean(L,4) : luaL_checkint(L,4),
				lua_isboolean(L,5) ? lua_toboolean(L,5) : luaL_checkint(L,5),
				(Ogre::RenderOperation::OperationType)luaL_checkint(L,6)
				);
			return 0;
		}
		
		/*
		must be called between RenderableBegin and RenderableEnd
		Real : 1 float
		Vector3 : 3 floats  x,y,z
		ColourValue : 4 floats  r,g,b,a
		void	RenderableVertex	(float,float,float,...);
		*/
		/// void		RenderableVertex	(x,y,z,nx,ny,nz,u,v,	r,g,b,a)
		static int		RenderableVertex	(lua_State *L) { PROFILE
			cRobRenderOp* pRobRenderOp = checkudata_alive(L)->mpSimpleRenderable;
			if (!pRobRenderOp) return 0;
			#define F(i) luaL_checknumber(L,i)
			#define V(i) Vector3(F(i+0),F(i+1),F(i+2))
			#define C(i) ColourValue(F(i+0),F(i+1),F(i+2),F(i+3))
			Ogre::Vector3 p(F(2),F(3),F(4));
			int argc = lua_gettop(L) - 1; // arguments, not counting "this"-object
			switch (argc) {
					  case 3:	pRobRenderOp->Vertex(p);					// x,y,z		
				break;case 5:	pRobRenderOp->Vertex(p,F(5),F(6));			// x,y,z,u,v
				break;case 6:	pRobRenderOp->Vertex(p,V(5));				// x,y,z,nx,ny,nz
				break;case 8:	pRobRenderOp->Vertex(p,V(5),F(8),F(9));		// x,y,z,nx,ny,nz,u,v
					
				break;case 7:	pRobRenderOp->Vertex(p,C(5));				// x,y,z,				r,g,b,a
				break;case 9:	pRobRenderOp->Vertex(p,F(5),F(6),C(7));		// x,y,z,u,v,			r,g,b,a
				break;case 10:	pRobRenderOp->Vertex(p,V(5),C(8));			// x,y,z,nx,ny,nz,		r,g,b,a
				break;case 12:	pRobRenderOp->Vertex(p,V(5),F(8),F(9),C(10));// x,y,z,nx,ny,nz,u,v,	r,g,b,a
				break;default: printf("WARNING ! cGfx3D_L::RenderableVertex : strange argument count : %d\n",argc);
			}
			return 0;
		}
		
		/// must be called between RenderableBegin and RenderableEnd
		/// void		RenderableIndex		(iIndex)
		static int		RenderableIndex		(lua_State *L) { PROFILE
			checkudata_alive(L)->mpSimpleRenderable->Index(luaL_checkint(L,2));
			return 0;
		}

		/// must be called between RenderableBegin and RenderableEnd, useful for triangles
		/// void		RenderableIndex3		(iIndex,iIndex,iIndex)
		static int		RenderableIndex3		(lua_State *L) { PROFILE
			checkudata_alive(L)->mpSimpleRenderable->Index(luaL_checkint(L,2));
			checkudata_alive(L)->mpSimpleRenderable->Index(luaL_checkint(L,3));
			checkudata_alive(L)->mpSimpleRenderable->Index(luaL_checkint(L,4));
			return 0;
		}
		
		/// must be called between RenderableBegin and RenderableEnd, useful for lines
		/// void		RenderableIndex2		(iIndex,iIndex)
		static int		RenderableIndex2		(lua_State *L) { PROFILE
			checkudata_alive(L)->mpSimpleRenderable->Index(luaL_checkint(L,2));
			checkudata_alive(L)->mpSimpleRenderable->Index(luaL_checkint(L,3));
			return 0;
		}
		
		/// void		RenderableSkipVertices	()
		static int		RenderableSkipVertices	(lua_State *L) { PROFILE
			checkudata_alive(L)->mpSimpleRenderable->SkipVertices(luaL_checkint(L,2));
			return 0;
		}
		
		/// void		RenderableSkipIndices	()
		static int		RenderableSkipIndices	(lua_State *L) { PROFILE
			checkudata_alive(L)->mpSimpleRenderable->SkipIndices(luaL_checkint(L,2));
			return 0;
		}
		
		/// void		RenderableEnd		()
		static int		RenderableEnd		(lua_State *L) { PROFILE
			checkudata_alive(L)->mpSimpleRenderable->End();
			return 0;
		}
		
		/// sMeshName	RenderableConvertToMesh		(sMeshName=GetUniqueName())
		static int		RenderableConvertToMesh		(lua_State *L) { PROFILE
			std::string sMeshName = (lua_gettop(L) >= 2 && !lua_isnil(L,2)) ? luaL_checkstring(L,2) : cOgreWrapper::GetSingleton().GetUniqueName();
			checkudata_alive(L)->mpSimpleRenderable->ConvertToMesh(sMeshName);
			lua_pushstring(L,sMeshName.c_str());
			return 1;
		}
		
		/// void		RenderableAddToMesh			(sMeshName)
		static int		RenderableAddToMesh			(lua_State *L) { PROFILE
			std::string sMeshName = luaL_checkstring(L,2);
			checkudata_alive(L)->mpSimpleRenderable->AddToMesh(sMeshName);
			return 0;
		}
		
		// ***** ***** ***** ***** ***** Rest

		/// void		SetStarfield			(udata_obj obj, int numstars,float rad,float fColoring, string matname="explosion")
		static int		SetStarfield			(lua_State *L) { PROFILE
			checkudata_alive(L)->SetStarfield(luaL_checkint(L, 2),luaL_checknumber(L,3),luaL_checknumber(L,4),luaL_checkstring(L,5));
			return 0;
		}
		
		/// void		SetTextFont		(sFontName)
		static int		SetTextFont		(lua_State *L) { PROFILE
			checkudata_alive(L)->SetTextFont(luaL_checkstring(L,2));
			return 0;
		}

		/// void		SetCastShadows		(bool shadow)
		static int		SetCastShadows		(lua_State *L) { PROFILE
			checkudata_alive(L)->SetCastShadows(lua_toboolean(L,2));
			return 0;
		}
		
		/// void		SetText		(sText,fSize,r,g,b,a,fWrapMaxW,align)
		/// align : kGfx2DAlign_Left,kGfx2DAlign_Right,kGfx2DAlign_Center
		static int		SetText		(lua_State *L) { PROFILE
			
			float fWrapMaxW	= (lua_gettop(L) >= 7 && !lua_isnil(L,7)) ? luaL_checknumber(L,7) : 0;
			int iTextAlign	= (lua_gettop(L) >= 8 && !lua_isnil(L,8)) ? luaL_checkint(L,8) : cGfx2D::kGfx2DAlign_Left;
			Ogre::GuiHorizontalAlignment ogrealign = Ogre::GHA_LEFT;
			switch (iTextAlign) {
				case cGfx2D::kGfx2DAlign_Left:		ogrealign = Ogre::GHA_LEFT; break;
				case cGfx2D::kGfx2DAlign_Center:	ogrealign = Ogre::GHA_CENTER; break;
				case cGfx2D::kGfx2DAlign_Right:		ogrealign = Ogre::GHA_RIGHT; break;
				default : printf("cGfx3D::SetText : unknown iTextAlign %d\n",iTextAlign);
			}
			
			checkudata_alive(L)->SetText(
				luaL_checkstring(L,2),
				luaL_checknumber(L,3),
				Ogre::ColourValue(
					luaL_checknumber(L,4),
					luaL_checknumber(L,5),
					luaL_checknumber(L,6)),
				fWrapMaxW,
				ogrealign);
			return 0;
		}

		virtual const char* GetLuaTypeName () { return "lugre.gfx"; }
};

/// lua binding
void	cGfx3D::LuaRegister 	(lua_State *L) { PROFILE
	cLuaBind<cGfx3D>::GetSingletonPtr(new cGfx3D_L())->LuaRegister(L);
}

};
