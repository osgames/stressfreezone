#include "lugre_prefix.h"
#include "lugre_luabind.h"
#include "lugre_camera.h"
#include "lugre_ogrewrapper.h"
#include <Ogre.h>

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

using namespace Ogre;

class lua_State;
	
namespace Lugre {

	
/// todo : port existing cam completely to this system ?

cCamera::cCamera	(Ogre::Camera* pCam) : mpCam(pCam) {}

cCamera::cCamera	(Ogre::SceneManager* pSceneMgr,const char* szCamName) {
	assert(pSceneMgr); 
	mpCam = pSceneMgr->createCamera(szCamName);
}

cCamera::~cCamera() {
	// detach before delete ?
	if (mpCam) mpCam->getSceneManager()->destroyCamera(mpCam); mpCam = 0;
}


/*
	mCamera->setAspectRatio(Real(mViewport->getActualWidth()) / Real(mViewport->getActualHeight()));
	mCamera->setPosition(Vector3(0,0,40));
	// Look back along -Z
	//mCamera->lookAt(Vector3(0,0,0));
	mCamera->setNearClipDistance(1);
	//mCamera->setPolygonMode(PM_WIREFRAME);
	Ogre::Quaternion qCamRot = cOgreWrapper::GetSingleton().mCamera->getOrientation();
		
	// ortho mode with screen coords = screensize in pixels
	pCam->setFOVy( Ogre::Degree(90) );
	pCam->setNearClipDistance( 0.5 * cOgreWrapper::GetSingleton().mViewport->getActualHeight() );
	pCam->setProjectionType( Ogre::PT_ORTHOGRAPHIC );
*/

class cCamera_L : public cLuaBind<cCamera> { public:
		/// called by Register(), registers object-methods (see cLuaBind constructor for examples)
		virtual void RegisterMethods	(lua_State *L) { PROFILE
			#define REGISTER_METHOD(methodname) mlMethod.push_back(make_luaL_reg(#methodname,&cCamera_L::methodname));
			REGISTER_METHOD(Destroy);
			REGISTER_METHOD(SetFOVy);
			REGISTER_METHOD(SetNearClipDistance);
			REGISTER_METHOD(SetFarClipDistance);
			REGISTER_METHOD(SetProjectionType);
			REGISTER_METHOD(SetAspectRatio);
			REGISTER_METHOD(Move);
			REGISTER_METHOD(SetPos);
			REGISTER_METHOD(GetPos); 
			REGISTER_METHOD(SetRot);
			REGISTER_METHOD(GetRot); 
			REGISTER_METHOD(LookAt); 
			REGISTER_METHOD(GetNearClipDistance); 
			REGISTER_METHOD(GetFarClipDistance); 
			REGISTER_METHOD(GetEulerAng); 
			
			lua_register(L,"CreateCamera",	&cCamera_L::CreateCamera);
			lua_register(L,"GetMainCam",	&cCamera_L::GetMainCam);
		}

	// object methods exported to lua

		// todo : rotation, position, aspect ratio, near/farclip...
			
		/// void		Destroy				()
		static int		Destroy				(lua_State *L) { PROFILE delete checkudata_alive(L); return 0; }
		
		/// void		SetFOVy				(float fAngInRadians)
		static int		SetFOVy				(lua_State *L) { PROFILE 
			if (Ogre::Camera* pCam = checkudata_alive(L)->mpCam) pCam->setFOVy(Ogre::Radian(luaL_checknumber(L,2))); 
			return 0; 
		}
		/// void		SetNearClipDistance		(float f)
		static int		SetNearClipDistance		(lua_State *L) { PROFILE 
			if (Ogre::Camera* pCam = checkudata_alive(L)->mpCam) pCam->setNearClipDistance(luaL_checknumber(L,2)); 
			return 0; 
		}
		/// void		SetFarClipDistance		(float f) : 0=infinite
		static int		SetFarClipDistance		(lua_State *L) { PROFILE 
			if (Ogre::Camera* pCam = checkudata_alive(L)->mpCam) pCam->setFarClipDistance(luaL_checknumber(L,2)); 
			return 0; 
		}
		/// void		SetProjectionType	(int mode) : 0=PT_PERSPECTIVE 1=PT_ORTHOGRAPHIC
		static int		SetProjectionType	(lua_State *L) { PROFILE 
			switch (luaL_checkint(L,2)) {
				case 1:	if (Ogre::Camera* pCam = checkudata_alive(L)->mpCam) pCam->setProjectionType(Ogre::PT_ORTHOGRAPHIC); break;
				default:if (Ogre::Camera* pCam = checkudata_alive(L)->mpCam) pCam->setProjectionType(Ogre::PT_PERSPECTIVE); break;
			}
			return 0; 
		}
		

		/// usually SetAspectRatio(Real(mViewport->getActualWidth()) / Real(mViewport->getActualHeight()))
		static int		SetAspectRatio	(lua_State *L) { PROFILE
			if (Ogre::Camera* pCam = checkudata_alive(L)->mpCam) 
				pCam->setAspectRatio(luaL_checknumber(L,2));
			return 0;
		}

		static int		Move	(lua_State *L) { PROFILE
			if (Ogre::Camera* pCam = checkudata_alive(L)->mpCam) 
				pCam->move(Ogre::Vector3(luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4)));
			return 0;
		}

		static int		SetPos	(lua_State *L) { PROFILE
			if (Ogre::Camera* pCam = checkudata_alive(L)->mpCam) 
				pCam->setPosition(luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4));
			return 0;
		}

		static int		SetRot (lua_State *L) { PROFILE
			if (Ogre::Camera* pCam = checkudata_alive(L)->mpCam) 
				pCam->setOrientation(Ogre::Quaternion(luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4),luaL_checknumber(L,5)));
			return 0;
		}
		
		/// for lua	: x,y,z		GetPos	()
		static int				GetPos	(lua_State *L) { PROFILE
			Ogre::Camera* pCam = checkudata_alive(L)->mpCam;
			if (!pCam) return 0;
			Ogre::Vector3 	vCamPos = pCam->getPosition();
			lua_pushnumber(L,vCamPos.x);
			lua_pushnumber(L,vCamPos.y);
			lua_pushnumber(L,vCamPos.z);
			return 3;
		}
		
		/// for lua	: w,x,y,z	GetRot	()
		static int				GetRot	(lua_State *L) { PROFILE
			Ogre::Camera* pCam = checkudata_alive(L)->mpCam;
			if (!pCam) return 0;
			Ogre::Quaternion 	qCamRot = pCam->getOrientation();
			lua_pushnumber(L,qCamRot.w);
			lua_pushnumber(L,qCamRot.x);
			lua_pushnumber(L,qCamRot.y);
			lua_pushnumber(L,qCamRot.z);
			return 4;
		}
		

		static int		LookAt (lua_State *L) { PROFILE
			if (Ogre::Camera* pCam = checkudata_alive(L)->mpCam) 
				pCam->lookAt(luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4));
			return 0;
		}
		
		static int		GetNearClipDistance (lua_State *L) { PROFILE
			Ogre::Camera* pCam = checkudata_alive(L)->mpCam;
			if (!pCam) return 0;
			lua_pushnumber(L,pCam->getNearClipDistance());
			return 1;
		}
		
		static int		GetFarClipDistance (lua_State *L) { PROFILE
			Ogre::Camera* pCam = checkudata_alive(L)->mpCam;
			if (!pCam) return 0;
			lua_pushnumber(L,pCam->getFarClipDistance());
			return 1;
		}
		
		/// returns cam rotation as euler angles
		static int		GetEulerAng (lua_State *L) { PROFILE
			Ogre::Camera* pCam = checkudata_alive(L)->mpCam;
			if (!pCam) return 0;
			Ogre::Quaternion qCamRot = pCam->getOrientation();
			lua_pushnumber(L,qCamRot.getRoll().valueRadians());
			lua_pushnumber(L,qCamRot.getPitch().valueRadians());
			lua_pushnumber(L,qCamRot.getYaw().valueRadians());
			return 3;
		}
		
				
	// static methods exported to lua

		/// udata_cam	CreateCamera	(sSceneMgrName="main",sCamName=uniquename())
		static int		CreateCamera	(lua_State *L) { PROFILE
			std::string sSceneMgrName 	= (lua_gettop(L) >= 1 && !lua_isnil(L,1)) ? luaL_checkstring(L,1) : "main";
			std::string sCamName 		= (lua_gettop(L) >= 2 && !lua_isnil(L,2)) ? luaL_checkstring(L,2) : cOgreWrapper::GetSingleton().GetUniqueName();
			Ogre::SceneManager*	pSceneMgr = cOgreWrapper::GetSingleton().GetSceneManager(sSceneMgrName.c_str());
			cCamera* target = pSceneMgr ? new cCamera(pSceneMgr,sCamName.c_str()) : 0;
			return CreateUData(L,target);
		}
		
		/// udata_cam	GetMainCam	()
		static int		GetMainCam	(lua_State *L) { PROFILE
			static cCamera* pMainCam = 0;
			if (!pMainCam) pMainCam = new cCamera(cOgreWrapper::GetSingleton().mCamera);
			return CreateUData(L,pMainCam);
		}
		
		virtual const char* GetLuaTypeName () { return "lugre.camera"; }
};

/// lua binding
void	cCamera::LuaRegister 	(lua_State *L) { PROFILE
	cLuaBind<cCamera>::GetSingletonPtr(new cCamera_L())->LuaRegister(L);
}

};
