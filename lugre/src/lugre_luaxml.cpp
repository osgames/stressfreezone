#include "lugre_prefix.h"
#include "lugre_luaxml.h"
#include "tinyxml.h"


namespace Lugre {

/*
lua array manipulation from c :

lua_rawseti
          void lua_rawseti (lua_State *L, int index, int n);
Does the equivalent of t[n] = v, where t is the value at the given valid index index and v is the value at the top of the stack,
This function pops the value from the stack. The assignment is raw; that is, it does not invoke metamethods. 

lua_setfield
          void lua_setfield (lua_State *L, int index, const char *k);
Does the equivalent to t[k] = v, where t is the value at the given valid index index and v is the value at the top of the stack,
This function pops the value from the stack. As in Lua, this function may trigger a metamethod for the "newindex" event (see 2.8). 

lua_settable
          void lua_settable (lua_State *L, int index);
Does the equivalent to t[k] = v, where t is the value at the given valid index index, v is the value at the top of the stack, 
and k is the value just below the top.
This function pops both the key and the value from the stack. As in Lua, 
this function may trigger a metamethod for the "newindex" event (see 2.8). 

lua_rawset
          void lua_rawset (lua_State *L, int index);
Similar to lua_settable, but does a raw assignment (i.e., without metamethods). 

lua_pushstring
          void lua_pushstring (lua_State *L, const char *s);

lua_createtable
          void lua_createtable (lua_State *L, int narr, int nrec);
Creates a new empty table and pushes it onto the stack. The new table has space pre-allocated 
for narr array elements and nrec non-array elements. 
This pre-allocation is useful when you know exactly how many elements the table will have. 
Otherwise you can use the function lua_newtable. 

*/


extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

/** Produces an XMLTree like :    
<paragraph justify='centered'>first child<b>bold</b>second child</paragraph> 

{name="paragraph", attr={justify="centered"}, 
  "first child",
  {name="b", "bold", n=1}
  "second child",
  n=3
} 

comments and other definitions are ignored
*/
void LuaXML_ParseNode (lua_State *L,TiXmlNode* pNode) { PROFILE
	if (!pNode) return;
	// resize stack if neccessary
	luaL_checkstack(L, 5, "LuaXML_ParseNode : recursion too deep");
	
	TiXmlElement* pElem = pNode->ToElement();
	if (pElem) {
		// element name
		lua_pushstring(L,"name");
		lua_pushstring(L,pElem->Value());
		lua_settable(L,-3);
		//lua_setfield(L,-2,"name");
		
		// parse attributes
		TiXmlAttribute* pAttr = pElem->FirstAttribute();
		if (pAttr) {
			lua_pushstring(L,"attr");
			lua_newtable(L);
			for (;pAttr;pAttr = pAttr->Next()) {
				lua_pushstring(L,pAttr->Name());
				lua_pushstring(L,pAttr->Value());
				//lua_setfield(L,-2,pAttr->Name());
				lua_settable(L,-3);
				
			}
			//lua_setfield(L,-2,"attr");
			lua_settable(L,-3);
		}
	}
	
	// children
	TiXmlNode *pChild = pNode->FirstChild();
	if (pChild) {
		int iChildCount = 0;
		for(;pChild;pChild = pChild->NextSibling()) {
			switch (pChild->Type()) {
				case TiXmlNode::DOCUMENT: break;
				case TiXmlNode::ELEMENT: 
					// normal element, parse recursive
					lua_newtable(L);
					LuaXML_ParseNode(L,pChild);
					lua_rawseti(L,-2,++iChildCount);
				break;
				case TiXmlNode::COMMENT: break;
				case TiXmlNode::TEXT: 
					// plaintext, push raw
					lua_pushstring(L,pChild->Value());
					lua_rawseti(L,-2,++iChildCount);
				break;
				case TiXmlNode::DECLARATION: break;
				case TiXmlNode::UNKNOWN: break;
			};
		}
		lua_pushstring(L,"n");
		lua_pushnumber(L,iChildCount);
		//lua_setfield(L,-2,"n");
		lua_settable(L,-3);
	}
}

static int LuaXML_ParseFile (lua_State *L) { PROFILE
	const char* sFileName = luaL_checkstring(L,1);
	TiXmlDocument doc(sFileName);
	doc.LoadFile();
	lua_newtable(L);
	LuaXML_ParseNode(L,&doc);
	return 1;
}

static int LuaXML_ParseString (lua_State *L) { PROFILE
	const char* sString = luaL_checkstring(L,1);
	TiXmlDocument doc;
	doc.Parse(sString,0,TIXML_DEFAULT_ENCODING);
	lua_newtable(L);
	LuaXML_ParseNode(L,&doc);
	return 1;
}

void	RegisterLuaXML (lua_State *L) {
	lua_register(L,"LuaXML_ParseFile",LuaXML_ParseFile);
	lua_register(L,"LuaXML_ParseString",LuaXML_ParseString);
}

};
