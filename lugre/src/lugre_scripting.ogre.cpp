#include "lugre_prefix.h"
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "lugre_net.h"
#include "lugre_fifo.h"
#include "lugre_game.h"
#include "lugre_listener.h"
#include "lugre_scripting.h"
#include "lugre_input.h"
#include "lugre_robstring.h"
#include "lugre_gfx3D.h"
#include "lugre_gfx2D.h"
#include "lugre_widget.h"
#include "lugre_luabind.h"
#include "lugre_shell.h"
#include "lugre_timer.h"
#include "lugre_ogrewrapper.h"
#include "lugre_bitmask.h"
#include "lugre_camera.h"
#include "lugre_viewport.h"
#include "lugre_rendertexture.h"
#include "lugre_sound.h"
#include <Ogre.h>
#include <OgreResourceManager.h>
#include <OgreFontManager.h>
#include <OgreTextAreaOverlayElement.h>
#include <OgreMeshSerializer.h>
#include <OgreCompositorManager.h>
#include "lugre_luaxml.h"
#include "lugre_meshshape.h"



using namespace Lugre;

	
namespace Lugre {
	void	DisplayNotice			(const char* szMsg); ///< defined in main.cpp, OS-specific
	void	DisplayErrorMessage		(const char* szMsg); ///< defined in main.cpp, OS-specific
	void	Material_LuaRegister	(void *L);
	void	Beam_LuaRegister		(void *L);
	void	PrintLuaStackTrace		();
	void	ProfileDumpCallCount	(); ///< defined in profile.cpp, only does something if PROFILE_CALLCOUNT is enabled
	void	OgreForceCloseFullscreen ();
};

/// string	CloneMesh (meshname)
static int l_CloneMesh (lua_State *L) { PROFILE
	std::string sOldMeshName 	= luaL_checkstring(L,1);
	std::string sNewMeshName 	= cOgreWrapper::GetSingleton().GetUniqueName();
	Ogre::MeshPtr mesh = Ogre::MeshManager::getSingleton().load(sOldMeshName,Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	if (mesh.isNull()) return 0;
	mesh->clone(sNewMeshName,Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	lua_pushstring(L,sNewMeshName.c_str());
	return 1;
}


/// only call this once at startup
static int l_InitOgre (lua_State *L) { PROFILE
	std::string sWindowTitle 	= (lua_gettop(L) >= 1 && !lua_isnil(L,1)) ? luaL_checkstring(L,1) : "Lugre";
	std::string sOgrePluginPath	= (lua_gettop(L) >= 2 && !lua_isnil(L,2)) ? luaL_checkstring(L,2) : "/usr/local/lib/OGRE";
	std::string sOgreBaseDir	= (lua_gettop(L) >= 3 && !lua_isnil(L,3)) ? luaL_checkstring(L,3) : "./";
	lua_pushboolean(L,cOgreWrapper::GetSingleton().Init(sWindowTitle.c_str(),sOgrePluginPath.c_str(),sOgreBaseDir.c_str()));
	return 1;
}

/// for lua :	ang,x,y,z 	  QuaternionToAngleAxis	(qw,qx,qy,qz)
static int					l_QuaternionToAngleAxis (lua_State *L) { PROFILE
	static	Ogre::Radian	angle;
	static	Vector3			axis;
	Ogre::Quaternion(	luaL_checknumber(L,1),
						luaL_checknumber(L,2),
						luaL_checknumber(L,3),
						luaL_checknumber(L,4)).ToAngleAxis(angle,axis);
	lua_pushnumber(L,angle.valueRadians());
	lua_pushnumber(L,axis.x);
	lua_pushnumber(L,axis.y);
	lua_pushnumber(L,axis.z);
	return 4;
}

/// for lua :	w,x,y,z 	  QuaternionSlerp	(qw,qx,qy,qz, pw,px,py,pz, t, bShortestPath=true)
static int 					l_QuaternionSlerp	(lua_State *L) { PROFILE
	static	Ogre::Radian	angle;
	static	Vector3			axis;
	Ogre::Quaternion q(	luaL_checknumber(L,1),luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4));
	Ogre::Quaternion p(	luaL_checknumber(L,5),luaL_checknumber(L,6),luaL_checknumber(L,7),luaL_checknumber(L,8));
	float t = luaL_checknumber(L,9);
	bool bShortestPath = (lua_gettop(L) >= 10 && !lua_isnil(L,10)) ? (lua_isboolean(L,10) ? lua_toboolean(L,10) : luaL_checkint(L,10)) : true;
	Ogre::Quaternion m = Ogre::Quaternion::Slerp(t,p,q,bShortestPath);
	lua_pushnumber(L,m.w);
	lua_pushnumber(L,m.x);
	lua_pushnumber(L,m.y);
	lua_pushnumber(L,m.z);
	return 4;
}



/// void OgreAddCompositor(compositor script name)
static int l_OgreAddCompositor (lua_State *L) { PROFILE
	Ogre::Viewport* pViewport = cLuaBind<cViewport>::checkudata_alive(L,1)->mpViewport;
//	printf("pViewport=%08x\n",pViewport);
	if (pViewport)
	{
		const char *name = luaL_checkstring(L,2);
		Ogre::CompositorManager::getSingleton().addCompositor(pViewport, name);
		Ogre::CompositorManager::getSingleton().setCompositorEnabled(pViewport, name, true);
	}
	else
	{
		return 0;
	}
	return 0;
}

/// void OgreRemoveCompositor(compositor script name)
static int l_OgreRemoveCompositor (lua_State *L) { PROFILE
	Ogre::Viewport* pViewport = cLuaBind<cViewport>::checkudata_alive(L,1)->mpViewport;
	if (pViewport)
	{
		const char *name = luaL_checkstring(L,2);
		Ogre::CompositorManager::getSingleton().setCompositorEnabled(pViewport, name, false);
		Ogre::CompositorManager::getSingleton().removeCompositor(pViewport, name);
	}
	else
	{
		return 0;
	}
	return 0;
}

/// int = OgreMemoryUsage(part)
/// part in {compositor,font,gpuprogram,highlevelgpuprogram,material,mesh,skeleton,texture,all}
/// returns memory usage in byte
static int l_OgreMemoryUsage (lua_State *L) { PROFILE
	std::string part(luaL_checkstring(L,1));
	size_t mem = 0;
	
#ifdef OGRE_VERSION_SUFFIX
	if(part.find("compositor") != std::string::npos || part.find("all") != std::string::npos)mem += Ogre::CompositorManager::getSingleton().getMemoryUsage();
	if(part.find("font") != std::string::npos || part.find("all") != std::string::npos)mem += Ogre::FontManager::getSingleton().getMemoryUsage();
	if(part.find("gpuprogram") != std::string::npos || part.find("all") != std::string::npos)mem += Ogre::GpuProgramManager::getSingleton().getMemoryUsage();
	if(part.find("highlevelgpuprogram") != std::string::npos || part.find("all") != std::string::npos)mem += Ogre::HighLevelGpuProgramManager::getSingleton().getMemoryUsage();
	if(part.find("material") != std::string::npos || part.find("all") != std::string::npos)mem += Ogre::MaterialManager::getSingleton().getMemoryUsage();
	if(part.find("mesh") != std::string::npos || part.find("all") != std::string::npos)mem += Ogre::MeshManager::getSingleton().getMemoryUsage();
	if(part.find("skeleton") != std::string::npos || part.find("all") != std::string::npos)mem += Ogre::SkeletonManager::getSingleton().getMemoryUsage();
	if(part.find("texture") != std::string::npos || part.find("all") != std::string::npos)mem += Ogre::TextureManager::getSingleton().getMemoryUsage();
#endif
	
	lua_pushnumber(L, mem);
	return 1;
}

/// bool = OgreMeshAvailable(resourcename)
static int l_OgreMeshAvailable (lua_State *L) { PROFILE
	const char *name = luaL_checkstring(L,1);
	bool ret;
	
	try {
		Ogre::MeshManager::getSingleton().load(name,Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		ret = true;
	} catch (...) {
			ret = false;
	}
	
	lua_pushboolean(L, ret);
	return 1;
}

/// see also OgreMaterialAvailable below
/// bool = OgreMaterialNameKnown(resourcename)
/// returns false if name is empty string or nil
static int l_OgreMaterialNameKnown (lua_State *L) { PROFILE
	std::string sMatName = (lua_gettop(L) >= 1 && !lua_isnil(L,1)) ? luaL_checkstring(L,1) : "";
	if (sMatName.size() > 0) {
		Ogre::MaterialPtr pMaterial = Ogre::MaterialManager::getSingleton().getByName(sMatName.c_str());
		lua_pushboolean(L,!pMaterial.isNull());
	} else {
		lua_pushboolean(L,false);
	}
	return 1;
}



/// bool = OgreMaterialAvailable(resourcename)
static int l_OgreMaterialAvailable (lua_State *L) { PROFILE
	assert(0 && "DON'T USE ME, ALWAYS RETURNS TRUE");
	// TODO, this code does not work, use l_OgreMaterialNameKnown  above
	const char *name = luaL_checkstring(L,1);
	bool ret;
	
	try {
		Ogre::MaterialManager::getSingleton().load(name,Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		ret = true;
	} catch (...) {
			ret = false;
	}
	
	lua_pushboolean(L, ret);
	return 1;
}

/// bool = OgreTextureAvailable(resourcename)
static int l_OgreTextureAvailable (lua_State *L) { PROFILE
	const char *name = luaL_checkstring(L,1);
	bool ret;
	
	try {
		Ogre::TextureManager::getSingleton().load(name,Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		ret = true;
	} catch (...) {
			ret = false;
	}
	
	lua_pushboolean(L, ret);
	return 1;
}



static int l_Client_SetSkybox (lua_State *L) { PROFILE
	cOgreWrapper::GetSingleton().SetSkybox( (lua_gettop(L) > 0 && !lua_isnil(L,1)) ? luaL_checkstring(L, 1) : 0 , true );
	return 0;
}

static int l_Client_SetFog (lua_State *L) { PROFILE
	int i=0;
	int numargs=lua_gettop(L);
	int iFogMode 			= (numargs > i && !lua_isnil(L,i+1)) ? luaL_checkint(L, ++i) : 0;
	Ogre::Real r 			= (numargs > i && !lua_isnil(L,i+1)) ? luaL_checknumber(L, ++i) : 1;
	Ogre::Real g 			= (numargs > i && !lua_isnil(L,i+1)) ? luaL_checknumber(L, ++i) : 1;
	Ogre::Real b 			= (numargs > i && !lua_isnil(L,i+1)) ? luaL_checknumber(L, ++i) : 1;
	Ogre::Real a 			= (numargs > i && !lua_isnil(L,i+1)) ? luaL_checknumber(L, ++i) : 1;
	Ogre::Real expDensity 	= (numargs > i && !lua_isnil(L,i+1)) ? luaL_checknumber(L, ++i) : 0.001;
	Ogre::Real linearStart 	= (numargs > i && !lua_isnil(L,i+1)) ? luaL_checknumber(L, ++i) : 0.0;
	Ogre::Real linearEnd 	= (numargs > i && !lua_isnil(L,i+1)) ? luaL_checknumber(L, ++i) : 1.0;
	/*
	void 	setFog (FogMode mode=FOG_NONE, const ColourValue &colour=ColourValue::White, 
					Real expDensity=0.001, Real linearStart=0.0, Real linearEnd=1.0)
    0=FOG_NONE 	No fog. Duh.
    1=FOG_EXP 	Fog density increases exponentially from the camera (fog = 1/e^(distance * density)).
    2=FOG_EXP2 	Fog density increases at the square of FOG_EXP, i.e. even quicker (fog = 1/e^(distance * density)^2).
    3=FOG_LINEAR 	Fog density increases linearly between the start and end distances.
	*/
	Ogre::FogMode      myFogMode = Ogre::FOG_NONE;
	if (iFogMode == 1) myFogMode = Ogre::FOG_EXP;
	if (iFogMode == 2) myFogMode = Ogre::FOG_EXP2;
	if (iFogMode == 3) myFogMode = Ogre::FOG_LINEAR;
	cOgreWrapper::GetSingleton().mSceneMgr->setFog(myFogMode,Ogre::ColourValue(r,g,b,a),expDensity,linearStart,linearEnd);
	return 0;
}

static int l_Client_RenderOneFrame (lua_State *L) { PROFILE
	cGame::GetSingleton().RenderOneFrame();
	return 0;
}



/// for lua : void	Client_SetAmbientLight	(r,g,b,a,scenemgr=main)
static int l_Client_SetAmbientLight (lua_State *L) { PROFILE
	std::string sSceneMgrName 	= (lua_gettop(L) >= 5 && !lua_isnil(L,5)) ? luaL_checkstring(L,5) : "main";
	Ogre::SceneManager* pSceneMgr = cOgreWrapper::GetSingleton().GetSceneManager(sSceneMgrName.c_str());
	if (pSceneMgr) pSceneMgr->setAmbientLight(Ogre::ColourValue(luaL_checknumber(L,1),luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4)));
	return 0;
}

/// for lua : void	Client_ClearLights	(scenemgr=main)
static int l_Client_ClearLights (lua_State *L) { PROFILE
	std::string sSceneMgrName 	= (lua_gettop(L) >= 1 && !lua_isnil(L,1)) ? luaL_checkstring(L,1) : "main";
	Ogre::SceneManager* pSceneMgr = cOgreWrapper::GetSingleton().GetSceneManager(sSceneMgrName.c_str());
	if (pSceneMgr) pSceneMgr->destroyAllLights();
	return 0;
}

/// for lua : string l_Client_AddPointLight(x,y,z)	-- x,y,z position
static int l_Client_AddPointLight (lua_State *L) { PROFILE
	Ogre::SceneManager* pSceneMgr = cOgreWrapper::GetSingleton().mSceneMgr;
	if (!pSceneMgr) return 0;
	std::string sName = cOgreWrapper::GetSingleton().GetUniqueName();
	Ogre::Light* pLight = pSceneMgr->createLight( sName );
	pLight->setType( Ogre::Light::LT_POINT );
	pLight->setPosition(luaL_checknumber(L,1),luaL_checknumber(L,2),luaL_checknumber(L,3));
	if (lua_gettop(L) >= 4) pLight->setDiffuseColour(luaL_checknumber(L,4),luaL_checknumber(L,5),luaL_checknumber(L,6));
	if (lua_gettop(L) >= 7) pLight->setSpecularColour(luaL_checknumber(L,7),luaL_checknumber(L,8),luaL_checknumber(L,9));
	if (lua_gettop(L) >= 10) pLight->setAttenuation(luaL_checknumber(L,10),luaL_checknumber(L,11),luaL_checknumber(L,12),luaL_checknumber(L,13));
	pLight->setCastShadows(true);
	lua_pushstring(L,sName.c_str());
	return 1;
}

/// for lua : string Client_AddDirectionalLight(x,y,z,scenemgr=main)	-- x,y,z direction
static int l_Client_AddDirectionalLight (lua_State *L) { PROFILE
	std::string sSceneMgrName 	= (lua_gettop(L) >= 4 && !lua_isnil(L,4)) ? luaL_checkstring(L,4) : "main";
	Ogre::SceneManager* pSceneMgr = cOgreWrapper::GetSingleton().GetSceneManager(sSceneMgrName.c_str());
	if (!pSceneMgr) return 0;
	std::string sName = cOgreWrapper::GetSingleton().GetUniqueName();
	Ogre::Light* pLight = pSceneMgr->createLight( sName );
	pLight->setType( Ogre::Light::LT_DIRECTIONAL );
	pLight->setDirection(luaL_checknumber(L,1),luaL_checknumber(L,2),luaL_checknumber(L,3));
	lua_pushstring(L,sName.c_str());
	return 1;
}

/// for lua : void Client_SetLightPosition(name,x,y,z)	-- name lightname, x,y,z position
static int l_Client_SetLightPosition (lua_State *L) { PROFILE
	Ogre::SceneManager* pSceneMgr = cOgreWrapper::GetSingleton().mSceneMgr;
	const char *name = luaL_checkstring(L,1);
	Ogre::Light* pLight = pSceneMgr->getLight( name );
	
	if(pLight){
		pLight->setPosition(luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4));
	}
	return 0;
}

/// for lua : void Client_SetLightDirection(name,x,y,z)	-- name lightname, x,y,z direction
static int l_Client_SetLightDirection (lua_State *L) { PROFILE
	Ogre::SceneManager* pSceneMgr = cOgreWrapper::GetSingleton().mSceneMgr;
	const char *name = luaL_checkstring(L,1);
	Ogre::Light* pLight = pSceneMgr->getLight( name );
	
	if(pLight){
		pLight->setDirection(luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4));
	}
	return 0;
}

/// for lua : void Client_RemoveLight(name)	-- name lightname
static int l_Client_RemoveLight (lua_State *L) { PROFILE
	Ogre::SceneManager* pSceneMgr = cOgreWrapper::GetSingleton().mSceneMgr;
	const char *name = luaL_checkstring(L,1);
	pSceneMgr->destroyLight( name );
	return 0;
}

static int l_Client_DeleteLight (lua_State *L) { PROFILE
	Ogre::SceneManager* pSceneMgr = cOgreWrapper::GetSingleton().mSceneMgr;
	if (pSceneMgr) pSceneMgr->destroyLight(luaL_checkstring(L,1));
	return 0;
}

/// for lua : void Client_SetLightSpecularColor(name,r,g,b)	-- name lightname, r,g,b spec color
static int l_Client_SetLightSpecularColor (lua_State *L) { PROFILE
	Ogre::SceneManager* pSceneMgr = cOgreWrapper::GetSingleton().mSceneMgr;
	const char *name = luaL_checkstring(L,1);
	Ogre::Light* pLight = pSceneMgr->getLight( name );
	
	if(pLight){
		pLight->setSpecularColour(luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4));
	}
	return 0;
}


/// for lua : void Client_SetLightPowerScale(name,fPowerScale) : used for HDR rendering, in shaders
static int l_Client_SetLightPowerScale (lua_State *L) { PROFILE
	Ogre::Light* pLight = cOgreWrapper::GetSingleton().mSceneMgr->getLight( luaL_checkstring(L,1) );
	if(pLight) pLight->setPowerScale(luaL_checknumber(L,2));
	return 0;
}

/// for lua : void Client_SetLightDiffuseColor(name,r,g,b)	-- name lightname, r,g,b diffuse color
static int l_Client_SetLightDiffuseColor (lua_State *L) { PROFILE
	Ogre::SceneManager* pSceneMgr = cOgreWrapper::GetSingleton().mSceneMgr;
	const char *name = luaL_checkstring(L,1);
	Ogre::Light* pLight = pSceneMgr->getLight( name );
	
	if(pLight){
		pLight->setDiffuseColour(luaL_checknumber(L,2),luaL_checknumber(L,3),luaL_checknumber(L,4));
	}
	return 0;
}

/// for lua : void		Client_TakeGridScreenshot (sPrefix="screenshots/")
static int			  l_Client_TakeGridScreenshot (lua_State *L) { PROFILE
	std::string sPrefix = (lua_gettop(L) >= 1 && !lua_isnil(L,1)) ? luaL_checkstring(L,1) : "screenshots/";
	std::string filename = strprintf( "%shighres_%d",sPrefix.c_str(), cShell::GetTicks() );
	std::string ext = ".jpg";
	cOgreWrapper::GetSingleton().TakeGridScreenshot(3,filename,ext,true);
	return 0;
}

/// for lua : void	Client_TakeScreenshot (sPrefix="screenshots/")
static int 		  l_Client_TakeScreenshot (lua_State *L) { PROFILE
	std::string sPrefix = (lua_gettop(L) >= 1 && !lua_isnil(L,1)) ? luaL_checkstring(L,1) : "screenshots/";
	cOgreWrapper::GetSingleton().TakeScreenshot(sPrefix.c_str());
	return 0;
}

/// shows ogre config dialog
static int l_Client_ShowOgreConfig (lua_State *L) { PROFILE
	bool bIsFullscreen = cOgreWrapper::GetSingleton().mWindow->isFullScreen();
	printf("Client_ShowOgreConfig fullscreen=%d\n",bIsFullscreen);
	bIsFullscreen = true; // detection fails in linux ?
	if (bIsFullscreen) {
		// hide window to make config window visible in fullscreen mode, evil hack since this is not supported by ogre
		OgreForceCloseFullscreen();
	}
	lua_pushboolean(L,cOgreWrapper::GetSingleton().mRoot->showConfigDialog());
	if (bIsFullscreen) cShell::mbAlive = false;
	// the application shoudl terminate after this
	// terminates the game if changes were made
	return 1;
}

/// for lua : dist = TriangleRayPick(ax,ay,az, bx,by,bz, cx,cy,cz, rx,ry,rz, rvx,rvy,rvz)  
/// mainly for mousepicking, dist=nil if not hit
static int l_TriangleRayPick (lua_State *L) { PROFILE
	// don't use ++i or something here, the compiler might mix the order
	Ogre::Vector3 	a(		luaL_checknumber(L,1),luaL_checknumber(L,2),luaL_checknumber(L,3));
	Ogre::Vector3 	b(		luaL_checknumber(L,4),luaL_checknumber(L,5),luaL_checknumber(L,6));
	Ogre::Vector3 	c(		luaL_checknumber(L,7),luaL_checknumber(L,8),luaL_checknumber(L,9));
	Ogre::Vector3	vRayPos(luaL_checknumber(L,10),luaL_checknumber(L,11),luaL_checknumber(L,12));
	Ogre::Vector3	vRayDir(luaL_checknumber(L,13),luaL_checknumber(L,14),luaL_checknumber(L,15));
	float myHitDist;
	if (!IntersectRayTriangle(vRayPos,vRayDir,a,b,c,&myHitDist)) return 0;
	lua_pushnumber(L,myHitDist);
	return 1;
}



/// for lua : dist = SphereRayPick(x,y,z,rad,rx,ry,rz,rvx,rvy,rvz)  -- mainly for mousepicking, dist=nil if not hit
static int l_SphereRayPick (lua_State *L) { PROFILE
	// don't use ++i or something here, the compiler might mix the order
	Ogre::Vector3 	vSpherePos(	luaL_checknumber(L,1),luaL_checknumber(L,2),luaL_checknumber(L,3));
	float			fSphereRad = luaL_checknumber(L,4);
	Ogre::Vector3	vRayPos(	luaL_checknumber(L,5),luaL_checknumber(L,6),luaL_checknumber(L,7));
	Ogre::Vector3	vRayDir(	luaL_checknumber(L,8),luaL_checknumber(L,9),luaL_checknumber(L,10));
	
	std::pair<bool, Real> hit = Ogre::Ray(vRayPos,vRayDir).intersects(Ogre::Sphere(vSpherePos,fSphereRad));
	if (!hit.first) return 0;
	lua_pushnumber(L,hit.second);
	return 1;
}

/// for lua : dist =  PlaneRayPick (x,y,z,nx,ny,nz,rx,ry,rz,rvx,rvy,rvz)  -- mainly for mousepicking, dist=nil if not hit
static int 			l_PlaneRayPick (lua_State *L) { PROFILE
	// don't use ++i or something here, the compiler might mix the order
	Ogre::Vector3 	vPlanePos(		luaL_checknumber(L,1),luaL_checknumber(L,2),luaL_checknumber(L,3));
	Ogre::Vector3 	vPlaneNormal(	luaL_checknumber(L,4),luaL_checknumber(L,5),luaL_checknumber(L,6));
	Ogre::Vector3	vRayPos(		luaL_checknumber(L,7),luaL_checknumber(L,8),luaL_checknumber(L,9));
	Ogre::Vector3	vRayDir(		luaL_checknumber(L,10),luaL_checknumber(L,11),luaL_checknumber(L,12));
	
	/*printf("c++:PlaneRayPick(%0.2f,%0.2f,%0.2f, %0.2f,%0.2f,%0.2f, %0.2f,%0.2f,%0.2f, %0.2f,%0.2f,%0.2f)\n",
		vPlanePos.x,vPlanePos.y,vPlanePos.z,
		vPlaneNormal.x,vPlaneNormal.y,vPlaneNormal.z,
		vRayPos.x,vRayPos.y,vRayPos.z,
		vRayDir.x,vRayDir.y,vRayDir.z
		);*/
	std::pair<bool, Real> hit = Ogre::Ray(vRayPos,vRayDir).intersects(Ogre::Plane(vPlaneNormal,vPlanePos));
	if (!hit.first) return 0;
	lua_pushnumber(L,hit.second);
	return 1;
}


static int l_UnloadMeshName (lua_State *L) { PROFILE
	const char* szMeshName = luaL_checkstring(L,1);
	Ogre::MeshPtr mesh = Ogre::MeshManager::getSingleton().load(szMeshName,Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	if (mesh.isNull()) return 0;
	mesh->unload();
	UnloadMeshShape(szMeshName);
	//Ogre::MeshManager::getSingleton().unload(luaL_checkstring(L,1));
	return 0;
}

static int l_CountMeshTriangles (lua_State *L) { PROFILE
	Ogre::MeshPtr pMesh = Ogre::MeshManager::getSingleton().load(luaL_checkstring(L,1),
					Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME );
	int res = 0;
	for (int i=0;i<pMesh->getNumSubMeshes();++i) {
		Ogre::SubMesh *pSub = pMesh->getSubMesh(i);
		if (pSub && pSub->indexData) res += pSub->indexData->indexCount / 3;
	}
	lua_pushnumber(L,res);
	return 1;
}

/// for lua : 	x1,y1,z1,x2,y2,z2	MeshGetBounds	(meshname)
static int l_MeshGetBounds (lua_State *L) { PROFILE
	Ogre::MeshPtr pMesh = Ogre::MeshManager::getSingleton().load(luaL_checkstring(L,1),
					Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME );
	if (pMesh.isNull()) return 0;
	const Ogre::AxisAlignedBox& mybounds = pMesh->getBounds();
	lua_pushnumber(L,mybounds.getMinimum().x);
	lua_pushnumber(L,mybounds.getMinimum().y);
	lua_pushnumber(L,mybounds.getMinimum().z);
	lua_pushnumber(L,mybounds.getMaximum().x);
	lua_pushnumber(L,mybounds.getMaximum().y);
	lua_pushnumber(L,mybounds.getMaximum().z);
	return 6;
}

/// for lua : 	void	MeshSetBounds	(meshname,x1,y1,z1,x2,y2,z2)
static int l_MeshSetBounds (lua_State *L) { PROFILE
	Ogre::MeshPtr pMesh = Ogre::MeshManager::getSingleton().load(luaL_checkstring(L,1),
					Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME );
	if (!pMesh.isNull()) {
		pMesh->_setBounds(Ogre::AxisAlignedBox(	mymin(luaL_checknumber(L,2),luaL_checknumber(L,5)),
												mymin(luaL_checknumber(L,3),luaL_checknumber(L,6)),
												mymin(luaL_checknumber(L,4),luaL_checknumber(L,7)),
												mymax(luaL_checknumber(L,2),luaL_checknumber(L,5)),
												mymax(luaL_checknumber(L,3),luaL_checknumber(L,6)),
												mymax(luaL_checknumber(L,4),luaL_checknumber(L,7))
												));
	}
	return 0;
}

/// for lua : 	float	MeshGetBoundRad	(meshname)
static int l_MeshGetBoundRad (lua_State *L) { PROFILE
	Ogre::MeshPtr pMesh = Ogre::MeshManager::getSingleton().load(luaL_checkstring(L,1),
					Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME );
	if (pMesh.isNull()) return 0;
	lua_pushnumber(L,pMesh->getBoundingSphereRadius());
	return 1;
}

/// for lua : 	void	MeshSetBoundRad	(meshname,boundrad)
static int l_MeshSetBoundRad (lua_State *L) { PROFILE
	Ogre::MeshPtr pMesh = Ogre::MeshManager::getSingleton().load(luaL_checkstring(L,1),
					Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME );
	if (!pMesh.isNull()) pMesh->_setBoundingSphereRadius(luaL_checknumber(L,2));
	return 0;
}



/// for lua :   void  ExportMesh  (meshname,filename)
static int l_ExportMesh		(lua_State *L) { PROFILE 
	Ogre::MeshPtr pMesh = Ogre::MeshManager::getSingleton().load(luaL_checkstring(L,1),
					// autodetect group location
					//Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
					Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME );
	Ogre::MeshSerializer myExporter;
	//Ogre::Mesh* pMesh = pEntity->getMesh().get();
	myExporter.exportMesh(pMesh.get(),luaL_checkstring(L,2)); 
	return 0;
}

/// see my_lugre_transform_mesh.cpp
void	TransformMesh	(Ogre::Mesh* pMesh,const Ogre::Vector3& vMove,const Ogre::Vector3& vScale,const Ogre::Quaternion& qRot);

/// applies reposition, rescale and rotation to mesh vertex data
/// for lua :   void  TransformMesh  (meshname, x,y,z, sx,sx,sz, qw,qx,qy,qz)
static int l_TransformMesh		(lua_State *L) { PROFILE 
	Ogre::MeshPtr pMesh = Ogre::MeshManager::getSingleton().load(luaL_checkstring(L,1),
					// autodetect group location
					//Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
					Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME );
	
	Ogre::Vector3 vMove(	luaL_checknumber(L,2),
							luaL_checknumber(L,3),
							luaL_checknumber(L,4));
	Ogre::Vector3 vScale(	luaL_checknumber(L,5),
							luaL_checknumber(L,6),
							luaL_checknumber(L,7));
	Ogre::Quaternion qRot(	luaL_checknumber(L,8),
							luaL_checknumber(L,9),
							luaL_checknumber(L,10),
							luaL_checknumber(L,11));
	TransformMesh(pMesh.get(),vMove,vScale,qRot);
	
	//Ogre::MeshSerializer myExporter;
	//Ogre::Mesh* pMesh = pEntity->getMesh().get();   pMesh.get()
	return 0;
}

/// see my_lugre_transform_mesh.cpp
void	MeshReadOutExactBounds	(Ogre::Mesh* pMesh,Ogre::Vector3& vMin,Ogre::Vector3& vMax);

/// for lua : 	x1,y1,z1,x2,y2,z2	MeshReadOutExactBounds	(meshname)
static int 						  l_MeshReadOutExactBounds	(lua_State *L) { PROFILE 
	Ogre::MeshPtr pMesh = Ogre::MeshManager::getSingleton().load(luaL_checkstring(L,1),
					Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME );
	
	Ogre::Vector3 vMin;
	Ogre::Vector3 vMax;
	MeshReadOutExactBounds(pMesh.get(),vMin,vMax);
	lua_pushnumber(L,vMin.x);
	lua_pushnumber(L,vMin.y);
	lua_pushnumber(L,vMin.z);
	lua_pushnumber(L,vMax.x);
	lua_pushnumber(L,vMax.y);
	lua_pushnumber(L,vMax.z);
	return 6;
}


/// for lua :   x,y,z,vx,vy,vz  GetScreenRay  (x,y) x,y in [0,1]
static int l_GetScreenRay		(lua_State *L) { PROFILE 
	cOgreWrapper& ogrewrapper = cOgreWrapper::GetSingleton();
	Ogre::Ray myray(ogrewrapper.mCamera->getCameraToViewportRay(luaL_checknumber(L,1),luaL_checknumber(L,2)));
	lua_pushnumber(L,myray.getOrigin().x);
	lua_pushnumber(L,myray.getOrigin().y);
	lua_pushnumber(L,myray.getOrigin().z);
	lua_pushnumber(L,myray.getDirection().x);
	lua_pushnumber(L,myray.getDirection().y);
	lua_pushnumber(L,myray.getDirection().z);
	return 6;
}

/// for lua :   z  GetMaxZ  ()
static int 		l_GetMaxZ		(lua_State *L) { PROFILE 
	lua_pushnumber(L,Ogre::Root::getSingleton().getRenderSystem()->getMaximumDepthInputValue());
	return 1;
}


/// for lua :   bIsInFront,px,py	  ProjectPos	(x,y,z)
static int							l_ProjectPos	(lua_State *L) { PROFILE 
	bool 		bIsInFront;
	Ogre::Real	fX,fY;
	bIsInFront = cOgreWrapper::GetSingleton().ProjectPos(
		Ogre::Vector3(luaL_checknumber(L,1),luaL_checknumber(L,2),luaL_checknumber(L,3)),fX,fY);
	lua_pushboolean(L,bIsInFront);
	lua_pushnumber(L,fX);
	lua_pushnumber(L,fY);
	return 3;
}

/// for lua :   bIsInFront,px,py,cx,cy	  ProjectSizeAndPos	(x,y,z,r)
static int 								l_ProjectSizeAndPos	(lua_State *L) { PROFILE 
	bool 		bIsInFront;
	Ogre::Real	fX,fY,fCX,fCY;
	bIsInFront = cOgreWrapper::GetSingleton().ProjectSizeAndPos(
		Ogre::Vector3(luaL_checknumber(L,1),luaL_checknumber(L,2),luaL_checknumber(L,3)),fX,fY,luaL_checknumber(L,4),fCX,fCY);
	lua_pushboolean(L,bIsInFront);
	lua_pushnumber(L,fX);
	lua_pushnumber(L,fY);
	lua_pushnumber(L,fCX);
	lua_pushnumber(L,fCY);
	return 5;
}

/// for lua :   px,py,pz,cx,cy,cz	  ProjectSizeAndPosEx	(x,y,z,r)
static int 							l_ProjectSizeAndPosEx	(lua_State *L) { PROFILE 
	Ogre::Vector3 s;
	Ogre::Vector3 p = cOgreWrapper::GetSingleton().ProjectSizeAndPosEx(
		Ogre::Vector3(luaL_checknumber(L,1),luaL_checknumber(L,2),luaL_checknumber(L,3)),luaL_checknumber(L,4),s);
	lua_pushnumber(L,p.x);
	lua_pushnumber(L,p.y);
	lua_pushnumber(L,p.z);
	lua_pushnumber(L,s.x);
	lua_pushnumber(L,s.y);
	lua_pushnumber(L,s.z);
	return 6;
}


/// for lua :   void  CreateSceneManager  (sSceneManagerName)
static int l_CreateSceneManager		(lua_State *L) { PROFILE  // TODO : move to seperate file ?
	cOgreWrapper::GetSingleton().mRoot->createSceneManager(Ogre::ST_GENERIC,luaL_checkstring(L,1));
	return 0;
}

/// for lua :   table[id=texname...]  OgreMeshTextures  (meshfile)
static int l_OgreMeshTextures	(lua_State *L) { PROFILE  // TODO : move to seperate file ?
	Ogre::MeshSerializer* meshSerializer = new Ogre::MeshSerializer();
	const char *szMeshName = luaL_checkstring(L,1);
	
	lua_newtable(L);
		
	//printf("open file: %s\n",szMeshName);
	// model file
	std::ifstream ifs;
	ifs.open(szMeshName, std::ios_base::in | std::ios_base::binary);
	Ogre::DataStreamPtr stream(new Ogre::FileStreamDataStream(&ifs, false));

	if(ifs.is_open()){
		//printf("create tmp mesh\n");
		// create tmp mesh import resource
		Ogre::MeshPtr mesh = Ogre::MeshManager::getSingleton().create("l_OgreMeshTextureMissing_conversion", 
			Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

		//printf("import\n");
		// import
		meshSerializer->importMesh(stream, mesh.getPointer());
		
		if(!mesh.isNull()){
			// iterator over submeshes
			Ogre::Mesh::SubMeshIterator it = mesh->getSubMeshIterator();
			int i = 1;
			while(it.hasMoreElements()){
				Ogre::SubMesh *submesh = it.getNext();
				std::string tex = submesh->getMaterialName();
				//printf("material found: %s\n",tex.c_str());
				lua_pushstring(L,tex.c_str()); lua_rawseti(L,-2,i);
				++i;
			}
		}
		
		// remove all stuff
		Ogre::MeshManager::getSingleton().remove("l_OgreMeshTextureMissing_conversion");
		
		ifs.close();
	} else {
		printf("ERROR can't open file: %s\n",szMeshName);
	}

	delete meshSerializer;
		
	return 1;
}


/// for lua :   void OgreShadowTechnique  (string techique)
static int l_OgreShadowTechnique	(lua_State *L) { PROFILE  // TODO : move to seperate file ?
	const char *tech = luaL_checkstring(L,1);
	Ogre::SceneManager *p = cOgreWrapper::GetSingleton().mSceneMgr;
	
	if(p){
		if(strcmp(tech,"stencil_modulative") == 0)p->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_MODULATIVE);
		else if(strcmp(tech,"stencil_additive") == 0)p->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
		else if(strcmp(tech,"texture_modulative") == 0)p->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_MODULATIVE);
		else if(strcmp(tech,"texture_additive") == 0)p->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_ADDITIVE);
		else if(strcmp(tech,"texture_additive_integrated") == 0)p->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_ADDITIVE_INTEGRATED);
		else if(strcmp(tech,"texture_modulative_integrated") == 0)p->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_MODULATIVE_INTEGRATED);
		else p->setShadowTechnique(Ogre::SHADOWTYPE_NONE);
	}
		
	return 0;
}

/// for lua :   void	  OgreSetShadowTextureSize  (int size)
static int 				l_OgreSetShadowTextureSize	(lua_State *L) { PROFILE
	cOgreWrapper::GetSingleton().mSceneMgr->setShadowTextureSize(luaL_checkint(L,1));
	return 0;
}



/// for lua :   void	  OgreSetShadowFarDistance	(float x)
static int 				l_OgreSetShadowFarDistance	(lua_State *L) { PROFILE
	cOgreWrapper::GetSingleton().mSceneMgr->setShadowFarDistance(luaL_checknumber(L,1));
	return 0;
}

/// for lua :   void	  OgreSetShadowDirLightTextureOffset	(float x)
static int 				l_OgreSetShadowDirLightTextureOffset	(lua_State *L) { PROFILE
	cOgreWrapper::GetSingleton().mSceneMgr->setShadowDirLightTextureOffset(luaL_checknumber(L,1));
	return 0;
}

/// for lua :   void	  OgreSetShadowTextureFadeStart	(float x)
static int 				l_OgreSetShadowTextureFadeStart	(lua_State *L) { PROFILE
	cOgreWrapper::GetSingleton().mSceneMgr->setShadowTextureFadeStart(luaL_checknumber(L,1));
	return 0;
}

/// for lua :   void	  OgreSetShadowTextureFadeEnd	(float x)
static int 				l_OgreSetShadowTextureFadeEnd	(lua_State *L) { PROFILE
	cOgreWrapper::GetSingleton().mSceneMgr->setShadowTextureFadeEnd(luaL_checknumber(L,1));
	return 0;
}

/// for lua :   void	  OgreSetShadowTexturePixelFormat	()
static int 				l_OgreSetShadowTexturePixelFormat	(lua_State *L) { PROFILE
	Ogre::PixelFormat pf = (lua_gettop(L) >= 2 && !lua_isnil(L,1)) ? ((Ogre::PixelFormat)luaL_checkint(L,1)) : Ogre::PF_FLOAT16_R;
	cOgreWrapper::GetSingleton().mSceneMgr->setShadowTexturePixelFormat(pf);
	return 0;
}


/// for lua :   table	  OgrePixelFormatList  ()
static int 				l_OgrePixelFormatList	(lua_State *L) { PROFILE
	lua_newtable(L);
	
	#define OgrePixelFormatList_REGISTER(pf) lua_pushstring(L,#pf); lua_pushnumber(L,(int)Ogre::pf); lua_rawset(L,-3); // k,v,set(L,tableindex)
	OgrePixelFormatList_REGISTER(PF_UNKNOWN)		// 	Unknown pixel format.
	OgrePixelFormatList_REGISTER(PF_L8)				// 		8-bit pixel format, all bits luminace.
	OgrePixelFormatList_REGISTER(PF_BYTE_L)			//
	OgrePixelFormatList_REGISTER(PF_L16)			// 		16-bit pixel format, all bits luminace.
	OgrePixelFormatList_REGISTER(PF_SHORT_L)		//
	OgrePixelFormatList_REGISTER(PF_A8)				// 		8-bit pixel format, all bits alpha.
	OgrePixelFormatList_REGISTER(PF_BYTE_A)			//
	OgrePixelFormatList_REGISTER(PF_A4L4)			// 	8-bit pixel format, 4 bits alpha, 4 bits luminace.
	OgrePixelFormatList_REGISTER(PF_BYTE_LA)		// 	2 byte pixel format, 1 byte luminance, 1 byte alpha
	OgrePixelFormatList_REGISTER(PF_R5G6B5)			// 	16-bit pixel format, 5 bits red, 6 bits green, 5 bits blue.
	OgrePixelFormatList_REGISTER(PF_B5G6R5)			// 	16-bit pixel format, 5 bits red, 6 bits green, 5 bits blue.
	OgrePixelFormatList_REGISTER(PF_R3G3B2)			// 	8-bit pixel format, 2 bits blue, 3 bits green, 3 bits red.
	OgrePixelFormatList_REGISTER(PF_A4R4G4B4)		// 	16-bit pixel format, 4 bits for alpha, red, green and blue.
	OgrePixelFormatList_REGISTER(PF_A1R5G5B5)		// 	16-bit pixel format, 5 bits for blue, green, red and 1 for alpha.
	OgrePixelFormatList_REGISTER(PF_R8G8B8)			// 	24-bit pixel format, 8 bits for red, green and blue.
	OgrePixelFormatList_REGISTER(PF_B8G8R8)			// 	24-bit pixel format, 8 bits for blue, green and red.
	OgrePixelFormatList_REGISTER(PF_A8R8G8B8)		// 	32-bit pixel format, 8 bits for alpha, red, green and blue.
	OgrePixelFormatList_REGISTER(PF_A8B8G8R8)		// 	32-bit pixel format, 8 bits for blue, green, red and alpha.
	OgrePixelFormatList_REGISTER(PF_B8G8R8A8)		// 	32-bit pixel format, 8 bits for blue, green, red and alpha.
	OgrePixelFormatList_REGISTER(PF_R8G8B8A8)		// 	32-bit pixel format, 8 bits for red, green, blue and alpha.
	OgrePixelFormatList_REGISTER(PF_X8R8G8B8)		// 	32-bit pixel format, 8 bits for red, 8 bits for green, 8 bits for blue like PF_A8R8G8B8, but alpha will get discarded
	OgrePixelFormatList_REGISTER(PF_X8B8G8R8)		// 	32-bit pixel format, 8 bits for blue, 8 bits for green, 8 bits for red like PF_A8B8G8R8, but alpha will get discarded
	OgrePixelFormatList_REGISTER(PF_BYTE_RGB)		// 	3 byte pixel format, 1 byte for red, 1 byte for green, 1 byte for blue
	OgrePixelFormatList_REGISTER(PF_BYTE_BGR)		// 	3 byte pixel format, 1 byte for blue, 1 byte for green, 1 byte for red
	OgrePixelFormatList_REGISTER(PF_BYTE_BGRA)		// 	4 byte pixel format, 1 byte for blue, 1 byte for green, 1 byte for red and one byte for alpha
	OgrePixelFormatList_REGISTER(PF_BYTE_RGBA)		// 	4 byte pixel format, 1 byte for red, 1 byte for green, 1 byte for blue, and one byte for alpha
	OgrePixelFormatList_REGISTER(PF_A2R10G10B10)	// 	32-bit pixel format, 2 bits for alpha, 10 bits for red, green and blue.
	OgrePixelFormatList_REGISTER(PF_A2B10G10R10)	// 	32-bit pixel format, 10 bits for blue, green and red, 2 bits for alpha.
	OgrePixelFormatList_REGISTER(PF_DXT1)			// 	DDS (DirectDraw Surface) DXT1 format.
	OgrePixelFormatList_REGISTER(PF_DXT2)			// 	DDS (DirectDraw Surface) DXT2 format.
	OgrePixelFormatList_REGISTER(PF_DXT3)			// 	DDS (DirectDraw Surface) DXT3 format.
	OgrePixelFormatList_REGISTER(PF_DXT4)			// 	DDS (DirectDraw Surface) DXT4 format.
	OgrePixelFormatList_REGISTER(PF_DXT5)			// 	DDS (DirectDraw Surface) DXT5 format.
	OgrePixelFormatList_REGISTER(PF_FLOAT16_R)		//
	OgrePixelFormatList_REGISTER(PF_FLOAT16_RGB)	//
	OgrePixelFormatList_REGISTER(PF_FLOAT16_RGBA)	//
	OgrePixelFormatList_REGISTER(PF_FLOAT32_R)		//
	OgrePixelFormatList_REGISTER(PF_FLOAT32_RGB)	//
	OgrePixelFormatList_REGISTER(PF_FLOAT32_RGBA)	//
	OgrePixelFormatList_REGISTER(PF_FLOAT16_GR)		//
	OgrePixelFormatList_REGISTER(PF_FLOAT32_GR)		//
	OgrePixelFormatList_REGISTER(PF_DEPTH)			//
	OgrePixelFormatList_REGISTER(PF_SHORT_RGBA)		//
	OgrePixelFormatList_REGISTER(PF_SHORT_GR)		//
	OgrePixelFormatList_REGISTER(PF_SHORT_RGB)		//
	OgrePixelFormatList_REGISTER(PF_COUNT)			//
	return 1;
}


/// for lua :   void	  OgreSetShadowTextureSelfShadow  (bool)
static int 				l_OgreSetShadowTextureSelfShadow	(lua_State *L) { PROFILE
	cOgreWrapper::GetSingleton().mSceneMgr->setShadowTextureSelfShadow(lua_toboolean(L,1));
	return 0;
}

/// for lua :   void 	  OgreSetShadowTextureCasterMaterial  (sMatName)
static int 				l_OgreSetShadowTextureCasterMaterial			(lua_State *L) { PROFILE
	cOgreWrapper::GetSingleton().mSceneMgr->setShadowTextureCasterMaterial(luaL_checkstring(L,1));
	return 0;
}

/// for lua :   void	  OgreSetShadowTextureReceiverMaterial  (sMatName)
static int 				l_OgreSetShadowTextureReceiverMaterial	(lua_State *L) { PROFILE  // TODO : move to seperate file ?
	cOgreWrapper::GetSingleton().mSceneMgr->setShadowTextureReceiverMaterial(luaL_checkstring(L,1));
	return 0;
}

/// for lua :   void OgreAmbientLight  (r,g,b) [color value 0..1 each]
static int l_OgreAmbientLight	(lua_State *L) { PROFILE  // TODO : move to seperate file ?
	float r = luaL_checknumber(L,1);
	float g = luaL_checknumber(L,2);
	float b = luaL_checknumber(L,3);
	Ogre::SceneManager *p = cOgreWrapper::GetSingleton().mSceneMgr;
	
	if(p){
		p->setAmbientLight( ColourValue( r, g, b ) );
	}
	
	return 0;
}

/// for lua :   string  GetUniqueName  ()
static int l_GetUniqueName	(lua_State *L) { PROFILE  // TODO : move to seperate file ?
	std::string n = cOgreWrapper::GetSingleton().GetUniqueName();
	lua_pushstring(L,n.c_str());
	return 1;
}

/// for lua :   number OgreLastFPS  ()
static int l_OgreLastFPS	(lua_State *L) { PROFILE lua_pushnumber(L,cOgreWrapper::GetSingleton().mfLastFPS);return 1; }
/// for lua :   number OgreAvgFPS  ()
static int l_OgreAvgFPS	(lua_State *L) { PROFILE lua_pushnumber(L,cOgreWrapper::GetSingleton().mfAvgFPS);return 1; }
/// for lua :   number OgreBestFPS  ()
static int l_OgreBestFPS	(lua_State *L) { PROFILE lua_pushnumber(L,cOgreWrapper::GetSingleton().mfBestFPS);return 1; }
/// for lua :   number OgreWorstFPS  ()
static int l_OgreWorstFPS	(lua_State *L) { PROFILE lua_pushnumber(L,cOgreWrapper::GetSingleton().mfWorstFPS);return 1; }
/// for lua :   number OgreBestFrameTime  ()
static int l_OgreBestFrameTime	(lua_State *L) { PROFILE lua_pushnumber(L,cOgreWrapper::GetSingleton().miBestFrameTime);return 1; }
/// for lua :   number OgreWorstFrameTime  ()
static int l_OgreWorstFrameTime	(lua_State *L) { PROFILE lua_pushnumber(L,cOgreWrapper::GetSingleton().miWorstFrameTime);return 1; }
/// for lua :   number OgreTriangleCount  ()
static int l_OgreTriangleCount	(lua_State *L) { PROFILE lua_pushnumber(L,cOgreWrapper::GetSingleton().miTriangleCount);return 1; }
/// for lua :   number OgreBatchCount  ()
static int l_OgreBatchCount	(lua_State *L) { PROFILE lua_pushnumber(L,cOgreWrapper::GetSingleton().miBatchCount);return 1; }

/// adds a resource location to
/// example : OgreAddResLoc("./data/SomeZip.zip","Zip","General")
/// example : OgreAddResLoc("./data/base","FileSystem","General")
static int l_OgreAddResLoc	(lua_State *L) { PROFILE 
	std::string sArchName	= luaL_checkstring(L, 1);
	std::string sTypeName	= luaL_checkstring(L, 2);
	std::string sSecName	= luaL_checkstring(L, 3);
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation(sArchName,sTypeName,sSecName);
	return 0;
}

static int l_OgreInitResLocs	(lua_State *L) { PROFILE 
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	return 0;
}


/*
static int l_Client_SetMouseSensitivity (lua_State *L) { PROFILE
	cClient* client = cGame::GetSingleton().mpClient;
	if (client) client->mfMouseSensitivity = luaL_checknumber(L,1);
	return 0;
}



static int l_Client_SetInvertMouse (lua_State *L) { PROFILE
	cClient* client = cGame::GetSingleton().mpClient;
	if (client) client->mbInvertMouse = luaL_checkint(L,1) != 0;
	return 0;
}


static int l_Client_SetCamera (lua_State *L) { PROFILE
	int i=0;
	Real x = luaL_checknumber(L,++i);
	Real y = luaL_checknumber(L,++i);
	Real z = luaL_checknumber(L,++i);
	Real qw = luaL_checknumber(L,++i);
	Real qx = luaL_checknumber(L,++i);
	Real qy = luaL_checknumber(L,++i);
	Real qz = luaL_checknumber(L,++i);
	cClient* client = cGame::GetSingleton().mpClient;
	if (client)
			client->SetCamera(Vector3(x,y,z),Quaternion(qw,qx,qy,qz));
	else	printf("l_Client_SetCamera called from lua on non-client");
	return 0;
}

static int l_Client_ForceCamRot (lua_State *L) { PROFILE
	int i=0;
	Real qw = luaL_checknumber(L,++i);
	Real qx = luaL_checknumber(L,++i);
	Real qy = luaL_checknumber(L,++i);
	Real qz = luaL_checknumber(L,++i);
	cClient* client = cGame::GetSingleton().mpClient;
	if (client)
			client->ForceCamRot(Quaternion(qw,qx,qy,qz));
	else	printf("l_Client_ForceCamRot called from lua on non-client");
	return 0;
}

static int l_Client_CameraLookAt (lua_State *L) { PROFILE
	int i=0;
	Real x = luaL_checknumber(L,++i);
	Real y = luaL_checknumber(L,++i);
	Real z = luaL_checknumber(L,++i);
	cClient* client = cGame::GetSingleton().mpClient;
	if (client)
			client->CameraLookAt(Vector3(x,y,z));
	else	printf("l_Client_CameraLookAt called from lua on non-client");
	return 0;
}
*/



void	RegisterLua_Ogre_GlobalFunctions	(lua_State*	L) {
	lua_register(L,"InitOgre",						l_InitOgre);
	lua_register(L,"CloneMesh",						l_CloneMesh);
	lua_register(L,"QuaternionToAngleAxis",			l_QuaternionToAngleAxis);
	lua_register(L,"QuaternionSlerp",				l_QuaternionSlerp);
	lua_register(L,"MeshGetBounds",					l_MeshGetBounds);
	lua_register(L,"MeshSetBounds",					l_MeshSetBounds);
	lua_register(L,"MeshGetBoundRad",				l_MeshGetBoundRad);
	lua_register(L,"MeshSetBoundRad",				l_MeshSetBoundRad);
	//lua_register(L,"Client_SetMaxFPS",			l_Client_SetMaxFPS);
	//lua_register(L,"Client_GetMaxFPS",			l_Client_GetMaxFPS);
	
	lua_register(L,"Client_ShowOgreConfig",			l_Client_ShowOgreConfig);
	lua_register(L,"Client_TakeScreenshot",			l_Client_TakeScreenshot);
	lua_register(L,"Client_TakeGridScreenshot",		l_Client_TakeGridScreenshot);
	//lua_register(L,"Client_SetCamera",			l_Client_SetCamera);
	//lua_register(L,"Client_ForceCamRot",			l_Client_ForceCamRot);
	//lua_register(L,"Client_CameraLookAt",			l_Client_CameraLookAt);

	lua_register(L,"Client_SetSkybox",				l_Client_SetSkybox);
	lua_register(L,"Client_SetFog",					l_Client_SetFog);
	lua_register(L,"Client_RenderOneFrame",			l_Client_RenderOneFrame);
	
	lua_register(L,"Client_SetAmbientLight",		l_Client_SetAmbientLight);
	lua_register(L,"Client_ClearLights",			l_Client_ClearLights);
	lua_register(L,"Client_AddPointLight",			l_Client_AddPointLight);
	lua_register(L,"Client_AddDirectionalLight",	l_Client_AddDirectionalLight);
	lua_register(L,"Client_SetLightPosition",		l_Client_SetLightPosition);
	lua_register(L,"Client_SetLightDirection",		l_Client_SetLightDirection);
	lua_register(L,"Client_SetLightSpecularColor",	l_Client_SetLightSpecularColor);
	lua_register(L,"Client_SetLightDiffuseColor",	l_Client_SetLightDiffuseColor);
	lua_register(L,"Client_RemoveLight",			l_Client_RemoveLight);
	lua_register(L,"Client_DeleteLight",			l_Client_DeleteLight);

	lua_register(L,"SphereRayPick",					l_SphereRayPick);
	lua_register(L,"TriangleRayPick",				l_TriangleRayPick);
	lua_register(L,"PlaneRayPick",					l_PlaneRayPick);
	lua_register(L,"UnloadMeshName",				l_UnloadMeshName);
	lua_register(L,"CountMeshTriangles",			l_CountMeshTriangles);
	
	lua_register(L,"ExportMesh",					l_ExportMesh);
	lua_register(L,"TransformMesh",					l_TransformMesh);
	lua_register(L,"MeshReadOutExactBounds",		l_MeshReadOutExactBounds);
	lua_register(L,"CreateSceneManager",			l_CreateSceneManager);
	lua_register(L,"GetUniqueName",					l_GetUniqueName);
	lua_register(L,"GetScreenRay",					l_GetScreenRay);
	lua_register(L,"GetMaxZ",						l_GetMaxZ);
	lua_register(L,"ProjectPos",					l_ProjectPos);
	lua_register(L,"ProjectSizeAndPosEx",			l_ProjectSizeAndPosEx);
	lua_register(L,"ProjectSizeAndPos",				l_ProjectSizeAndPos);
	lua_register(L,"OgreMemoryUsage",				l_OgreMemoryUsage);
	lua_register(L,"OgreMeshAvailable",				l_OgreMeshAvailable);
	lua_register(L,"OgreMaterialNameKnown",			l_OgreMaterialNameKnown);
	lua_register(L,"OgreMaterialAvailable",			l_OgreMaterialAvailable);
	lua_register(L,"OgreTextureAvailable",			l_OgreTextureAvailable);
	lua_register(L,"OgreMeshTextures",				l_OgreMeshTextures);
	lua_register(L,"OgreAddCompositor",				l_OgreAddCompositor);
	lua_register(L,"OgreRemoveCompositor",			l_OgreRemoveCompositor);
	// shadow stuff
	lua_register(L,"OgreSetShadowTextureFadeStart",						l_OgreSetShadowTextureFadeStart);
	lua_register(L,"OgreSetShadowTextureFadeEnd",						l_OgreSetShadowTextureFadeEnd);
	lua_register(L,"OgreSetShadowDirLightTextureOffset",				l_OgreSetShadowDirLightTextureOffset);
	lua_register(L,"OgreSetShadowFarDistance",							l_OgreSetShadowFarDistance);
	lua_register(L,"OgreSetShadowTextureSize",							l_OgreSetShadowTextureSize);
	lua_register(L,"OgreSetShadowTexturePixelFormat",					l_OgreSetShadowTexturePixelFormat);
	lua_register(L,"OgrePixelFormatList",								l_OgrePixelFormatList);
	lua_register(L,"OgreSetShadowTextureSelfShadow",					l_OgreSetShadowTextureSelfShadow);
	lua_register(L,"OgreSetShadowTextureCasterMaterial",				l_OgreSetShadowTextureCasterMaterial);
	lua_register(L,"OgreSetShadowTextureReceiverMaterial",				l_OgreSetShadowTextureReceiverMaterial);
	lua_register(L,"OgreShadowTechnique",			l_OgreShadowTechnique);
	lua_register(L,"OgreAmbientLight",				l_OgreAmbientLight);
	// some statistic stuff
	lua_register(L,"OgreLastFPS",					l_OgreLastFPS);
	lua_register(L,"OgreAvgFPS",					l_OgreAvgFPS);
	lua_register(L,"OgreBestFPS",					l_OgreBestFPS);
	lua_register(L,"OgreWorstFPS",					l_OgreWorstFPS);
	lua_register(L,"OgreBestFrameTime",				l_OgreBestFrameTime);
	lua_register(L,"OgreWorstFrameTime",			l_OgreWorstFrameTime);
	lua_register(L,"OgreTriangleCount",				l_OgreTriangleCount);
	lua_register(L,"OgreBatchCount",				l_OgreBatchCount);
	lua_register(L,"OgreAddResLoc",					l_OgreAddResLoc);
	lua_register(L,"OgreInitResLocs",				l_OgreInitResLocs);
}

void	RegisterLua_Ogre_Classes			(lua_State*	L) {
	cCamera::LuaRegister(L);
	cViewport::LuaRegister(L);
	cRenderTexture::LuaRegister(L);
	Material_LuaRegister(L);
	Beam_LuaRegister(L);
}


