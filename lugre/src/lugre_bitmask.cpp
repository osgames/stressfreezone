#include "lugre_prefix.h"
#include "lugre_bitmask.h"
#include <stdlib.h>

namespace Lugre {

cBitMask::cBitMask	() : mpData(0) {}
cBitMask::~cBitMask	() { Reset(); }

void	cBitMask::SetDataFrom16BitImage	(const short *pImageData16Bit,const int iW,const int iH) {
	Reset();
	miW = iW;
	miH = iH;
	mpData = (char*)malloc((miW*miH+7)/8);
	int x,y,iPixelOffset;
	for (y=0;y<miH;++y) {
		for (x=0;x<miW;++x) {
			iPixelOffset = y*miW+x;
			mpData[iPixelOffset/8] |= (1<<(iPixelOffset%8)); // set the bit
			if (!pImageData16Bit[iPixelOffset]) 
				mpData[iPixelOffset/8] ^= (1<<(iPixelOffset%8)); // clear the bit by flipping it, as it was set before
			//if (!pImageData16Bit[iPixelOffset]) printf("."); else  printf("#");
		}
		//printf("\n");
	}
}
void	cBitMask::Reset	() { 
	if (mpData) free(mpData); mpData = 0; 
	miW = 0;
	miH = 0;
}

};
