This is an Emergency Updater, if the "updater.exe" don't work on your system.
Just start emergency_update.bat from the sfz bin directory (ie. c:\games\sfz\bin).
It should update sfz.

Dies ist ein zusätzlicher Updater, falls "updater.exe" auf Ihrem System nicht funktioniert.
Bitte einfach nur die "emergency_update.bat" auf dem bin Verzeichnis von sfz starten (bsp. c:\games\sfz\bin).
Nun sollte sfz geupdated werden.