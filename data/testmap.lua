function MapMain ()
	gMap = {
		{
			name="Nebular 23",
			skybox="GhoulSkyBox1",
			asteroid_count=155,
			asteroid_spread=2000,
			asteroid_speed=30,
			station_trader="Xotus2",
			isdefault=1
		},
		{
			name="Asteroid Field 0x17",
			skybox="GhoulSkyBox1",
			asteroid_count=21,
			asteroid_spread=2000,
			asteroid_speed=30,
			pirates_count=2,
			refid=1
		},
		{
			name="BlueZone",
			asteroid_count=155,
			asteroid_spread=2000,
			asteroid_speed=30,
			fog_mode=2,
			fog_r=0,
			fog_g=0.1,
			fog_b=0.2,
			fog_a=1.0,
			fog_exp=0.001, -- 0.001 = default
		},
		{
			name="RedZone",
			asteroid_count=155,
			asteroid_spread=2000,
			asteroid_speed=30,
			pirates_count=11,
			fog_mode=1,
			fog_r=0.2,
			fog_g=0.1,
			fog_b=0.0,
			fog_a=1.0,
			fog_exp=0.001, -- 0.001 = default
		},
		{
			name="Deep Space Trading Post",
			station_trader="Xotus2"
		}
	}
	ServerSpawnMap(gMap)
	

	gPirateSaturation = 0
	gPirateStrategyCycle = 5*1000 -- 1000 = 1 second
	gPirateRespawnCycle = 40*1000 -- 1000 = 1 second
	gPirateRespawnCount = 0 -- number of ships
	
	local iLocationID = SearchLocation("refid",1).miID
	local ubership = ServerSpawn(iLocationID,cObjShip,"sphereglider/sphereglider.mesh",1000,0,0,200,sprintf("Ray\nUbership"),-1,5000)
	ubership.onDamage = server.PirateOnDamage
	ubership.iNumOfWeapons = 10
end


