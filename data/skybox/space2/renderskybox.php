#!/usr/bin/php
<?php
/*
	This script renders 6 sides of a skybox with povray.
	Written in PHP, to execute "apt-get install php4-cli"
*/
// resolution in pixels, height=weight=size
$size = 512; // 32^x ??
// $size = 128;
// $size = 256;
$infile = "skybox2.pov";
$outfile_prefix = "ghoulskybox2";
$outfile_postfix = ".tga";

$frame_name = array();  // ogre skybox naming sheme
$frame_name[0] = "_fr"; // front
$frame_name[1] = "_rt"; // right
$frame_name[2] = "_bk"; // back
$frame_name[3] = "_lf"; // left
$frame_name[4] = "_up"; // up
$frame_name[5] = "_dn"; // down


for ($i=0;$i<6;++$i) {
	echo "rendering frame ".($i+1)."/6:\n";
	$outfile = $outfile_prefix . $frame_name[$i] . $outfile_postfix;
	//shell_exec("povray  -I$infile -H$size -W$size +FN32 -D -O$outfile -K$i");
	//shell_exec("povray  -I$infile -H$size -W$size +FT +D32 -O$outfile -K$i");
	//shell_exec("povray  -I$infile -H$size -W$size +FT -D32 -O$outfile -K$i");
	//shell_exec("rm -f ".str_replace(".png",".jpg",$outfile));
	shell_exec("convert $outfile -quality 100 ".str_replace(".tga",".jpg",$outfile));
	//break;
}
echo "done.\n";

/*
	ogre sample space skybox named "Examples/SpaceSkyBox"
	is stored in ogrenew/Samples/Media/materials/scripts/Example.material
	and looks like this :
	material Examples/SpaceSkyBox
	{
		technique
		{
			pass
			{
				lighting off
				depth_write off
	
				texture_unit
				{
					cubic_texture stevecube.jpg separateUV
					tex_address_mode clamp
				}
			}
		}
	}
	the stevecube.jpg adresses 6 images, which are zipped in ogrenew/Samples/Media/packs/skybox.zip
	stevecube_UP.jpg
	stevecube_FR.jpg
	stevecube_DN.jpg
	stevecube_BK.jpg
	stevecube_LF.jpg
	stevecube_RT.jpg
	
	the povray camera for the skybox should be something like this
	camera {
	  location <0,0,0>
	  angle 90
	  right <1,0,0> up <0,1,0>
	  // turn the cam based on the current frame=clock : [0-5]
	  #switch (clock)
		#range (0,3)
		  // first 4 frames : turn from left to right
		  rotate (90*clock)*y
		#break
		#case (4)
		  // look at the sky
		  rotate 90*x
		#break
		#case (5)
		  // look at the ground
		  rotate -90*x
		#break
	  #end // End of conditional part
	}
*/
?>
