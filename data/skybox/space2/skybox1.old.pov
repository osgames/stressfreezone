//	Persistence of Vision Raytracer Version 3.5 Scene Description File
//*******************************************

global_settings {
  //max_trace_level 5
}

#include "colors.inc"

camera {
  location <0,0,0>
  angle 90
  right <1,0,0> up <0,1,0>
  // turn the cam based on the current frame=clock : [0-5]
  #switch (clock)
    #range (0,3)
      // first 4 frames : turn from left to right
      rotate (90*clock)*y
    #break
    #case (4)
      // look at the sky
      rotate -90*x
    #break
    #case (5)
      // look at the ground
      rotate 90*x
    #break
  #end // End of conditional part
}

background { color rgb <0,0,0> }

// light_source { <100, 100, -200> color White }


sphere { < 0, 0, 0>, 2
	pigment { rgbt 1 } // surface of sphere is transparent
	interior {
		media {
			emission 0.02
			intervals 1
			samples 25
			method 3
			density {
				spherical
				ramp_wave
        translate 1.0*y  // replace 1.0 = t   by time for animation
        warp { turbulence 1.5 }
        translate -1.0*y // replace -1.0 = -t  by time for animation
				color_map {
					[0.0 color rgb <0, 0, 0>]
					[0.1 color rgb <1, 0, 0>]
					[0.5 color rgb <1, 1, 0>]
					[1.0 color rgb <1, 1, 0>]
				}
			}
		}
	}
	scale 25
	hollow
}

