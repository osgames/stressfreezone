#!/usr/bin/php
<?php
	/*
	generates an sfz.api that can be used to get calltipps and autocompletion in the SCiTE code-editor
	
	edit lua.properties and add something like this : 
	
	(also get the lua api from the scite hp)
	
	api.$(file.patterns.lua)=/home/ghoul/sciteapi/lua5api/lualib5_annot.api;/home/ghoul/sciteapi/sfz.api
	calltip.lua.word.characters=.$(word.chars.lua)
	calltip.lua.end.definition=)
	*/
	function beginswith ($str,$begin) { return strncmp($str,$begin,strlen($begin)) == 0; }
	
	$functionlist = array();
	
	if (0) {
		$functionlist_cpp = explode("\n",shell_exec('grep -r --no-filename --include "*.cpp" "for lua" src'));
		foreach ($functionlist_cpp as $k => $line) {
			$line = strtr($line,array(
				"///"=>"",
				"for lua"=>"",
				":"=>"",
				";"=>"",
				));
			$line = ereg_replace("^[ \t]+", "",$line); 
			$line = ereg_replace("^.*[ \t]+([^ \t]+)[ \t]*\\(", "\\1(",$line); 
			$functionlist[] = $line."\n";
		}
	}
	
	if (1) {
		$functionlist_lua = explode("\n",shell_exec('grep -r --no-filename --include "*.lua" "function" data'));
		foreach ($functionlist_lua as $k => $line) {
			$line = ereg_replace("^[ \t]+", "",$line);
			if (!beginswith($line,"function")) continue;
			$line = ereg_replace("^function[ \t]+", "",$line); 
			$line = ereg_replace("\\).*", ")",$line); 
			$line = ereg_replace("[ \t]+\\(", "(",$line); 
			$line = ereg_replace("^[^:\\(]+:", "",$line); 
			$line = ereg_replace("^[^\\.\\(]+\\.", "",$line); 
			if (beginswith($line,"(")) continue;
			
			$functionlist[] = $line."\n";
		}
	}
	
	$output_filepath = "sfz.api";
	echo "writing to $output_filepath\n";
	file_put_contents($output_filepath,$functionlist);
	
	//for ($i=0;$i<10;++$i) echo $functionlist[$i];
	//foreach ($functionlist as $o) echo $o;
		
		
	
	/*
	$path = "/cavern/wwwroot/iris/iris_ogre3d/mylugre/src";
	function dirfilelist ($path) {
		// plakat/  last slash is important !
		$list = array();
		if (!file_exists($path)) return $list;
		$dir = opendir($path);
		if (!$dir) return $list;
		while (($file = readdir($dir)) !== false)
			if ($file != "." && $file != ".." && is_file($path.$file)) $list[] = $file;
		closedir($dir);
		return $list;
	}
	
	function beginswith ($str,$begin) { return strncmp($str,$begin,strlen($begin)) == 0; }
	
	$s = "lugre_";
	$path = $path."/";
	$arr = dirfilelist($path);
	foreach ($arr as $filename) {
		if (beginswith($filename,"tiny")) continue;
		//if (!beginswith($filename,$s)) continue;
		$oldfilename = $filename;
		//$newfilename = substr($filename,strlen($s));
		$newfilename = $s.$filename;
		echo "$oldfilename -> $newfilename\n";
		
		//rename($path.$oldfilename,$path.$newfilename);
	}
	*/
?>