-- plugin for constructing the ingame rightclick menu 
-- see also lib.plugin.lua
-- ClientCloseRightClickMenu


-- creates a filterfunction
function CreateObjFilter_Type (typename)
	local mytypename = typename -- closure
	return function (obj) return obj.objtype:IsSubType(mytypename) end
end

function GetServicesContextMenu (obj)
	if (not obj.objtype.services) then return end
	local submenudata = {}
	for k,servicetypename in pairs(obj.objtype.services) do 
		local servicetype = GetServiceType(servicetypename) assert(servicetype)
		table.insert(submenudata,{ servicetype.name,function () ClientRequestService(obj,servicetype) end })
	end
	return submenudata
end

RegisterListener("Hook_Client_SetBody",function ()  
	if (gCommandLineSwitches["-i"]) then 
		ClientRequestViewShipInside()
	end
	end)

function OpenFurniturePlacementDialog (dx,dy,dz)
	local rows = {}
	local vw,vh = GetViewportSize()
	local maxperrow = 4 -- math.floor(vw*0.75/kFurniturePreviewSize)
	-- kFurniturePreviewSize
	
	local lastrow = nil
	for k,t in pairs(gObjectTypes:GetList(function (t) return t.bFurniture end)) do
		print("OpenFurniturePlacementDialog",t.type_name,t.gfx_previewmat)
		if (t.gfx_previewmat) then
			local myobjtype = t
			local r = t.iPreviewSize
			lastrow = lastrow or {}
			table.insert(lastrow,{ matname=t.gfx_previewmat,w=r,h=r,function () 
				ClientRequestPlaceFurnitureRelative(myobjtype,dx,dy,dz)
				end } )
			if (table.getn(lastrow) == maxperrow) then table.insert(rows, lastrow ) lastrow = nil end
		end
	end
	if (lastrow) then table.insert(rows,lastrow) end
	
	table.insert(rows, { { "close" , function (widget) widget.dialog:Destroy() end  } } )
	
	
	local vw,vh = GetViewportSize()
	local dialog = guimaker.MakeTableDlg(rows,100,10,false,true,gGuiDefaultStyleSet,"window")
end

RegisterListener("Hook_Client_BuildRightClickMenu",function (menudata,clicked_obj) 
	
		local label = clicked_obj and clicked_obj:GetTypeAndNameText() or "empty space"
		table.insert(menudata,{ label })
		
		if (not gClientBody) then return end -- dead people cannot do much
		
		if (clicked_obj) then
			local obj = clicked_obj -- closure
			table.insert(menudata,{ sprintf("dist : %0.0f m ",gClientBody:GetDistToObject(obj)) })
			table.insert(menudata,{ "approach",function () ClientAutoPilot_Approach(obj) end })
			if (obj ~= gClientTarget) then table.insert(menudata,{ "select",function () ClientSetTarget(obj) end }) end
			--table.insert(menudata,{ "attack",function () print("test") end })
			
			local services_submenu = GetServicesContextMenu(obj)
			if (services_submenu) then table.insert(menudata,{ "services",services_submenu }) end
			if (obj.objtype.tradelist) then table.insert(menudata,{ "trade", function () ClientOpenTradeDialog(obj) end }) end
			if (obj:IsLandable()) then table.insert(menudata,{ "land", function () ClientRequestLandOnPlanet(obj) end }) end
			
			if (obj:HasMissions()) then 
				local missions_submenu = {}
				for k,mission in pairs(obj:ListMissions()) do
					local mymission = mission
					table.insert(missions_submenu,{ mission.description, function () ClientRequestAcceptMission(mymission.id) end }) 
				end
				table.insert(menudata,{ "missions", missions_submenu }) 
			end
			
			
			--if (obj.objtype.bIsShip) then 
			--	table.insert(menudata,{ "follow",function () print("test") end })
			--end
		else
			if (gClientTarget) then
				table.insert(menudata,{ sprintf("dist to selected : %0.0f m ",gClientBody:GetDistToObject(gClientTarget)) })
				-- selected services
				local services_submenu = gClientTarget and GetServicesContextMenu(gClientTarget)
				if (services_submenu) then table.insert(menudata,{ "services",services_submenu }) end
			end
		end
		
		if (gClientTarget) then
			table.insert(menudata,{ "deselect target",function () ClientChooseTarget_NoTarget() end })
		end
		
		
		if (clicked_obj and clicked_obj.objtype.bFurniture) then
			table.insert(menudata,{ "turn furniture",function () ClientRequestTurnFurniture(clicked_obj,90) end })
			
		end
		
		-- furniture menu
		if (ClientIsInsideShip()) then
			local submenu_furniture = {}
			table.insert(submenu_furniture,{ "place here",		function () OpenFurniturePlacementDialog(0,0,0) end })
			table.insert(submenu_furniture,{ "place infront",	function () OpenFurniturePlacementDialog(0,0,-1) end })
			table.insert(submenu_furniture,{ "place above",		function () OpenFurniturePlacementDialog(0,1,0) end })
			table.insert(menudata,{ "furniture",submenu_furniture })
		end
		
		-- goto submenu
		if (true) then
			local submenu_goto = {}
			if (gClientTarget) then
				table.insert(submenu_goto,{ "selected" , function () ClientAutoPilot_Approach(gClientTarget) end})
			end
			
			table.insert(submenu_goto,{ "nearest base",		function () 
					local o = ClientGetNearestObject(ObjFilter_IsBase)
					ClientSetTarget(o)
					ClientAutoPilot_Approach(o) 
				end})
			table.insert(submenu_goto,{ "nearest asteroid",	function () 
					local o = ClientGetNearestObject(CreateObjFilter_Type("asteroid"))
					ClientSetTarget(o)
					ClientAutoPilot_Approach(o) 
				end})
			table.insert(submenu_goto,{ "nearest hostile",	function () 
					ClientChooseTarget_NearestHostile()
					if (gClientTarget) then ClientAutoPilot_Approach(gClientTarget) end
				end})
				
			
			table.insert(menudata,{ "goto",submenu_goto })
		end
		
		if (gClientBody:IsSpaceSuit()) then
			if (clicked_obj and clicked_obj:CanEnterShip(gClientOwner)) then 
				table.insert(menudata,{ "enter ship",function () ClientRequestEnterShip(clicked_obj) end })
			end
		else
			table.insert(menudata,{ "leave ship in spacesuit",function () ClientRequestExitShip() end })
			
			if (clicked_obj and clicked_obj:HasHangar()) then
				table.insert(menudata,{ "dock",function () ClientRequestDockToObject(clicked_obj) end })
			end
		end
		
		if (gClientBody:HasHangar()) then
			table.insert(menudata,{ "launch in fighter",function () ClientRequestLaunchFighter() end })
		end
		
		if (gClientBody:IsShip() and (not gClientBody:IsSpaceSuit())) then
			table.insert(menudata,{ "view ship interior",function () ClientRequestViewShipInside() end })
		end
		
		if (ClientIsInsideShip()) then
			table.insert(menudata,{ "view ship outside",function () ClientRequestViewShipOutside() end })
		end
		
		if (ClientIsOnPlanet()) then
			table.insert(menudata,{ "leave planet",function () ClientRequestLeavePlanet() end })
		end
		
		--[[
		local submenudata = {
			{ "Label" },
			{ "ButtonText", function () print("blub") end },
			{ "SubMenuTitle", { {"subsub1"},{"subsub2",function () end},{"subsu3",function () end}, } },
		}
		table.insert(menudata,{ "submenutest",submenudata })
		]]--
	end)


