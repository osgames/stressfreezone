-- plugin for adding a crosshair
-- see also lib.plugin.lua

local pluginenabled = false -- set this to true to enable the plugin


if (pluginenabled) then  
	
	RegisterListener("Hook_Client_Start",function () 
		--local mat = GetPlainTextureMat("crosshair01.png")
		local mat = "crosshair01"
		gCrossHairDialogLayer = guimaker.MyCreateDialog()
		local e = 64
		gCrossHairDialogLayer.panel = guimaker.MakePlane(gCrossHairDialogLayer,mat,-0.5*e,-0.5*e,e,e)
		gCrossHairDialogLayer.panel.gfx:SetAlignment(kGfx2DAlign_Center,kGfx2DAlign_Center)
		gCrossHairDialogLayer:SendToBack()
	end)
end
