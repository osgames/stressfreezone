-- displaying own status on hud

gHUDStatusColor_Shield	= {0.2,0.2,0.9, 1.0}
gHUDStatusColor_Hull	= {0.9,0.2,0.2, 1.0}
gHUDStatusColor_Cargo	= {0.5,0.3,0.1, 1.0}
gHUDStatusColor_Energy  = {0.9,0.9,0.2, 1.0}
gHUDStatusColor_Back	= {0.2,0.2,0.2, 1.0}
gHUDStatusBarH = 10
gHUDStatusBarBorder = 2	-- pixel between two bars
gHUDStatusBarW = gHUDStatusBarH*8
gHUDStatusBarUMax = gHUDStatusBarW/gHUDStatusBarH
gHUDTexts = {}
gHUDTexts_LastKnownTarget = nil
gHUDTexts_NextUpdate = 0
kHUDTexts_UpdateInterval = 200

function MakeStatusBar (parent,matname,tooltip)
	local widget = guimaker.MakeRROC(parent,matname)
	widget.mbIgnoreMouseOver = false
	widget.mytooltip = tooltip
	widget.on_simple_tooltip = function (widget) return widget.mytooltip end
	return widget
end


-- text fields : left/right align, update every 500sec (dist) and on target change
-- param align : "l" or "r" or "c" for left, right, center
-- TODO : maybe later rightclick
-- TODO : maybe later color function : shield from green to red, name in FOF color
function MakeHUDText (x,y,align,tooltip_text,curvalue_fun)
	local offx,offy = 0,2
		if (align == "r") then	offx = -2 align = kGfx2DAlign_Right 
	elseif (align == "c") then	offx =  0 align = kGfx2DAlign_Center 
	else						offx =  2 align = kGfx2DAlign_Left end 
	local charh = gHUDStatusBarH
	local col = {1,1,1,1}
	local text = curvalue_fun() -- should return "" during preload
	local parent = gHudOwnStatusDialog.panel
	local hudtext = guimaker.MakeText(parent,x+offx,y+offy,text,charh,col)
	hudtext.gfx:SetTextAlignment(align)
	hudtext.curvalue_fun = curvalue_fun
	table.insert(gHUDTexts,hudtext)
end

-- returns dist or nil if not availble
function ClientGetDistToTarget ()
	return gClientBody and gClientTarget and gClientBody:GetDistToObject(gClientTarget)
end


RegisterListener("Hook_Client_Start",function () 
	gHUDInitialized = true

	local vw,vh = GetViewportSize()
	
	local col_back = {0,0,0,0}
	gHudOwnStatusDialog = guimaker.MyCreateDialog()
	gHudOwnStatusDialog.panel = guimaker.MakeBorderPanel(gHudOwnStatusDialog,0,0,0,0,col_back)
	gHudOwnStatusDialog.panel.mbClipChildsHitTest = false
	gHudOwnStatusDialog:SendToBack()
	local mat = GetPlainTextureMat("bar07.png",true) -- GetPlainWhiteMat()
	gHudOwnStatus_Bar_Hull		= MakeStatusBar(gHudOwnStatusDialog.panel,mat,"own hull"		)
	gHudOwnStatus_Bar_Shield	= MakeStatusBar(gHudOwnStatusDialog.panel,mat,"own shields"		)
	gHudOwnStatus_Bar_Hull2		= MakeStatusBar(gHudOwnStatusDialog.panel,mat,"target hull"		)
	gHudOwnStatus_Bar_Shield2	= MakeStatusBar(gHudOwnStatusDialog.panel,mat,"target shields"	)
	gHudOwnStatus_Bar_Cargo		= MakeStatusBar(gHudOwnStatusDialog.panel,mat,"own cargo"		)
	gHudOwnStatus_Bar_Energy    = MakeStatusBar(gHudOwnStatusDialog.panel,mat,"own energy"		)
	gHudOwnStatus_Bar_Cargo.on_button_click		= function () OpenOwnCargoDialog() end
	
	local w,h,b = gHUDStatusBarW,gHUDStatusBarH,gHUDStatusBarBorder
	MakeHUDText(0,0*(h+b),		"l","own type"			,function () return HUDGetText_Type(gClientBody)				end)
	MakeHUDText(w,1*(h+b),		"l","own hull"			,function () return HUDGetText_Hull(gClientBody)				end)
	MakeHUDText(w,2*(h+b),		"l","own shields"		,function () return HUDGetText_Shield(gClientBody)				end)
	MakeHUDText(w,3*(h+b),		"l","own energy"		,function () return HUDGetText_Energy(gClientBody)				end)
	MakeHUDText(w,4*(h+b),		"l","own cargo"			,function () return HUDGetText_Cargo(gClientBody)				end)

	MakeHUDText(vw-0,0*(h+b),	"r","target type"		,function () return HUDGetText_Type(gClientTarget)				end)
	MakeHUDText(vw-w,1*(h+b),	"r","target hull"		,function () return HUDGetText_Hull(gClientTarget)				end)
	MakeHUDText(vw-w,2*(h+b),	"r","target shields"	,function () return HUDGetText_Shield(gClientTarget)			end)
	MakeHUDText(vw-0,3*(h+b),	"r","distance"			,function () return HUDGetText_Dist(gClientBody,gClientTarget)	end)
end)

function HUDGetText_Type	(o) return o and o:IsAlive() and o:GetTypeAndNameText() or "" end
function HUDGetText_Hull	(o) return o and o:IsAlive() and sprintf("%d/%d",o:GetHull() or 0,o:GetHullMax() or 0) or "" end
function HUDGetText_Shield	(o) return o and o:IsAlive() and sprintf("%d/%d",o:GetShield() or 0,o:GetShieldMax() or 0) or "" end
function HUDGetText_Cargo	(o) return o and o:IsAlive() and sprintf("%d/%d",o:GetCurCargo() or 0,o:GetMaxCargo() or 0) or "" end
function HUDGetText_Energy	(o) return o and o:IsAlive() and sprintf("%d/%d",o:GetEnergy() or 0,o:GetEnergyMax() or 0) or "" end
function HUDGetText_Dist	(a,b) return a and b and a:IsAlive() and b:IsAlive() and sprintf("distance : %0.0fm",a:GetDistToObject(b)) or "" end

RegisterListener("Hook_HUDStep",function ()
	if not gHUDInitialized then return end
	local ship = gClientBody
	local target = gClientTarget
	local f_shield	= ship and ship:GetShieldPercentage() or 0
	local f_hull	= ship and ship:GetHullPercentage() or 0
	local f_cargo	= ship and ship:GetCargoPercentage() or 0
	local f_energy	= ship and ship:GetEnergyPercentage() or 0
	local f_shield2	= target and target:GetShieldPercentage() or 0
	local f_hull2	= target and target:GetHullPercentage() or 0
	local vw,vh = GetViewportSize()
	local w,h,b = gHUDStatusBarW,gHUDStatusBarH,gHUDStatusBarBorder
	Gfx2DSetBar(gHudOwnStatus_Bar_Hull.gfx,		0,1*(h+b),	w,h,f_hull,		gHUDStatusColor_Hull,	gHUDStatusColor_Back,0,gHUDStatusBarUMax)
	Gfx2DSetBar(gHudOwnStatus_Bar_Shield.gfx,	0,2*(h+b),	w,h,f_shield,	gHUDStatusColor_Shield,	gHUDStatusColor_Back,0,gHUDStatusBarUMax)
	Gfx2DSetBar(gHudOwnStatus_Bar_Energy.gfx,	0,3*(h+b),	w,h,f_energy,	gHUDStatusColor_Energy,	gHUDStatusColor_Back,0,gHUDStatusBarUMax)
	Gfx2DSetBar(gHudOwnStatus_Bar_Cargo.gfx,	0,4*(h+b),	w,h,f_cargo,	gHUDStatusColor_Cargo,	gHUDStatusColor_Back,0,gHUDStatusBarUMax)
	if (target) then
		gHudOwnStatus_Bar_Hull2.gfx:SetVisible(true)
		gHudOwnStatus_Bar_Shield2.gfx:SetVisible(true)
		Gfx2DSetBar(gHudOwnStatus_Bar_Hull2.gfx,	vw-w,1*(h+b),	w,h,f_hull2,	gHUDStatusColor_Hull,	gHUDStatusColor_Back,gHUDStatusBarUMax,0)
		Gfx2DSetBar(gHudOwnStatus_Bar_Shield2.gfx,	vw-w,2*(h+b),	w,h,f_shield2,	gHUDStatusColor_Shield,	gHUDStatusColor_Back,gHUDStatusBarUMax,0)
	else
		gHudOwnStatus_Bar_Hull2.gfx:SetVisible(false)
		gHudOwnStatus_Bar_Shield2.gfx:SetVisible(false)
	end	
	
	if (gHUDTexts_LastKnownTarget ~= gClientTarget or gMyTicks >= gHUDTexts_NextUpdate) then
		gHUDTexts_LastKnownTarget  = gClientTarget
		gHUDTexts_NextUpdate = gMyTicks + kHUDTexts_UpdateInterval
		for k,hudtext in pairs(gHUDTexts) do
			hudtext.gfx:SetText(hudtext:curvalue_fun())
		end
	end
end)



-- a bar consist of two rects, colored by vertex color
-- todo : replace by custom rect class ?
function Gfx2DSetBar (gfx,x,y,w,h,fraction,col_fore,col_back,umin,umax,bIsVertical)
	local maxz = GetMaxZ()
	fraction = math.max(0,math.min(1,fraction))
	local m = (bIsVertical and h or w)*fraction
	gfx:RenderableBegin(4 + 4,6 + 6,true,false,OT_TRIANGLE_LIST)
	local vw,vh = GetViewportSize()
	
		local r,g,b,a = unpack(col_fore)
		if (bIsVertical) then
			--[[
			gfx:RenderableVertex(0,0,maxz, r,g,b,a)
			gfx:RenderableVertex(0,m,maxz, r,g,b,a)
			gfx:RenderableVertex(w,0,maxz, r,g,b,a)
			gfx:RenderableVertex(w,m,maxz, r,g,b,a)
			r,g,b,a = unpack(col_back)
			gfx:RenderableVertex(0,m,maxz, r,g,b,a)
			gfx:RenderableVertex(0,h,maxz, r,g,b,a)
			gfx:RenderableVertex(w,m,maxz, r,g,b,a)
			gfx:RenderableVertex(w,h,maxz, r,g,b,a)
			]]--
		else
			local x1 =  -1 + (x+0)*2/vw
			local x2 =  -1 + (x+m)*2/vw
			local x3 =  -1 + (x+w)*2/vw
			local y1 =   1 - (y+0)*2/vh
			local y2 =   1 - (y+h)*2/vh
			local u1 = umin
			local u2 = umin + (umax-umin)*fraction
			local u3 = umax
			local v = 1
			gfx:RenderableVertex(x1,y2,maxz, u1,v, r,g,b,a)
			gfx:RenderableVertex(x2,y2,maxz, u2,v, r,g,b,a)
			gfx:RenderableVertex(x1,y1,maxz, u1,0, r,g,b,a)
			gfx:RenderableVertex(x2,y1,maxz, u2,0, r,g,b,a)
			r,g,b,a = unpack(col_back)
			gfx:RenderableVertex(x2,y2,maxz, u2,v, r,g,b,a)
			gfx:RenderableVertex(x3,y2,maxz, u3,v, r,g,b,a)
			gfx:RenderableVertex(x2,y1,maxz, u2,0, r,g,b,a)
			gfx:RenderableVertex(x3,y1,maxz, u3,0, r,g,b,a)
		end
		
		gfx:RenderableIndex3(0,1,2)
		gfx:RenderableIndex3(2,1,3)
		
		gfx:RenderableIndex3(4,5,6)
		gfx:RenderableIndex3(6,5,7)
	gfx:RenderableEnd()
	gfx:SetPos(x,y)
	gfx:SetDimensions(w,h)
end

