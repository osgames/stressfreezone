-- handles the plain text like selectship dialog

gSelectShipDialog = nil
gSelectShipDialogX, gSelectShipDialogY = 50,50

RegisterListener("Hook_Client_OpenSelectShipDialog",function () OpenSelectShipDialog() end)

function CloseSelectShipDialog ()
	if (gSelectShipDialog) then gSelectShipDialog:Destroy() gSelectShipDialog = nil end
end

function SelectShipDialogChoose(typename)
   if GetObjectType(typename) then
        RequestBody(typename)
        CloseSelectShipDialog()
    end
end

function OpenSelectShipDialog (x,y)
    x = x or gSelectShipDialogX
    y = y or gSelectShipDialogY

	CloseSelectShipDialog()
	local rows = { { 
		{ type="Label",	text="SelectShip:"},
		{ type="Label",	text=""},
		{ type="Button",text="close",on_button_click=CloseSelectShipDialog },
		} }

    -- get all ship types
    local l = GetObjectTypeList(function(x)
        return x.bIsShip or x.bIsSpaceSuit
    end)

	for k,o in pairs(l) do 
        local typename = o.type_name
        local row = { 
            { type="Button",text=typename,
                on_button_click=function (self) SelectShipDialogChoose(typename) end },
        }
        table.insert(rows,row)
	end

	gSelectShipDialog = guimaker.MakeTableDlg(rows,x,y,true,false,gGuiDefaultStyleSet,"window")
	gSelectShipDialog.rootwidget.onDestroy = function()
		gSelectShipDialogX, gSelectShipDialogY = gSelectShipDialog.rootwidget.gfx:GetPos()
	end

end

