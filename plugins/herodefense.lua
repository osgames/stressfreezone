-- plugin for adding pirate ships attacking the player
-- see also lib.plugin.lua

local pluginenabled = true -- set this to true to enable the plugin
if (gSkipHeroDefense) then pluginenabled = false end

gPlugin_HeroDefense = {}
gPlugin_HeroDefense.kiStartTimeout = 1 * 1000
gPlugin_HeroDefense.kiWaveTimeout = 20 * 1000
gPlugin_HeroDefense.kiCountDownTimeout = 2 * 1000
gPlugin_HeroDefense.kSpawnRad = 50

-- sends chat count down chat messages
gPlugin_HeroDefense.CountDown = function(dt,count,message)
	local timeout = dt * count
	for i = 1,count do
		local ii = i
		local t = ii * dt
		
		InvokeLater(t,function()
			local left = timeout - t
			ServerBroadcastChatMessage(math.floor(left / 1000) .. message)
		end)
	end
end

gPlugin_HeroDefense.GetRandomPlayerBody = function()
	local l = countarr(gServerPlayers)
	local c = 1
	local r = math.random(l)
	for k,v in pairs(gServerPlayers) do
		if c >= r and v.body then return v.body end
		c = c + 1
	end
	
	return nil
end

-- wave start position x,y,z
gPlugin_HeroDefense.Init = function()
	if (not gbServerRunning) then return end
	print("gPlugin_HeroDefense.Init")

	-- number of the current wave
	gPlugin_HeroDefense.miCurrentWave = 1
	
	gPlugin_HeroDefense.CountDown(gPlugin_HeroDefense.kiCountDownTimeout, gPlugin_HeroDefense.kiStartTimeout / gPlugin_HeroDefense.kiCountDownTimeout, " seconds till start")

	-- start the first wave after timeout
	InvokeLater(gPlugin_HeroDefense.kiStartTimeout,function()
		-- start first wave
		gPlugin_HeroDefense.StartWave(gPlugin_HeroDefense.miCurrentWave)
	end)

	ServerBroadcastChatMessage("HeroDefense: Welcome!!!")
	
	-- TODO its not possible to restart the mod
	return true	-- only the first join triggers init
end

gPlugin_HeroDefense.fDistFromPlayers = 1600
gPlugin_HeroDefense.fGroupStartRadius = 50
gPlugin_HeroDefense.StartWave = function(number)
	if not gPlugin_HeroDefense.mCurrentGroup then
		
		local x,y,z = 0,0,0
		local player = arrfirst(gServerPlayers)
		local playerbody = player and player.body
		if (playerbody) then x,y,z = unpack(playerbody.mvPos) end
		x,y,z = GetRandomPositionAtDist(gPlugin_HeroDefense.fDistFromPlayers,x,y,z)
		x,y,z = GetRandomPositionAtDist(gPlugin_HeroDefense.fGroupStartRadius,x,y,z)

		local leveldata = gPlugin_HeroDefense.GetNextLevel(number)
		if leveldata then
			-- start the next wave
			gPlugin_HeroDefense.miCurrentWave = number
			
			gPlugin_HeroDefense.mCurrentGroup = SpawnShipGroup ( leveldata[1], x, y, z, gPlugin_HeroDefense.kSpawnRad, leveldata[2])
			gPlugin_HeroDefense.mCurrentGroup.onDestroy = function (self)
				-- trigger next wave
				gPlugin_HeroDefense.CountDown(gPlugin_HeroDefense.kiCountDownTimeout, gPlugin_HeroDefense.kiWaveTimeout / gPlugin_HeroDefense.kiCountDownTimeout, " seconds till next wave")
				gPlugin_HeroDefense.mCurrentGroup = nil
				
				InvokeLater(gPlugin_HeroDefense.kiWaveTimeout,function()
					-- start first wave
					gPlugin_HeroDefense.StartWave(gPlugin_HeroDefense.miCurrentWave + 1)
				end)
			end
			
			local target = gPlugin_HeroDefense.GetRandomPlayerBody()
			print("TARGET",target)
			gPlugin_HeroDefense.mCurrentGroup:Attack(target)
			
			ServerBroadcastChatMessage("HeroDefense: wave " .. number .. " started")
		else
			-- finished all waves ... this will never occure
			-- TODO
			
			ServerBroadcastChatMessage("HeroDefense: there is no wave left :)")
		end
	end
end

gPlugin_HeroDefense.GetNextLevel = function(currentLevel) 
	local pnum = countarr(gServerPlayers)
	local piratenum = floor(math.max(0,pnum*currentLevel-10))
	local little_piratenum = floor(pnum*currentLevel*2)
	local big_piratenum = floor(math.max(0,pnum*currentLevel/2-15))
	local mega_piratenum = floor(math.max(0,pnum*currentLevel/3-20))
	return {{["shiptype/pirate"]=piratenum,["shiptype/little_pirate"]=little_piratenum,["shiptype/bigpirate"]=big_piratenum,["shiptype/megapirate"]=mega_piratenum},"Pirate"}
end

if pluginenabled then
	-- this is a server plugin
	RegisterListener("Hook_Server_SetBody", function() return gPlugin_HeroDefense.Init() end)
end
