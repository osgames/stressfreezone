-- plugin example for adding custom main-menu buttons
-- see also lib.plugin.lua

local pluginenabled = false -- set this to true to enable the plugin

if (pluginenabled) then  
	RegisterListener("Hook_MainMenu_Buttons",function (rows)
		table.insert(rows,{ {type="Button",text="MainMenuPluginTest",onLeftClick=function () print("test") end } })
		end)
end
