-- a library of shippart skins (texcoords and materials)
-- see also lib.plugin.lua
-- RegisterShipPartSkin(name,parentname,arr)

function LoadCommonShipPartSkins ()
	--~ tri1	: ab is orthogonal on bc,  ca is the diagonal
	--~ *2		: ab is a long side, but bc is ortho on ab
	-- data/texture/gb_shippartskin_01.png
	local windowmat01 = GetTexturedMat("shippartskin_base_with_CubeMap","gb_shippartskin_01.png")
	RegisterShipPartSkin("shippartskin",		false,			{preloader=ShipPartSkinDefaultPreLoader},true)
	RegisterShipPartSkin("shippartskin/metal",	"shippartskin",	{tex="gb_shippartskin_01.png", tw=2,th=4,
																	quad1	= {0,3, 1,3, 1,4, 0,4	},
																	tri1	= {0,4, 0,3, 1,3		},
																	quad2	= {0,3, 2,3, 2,4, 0,4	},
																	tri2	= {0,4, 2,4, 2,3		}, })
	RegisterShipPartSkin("shippartskin/window",	"shippartskin",	{mat=windowmat01, tw=2,th=4,
																	quad1	= {0,2, 1,2, 1,3, 0,3	},
																	tri1	= {0,2, 0,1, 1,1		},
																	quad2	= {0,0, 2,0, 2,1, 0,1	},
																	tri2	= {0,2, 2,2, 2,1		}, })
	RegisterShipPartSkin("shippartskin/dock",	"shippartskin",	{tex="gb_shippartskin_01.png", tw=2,th=4,
																	quad1	= {1,3, 2,3, 2,4, 1,4	}, })
end

-- t = typeobject
function ShipPartSkinDefaultPreLoader (t)
	if (t.abstract) then return end
	local myTexCoordListNames = {"quad1","quad2","tri1","tri2"}
	t.mat = t.mat or GetPlainTextureMat(t.tex)
	for k,name in pairs(myTexCoordListNames) do 
		for k,v in pairs(t[name] or {}) do
			t[name][k] = v / ((mod(k-1,2) == 0) and t.tw or t.th)
		end
	end
	-- see also GetPlainColourMat(1,1,1) GetHuedMat(matname,r,g,b) (hueing should be done dynamically)
end

RegisterListener("Hook_PreLoad",function () LoadCommonShipPartSkins() end)
