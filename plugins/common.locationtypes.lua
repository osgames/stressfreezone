-- a library of location types (e.g. outer space, inside hangar, inside wormhole...)
-- see also lib.location.lua
-- see also lib.plugin.lua
-- RegisterLocationType(name,parentname,arr)
-- affects skybox,starfield,ambient light,some special effect (quasispace,wormhole), root camera scenenode...

function LoadCommonLocationTypes ()
	RegisterLocationType("locationtype/base",				false,					{name=""})
	RegisterLocationType("locationtype/shipinterior",		"locationtype/base",	{name="ship interior"	,skycolor={0.0,0.0,0.0}, 	bDirectionalLight=true, bIsInsideShip=true})
	RegisterLocationType("locationtype/onplanet",			"locationtype/base",	{name="planet"			,skybox="SkyBox_Clouds_01", bDirectionalLight=true, bIsPlanet=true})
	RegisterLocationType("locationtype/default",			"locationtype/base",	{name="outer space"		,skybox="GhoulSkyBox1", 	bStarField=true})
end


RegisterListener("Hook_PreLoad",function () LoadCommonLocationTypes() end)

