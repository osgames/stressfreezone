-- plugin for adding pirate ships attacking the player
-- see also lib.plugin.lua

local pluginenabled = false-- set this to true to enable the plugin

if (pluginenabled) then  

gPirates = {}
gPirateNum = 4
kPirateLittleOnesOnDeath = 20
kPirateSpawnRad = 2000
kPirateAggroRad = 500
gNextPirateSpawn = 4
gNextPirateSpawnInterval = 10*1000

gPirateChatTimeout = 5 * 1000	-- 2 sec timeout between chats of one pirate
gPirateChat = {
	death = {	-- pirate died
		"Aaaaarrrrrrrrrggggggggghhhhhh!!!!",
		"See you in hell!",
		"You shouldn't have done that.",
		"You will regret that",
	},
	kill = {	-- pirate killed someone
		"Hahahahaa !!!",
		"From dust to dust.",
		"Suck vacuum!",
		"Busted your ass",
		"Your frozen corpse will drift in eternity",
		"Thats for messing with me",
		"Your debris will sparkle on my shields", 
	},
	hit = {	-- pirate got hit
		"That was a very very bad idea",
		"I'm immortal",
		"Is this all you can do ?",
		"My mommy hits harder",
	},
	critical_hit = {	-- pirate got hit, hulldamage > 0
		"I painted my ship yesterday!!! ARgh!",
		"You will pay for this!",
		"You bastard!",
		"Damn!",
		"Ouch!",
	}
}
-- gPirateChat[math.random(table.getn(gPirateChat))]

function PirateChat(pirate,topic)
	-- only send chat if timeout reached
	if (
		not pirate.last_chat_timestamp or 
		pirate.last_chat_topic ~= topic or
		gMyTicks - pirate.last_chat_timestamp > gPirateChatTimeout
	) then
		-- update last chat time
		pirate.last_chat_timestamp = gMyTicks
		pirate.last_chat_topic = topic
		
		local name = "pirate"
		local msg = nil
		if pirate then name = pirate:GetName() end
		
		local l = gPirateChat[topic]
		-- read out random message
		if l then msg = l[math.random(table.getn(l))] end
		
		if msg then ServerBroadcastChatMessage(name .. ": " .. msg) end
	end
end

	
	function PiratesStep ()
		if (not gbServerRunning) then return end
		--print("pirate count",table.getn(gPirates))
		if (gNextPirateSpawn <= gMyTicks) then
			gNextPirateSpawn = gMyTicks + gNextPirateSpawnInterval
			if (countarr(gPirates) < gPirateNum) then print("create pirate") table.insert(gPirates,SpawnPirate("shiptype/pirate")) end
		end
		
		for k,pirate in pairs(gPirates) do 
			if (pirate:IsAlive()) then 
				StepPirate(pirate) 
			else 
				-- clean up pirate
				gPirates[k] = nil
				print("pirate died",countarr(gPirates))
				gNextPirateSpawn = gMyTicks + gNextPirateSpawnInterval -- prevent the terrifying "kill just before spawntime" effect
				
				PirateChat(pirate,"death")
			end
		end
	end
	
	function SpawnPirate (typename,x,y,z)
		if not x then
			x,y,z = Vector.random3(kPirateSpawnRad)
		end
		
		local pirate = SpawnObject(typename,gMainLocation,{x,y,z},{Quaternion.random()})
		pirate:SetName(GenerateRandomName_Pirate())
		pirate.bIsPirate = true
		return pirate
	end
	
	function PiratesDamageNotify (obj,attacker,shielddamage,hulldamage)
		if (not obj.bIsPirate) then return end
		local pirate = obj
		local target = attacker

		if hulldamage > 0 then
			PirateChat(pirate,"critical_hit")
		else 
			PirateChat(pirate,"hit")
		end

		-- activate all other idle pirates
		if (target) then 
			for k,o in pairs(gPirates) do
				if not o.target then
					AutoPilot_Follow(o,target)
					--print("pirate : PiratesDamageNotify : AutoPilot_Follow",target)
					o.target = target
				end
			end	
		end
	end
	
	function PiratesDestroy (pirate)
		if pirate.target and pirate.target:IsAlive() and 
			kPirateLittleOnesOnDeath > 0 and pirate.objtype.name == "pirate" then
			
			local vx,vy,vz = unpack(pirate.mvVel)
			-- this pirate has a target so create 2 little fighters that escaped the explosion
			for i = 1,kPirateLittleOnesOnDeath do
				local littlepirate = SpawnPirate("shiptype/little_pirate",unpack(pirate.mvPos))
				table.insert(gPirates,littlepirate)
				-- the last target of the dying pirate is also the target of the new ones
				littlepirate.target = pirate.target
				AutoPilot_Follow(littlepirate,pirate.target)
				-- set random start velocity
				littlepirate.mvVel = {Vector.add(vx,vy,vz,Vector.random3(25))}
			end
		end
	end
	
	function StepPirate (pirate)
		if ( pirate and pirate:IsAlive() ) then
			if ( not pirate.target ) then
				--print("pirate : StepPirate : no target")
				-- local k,v = next(gServerPlayers) doing nothing until attacked
				-- TODO : aggro radius ?
				if gServerPlayers then
					for k,o in pairs(gServerPlayers) do
						if o.body and o.body:IsAlive() and o.body:GetRootLocation() == pirate:GetRootLocation() then
							local x,y,z = unpack(o.body.mvPos)
							local dist = Vector.len(Vector.sub(x,y,z,unpack(pirate.mvPos)))
							if dist <= kPirateAggroRad then
								-- awake the pirate and go hunting
								AutoPilot_Follow(pirate,o.body)
								pirate.target = o.body
							end
						end
					end
				end
			elseif (not pirate.target:IsAlive() ) then
				--print("pirate : StepPirate : target dead")
				AutoPilot_Stop(pirate)
				pirate.target = nil
				
				PirateChat(pirate,"kill")
			else	
				-- hunting target
				local x1,y1,z1 = unpack(pirate.target.mvPos)
				local x2,y2,z2 = unpack(pirate.mvPos)
				local qw,qx,qy,qz = Quaternion.getRotation(0,0,-1,x1-x2,y1-y2,z1-z2) 
				qw,qx,qy,qz = Quaternion.normalise(qw,qx,qy,qz)
				pirate.mqTurn = {1,0,0,0} -- todo : calc delta as per second, to enable other clients smoothing/prediction
				pirate.mqRot = {qw,qx,qy,qz}
				pirate:MarkChanged()
				FirePrimaryWeapons(pirate,pirate.target) -- target might be dead after this
			end
		end
	end
	
	RegisterListener("Hook_Object_Damage",PiratesDamageNotify)
	
	RegisterListener("Hook_MainStep",PiratesStep)
	
	RegisterListener("Hook_Object_Destroy",PiratesDestroy)
end
