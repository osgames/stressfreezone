-- a library of container types (e.g. cargo, weaponslots, engineslots...)
-- see also lib.object.container.lua
-- see also common.objecttypes.lua
-- see also lib.plugin.lua
-- RegisterContainerType(name,parentname,arr)


function LoadCommonContainerTypes ()
	RegisterContainerType("containertype/base",			false,					{name=""})
	
	RegisterContainerType("containertype/cargo",		"containertype/base",	{name="cargo"	,})
	
	-- cargo is the only inactive container. bActiveEquipment == true if the equipped items cause an effect (ie. energy decrease)
	RegisterContainerType("slottype",					"containertype/base",	{name="slot", bIsSlot=true, bActiveEquipment=true, size=1})
	RegisterContainerType("slottype/shieldsystem",		"slottype",				{name="shieldsystem"	,})
	RegisterContainerType("slottype/energy/reactor",	"slottype",				{name="reactor"	,})
	RegisterContainerType("slottype/energy/accumulator","slottype",				{name="accumulator"	,})
	RegisterContainerType("slottype/weapon",			"slottype",				{name="weapon"	, bIsWeaponSlot=true})
	RegisterContainerType("slottype/armor",				"slottype",				{name="armor"	,})
end

RegisterListener("Hook_PreLoad",function () LoadCommonContainerTypes() end)	
