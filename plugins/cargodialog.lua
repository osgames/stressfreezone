-- handles the plain text like cargo dialog

gOwnCargoDialog = nil
gOwnCargoDialogX, gOwnCargoDialogY = 50,50

function CloseCargoDialog ()
	if (gOwnCargoDialog) then gOwnCargoDialog:Destroy() gOwnCargoDialog = nil end
end

function OpenCargoDialog (x,y)
	if not gClientBody then return end
	
	CloseCargoDialog()
	local o = gClientBody
	local cargoinfo = sprintf("Cargo: %d/%d spaceleft:%d",	o and o:GetCurCargo() or 0,	o and o:GetMaxCargo() or 0,o and o:GetCargoSpaceLeft() or 0)
	local rows = { { 
		{ cargoinfo},
		{ ""},
		{ "Equipment",OpenOwnEquipmentDialog },
		{ "close",CloseCargoDialog },
		} }
	
	
	function GetCargoDialogClickMultiplier ()
		return gKeyPressed[key_lshift] and 100 or 1
	end
	-- 
	
	for cargotype,amount in pairs(gClientBody:GetCargoList() or {}) do 
		 if (amount > 0) then
			local iCargoTypeId = cargotype.type_id
			local row = { 
				{ cargotype.name},
				{ amount},
				
				{ "jettison", function () ClientRequestJettisonCargo(iCargoTypeId,GetCargoDialogClickMultiplier() * 1) end },
				-- TODO : stuff like use/equip.. maybe right click menu would be better ?
			}
			if (gClientBody:CanEquip(cargotype)) then 
				table.insert(row,{ "equip", function () ClientRequestEquipCargo(iCargoTypeId,GetCargoDialogClickMultiplier() * 1) end })
			end
			
			table.insert(rows,row)
		end
	end
	gOwnCargoDialog = guimaker.MakeTableDlg(rows,x,y,true,false,gGuiDefaultStyleSet,"window")
	gOwnCargoDialog.rootwidget.onDestroy = function()
		gOwnCargoDialogX, gOwnCargoDialogY = gOwnCargoDialog.rootwidget.gfx:GetPos()
	end

end

RegisterListener("Hook_Client_OpenOwnCargoDialog",function () 
	if (gOwnCargoDialog) then 
		CloseCargoDialog()
	else 
		OpenCargoDialog(gOwnCargoDialogX, gOwnCargoDialogY)
	end
end)

RegisterListener("Hook_Object_SetContainerContent",function (container,objecttype,fCurAmount) 
	local obj = container:GetObject()
	if (obj ~= gClientBody) then return end
	if (not gOwnCargoDialog) then return end
	-- re-open dialog on the same position
	local x = gOwnCargoDialog.rootwidget.gfx:GetDerivedLeft()
	local y = gOwnCargoDialog.rootwidget.gfx:GetDerivedTop()
	OpenCargoDialog(x,y)
end)
