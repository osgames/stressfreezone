-- a library of common object types
-- see also lib.object.lua
-- see also lib.plugin.lua
-- see also plugins/common.equipmenttypes.lua
-- RegisterObjectType(name,parentname,arr)

-- list of ship particle system parameters
glShipParticleSystem = {}
glShipParticleSystem["engine"] = {
	name = "TravelEllipseParticles", psize = 2, pvel = 10,
}
glShipParticleSystem["rocket"] = {
	name = "TravelEllipseParticles", psize = 1, pvel = 10,
}


-- list of ship trail parameters
glShipTrail = {}
glShipTrail["engine"] = {
	len = 500, elem = 50, mat = "trail",
	r = 1, g = 1, b = 1, a = 0.25,
	dr = 0, dg = 0, db = 0, da = 0.1,
	w = 1, dw = 0.2
}
glShipTrail["rocket"] = {
	len = 200, elem = 20, mat = "trail",
	r = 1, g = 0, b = 0, a = 0.25,
	dr = 0, dg = 0, db = 0, da = 0.1,
	w = 1, dw = 0.2
}

kFurniturePreviewSize = 48

function LoadCommonObjectTypes ()
	local sunsize     	= 800
	local planetsize  	= 100
	local stationsize 	=  20
	local shipsize  	=   5 -- asteroid, ship
	local smallsize   	=   2 -- rocket, box
	local humansize   	=   0.5 -- spacesuit
	local asteroidhue	= {hex2rgb("#b16a00")} -- rgb
	
	local containertypes_cargo = {["containertype/cargo"]=1}
	
	local h = 0.5
	local ladder_geom = { 	{-h,-h,0,  h,-h,0, -h,h,h,  h,h,h, },
							{ h,-h,0, -h,-h,0,  h,h,h, -h,h,h, },}
	local ladder_texcoords = { {0,1, 1,1, 0,0, 1,0}, {0,1, 1,1, 0,0, 1,0}, }
	local grate_geom = { 	{-h,-h,-h,  h,-h,-h, -h,-h,h,  h,-h,h, },
							{ h,-h,-h, -h,-h,-h,  h,-h,h, -h,-h,h, },}
	local grate_texcoords = { {0,1, 1,1, 0,0, 1,0}, {0,1, 1,1, 0,0, 1,0}, }
	local door_geom = { 	{-h,-h,h,  h,-h,h, -h,h,h,  h,h,h, },
							{ h,-h,h, -h,-h,h,  h,h,h, -h,h,h, },}
	local door_texcoords = { {0,1, 1,1, 0,0, 1,0}, {0,1, 1,1, 0,0, 1,0}, }

	RegisterObjectType("objecttype",				false,{name="thing",preloader=ObjectTypeDefaultPreLoader,ingame_col={0.5,0.5,0.5,1},hitrad=5,hp=100,gfx_billboardrad=100,gfx_billboard="billboard/particle_round.png",phys_mass=100,phys_radius=10,constructor=ObjectTypeDefaultConstructor},true) -- abstract

	RegisterObjectType("box",						"objecttype",	{name="box",	containertypes=containertypes_cargo,		gfx_mesh="box.mesh",	cargo_max=10000		})
	
	
	RegisterObjectType("furniture",					"objecttype",	{hp=10,	bFurniture=true, hitrad=0.5, iPreviewSize=kFurniturePreviewSize},true) -- abstract
	RegisterObjectType("furniture/engine_middle",	"furniture",	{name="engine_middle",	gfx_mesh="sd_engine_middle.mesh",			})
	RegisterObjectType("furniture/fridge",			"furniture",	{name="fridge",			gfx_mesh="fridge.mesh",			})
	RegisterObjectType("furniture/lamp",			"furniture",	{name="lamp",			gfx_mesh="ad_lampe.mesh",			})
	RegisterObjectType("furniture/kiste",			"furniture",	{name="kiste",			gfx_mesh="ad_kiste.mesh",			})
	RegisterObjectType("furniture/engine_end",		"furniture",	{name="engine_end",		gfx_mesh="sd_engine_end.mesh",			})
	RegisterObjectType("furniture/egg",				"furniture",	{name="egg",			gfx_mesh="sd_egg.mesh",			})
	RegisterObjectType("furniture/flower",			"furniture",	{name="flower",			gfx_mesh="sd_flower.mesh",			})
	RegisterObjectType("furniture/table",			"furniture",	{name="table",			gfx_mesh="sd_table.mesh",			})
	RegisterObjectType("furniture/couch",			"furniture",	{name="couch",			gfx_mesh="sd_couch.mesh",hue={1.0,0,0}	})
	RegisterObjectType("furniture/computer",		"furniture",	{name="computer",		gfx_mesh="sd_computer.mesh"	})
	RegisterObjectType("furniture/generator",		"furniture",	{name="generator",		gfx_mesh="sd_generator.mesh"	})
	RegisterObjectType("furniture/ladder",			"furniture",	{name="ladder",			bIsLadder=true,		gfx_geom=ladder_geom,	gfx_geom_texcoords=ladder_texcoords, gfx_geom_texname="gb_ladder.png" })
	RegisterObjectType("furniture/grid",			"furniture",	{name="grid",			bIsPlatform=true,	gfx_geom=grate_geom,	gfx_geom_texcoords=grate_texcoords, gfx_geom_texname="gb_grate.png" })
	RegisterObjectType("furniture/door",			"furniture",	{name="door",			bIsDoor=true,		gfx_geom=door_geom,		gfx_geom_texcoords=door_texcoords, gfx_geom_texname="gb_door.png" })
	
	-- gfx_meshrad=0.5
	RegisterObjectType("furniture/biotube",			"furniture",	{name="biotube",		gfx_mesh="ke_gpl3_biotubeCube.002.mesh",	})
	RegisterObjectType("furniture/walllamp",		"furniture",	{name="walllamp",		gfx_mesh="ke_gpl_walllamp.mesh",				})
	RegisterObjectType("furniture/fusionreactor",	"furniture",	{name="fusionreactor",	gfx_mesh="ct_pd_fusion_reactor_Cylinder.001.mesh",				})
	RegisterObjectType("furniture/sandcaster",		"furniture",	{name="sandcaster",		gfx_mesh="ct_pd_sandcaster_Cube.001.mesh",				})
	RegisterObjectType("furniture/fuelcell",		"furniture",	{name="fuelcell",		gfx_mesh="ct_pd_fuelcell_Cylinder.mesh",				})
	
	RegisterObjectType("furniture/handrail",		"furniture",	{name="handrail",		gfx_mesh="ct_pd_handrail.mesh",				})
	RegisterObjectType("furniture/pilotsseat",		"furniture",	{name="pilotsseat",		gfx_mesh="ct_pd_pilotsseat.mesh",			})
	RegisterObjectType("furniture/sodamachine",		"furniture",	{name="sodamachine",	gfx_mesh="ct_pd_sodamachine.mesh",			})
	RegisterObjectType("furniture/toaster",			"furniture",	{name="toaster",		gfx_mesh="ct_pd_toaster.mesh",				})
	RegisterObjectType("furniture/television",		"furniture",	{name="television",		gfx_mesh="ct_pd_tv.mesh",				})
	RegisterObjectType("furniture/counter",			"furniture",	{name="counter",		gfx_mesh="ct_pd_counter.mesh",			})
	RegisterObjectType("furniture/countercorner",	"furniture",	{name="counter corner",	gfx_mesh="ct_pd_counter_corner.mesh",	})
	RegisterObjectType("furniture/locker",	"furniture",	{name="locker",	gfx_mesh="ke_gpl3_locker.mesh",	})
	
	local tradelist_common = {
		{offer={["weapontype/projectile/rocket"]=1}		,cost={["cargo/iron"]=50}},
		{offer={["weapontype/laser/red"]=1}		,cost={["cargo/iron"]=50}},
		{offer={["weapontype/laser/green"]=1}	,cost={["cargo/iron"]=60}},
		{offer={["weapontype/laser/blue"]=1}	,cost={["cargo/iron"]=60}},
		{offer={["cargo/copper"]=1}				,cost={["cargo/iron"]=20}},
		{offer={["cargo/titanium"]=1}			,cost={["cargo/iron"]=40}},
		{offer={["equipmenttype/shield/battery"]=1}			,cost={["cargo/iron"]=100}},
		{offer={["equipmenttype/shield/battery/big"]=1}		,cost={["cargo/iron"]=500}},
		{offer={["equipmenttype/shield/charger"]=1}			,cost={["cargo/iron"]=500}},
		{offer={["equipmenttype/energy/reactor/fusion"]=1}	,cost={["cargo/iron"]=800}},
		{offer={["equipmenttype/energy/accumulator/nc"]=1}	,cost={["cargo/iron"]=400}},
		
		}
		
	local services_common = {"service/repair"}
	local containertypes_common = { 
		["containertype/cargo"]=1, 
		["slottype/energy/reactor"]=1, 
		["slottype/energy/accumulator"]=4, 
		["slottype/shieldsystem"]=4, 
		["slottype/weapon"]=6, 
		["slottype/armor"]=2 }
	
	RegisterObjectType("waypoint",					"objecttype",	{name="waypoint", ingame_col={0,0.78,0,1}, phys_mass=1,phys_radius=1, name_short="wp", gfx_billboardrad=10, gfx_billboard="billboard/waypoint.png", hp=1 })
	
	RegisterObjectType("asteroid",					"objecttype",	{ingame_col={0.3,0.3,0.3,1}, phys_mass=10,phys_radius=10, name_short="a", loot={["cargo/iron"]=20}},true) -- abstract
	RegisterObjectType("planet",					"objecttype",	{ingame_col={0.3,0.3,1.0,1}, phys_mass=1000,phys_radius=100, name="planet",	bHasSpacePort=true,services=services_common,tradelist=tradelist_common,hp=10000},true) -- abstract
	RegisterObjectType("station",					"objecttype",	{ingame_col={0.0,0.0,1.0,1}, phys_mass=100,phys_radius=50, bHasSpacePort=true,services=services_common,tradelist=tradelist_common,hp=1000},true) -- abstract
	RegisterObjectType("sat",						"objecttype",	{ingame_col={0.0,0.5,0.5,1}},true) -- abstract
	
	RegisterObjectType("asteroid/asteroid1",		"asteroid",		{name="asteroid",	gfx_meshrad=shipsize,		gfx_mesh="ke_gpl_asteroid_crystal_v2_Sphere.mesh" })
	RegisterObjectType("asteroid/asteroid2",		"asteroid",		{name="asteroid",	gfx_meshrad=shipsize,		gfx_mesh="stone2.mesh",hue=asteroidhue	})
	RegisterObjectType("asteroid/asteroid3",		"asteroid",		{name="asteroid",	gfx_meshrad=shipsize,		gfx_mesh="stone3.mesh",hue=asteroidhue	})
	RegisterObjectType("asteroid/asteroid4",		"asteroid",		{name="asteroid",	gfx_meshrad=shipsize,		gfx_mesh="stone4.mesh",hue=asteroidhue	})
	RegisterObjectType("asteroid/asteroid5",		"asteroid",		{name="asteroid",	gfx_meshrad=shipsize,		gfx_mesh="stone1.mesh",hue=asteroidhue	})
	
	RegisterObjectType("station/station1",			"station",		{name="station",	gfx_meshrad=stationsize,	gfx_mesh="spacestation.mesh",	})
	RegisterObjectType("station/station2",			"station",		{name="station",	gfx_billboardrad=stationsize,	gfx_billboard="xotusbill/spacestation.png",		})
	RegisterObjectType("station/station3",			"station",		{name="station",	gfx_billboardrad=stationsize,	gfx_billboard="xotusbill/weltraumboje.png",		})
	RegisterObjectType("station/station4",			"station",		{name="station",	gfx_billboardrad=stationsize,	gfx_billboard="xotusbill/liftende.png",			})
	RegisterObjectType("station/station5",			"station",		{name="station",	gfx_billboardrad=stationsize,	gfx_billboard="xotusbill/xotus4mondstation_stat.png",	})

	RegisterObjectType("planet/xotus1",				"planet",		{					gfx_billboardrad=planetsize,	gfx_billboard="xotusbill/xotus1.png",			})
	RegisterObjectType("planet/xotus2",				"planet",		{					gfx_billboardrad=planetsize,	gfx_billboard="xotusbill/xotus2.png",			})
	RegisterObjectType("planet/xotus3",				"planet",		{					gfx_billboardrad=planetsize,	gfx_billboard="xotusbill/xotus3.png",			})
	RegisterObjectType("planet/xotus4",				"planet",		{					gfx_billboardrad=planetsize,	gfx_billboard="xotusbill/xotus4.png",			})
	RegisterObjectType("planet/xotus5",				"planet",		{					gfx_billboardrad=planetsize,	gfx_billboard="xotusbill/xotus5.png",			})
	RegisterObjectType("planet/moon1",				"planet",		{name="moon",		gfx_billboardrad=planetsize,	gfx_billboard="xotusbill/xotus4_mond3.png",		})
	
	RegisterObjectType("roundplanet",				"planet",			{gfx_planetradius=planetsize},true) -- abstract
	RegisterObjectType("planet/inferno",			"roundplanet",		{gfx_planetmap="Inferno02.png",})
	RegisterObjectType("planet/terran",				"roundplanet",		{gfx_planetmap="Terran02.png",})
	RegisterObjectType("planet/barren",				"roundplanet",		{gfx_planetmap="Barren03.png",})
	RegisterObjectType("planet/barren/red",			"planet/barren",	{hue={hex2rgb("#ff0000")}})
	RegisterObjectType("planet/desert",				"roundplanet",		{gfx_planetmap="Desert01.png",})
	RegisterObjectType("planet/gas/brown",			"roundplanet",		{gfx_planetmap="GasGiant08.png",})
	RegisterObjectType("planet/gas/mixed",			"roundplanet",		{gfx_planetmap="GasGiant01.png",})
	RegisterObjectType("planet/gas/bright",			"roundplanet",		{gfx_planetmap="GasGiant02.png",})
	RegisterObjectType("planet/gas/dark",			"roundplanet",		{gfx_planetmap="GasGiant09.png",})
	RegisterObjectType("planet/gas/inferno",		"roundplanet",		{gfx_planetmap="Inferno01.png",})
	
	RegisterObjectType("planet/gaia",				"roundplanet",		{gfx_planetmap="Gaia.png",})
	
	
	--[[
	planet/xotus1", "Jupiter",	"
	planet/xotus4", "Saturn",	"
	planet/xotus5", "Uranus",	"
	planet/xotus3", "Neptune",	"
	]]--
	
	
	RegisterObjectType("sat/sat1",					"sat",			{name="satellite",	gfx_billboardrad=shipsize,		gfx_billboard="xotusbill/satellit.png",			})
	RegisterObjectType("sat/sat2",					"sat",			{name="satellite",	gfx_billboardrad=shipsize,		gfx_billboard="xotusbill/satellit2.png",		})
	
	RegisterObjectType("sun",						"objecttype",	{name="sun",		hp=10000,	gfx_billboardrad=sunsize,		gfx_billboard="xotusbill/sonne.png",			gfx_billboard_additive=true})
	RegisterObjectType("wormhole",					"objecttype",	{name="wormhole",	hp=10000,	gfx_billboardrad=planetsize,	gfx_billboard="xotusbill/wurmlochklein.png",	})
	
	-- ships  
	-- TODO : energystuff
	RegisterObjectType("shiptype",						"objecttype",			{containertypes=containertypes_common, phys_mass=100,phys_radius=10, ingame_col={0.0,1.0,0.0,1}, accel=100, max_speed=50, bIsShip=true, cargo_max=100, loot={["cargo/iron"]=10}, energy_max=100, energy_rate=10, shield_max=100, shield_rate=5, spawncallback=ShipSpawnCallBack, startequipment=gCommonStartEquipment},true) -- abstract
	--RegisterObjectType("stuff/spacesuit",				"shiptype",				{name="spacesuit",	hp=5, accel=5, bIsSpaceSuit=true, cargo_max=1, shield_max=0, startequipment=gCommonStartEquipment_empty, gfx_billboardrad=humansize, gfx_billboard_aspect=0.5, gfx_billboard2={"spaceman_front.png","spaceman_back.png"},	})
	RegisterObjectType("shiptype/spacesuit",			"shiptype",				{name="spacesuit",	phys_mass=5,phys_radius=1, hp=5, accel=5, bIsSpaceSuit=true, cargo_max=1, energy_max=100, energy_rate=10, shield_max=0, startequipment=gCommonStartEquipment_weak, gfx_mesh="sd_spaceman.mesh",	 shipEngineParticleSize=0.5, shipEngineParticleVel=0.25})
	RegisterObjectType("shiptype/razor",				"shiptype",				{name="razor",		gfx_mesh="razor.mesh",			})
	RegisterObjectType("shiptype/sphereglider",			"shiptype",				{name="glider",		gfx_mesh="sphereglider.mesh",	})
	
	RegisterObjectType("shiptype/customship",			"shiptype",				{name="customship",	constructor=ObjectTypeCustomShipConstructor,
			trails = {{x=0,y=0,z=0,name="engine"}}, 
			particles = {{x=0,y=0,z=0,name="engine"}},
		},true) -- abstract type
	
	RegisterObjectType("shiptype/little_pirate",		"shiptype/customship",	{name="little_pirate" , 	gfx_shipfile="little_pirate.lua", hp=50, energy_max=10, energy_rate=2, shield_max=25, ingame_col={1.0,0.0,0.0,1}, startequipment=gCommonStartEquipment_weak,
			trails = {
				{x=-0.5,	y=0.5,	z=-0.5, name="engine"},
			},
			particles = {
				{x=-0.5,	y=0.5,	z=-0.5, name="engine"},
			},	
	})
	
	RegisterObjectType("shiptype/pirate",				"shiptype/customship",	{name="pirate" , 	gfx_shipfile="pirate.lua", hp=100, energy_max=100, energy_rate=10, shield_max=100, ingame_col={1.0,0.0,0.0,1}, startequipment=gCommonStartEquipment,
			trails = {
				{x=-0.5,	y=0.5,	z=2.5, name="engine"},
			},
			particles = {
				{x=-0.5,	y=0.5,	z=2.5, name="engine"},
			},	
	})
	
	RegisterObjectType("shiptype/bigpirate",			"shiptype/customship",	{name="pirate" , 	gfx_shipfile="pirate.lua", hp=300, energy_max=100, energy_rate=15, shield_max=250, ingame_col={1.0,0.0,0.0,1}, startequipment={["weapontype/laser/blue"]=4},
			trails = {
				{x=-0.5,	y=0.5,	z=2.5, name="engine"},
			},
			particles = {
				{x=-0.5,	y=0.5,	z=2.5, name="engine"},
			},	
	})
	
	RegisterObjectType("shiptype/megapirate",			"shiptype/customship",	{name="pirate" , 	gfx_shipfile="pirate.lua", hp=300, energy_max=200, energy_rate=20, shield_max=250, ingame_col={1.0,0.0,0.0,1}, startequipment={["weapontype/laser/red"]=2,["weapontype/laser/blue"]=1,["weapontype/laser/green"]=2,["weapontype/projectile/nutrocket"]=1},
			trails = {
				{x=-0.5,	y=0.5,	z=2.5, name="engine"},
			},
			particles = {
				{x=-0.5,	y=0.5,	z=2.5, name="engine"},
			},	
	})
	
	RegisterObjectType("shiptype/customship/firefly",	"shiptype/customship",	{name="firefly",		gfx_shipfile="firefly.lua", hitrad=10, bHasHangar=true,hp=500, energy_max=100, energy_rate=10, shield_max=500, cargo_max=1000, startequipment=gCommonStartEquipment_weak,
			trails = {
				{x=0.5,	y=2.5,	z=12.5, name="engine"},
			},
			particles = {
				{x=0.5,	y=2.5,	z=12.5, name="engine"},
			},
	  })
	RegisterObjectType("shiptype/customship/falcon",	"shiptype/customship",	{name="falcon",		gfx_shipfile="tr_gpl3_falcon.lua", hitrad=10, bHasHangar=true,hp=500, energy_max=100, energy_rate=10, shield_max=500, cargo_max=1000, startequipment=gCommonStartEquipment_weak,
			trails = {
				{x=0.5,	y=2.5,	z=12.5, name="engine"},
			},
			particles = {
				{x=0.5,	y=2.5,	z=12.5, name="engine"},
			},
	  })
	RegisterObjectType("shiptype/customship/xwing",		"shiptype/customship",	{
		name="xwing",		gfx_shipfile="xwing.lua", hitrad=5, 
		bHasHangar=true,hp=50, energy_max=100, energy_rate=10, 
		shield_max=200, cargo_max=100, startequipment=gCommonStartEquipment_strong,
		trails = {
			{x=-5.5,	y=2.5,	z=5.5, name="engine"},
			{x=-5.5,	y=-1.5,	z=5.5, name="engine"},
			{x=5.5,		y=2.5,	z=5.5, name="engine"},
			{x=7.5,		y=-1.5,	z=5.5, name="engine"},
		},
		particles = {
			{x=-5.5,	y=2.5,	z=5.5, name="engine"},
			{x=-5.5,	y=-1.5,	z=5.5, name="engine"},
			{x=5.5,		y=2.5,	z=5.5, name="engine"},
			{x=7.5,		y=-1.5,	z=5.5, name="engine"},
		},
	})
	
	RegisterObjectType("shiptype/customship/fighter",	"shiptype/customship",	{name="fighter",		gfx_shipfile="fighter.lua",
			trails = {
				{x=-0.5,	y=0.5,	z=-0.5, name="engine"},
			},
			particles = {
				{x=-0.5,	y=0.5,	z=-0.5, name="engine"},
			},
		})
	RegisterObjectType("shiptype/customship/hagship",	"shiptype/customship",	{name="hagship", gfx_shipfile="hagship.lua",
			trails = {
				{x=-3.5,	y=0.5,	z=3.5, name="engine"},
				{x=2.5,		y=0.5,	z=3.5, name="engine"},
				{x=-1.5,	y=0.5,	z=3.5, name="engine"},
				{x=0.5,		y=0.5,	z=3.5, name="engine"},
			},
		})
	RegisterObjectType("shiptype/customship/ghoulship",	"shiptype/customship",	{name="ghoulship",	gfx_shipfile="ghoulship.lua",
			trails = {
				{x=-0.5,	y=0.5,	z=-0.5, name="engine"},
			},
			particles = {
				{x=-0.5,	y=0.5,	z=-0.5, name="engine"},
			},
		})
	RegisterObjectType("shiptype/customship/satellite",	"shiptype/customship",	{name="satellite",	gfx_shipfile="satellite.lua",	startequipment=gCommonStartEquipment,
			trails = {
				{x=-3.5,	y=0.5,	z=-0.5, name="engine"},
				{x=3.5,	y=0.5,	z=-0.5, name="engine"},
			},
			particles = {
				{x=-3.5,	y=0.5,	z=-0.5, name="engine"},
				{x=3.5,	y=0.5,	z=-0.5, name="engine"},
			},
		})
	RegisterObjectType("shiptype/customship/mothership",	"shiptype/customship",	{name="mothership",	gfx_shipfile="mothership.lua",	hitrad=20, bHasHangar=true,hp=1000, energy_max=100, energy_rate=10, shield_max=1000, cargo_max=5000, startequipment=gCommonStartEquipment,
			trails = {
				{x=0.5,	y=3.5,	z=37.5, name="engine"},
				{x=0.5,	y=4.5,	z=37.5, name="engine"},
				{x=0.5,	y=2.5,	z=37.5, name="engine"},
				{x=1.5,	y=3.5,	z=37.5, name="engine"},
				{x=-0.5,	y=3.5,	z=37.5, name="engine"},
			},
			particles = {
				{x=0.5,	y=3.5,	z=37.5, name="engine"},
				{x=0.5,	y=4.5,	z=37.5, name="engine"},
				{x=0.5,	y=2.5,	z=37.5, name="engine"},
				{x=1.5,	y=3.5,	z=37.5, name="engine"},
				{x=-0.5,	y=3.5,	z=37.5, name="engine"},
			},	
		})
	
	
	-- cargo types
	-- todo : also equipment ?
	-- weight must always be positive (> 0)
	
	RegisterObjectType("cargo",				false,			{name="unknown_cargo", weight=1, bIsCargo=true},true) -- abstract
	RegisterObjectType("cargo/iron",		"cargo",		{name="iron"})
	RegisterObjectType("cargo/copper",		"cargo",		{name="copper"})
	RegisterObjectType("cargo/titanium",	"cargo",		{name="titanium"})
	
	RegisterObjectType("missioncargo",		"cargo",		{bIsMissionCargo=true},true) -- abstract
	RegisterObjectType("cargo/supplies",	"missioncargo",	{name="supplies"})
	RegisterObjectType("cargo/equipment",	"missioncargo",	{name="equipment"})
	RegisterObjectType("cargo/medicine",	"missioncargo",	{name="medicine"})
	RegisterObjectType("cargo/food",		"missioncargo",	{name="food"})
	RegisterObjectType("cargo/drugs",		"missioncargo",	{name="drugs"})
	RegisterObjectType("cargo/chemicals",	"missioncargo",	{name="chemicals"})
	RegisterObjectType("cargo/flax",		"missioncargo",	{name="flax"})
	RegisterObjectType("cargo/coffee",		"missioncargo",	{name="coffee"})
	
	NotifyListener("Hook_CommonObjectTypesLoaded")
end



function ObjectTypeDefaultPreLoader (t)
	if (t.gfx_geom) then
		local gfx = CreateGfx3D()
		GfxSetGeom(gfx,t.gfx_geom,t.gfx_geom_texcoords,1,1,1) 
		gfx:SetMaterial(GetTexturedMat("alphareject_base",t.gfx_geom_texname))
		local sMeshName = gfx:RenderableConvertToMesh()
		gfx:Destroy()	
		t.gfx_mesh = sMeshName
		--print("ObjectTypeDefaultPreLoader : generated mesh for geom ",t.type_name,sMeshName)
	end

	if (t.gfx_mesh) then
		-- correct bounds manually, seems to be wrong sometimes
		local meshname = t.gfx_mesh
		local x1,y1,z1,x2,y2,z2 = MeshReadOutExactBounds(meshname)
		local boundrad = math.max(Vector.len(x1,y1,z1),Vector.len(x2,y2,z2))
		MeshSetBounds(meshname,x1,y1,z1,x2,y2,z2)
		MeshSetBoundRad(meshname,boundrad)
		
		-- rescale calc
		t.meshscale = t.gfx_meshrad and (t.gfx_meshrad / boundrad) or 1
		local typeboundrad = t.gfx_meshrad or boundrad
		t.gfx_meshrad = t.typeboundrad
		
		-- generate mesh preview using rtt : render to texture
		if (t.iPreviewSize and (not gNoObjectPreviews)) then
			local filename = string.gsub(t.type_name,"/","_")..".png"
			local filepath = kObjTypePreviewDir..filename
			if (file_exists(filepath) and kLoadObjectPreviewFromCacheIfPossible) then
				--print("ObjectTypeDefaultPreLoader load from cache",filepath)
				t.gfx_previewtex = filename
			else
				--print("ObjectTypeDefaultPreLoader generate and save to cache",kObjTypePreviewDir..filename)
				local texname,rtt = GetMeshPreview(t.gfx_mesh,t.iPreviewSize,nil,nil,pixelformat)
				t.gfx_previewtex = texname
				--texname = "xotusbill/spacestation.png"
				if (kLoadObjectPreviewFromCacheIfPossible) then
					if (rtt) then rtt:WriteContentsToFile(kObjTypePreviewDir..filename) end
					--if (texname) then SaveTextureToFile(texname,kObjTypePreviewDir.."tex_"..filename) end
				end
			end
			t.gfx_previewmat = t.gfx_previewtex and GetPlainTextureMat(t.gfx_previewtex)
		end
		
		-- calc
		t.gfx_maxrenderdist = GetRenderingDistanceForPixelSize(typeboundrad,3)
	end
end


-- returns one equipment item (container,objtype) that will be deactivated if the power is low (searches for items consuming power)
function GetEquipmentToDeactivate(obj)
	for containertype,containerlist in pairs(obj:GetContainerListByType() or {}) do 
		if ContainerTypeFilter_ActiveEquipment(containertype) then
			for k,container in pairs(containerlist) do 
				for objtype,amount in pairs(container.content) do 
					if (objtype.equipment_add_energyrate or 0) < 0 and amount>0 then
						return container,objtype
					end
				end
			end
		end
	end
end

-- attaches the ship trails and particle emitters
function AttachTrailsAndParticles (obj)
	local t = obj.objtype
	
	-- ship engine particles
	if t.particles then
		for k,o in pairs(t.particles) do
			-- particle parameter available?
			if glShipParticleSystem[o.name] then
				local parentgfx = obj.gfx
				local gfx = parentgfx and parentgfx:CreateChild() or CreateRootGfx3D()
				
				local varname = "particle_" .. k
				obj[varname] = gfx
				local particle = glShipParticleSystem[o.name]
				local dx,dy,dz = o.x,o.y,o.z
				local x,y,z = unpack(obj.mvPos)
				
				gfx:SetParticleSystem(particle.name)
				local psize = particle.psize
				local pvel = particle.pvel
				obj[varname]:ParticleSystem_SetDefaultParticleSize(psize,psize)
				obj[varname]:ParticleSystem_SetEmitterVelocityMinMax(0,pvel,pvel)
				obj[varname]:SetPosition(dx,dy,dz)

				obj:AddToDestroyList(gfx)

				RegisterListener("Hook_MainStep",function()
					if obj and obj:IsAlive() and gfx and gfx:IsAlive() then
						local vel = Vector.len(unpack(obj.mvVel))
						local fRate = math.min(150,vel)
						obj[varname]:ParticleSystem_SetEmitterRate(0,fRate)
					else 
						print("DEBUG","particle engine is dead")
						return true
					end
				end)

				-- RegisterStepper(ShipEngineParticleStepper,obj) 
			end
		end
	end

	-- trail
	if t.trails then
		for k,o in pairs(t.trails) do
			-- trail parameter available?
			if glShipTrail[o.name] then
				local parentgfx = obj.gfx
				local gfx = CreateRootGfx3D() --parentgfx and parentgfx:CreateChild() or CreateRootGfx3D()
				local varname = "trail_" .. k
				obj[varname] = gfx
				local trail = glShipTrail[o.name]
				local dx,dy,dz = o.x,o.y,o.z
				local x,y,z = unpack(obj.mvPos)
				gfx:SetTrail(
					x+dx,y+dy,z+dz,
					trail.len,trail.elem,trail.mat, -- 500,	50,	"trail", --len,elem,mat,
					trail.r,trail.g,trail.b,trail.a, -- 1,	1,	1,	0.5, --r,g,b,a,
					trail.dr,trail.dg,trail.db,trail.da, -- 0,	0,	0,	0.2, --dr,dg,db,da,
					trail.w,trail.dw -- 1, 	0.2 --w,dw
				)
				
				RegisterListener("Hook_MainStep",function()
					if obj and obj:IsAlive() and obj[varname] and obj[varname]:IsAlive() then
						local x,y,z = obj:LocalToGlobal(dx,dy,dz)
						obj[varname]:SetPosition(x,y,z)
					else 
						return true
					end
				end)
				
				-- delayed deletion of trails
				obj:AddToDestroyNotifyList(function()
					InvokeLater(5*1000, function()
						if gfx and gfx:IsAlive() then gfx:Destroy() end	
					end)
				end)
			end
		end
	end	
end

function ShipSpawnCallBack (obj)
	local t = obj.objtype
	
	if (t.max_speed) then 
		obj.mfMaxSpeed = t.max_speed
	end
	
	if (gbServerRunning and t.startequipment) then 
		for objtypename,num in pairs(t.startequipment) do 
			for i=1,num do obj:EquipOrAddToCargo(objtypename) end 
		end
		
		RegisterListener("Hook_MainStep",function ()
			if not (obj:IsAlive()) then return true end
			if obj:GetEnergy() <= 0 and obj:GetEnergyRate() < 0 then
				-- GetActiveEquipmentSum("equipment_add_energyrate")
				-- ContainerTypeFilter_ActiveEquipment(t) ContainerTypeFilter_ActiveEquipment(t)
				local c,t = GetEquipmentToDeactivate(obj)
				if c then obj:Unequip(c,t) end
			end
		end)
	end
	
	AttachTrailsAndParticles(obj)
	
	--[[
	if (false and not t.bIsSpaceSuit) then
		local parentgfx = obj.gfx
		local gfx = CreateRootGfx3D() --parentgfx and parentgfx:CreateChild() or CreateRootGfx3D()
		obj.enginetrail = gfx
		local x,y,z = unpack(obj.mvPos)
		gfx:SetTrail(
			x,y,z,
			500,	50,	"trail", --len,elem,mat,
			1,	1,	1,	0.5, --r,g,b,a,
			0,	0,	0,	0.2, --dr,dg,db,da,
			1, 	0.2 --w,dw
		)
		obj:AddToDestroyList(obj.enginetrail)
		RegisterListener("Hook_MainStep",function()
			if obj and obj:IsAlive() then
				if obj.enginetrail then
					obj.enginetrail:SetPosition(unpack(obj.mvPos))
				end
			else 
				return true
			end
		end)
	end
	]]--
end

function ShipEngineParticleStepper (obj)
	if ((not obj.engineparticles) or (not obj.engineparticles:IsAlive())) then return true end -- object or particlesystem dead
	local vel = Vector.len(unpack(obj.mvVel))
	local fRate = math.min(150,vel)
	obj.engineparticles:ParticleSystem_SetEmitterRate(0,fRate * 100)
end


function ObjectTypeDefaultConstructor (t,parentgfx)
	local gfx = parentgfx and parentgfx:CreateChild() or CreateRootGfx3D()
	if (t.gfx_planetmap) then 
		local meshgengfx = CreateGfx3D()
		GfxSetSphere(meshgengfx,t.gfx_planetradius)
		meshgengfx:SetMaterial(GetPlainTextureMat(t.gfx_planetmap))
		t.gfx_mesh = meshgengfx:RenderableConvertToMesh()
		t.gfx_meshrad = t.gfx_planetradius
		meshgengfx:Destroy()	
	end
	
	if (t.gfx_mesh) then 
		local s = t.meshscale or 1
		local meshgfx = gfx
		-- don't scale gfx directly, this would also affect effects and other stuff attached to it
		if (s ~= 1) then meshgfx = gfx:CreateChild() end
		meshgfx:SetMesh(t.gfx_mesh)
		if (s ~= 1) then 
			meshgfx:SetScale(s,s,s)
			meshgfx:SetNormaliseNormals(s ~= 1)
		end
		gfx.mesh = meshgfx
		if (t.hue) then HueMeshEntity(meshgfx,unpack(t.hue)) end
		
		if (t.gfx_maxrenderdist) then
			gfx:SetRenderingDistance(t.gfx_maxrenderdist)
			gfx.mesh:SetRenderingDistance(t.gfx_maxrenderdist)
		end
	elseif (t.gfx_geom) then
		--local gfx = CreateRootGfx3D()
		GfxSetGeom(gfx,t.gfx_geom,t.gfx_geom_texcoords,1,1,1) 
		gfx:SetMaterial(GetTexturedMat("alphareject_base",t.gfx_geom_texname))
		--t.gfx_mesh = gfx:RenderableConvertToMesh()
		--gfx:Destroy()
	elseif (t.gfx_billboard2) then
		-- these billboards don't orient themselves to the cam.
		gfx.billboard_rad = t.gfx_billboardrad or 10
		local y = t.gfx_billboardrad
		local x = y*t.gfx_billboard_aspect
		for k,texname in pairs(t.gfx_billboard2) do
			local matname = GetTexturedMat("culled_billboard_base",texname)
			local stackpartgfx = gfx:CreateChild()
			
			stackpartgfx:SetSimpleRenderable()
			stackpartgfx:RenderableBegin(4,6,false,false,OT_TRIANGLE_LIST)
			stackpartgfx:RenderableVertex(-x,-y,0, 0,1)
			stackpartgfx:RenderableVertex( x,-y,0, 1,1)
			stackpartgfx:RenderableVertex(-x, y,0, 0,0)
			stackpartgfx:RenderableVertex( x, y,0, 1,0)
			if (k==2) then
				stackpartgfx:RenderableIndex3(0,1,2)
				stackpartgfx:RenderableIndex3(1,3,2)
				gfx.frontgfx = stackpartgfx
			else
				stackpartgfx:RenderableIndex3(2,1,0)
				stackpartgfx:RenderableIndex3(2,3,1)
				gfx.backgfx = stackpartgfx
			end
			stackpartgfx:RenderableEnd()
			stackpartgfx:SetMaterial(matname)
		end
	elseif (t.gfx_billboard) then
		gfx.billboard_rad = t.gfx_billboardrad or 10
		gfx:SetBillboard(0,0,0,gfx.billboard_rad,GetBillBoardMat(t.gfx_billboard,t.gfx_billboard_additive))
	end
	return gfx
end

-- creates a custiom ship graphic
-- gfx_shipfile : contains the custom ship file
function ObjectTypeCustomShipConstructor (t,parentgfx)
	local gfx = parentgfx and parentgfx:CreateChild() or CreateRootGfx3D()
	if (t.gfx_shipfile) then 
		LoadShipToGfx(gfx,gShipDirPath .. t.gfx_shipfile)
	else
		gfx.childs = {}
	end
	return gfx
end

RegisterListener("Hook_PreLoad",function () LoadCommonObjectTypes() end)
