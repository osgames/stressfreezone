-- plugin for a list dialog listing stations and planets
-- see also lib.plugin.lua
-- see also menubar.lua

function CloseBaseListDialog ()
	if (gBaseListDialog) then gBaseListDialog:Destroy() gBaseListDialog = nil end
end

RegisterListener("Hook_Client_OpenBaseListDialog",function () 
	CloseBaseListDialog()
	
	local rows = { { 
		{ type="Label",	text="BaseList"},
		{ type="Label",	text=""},
		{ type="Button",text="close",on_button_click=CloseBaseListDialog },
		} }
	
	local list = GetObjectList(ObjFilter_IsBase)  -- todo : sort by dist
	for k,obj in pairs(list) do
		local myobj = obj
		table.insert(rows,{ 
				{ type="Button",text="select",on_button_click=function () ClientSetTarget(myobj) end },
				{ type="Label",	text=obj.objtype.name},
				{ type="Label",	text=gClientBody and sprintf("%8.0f",obj:GetDistToObject(gClientBody) or "n/a" ) },
				}
			)
	end
	
	
	local x,y = 10,50
	gBaseListDialog = guimaker.MakeTableDlg(rows,x,y,true,false,gGuiDefaultStyleSet)
end)

