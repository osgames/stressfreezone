-- handles weapons and other equipment
-- see also common.objecttypes.lua

kDefaultEquipmentIcon = "rn_pd_afterburner.png" 
kEquipIconCX,kEquipIconCY = 48,48

gCheatObjTypes = {}
gCheatObjTypes_AllEquipment = true
gCheatObjTypes["cargo/iron"]							= 20
gCheatObjTypes["equipmenttype/shield/battery"]			= 1
gCheatObjTypes["equipmenttype/shield/battery/big"]		= 1
gCheatObjTypes["equipmenttype/shield/charger"]			= 1
gCheatObjTypes["equipmenttype/energy/reactor/fusion"]	= 1
gCheatObjTypes["equipmenttype/energy/accumulator/nc"]	= 1
gCheatObjTypes["equipmenttype/armor/titanium"]			= 1

gCheatObjTypes["weapontype/laser/red"]					= 1
gCheatObjTypes["weapontype/laser/green"]				= 1
gCheatObjTypes["weapontype/laser/blue"]					= 1
gCheatObjTypes["weapontype/projectile/plasmaglobber"]			= 1

gCheatObjTypes["projectile/rocket"]						= 5

gCommonStartEquipment_empty		= {}
--gCommonStartEquipment_strong	= {["weapontype/laser/red"]=4,["weapontype/projectile/nutrocket"]=1}		
gCommonStartEquipment_strong	= {["weapontype/projectile/starrocket"]=1}		
gCommonStartEquipment			= {["weapontype/laser/red"]=2}		
--~ gCommonStartEquipment_weak		= {["weapontype/laser/red"]=1}	-- firefly has this
gCommonStartEquipment_weak		= {["weapontype/laser/red"]=1}	-- firefly has this
	
	
	
-- should be done after LoadCommonObjectTypes
function LoadCommonEquipmentTypes ()
	local sunsize     	= 800
	local planetsize  	= 100
	local stationsize 	=  20
	local shipsize  	=   5 -- asteroid, ship
	local smallsize   	=   2 -- rocket, box
	local humansize   	=   0.5 -- spacesuit
	
	RegisterObjectType("equipmenttype",							"cargo",			{name="unknown_equipment",				slottype="slottype/equipment", 			constructor=false, bIsEquipment=true},true) -- abstract
	RegisterObjectType("equipmenttype/shield/battery",			"equipmenttype",	{name="shield-battery",					slottype="slottype/shieldsystem", 		equipment_add_shieldmax=100})
	RegisterObjectType("equipmenttype/shield/battery/big",		"equipmenttype",	{name="big shield-battery",				slottype="slottype/shieldsystem", 		equipment_add_shieldmax=200})
	RegisterObjectType("equipmenttype/shield/charger",			"equipmenttype",	{name="shield-charger",					slottype="slottype/shieldsystem",		equipment_add_shieldrate=50, equipment_add_energyrate=-5})
	RegisterObjectType("equipmenttype/energy/reactor/fusion",	"equipmenttype",	{name="fusion-reactor",					slottype="slottype/energy/reactor",		equipment_add_energyrate=50})
	RegisterObjectType("equipmenttype/energy/accumulator/nc",	"equipmenttype",	{name="nickel-cadmium-accumulator",		slottype="slottype/energy/accumulator", equipment_add_energymax=100})
	RegisterObjectType("equipmenttype/armor/titanium",			"equipmenttype",	{name="titanium-armor",					slottype="slottype/armor", 				equipment_add_hullmax=200})
	
	-- active equipment must be activated to have an effect, it appears in the main-slotbar 
	RegisterObjectType("equipmenttype/active",					"equipmenttype",	{ bIsActive=true, icon=kDefaultEquipmentIcon},true) -- abstract
	
	
	-- weapon types
	-- see also lib.object.weapon.lua
	-- (needed) fire_weapon_fun (sx,sy,sz,ship,weapon,target,vx,vy,vz) : see lib.object.weapon.lua
	-- (needed) fire_possible_fun (weapon,ship,target) : check if its possible to fire (range, ressource... check)
	-- (needed) fire_acquire_res_fun (weapon,ship) : acquire the ressources (remove energy...)
	-- TODO : weapontype : projectile object_or_effect, damage, cooldown, mountpoint?, maybe cost ? powerdrain etc...
	-- TODO : plasma.jpg shot01.jpg , additive blend for energy weapons
	-- TODO : projectile weapons  (first:plasma : billboards, later:gatling:short beam gfx for bullets)
	--RegisterObjectType("stuff/cash",			"objecttype",	{name="cash",			gfx_billboardrad=shipsize,		gfx_billboard="billboard/loot.png",				})

	RegisterObjectType("weapontype",				"equipmenttype/active",	{name="unknown_weapon",icon="rn_pd_projectile.png",slottype="slottype/weapon", constructor=false, bIsWeapon=true,  fire_possible_fun = FireCheckEnergyRange, fire_acquire_res_fun = FireAcquireEnergy},true) -- abstract
	
	RegisterObjectType("weapontype/laser",			"weapontype",		{name="laser",		icon="rn_pd_laser.png", jumps=0, energy=10,  damage=10, range=2000, iFireInterval=500, effecttype="effect/laser/red", fire_weapon_fun=FireWeaponLaser})
	RegisterObjectType("weapontype/laser/red",		"weapontype/laser",	{name="laser-red",	energy=10,  damage=20, range= 500,	effecttype="effect/laser/red"})
	RegisterObjectType("weapontype/laser/green",	"weapontype/laser",	{name="laser-green",energy=10,  damage=10, range=1000,	effecttype="effect/laser/green"})
	RegisterObjectType("weapontype/laser/blue",		"weapontype/laser",	{name="laser-blue",	energy=10,  damage= 5, range=2000,	effecttype="effect/laser/blue"})

	--RegisterObjectType("weapontype/laser/chainblue",		"weapontype/laser/blue",	{name="chain-laser-blue",	icon="rn_pd_chainlightning.png", jumps=5,	energy=10,  damage= 5, range=2000,	effecttype="effect/laser/blue", jump_weapontype="weapontype/laser/blue", fire_weapon_fun=FireWeaponChain})

	RegisterObjectType("weapontype/lightning",		"weapontype/laser",	{name="lightning",	icon="rn_pd_lightning.png", energy=100,  damage= 100, range=200,	effecttype="effect/lightning"})
	
	RegisterObjectType("weapontype/chainlightning",	"weapontype/laser",	{name="chainlightning",	jumps=2, jump_weapontype="weapontype/lightning", fire_weapon_fun=FireWeaponChain, icon="rn_pd_chainlightning.png", energy=25, range=500	})

	RegisterObjectType("weapontype/projectile",		"weapontype",		{name="projectile",		range=200000, damage=50, lifetime=10, acceleration=20, explodedistance=50, damagerange=50, iFireInterval=500, fire_weapon_fun=FireWeaponProjectile	}, true) -- abstract
	
	RegisterObjectType("weapontype/projectile/rocket",	"weapontype/projectile",	{name="rocketlauncher",			projectile_type="projectile/rocket", 		icon="rn_pd_missile.png",
		iFireInterval=500, startspeed=50, damage=50,	energy=50,		lifetime=30,	acceleration=10,	explodedistance=50, damagerange=50 , bGuided=true})
	RegisterObjectType("weapontype/projectile/bigrocket",	"weapontype/projectile",	{name="bigrocketlauncher",	projectile_type="projectile/rocket",		icon="rn_pd_missile_strong.png",
		iFireInterval=500, startspeed=50, damage=150,	energy=150,		lifetime=30,	acceleration=10,	explodedistance=150, damagerange=150 , bGuided=true})
	
	RegisterObjectType("weapontype/projectile/nutrocket",	"weapontype/projectile/rocket",	{name="nutrocketlauncher",	projectile_type="projectile/rocket", 	icon="rn_pd_rocket_split1.png",
		iFireInterval=500, energy=50,	splittime=2*1000, splitcount=5, splitspread=2, splitprojectile_weapontype="weapontype/projectile/rocket", fire_weapon_fun=FireWeaponProjectileSplit})
	
	RegisterObjectType("weapontype/projectile/starrocket",	"weapontype/projectile/rocket",	{name="starrocketlauncher",	projectile_type="projectile/rocket", 	icon="rn_pd_rocket_split_star.png",
		iFireInterval=500, energy=50,	splittime=2*1000, splitcount=5, splitspread=2, splitprojectile_weapontype="weapontype/projectile/rocket", fire_weapon_fun=FireWeaponProjectileSplitMultiTarget})
	
	RegisterObjectType("weapontype/projectile/plasmaglobber",	"weapontype/projectile",	{name="plasmaglobber", 	projectile_type="projectile/plasmaglob", 	icon="rn_pd_plasma_glob.png",
		iFireInterval=200, startspeed=200, damage=50,energy=10,	lifetime=2,		acceleration=0,		explodedistance=0,  damagerange=10 , bLineCollision=true })
	
	
	-- projectiles are the objects being fired by projectile 
	RegisterObjectType("projectile",				"objecttype",	{name="projectile", weight=1, spawncallback=AttachTrailsAndParticles, bIsCargo=true }, true) -- abstract
	RegisterObjectType("projectile/plasmaglob",		"projectile",	{name="plasmaglob", gfx_billboardrad=humansize*2,	gfx_billboard="plasma.png", gfx_billboard_additive=true})
	RegisterObjectType("projectile/rocket",			"projectile",	{name="rocket", gfx_mesh="rocket.mesh", max_speed=60,
			trails		= { {x=0,	y=0,	z=0, name="rocket"}, },		
			particles	= { {x=0,	y=0,	z=0, name="rocket"}, },	   })
	
end

RegisterListener("Hook_CommonObjectTypesLoaded",function () LoadCommonEquipmentTypes() end)
	
