-- a library of property types (e.g. shield, energy...)
-- see also lib.object.property.lua
-- RegisterPropType(name,parentname,arr)


function LoadCommonPropTypes ()
	RegisterPropType("proptype",			false,		{})
	
	RegisterPropType("proptype/hull",		"proptype",	{})
	RegisterPropType("proptype/shield",		"proptype",	{})
	RegisterPropType("proptype/energy",		"proptype",	{})
	RegisterPropType("proptype/name",		"proptype",	{})
end


RegisterListener("Hook_PreLoad",function () LoadCommonPropTypes() end)

