-- a library of service types
-- see also lib.service.lua
-- see also lib.plugin.lua
-- RegisterServiceType(name,parentname,arr)

function LoadCommonServiceTypes ()
	RegisterServiceType("service/base",		false,			{name="stuff"})
	RegisterServiceType("service/repair",	"service/base",	{name="repair",descr="repair the hull of your ship", server_fun=Service_Repair_Server})
	--RegisterServiceType("service/smelting",	"service/base",	{name="smelting",	descr="produce a metal from its ore"})
	--RegisterServiceType("service/trade",		"service/base",	{name="trade",		descr="exchange goods and equipment"})
	--RegisterServiceType("service/missions",	"service/base",	{name="missions",	descr="bounty hunting and parcel service"})
end

--[[
	trade :
	{name="repair",descr="restores 10 hp",cost={["cargo/iron"]=10},onbuy=function (player) player.body:RepairHull(10) end}
	{cargo_or_weapon="weapontype/laser/red",cost={["cargo/iron"]=50}}
	excel-like-table, with cost as html with icon+tooltipp+color_affordable ?
	
	
	sfz :	economy/commerce/industry type:
		planet-types (ocean,desert,rocky,ice,water..)
		station types : solar,bio,asteroid/mining,sience,nebulae,military,tradepost,orbital...
		produces, consumes...	
		technologylevel -> available upgrades and equipment
		
]]--

function Service_Repair_Server (player,servicetype,obj,...)
	--print("Service_Repair_Server",player,obj)
	local body = player.body
	if ((not obj) or (not obj:IsAlive())) then return end 
	if ((not body) or (not body:IsAlive())) then return end 
	local range = 500
	local dist = body:GetDistToObject(obj)
	if (dist > range) then 
		ServerServiceSendError(player,servicetype,obj,"range must be smaller than "..range.." for repair")
		return
	end
	local cost_cargotype = GetObjectType("cargo/iron") assert(cost_cargotype)
	local cost_cargoname = cost_cargotype.name
	local player_has = math.floor(body:GetCargo(cost_cargotype))
	local player_needs = math.floor(body:GetHullMax() - body:GetHull())
	if (player_needs <= 0) then
		ServerServiceSendError(player,servicetype,obj,"your ship does not need repairs")
		return
	end
	if (player_has <= 0) then
		ServerServiceSendError(player,servicetype,obj,"you cannot afford repairing your ship, you need "..cost_cargoname)
		return
	end
	
	local repairdone = math.floor(math.min(player_has,player_needs))
	
	body:RepairHull(repairdone)
	body:SetCargo(cost_cargotype,body:GetCargo(cost_cargotype) - repairdone) 
	ServerServiceSendMessage(player,servicetype,obj,sprintf("ship repaired, cost : %d %s",repairdone,cost_cargoname))
end


RegisterListener("Hook_PreLoad",function () LoadCommonServiceTypes() end)
