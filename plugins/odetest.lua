
---- test code ---------------------------------------
---- test code ---------------------------------------
---- test code ---------------------------------------
---- test code ---------------------------------------

kOdeTestTableCamSpeedFactor = 0.02
kInitialOdeTestThirdPersonDist = 50

function CreateOdeTestBox (x,y,z)
	local o = {}
	o.bodyid = OdeBodyCreate(gOdeWorld)
	o.geomid = OdeCreateBox(gOdeSpace, 1,1,1)
	o.massid = OdeMassCreate()
	
	OdeMassSetBoxTotal(o.massid, 1, 1, 1, 1)
	OdeBodySetMass(o.bodyid, o.massid)
	OdeGeomSetBody(o.geomid, o.bodyid) 
  	OdeBodySetPosition(o.bodyid,x,y,z)

  	o.gfx = CreateRootGfx3D()
  	o.gfx:SetMesh("box.mesh")
  	s = 0.5
  	o.gfx:SetScale(s,s,s)
  	
  	o.GetPosition = function(self)
  		return OdeBodyGetPosition(self.bodyid)
  	end
  	
  	o.Step = function(self)
  		local w,x,y,z
  		
  		x,y,z = OdeBodyGetPosition(self.bodyid)
  		self.gfx:SetPosition(x,y,z)
  		
  		w,x,y,z = OdeBodyGetQuaternion(self.bodyid)
  		self.gfx:SetOrientation(w,x,y,z)
  		-- print("Box[",self,"]:Step()",x,y,z)
  	end
  	
  	o.Reset = function(self)
  		OdeBodySetPosition(self.bodyid,x,y,z)
  		OdeBodySetQuaternion(self.bodyid,0,0,0,1)
  		OdeBodySetLinearVel(self.bodyid,0,0,0)
  		OdeBodySetAngularVel(self.bodyid,0,0,0)
  		OdeBodySetForce(self.bodyid,0,0,0)
  		OdeBodySetTorque(self.bodyid,0,0,0)
  		OdeBodyEnable(self.bodyid)
  	end
  	
  	o.MoveTo = function(self,x,y,z)
  		local px,py,pz = OdeBodyGetPosition(self.bodyid)
  		local fx,fy,fz = Vector.normalise(x - px, y - py, z - pz)
  		fx,fy,fz = Vector.scalarmult(fx,fy,fz,0.5)
  		OdeBodyAddForceAtRelPos(self.bodyid,fx,fy,fz,0,0,0)
  	end

	return o
end

function CreateOdeBuggy (x,y,z)
	local o = {}
	
	local l,w,h = 3.5,2.5,1
	local r = 0.5
	local wmass = 1
	local cmass = 20
	local comoffset = -5
	local fmax = 25
	
	local dx,dy,dz = -l/2,-w/2,-h/2
	
	-- chassis body
  	o.bodyid = OdeBodyCreate(gOdeWorld)
  	o.massid = OdeMassCreate()
 	o.geomid = OdeCreateBox(gOdeSpace,l,w,h)

  	OdeBodySetPosition(o.bodyid,x,y,z)
  	OdeMassSetBox(o.massid,cmass / 2,l,w,h)
  	OdeBodySetMass(o.bodyid,o.massid)
 	OdeGeomSetBody(o.geomid,o.bodyid)

	---o.spaceid = OdeSimpleSpaceCreate(gOdeSpace)
	---OdeSpaceSetCleanup(o.spaceid,0)
	---OdeSpaceAdd(o.spaceid,o.geomid)

  	o.gfx = CreateRootGfx3D()
  	
  	local gfx = o.gfx:CreateChild()
  	SetBeamBox(gfx, 0.1, l,w,h, 1,1,1,1) 
	gfx:SetPosition(dx, dy, dz)
  	
  	o.wheels = {}
	
  	local wheelmesh = MakeSphereMesh(10,10,r,r,r)

	-- wheel bodies
	for i = 1,4 do
		local wheel = {}
		wheel.bodyid = OdeBodyCreate(gOdeWorld)
		wheel.massid = OdeMassCreate()
		wheel.geomid = OdeCreateSphere(gOdeSpace,r)

		-- OdeBodySetQuaternion(wheel.bodyid,Quaternion.fromAngleAxis(1,0,0,1))
		OdeMassSetSphere(wheel.massid,wmass,r)
		OdeBodySetMass(wheel.bodyid,wheel.massid)
		OdeGeomSetBody(wheel.geomid,wheel.bodyid)
		
		wheel.gfx = o.gfx:CreateChild()
		wheel.gfx:SetMesh(wheelmesh)
		
		table.insert(o.wheels,wheel)
	end
	
	OdeBodySetPosition(o.wheels[1].bodyid, x+0.4*l-0.5*r, y+w*0.5, z-h*0.75)
	OdeBodySetPosition(o.wheels[2].bodyid, x+0.4*l-0.5*r, y-w*0.5, z-h*0.75)
	OdeBodySetPosition(o.wheels[3].bodyid, x-0.4*l+0.5*r, y+w*0.5, z-h*0.75)
	OdeBodySetPosition(o.wheels[4].bodyid, x-0.4*l+0.5*r, y-w*0.5, z-h*0.75)

	o.wheels[1].gfx:SetPosition(0+0.4*l-0.5*r, 0+w*0.5, 0-h*0.75)
	o.wheels[2].gfx:SetPosition(0+0.4*l-0.5*r, 0-w*0.5, 0-h*0.75)
	o.wheels[3].gfx:SetPosition(0-0.4*l+0.5*r, 0+w*0.5, 0-h*0.75)
	o.wheels[4].gfx:SetPosition(0-0.4*l+0.5*r, 0-w*0.5, 0-h*0.75)
	
	o.joints = {}
	
	-- front and back wheel hinges
	for i = 1,4 do
		local joint = {}
		
		joint.jointid = OdeJointCreateHinge2(gOdeWorld,0)
		
		OdeJointAttach(joint.jointid,o.bodyid,o.wheels[i].bodyid)
		
		local ax,ay,az = OdeBodyGetPosition(o.wheels[i].bodyid)
		
		OdeJointSetHinge2Anchor(joint.jointid,ax,ay,az)
		
		local p
		if i < 2 then p = 1 else p = -1 end 
		
		OdeJointSetHinge2Axis1(joint.jointid,0,0,p)
		OdeJointSetHinge2Axis2(joint.jointid,0,1,0)
		OdeJointSetHinge2Param(joint.jointid,OdeParamSuspensionERP,0.8)
		OdeJointSetHinge2Param(joint.jointid,OdeParamSuspensionCFM,1e-5)
		OdeJointSetHinge2Param(joint.jointid,OdeParamVel2,0)
		OdeJointSetHinge2Param(joint.jointid,OdeParamFMax2,fmax)
		
		table.insert(o.joints,joint)
	end
	
	-- center of mass offset body. (hang another copy of the body COMOFFSET units below it by a fixed joint)

	local b = OdeBodyCreate(gOdeWorld)
	local m = OdeMassCreate()
	local j = OdeJointCreateFixed(gOdeWorld, 0)
	OdeBodySetPosition(b,x,y,z+comoffset)
	OdeMassSetBox(m,1,l,w,h)
	OdeMassAdjust(m,cmass/2)
	OdeBodySetMass(b,m)
	OdeJointAttach(j, o.bodyid, b)
	OdeJointSetFixed(j)
	
	-- OdeBodySetQuaternion(o.bodyid,Quaternion.fromAngleAxis(math.pi * 1,1,0,0))
	  	
   	o.GetPosition = function(self)
  		return OdeBodyGetPosition(self.bodyid)
  	end

	o.Step = function(self)
  		self:Move()

  		local x,y,z,w
  		
  		x,y,z = OdeBodyGetPosition(self.bodyid)
  		self.gfx:SetPosition(x,y,z)
  		
  		gCamThirdPersonX,gCamThirdPersonY,gCamThirdPersonZ = x,y,z
  		
  		w,x,y,z = OdeBodyGetQuaternion(self.bodyid)
  		self.gfx:SetOrientation(w,x,y,z)
  		-- print("Box[",self,"]:Step()",x,y,z)
   	end
  	
  	o.Reset = function(self)
  		OdeBodySetPosition(self.bodyid,x,y,z)
		OdeBodySetQuaternion(self.bodyid,Quaternion.fromAngleAxis(-math.pi * 0.5,1,0,0))
  		OdeBodySetLinearVel(self.bodyid,0,0,0)
  		OdeBodySetAngularVel(self.bodyid,0,0,0)
  		OdeBodySetForce(self.bodyid,0,0,0)
  		OdeBodySetTorque(self.bodyid,0,0,0)
  		OdeBodyEnable(self.bodyid)
  		self.speed = 0
		self.turn = 0
		
		for k,v in pairs(self.wheels) do
	  		OdeBodySetLinearVel(v.bodyid,0,0,0)
	  		OdeBodySetAngularVel(v.bodyid,0,0,0)
	  		OdeBodySetForce(v.bodyid,0,0,0)
	  		OdeBodySetTorque(v.bodyid,0,0,0)
	  		OdeBodyEnable(v.bodyid)
		end
		
		for k,j in pairs(self.joints) do
			OdeJointSetHinge2Param(j.jointid,OdeParamVel,0)
			OdeJointSetHinge2Param(j.jointid,OdeParamFMax,OdeInfinity)
			OdeJointSetHinge2Param(j.jointid,OdeParamVel2,0)
			OdeJointSetHinge2Param(j.jointid,OdeParamFMax2,fmax)
			OdeBodyEnable(OdeJointGetBody(j.jointid,0))
			OdeBodyEnable(OdeJointGetBody(j.jointid,1))
		end
  	end
  	
  	o.MoveTo = function(self,x,y,z)
  		local px,py,pz = OdeBodyGetPosition(self.bodyid)
  		local fx,fy,fz = Vector.normalise(x - px, y - py, z - pz)
  		fx,fy,fz = Vector.scalarmult(fx,fy,fz,0.5)
  		OdeBodyAddForceAtRelPos(self.bodyid,fx,fy,fz,0,0,0)
  	end

	o.speed = 0
	o.turn = 0
	
	o.NoTurn = function(self)
		self.turn = 0
		self.speed = 0
	end
	o.Accel = function(self)
		self.speed = 2
	end
	o.Brake = function(self)
		self.speed = -2
	end
	o.TurnLeft = function(self)
		self.turn = -0.3
	end
	o.TurnRight = function(self)
		self.turn = 0.3
	end
	o.Move = function(self)
		for k,j in pairs(self.joints) do
			local curturn = OdeJointGetHinge2Angle1 (j.jointid)
			OdeJointSetHinge2Param(j.jointid,OdeParamVel,(self.turn-curturn)*1.0)
			OdeJointSetHinge2Param(j.jointid,OdeParamFMax,OdeInfinity)
			OdeJointSetHinge2Param(j.jointid,OdeParamVel2,self.speed)
			OdeJointSetHinge2Param(j.jointid,OdeParamFMax2,fmax)
			OdeBodyEnable(OdeJointGetBody(j.jointid,0))
			OdeBodyEnable(OdeJointGetBody(j.jointid,1))
		end
		-- print("turn",self.turn,"speed",self.speed)
	end
	
	return o
end

kBoxX,kBoxY,kBoxZ = -50,50,-50

function StartOdeTest ()
	Client_SetSkybox(nil)
	GetMainViewport():SetBackCol(0,0,0,1)
	if (gStarField) then gStarField:Destroy() gStarField = nil end
	
	gCamThirdPersonDist = kInitialOdeTestThirdPersonDist
	gOdeTest_Mat_On	= GetPlainColourMat(0,1,0) -- green
	gOdeTest_Mat_Off	= GetPlainColourMat(1,0,0) -- red
	
	gEditorLight = Client_AddPointLight(GetMainCam():GetPos())
	gCamThirdPersonX,gCamThirdPersonY,gCamThirdPersonZ = 0,0,0
	
	RegisterListener("mouse_left_click",function () OdeTest_Click() end)
	RegisterListener("Hook_MainStep",StepOdeTest)
	
	--~ gPlanetBaseTex = "sd_metall_6.png"
	gPlanetBaseTex = "ba_ccby_TiZeta_cem.jpg"
	--~ function MakePlanetGround (loc) return MakePlane(loc.gfx,100,50,gPlanetBaseTex) end
	local r,g,b = hex2rgb("#a19429")
	gHF = CreateRootGfx3D()
	gHF:SetPosition(0,0,0)
				
	InitOde(0.05)
	OdeWorldSetGravity (gOdeWorld,0,-0.5,0)

	glOdeObject = {}
	
	-- table.insert(glOdeObject,CreateOdeTestBox(1,0,10))
	-- table.insert(glOdeObject,CreateOdeTestBox(-1,0,10))
		
	-- create ode hightfield data
	--[[
	local data = OdeGeomTriMeshDataCreate()
	local rawdata = OdeTriMeshRawDataCreate(fifo)
	OdeTriMeshRawDataPrint(rawdata)
	OdeGeomTriMeshDataBuildFromRaw(data, rawdata)
	local trimesh = OdeCreateTriMesh(gOdeSpace, data)
	]]--
	local fifo = CreateFIFO()
	local hf = {}
	hf.gfx,hf.fun,hf.w,hf.h, hf.dx, hf.dz = MakeRandomHeightField(gHF,GetHuedMat(GetPlainTextureMat(gPlanetBaseTex),r,g,b), fifo) 
	local dx,dy,dz = -hf.h/2,0,-hf.w/2
	hf.gfx:SetPosition(dx,dy,dz)
	
	local mesh = MakeSphereMesh(5,5,0.5,0.5,0.5)

	local fun = function(x,z)
		local y = hf.fun(x,z)
		--[[
		local gfx = CreateRootGfx3D()
		gfx:SetMesh(mesh)
		gfx:SetPosition(x-hf.h/2,y,z-hf.w/2)
		print("HF",x,y,z)
		]]--
		return y
	end

	hf.data = OdeGeomHeightfieldDataCreate()
	OdeGeomHeightfieldDataBuildFromFun(hf.data,hf.w,hf.h,hf.dx,hf.dz,fun)
	hf.geomid = OdeCreateHeightfield(gOdeSpace,hf.data,1)
	-- OdeGeomSetQuaternion( hf.geomid, Quaternion.fromAngleAxis(90*gfDeg2Rad,0,0,1) )
	OdeGeomSetPosition( hf.geomid, 0,0,0 )

	OdeCreatePlane(gOdeSpace,0,1,0,-40)

	local x,y,z = 0,0,0
	y = hf.gfx:GetHeightAtPos(x - dx,z - dz) + 5
	--car = CreateOdeBuggy(x,y,z)
	--table.insert(glOdeObject,car)
	
	local d = 1.1
	local size = 3
	for a = 1,size do
		for b = 1,size do
			for c = 1,size do
				table.insert(glOdeObject,CreateOdeTestBox(
					kBoxX + a*d,
					hf.gfx:GetHeightAtPos(kBoxX - dx,kBoxZ - dz) + 5 + c*d,
					kBoxZ + b*d
				))
			end
		end
	end

	gOdeLastTimer = gMyTicks	
	
end

gCollisionSpots = {}
gCollisionMesh = nil
-- creates a little ball at the given position
function MarkCollisionSpot(x,y,z)
	if not gCollisionMesh then
		-- create mesh
		gCollisionMesh = MakeSphereMesh(3,3,0.3,0.2,0.2,1,0,0)
	end
	
	local gfx = CreateRootGfx3D()
	gfx:SetMesh(gCollisionMesh)
	gfx:SetPosition(x,y,z)
	table.insert(gCollisionSpots,gfx)
end

-- remove all but count spots
function ReduceCollisionSpots(count)
	while table.getn(gCollisionSpots) > count do
		local gfx = gCollisionSpots[1]
		gfx:Destroy()
		table.remove(gCollisionSpots,1)
	end
end

function StepOdeTest ()
	ReduceCollisionSpots(50)
	StepTableCam(GetMainCam(),gKeyPressed[key_mouse2] and (not gui.bMouseBlocked),kOdeTestTableCamSpeedFactor)
	StepThirdPersonCam(GetMainCam(),gCamThirdPersonDist,gCamThirdPersonX,gCamThirdPersonY,gCamThirdPersonZ)
	if gEditorLight then Client_SetLightPosition(gEditorLight,GetMainCam():GetPos()) end -- update light
	
	if car then
		car:NoTurn()
	
		if gKeyPressed[GetNamedKey("up")] then car:Accel() end
		if gKeyPressed[GetNamedKey("down")] then car:Brake() end
		if gKeyPressed[GetNamedKey("left")] then car:TurnLeft() end
		if gKeyPressed[GetNamedKey("right")] then car:TurnRight() end
	end
	
	if gKeyPressed[GetNamedKey("space")] then 
		for k,v in pairs(glOdeObject) do
			v:Reset()
		end
	end

	local cx,cy,cz = 0,0,0
	
  	for k,v in pairs(glOdeObject) do
		v:Step()
		
		-- damper movement
  		w = -0.02
  		x,y,z = OdeBodyGetLinearVel(v.bodyid)
  		OdeBodyAddForce(v.bodyid, x * w, y * w, z * w)
  		
  		w = -0.05
  		x,y,z = OdeBodyGetAngularVel(v.bodyid)
  		OdeBodyAddTorque(v.bodyid, x * w, y * w, z * w)
		
		-- v:MoveTo(kBoxX,kBoxY,kBoxZ)
		local x,y,z = v:GetPosition()
		cx = cx + x
		cy = cy + y
		cz = cz + z
	end

	local n = table.getn(glOdeObject)
	cx = cx / n
	cy = cy / n
	cz = cz / n
	gCamThirdPersonX,gCamThirdPersonY,gCamThirdPersonZ = cx,cy,cz
	
	StepOde()
end

function OdeTest_Click ()
	local rx,ry,rz,rvx,rvy,rvz = GetMouseRay()
	
	local ray = OdeCreateRay(gOdeSpace,1000)
	
	OdeGeomRaySet(ray,rx,ry,rz,rvx,rvy,rvz)
	
	for k,v in pairs(glOdeObject) do
		local l = OdeCollide(ray,v.geomid,1)
		if l then
			for k2,cg in pairs(l) do
				local px,py,pz, nx,ny,nz, depth, g1,g2, side1, side2 = OdeGetContactGeom(cg)
				
				MarkCollisionSpot(px,py,pz)
				
				local fx,fy,fz = Vector.normalise(rvx,rvy,rvz)
				fx,fy,fz = Vector.scalarmult(fx,fy,fz,10)
				
				if gKeyPressed[GetNamedKey("lshift")] then 
					fx,fy,fz = Vector.scalarmult(fx,fy,fz,-1)
				end
				
				OdeBodyEnable(v.bodyid)
				OdeBodyAddForceAtPos(v.bodyid,fx,fy,fz,px,py,pz)
			end
		end
	end
	
	OdeGeomDestroy(ray)
end
