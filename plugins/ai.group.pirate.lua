-- plugin for adding pirate ships attacking the player
-- see also lib.plugin.lua
--
-- Most of this is derived from the old pirate01.lua code
--

kPirateLittleOnesOnDeath = 2
kPirateAggroRad = 500


gPirateChatTimeout = 5 * 1000	-- 5 sec timeout between chats of one pirate
gPirateChat = {
	death = {	-- pirate died
		"Aaaaarrrrrrrrrggggggggghhhhhh!!!!",
		"See you in hell!",
		"You shouldn't have done that.",
		"You will regret that",
	},
	kill = {	-- pirate killed someone
		"Hahahahaa !!!",
		"From dust to dust.",
		"Suck vacuum!",
		"Busted your ass",
		"Your frozen corpse will drift in eternity",
		"Thats for messing with me",
		"Your debris will sparkle on my shields", 
	},
	hit = {	-- pirate got hit
		"That was a very very bad idea",
		"I'm immortal",
		"Is this all you can do ?",
		"My mommy hits harder",
	},
	critical_hit = {	-- pirate got hit, hulldamage > 0
		"I painted my ship yesterday!!! ARgh!",
		"You will pay for this!",
		"You bastard!",
		"Damn!",
		"Ouch!",
	}
}


function PirateChat(pirate,topic)
	-- only send chat if timeout reached
	if (
		not pirate.last_chat_timestamp or 
		pirate.last_chat_topic ~= topic or
		gMyTicks - pirate.last_chat_timestamp > gPirateChatTimeout
	) then
		-- update last chat time
		pirate.last_chat_timestamp = gMyTicks
		pirate.last_chat_topic = topic
		
		local l, name
		local msg = nil

		name = pirate:GetName()
		l    = gPirateChat[topic]

		-- read out random message
		if l then msg = l[math.random(table.getn(l))] end

		-- broadcasting messages to everyone might not be a good idea
		if msg then ServerBroadcastChatMessage(name .. ": " .. msg) end
	end
end

gAI_Pirate = {}

gAI_Pirate.Attack = function (self,target)
	self:SetState("Attacking", target)

	for k,v in pairs(self.ships) do
		AutoPilot_Follow(v,target)
		v.target = target
	end

	-- create the step function for the "Attack" state
	
	self.Step = function (self)

		for i,v in pairs(self.ships) do

			-- why the hell dosn't lua have a c-like continue statement?
			if v:IsAlive() then 
				if v.target and v.target:IsAlive() then
					-- hunting target
					
					-- TODO: shouldn't all this rotation code be part of AutoPilot_Follow?
					local x1,y1,z1 = unpack(v.target.mvPos)
					local x2,y2,z2 = unpack(v.mvPos)
					local qw,qx,qy,qz = Quaternion.getRotation(0,0,-1,x1-x2,y1-y2,z1-z2)
					qw,qx,qy,qz = Quaternion.normalise(qw,qx,qy,qz)
					-- todo : calc delta as per second, to enable other clients smoothing/prediction
					v.mqTurn = {1,0,0,0}
					v.mqRot = {qw,qx,qy,qz}
					v:MarkChanged()
					FirePrimaryWeapons(v, v.target) -- target might be dead after this
				else
					-- target is dead
					AutoPilot_Stop(v)
					v.target = nil
					-- return home
					self:Move(self:CountShipsAlive() * 5, unpack(self.homePos))
				end
			end
		end

		return
	end

	return
end

gAI_Pirate.Idle = function (self)
	self:SetState("Idle")

	-- TODO: make it scan for hostiles while idling, rather than waiting to get attacked
	self.Step = function (self) return end	

	return
end

gAI_Pirate.Move = function (self, radius, x, y, z)
	self:SetState("Moving", {x, y, z})

	for k,v in pairs(self.ships) do
		AutoPilot_GoTo(v, 0, Vector.random2(
		               x-radius,y-radius,z-radius,
		               x+radius,y+radius,z+radius)
		)
	end

	-- the autopilot code automaticly stops, so a blank stepper is fine
	self.Step = function (self) return end

end

gAI_Pirate.onDamage = function (ship, attacker, shielddamage, hulldamage)
	ship.group:Attack(attacker)
	ship.lastattacker = attacker

	if hulldamage > 0 then
		PirateChat(ship, "critical_hit")
	else 
		PirateChat(ship, "hit")
	end
end

gAI_Pirate.onDeath = function (ship)
	--[[
	if ship.objtype.type_name == "shiptype/pirate" then
		ship.group:AddShips( {["shiptype/little_pirate"]=kPirateLittleOnesOnDeath}, unpack( ship.mvPos))
	end
	]]--
	
	ship.group:Attack(ship.lastattacker)	

	PirateChat(ship, "death")
end


gAI_Pirate.Init   = function (self) return end
gAI_Pirate.Deinit = function (self) return end

-- Register the Group AI
RegisterGroupAI("Pirate", gAI_Pirate)
