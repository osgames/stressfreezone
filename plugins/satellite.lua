-- plugin for adding defense-sats attacking the pirates
-- see also lib.plugin.lua

local pluginenabled = false -- set this to true to enable the plugin

gSatellites = {}
gSatelliteNum = 1

if (pluginenabled) then  
	
	function SatellitesStep ()
		if (not gbServerRunning) then return end
		--print("sat count",table.getn(gSatellites))
		-- if (countarr(gSatellites) < gSatelliteNum) then print("create sat") table.insert(gSatellites,SpawnSatellite()) end
		for k,sat in pairs(gSatellites) do 
			if (sat:IsAlive()) then 
				StepSatellite(sat) 
			else 
				-- clean up sat
				gSatellites[k] = nil
				print("sat died",countarr(gSatellites))
			end
		end
	end
	
	function SpawnSatellite (player, iActionKeyID)
		if ( iActionKeyID == kActionKey_FireMissile and player and player.body and player.body:IsAlive() ) then
			local x,y,z = unpack(player.body.mvPos)
			local s = SpawnObject("shiptype/customship/satellite",player.body:GetParentLocation(),{x,y,z},{Quaternion.random()})
			s.iOwnerPlayerId = player.id
			s.fWeaponRange = s:GetMaxWeaponRange()
			
			table.insert(gSatellites,s)
		end
	end
	
	function StepSatellite (sat)
		if (not sat.target or not sat.target:IsAlive() ) then
			-- search new target
			local x1,y1,z1 = unpack(sat.mvPos)
			
			local l = sat.location:IntersectSphere(x1,y1,z1,sat.fWeaponRange)
			
			for k,v in pairs(l) do 
				local o = GetObject(v)
				
				if o then
					local skip = false
					-- is this the owner?
					if ( o.player and o.player.id == sat.iOwnerPlayerId ) then skip = true end
					-- is this a friend? (owner by the owner)
					if ( o.iOwnerPlayerId and o.iOwnerPlayerId == sat.iOwnerPlayerId ) then skip = true end
					
					if not skip then
						-- new target found
						sat.target = o
						break
					end
				end
			end
			
		else	
			-- hunting target
			local x1,y1,z1 = unpack(sat.target.mvPos)
			local x2,y2,z2 = unpack(sat.mvPos)
			local dist = Vector.len(x1-x2,y1-y2,z1-z2)
			
			local qw,qx,qy,qz = Quaternion.getRotation(0,0,-1,x1-x2,y1-y2,z1-z2) 
			qw,qx,qy,qz = Quaternion.normalise(qw,qx,qy,qz)
			sat.mqTurn = {1,0,0,0} -- todo : calc delta as per second, to enable other clients smoothing/prediction
			sat.mqRot = {qw,qx,qy,qz}
			sat:MarkChanged()
			
			if (dist < sat.fWeaponRange) then
				FirePrimaryWeapons(sat,sat.target) -- target might be dead after this
			else
				-- lose target
				sat.target = nil
			end
		end
	end
	
	RegisterListener("Hook_MainStep",SatellitesStep)
	RegisterListener("Hook_Server_PlayerActionKey",SpawnSatellite)
end
