-- a library of shippart types
-- see also lib.shipedit.lua
-- see also lib.plugin.lua
-- RegisterShipPartType(name,parentname,arr)
-- todo : description, skinlist,user-hueable,preview image, interior, rotation constraints, slots

function LoadCommonShipPartTypes ()
	local h = 0.5 -- half
	local texcoords_pyramid_0       = {
			{0,0, h,0, 0,1},
			{0,0, 0,1, 1,0},
			{0,0, 1,0, 0,1},
			{0,1, 1,0, 1,1},
		} -- 4 triangles : sd_cockpit_seite_1.png
	local texcoords_ramp_0 = {
			{0,h, 0,1, 1,h},
			{0,h, 1,h, 0,1},
			{0,0, 0,h, 1,0, 1,h},
			{1,1, 0,1, 1,h, 0,h},
			{0,h, h,h, 0,1, h,1},
		} -- 2 triangles and 3 quads : sd_cockpit_mitte_1.png
	
	
	RegisterShipPartType("shippart",			false,		{gfx_geom=kGeomBox,cx=1,cy=1,cz=1,	texcoords=kTexCoords_Box0,		preloader=ShipPartTypeDefaultPreLoader,constructor=ShipPartTypeDefaultConstructor})
	RegisterShipPartType("shippart/box",		"shippart",	{bIsBox=true, gfx_tex="gh_metal2.png"})
	RegisterShipPartType("shippart/engine",		"shippart",	{gfx_tex="sd_metall_8.png"})
	RegisterShipPartType("shippart/pyramid2",   "shippart", {gfx_geom=kGeomPyramid,cz=2, texcoords=texcoords_pyramid_0,  gfx_mat="CockPitCubeMapTest",   })
	RegisterShipPartType("shippart/pyramid1",   "shippart", {gfx_geom=kGeomPyramid,      texcoords=texcoords_pyramid_0,  gfx_mat="CockPitCubeMapTest",   })
	RegisterShipPartType("shippart/ramp2",      "shippart", {gfx_geom=kGeomRamp,cz=2,    texcoords=texcoords_ramp_0,             gfx_mat="CockPitCubeMapTestRamp",       })
	RegisterShipPartType("shippart/ramp1",      "shippart", {gfx_geom=kGeomRamp,         texcoords=texcoords_ramp_0,             gfx_mat="CockPitCubeMapTestRamp",       })
end






function GetShipPartPlainTextureMat (texname)
	if (gUseCellShading) then 
		return GetTexturedMat("CelShadingBase",texname) 
		-- return "CelShadingBase"
	end
	return GetPlainTextureMat(texname)
end

-- t = typeobject
function ShipPartTypeDefaultPreLoader (t)
	print("ShipPartTypeDefaultPreLoader",t.type_name)
	
	-- custom material
	local matname =  (t.gfx_tex and GetShipPartPlainTextureMat(t.gfx_tex)) or t.gfx_mat -- use t.gfx_mat or t.gfx_tex
	if (not matname) then matname = GetPlainColourMat(1,1,1) end -- fallback to plain white
	if (t.hue) then matname = GetHuedMat(matname,unpack(t.hue)) end -- hue
	t.gfx_mat = matname
	--matname = GetPlainColourMat(1,1,1,0.5)
	
	if (gCommandLineSwitches["-se"]) then
		SetSoftwareCulling(matname,0,0,0)
		SetHardwareCulling(matname,0,0,0)
	end
	
	-- base geometry
	if (t.gfx_geom) then
		local gfx = CreateRootGfx3D()
		GfxSetGeom(gfx,t.gfx_geom,t.texcoords,t.cx,t.cy,t.cz) 
		gfx:SetMaterial(matname)
		t.gfx_mesh = gfx:RenderableConvertToMesh()
		gfx:Destroy()
	end
	
	-- calculate some mesh properties, such as bounds, boundsmid, and centroid
	if (t.gfx_mesh) then
		local gfx = CreateRootGfx3D()
		gfx:SetMesh(t.gfx_mesh)
		--t.voxelgrid = CalcVoxelGrid(gfx,t.cx,t.cy,t.cz,1/4)
		gfx:Destroy()
	end
end


-- t = typeobject
function ShipPartTypeDefaultConstructor (t,parentgfx)
	local gfx = parentgfx and parentgfx:CreateChild() or CreateRootGfx3D()
	if (t.gfx_mesh) then 
		gfx:SetMesh(t.gfx_mesh) 
		--DrawVoxelGrid(t.voxelgrid,gfx,1/4)
	end
	return gfx
end

RegisterListener("Hook_PreLoad",function () LoadCommonShipPartTypes() end)
