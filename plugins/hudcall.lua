-- callmarker

gHudCallMarker = nil
gHudCallMarkerObj = nil
gHudCallMarkerLine = nil

RegisterListener("Hook_Client_SetCall",function (obj) 
	DestroyHudCallMarker()
	if (obj) then 
		print("hudline : Hook_Client_SetCall")
		gHudCallMarker = MakeObjectHudCallMarker(obj.gfx) gHudCallMarkerObj = obj
		gHudCallMarkerLineDialog = guimaker.MyCreateDialog()
		gHudCallMarkerLine = guimaker.MakeRROC(gHudCallMarkerLineDialog,"objmark_main")
	end
end) -- client selects new call

function DestroyHudCallMarker ()
	if (gHudCallMarker) then
		gHudCallMarker:Destroy() 
		gHudCallMarker = nil
		gHudCallMarkerObj = nil
		gHudCallMarkerLineDialog:Destroy()
		gHudCallMarkerLineDialog = nil
	end
end

RegisterListener("Hook_Object_Destroy",function (obj) 
	if (obj == gHudCallMarkerObj) then 
		print("hudline : Hook_Object_Destroy")
		DestroyHudCallMarker() 
	end
end) -- call destroyed

RegisterListener("Hook_HUDStep",function () 
	if ( gHudCallMarkerLineDialog and gHudCallMarkerObj and gHudCallMarkerObj:IsAlive()) then
		local vw,vh = GetViewportSize()
		local maxz = GetMaxZ()
		
		
		-- coordinates in pixel
		local sx3,sy3 = gHudCallMarkerObj:GetProjectedSizeAndPosInPixels()
		local sx1,sy1 = 0.5*(0.5*vw + sx3),0.5*(0.5*vh + sy3)
		local sx2,sy2 = (sx1+sx3)/2,(sy1+sy3)/2
		
		-- local sx2,sy2 = sx3,sy1

		-- transfer pixel coordinates into ogre
		local x1,y1 = -1 + (sx1+0)*2/vw, 1 - (sy1+0)*2/vh
		local x2,y2 = -1 + (sx2+0)*2/vw, 1 - (sy2+0)*2/vh
		local x3,y3 = -1 + (sx3+0)*2/vw, 1 - (sy3+0)*2/vh
		
		local r,g,b = 1,0,0
		
		-- draw very very helpful lines
		local gfx = gHudCallMarkerLine.gfx
		gfx:RenderableBegin(3,0,true,false,OT_LINE_STRIP)
		gfx:RenderableVertex(x1,y1,maxz, r,g,b,1)
		gfx:RenderableVertex(x2,y2,maxz, r,g,b,1)
		gfx:RenderableVertex(x3,y3,maxz, r,g,b,1)
		gfx:RenderableEnd()
	end
end)

RegisterListener("Hook_PreLoad",function () 
	local col_back = {0,0,0,0}
	gHudCallMarkerDialogLayer = guimaker.MyCreateDialog()
	gHudCallMarkerDialogLayer.panel = guimaker.MakeBorderPanel(gHudCallMarkerDialogLayer,0,0,0,0,col_back)
	gHudCallMarkerDialogLayer:SendToBack()
end)
	
function MakeObjectHudCallMarker (trackgfx,r,g,b,a)
	r = r or 1
	g = g or 0
	b = b or 0
	a = a or 0.2

	local mat = GetPlainTextureMat("mark.png")
	mat = GetHuedMat(mat,1,1,1, 1,1,1,0.2)
	local e = 64
	local hudmarker = guimaker.MakePlane(gHudMarkerDialogLayer.panel,mat,-0.5*e,-0.5*e,e,e)
	hudmarker.gfx:SetTrackOffset(-0.5*e,-0.5*e)
	hudmarker.gfx:SetTrackPosSceneNode(trackgfx)
	hudmarker.gfx.mbTrackHideIfClamped = false
	hudmarker.gfx.mbTrackHideIfBehindCam = false
	local vw,vh = GetViewportSize()
	hudmarker.gfx.mbTrackClamp = true
	hudmarker.gfx.mvTrackClampMin = {0,0}
	hudmarker.gfx.mvTrackClampMax = {vw,vh}
	hudmarker.gfx:SetColour(r,g,b,a)
	return hudmarker
end
