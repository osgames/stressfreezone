-- a library of effect types
-- see also lib.effect.lua
-- see also lib.plugin.lua
-- RegisterEffectType(name,parentname,arr)
-- todo : also sounds ?

function LoadCommonEffectTypes ()
	RegisterEffectType("effect/beam",		false,			{sound=kSoundDir.."beam4.ogg",mat="beam",dur=200,w=0.5,dz=32000,col={1,1,1,1},constructor=CreateBeamEffect})
	RegisterEffectType("effect/laser",		"effect/beam",	{})
	RegisterEffectType("effect/laser/red",		"effect/laser",	{col={1,0,0,1}})
	RegisterEffectType("effect/laser/green",	"effect/laser",	{col={0,1,0,1}})
	RegisterEffectType("effect/laser/blue",		"effect/laser",	{col={0,0,1,1}})
	RegisterEffectType("effect/lightning",	false,			{name="Lightning",spreading=1,complexity=15,col={1,1,1,0.5},dur=2000,constructor=CreateLightningEffect})
	RegisterEffectType("effect/particle",	false,			{name="Explosion",dur=10000,constructor=CreateParticleEffect})
	RegisterEffectType("effect/hit",		false,			{name="ExplosionHit",dur=1000,constructor=CreateParticleEffect})
	RegisterEffectType("effect/die",		false,			{sound=kSoundDir.."bumm3.ogg",name="ExplosionDie",dur=1500,constructor=CreateParticleEffect})
	RegisterEffectType("effect/travel",		false,			{name="TravelEllipseParticles",dur=20000,constructor=CreateParticleEffect})
end
-- sound=kSoundDir.."hit.ogg",
-- sound=kSoundDir.."boom.ogg",



function CreateParticleEffect (effecttype,parentgfx,effect)
	-- print("CreateParticleEffect",effecttype,parentgfx,effect)
	local gfx = parentgfx and parentgfx:CreateChild() or CreateRootGfx3D()
	--local gfx = CreateRootGfx3D()
	gfx:SetParticleSystem(effecttype.name)
	gfx.iBirthTime = gMyTicks
	gfx.iDur = effecttype.dur
	RegisterStepper(DurationEffectFader,effect)
	return gfx
end

function CreateBeamEffect (effecttype,parentgfx,effect,beamlen)
	--print("CreateBeamEffect",effecttype,parentgfx,effect)
	local gfx = parentgfx and parentgfx:CreateChild() or CreateRootGfx3D()
	effect.beam_gfx = gfx
	effect.beamlen = beamlen
	gfx:SetBeam(true)
	gfx:SetMaterial(effecttype.mat)
	gfx.iBirthTime = gMyTicks
	gfx.iDur = effecttype.dur
	RegisterStepper(DurationEffectFader,effect) 
	UpdateBeamEffect(effect,1)
	return gfx
end

function CreateLightningEffect (effecttype,parentgfx,effect,lightninglen)
	print("CreateLightningEffect",effecttype,parentgfx)
	local gfx = parentgfx and parentgfx:CreateChild() or CreateRootGfx3D()
	effect.lightning_gfx = gfx
	effect.lightninglen = lightninglen

	local spreading = effecttype.spreading or 2
	local parts = effecttype.complexity or 10
	local steps = effecttype.complexity or 10

	local depth = lightninglen / steps

	local d = effecttype.d or {spreading,spreading,-depth}
	local rd = effecttype.rd or {-spreading*2,-spreading*2,spreading*2}

	-- TODO keep in sync on all client
	local seed = math.random(0,10000)

	NewPlant(seed,gfx,effecttype.col,parts,steps,d,rd)
	
	gfx.iBirthTime = gMyTicks
	gfx.iDur = effecttype.dur
	RegisterStepper(DurationEffectFader,effect) 
	return gfx
end

function UpdateBeamEffect (effect,colmult)
	local gfx = effect.beam_gfx
	local effecttype = effect.effecttype
	--print(effect,effecttype,effect.beamlen,effecttype.dz,alphamult)
	local x,y,z = 0,0,-(effect.beamlen or effecttype.dz or 1)
	local w = effecttype.w 
	local u1,u2 = 0.5,0.5
	local r,g,b,a = unpack(effecttype.col)
	r = r * colmult
	g = g * colmult
	b = b * colmult
	gfx:BeamClearLines()
	BeamSingleLine(gfx,0,0,0,x,y,z,w,-w,w,-w,u1,u1,u2,u2,0,1,0,1,r,g,b,a,r,g,b,a)
	gfx:BeamUpdateBounds()
end

function DurationEffectFader (effect)
	if ((not effect.gfx) or (not effect.gfx:IsAlive())) then return true end -- object was destroyed before effect ended
	local t = (gMyTicks - effect.gfx.iBirthTime) / effect.gfx.iDur
	if (t > 1) then
		effect:Destroy()
		return true
	end
	if (effect.beam_gfx) then UpdateBeamEffect(effect,1.0 - t) end
end

RegisterListener("Hook_PreLoad",function () LoadCommonEffectTypes() end)
