-- handles the plain text like mission dialog

gOwnMissionDialog = nil
gOwnMissionDialogX, gOwnMissionDialogY = 50,50
gOwnMissions = {}

function CloseMissionDialog ()
	if (gOwnMissionDialog) then gOwnMissionDialog:Destroy() gOwnMissionDialog = nil end
end

function MissionStateToText (state)
	if state == kTaskState_Running then return ""
	elseif state == kTaskState_Finished then return "OK"
	elseif state == kTaskState_Failed then return "ERR"
	else return "?" end
end

function OpenMissionDialog (x,y)
	CloseMissionDialog()

	local rows = { 
		{ { "Missions" } },
	}
	
	for k,v in pairs(gOwnMissions) do
		local button = nil
		if v.wheretogo then
			local where = v.wheretogo
			button = { "show", function(widget) NotifyListener("Hook_Client_HUDShowWaypoint",where) end }
		end
		table.insert(rows, { { MissionStateToText(v.state) }, { "" .. v.id .. ": " .. v.name }, button })
		
		table.insert(rows, { { "---" },{ "---------------------" }, })
		
		for kk,vv in pairs(v.tasks) do
			local button = nil
			if vv.wheretogo then
				local where = vv.wheretogo
				button = { "show", function(widget) NotifyListener("Hook_Client_HUDShowWaypoint",where) end }
			end
			table.insert(rows, { { MissionStateToText(vv.state) }, { "" .. vv.id .. ": " .. vv.description }, button })
		end

		table.insert(rows, { { "" } })
	end
	
	gOwnMissionDialog = guimaker.MakeTableDlg(rows,x,y,true,false,gGuiDefaultStyleSet,"window")
	gOwnMissionDialog.rootwidget.onDestroy = function()
		gOwnMissionDialogX, gOwnMissionDialogY = gOwnMissionDialog.rootwidget.gfx:GetPos()
	end

end

RegisterListener("Hook_Client_OpenOwnMissionDialog",function () 
	if (gOwnMissionDialog) then 
		CloseMissionDialog()
	else 
		OpenMissionDialog(gOwnMissionDialogX, gOwnMissionDialogY)
	end
end)

RegisterListener("Hook_MissionUpdate",function (mission) 
	-- is this a new mission?
	if not gOwnMissions[mission.id] and mission.state == kTaskState_Running then
		-- so tell the player
		MissionNotifyNew(mission)
	end

	gOwnMissions[mission.id] = mission
	
	if (not gOwnMissionDialog) then return end
	-- re-open dialog on the same position
	local x = gOwnMissionDialog.rootwidget.gfx:GetDerivedLeft()
	local y = gOwnMissionDialog.rootwidget.gfx:GetDerivedTop()
	OpenMissionDialog(x,y)
end)


gMissionDialog_RemoveOldTimeout = 5 * 1000

-- show the player a new mission
function MissionNotifyNew(mission)
	SoundPlayOmniEffect(kSoundDir.."summ1.ogg")
	local info = mission.description or ""
	AddFadeLines("new mission: " .. info)
end

-- show the player a mission success
function MissionNotifySuccess(mission)
	SoundPlayOmniEffect(kSoundDir.."summ2.ogg")
	local info = mission.description or ""
	AddFadeLines("mission finished: " .. info)
end

-- show the player a mission failure
function MissionNotifyFailure(mission)
	SoundPlayOmniEffect(kSoundDir.."summ2.ogg")
	local info = mission.description or ""
	AddFadeLines("mission failed: " .. info)
end

RegisterListener("Hook_MissionStateUpdate",function (mission_id, state) 
	if state == kTaskState_Finished or kTaskState_Failed then
		local mission = gOwnMissions[mission_id]
		
		if mission then		
			-- notify succeed or failure
			if state == kTaskState_Finished then MissionNotifySuccess(mission) end
			if state == kTaskState_Failed then MissionNotifyFailure(mission) end
			
			-- remove this entry after timeout
			InvokeLater(gMissionDialog_RemoveOldTimeout,function()
			
				gOwnMissions[mission_id] = nil
		
				if (not gOwnMissionDialog) then return end
				-- re-open dialog on the same position
				local x = gOwnMissionDialog.rootwidget.gfx:GetDerivedLeft()
				local y = gOwnMissionDialog.rootwidget.gfx:GetDerivedTop()
				OpenMissionDialog(x,y)

			end)
		end
	end
end)
