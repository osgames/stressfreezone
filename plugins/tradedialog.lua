-- handles the plain text like trade dialog

gTradeDialog = nil

function CloseTradeDialog ()
	if (gTradeDialog) then gTradeDialog:Destroy() gTradeDialog = nil end
end

function MyOpenTradeDialog (x,y,obj_trader)
	CloseTradeDialog()
	local o = gClientBody
	print("MyOpenTradeDialog",obj_trader)
	local tradelist = obj_trader.objtype.tradelist
	
	local cargoinfo = sprintf("Cargo: %d/%d spaceleft:%d",	o and o:GetCurCargo() or 0,	o and o:GetMaxCargo() or 0,o and o:GetCargoSpaceLeft() or 0)
	local rows = { { 
			{ type="Label",	text="trade"},
			{ type="Label",	text=""},
			{ type="Label",	text=""},
			{ type="Label",	text=""},
			{ type="Button",text="close",on_button_click=CloseTradeDialog },
		},{ 
			{ type="Label",	text="offer"},
			{ type="Label",	text="cost"},
			{ type="Label",	text=""},
			{ type="Label",	text=""},
			{ type="Label",	text=""},
		},

		}
		
	
	for offerid,offerarr in pairs(tradelist) do 
		print("DEBUG",offerid,offerarr)
		local offer_txt = GetObjTypeListText(offerarr.offer)
		local cost_txt = GetObjTypeListText(offerarr.cost)
	
		table.insert(rows,{ 
			{ type="Label",	text=offer_txt},
			{ type="Label",	text=" : "..cost_txt},
			
			{ type="Button",text="buy", offerid=offerid, 
				on_button_click=function (self) ClientRequestTrade(obj_trader,self.offerid, 1) end },
				
			{ type="Button",text="sell", offerid=offerid, 
				on_button_click=function (self) ClientRequestTrade(obj_trader,self.offerid,-1) end },
		})
	end
	
	gTradeDialog = guimaker.MakeTableDlg(rows,x,y,true,false,gGuiDefaultStyleSet,"window")
end

RegisterListener("Hook_Client_OpenTradeDialog",function (obj_trader) 
	print("Hook_Client_OpenTradeDialog",obj_trader)
	if (gTradeDialog) then return end -- already open
	MyOpenTradeDialog(50,50,obj_trader)
end)


