-- Waypointmarker

gHudWaypointMarker = nil
gHudWaypointMarkerObj = nil

RegisterListener("Hook_Client_HUDShowWaypoint",function (obj) 
	DestroyHudWaypointMarker()
	if (obj) then 
		gHudWaypointMarker = MakeObjectHudWaypointMarker(obj.gfx) gHudWaypointMarkerObj = obj
	end
end) -- client selects new Waypoint

function DestroyHudWaypointMarker ()
	if (gHudWaypointMarker) then
		gHudWaypointMarker:Destroy() 
		gHudWaypointMarker = nil
		gHudWaypointMarkerObj = nil
	end
end

RegisterListener("Hook_Object_Destroy",function (obj) 
	if (obj == gHudWaypointMarkerObj) then 
		DestroyHudWaypointMarker() 
	end
end) -- Waypoint destroyed

RegisterListener("Hook_HUDStep",function () 
	if ( gHudWaypointMarkerObj and gHudWaypointMarkerObj:IsAlive()) then
		local vw,vh = GetViewportSize()
		local maxz = GetMaxZ()
	end
end)

RegisterListener("Hook_PreLoad",function () 
	local col_back = {0,0,0,0}
	gHudWaypointMarkerDialogLayer = guimaker.MyCreateDialog()
	gHudWaypointMarkerDialogLayer.panel = guimaker.MakeBorderPanel(gHudWaypointMarkerDialogLayer,0,0,0,0,col_back)
	gHudWaypointMarkerDialogLayer:SendToBack()
end)
	
function MakeObjectHudWaypointMarker (trackgfx,r,g,b,a)
	r = r or 1
	g = g or 1
	b = b or 1
	a = a or 0.5

	local mat = GetPlainTextureMat("waypoint.png")
	mat = GetHuedMat(mat,1,1,1, 1,1,1,0.2)
	local e = 32
	local hudmarker = guimaker.MakePlane(gHudMarkerDialogLayer.panel,mat,-0.5*e,-0.5*e,e,e)
	hudmarker.gfx:SetTrackOffset(-0.5*e,-0.5*e)
	hudmarker.gfx:SetTrackPosSceneNode(trackgfx)
	hudmarker.gfx.mbTrackHideIfClamped = false
	hudmarker.gfx.mbTrackHideIfBehindCam = false
	local vw,vh = GetViewportSize()
	hudmarker.gfx.mbTrackClamp = true
	hudmarker.gfx.mvTrackClampMin = {0,0}
	hudmarker.gfx.mvTrackClampMax = {vw,vh}
	hudmarker.gfx:SetColour(r,g,b,a)
	return hudmarker
end
