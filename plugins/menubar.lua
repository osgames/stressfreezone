
function SelectByNameDialog ()
	-- only allow one dialog
	if gMenuBarSelectByNameDialog then 
		return
	end
	
	local MySelect = function (widget) 
		local patterninput = widget.dialog.controls["patterninput"].plaintext
		if patterninput then
			ClientChooseTarget_ByName(patterninput)
		end
		widget.dialog:Destroy()
		gMenuBarSelectByNameDialog = nil
	end
	
	local rows = {
		{	{type="Label",		text="Select by name"} },
		
		{	{type="Label",		text="Name:"},
			{type="EditText",	w=300,h=16,text="",controlname="patterninput"}  },
			
		{	{type="Label",		text="Tipp:"},
			{type="Label",		text=	"selects the nearest object containing the pattern\n"..
										"in the objectname."}, },
			
		{	{type="Button",on_mouse_left_click=MySelect,text="Select"}, },
		
	}
	
	gMenuBarSelectByNameDialog = guimaker.MakeTableDlg(rows,10,10,true,false,gGuiDefaultStyleSet,"window")
end


function CreateMenuBar	()
	
	local submenu_goto = {}
	table.insert(submenu_goto,{ "selected" , function () if (gClientTarget) then ClientAutoPilot_Approach(gClientTarget) end end})
	table.insert(submenu_goto,{ "nearest base",		function () 
			local o = ClientGetNearestObject(ObjFilter_IsBase)
			ClientSetTarget(o)
			ClientAutoPilot_Approach(o) 
		end})
	table.insert(submenu_goto,{ "nearest asteroid",	function () 
			local o = ClientGetNearestObject(CreateObjFilter_Type("asteroid"))
			ClientSetTarget(o)
			ClientAutoPilot_Approach(o) 
		end})
	table.insert(submenu_goto,{ "nearest hostile",	function () 
			ClientChooseTarget_NearestHostile()
			if (gClientTarget) then ClientAutoPilot_Approach(gClientTarget) end
		end})

	local submenu_select = {}
	table.insert(submenu_select,{ "deselect target",function () 
			if (gClientTarget) then ClientChooseTarget_NoTarget() end 
		end})
	table.insert(submenu_select,{ "nearest base",		function () 
			local o = ClientGetNearestObject(ObjFilter_IsBase)
			ClientSetTarget(o)
		end})
	table.insert(submenu_select,{ "nearest asteroid",	function () 
			local o = ClientGetNearestObject(CreateObjFilter_Type("asteroid"))
			ClientSetTarget(o)
		end})
	table.insert(submenu_select,{ "nearest hostile",	function () 
			ClientChooseTarget_NearestHostile()
		end})
	table.insert(submenu_select,{ "by name",	function () 
			SelectByNameDialog()
		end})
	table.insert(submenu_select,{ "open base list...",	function () 
			NotifyListener("Hook_Client_OpenBaseListDialog")
		end})


	local menubardata = {
		{ "goto", submenu_goto },
		{ "select", submenu_select },
		{ "spaceports", function() 
			local lm = {}
			local lp = ClientGetCustomTargetList(function(obj) return obj.objtype.bHasSpacePort end)
			-- add a select planet button for each planet
			for k,o in pairs(lp) do
				local oo = o
				table.insert(lm,{o:GetName() or "unknown",function()
					ClientSetTarget(oo)
				end})	
			end
			-- sort spaceports alphabetically
			table.sort(lm,function(a,b) 
				-- a > b -> true
				return a[1] < b[1]
			end)	
			return lm
		end },
		{ "info", { 
			{ "cargo", function() OpenOwnCargoDialog() end },
			{ "equipment", function() OpenOwnEquipmentDialog() end },
		}},
		{ "game", {
			{ "load" },
			{ "save" },
			{ "options" },
			{ "exit", function() Exit() end }
		}},
	}
	
	-- create menubar
	local menubardialog = ShowMenuBar(menubardata,0,0,gGuiDefaultStyleSet)
	
	-- center menubar
	local vw,vh = GetViewportSize()
	local w = menubardialog.rootwidget.gfx:GetWidth()
	local x,y = menubardialog.rootwidget.gfx:GetPos()
	menubardialog.rootwidget.gfx:SetPos(vw / 2 - w / 2,y)
	guimaker.LayoutTableDlg(menubardialog,menubardialog.rootwidget) 
end

RegisterListener("Hook_Client_Start",function ()
	CreateMenuBar()
end ) 
