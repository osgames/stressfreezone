-- plugin for interactive equipment-icons in the hud (rightclick, click...)
-- see also lib.plugin.lua
-- see also Hook_FireWeapon,Hook_Client_SetBody,Hook_Object_SetContainerContent container:GetObject() container:GetType().bIsSlot 

gEquipmentIcons = {}

gHudEquipmentIconsDialog = nil
kEquipIconStartX = 0
kEquipIconStartY = 64
kEquipIconColor_Cooldown	= {0.9,0.9,0.2, 1.0}
kEquipIconColor_Back		= {0.2,0.2,0.2, 1.0}
kEquipIconBarH = 4
kEquipIconBarW = kEquipIconCX
kEquipIconBarUMax = kEquipIconBarW/kEquipIconBarH

kEquipIconDistance = kEquipIconCX + kEquipIconBarH + 4

-- weapon_container.client_last_fired = gMyTicks

RegisterListener("Hook_Client_Start",function () 
	if (gHudEquipmentIconsDialog) then return end
	local col_back = {0,0,0,0}
	gHudEquipmentIconsDialog = guimaker.MyCreateDialog()
	gHudEquipmentIconsDialog.panel = guimaker.MakeBorderPanel(gHudEquipmentIconsDialog,0,0,0,0,col_back)
	gHudEquipmentIconsDialog.panel.mbClipChildsHitTest = false
	gHudEquipmentIconsDialog:BringToFront()
end)

function EquipmentIconContextMenu (container)
	local menudata = {}
	local objecttype = container:GetFirstContentType()
	if (objecttype) then
		table.insert(menudata,{ objecttype.name })
		table.insert(menudata,{ "unequip", function () ClientRequestUnequip(container.id,objecttype.type_id) end })
	else
		-- empty slot
		table.insert(menudata,{ "empty" })
	end
	ShowContextMenu(menudata,nil,nil,gGuiDefaultStyleSet)
end

function EquipmentIconClick (container)
	local ship = container:GetObject() -- container:GetType().type_name
	local target = GetPlayerShipTarget(ship)
	local slotnumber = container:GetWeaponSlotNumber()
	FireWeaponSlot(ship,target,slotnumber)
end



function MakeEquipmentIcon (matname,mainSlotIndex)
	local vw,vh = GetViewportSize()
	local iconsPerLine = math.floor(vw / kEquipIconDistance)
	local imod = kEquipIconDistance * math.mod(mainSlotIndex,iconsPerLine)
	local idiv = kEquipIconDistance * math.floor(mainSlotIndex/iconsPerLine)
	local x = idiv
	local y = kEquipIconStartY + imod
	local icon = {}
	local parent = gHudEquipmentIconsDialog

	-- main image
	local widget = guimaker.MakePlane(parent,matname,x,y,kEquipIconCX,kEquipIconCY)
	widget.mbIgnoreMouseOver = false
	widget.on_simple_tooltip		= function (widget) return widget.icon.mytooltip end
	widget.on_mouse_left_down		= function (widget) EquipmentIconClick(widget.icon.container) end
	widget.on_mouse_right_down		= function (widget) EquipmentIconContextMenu(widget.icon.container) end
	widget.icon = icon
	icon.image = widget
	
	-- blocked layer
	local widget = guimaker.MakePlane(parent,GetPlainTextureMat("sd_pd_blocked.png",true),x,y,kEquipIconCX,kEquipIconCY)
	widget.mbIgnoreMouseOver = true
	icon.blocked = widget
	icon.blocked.gfx:SetVisible(false)
	
	-- cooldown bar
	local matname = GetPlainTextureMat("bar07.png",true)
	local widget = guimaker.MakeRROC(parent,matname)
	widget.mbIgnoreMouseOver = false
	widget.on_simple_tooltip = function (widget) return "cooldown" end
	widget.w = kEquipIconBarW
	widget.h = kEquipIconBarH
	widget.x = x
	widget.y = y + kEquipIconCY
	icon.bar_cooldown = widget
	
	return icon
end

function DestroyEquipmentIcon (icon)
	icon.image:Destroy()
	icon.bar_cooldown:Destroy()
end

function StepEquipmentIconWidget (objecttype,container,mainSlotIndex)
	-- calc matname
	local matname = GetPlainTextureMat(objecttype.icon or kDefaultEquipmentIcon,true)
	
	-- create widgets if needed
	local icon = gEquipmentIcons[mainSlotIndex]
	if (not icon) then
		icon = MakeEquipmentIcon(matname,mainSlotIndex)
		gEquipmentIcons[mainSlotIndex] = icon
	end
	
	-- update icon parts
	icon.mytooltip = objecttype.name
	icon.container = container
	icon.image.gfx:SetMaterial(matname)
	
	-- cooldown bar
	local fCoolDown = container.client_last_fired and
		(gMyTicks - container.client_last_fired) / objecttype.iFireInterval or 1.0
	local bar = icon.bar_cooldown
	--function Gfx2DSetBar (gfx,x,y,w,h,fraction,col_fore,col_back,umin,umax,bIsVertical)
	Gfx2DSetBar(bar.gfx,bar.x,bar.y,bar.w,bar.h,fCoolDown,kEquipIconColor_Cooldown,kEquipIconColor_Back,0,kEquipIconBarUMax)
	
	-- update blocked border
	if objecttype.fire_possible_fun and icon.blocked then
		local ship = gClientBody
		local target = gClientTarget
		if ship and target then
			local possible = objecttype.fire_possible_fun(container,ship,target)
			icon.blocked.gfx:SetVisible(not possible)
		else
			icon.blocked.gfx:SetVisible(true)
		end
	end
end


--- called every frame
function StepEquipmentIcons ()
	local nextSlotIndex = 0
	for containertype,containerlist in pairs(gClientBody and gClientBody:GetContainerListByType() or {}) do
		if (containertype.bIsSlot) then
			for k,container in pairs(containerlist) do 
				local objecttype = container:GetFirstContentType() 
				if (objecttype and objecttype.bIsActive) then
					StepEquipmentIconWidget(objecttype,container,nextSlotIndex)
					nextSlotIndex = nextSlotIndex + 1
				end
			end
		end
	end
	
	for k,icon in pairs(gEquipmentIcons) do 
		if (k >= nextSlotIndex) then DestroyEquipmentIcon(icon) gEquipmentIcons[k] = nil end
	end
end

RegisterListener("Hook_HUDStep",function () StepEquipmentIcons() end)
