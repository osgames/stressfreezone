-- plugin for adding pirate ships attacking the player
-- see also lib.plugin.lua

local pluginenabled = false -- set this to true to enable the plugin

if pluginenabled then

	gPirateNum = 4
	kPirateSpawnRad = 2000

	wespawned = false

	function grouptest ()
		if (not gbServerRunning) then return end

		if( wespawned == false ) then
			them = SpawnShipGroup ( {["shiptype/pirate"]=4}, 1000, 1000, 0, kPirateSpawnRad, "Pirate")
			them.onDestroy = function (self) wespawned = false end
			
			wespawned = true
		end
	

	end

	RegisterListener("Hook_Client_Start", grouptest)

end
