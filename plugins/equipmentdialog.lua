-- handles the plain text like equipment dialog

gOwnEquipmentDialog = nil
gOwnEquipmentDialogX, gOwnEquipmentDialogY = 50,50

function CloseEquipmentDialog ()
	if (gOwnEquipmentDialog) then gOwnEquipmentDialog:Destroy() gOwnEquipmentDialog = nil end
end

function OpenEquipmentDialog (x,y)
	if not gClientBody then return end

	CloseEquipmentDialog()
	local o = gClientBody
	local rows = { { 
		{ "Equipment"},
		{ ""},
		{ "Cargo",OpenOwnCargoDialog },
		{ "close",CloseEquipmentDialog },
		} }
	
	function GetEquipmentDialogClickMultiplier ()
		return gKeyPressed[key_lshift] and 10 or 1
	end
	
	for containertype,containerlist in pairs(gClientBody:GetContainerListByType() or {}) do
		if (containertype.bIsSlot) then
			for k,container in pairs(containerlist) do 
				for objecttype,amount in pairs(container:GetContentByType() or {}) do 
					if (amount > 0) then
						local iContainerID = container.id
						local iObjTypeID = objecttype.type_id
						table.insert(rows,{	{ containertype.name.."["..k.."] : "..objecttype.name },
											{ sprintf("  %d  ",amount) },
											{ "unequip", function () ClientRequestUnequip(iContainerID,iObjTypeID) end },
									})
					end
				end
			end
		end
	end
	
	gOwnEquipmentDialog = guimaker.MakeTableDlg(rows,x,y,true,false,gGuiDefaultStyleSet,"window")
	gOwnEquipmentDialog.rootwidget.onDestroy = function()
		gOwnEquipmentDialogX, gOwnEquipmentDialogY = gOwnEquipmentDialog.rootwidget.gfx:GetPos()
	end
end

RegisterListener("Hook_Client_OpenOwnEquipmentDialog",function () 
	if (gOwnEquipmentDialog) then 
		CloseEquipmentDialog()
	else 
		OpenEquipmentDialog(gOwnEquipmentDialogX, gOwnEquipmentDialogY)
	end
end)

RegisterListener("Hook_Object_SetContainerContent",function (container,objecttype,fCurAmount) 
	local obj = container:GetObject()
	if (obj ~= gClientBody) then return end
	if (not gOwnEquipmentDialog) then return end
	-- re-open dialog on the same position
	local x = gOwnEquipmentDialog.rootwidget.gfx:GetDerivedLeft()
	local y = gOwnEquipmentDialog.rootwidget.gfx:GetDerivedTop()
	OpenEquipmentDialog(x,y)
end)

