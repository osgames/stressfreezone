-- just a test so far, start with -ge to try

-- todo : texcoords (depend on skin, texture-atlas)
-- todo : skinning info should remain as good as possible after geometry change 
--   (search equal, and overlapping, and most used for new faces)
-- todo : minor fix : not all in one submesh : generate depending on skin info (+scaling?)

gShipPartSkins = CreateTypeList()
function RegisterShipPartSkin	(...) return gShipPartSkins:RegisterType(unpack(arg)) end
function GetShipPartSkin		(shippartskin_or_name_or_id) return gShipPartSkins:Get(shippartskin_or_name_or_id) end

kPartGeomEdit_EdgeGizmoRad = 0.1
kPartGeomEdit_EdgeGizmoRad_Pick = kPartGeomEdit_EdgeGizmoRad*1.1
kInitialShipPartGeomEditorThirdPersonDist = 10
gShipPartGeomEditorTableCamSpeedFactor = 0.5 * gfDeg2Rad
gShipPartGeomEditor_Mat_On	= nil
gShipPartGeomEditor_Mat_Off	= nil
gShipPartGeomEditor_Gizmo = nil
gShipPartGeomEditor_EdgeGizmoMeshName = nil

gGeomEditEdgeGizmoPrototype = {}
gGeomEditGizmoPrototype = {}


-- ##### ##### ##### ##### ##### geometry utils

assert(1-1 == 0 and 0+1 == 1 and -1+1 == 0) -- make sure that there are no funny rounding errors
gUnitCubeEdgePos = {{0,0,0}, {1,0,0},{0,1,0},{0,0,1},
					{1,1,1}, {1,1,0},{0,1,1},{1,0,1}, }  -- unit-cube edges

function EdgePosDelta	(i,j)	-- returns {dx,dy,dz}
	local xa,ya,za = unpack(gUnitCubeEdgePos[i])
	local xb,yb,zb = unpack(gUnitCubeEdgePos[j])
	return {Vector.sub(xa,ya,za, xb,yb,zb)}
end
function IsDirOrtho		(va,vb)	-- takes two {dx,dy,dz} , and returns true if they are orthogonal (dot=0)
	local xa,ya,za = unpack(va)
	local xb,yb,zb = unpack(vb)
	return Vector.dot(xa,ya,za,xb,yb,zb) == 0
end
function IsDirEqual		(va,vb)	-- takes two {dx,dy,dz} , and returns true if they are equal
	return (va[1] == vb[1] and va[2] == vb[2] and va[3] == vb[3])
end

-- returns list of {a,b,c,d} with a<b<c<d
function GetEdgeDataQuadList (edgedata)
	local res = {}
	for a=   1,8 do if (edgedata[a]) then 
	for b= a+1,8 do if (edgedata[b] and b~=a) then 
	for c= a+1,8 do if (edgedata[c] and c~=a and c~=b and			IsDirOrtho(EdgePosDelta(a,b),EdgePosDelta(b,c))) then
	for d= a+1,8 do if (edgedata[d] and d~=a and d~=b and d~=a and	IsDirEqual(EdgePosDelta(a,b),EdgePosDelta(d,c)) and
																	IsDirEqual(EdgePosDelta(b,c),EdgePosDelta(a,d)) and
																	(not IsInnerTri(a,b,c,edgedata))) then
		local x1,y1,z1 = unpack(gUnitCubeEdgePos[a])
		local x2,y2,z2 = unpack(gUnitCubeEdgePos[b])
		local x3,y3,z3 = unpack(gUnitCubeEdgePos[c])
		local x4,y4,z4 = unpack(gUnitCubeEdgePos[d])
		local nx,ny,nz = Vector.normalise(Vector.cross(x2-x1,y2-y1,z2-z1,x3-x1,y3-y1,z3-z1))
		res[a..b..c..d] = {a,b,c,d, x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4, nx,ny,nz}
		
		
	end end
	end end
	end end
	end end
	return res
end

-- checks if a triangle is in the quadlist
function IsTriInQuadList	(a,b,c,quadlist)
	for k,quad in pairs(quadlist) do
		local qa,qb,qc,qd = unpack(quad)
		if (	(qa == a or qb == a or qc == a or qd == a)  and 					-- a in quad
				(qa == b or qb == b or qc == b or qd == b)  and 					-- b in quad
				(qa == c or qb == c or qc == c or qd == c)) then return true end	-- c in quad
	end
end

-- used to remove double-sided and inner triangles
-- uses the plane going through the triangle to split space into two halves
-- checks to see if there are active points on either half (ignoring all points on the plane)
-- here we check just the half in the direction where the surface normal points to
-- (depends on clockwise/anticlockwise vertex order)
-- so we can not only remove inner faces, but also avoid having double-sided faces,
-- so culling can work correctly to distinguish inside and outside
function IsInnerTri			(a,b,c,edgedata)
	local x1,y1,z1 = unpack(gUnitCubeEdgePos[a])
	local x2,y2,z2 = unpack(gUnitCubeEdgePos[b])
	local x3,y3,z3 = unpack(gUnitCubeEdgePos[c])
	local nx,ny,nz = Vector.normalise(Vector.cross(x2-x1,y2-y1,z2-z1,x3-x1,y3-y1,z3-z1))
	for i = 1,8 do if (edgedata[i] and i ~= a and i ~= b and i ~= c) then
		local x,y,z = unpack(gUnitCubeEdgePos[i])
		if (Vector.dot(x-x1,y-y1,z-z1, nx,ny,nz) > 0.1) then return true end
	end end
end

-- returns list of triangles that are not inner and not covered by quads
function GetEdgeDataTriList		(edgedata,quadlist)
	local res = {}
	for a=  1,8 do if (	edgedata[a]) then 
	for b=a+1,8 do if (	edgedata[b]) then 
	for c=a+1,8 do if (	edgedata[c] and c ~= b and 
						(not IsTriInQuadList(a,b,c,quadlist)) and 
						(not IsInnerTri(a,b,c,edgedata))		) then 
		local x1,y1,z1 = unpack(gUnitCubeEdgePos[a])
		local x2,y2,z2 = unpack(gUnitCubeEdgePos[b])
		local x3,y3,z3 = unpack(gUnitCubeEdgePos[c])
		local nx,ny,nz = Vector.normalise(Vector.cross(x2-x1,y2-y1,z2-z1,x3-x1,y3-y1,z3-z1))
		res[a..b..c] = {a,b,c, x1,y1,z1, x2,y2,z2, x3,y3,z3, nx,ny,nz}
	end end
	end end
	end end
	return res
end


-- returns mindist,foundsidename with mindist = nil if not hit
function EdgeListRayPick (rx,ry,rz,rvx,rvy,rvz,trilist,quadlist)
	local mindist,foundsidename
	local MyRegisterHit = function (dist,sidename) 
		if (dist and ((not mindist) or dist < mindist)) then mindist = dist foundsidename = sidename end
	end
	for sidename,arr in pairs(quadlist) do  
		local a,b,c,d, x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4, nx,ny,nz = unpack(arr)
		MyRegisterHit(TriangleRayPick(x1,y1,z1, x2,y2,z2, x3,y3,z3, rx,ry,rz, rvx,rvy,rvz),sidename)
		MyRegisterHit(TriangleRayPick(x1,y1,z1, x3,y3,z3, x4,y4,z4, rx,ry,rz, rvx,rvy,rvz),sidename)
	end
	for sidename,arr in pairs(trilist) do  
		local a,b,c, x1,y1,z1, x2,y2,z2, x3,y3,z3, nx,ny,nz = unpack(arr)
		MyRegisterHit(TriangleRayPick(x1,y1,z1, x2,y2,z2, x3,y3,z3, rx,ry,rz, rvx,rvy,rvz),sidename)
	end
	return mindist,foundsidename
end

-- ##### ##### ##### ##### ##### edit geom-edge gizmo

function MakeGeomEditEdgeGizmo (edgenum,maingizmo)
	local res = CreateRootGfx3D()
	res:SetMesh(gShipPartGeomEditor_EdgeGizmoMeshName)
	res.edgenum		= edgenum
	res.maingizmo	= maingizmo
	res.maingizmo.edgedata[res.edgenum] = true
	
	res:SetPosition(unpack(gUnitCubeEdgePos[edgenum]))
	ArrayOverwrite(res,gGeomEditEdgeGizmoPrototype)
	res:EdgeGizmo_Update()
	return res
end

function gGeomEditEdgeGizmoPrototype:SetState (state) self.maingizmo.edgedata[self.edgenum] = state end
function gGeomEditEdgeGizmoPrototype:GetState () return self.maingizmo.edgedata[self.edgenum] end
function gGeomEditEdgeGizmoPrototype:EdgeGizmo_Update ()
	self:SetMeshSubEntityMaterial(0,self:GetState() and gShipPartGeomEditor_Mat_On or gShipPartGeomEditor_Mat_Off) 
end
function gGeomEditEdgeGizmoPrototype:Toggle () 
	self:SetState(not self:GetState()) 
	self:EdgeGizmo_Update()
	self.maingizmo:UpdateGeom()
end

-- returns dist
function gGeomEditEdgeGizmoPrototype:RayPick (rx,ry,rz,rvx,rvy,rvz)
	local x,y,z = self:GetPosition()
	return SphereRayPick(x,y,z,kPartGeomEdit_EdgeGizmoRad_Pick,rx,ry,rz,rvx,rvy,rvz)
end


-- ##### ##### ##### ##### ##### edit geom gizmo

function gGeomEditGizmoPrototype:UpdateGeom	() 	
	self.quadlist	= GetEdgeDataQuadList(	self.edgedata)
	self.trilist	= GetEdgeDataTriList(	self.edgedata,self.quadlist)
	-- print("quads,tris",countarr(self.quadlist),countarr(self.trilist))
	
	for mat,gfx in pairs(self.gfx_bymat or {}) do gfx:Destroy() end 
	self.gfx_bymat = {}
	self.sideskin = self.sideskin or {}
	self.gfx = self.gfx or CreateRootGfx3D()
	local defaultskin = GetShipPartSkin("shippartskin/metal")
	
	-- prepare collecting geometry data grouped by material
	local bymat_vertices = {}
	local bymat_indices = {}
	local MyRegisterVertex	= function	(matname, ...) 
		local myarr = bymat_vertices[matname]
		if (not myarr) then myarr = {} bymat_vertices[matname] = myarr end
		table.insert(myarr,{unpack(arg)})
		return table.getn(myarr)-1
	end
	local MyRegisterIndices	= function	(matname,...) 
		local myarr = bymat_indices[matname]
		if (not myarr) then myarr = {} bymat_indices[matname] = myarr end
		for k,v in ipairs(arg) do table.insert(myarr,v) end 
	end
	
	-- collect geometry data
	for sidename,arr in pairs(self.quadlist) do  
		local skin = GetShipPartSkin(self.sideskin[sidename] or defaultskin)
		local t = skin.quad1
		local a,b,c,d, x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4, nx,ny,nz = unpack(arr)
		local a = MyRegisterVertex(skin.mat, x1,y1,z1, nx,ny,nz, t[1],t[2])
		local b = MyRegisterVertex(skin.mat, x2,y2,z2, nx,ny,nz, t[3],t[4])
		local c = MyRegisterVertex(skin.mat, x3,y3,z3, nx,ny,nz, t[5],t[6])
		local d = MyRegisterVertex(skin.mat, x4,y4,z4, nx,ny,nz, t[7],t[8])
		MyRegisterIndices(skin.mat,a,b,c,a,c,d)
	end
	for sidename,arr in pairs(self.trilist) do  
		local skin = GetShipPartSkin(self.sideskin[sidename] or defaultskin)
		local t = skin.tri1
		--print("tri",sidename,self.sideskin[sidename],unpack(t))
		local a,b,c, x1,y1,z1, x2,y2,z2, x3,y3,z3, nx,ny,nz = unpack(arr)
		local a = MyRegisterVertex(skin.mat, x1,y1,z1, nx,ny,nz, t[1],t[2])
		local b = MyRegisterVertex(skin.mat, x2,y2,z2, nx,ny,nz, t[3],t[4])
		local c = MyRegisterVertex(skin.mat, x3,y3,z3, nx,ny,nz, t[5],t[6])
		MyRegisterIndices(skin.mat,a,b,c)
	end
	
	-- create child gfx elements, one for each material
	for matname,vertexarr in pairs(bymat_vertices) do
		local indexarr = bymat_indices[matname]
		--print("bymat_vertexarr",matname,table.getn(vertexarr),table.getn(indexarr))
		local gfx = self.gfx:CreateChild()
		gfx:SetSimpleRenderable()
		gfx:RenderableBegin(table.getn(vertexarr),table.getn(indexarr),false,false,OT_TRIANGLE_LIST)
		for k,vertex in pairs(vertexarr) do gfx:RenderableVertex(unpack(vertex)) end
		for k,index in pairs(indexarr) do gfx:RenderableIndex(index) end
		gfx:RenderableEnd()
		gfx:SetMaterial(matname)
		self.gfx_bymat[matname] = gfx
	end
end

function gGeomEditGizmoPrototype:Destroy	() for i=1,8 do self[i]:Destroy() end end

-- returns mindist,foundedgenum,foundsidename  : either foundedgenum or foundsidename or both will be nil
function gGeomEditGizmoPrototype:RayPick	(rx,ry,rz,rvx,rvy,rvz) 
	local mindist,foundedgenum,foundsidename
	
	-- early out
	local x,y,z,r = 0.5,0.5,0.5, 1 -- radius of bounding sphere of unitcube is roughly 0.87 , so take r=1 to be safe
	if (not SphereRayPick(x,y,z,r,rx,ry,rz,rvx,rvy,rvz)) then return end -- early out
	
	-- raypick edges (spheres, bigger hit area than visible)
	for i=1,8 do 
		local dist = self[i]:RayPick(rx,ry,rz,rvx,rvy,rvz) 
		if (dist and ((not mindist) or dist < mindist)) then mindist = dist foundedgenum = i end
	end 
	
	-- raypick sides
	local dist,sidename = EdgeListRayPick(rx,ry,rz,rvx,rvy,rvz,self.trilist,self.quadlist)
	if (dist and ((not mindist) or dist < mindist)) then mindist = dist foundedgenum = nil foundsidename=sidename end
	
	return mindist,foundedgenum,foundsidename
end

function gGeomEditGizmoPrototype:ToggleEdge	(edgenum) self[edgenum]:Toggle() end

function MakeGeomEditGizmo ()
	local res = { edgedata={} }
	for i=1,8 do res[i] = MakeGeomEditEdgeGizmo(i,res) end
	ArrayOverwrite(res,gGeomEditGizmoPrototype)
	res:UpdateGeom()
	return res
end


-- ##### ##### ##### ##### ##### editor


function StartShipPartGeomEditor ()
	gCamThirdPersonDist = kInitialShipPartGeomEditorThirdPersonDist
	gShipPartGeomEditor_Mat_On	= GetPlainColourMat(0,1,0) -- green
	gShipPartGeomEditor_Mat_Off	= GetPlainColourMat(1,0,0) -- red
	
	gEditorLight = Client_AddPointLight(GetMainCam():GetPos())
	gCamThirdPersonX,gCamThirdPersonY,gCamThirdPersonZ = 0.5,0.5,0.5
	
	if (true) then
		local meshgengfx = CreateGfx3D()
		local r = kPartGeomEdit_EdgeGizmoRad
		GfxSetSphere(meshgengfx,r,r,r,3,2)
		meshgengfx:SetMaterial(gShipPartGeomEditor_Mat_Off) -- this is overwritten by update
		gShipPartGeomEditor_EdgeGizmoMeshName = meshgengfx:RenderableConvertToMesh()
		meshgengfx:Destroy()	
	end
	
	RegisterListener("mouse_left_click",function () ShipPartGeomEditor_Click() end)
	RegisterListener("Hook_MainStep",StepShipPartGeomEditor)
	
	gShipPartGeomEditor_Gizmo = MakeGeomEditGizmo()
end

function StepShipPartGeomEditor ()
	StepTableCam(GetMainCam(),gKeyPressed[key_mouse1] and (not gui.bMouseBlocked),gShipPartGeomEditorTableCamSpeedFactor)
	StepThirdPersonCam(GetMainCam(),gCamThirdPersonDist,gCamThirdPersonX,gCamThirdPersonY,gCamThirdPersonZ)
	if gEditorLight then Client_SetLightPosition(gEditorLight,GetMainCam():GetPos()) end -- update light
	
	-- mousepick
	local rx,ry,rz,rvx,rvy,rvz = GetMouseRay()
	local o = gShipPartGeomEditor_Gizmo
	local mindist,foundedgenum,foundsidename = o:RayPick(rx,ry,rz,rvx,rvy,rvz)
	SetBottomLine(arrdump({foundedgenum,foundsidename}))
end

function ShipPartGeomEditor_Click ()
	local rx,ry,rz,rvx,rvy,rvz = GetMouseRay()
	local o = gShipPartGeomEditor_Gizmo
	local mindist,foundedgenum,foundsidename = o:RayPick(rx,ry,rz,rvx,rvy,rvz)
	if (mindist and foundedgenum) then o:ToggleEdge(foundedgenum) end
	if (mindist and foundsidename) then 
		o.sideskin[foundsidename] = (o.sideskin[foundsidename] == "shippartskin/window") and "shippartskin/metal" or "shippartskin/window"
		o:UpdateGeom()
	end
end
