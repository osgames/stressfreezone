-- plugin for adding hud-markers indicating the direction and distance of ships
-- see also lib.plugin.lua

local pluginenabled = true -- set this to true to enable the plugin
gHudMarkerRootLocation = nil
gHudMarkerObjectUnderMouse = nil
gHudMarkerTarget = nil



if (pluginenabled) then  
	
	RegisterListener("Hook_PreLoad",function () 
		gHudMarkerDialogLayer = guimaker.MyCreateDialog()
		gHudMarkerDialogLayer.panel = guimaker.MakeBorderPanel(gHudMarkerDialogLayer,0,0,0,0,{0,0,0,0})
		gHudMarkerDialogLayer:SendToBack()
	end)
	
	function MakeHudMarker_Edge (obj) 
		local trackgfx = obj.gfx
		local mat = GetPlainTextureMat("objmark_smalledge.png",true)
		local col = obj.objtype.ingame_col
		local r,g,b = unpack(col)
		local vw,vh = GetViewportSize()
		local e = 4
		local o = 2
		local hudmarker1 = guimaker.MakePlane(gHudMarkerDialogLayer.panel,mat,0,0,e,e)
		local hudmarker2 = guimaker.MakePlane(gHudMarkerDialogLayer.panel,mat,0,0,e,e)
		local hudmarker3 = guimaker.MakePlane(gHudMarkerDialogLayer.panel,mat,0,0,e,e)
		local hudmarker4 = guimaker.MakePlane(gHudMarkerDialogLayer.panel,mat,0,0,e,e)
		local arr = {hudmarker1,hudmarker2,hudmarker3,hudmarker4} 
		hudmarker1.gfx:SetTrackOffset( 0-o, 0-o)	hudmarker1.gfx.mvTrackPosTargetSizeFactor = {-1,-1}hudmarker1.gfx:SetUV(0,0,1,1)
		hudmarker2.gfx:SetTrackOffset(-e+o, 0-o)	hudmarker2.gfx.mvTrackPosTargetSizeFactor = { 1,-1}	hudmarker2.gfx:SetUV(1,0,0,1)
		hudmarker3.gfx:SetTrackOffset( 0-o,-e+o)	hudmarker3.gfx.mvTrackPosTargetSizeFactor = {-1, 1}	hudmarker3.gfx:SetUV(0,1,1,0)
		hudmarker4.gfx:SetTrackOffset(-e+o,-e+o)	hudmarker4.gfx.mvTrackPosTargetSizeFactor = { 1, 1}	hudmarker4.gfx:SetUV(1,1,0,0)
		for k,hudmarker in pairs(arr) do 
			hudmarker.gfx:SetColour(r,g,b,1)
			hudmarker.gfx:SetTrackPosSceneNode(trackgfx)
			hudmarker.gfx.mbTrackHideIfClamped = false
			hudmarker.gfx.mbTrackHideIfBehindCam = true
			hudmarker.gfx.mbTrackClamp = true
			hudmarker.gfx.mvTrackClampMin = {0,0}
			hudmarker.gfx.mvTrackClampMax = {vw,vh}
		end
		return { arr=arr, obj=obj, Destroy = function (self)
			if (self.bIsDead) then return end
			for k,hudmarker in pairs(self.arr) do hudmarker:Destroy() end
			self.bIsDead = true
		end }
	end
	
	function MakeHudMarker_Text (obj,text)
		-- trackgfx
		local trackgfx = obj.gfx
		local col = obj.objtype.ingame_col
		local vw,vh = GetViewportSize()
		local fontsize = 12 -- gHudNamesFontSize
		local r,g,b,a = 0,1,0,1
		if (col) then r,g,b,a = unpack(col) end
		local col_text = {r,g,b,a}
		--local tw,th = gFPSField.text.gfx:GetTextBounds()	
		
		if (not gHudNamesDialogLayer) then
			local col_back = {0,0,0,0}
			gHudNamesDialogLayer = guimaker.MyCreateDialog()
			local w,h = 0,0 -- GetViewportSize()
			local stylesetname,stylename
			gHudNamesDialogLayer.panel = guimaker.MakeBorderPanel(gHudNamesDialogLayer,0,0,w,h,col_back,stylesetname,stylename)
			gHudNamesDialogLayer:SendToBack()
		end
		
		local hudname = guimaker.MakeText(gHudNamesDialogLayer.panel,0,0,text,fontsize,col_text)
		--hudname.gfx:SetPos(110,110)
		hudname.gfx:SetTrackPosSceneNode(trackgfx)
		hudname.gfx:SetTextAlignment(kGfx2DAlign_Center)
		
		
		hudname.gfx.mbTrackHideIfClamped = false
		hudname.gfx.mbTrackHideIfBehindCam = true
		hudname.gfx.mbTrackClamp = true
		hudname.gfx.mvTrackClampMin = {0,0}
		hudname.gfx.mvTrackClampMax = {vw,vh}
			
			
		--hudname.gfx:SetColour(r,g,b,a)
		hudname.SetText = function (self,text) if (self.gfx:IsAlive()) then self.gfx:SetText(text) end end
		hudname.mbIgnoreMouseOver = false
		
		
		return hudname
	end
	
	function UpdateHudMarker (obj)
		local bVisible 			= gHudMarkerRootLocation == obj:GetRootLocation()
		local bIsShip 			= obj.objtype.bIsShip
		local bHasSpacePort 	= obj.objtype.bHasSpacePort
		local bIsUnderMouse 	= obj == gHudMarkerObjectUnderMouse
		local bIsSelected 		= obj == gHudMarkerTarget
		local bIsClientBody 	= obj == gClientBody
		local bHasEdgeMarkers 	= bVisible and (bIsUnderMouse or bIsSelected or bIsShip or bHasSpacePort)
		local bHasText 			= bVisible and (bIsUnderMouse or bIsSelected or (gHUDShowAllNames and gHUDSANType and gHUDSANType(obj.objtype,obj)))
		
		-- hudmarker_edge
		if (bHasEdgeMarkers) then 
			if (not obj.hudmarker_edge) then
				obj.hudmarker_edge = MakeHudMarker_Edge(obj) 
				obj:AddToDestroyList(obj.hudmarker_edge)
			end
		elseif (obj.hudmarker_edge) then -- hide:destroy
			obj:RemoveFromDestroyList(obj.hudmarker_edge)
			obj.hudmarker_edge:Destroy()
			obj.hudmarker_edge = nil
		end
		
		-- hudmarker_text
		if (bHasText) then
			local text = obj:GetTypeAndNameText()
			if (not obj.hudmarker_text) then
				-- create
				obj.hudmarker_text = MakeHudMarker_Text(obj,text) 
				obj:AddToDestroyList(obj.hudmarker_text)
			else
				-- update
				local bUpdate = true
				if gKeyPressed[key_lalt] and gHUDSANType and gHUDSANType(obj.objtype,obj) then
					local x,y = obj.hudmarker_text.gfx:GetPos()
					local d = (gHUDMouseX-x)*(gHUDMouseX-x) + (gHUDMouseY-y)*(gHUDMouseY-y)
					local s = math.max(26-d*14/10000,12)
					if math.abs((obj.hudmarker_text._s or 12)-s)<1 then bUpdate = false end
					if bUpdate then
						obj.hudmarker_text.gfx:SetCharHeight(s)
						obj.hudmarker_text._s = s
					end
				end
				if bUpdate then
					obj.hudmarker_text:SetText(text)
				end
			end
		elseif (obj.hudmarker_text and obj.hudmarker_text:IsAlive()) then -- hide:destroy
			obj:RemoveFromDestroyList(obj.hudmarker_text)
			obj.hudmarker_text:Destroy()
			obj.hudmarker_text = nil
		end
	end
	
	-- calls UpdateHudMarker for every game object
	function UpdateAllHudMarkers ()
		for k1,loc in pairs(gLocationsByID) do 
			for k2,obj in pairs(loc.childs) do 
				UpdateHudMarker(obj)
			end
		end
	end
	
	function HudMarkerSetRootLocation (loc)
		if (loc == gHudMarkerRootLocation) then return end
		gHudMarkerRootLocation = loc
		UpdateAllHudMarkers()
	end
	
	RegisterListener("Hook_Client_SetBody",function (newbody,oldbody)
			if (not newbody) then return end
			HudMarkerSetRootLocation(newbody:GetRootLocation())
		end)
		
	RegisterListener("Hook_Client_MousePick",function (objectUnderMouse)
			if (gHudMarkerObjectUnderMouse ~= objectUnderMouse) then
				local old = gHudMarkerObjectUnderMouse
				local new = objectUnderMouse
				gHudMarkerObjectUnderMouse = objectUnderMouse
				if (old) then UpdateHudMarker(old) end
				if (new) then UpdateHudMarker(new) end
			end
		end)
		
	RegisterListener("Hook_Client_SetTarget",function (target)
			if (gHudMarkerTarget ~= target) then
				local old = gHudMarkerTarget
				local new = objectUnderMouse
				gHudMarkerTarget = target
				if (old and old:IsAlive()) then UpdateHudMarker(old) end
				if (new and new:IsAlive()) then UpdateHudMarker(new) end
			end
		end)
		
	-- location assignment, happens during spawn and when location changes (e.g. docking)
	RegisterListener("Hook_Object_SetParentLocation",function (obj,oldloc,newloc)
			if (obj == gClientBody) then HudMarkerSetRootLocation(gClientBody:GetRootLocation()) end
			UpdateHudMarker(obj)
		end)
	
	RegisterListener("keydown", function(key,char,bConsumed)
			if key == key_rwin then
				gHUDShowAllNames = true
				gDisableSteering = true
				gHUDSANType = function() return true end
				UpdateAllHudMarkers ()
			end
			if gKeyPressed[key_rwin] then
				if key == key_a then
					gHUDSANType = function(t) return t:IsSubType("asteroid") end
				elseif key == key_b then
					gHUDSANType = function(t) return t.bHasSpacePort end
				elseif key == key_s then
					gHUDSANType = function(t) return t.bIsShip end
				elseif key == key_e then -- TODO: implement enemy state
					gHUDSANType = function(t,o) return o.bIsPirate end
				end
				UpdateAllHudMarkers ()
			end
	end)
	
	RegisterListener("keyup", function(key)
			if key == key_rwin then
				gHUDShowAllNames = false
				gHUDSANType = false
				gDisableSteering = false
				UpdateAllHudMarkers ()
			end
	end)
	
	
	RegisterListener("Hook_MainStep",function () 
		gHUDMouseX,gHUDMouseY = GetMousePos()
		if gHUDNamesResize and gKeyPressed[key_lalt] and math.mod(gMyFrameCounter,2) then
			UpdateAllHudMarkers()
		end
	end)
end

--[[
old code...

-- for playerid,body in pairs(gClientPlayerBodies) do if (playerid ~= giClientPlayerID) then ... end
	
--~ 	function MakeObjectHudShipMarker (trackgfx,col) 
--~ 		local mat = GetPlainTextureMat("raute.png")
--~ 		mat = GetHuedMat(mat,1,1,1, 1,1,1,0.5)
--~ 		local e = 32
--~ 		local hudmarker = guimaker.MakePlane(gHudMarkerDialogLayer.panel,mat,-0.5*e,-0.5*e,e,e)
--~ 		hudmarker.gfx:SetTrackOffset(-0.5*e,-0.5*e)
--~ 		hudmarker.gfx:SetTrackPosSceneNode(trackgfx)
--~ 		hudmarker.gfx.mbTrackHideIfClamped = false
--~ 		hudmarker.gfx.mbTrackHideIfBehindCam = false
--~ 		local vw,vh = GetViewportSize()
--~ 		hudmarker.gfx.mbTrackClamp = true
--~ 		hudmarker.gfx.mvTrackClampMin = {0,0}
--~ 		hudmarker.gfx.mvTrackClampMax = {vw,vh}
--~ 		return hudmarker
--~ 	end
--~ 	function StepHudMarker (obj)
--~ 		if ( gClientBody and gClientBody:IsAlive() and obj and obj:IsAlive() ) then
--~ 			-- print("StepHudMarker",obj,obj:IsAlive(),gClientBody,gClientBody:IsAlive())
--~ 			local x1,y1,z1 = unpack(obj.mvPos)
--~ 			local x2,y2,z2 = unpack(gClientBody.mvPos)
--~ 			local dist = Vector.len(x1-x2,y1-y2,z1-z2)
--~ 			local invisdist = 4000
--~ 			local maxa = 0.7
--~ 			local a = math.max(0.2,maxa*(1.0 - dist/invisdist))
--~ 			local col = obj.objtype.ingame_col
--~ 			local r,g,b = 0,1,0
--~ 			if (col) then r,g,b = unpack(col) end
--~ 			obj.hudmarker.gfx:SetColour(r,g,b,a)
--~ 			--print("StepHudMarker",obj.id,dist,a)
--~ 		end
--~ 	end
--~ 			obj.hudmarker = MakeObjectHudShipMarker(obj.gfx,obj.objtype.ingame_col) 
--~ 			local myobj = obj -- closure
--~ 			StepHudMarker(myobj)
--~ 			obj.hudmarker_stepper = RegisterListener("Hook_MainStep",function () StepHudMarker(myobj) end)
--~ 			UnregisterListener("Hook_MainStep",obj.hudmarker_stepper)
--~ 			obj.hudmarker_stepper = nil
]]--
