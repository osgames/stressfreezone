-- Standard group AI functions, in these "self" refers to the ship
-- group the AI is controlling
--
-- The code for this AI group was taken from the old lib.enemygroup.lua code
-- it is always used unless a different AI is specified

gStdAI = {}

gStdAI.Attack = function (self,target)
	self:SetState("Attacking", target)

	for k,v in pairs(self.ships) do
		AutoPilot_Follow(v,target)
		v.target = target
	end

	-- create the step function for the "Attack" state
	
	self.Step = function (self)

		for i,v in pairs(self.ships) do

			-- why the hell dosn't lua have a c-like continue statement?
			if v:IsAlive() then 
				if v.target:IsAlive() then
					-- hunting target
					
					-- TODO: shouldn't all this rotation code be part of AutoPilot_Follow?
					local x1,y1,z1 = unpack(v.target.mvPos)
					local x2,y2,z2 = unpack(v.mvPos)
					local qw,qx,qy,qz = Quaternion.getRotation(0,0,-1,x1-x2,y1-y2,z1-z2)
					qw,qx,qy,qz = Quaternion.normalise(qw,qx,qy,qz)
					-- todo : calc delta as per second, to enable other clients smoothing/prediction
					v.mqTurn = {1,0,0,0}
					v.mqRot = {qw,qx,qy,qz}
					v:MarkChanged()
					FirePrimaryWeapons(v, v.target) -- target might be dead after this
				else
					-- target is dead
					AutoPilot_Stop(v)
					v.target = nil
					-- return home
					self:Move(self:CountShipsAlive() * 5, unpack(self.homePos))
				end
			end
		end

		return
	end

	return
end

gStdAI.Idle = function (self)
	self:SetState("Idle")

	-- TODO: make it scan for hostiles while idling, rather than waiting to get attacked
	self.Step = function (self) return end	

	return
end

gStdAI.Move = function (self, radius, x, y, z)
	self:SetState("Moving", {x, y, z})

	for k,v in pairs(self.ships) do
		AutoPilot_GoTo(v, 0, Vector.random2(
		               x-radius,y-radius,z-radius,
		               x+radius,y+radius,z+radius)
		)
	end

	-- the autopilot code automaticly stops, so a blank stepper is fine
	self.Step = function (self) return end

end

gStdAI.onDamage = function (ship, attacker, shielddamage, hulldamage)
	ship.group:Attack(attacker)
	ship.lastattacker = attacker
end

gStdAI.onDeath = function (ship) return end


gStdAI.Init   = function (self) return end
gStdAI.Deinit = function (self) return end

-- Register the Group AI
RegisterGroupAI("Standard", gStdAI)
