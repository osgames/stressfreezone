-- see also sol.lua
-- see also http://en.wikipedia.org/wiki/Lalande_21185
-- this star is thought to possess at least two Jupiter-sized planets in orbit
local s = RegisterSystem("Lalande 21185")
s:AddSun("sun",s.name,"M2.0V")
