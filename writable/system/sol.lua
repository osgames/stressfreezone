--[[ 
see also 	
	http://en.wikipedia.org/wiki/Solar_system
	http://en.wikipedia.org/wiki/Planets
	http://en.wikipedia.org/wiki/List_of_nearest_stars
	http://en.wikipedia.org/wiki/Stellar_Classification 
nearby systems :  (ly : lightyears distance from sol)
	Alpha Centauri	(3 sun, 4.3 ly)
	Barnard's Star	(1 sun, 6.0 ly)
	Wolf 359 		(1 sun, 7.8 ly)
	Lalande 21185	(1 sun, 8.3 ly)
	Sirius			(2 sun, 8.6 ly)
]]--

local s = RegisterSystem("Sol")
s:AddConnections("Alpha Centauri","Barnard's Star","Wolf 359","Lalande 21185","Sirius")
s:AddSun("sun","Sun","G2V") -- 2nd is stellar classification

-- 							type,			data(see below),						moons,	atmosphere
s:AddPlanetEx("planet/inferno", 		"Mercury",	"Terrestrial",	0.39,0.06,0.39,0.24,3.38,0.206,58.64,	0,		""			)	
s:AddPlanetEx("planet/desert",			"Venus",	"Terrestrial",	0.95,0.82,0.72,0.62,3.86,0.007,-243.02,	0,		"CO2,N2"	)
s:AddPlanetEx("planet/terran", 			"Earth",	"Terrestrial",	1.00,1.00,1.00,1.00,7.25,0.017,1.00,	1,		"N2,O2"		)
s:AddPlanetEx("planet/barren/red", 		"Mars",		"Terrestrial",	0.53,0.11,1.52,1.88,5.65,0.093,1.03,	2,		"CO2,N2"	)
s:AddPlanetEx("planet/gas/brown",		"Jupiter",	"Gas Giant",	11.21,317.8,5.20,11.86,6.09,0.048,0.41,	63,		"H2,He"		)
s:AddPlanetEx("planet/gas/mixed", 		"Saturn",	"Gas Giant",	9.41,95.2,9.54,29.46,5.51,0.054,0.43,	56,		"H2,He"		)
s:AddPlanetEx("planet/gas/bright", 		"Uranus",	"Gas Giant",	3.98,14.6,19.22,84.01,6.48,0.047,-0.72,	27,		"H2,He"		)
s:AddPlanetEx("planet/gas/dark", 		"Neptune",	"Gas Giant",	3.81,17.2,30.06,164.8,6.43,0.009,0.67,	13,		"H2,He"		)
s:AddPlanetEx("planet/gas/inferno",		"Betthupferl",	"Gas Giant",	3.81,17.2,6.06,164.8,6.43,0.009,0.67,	13,		"H2,He"		)
s:AddPlanetEx("planet/gaia",			"Gaia",		"Terrestrial",	3.81,17.2,5.6,164.8,6.43,0.009,0.67,	13,		"H2,He"		)

--[[
planet data :
Equatorial diameter[a]	
Mass[a]	
Orbital radius (AU)	
Orbital period (years)	
Inclination to Sun's equator (�)	
Orbital eccentricity	
Rotation period (days)
]]--


-- (TODO  :Ceres	Pluto	Eris)






