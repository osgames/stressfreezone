-- see also sol.lua
-- see also http://en.wikipedia.org/wiki/Alpha_Centauri
local s = RegisterSystem("Alpha Centauri")
s:AddSun("sun","Alpha Centauri A","G2V")
s:AddSun("sun","Alpha Centauri B","K0V")
s:AddSun("sun","Proxima Centauri","M5.5Ve")

--[[
Computer models of planetary formation suggest that terrestrial planets 
would be able to form close to both Alpha Centauri A and B.
]]--
