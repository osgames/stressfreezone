//#ifndef HUDELEMENT2D_H
#if 0  // OBSOLETE FILE
#define HUDELEMENT2D_H
#include "lugre_listener.h"
#include "lugre_gfx2D.h"
#include <OgrePrerequisites.h>
#include <OgreVector2.h>

class lua_State;
class cObject;
	
/// manages all kinds of hud elements, 
/// TODO : split up into base class and derived classes to use inheritance, but lua binding the whole thing in one is easier for now..
	
class cHUDElement2D : public cListener { public:
	
	/// only usable if parent == null
	enum {
		kPosMode_None,			/// no positoning every frame, use SetPos()					crosshair, meters...
		kPosMode_Object,		/// pos=screenpos(mpObj1) + mvParam*screensize(mpObj1) 		target-indicators, target-info,...
		kPosMode_Aim,			/// pos=where to aim for mpObj1 shooting at mpObj2 with bulletvelocity=mfParam		
		kPosMode_ObjectDir,		/// only visible if target is not on screen, and then always on the border of the screen, min=mvParam max=mvParam2
	}; /// for miPosMode, but kept as size_t for lua binding
	
	/// only usable if parent == null
	enum {
		kTurnMode_None,			/// no turning
		kTurnMode_Constant,		/// turnspeed = mfTurnBase
		kTurnMode_ObjectDist,	/// dist to obj = x, turnspeed = mfTurnBase + mfTurnParam*x + mfTurnParam2*x^2  clipped to [mfTurnMin;mfTurnMax]
		kTurnMode_ObjectDir,	/// rotation is set to point to object direction + mfTurnBase, useful together with kPosMode_ObjectDir
	}; /// for miTurnMode, but kept as size_t for lua binding
	
	/// only usable if mpText != null, SetText every half second
	enum {
		kTextMode_None,			/// no turning
		kTextMode_Dist,			/// distance between mpObj1 and mpObj2
		kTextMode_RelSpeed,		/// relative speed between mpObj1 and mpObj2
		kTextMode_FPS,			/// current fps, updated every half second
		kTextMode_AbsSpeed,			/// absolute speed of mpObj1 or player object
	}; /// for miTextMode, but kept as size_t for lua binding
	
	bool							mbObjHasBeenKilled;
	bool							mbVisible;
	bool							mbPosModeObject_UpdateSize; // hudelement size is set to the screensize of the object * mvSizeParam
	bool							mbWatchMouse; // true => element triggers mouseenter and mouseleave callback
	bool							mbMouseIsOver; // used by mbWatchMouse
	size_t							miUID;
	size_t							miPosMode;
	size_t							miTurnMode;
	size_t							miTextMode;
	Ogre::Vector2					mvBase;
	Ogre::Vector2					mvParam;
	Ogre::Vector2					mvParam2;
	Ogre::Vector2					mvSizeParam;
	Ogre::Real						mfParam;
	Ogre::Real						mfCurRotate; // in radians , needed for SetRotate(mfCurRotate + x)
	Ogre::Real						mfTurnBase;
	Ogre::Real						mfTurnParam;
	Ogre::Real						mfTurnParam2;
	Ogre::Real						mfTurnMin;
	Ogre::Real						mfTurnMax;
	cSmartPtr<cObject>				mpObj1; // usually set to target
	static Ogre::Overlay*			mpHUDOverlay; // one global overlay
	cGfx2D*							mpGfx2D;
	
			 cHUDElement2D	();
	virtual ~cHUDElement2D	();
	
	virtual void Listener_Notify (cListenable* pTarget,const size_t eventcode = 0,const size_t param = 0,const size_t userdata = 0);

	void	Init			();
	void	VarUpdate		();
	void	TrackingStep	();
	static bool	GetScreenPos	(const Ogre::Vector3& pos,Ogre::Real& x,Ogre::Real& y,const Ogre::Real rad,Ogre::Real& cx,Ogre::Real& cy);
	
	// lua binding
	static void		LuaRegister 		(lua_State *L);
};

#endif
