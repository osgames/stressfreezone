#ifndef OBJECT_H
#define OBJECT_H

#include "lugre_smartptr.h"
#include "location.h"
#include "objectcontroller.h"
#include <list>

#define kResyncMessageLen (4+2+4+3*3+2*4)

#include <OgreVector3.h>
#include <OgreQuaternion.h>
using Ogre::Vector3;
using Ogre::Quaternion;
using namespace Lugre;

class lua_State;
class cLocation;
class cObjectController;
	
namespace Lugre {
	class cFIFO;
	class cGfx3D;
	class cOdeObject;
};

/// holds position info and manages resyncing, an "object" doesn't need to be something visible, can also be a location
/// about miLastChangeTime : the meaning of this is defined by scripts, see cPlayer::ServerSendResyncs
/// about miResyncCounterHigh : the client should ignore all (udp) resync messages which don't match the current value of this
///  this should only be updated on the client via reliable network message, e.g. teleport, locationchange etc
///  this is to avoid strange effects by udp resync packets arriving after (or even before) the coordinate system was changed
class cObject : public cSmartPointable { public :
	cOdeObject*			mpPhysicForm; ///< mass,collision geometry,...
	cObjectController*	mpController; ///< autopilot,...
	int					miLastChangeTime; ///< object wich are not "changed" don't need to be resynced so often. 
	int					miResyncCounterHigh; ///< changed on major events like location change, etc
	uint32				miResyncCounterLow; ///< avoids problems with udp resync packets arriving late
	float				mfMaxSpeed;
	float				mfBoundingRad;
	float				mfDistToLocationCenter;
	bool				mbResynced; ///< true by default, if this is false no resyncs for this obj will be sent at all  
	Vector3				mvPos,mvVel,mvAccel;
	Quaternion			mqRot,mqTurn;
	cGfx3D*				mpGfx3D; ///< not owned by object : destroy this yourself,  position/rotation is updated in Step()
	
			 cObject();
	virtual	~cObject();
	
	void				Step			(const int iCurTime,const float fPhysStepTime,const bool bIncResyncCounterLow=false);
	
	void				SaveResyncData	(cFIFO& pFIFO);
	void				LoadResyncData	(cFIFO& pFIFO);
	static void			SkipResyncData	(cFIFO& pFIFO);
	
	void				SetController	(cObjectController* pObjectController);
	
	void				SetParentLocation	(cLocation* pParentLocation);
	inline cLocation*	GetParentLocation	() { return mpParentLocation; }
	
	/// don't use this directly ! only to be used for efficient mass delete from location
	inline void			_HackForgetParentLocation	() { mpParentLocation = 0; }
	
	inline int			GetID	() { return miID; }
	void				SetID	(const int iID);
	
	// lua binding
	static void		LuaRegister 	(lua_State *L);
	
	private:
	// keeping insert position iterors enables constant time removal later
	cLocation*						mpParentLocation; ///< defines the parent coordinate system
	std::list<cObject*>::iterator	_mLocationInsertPos;
	int								miID;
};

#endif
