#ifndef PHYSICFORM_H
#define PHYSICFORM_H

#include "lugre_smartptr.h"

using namespace Lugre;
class lua_State;

/// autopilot, etc
class cPhysicForm : public cSmartPointable { public :
	
	// lua binding
	static void		LuaRegister 	(lua_State *L);
};

#endif
