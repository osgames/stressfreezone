#ifndef OBJECTCONTROLLER_H
#define OBJECTCONTROLLER_H

#include "lugre_smartptr.h"

#include <OgreVector3.h>
#include <OgreQuaternion.h>
using Ogre::Vector3;
using Ogre::Quaternion;
using namespace Lugre;

class lua_State;
class cObject;

class cObjectContollerTarget { public:
	// only call if alive
	virtual Vector3 GetPosition() = 0;
	virtual Vector3 GetVelocity() = 0;
	
	virtual bool	IsAlive() = 0;
	
	virtual ~cObjectContollerTarget();
};

class cObjectContollerTargetObject : public cObjectContollerTarget { public:
	cSmartPtr<cObject>	mpApproachObject;
	
	// only call if alive
	virtual Vector3 GetPosition();
	virtual Vector3 GetVelocity();
	
	virtual bool	IsAlive();

	cObjectContollerTargetObject(cObject* pObject);
	virtual ~cObjectContollerTargetObject();
};

class cObjectContollerTargetPosition : public cObjectContollerTarget { public:
	Vector3	mvPosition;
	
	// only call if alive
	virtual Vector3 GetPosition();
	virtual Vector3 GetVelocity();

	virtual bool	IsAlive();

	cObjectContollerTargetPosition(Vector3 vPosition);
	virtual ~cObjectContollerTargetPosition();
};

/// autopilot, etc
class cObjectController : public cSmartPointable { public :
	cObjectContollerTarget*	mpApproachTarget;
	float					mfApproachMinDist;
	float					mfMaxAccel;
	float					mfMaxSpeed;
	bool					mbStupid;
	
			 cObjectController	();
	virtual ~cObjectController	();
	
	// the controller takes over the ownership of the target pointer
	// never touch the pointer again!!
	void	SetTarget	(cObjectContollerTarget *pTarget);
	
	void	Lock	(); ///< miControlledObjectCount++
	void	Release	(); ///< miControlledObjectCount-- 
	void	Step	(cObject* pObj);
	
	// lua binding
	static void		LuaRegister 	(lua_State *L);
	
	private:
	int		miControlledObjectCount; ///< mainly for debugging
};

#endif
