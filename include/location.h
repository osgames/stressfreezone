#ifndef LOCATION_H
#define LOCATION_H

#include "lugre_smartptr.h"
#include "lugre_net.h"
#include <list>

using namespace Lugre;

class lua_State;
class cObject;
	
namespace Lugre {
	class cFIFO;
	class cUDP_SendSocket;
};
	
/// spatial grouping of objects, useful for gfx-LOD, reducing network traffic, hierarchy, mapmaking...
class cLocation : public cSmartPointable { public:
	std::list<cObject*>		mlObjects;
	float					mfBoundingRad; ///< updated during StepAllObjects(), max(dist+rad) of all objects
	
	cLocation			();
	virtual ~cLocation	();
	
	void	StepAllObjects		(const int iCurTime,const float fPhysStepTime,const bool bIncResyncCounterLow=false);
	void	DeleteAllObjects	();
	int		CountParents		();
	int		SendResyncs			(cUDP_SendSocket& pUDPSocket,const uint32 iAddr,const int iPort,const int iMinLastChangeTime=0,const float fRandomResyncProb=0.0);
	int		StoreResyncs		(cFIFO& pFIFO,const int iMinLastChangeTime=0,const float fRandomResyncProb=0.0);
	
	// lua binding
	static void		LuaRegister 	(lua_State *L);
};

#endif
