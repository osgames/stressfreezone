#ifndef ROB_PRIMITIVES_H
#define ROB_PRIMITIVES_H

#include <OgreSimpleRenderable.h>
#include <OgreCamera.h>
#include <vector>

/**	Welcome to the GhoulyPrimitives library
 *	the main purpose of this library is to create "primitives", simple geometric objects like sphere, ellipsoid, cone, cylinder...
 *	this library is by default bound to ogre, but that can be changed easily, as the geometry generation is implemented independantly
 *	generation of geometric data has been seperated from usage of such data (e.g. rendering)
 *	generators for geometric are derived from the "Primitive" class
 *	users of such data are derived from the "Drawer" class
 *	thus it is possible to use the same data for different purposes, for example :
 *	- Drawing the Data on a 3dCard, the "OgreRenderableDrawer" does that (actually it rather transfers the data to vertex and index buffers)
 *	- Generating Mesh Data, so it can be saved to disk, the "OgreMeshDrawer" class will do that, but it is not finished yet
 *	- Constructing Complex Objects from Base data, for example a bunch of 3d-Steel-Pylons from Lines
 *  also using something like the DesignPattern "Decorator" it is possible to have a stack of Drawers,
 *	representing a processing pipe before sending the data to the 3d Card, 
 *	this can be used for position or texcoord based coloring and transformation,
 *	or "SpaceWarps" like twisting a mesh around the z Axis, based on the z coordinate of the vertices
 *	(Warning ! Vertex Colors are not yet implemented here)
 *	
 *	this library is not yet finished, but i think it is useful already
 *	Questions and Comments are welcome, just drop me an eMail to ghoulsblade@schattenkind.net
 *	i'm interested to see where this lib is used
 *	if you like spacegames, you might want to visit the project i'm currently working on : http://sfz.schattenkind.net
 *	for more code snippets for c++, php and other languages, visit my homepage http://ghoulsblade.schattenkind.net
 *	
 *	Have fun =)
 */

// who needs meshes, all is loft !
// multiple inheritance rocks !

// TODO : lua bindings
// TODO : vertex colors
// TODO : OgreMeshDrawer (to generate a mesh instead of filling vertex buffers, that can be exported, 

namespace GhoulPrimitive {

class Primitive;
class Drawer;
	
// types  (edit me for binding to something different than ogre)
typedef Ogre::Real			Real;
typedef Ogre::Vector3		Vector3;
typedef Ogre::Quaternion	Quaternion;
typedef Ogre::Camera		Camera;
typedef unsigned short 		IndexInt; // for hardware-index-buffer (vertex index)

typedef const Real			kReal;
typedef const Vector3		kVector3;
typedef const Quaternion	kQuaternion;
	
// constants
const Real pi = 		3.14159265358979323846;	// math.h : M_PI
const Real pi2 = 2.0*	3.14159265358979323846;	// (pi*2.0)

typedef enum {
	kInterpolate_Constant,
	kInterpolate_Linear,
	kInterpolate_SmoothQuadric, // quadric with automatic tangents
	// kInterpolate_Quadric, // TODO? : maybe handle tangent points extra...
} eInterpolationMode;
	
typedef enum {
	kOpType_POINT_LIST,		// Ogre::RenderOperation::OT_POINT_LIST
	kOpType_LINE_LIST,
	kOpType_LINE_STRIP,
	kOpType_TRIANGLE_LIST,
	kOpType_TRIANGLE_STRIP,
	kOpType_TRIANGLE_FAN,
} eOpType;

typedef enum {
	// gives a hint at the frequency of geometry changes 
	kGeometryChange_Seldom,				// Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY
	kGeometryChange_Often,				// Ogre::HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY
	kGeometryChange_AlmostEveryFrame,	// Ogre::HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE
} eGeometryChange;

/// always call prepare before, and finish after adding vertices and indices
/// abstract Base Class for different types of drawing (DrawToMesh,Renderable,Decorators for Scaling, UV switching/animating, Coloring...)
class Drawer {
	public:
	Drawer();
	virtual ~Drawer();
	
	virtual void	Prepare		(eOpType opType,eGeometryChange gcHint,size_t vertexCount,size_t indexCount=0,bool hasNormals=true,bool hasTexCoords=true); // renderop(trilist,linelist...)
			void	AddVertex	(kVector3 p,kVector3 n=Vector3::ZERO,kReal u=0.0,kReal v=0.0);
	virtual void	AddVertex	(kReal x,kReal y,kReal z=0.0,kReal nx=0.0,kReal ny=0.0,kReal nz=0.0,kReal u=0.0,kReal v=0.0);
	virtual void	AddIndex	(IndexInt i);
	virtual void	Finish		();  
};


class OgreRenderableDrawer : public Drawer,public Ogre::SimpleRenderable {
	public:
	/// construction, a primitive can be passed to initialize the buffers right away
	OgreRenderableDrawer			();
	virtual ~OgreRenderableDrawer	();
	
	// TODO : lua binding ??
	
	/// Drawer interface
	virtual void	Prepare		(eOpType opType,eGeometryChange gcHint,size_t vertexCount,size_t indexCount=0,bool hasNormals=true,bool hasTexCoords=true); // renderop(trilist,linelist...)
	virtual void	AddVertex	(kReal x,kReal y,kReal z=0.0,kReal nx=0.0,kReal ny=0.0,kReal nz=0.0,kReal u=0.0,kReal v=0.0);
	virtual void	AddIndex	(IndexInt i);
	virtual void	Finish		();
	
	static Ogre::RenderOperation::OperationType		GetOgreOpType	(const eOpType opType);
	static Ogre::HardwareBuffer::Usage				GetOgreHWBUsage	(const eGeometryChange geometryChangeHint);
	
	
	/// Implementation of Ogre::SimpleRenderable
	virtual Ogre::Real getBoundingRadius(void) const;
	/// Implementation of Ogre::SimpleRenderable
	virtual Ogre::Real getSquaredViewDepth(const Ogre::Camera* cam) const;
	
	protected:
	bool		mReady;
	bool		mVertexDecInitialized;
	bool		mIndexBufferInitialized;
	bool		mHasNormals;
	bool		mHasTexCoords;
	bool		mBoundingBoxEmpty;
	Vector3		mvAABMin;
	Vector3		mvAABMax;
	Real*		mWritePtr;
	IndexInt*	mIndexWritePtr;
	Ogre::HardwareVertexBufferSharedPtr	mHWVBuf;
	Ogre::HardwareIndexBufferSharedPtr	mHWIBuf;
	size_t		miVerticesLeft;
	size_t		miIndicesLeft;
	size_t		mVertexBufferCapacity;
	size_t		mIndexBufferCapacity;
};


class Primitive {
	/// (abstract) Base Class with utility functions 
	public:
	
	/// interface
	virtual	void	Update			(Drawer& drawer,bool bHasNormals=true,bool bHasTexCoords=true);
	// TODO : (getboundingbox,boundingsphere..)
	
	// construction
	Primitive();
	virtual ~Primitive();
	
	/// utils
	static void		Circle					(Real* a,size_t blocks,size_t components=2,kReal radius=1.0,kReal startang=0.0,kReal endang=pi2,bool setOtherComponentsToZero=true);
	static void		Ellipse					(Real* a,size_t blocks,size_t components=2,kReal radiusx=1.0,kReal radiusy=1.0,kReal startang=0.0,kReal endang=pi2,bool setOtherComponentsToZero=true);
	
	// utils : interpolation
	static void		InterpolatateFloatV 	(kReal* source,Real* dest,kReal t,size_t num=1,size_t startoff=0,size_t bufsize=2,eInterpolationMode mode=kInterpolate_Linear,size_t stride=0); 
	static Real		InterpolatateFloat 		(kReal* source,kReal t,size_t startoff=0,size_t bufsize=2,eInterpolationMode mode=kInterpolate_Linear,size_t stride=0); 
	/// t=0 -> p1, t=1 -> p2
	static Real		InterpolatateFloat_Linear	(kReal p1,kReal p2,kReal t); 
	/// t=0 -> p1, t=1 -> p2
	static Real		InterpolatateFloat_Smooth	(kReal q1,kReal p1,kReal p2,kReal q2,kReal t);
	
	// static void Interpolatate_quaternion4_x	slerp,squad
};


/// WARNING ! the implementation of this loft is still incomplete, as it does not correctly change normals along the path,
/// this does not affect the sphere-like forms, which use autogenerated normals based on the center (mSphericalAutoNormals=true)
/// it is also ok for some symetric forms ( X ) where normals don't need to be adjusted
class Loft : public Primitive {
	public:
	// interface
	
	/// construction
	Loft();
	virtual ~Loft();
	
	// interface
	virtual	void	Update			(Drawer& drawer,bool bHasNormals=true,bool bHasTexCoords=true);
	
	// the "base" polygon defined by BasePoints is extruded along the "path" defined by PathPoints
	// the "base" defines the first texcoord "u"
	// the "path" defines the second texcoord "v"
	
	void	ClearBasePoints	();
	void	SetBasePoint	(size_t i,	kVector3 p,kVector3 n=Vector3::ZERO,kReal u=0);
	void	AddBasePoint	(			kVector3 p,kVector3 n=Vector3::ZERO,kReal u=0);
	
	void	ClearPathPoints	();
	void	SetPathPoint	(size_t i,	kVector3 p,kReal uBias=0,kReal v=0,kVector3 scale=Vector3::UNIT_SCALE,kQuaternion rot=Quaternion::IDENTITY);
	void	AddPathPoint	(			kVector3 p,kReal uBias=0,kReal v=0,kVector3 scale=Vector3::UNIT_SCALE,kQuaternion rot=Quaternion::IDENTITY);
	
	bool					mBase_bAutoNormal;				/// default = true, if (normal = zero) normal = norm(basepoint)
	bool					mUseScale; 						/// default = true
	bool 					mUseRot; 						/// default = true
	bool					mAutoBackSide;					/// default = false, add inverted base form (with inverted normals)
	bool					mSphericalAutoNormals;			/// default = false, normals generated from normalized "absolute" position, used for sphere
	Vector3					mSphericalAutoNormals_Center;	/// default = ZERO
	
	private:
		
    class BasePoint {
		public:
		Vector3		p;
		Vector3		n;
		Real		u;
        inline BasePoint () { }
        inline BasePoint (kVector3 p,kVector3 n,kReal u) : p(p), n(n), u(u) {}
        inline BasePoint (const BasePoint& a) : p(a.p), n(a.n), u(a.u) {}
	};
    class PathPoint {
		public:
		Vector3 	p;
		Real 		v;
		Real 		uBias;
		Vector3 	scale;
		Quaternion	rot;
        inline PathPoint () { }
        inline PathPoint (kVector3 p,kReal v,kReal uBias,kVector3 scale,kQuaternion rot) : p(p), v(v), uBias(uBias), scale(scale), rot(rot) {}
        inline PathPoint (const PathPoint& a) : p(a.p), v(a.v), uBias(a.uBias), scale(a.scale), rot(a.rot) {}
	};
	std::vector<BasePoint>		mlBasePoint;
	std::vector<PathPoint>		mlPathPoint;
};


/// ellipsoid
class Ellipsoid : public Loft { public:
	virtual ~Ellipsoid();
	Ellipsoid				(kVector3 pos=Vector3::ZERO,kVector3 rad=Vector3::UNIT_SCALE,size_t rings=11,size_t segments=23);
	Ellipsoid&	SetParams	(kVector3 pos=Vector3::ZERO,kVector3 rad=Vector3::UNIT_SCALE,size_t rings=11,size_t segments=23);
};
class OgreEllipsoid : public Ellipsoid,public OgreRenderableDrawer { public:
	virtual ~OgreEllipsoid();
	OgreEllipsoid			(kVector3 pos=Vector3::ZERO,kVector3 rad=Vector3::UNIT_SCALE,size_t rings=11,size_t segments=23);
};

/// cone
class Cone : public Loft { public:
	virtual ~Cone();
	Cone					(kVector3 pos1=Vector3::ZERO,kVector3 pos2=Vector3::UNIT_Y,kReal rad1X=1.0,kReal rad1Y=1.0,kReal endscale=1.0,size_t segments=23,size_t height_segments=1);
	Cone&	SetParams		(kVector3 pos1=Vector3::ZERO,kVector3 pos2=Vector3::UNIT_Y,kReal rad1X=1.0,kReal rad1Y=1.0,kReal endscale=1.0,size_t segments=23,size_t height_segments=1);
};
class OgreCone : public Cone, public OgreRenderableDrawer { public:
	virtual ~OgreCone();
	OgreCone				(kVector3 pos1=Vector3::ZERO,kVector3 pos2=Vector3::UNIT_Y,kReal rad1X=1.0,kReal rad1Y=1.0,kReal endscale=1.0,size_t segments=23,size_t height_segments=1);
};

}; // end namespace


#if 0 // NOTES 

	/*
	// todo : hyperloft : mutliple different base-polygons, all with the same vertexcount, and every pathpoint gets a FLOAT to interpolate (quadric) between them
	// LOFT-LOD : just change indices ?
	// loft-wireframe... obvious... different modi : only u, only v

	/// interpolation modes for different path parameters
	eInterpolationMode		mInterpolate_Path_Pos;
	eInterpolationMode		mInterpolate_Path_V;
	eInterpolationMode		mInterpolate_Path_UBias;
	eInterpolationMode		mInterpolate_Path_Scale; 		
	eInterpolationMode		mInterpolate_Path_Rotation;	

	mInterpolate_Path_Pos 		= kInterpolate_Linear;
	mInterpolate_Path_V 		= kInterpolate_Linear;
	mInterpolate_Path_UBias 	= kInterpolate_Linear;
	mInterpolate_Path_Scale 	= kInterpolate_Linear;
	mInterpolate_Path_Rotation 	= kInterpolate_Linear;
	*/

	// class OgreRenderableDrawer : public OgreSimpleRenderable,private Drawer { };	// TODO : runtime drawing using hardware buffers
	// class OgreMeshDrawer : public OgreSimpleRenderable,private Drawer { }; 		// TODO : generate ogre-mesh
	// primitivegroups (not renderable in one flow): capped cylinders, grids, automatic LOD decorator
		// use one common hardwarebuffer -> still faster than simply combining primitives directly

	// drawer decorators:
	//	texturecoordinate transformer/animator
	//	texturecoordinate assignment (cylindrical/plane/sphere wrapping)
	//	vertex deformer (spacewarp) , jitter,twist&bend&scale,mirror,... animation
	//	colormapper (based on TexCoords,VertexCoords or normals)


	/*
		class Prism : Loft {};
		class Cylinder : Prism {
			Cylinder(Real rad,Real height,size_t height_segments=1,size_t round_segments=16,bool hasNormals=true,bool hasTexCoords=true);	
		};
		// maybe even csg ? (constructive solid geomety : intersect,difference,merge) ... probably not
		look at 
		#include <OgreSimpleSpline.h>
		#include <OgreRotationalSpline.h>
	*/
	
	
	// Triangles
	// TriangleStrip
	// TriangleFan
	// Quads				Triangles    	see Ogre::RenderOperation,Renderable
	// QuadStrip			TriangleStrip	see Ogre::RenderOperation,Renderable
	// Polygon				TriangleFan if convex ? .. filling currently not possible if not convex... (i dunno how)
	// Polygon_Wire			LineStrip

	// Lines
	// LineStrip
	// Spline 				LineStrip
		// (segments, interpolation(coord,uv)(constant,linear,quadric:smooth,quadric:controlpoints), can be "rendered" to buffer, with stride)
		// add "refine" command ("time" as param?) to input vertex in the middle and maybe set controlpoints automatically
		// use as input to various forms (polygon,loft,prism...)
		// add some nice calc functions such as "GetClosestTime(Point,index1,index2)", approximate using segments at first
		// can splines have some kind of normals or quaternions ? -> loft-path, quaternion for pointing z axis along the path

	// Circle				TriangleFan, around the first point (one poly and one vertex less than centerfan)
	// Circle_CenterFan		TriangleFan, around the center point, has nicer lighting ?
	// Circle_Wire			LineStrip, (or Spline with 4(or even 2, normal-bug) points ???)

	// Rect					QuadStrip
	// Rect_Wire			LineStrip
	// Rect_FastWire		Lines, no vertex at line crossing => fewer lines

	// Ellipsoid			Loft (cool for partial sphere, pacman) , Sphere
	// Ellipsoid_Wire		Loft
	// Box					Loft (collapsed on lower left edge) , needs not be orthogonal
	// Box_Wire				Lines ? LineStrip ? Loft-in-wire-mode ? Box_FastWire alone should be enough
	// Box_FastWire			Lines, no vertex at line crossing => fewer lines	
	// Torus				Loft, "donut"

	// Spline				also as possible input for loft, also add "refine" command
	// Nurbs				
	// RoundedCube			Loft
	// Capsule				Loft, (=oil-tank?) : a cylinder with half-spheres at the end
	// Arrow				Loft, 3d and 2d(just less segments)
	// Wall					Loft, extruded line, lofted upwards, spline-based ?
	// Lathe				Loft, (no move, only rotate)
	// Frame				Group<Prism> : generated from mesh (or spline-patch-mesh-thing), also used for spaceships

	// teapot				this one is a must
	// TorusKnot			orge-company-logo : Loft (with control-points if quadric)
	// superellipsoid ??? dunno what that is
	// geosphere : triangle-strip, created by subdividing faces 1 to 4, start with 2 mirrored triangles

	// Loft
		// Prism		(Cone,Cylinder)  		// Arrow,Crystals ?
		// Lathe								// Asteroid ?
			// Ellipsoid	(Sphere)
			// Torus 1 (spinning circle)

	// morping loft : more than one base-shape -> spike-ball, teapot (not quite) 
		
		
#endif

#endif // ROB_PRIMITIVES_H
