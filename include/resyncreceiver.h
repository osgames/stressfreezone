#ifndef RESYNCRECEIVER_H
#define RESYNCRECEIVER_H

#include "lugre_smartptr.h"
#include <map>

using namespace Lugre;
class lua_State;
class cObject;
class cResyncReceiver;
namespace Lugre {
	class cUDP_ReceiveSocket;
	class cFIFO;
};
	
// global receiver available for convenience
extern cResyncReceiver* gpResyncReceiver;

/// client only, holds an associative map of objects and receives resyncs in c++ for efficiency
class cResyncReceiver : public cSmartPointable { public:
	std::map<int,cObject*>		mlObjects;
	
	cResyncReceiver				();
	virtual ~cResyncReceiver	();
	
	void	AddObject				(const int iID,cObject* pObj) { mlObjects[iID] = pObj; }
	void	RemoveObject			(const int iID) { mlObjects.erase(iID); }
	int		ReceiveResyncs			(cUDP_ReceiveSocket& pUDPSocket,const uint32 iServerAddr);
	void	ReceiveResyncsFromFIFO	(cFIFO& pFIFO);
	
	// lua binding
	static void		LuaRegister 	(lua_State *L);
};

#endif
