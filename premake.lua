project.name = "StreeFreeZone"
-- project.path = "build"
-- project.bindir = "."

function addpkgconfiglib (package, libname)
    if options.target == "gnu" and os.execute("pkg-config --exists "..libname) == 0 then
      tinsert (package.buildoptions, "`pkg-config --cflags "..libname.."`")
      tinsert (package.linkoptions, "`pkg-config --libs "..libname.."`")
    else
      tinsert (package.linkoptions, findlib (libname))
    end
  end

function addcustomconfiglib (package, configcommand)
    if options.target == "gnu" and os.execute(configcommand.." --version") == 0 then
      tinsert (package.buildoptions, "`"..configcommand.." --cflags`")
      tinsert (package.linkoptions, "`"..configcommand.." --libs`")
    else
      -- TODO tinsert (package.linkoptions, findlib (libname))
    end
  end

function addcustomlib (package, libname)
	local path = os.findlib(libname)
	
    if path then
      tinsert (package.libpaths,path)
      tinsert (package.links, libname)
    end
  end

package = newpackage()
package.name = "sfz"
package.kind = "exe"
package.language = "c++"

package.buildflags = { "extra-warnings", "optimize" }
package.buildoptions = {}

package.includepaths = { "lugre/include", "include" }

package.libpaths = {}
package.linkoptions = {}
package.links = {}

package.defines = { 
	"USE_OPENAL",
	"MAIN_WORKING_DIR=\\\".\\\"",
	"LUA_DIR=\\\"lua\\\"",
	"LUGRE_DIR=\\\"lugre\\\"",
	"DATA_DIR=\\\"data\\\"",
	"ENABLE_ODE",
	"ENABLE_THREADS",
}

addpkgconfiglib(package, "OGRE")
addpkgconfiglib(package, "OIS")
addpkgconfiglib(package, "lua50")
addpkgconfiglib(package, "lualib50")
addpkgconfiglib(package, "openal")
addpkgconfiglib(package, "vorbisfile")

addcustomconfiglib(package, "ode-config")

addcustomlib(package,"boost_thread")
addcustomlib(package,"boost_thread-mt")

package.files = {
  matchrecursive("lugre/include/*.h", "lugre/src/*.cpp"),
  matchrecursive("include/*.h", "src/*.cpp"),
}
package.excludes = {
--  "dont_build_this.c"
}

