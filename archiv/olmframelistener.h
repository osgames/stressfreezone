#if 0
#ifndef __OlmFrameListener_H__
#define __OlmFrameListener_H__

#include <Ogre.h>
#include "OgreKeyEvent.h"
#include "OgreEventListeners.h"
#include "OgreStringConverter.h"
#include "OgreException.h"

using namespace Ogre;

class GuiFrameListener : public FrameListener, public KeyListener, public MouseMotionListener, public MouseListener {
public:
    GuiFrameListener(RenderWindow* win, Camera* cam, CEGUI::Renderer* renderer);
    virtual ~GuiFrameListener();
    void requestShutdown(void);

    virtual bool processUnbufferedKeyInput(const FrameEvent& evt);
    bool processUnbufferedMouseInput(const FrameEvent& evt);
	void moveCamera();
    void showDebugOverlay(bool show);
	void switchMouseMode();
	void switchKeyMode();


    bool frameStarted	(const FrameEvent& evt);
    bool frameEnded		(const FrameEvent& evt);
	
    void mouseMoved		(MouseEvent* e);
    void mouseDragged	(MouseEvent* e);
    void mousePressed	(MouseEvent* e);
    void mouseReleased	(MouseEvent* e);
	void mouseClicked	(MouseEvent* e);
	void mouseEntered	(MouseEvent* e);
	void mouseExited	(MouseEvent* e);
	
    void keyPressed		(KeyEvent* e);
	void keyReleased	(KeyEvent* e);
	void keyClicked		(KeyEvent* e);
	
protected:
	int mSceneDetailIndex ;
    Real mMoveSpeed;
    Degree mRotateSpeed;
    Overlay* mDebugOverlay;
    CEGUI::Renderer* mGUIRenderer;
    bool mShutdownRequested;


    EventProcessor* mEventProcessor;
    InputReader* mInputDevice;
    Camera* mCamera;

    Vector3 mTranslateVector;
    RenderWindow* mWindow;
    bool mStatsOn;
    bool mUseBufferedInputKeys, mUseBufferedInputMouse, mInputTypeSwitchingOn;
	unsigned int mNumScreenShots;
    float mMoveScale;
    Degree mRotScale;
    // just to stop toggles flipping too fast
    Real mTimeUntilNextToggle ;
    Radian mRotX, mRotY;
    TextureFilterOptions mFiltering;
    int mAniso;

    void updateStats(void);
};

#endif
#endif
