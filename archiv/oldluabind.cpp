
// was in object_L.cpp, obsoleted by location system, hasn't even been used before, as lua keeps its own object lists with additional data

/// returns list : objID1, objID2, ...
/// objid-list	GetAllObjs		()
static int		GetAllObjs	(lua_State *L) { PROFILE
	int nres = 0;
	// todo : adjust for location system
	/*
	cObject* obj;
	cGame& game = cGame::GetSingleton();
	for (std::map<size_t,cObject*>::iterator itor=game.mlObject.begin();itor!=game.mlObject.end();++itor) {
		obj = (*itor).second;
		if (!obj || obj->mbDead) continue;
		lua_pushnumber(L,obj->miID);
		luaL_checkstack(L, 1, "too many results");
		++nres;
	}
	*/
	return nres;
}