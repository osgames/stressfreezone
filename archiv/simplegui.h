#ifndef _SIMPLEGUI_H_
#define _SIMPLEGUI_H_

#include <OgreCEGUIRenderer.h>

#include <CEGUI/CEGUIImageset.h>
#include <CEGUI/CEGUISystem.h>
#include <CEGUI/CEGUILogger.h>
#include <CEGUI/CEGUISchemeManager.h>
#include <CEGUI/CEGUIWindowManager.h>
#include <CEGUI/CEGUIWindow.h>
#include <CEGUI/CEGUIXMLParser.h>
#include <CEGUI/CEGUIXMLHandler.h>

#include "tinyxml.h"

#include <string>
#include <list>
#include <map>

class SimpleGuiNode;

class cSimpleGui {
private:
		cSimpleGui();
		~cSimpleGui();
public:

		inline static cSimpleGui& GetSingleton () { 
				static cSimpleGui* mSingleton = 0;
				if (!mSingleton) mSingleton = new cSimpleGui();
				return *mSingleton;
		};
		
		//this needs to be set after creating the windowmanager and parent or u will never see anything
		void SetWndManagerAndParent(CEGUI::WindowManager *WndMng, CEGUI::Window *Parent);
		
		///generates a cegui + lua handling of buttons and things like this from an xml string
		///returns the handle of the windowtree, usable by lua
		unsigned int Parse(const char *sXml);
		
		///cegui gettext method, returns 0 on error
		const char *GetText(unsigned int iHandle,const char *sId);
		///cegui settext method
		void SetText(unsigned int iHandle,const char *sId,const char *sText);
		///close a window tree
		void CloseWindow(unsigned int iHandle);
		
		///sets the state of a checkbox
		void SetCheckboxSelected(unsigned int iHandle,const char *sId,bool bSelected);
		///get the state of a checkbox, false on error, works only on checkboxes
		bool GetCheckboxSelected(unsigned int iHandle,const char *sId);
		
		
		//CEGUI::Window *ParseNode(CEGUI::Window *ParentWindow, TiXmlNode *Node,
		//		unsigned int iX,unsigned int iY,unsigned int iW,unsigned int iH,unsigned int iHandle);
		CEGUI::Window *BuildWindow(CEGUI::Window *ParentWindow, SimpleGuiNode *Node,
				float iX,float iY,float iW,float iH,unsigned int iHandle);

		unsigned int GetFreeHandle();
		
		bool handleClick(const CEGUI::EventArgs& e);
		bool handleClose(const CEGUI::EventArgs& e);
		bool handleSized(const CEGUI::EventArgs& e);

		///returns the window with the given id (in a iHandle window tree) or 0 on error
		CEGUI::Window *GetWindow(unsigned int iHandle,const std::string &sId);
		///returns the base window window with the given handle
		CEGUI::Window *GetBaseWindow(unsigned int iHandle);
		///returns the id of the given window or 0 on error
		const std::string &GetId(CEGUI::Window *Window);
		///returns the handle of the windowtree this window belongs to
		unsigned int GetHandle(CEGUI::Window *Window);

		void SetId(unsigned int iHandle,const char *sId,CEGUI::Window *Window);
		
		std::map<CEGUI::Window *,std::string> mlWindowId;
		std::map<unsigned int, std::map<std::string,CEGUI::Window *> > mlHandleIdWindow;
		///stores handles for cegui base windows
		std::map<unsigned int,CEGUI::Window *> mlHandleWindow;
		///and vice versa, but also stores the same handle for child buttons to easily get the parent handle of a button
		std::map<CEGUI::Window *,unsigned int> mlWindowHandle;
		unsigned int miNextHandle;
		
		std::map<unsigned int, SimpleGuiNode *> mlHandleSimpleGuiNode;
		
private:
		SimpleGuiNode *mpSimpleGuiRootNode;
		CEGUI::WindowManager *mpWndMng;
		CEGUI::Window *mpParent;
};


#endif
