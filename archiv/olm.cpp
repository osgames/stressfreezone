#if 0
#include <CEGUI/CEGUIImageset.h>
#include <CEGUI/CEGUISystem.h>
#include <CEGUI/CEGUILogger.h>
#include <CEGUI/CEGUISchemeManager.h>
#include <CEGUI/CEGUIWindowManager.h>
#include <CEGUI/CEGUIWindow.h>
#include <CEGUI/CEGUIXMLParser.h>
#include <CEGUI/CEGUIXMLHandler.h>

#include <CEGUI/elements/CEGUICombobox.h>
#include <CEGUI/elements/CEGUIListbox.h>
#include <CEGUI/elements/CEGUIListboxTextItem.h>
#include <CEGUI/elements/CEGUIPushButton.h>
#include <CEGUI/elements/CEGUIScrollbar.h>
#include <CEGUI/elements/CEGUIStaticImage.h>

#include "olmframelistener.h"
#include "DynamicLines.h"
#include "tinyxml.h"

#include <Ogre.h>
#include <OgreKeyEvent.h>
#include <OgreEventListeners.h>
#include <OgreStringConverter.h>
#include <OgreException.h>
#include <OgreConfigFile.h>

#include <OgreCEGUIRenderer.h>
#include <OgreCEGUIResourceProvider.h>

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#include <SDL/SDL.h>

#include <openalpp/alpp.h>

#include "net.h"
#include "smartptr.h"
#include "fifo.h"
#include "robstring1.2.h"

// see curtest.cpp
void	CurTest		(SceneManager* mSceneMgr);


using namespace Ogre;

/*
CEGUI :
Additional Include Directories = $(OGRE_HOME)\OgreMain\Dependencies\include $(OGRE_HOME)\OgreMain\Dependencies\include\CEGUI
Additional Library Path        = $(OGRE_HOME)\OgreMain\Dependencies\Lib\Debug
*/


extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

#include "lunar.h"

class Account {
  lua_Number m_balance;
public:
  static const char className[];
  static Lunar<Account>::RegType methods[];

  Account(lua_State *L)      { m_balance = luaL_checknumber(L, 1); }
  int deposit (lua_State *L) { m_balance += luaL_checknumber(L, 1); return 0; }
  int withdraw(lua_State *L) { m_balance -= luaL_checknumber(L, 1); return 0; }
  int balance (lua_State *L) { lua_pushnumber(L, m_balance); return 1; }
  ~Account() { printf("deleted Account (%p)\n", this); }
};

const char Account::className[] = "Account";


#define method(class, name) {#name, &class::name}

Lunar<Account>::RegType Account::methods[] = {
  method(Account, deposit),
  method(Account, withdraw),
  method(Account, balance),
  {0,0}
};


/*
dynamic lines
In initialization somewhere, create the initial lines object:

      DynamicLines *lines = new DynamicLines(RenderOperation::OT_LINE_LIST);
      for (i=0; i<somePoints.size(); i++) {
        lines->addPoint(somePoints[i]);
      }
      lines->update();
      SceneNode *linesNode = mScene->getRootSceneNode()->createChildSceneNode("lines");
      linesNode->attachObject(lines);
 

Then later on when you want to update the lines:

    SceneNode *lnode = dynamic_cast<SceneNode*>(mScene->getRootSceneNode()->getChild("lines"));
    DynamicLines *lines = dynamic_cast<DynamicLines*>(lnode->getAttachedObject(0));

    if (lines->getNumPoints()!= myPoints.size()) {
      // Oh no!  Size changed, just recreate the list from scratch
      lines->clear();
      for (int i=0; i<myPoints.size(); ++i) {
        lines->addPoint(myPoints[i]);
      }
    }
    else {
      // Just values have changed, use 'setPoint' instead of 'addPoint'
      for (int i=0; i<myPoints.size(); ++i) {
        lines->setPoint(i,cvt(ppos[c->m_p1]));
      }
    }
    lines->update();

The inherited mBox member is used to decide whether or not the object is in view and therefore should be drawn. It is not supposed to be dynamic, so you must take an extra step to get Ogre to notice whatever changes were made to it.

You will want to keep a pointer to the scene node you attached the DynamicLines instance to, and call the following whenever mBox changes :

    linesNode->needUpdate();


*/


void createSphere(const std::string& strName, const float r, const int nRings = 16, const int nSegments = 16) {
	MeshPtr pSphere = MeshManager::getSingleton().createManual(strName, ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	SubMesh *pSphereVertex = pSphere->createSubMesh();

	pSphere->sharedVertexData = new VertexData();
	VertexData* vertexData = pSphere->sharedVertexData;

	// define the vertex format
	VertexDeclaration* vertexDecl = vertexData->vertexDeclaration;
	size_t currOffset = 0;
	// positions
	vertexDecl->addElement(0, currOffset, VET_FLOAT3, VES_POSITION);
	currOffset += VertexElement::getTypeSize(VET_FLOAT3);
	// normals
	vertexDecl->addElement(0, currOffset, VET_FLOAT3, VES_NORMAL);
	currOffset += VertexElement::getTypeSize(VET_FLOAT3);
	// two dimensional texture coordinates
	vertexDecl->addElement(0, currOffset, VET_FLOAT2, VES_TEXTURE_COORDINATES, 0);
	currOffset += VertexElement::getTypeSize(VET_FLOAT2);

	// allocate the vertex buffer
	vertexData->vertexCount = (nRings + 1) * (nSegments+1);
	HardwareVertexBufferSharedPtr vBuf = HardwareBufferManager::getSingleton().createVertexBuffer(vertexDecl->getVertexSize(0), vertexData->vertexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY, false);
	VertexBufferBinding* binding = vertexData->vertexBufferBinding;
	binding->setBinding(0, vBuf);
	float* pVertex = static_cast<float*>(vBuf->lock(HardwareBuffer::HBL_DISCARD));

	// allocate index buffer
	pSphereVertex->indexData->indexCount = 6 * nRings * (nSegments + 1);
	pSphereVertex->indexData->indexBuffer = HardwareBufferManager::getSingleton().createIndexBuffer(HardwareIndexBuffer::IT_16BIT, pSphereVertex->indexData->indexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY, false);
	HardwareIndexBufferSharedPtr iBuf = pSphereVertex->indexData->indexBuffer;
	unsigned short* pIndices = static_cast<unsigned short*>(iBuf->lock(HardwareBuffer::HBL_DISCARD));

	float fDeltaRingAngle = (Math::PI / nRings);
	float fDeltaSegAngle = (2 * Math::PI / nSegments);
	unsigned short wVerticeIndex = 0 ;

	// Generate the group of rings for the sphere
	for( int ring = 0; ring <= nRings; ring++ ) {
		float r0 = r * sinf (ring * fDeltaRingAngle);
		float y0 = r * cosf (ring * fDeltaRingAngle);

		// Generate the group of segments for the current ring
		for(int seg = 0; seg <= nSegments; seg++) {
			float x0 = r0 * sinf(seg * fDeltaSegAngle);
			float z0 = r0 * cosf(seg * fDeltaSegAngle);

			// Add one vertex to the strip which makes up the sphere
			*pVertex++ = x0;
			*pVertex++ = y0;
			*pVertex++ = z0;

			Vector3 vNormal = Vector3(x0, y0, z0).normalisedCopy();
			*pVertex++ = vNormal.x;
			*pVertex++ = vNormal.y;
			*pVertex++ = vNormal.z;

			*pVertex++ = (float) seg / (float) nSegments;
			*pVertex++ = (float) ring / (float) nRings;

			if (ring != nRings) {
				// each vertex (except the last) has six indices pointing to it
				*pIndices++ = wVerticeIndex + nSegments + 1;
				*pIndices++ = wVerticeIndex;               
				*pIndices++ = wVerticeIndex + nSegments;
				*pIndices++ = wVerticeIndex + nSegments + 1;
				*pIndices++ = wVerticeIndex + 1;
				*pIndices++ = wVerticeIndex;
				wVerticeIndex ++;
			}
		}; // end for seg
	} // end for ring

	// Unlock
	vBuf->unlock();
	iBuf->unlock();
	// Generate face list
	pSphereVertex->useSharedVertices = true;

	// the original code was missing this line:
	pSphere->_setBounds( AxisAlignedBox( Vector3(-r, -r, -r), Vector3(r, r, r) ), false );
	pSphere->_setBoundingSphereRadius(r);
	// this line makes clear the mesh is loaded (avoids memory leaks)
	pSphere->load();
}

class MyApplication {
private:
    CEGUI::OgreCEGUIRenderer* mGUIRenderer;
    CEGUI::System* mGUISystem;
    CEGUI::Window* mEditorGuiSheet;
	CEGUI::Scrollbar* mRed;
	CEGUI::Scrollbar* mGreen;
	CEGUI::Scrollbar* mBlue;
	CEGUI::StaticImage* mPreview;
	CEGUI::Window* mTip;
	CEGUI::Listbox* mList;
	CEGUI::Window* mEditBox;
	typedef std::map<CEGUI::String, CEGUI::String> DescriptionMap;
	DescriptionMap mDescriptionMap;

public:
    /// Standard constructor
    MyApplication()
      : mGUIRenderer(0),
        mGUISystem(0),
        mEditorGuiSheet(0)
    {
        mFrameListener = 0;
        mRoot = 0;
		mDescriptionMap[(CEGUI::utf8*)"Demo8"] = (CEGUI::utf8*)"The main containing panel";
	}
	
    /// Standard destructor
    ~MyApplication() {
        if (mEditorGuiSheet) {
            CEGUI::WindowManager::getSingleton().destroyWindow(mEditorGuiSheet);
        }
        if (mGUISystem) {
            delete mGUISystem;
            mGUISystem = 0;
        }
        if(mGUIRenderer) {
            delete mGUIRenderer;
            mGUIRenderer = 0;
        }
        if (mFrameListener)
            delete mFrameListener;
        if (mRoot)
            delete mRoot;
    }
	
    /// Start the example
    virtual void go(void) {
		
		if (1) {
			{
				//openal test
				openalpp::ref_ptr<openalpp::Source> bg = new openalpp::Source();
				openalpp::ref_ptr<openalpp::FileStream> stream_bg = new openalpp::FileStream("data/ship.ogg");
				openalpp::ref_ptr<openalpp::Listener> listener = new openalpp::Listener(0,0,0);
				openalpp::ref_ptr<openalpp::Source> pling = new openalpp::Source("data/pling.wav");

				pling->stop();
				bg->stop();
				
				bg->setSound(stream_bg.get());
	//  			bg->setSound("data/ambient.wav");
				bg->setAmbient(false);
				bg->setPosition(0.0f,0.0f,0.0f);
				bg->play();

				pling->setAmbient();
				pling->play();
	
				float x,y,z;
				while(bg->getState() == openalpp::Playing){
					bg->getPosition(x,y,z);
					x += 1.0f;
					bg->setPosition(x,y,z);
					sleep(1);
				}

				bg->stop();
				pling->stop();

				printf("finished\n");
			}
			
			exit(0);
		}
		if (0) {
			int a = 4;
			int b = 8;
			
			int& ref = a;
			//&ref = &b;
			
			printf("a=%d,b=%d,ref=%d\n",a,b,ref);
			exit(0);
		}
		
		if (0) {
			// TEST for local cConnection and cMessageWriter,cMessageReader and cBroadcast
			cConnection		mycon1,mycon2;
			cBroadcast		myBroadcast;
			cConnection*	myCons[] = {&mycon1,&mycon2};
			const size_t kMsgType_Welcome = 1;
			const size_t kMsgType_Announcement = 2;
			const size_t kMsgType_Explosion = 3;
			
			
			cMessageWriter myWriter; 
			for (size_t i=0;i<2;++i) {
				printf("player joined\n");
				cConnection* myPlayer = new cConnection(myCons[i]);
				
				// welcome message to new player
				myWriter.Open(myPlayer->mpOutBuffer,kMsgType_Welcome);
				myWriter.Push(strprintf("Welcome Player %d",i));
				myWriter.Close(); 
				
				// inform all other players of the newcomer
				myWriter.Open(&myBroadcast.mOutBuffer,kMsgType_Announcement);
				myWriter.Push(strprintf("Player %d joined",i));
				myWriter.Close(); 
				
				// send messages to new player
				cNet::GetSingleton().Step();
				
				// from now on, the newcomer gets all messages
				myBroadcast.mlCons.insert(myPlayer);
			}
			
		
			// send some data
			myWriter.Open(&myBroadcast.mOutBuffer,kMsgType_Explosion);
			myWriter.Push(Vector3(1.0,2.0,3.0));
			myWriter.Next(kMsgType_Explosion);
			myWriter.Push(Vector3(4.0,5.0,6.0));
			myWriter.Next(kMsgType_Explosion);
			myWriter.Push(Vector3(7.0,8.0,9.0));
			myWriter.Close(); 
			
			cNet::GetSingleton().Step();
			
			// players read messages
			cMessageReader myReader;
			for (size_t i=0;i<2;++i) {
				while (myReader.Next(myCons[i]->mpInBuffer)) { 
					printf("#%d ",i);
					//printf("player %d : incoming message, type=%d, len=%d\n",i,myReader.miType,myReader.miLen);
					switch (myReader.miType) {
						case kMsgType_Welcome: {
							std::string res = myReader.PopS();
							printf("Welcome(%s)\n",res.c_str());
						} break;
						case kMsgType_Announcement: {
							std::string res = myReader.PopS();
							printf("Announcement(%s)\n",res.c_str());
						} break;
						case kMsgType_Explosion: {
							Vector3 x = myReader.PopV(); 
							printf("Explosion(%0.3f,%0.3f,%0.3f)\n",x.x,x.y,x.z);
						} break;
					}
				}
				myReader.Close();
			}
			
			/* output:
			player joined
			player joined
			#0 Welcome(Welcome Player 0)
			#0 Announcement(Player 1 joined)
			#0 Explosion(1.000,2.000,3.000)
			#0 Explosion(4.000,5.000,6.000)
			#0 Explosion(7.000,8.000,9.000)
			#1 Welcome(Welcome Player 1)
			#1 Explosion(1.000,2.000,3.000)
			#1 Explosion(4.000,5.000,6.000)
			#1 Explosion(7.000,8.000,9.000)
			*/
			
			exit(0);
		}
		if (0) {/*
			size_t port = 1024+66;
			cNetListener mylistener(port);
			cConnection mycon("127.0.0.1",port);
			
			cNet::GetSingleton().Step();
			
			cConnection* myPlayer = mylistener.PopAccepted();
			if (myPlayer) {
				printf("player joined\n");
				myPlayer->mpOutBuffer->Push(Vector3(1,2,3));
			}
			
			// call this twice if using local connections, needs to recheck for read availability after writing
			cNet::GetSingleton().Step();
			cNet::GetSingleton().Step();
			
			printf("incoming data : %d\n",mycon.mpInBuffer->size());
			if (mycon.mpInBuffer->size() > 0) {
				Vector3 x;
				mycon.mpInBuffer->Pop(x);
				printf("received vector3 (%0.3f,%0.3f,%0.3f)\n",x.x,x.y,x.z);
			}
			
			exit(0);*/
		}
		
		if (0) {
			cFIFO fifo;
			fifo.Push(2); 
			fifo.Push(-4);
			fifo.PushU(6);
			fifo.PushF(1.2345);
			fifo.Push(Vector3(1,2,3));
			fifo.Push(Quaternion(1,2,3,4));
			fifo.Push(std::string("Hallo welt!")); 
			
			{ int x; fifo.Pop(x); printf("%d\n",x); }
			{ int x; fifo.Pop(x); printf("%d\n",x); }
			{ size_t x; fifo.PopU(x); printf("%d\n",x); }
			{ Real x; fifo.PopF(x); printf("%0.3f\n",x); }
			{ Vector3 x; fifo.Pop(x); printf("(%0.3f,%0.3f,%0.3f)\n",x.x,x.y,x.z); }
			{ Quaternion x; fifo.Pop(x); printf("(%0.3f,%0.3f,%0.3f,%0.3f)\n",x.w,x.x,x.y,x.z); }
			{ std::string x; fifo.Pop(x); printf("(%s)\n",x.c_str()); }
			
			// { int x; fifo.Pop(x); printf("underrunner\n",x); }
			
			exit(0);
		}
		
		if (0) {
			lua_State *L = lua_open();
			
			luaopen_base(L);
			luaopen_table(L);
			luaopen_io(L);
			luaopen_string(L);
			luaopen_math(L);
			luaopen_debug(L);
			
			Lunar<Account>::Register(L);
			
			lua_dofile(L,"data/luatest.lua");
			
			lua_setgcthreshold(L, 0);  // collected garbage
			lua_close(L);
			exit(0);
		}
	
		if (0) {
			TiXmlDocument doc( "data/sample.xml" );
			doc.LoadFile();
			TiXmlHandle docHandle( &doc );
			TiXmlElement * element = docHandle.FirstChild( "TEST" ).Child( "gumba", 0 ).Element();
			if (element) {
				std::cerr << element->Attribute( "testattribute" ) << "\n";
			} else {
				std::cerr << "gumba not found" << "\n";
			}
			// do something useful
			/*
			child = NULL;
			while ( ( child = parent->IterateChildren( child ) ) )
			{
				//TiXmlElement * e = child.ToElement();
				//const char * nchar = e->Attribute( "solid" );
				//const char * mchar = e->Attribute( "testattribute" );
			}	*/
			exit(0);

			/*
			cegui_mk2/include/CEGUIXMLHandler.h
			
			// tinyxml : 
			// TiXmlBase::SetCondenseWhiteSpace(false) // default was true
			TiXmlHandle docHandle( &document );
			TiXmlElement* child2 = docHandle.FirstChild( "Document" ).FirstChild( "Element" ).Child( "Child", 1 ).Element();
			if ( child2 ) ;
				// do something useful
			
			debug xml : The TiXmlBase::Row() and TiXmlBase::Column() methods return the origin of the node in the source text
			
			Add tinyxml.cpp, tinyxml.h, tinyxmlerror.cpp, tinyxmlparser.cpp, tinystr.cpp, tinystr.h
			
			# the Print() and SaveFile() methods
			# the Parse() and LoadFile() methods
			*/
		}

        if (!setup())
            return;

		if (0) {
			CEGUI::XMLParser* parser = CEGUI::System::getSingleton().getXMLParser();
			if (!CEGUI::System::getSingleton().getXMLParser()) {
				std::cerr << "NO XML PARSER FOUND";
				exit(1);
			}


			class myXMLHandler : public CEGUI::XMLHandler {
				public:
				myXMLHandler () : CEGUI::XMLHandler() {}
				virtual ~myXMLHandler() {}	
				virtual void elementStart(const CEGUI::String& element, const CEGUI::XMLAttributes& attributes) {
					std::cerr << "[start]" << element << "\n";
				}
				virtual void elementEnd(const CEGUI::String& element) {
					std::cerr << "[end]" << element << "\n";
				}
			};
			CEGUI::XMLHandler *bla = new myXMLHandler();
			CEGUI::System::getSingleton().getXMLParser()->parseXMLFile(*bla,"sample.xml","","");
			exit(0);
		}
		
		/*
		SDL_Event event;
		
		while (1) {
			while (SDL_PollEvent(&event) >= 1)
			switch (event.type) {
			
			}		
			mRoot->renderOneFrame();
		}
		*/
		
		mRoot->startRendering();
        /*while (1) { 
			//mFrameListener->
		}*/
    }
	
	


protected:
    Root *mRoot;
    Camera* mCamera;
    SceneManager* mSceneMgr;
    GuiFrameListener* mFrameListener;
    RenderWindow* mWindow;

    // These internal methods package up the stages in the startup process
    /** Sets up the application - returns false if the user chooses to abandon configuration. */
    virtual bool setup(void) {
		
	// TiXmlDocument doc( "models.xml" );
		
		mRoot = new Root();
		if (0)  {
			mRoot = new Root("plugins.cfg","display.cfg","log.txt");
			RenderSystemList *rsList = mRoot->getAvailableRenderers();
			int c=0;
			bool foundit = false;
			RenderSystem *selectedRenderSystem=0;
			while(c < (int) rsList->size()) {
				selectedRenderSystem = rsList->at(c);
				String rname = selectedRenderSystem->getName();
				if(rname.compare("OpenGL Rendering SubSystem")==0){
					foundit=true;
					break;
				}
				c++;
			}
			if(!foundit) return 0; 
			mRoot->setRenderSystem(selectedRenderSystem);
			
			selectedRenderSystem->setConfigOption("Full Screen","No");
			selectedRenderSystem->setConfigOption("Video Mode","640 x 480 @ 32-bit colour");
			
				
			//retrieve the config option map
			ConfigOptionMap comap = selectedRenderSystem->getConfigOptions();
			
			//and now we need to run through all of it
			ConfigOptionMap::const_iterator start = comap.begin();
			ConfigOptionMap::const_iterator end = comap.end();
			while(start != end){
				String OptionName = start->first;
				String CurrentValue = start->second.currentValue;
				StringVector PossibleValues = start->second.possibleValues;
				int c=0;
				while (c < (int) PossibleValues.size()){
					String OneValue = PossibleValues.at(c);
					c++;
				}
				start++;
			}
		
		
			RenderWindow* mWindow;
			mWindow = mRoot->initialise(true, "Some Window Title");
			
			if (0) { // alternate
				// initialise root
				mRoot->initialise(false);
				// create main window
				RenderWindow *renderWindow = mRoot->createRenderWindow("Main",320,240,false);
			}
				
			SceneManager* mSceneMgr;
			mSceneMgr = mRoot->getSceneManager(ST_GENERIC);
			
			Camera* mCamera;
			mCamera = mSceneMgr->createCamera("PlayerCam");
			mCamera->setPosition(Vector3(0,0,500));
			mCamera->lookAt(Vector3(0,0,-300));
			mCamera->setNearClipDistance(5);

			// Create one viewport, entire window
			Viewport* vp = mWindow->addViewport(mCamera);
			vp->setBackgroundColour(ColourValue(0,0,0));
			
			// Alter the camera aspect ratio to match the viewport
			mCamera->setAspectRatio( Real(vp->getActualWidth()) / Real(vp->getActualHeight()) );


		}

        setupResources();

        bool carryOn = configure();
        if (!carryOn) return false;

        chooseSceneManager();
        createCamera();
        createViewports();

        // Set default mipmap level (NB some APIs ignore this)
        TextureManager::getSingleton().setDefaultNumMipmaps(5);

		// Create any resource listeners (for loading screens)
		createResourceListener();
		// Load resources
		loadResources();

		// Create the scene
        createScene();

        createFrameListener();

        return true;

    }

    virtual void chooseSceneManager(void) {
        // Get the SceneManager, in this case a generic one
        mSceneMgr = mRoot->getSceneManager(ST_GENERIC);
    }
    virtual void createCamera(void) {
        // Create the camera
        mCamera = mSceneMgr->createCamera("PlayerCam");

        // Position it at 500 in Z direction
        mCamera->setPosition(Vector3(0,0,500));
        // Look back along -Z
        mCamera->lookAt(Vector3(0,0,-300));
        mCamera->setNearClipDistance(5);

    }
	


    virtual void createViewports(void) {
        // Create one viewport, entire window
        Viewport* vp = mWindow->addViewport(mCamera);
        vp->setBackgroundColour(ColourValue(0,0,0));

        // Alter the camera aspect ratio to match the viewport
        mCamera->setAspectRatio(
            Real(vp->getActualWidth()) / Real(vp->getActualHeight()));
    }

    /// Method which will define the source of resources (other than current folder)
    virtual void setupResources(void) {
        // Load resource paths from config file
        ConfigFile cf;
        cf.load("resources.cfg");

        // Go through all sections & settings in the file
        ConfigFile::SectionIterator seci = cf.getSectionIterator();

        String secName, typeName, archName;
        while (seci.hasMoreElements())
        {
            secName = seci.peekNextKey();
            ConfigFile::SettingsMultiMap *settings = seci.getNext();
            ConfigFile::SettingsMultiMap::iterator i;
            for (i = settings->begin(); i != settings->end(); ++i)
            {
                typeName = i->first;
                archName = i->second;
                ResourceGroupManager::getSingleton().addResourceLocation(
                    archName, typeName, secName);
            }
        }
    }

	/// Optional override method where you can create resource listeners (e.g. for loading screens)
	virtual void createResourceListener(void) { }

	/// Optional override method where you can perform resource group loading
	/// Must at least do ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	virtual void loadResources(void) {
		// Initialise, parse scripts etc
		ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	}
	
	
	
    // Just override the mandatory create scene method
	
	bool configure(void) {
		if (mRoot->restoreConfig()) {
			mWindow = mRoot->initialise(true);
			return true;
		}
		if(mRoot->showConfigDialog()) {
            // If returned true, user clicked OK so initialise
            // Here we choose to let the system create a default rendering window by passing 'true'
            mWindow = mRoot->initialise(true);
            return true;
        } else {
            return false;
        }
	}
	
	
    void createScene(void) {       
		// Set ambient light
		mSceneMgr->setAmbientLight(ColourValue(0.5, 0.5, 0.5));
		
		// sky
		//mSceneMgr->setSkyDome( true, "Examples/CloudySky", 5, 8 );
		mSceneMgr->setSkyBox( true, "GhoulSkyBox1" );
		// mSceneMgr->setSkyBox( true, "Examples/SpaceSkyBox" );
		// mSceneMgr->setSkyBox( true, "Examples/SpaceSkyBox", 10 );
		// mSceneMgr->setSkyBox( true, "Examples/SpaceSkyBox", 5000, false );
		// mSceneMgr->setSkyBox( true, "Examples/SpaceSkyBox", 100, false );
		/*
		Plane plane;
		plane.d = 1000;
		plane.normal = Vector3::NEGATIVE_UNIT_Y;
		mSceneMgr->setSkyPlane( true, plane, "Examples/SpaceSkyPlane", 1500, 75 );
		mSceneMgr->setSkyPlane( true, plane, "Examples/SpaceSkyPlane", 1500, 50, true, 1.5f, 150, 150 );
		mSceneMgr->setSkyPlane( true, plane, "Examples/CloudySky", 1500, 40, true, 1.5f, 150, 150  );
		mSceneMgr->setSkyPlane( true, plane, "Examples/CloudySky", 1500, 40, true, 1.5f, 150, 150  );
		mSceneMgr->setSkyPlane( false, Plane(), "" ); // clear
		//*/
		
        // Create a light
        Light* l = mSceneMgr->createLight("MainLight");
        l->setPosition(200,800,500);
		
		
		// mSceneMgr->setAmbientLight(ColourValue(0.5, 0.5, 0.5));
		
		// fog
		/*
		ColourValue fadeColour( 0.9, 0.9, 0.9 );
		mWindow->getViewport(0)->setBackgroundColour( fadeColour );
		mSceneMgr->setFog( FOG_LINEAR, fadeColour, 0.0, 50, 500 );
		//*/
		
		// terrain
		//mSceneMgr->setWorldGeometry( "terrain.cfg" );
		
		
		// dynamic-lines-test
		if (0) { 
			DynamicLines *lines = new DynamicLines(); // RenderOperation::OT_LINE_LIST 
			lines->addPoint(-100,0,0);
			lines->addPoint(0,0,0);
			lines->addPoint(100,100,0);
			lines->addPoint(200,100,0);
			lines->update();
			SceneNode *linesNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("lines");
			linesNode->attachObject(lines);
		}
		
		CurTest(mSceneMgr);
		
		// setup GUI system
		mGUIRenderer = new CEGUI::OgreCEGUIRenderer(mWindow, Ogre::RENDER_QUEUE_OVERLAY, false, 3000);
		mGUISystem = new CEGUI::System(mGUIRenderer);
		CEGUI::Logger::getSingleton().setLoggingLevel(CEGUI::Informative);
	   
		if (0) {
			//Entity* ogreHead = mSceneMgr->createEntity("Head", "ogrehead.mesh");
			//Entity* ogreHead = mSceneMgr->createEntity("razor", "razor.mesh");
			
			
			createSphere("mySphereMesh", 10, 64, 64);
			Entity* sphereEntity = mSceneMgr->createEntity("mySphereEntity", "mySphereMesh");
			SceneNode* sphereNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
			sphereEntity->setMaterialName("BaseWhiteNoLighting");
			sphereNode->attachObject(sphereEntity);

			
			//createSphere("MySphereMesh",100);
			//Entity* ogreHead = mSceneMgr->createEntity("razor", "razor.mesh");
			
			//SceneNode* headNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
			//headNode->attachObject(ogreHead);
		}
		
		
		
		
		
        // load scheme and set up defaults
		
        CEGUI::SchemeManager::getSingleton().loadScheme((CEGUI::utf8*)"TaharezLook.scheme");
        mGUISystem->setDefaultMouseCursor((CEGUI::utf8*)"TaharezLook", (CEGUI::utf8*)"MouseArrow");
		//CEGUI::MouseCursor::getSingleton().setImage(CEGUI::System::getSingleton().getDefaultMouseCursor());  
        mGUISystem->setDefaultFont((CEGUI::utf8*)"Tahoma-12");
		
		
		/*
		
        CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout((CEGUI::utf8*)"ogregui.layout"); 
        mGUISystem->setGUISheet(sheet);
		mEditorGuiSheet = sheet;
		
        CEGUI::Combobox* objectComboBox = (CEGUI::Combobox*)CEGUI::WindowManager::getSingleton().getWindow("OgreGuiDemo/TabCtrl/Page2/ObjectTypeList");

        CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem((CEGUI::utf8*)"FrameWindow", 0);
        objectComboBox->addItem(item);
        item = new CEGUI::ListboxTextItem((CEGUI::utf8*)"Horizontal Scrollbar", 1);
        objectComboBox->addItem(item);
        item = new CEGUI::ListboxTextItem((CEGUI::utf8*)"Vertical Scrollbar", 2);
        objectComboBox->addItem(item);
        item = new CEGUI::ListboxTextItem((CEGUI::utf8*)"StaticText", 3);
        objectComboBox->addItem(item);
        item = new CEGUI::ListboxTextItem((CEGUI::utf8*)"StaticImage", 4);
        objectComboBox->addItem(item);
        item = new CEGUI::ListboxTextItem((CEGUI::utf8*)"Render to Texture", 5);
        objectComboBox->addItem(item);
		
		*/
		
		mEditorGuiSheet = CEGUI::WindowManager::getSingleton().createWindow((CEGUI::utf8*)"DefaultWindow", (CEGUI::utf8*)"Sheet");  
		mGUISystem->setGUISheet(mEditorGuiSheet);
		
		/*
		mEditorGuiSheet = CEGUI::WindowManager::getSingleton().loadWindowLayout((CEGUI::utf8*)"TutorialGui.xml");
		mGUISystem->setGUISheet(mEditorGuiSheet);
		CEGUI::PushButton* quitButton = (CEGUI::PushButton*)CEGUI::WindowManager::getSingleton().getWindow((CEGUI::utf8*)"Quit");
		//*/
		
		/*
		CEGUI::PushButton* quitButton = (CEGUI::PushButton*)CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", (CEGUI::utf8*)"Quit");
		mEditorGuiSheet->addChildWindow(quitButton);
		quitButton->setPosition(CEGUI::Point(0.35f, 0.45f));
		quitButton->setSize(CEGUI::Size(0.3f, 0.1f));
		quitButton->setText("Quit");
		//*/
		
		CEGUI::PushButton* quitButton = (CEGUI::PushButton*)CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", (CEGUI::utf8*)"Quit");
		mEditorGuiSheet->addChildWindow(quitButton);
		quitButton->setPosition(CEGUI::Point(0.35f, 0.9f));
		quitButton->setSize(CEGUI::Size(0.3f, 0.1f));
		quitButton->setText("Quit");
		
		setupEventHandlers();
    }

    // Create new frame listener
    void createFrameListener(void) {
        mFrameListener= new GuiFrameListener(mWindow, mCamera, mGUIRenderer);
        mRoot->addFrameListener(mFrameListener);
        //mFrameListener->showDebugOverlay(true);
    }
	
	
    void setupEventHandlers(void) {
	   CEGUI::WindowManager& wmgr = CEGUI::WindowManager::getSingleton();
		wmgr.getWindow((CEGUI::utf8*)"Quit")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MyApplication::handleQuit, this));
	/*
		CEGUI::WindowManager& wmgr = CEGUI::WindowManager::getSingleton();
        wmgr.getWindow((CEGUI::utf8*)"OgreGuiDemo/TabCtrl/Page1/QuitButton")
			->subscribeEvent(
				CEGUI::PushButton::EventClicked, 
				CEGUI::Event::Subscriber(&MyApplication::handleQuit, this));
        wmgr.getWindow((CEGUI::utf8*)"OgreGuiDemo/TabCtrl/Page1/NewButton")
			->subscribeEvent(
				CEGUI::PushButton::EventClicked, 
				CEGUI::Event::Subscriber(&MyApplication::handleNew, this));
        wmgr.getWindow((CEGUI::utf8*)"OgreGuiDemo/TabCtrl/Page1/LoadButton")
			->subscribeEvent(
				CEGUI::PushButton::EventClicked, 
				CEGUI::Event::Subscriber(&MyApplication::handleLoad, this));
        wmgr.getWindow((CEGUI::utf8*)"OgreGuiDemo/TabCtrl/Page2/ObjectTypeList")
			->subscribeEvent(
				CEGUI::Combobox::EventListSelectionAccepted, 
				CEGUI::Event::Subscriber(&MyApplication::handleObjectSelection, this));
		*/

    }

	void setupEnterExitEvents(CEGUI::Window* win) {
		win->subscribeEvent(
			CEGUI::Window::EventMouseEnters, 
			CEGUI::Event::Subscriber(&MyApplication::handleMouseEnters, this));
		win->subscribeEvent(
			CEGUI::Window::EventMouseLeaves, 
			CEGUI::Event::Subscriber(&MyApplication::handleMouseLeaves, this));
		for (unsigned int i = 0; i < win->getChildCount(); ++i)
		{
			CEGUI::Window* child = win->getChildAtIdx(i);
			setupEnterExitEvents(child);
		}

	}

	void setupLoadedLayoutHandlers(void) {
		CEGUI::WindowManager& wmgr = CEGUI::WindowManager::getSingleton();
		mRed = static_cast<CEGUI::Scrollbar*>(
			wmgr.getWindow((CEGUI::utf8*)"Demo8/Window1/Controls/Red"));
		mGreen = static_cast<CEGUI::Scrollbar*>(
			wmgr.getWindow((CEGUI::utf8*)"Demo8/Window1/Controls/Green"));
		mBlue = static_cast<CEGUI::Scrollbar*>(
			wmgr.getWindow((CEGUI::utf8*)"Demo8/Window1/Controls/Blue"));
		mPreview = static_cast<CEGUI::StaticImage*>(
			wmgr.getWindow((CEGUI::utf8*)"Demo8/Window1/Controls/ColourSample"));
		mList = static_cast<CEGUI::Listbox*>(
			wmgr.getWindow((CEGUI::utf8*)"Demo8/Window1/Listbox"));
		mEditBox = 
			wmgr.getWindow((CEGUI::utf8*)"Demo8/Window1/Controls/Editbox");
		mTip = 
			wmgr.getWindow((CEGUI::utf8*)"Demo8/Window2/Tips");
	
		mRed->subscribeEvent(
				CEGUI::Scrollbar::EventScrollPositionChanged, 
				CEGUI::Event::Subscriber(&MyApplication::handleColourChanged, this));
		mGreen->subscribeEvent(
			CEGUI::Scrollbar::EventScrollPositionChanged, 
			CEGUI::Event::Subscriber(&MyApplication::handleColourChanged, this));
		mBlue->subscribeEvent(
			CEGUI::Scrollbar::EventScrollPositionChanged, 
			CEGUI::Event::Subscriber(&MyApplication::handleColourChanged, this));

		wmgr.getWindow((CEGUI::utf8*)"Demo8/Window1/Controls/Add")
			->subscribeEvent(
			CEGUI::PushButton::EventClicked, 
			CEGUI::Event::Subscriber(&MyApplication::handleAdd, this));

		CEGUI::Window* root = wmgr.getWindow("Demo8");
		setupEnterExitEvents(root);



	}

    CEGUI::Window* createRttGuiObject(void) {
        static unsigned int rttCounter = 0;
		String guiObjectName = "NewRttImage" + StringConverter::toString(rttCounter);

        CEGUI::Imageset* rttImageSet = 
            CEGUI::ImagesetManager::getSingleton().getImageset(
                (CEGUI::utf8*)"RttImageset");
        CEGUI::StaticImage* si = (CEGUI::StaticImage*)CEGUI::WindowManager::getSingleton().createWindow((CEGUI::utf8*)"TaharezLook/StaticImage", (CEGUI::utf8*)guiObjectName.c_str());
        si->setSize(CEGUI::Size(0.5f, 0.4f));
        si->setImage(&rttImageSet->getImage((CEGUI::utf8*)"RttImage"));

        rttCounter++;

        return si;
    }

    CEGUI::Window* createStaticImageObject(void) {
        static unsigned int siCounter = 0;
        String guiObjectName = "NewStaticImage" + StringConverter::toString(siCounter);

        CEGUI::Imageset* imageSet = 
            CEGUI::ImagesetManager::getSingleton().getImageset(
                (CEGUI::utf8*)"TaharezLook");

        CEGUI::StaticImage* si = (CEGUI::StaticImage*)CEGUI::WindowManager::getSingleton().createWindow((CEGUI::utf8*)"TaharezLook/StaticImage", (CEGUI::utf8*)guiObjectName.c_str());
        si->setSize(CEGUI::Size(0.2f, 0.2f));
        si->setImage(&imageSet->getImage((CEGUI::utf8*)"ClientBrush"));

        siCounter++;

        return si;
    }

    bool handleQuit(const CEGUI::EventArgs& e) {
        static_cast<GuiFrameListener*>(mFrameListener)->requestShutdown();
        return true;
    }

    bool handleNew(const CEGUI::EventArgs& e) {
        if(mEditorGuiSheet)
            CEGUI::WindowManager::getSingleton().destroyWindow(mEditorGuiSheet);

        mEditorGuiSheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultGUISheet", "NewLayout");

        CEGUI::Window* editorWindow = CEGUI::WindowManager::getSingleton().getWindow((CEGUI::utf8*)"OgreGuiDemo2/MainWindow");
        editorWindow->addChildWindow(mEditorGuiSheet);

        return true;
    }

    bool handleLoad(const CEGUI::EventArgs& e) {
        if(mEditorGuiSheet)
            CEGUI::WindowManager::getSingleton().destroyWindow(mEditorGuiSheet);

        mEditorGuiSheet = 
            CEGUI::WindowManager::getSingleton().loadWindowLayout(
                (CEGUI::utf8*)"cegui8.layout"); 
		setupLoadedLayoutHandlers();

        CEGUI::Window* editorWindow = CEGUI::WindowManager::getSingleton().getWindow((CEGUI::utf8*)"OgreGuiDemo2/MainWindow");
        editorWindow->addChildWindow(mEditorGuiSheet);

        return true;
    }


    bool handleObjectSelection(const CEGUI::EventArgs& e) {
        static unsigned int windowNumber = 0;
        static unsigned int vertScrollNumber = 0;
        static unsigned int horizScrollNumber = 0;
        static unsigned int textScrollNumber = 0;
        String guiObjectName;
        CEGUI::Window* window = 0;

        // Set a random position to place this object.
        Real posX = Math::RangeRandom(0.0, 0.7); 
        Real posY = Math::RangeRandom(0.1, 0.7); 

        const CEGUI::WindowEventArgs& windowEventArgs = static_cast<const CEGUI::WindowEventArgs&>(e);
        CEGUI::ListboxItem* item = static_cast<CEGUI::Combobox*>(windowEventArgs.window)->getSelectedItem();

        CEGUI::Window* editorWindow = CEGUI::WindowManager::getSingleton().getWindow((CEGUI::utf8*)"OgreGuiDemo2/MainWindow");

        switch(item->getID())
        {
        case 0:
            guiObjectName = "NewWindow" + StringConverter::toString(windowNumber);
            window = CEGUI::WindowManager::getSingleton().createWindow((CEGUI::utf8*)"TaharezLook/FrameWindow", (CEGUI::utf8*)guiObjectName.c_str());
            window->setSize(CEGUI::Size(0.3f, 0.3f));
            window->setText((CEGUI::utf8*)"New Window");
            windowNumber++;
            break;
        case 1:
            guiObjectName = "NewHorizScroll" + StringConverter::toString(horizScrollNumber);
            window = CEGUI::WindowManager::getSingleton().createWindow((CEGUI::utf8*)"TaharezLook/HorizontalScrollbar", (CEGUI::utf8*)guiObjectName.c_str());
            window->setSize(CEGUI::Size(0.75f, 0.03f));
            horizScrollNumber++;
            break;
        case 2:
            guiObjectName = "NewVertScroll" + StringConverter::toString(vertScrollNumber);
            window = CEGUI::WindowManager::getSingleton().createWindow((CEGUI::utf8*)"TaharezLook/VerticalScrollbar", (CEGUI::utf8*)guiObjectName.c_str());
            window->setSize(CEGUI::Size(0.03f, 0.75f));
            vertScrollNumber++;
            break;
        case 3:
            guiObjectName = "NewStaticText" + StringConverter::toString(textScrollNumber);
            window = CEGUI::WindowManager::getSingleton().createWindow((CEGUI::utf8*)"TaharezLook/StaticText", (CEGUI::utf8*)guiObjectName.c_str());
            window->setSize(CEGUI::Size(0.25f, 0.1f));
            window->setText((CEGUI::utf8*)"Example static text");
            textScrollNumber++;
            break;
        case 4:
            window = createStaticImageObject();
            break;
        case 5:
            window = createRttGuiObject();
            break;
        };

        editorWindow->addChildWindow(window);
        window->setPosition(CEGUI::Point(posX, posY));

        return true;
    }

	bool handleColourChanged(const CEGUI::EventArgs& e) {
		mPreview->setImageColours(CEGUI::colour(
			mRed->getScrollPosition() / 255.0f,
			mGreen->getScrollPosition() / 255.0f,
			mBlue->getScrollPosition() / 255.0f));

		return true;

	}

	bool handleAdd(const CEGUI::EventArgs& e) {
		CEGUI::ListboxTextItem *listboxitem = 
			new CEGUI::ListboxTextItem (mEditBox->getText());
		listboxitem->setSelectionBrushImage("TaharezLook", "ListboxSelectionBrush");
		listboxitem->setSelected(mList->getItemCount() == 0);
		listboxitem->setSelectionColours(mPreview->getImageColours());
		mList->addItem(listboxitem);
		return true;
	}

	bool handleMouseEnters(const CEGUI::EventArgs& e) {
		CEGUI::WindowEventArgs& we = ((CEGUI::WindowEventArgs&)e);
		DescriptionMap::iterator i = 
			mDescriptionMap.find(we.window->getName());
		if (i != mDescriptionMap.end())
		{
			mTip->setText(i->second);
		}
		else
		{
			mTip->setText((CEGUI::utf8*)"");
		}
		
		return true;
	}
	
	bool handleMouseLeaves(const CEGUI::EventArgs& e) {
		mTip->setText((CEGUI::utf8*)"");
		return true;
	}
};


/*
    // Create application object
    MyApplication app;
    try {
        app.go();
*/
// renderOneFrame instead of startrendering
#endif
