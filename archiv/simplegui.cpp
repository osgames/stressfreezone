#include "prefix.h"
#include "simplegui.h"
#include "robstring1.2.h"
#include "game.h"
#include "scripting.h"

#define ASSERT(x) assert(x)
#define min(a,b) (a>b?b:a)
#define max(a,b) (a<b?b:a)

#include <map>

#include <CEGUI/elements/CEGUICombobox.h>
#include <CEGUI/elements/CEGUIListbox.h>
#include <CEGUI/elements/CEGUIListboxTextItem.h>
#include <CEGUI/elements/CEGUIPushButton.h>
#include <CEGUI/elements/CEGUIScrollbar.h>
#include <CEGUI/elements/CEGUIScrolledContainer.h>
#include <CEGUI/elements/CEGUIScrollablePane.h>
#include <CEGUI/elements/CEGUIStaticImage.h>
#include <CEGUI/elements/CEGUIStaticText.h>
#include <CEGUI/elements/CEGUIEditbox.h>
#include <CEGUI/elements/CEGUIMultiLineEditbox.h>
#include <CEGUI/elements/CEGUIFrameWindow.h>
#include <CEGUI/elements/CEGUICheckbox.h>

///prints count chars c
void NPrint(int count,char c=' '){ PROFILE
		for(int i=0;i<count;++i)printf("%c",c);
}

///simple gui elements
enum kSimpleGuiElement {
		kSGUI_Window,
		kSGUI_Text,
		kSGUI_Rows,
		kSGUI_Row,
		kSGUI_Cols,
		kSGUI_Col,
		kSGUI_Button,
		kSGUI_Edit,
		kSGUI_Checkbox,
		kSGUI_MultiLineEdit,
		kSGUI_Scrollable,
		kSGUI_Unknown
};


kSimpleGuiElement GetGuiElementFromName(const char *sName);
const char *GetNodeContent(TiXmlNode *Node);
const char *GetNodeAttributeValue(TiXmlElement *Node,const char *sAttributeName);
unsigned int CountChildElements(TiXmlNode *Node,const char *sName);


///represents a xml node of simplegui xml
class SimpleGuiNode {
public:
		//string pair list of attribute name and value of xml node
		std::map<std::string,std::string> mlAttribute;
		//node name
		std::string msName;
		//link to parent object, and next one with the same parent and first child
		SimpleGuiNode *mParent,*mNext,*mFirstChild;
		//cegui window that represents this node, can be 0
		CEGUI::Window *mWindow;

		SimpleGuiNode(SimpleGuiNode *parent) : mParent(parent), mWindow(0), mNext(0), mFirstChild(0) {
		}
		
		///creates a node with the given size infos belonging to the given parent
		SimpleGuiNode(SimpleGuiNode *parent,float fX,float fY,float fW,float fH) : mParent(parent), mWindow(0), mNext(0), mFirstChild(0) { PROFILE
			SetAttributeInt("x",fX);
			SetAttributeInt("y",fY);
			SetAttributeInt("w",fW);
			SetAttributeInt("h",fH);
		}
		
		~SimpleGuiNode(){ PROFILE
				if(mFirstChild)delete(mFirstChild);
				if(mNext)delete(mNext);
		}
		
		///gets the cegui window belonging to this node or the first parent one that can be found
		CEGUI::Window *GetWindow(){ PROFILE
			if(mWindow == 0 && mParent == 0){
				printf("ERROR: no window and no parent!!!!");
				return 0;
			} else if(mWindow == 0 && mParent != 0){
				return mParent->GetWindow();
			} else return mWindow;
		}
		
		void ReadNameFromXMLElement(TiXmlElement *node){ PROFILE
				ASSERT(node);
				msName = std::string(node->Value());
		}
		
		void ReadAttributesFromXMLElement(TiXmlElement *node){ PROFILE
				ASSERT(node);

				TiXmlAttribute* pAttrib = node->FirstAttribute();
				while (pAttrib)
				{
						std::string sName(pAttrib->Name());
						std::string sValue(pAttrib->Value());
						mlAttribute[sName] = sValue;
						pAttrib=pAttrib->Next();
				}
		}
		
		bool HasAttribute(const std::string &sName){ PROFILE
				return (mlAttribute.find(sName) != mlAttribute.end());
		}
		
		int GetAttributeInt(const std::string &sName){ PROFILE
				return atoi((mlAttribute[sName]).c_str());
		}

		const std::string &GetAttribute(const std::string &sName){ PROFILE
				return mlAttribute[sName];
		}
		
		void SetAttributeInt(const std::string &sName,float fValue){ PROFILE
				mlAttribute[sName] = strprintf("%0.0f",fValue);
		}

		void SetAttributeInt(const std::string &sName,int iValue){ PROFILE
				mlAttribute[sName] = strprintf("%i",iValue);
		}

		const char *GetAttributeCstr(const std::string &sName){ PROFILE
				return mlAttribute[sName].c_str();
		}

		bool GetAttributeBool(const std::string &sName){ PROFILE
				return strcmp(mlAttribute[sName].c_str(),"1")==0;
		}

		void AddChild(SimpleGuiNode *node){ PROFILE
				ASSERT(node);
				if(mFirstChild == 0)mFirstChild = node;
				else mFirstChild->AddNext(node);
		}
		
		void AddNext(SimpleGuiNode *node){ PROFILE
				ASSERT(node);
				if(mNext == 0)mNext = node;
				else mNext->AddNext(node);
		}

		void RemoveChild(SimpleGuiNode  *node){ PROFILE
				ASSERT(node);
				if(mFirstChild == 0)return;
				else if(mFirstChild == node)mFirstChild = mFirstChild->mNext;
				else mFirstChild->RemoveNext(node);
		}
		
		void RemoveNext(SimpleGuiNode  *node){ PROFILE
				ASSERT(node);
				if(mNext == 0)return;
				else if(mNext == node)mNext = mNext->mNext;
				else mNext->RemoveNext(node);
		}

		bool HasChild(SimpleGuiNode *node){ PROFILE
				ASSERT(node);
				if(mFirstChild == 0)return false;
				else if(mFirstChild == node)return true;
				else mFirstChild->HasNext(node);
		}
		
		bool HasNext(SimpleGuiNode *node){ PROFILE
				ASSERT(node);
				if(mNext == 0)return false;
				else if(mNext == node)return true;
				else return mNext->HasNext(node);
		}
		
		void PrintMe(){ PROFILE
				printf(" %s[",msName.c_str());
				for(std::map<std::string,std::string>::iterator it=mlAttribute.begin();it!=mlAttribute.end();++it)
						printf("'%s'='%s' ",(*it).first.c_str(),(*it).second.c_str());
				printf("]\n");
		}
		
		///some output for debugging, dumps the tree
		void Print(int level){ PROFILE
				NPrint(level,'+');
				PrintMe();
				if(mFirstChild)mFirstChild->Print(level+1);
				if(mNext)mNext->Print(level);
		}

		///searche sname in this and all childs, return -1 if nothing found
		int GetNextAttributeInt(const std::string &sName){ PROFILE
				if(HasAttribute(sName)){
						printf("GetNextAttributeInt: FOUND %s=%i\n",sName.c_str(),GetAttributeInt(sName));
						return GetAttributeInt(sName);
				} else {
						for(SimpleGuiNode *Child = mFirstChild;Child;Child = Child->mNext){
								int x = Child->GetNextAttributeInt(sName);
								if(x != -1)return x;
						}
						return -1;
				}
		}
		
		///calculates a possible size for the node, depending on the type and childs
		void GetPreferredSize(float &width,float &height, const unsigned int iBorder){ PROFILE
				static int defaultW = 80;
				static int defaultH = 20;
				
				if(mFirstChild == 0){
						//leave node default width if none is set
						if(HasAttribute("h"))height = GetAttributeInt("h");else height = defaultH;
						if(HasAttribute("w"))width = GetAttributeInt("w");else width = defaultW;
				} else {
						width = iBorder;
						height = iBorder;
						
						if(HasAttribute("h"))height = GetAttributeInt("h");
						if(HasAttribute("w"))width = GetAttributeInt("w");
						
						//should width,height sum increase with each child? (used for rows and cols)
						bool bStackW,bStackH;
						kSimpleGuiElement type = GetGuiElementFromName(msName.c_str());
						switch(type){
								case kSGUI_Rows:	bStackW=false;	bStackH=true;	break;
								case kSGUI_Cols:	bStackW=true;	bStackH=false;	break;
								case kSGUI_Scrollable: 
										height = defaultH;
										width = defaultW;
										printf("GetPreferredSize: END %s: %0.2f x %0.2f\n",msName.c_str(),width,height);
										return;
								break;
								default:					bStackW=false;	bStackH=false;	break;
						}

						for(SimpleGuiNode *Child = mFirstChild;Child;Child = Child->mNext){
								float w,h;
								Child->GetPreferredSize(w,h,iBorder);
								if(!HasAttribute("h")){
										if(bStackH)height += h+iBorder;else height = max(height,2*iBorder+h);
										//printf("Add to %s h %f+boder %d   stackh=%d height=%0.2f\n",msName.c_str(),h,iBorder,bStackH?1:0,height);
										//printf("\n",height);
								} //else printf("H set\n");
								if(!HasAttribute("w")){
										if(bStackW)width += w+iBorder;else width = max(width,2*iBorder+w);
										//printf("Add w %f+boder %d   stackw=%d\n",w,iBorder,bStackW?1:0);
										//printf("width=%0.2f\n",width);
								} //else printf("W set\n");
						}
				}
				printf("GetPreferredSize: END %s: %0.2f x %0.2f\n",msName.c_str(),width,height);
		}
};




///cegui mouse click callback, calls lua click callback with window handle and button id
bool cSimpleGui::handleClick(const CEGUI::EventArgs& e){ PROFILE
		const CEGUI::WindowEventArgs *ew = static_cast<const CEGUI::WindowEventArgs *>(&e);
		CEGUI::Window *w = ew->window;
		const std::string & id = GetId(w);
		unsigned int handle = GetHandle(w);

		printf("handleClick: handle=%i window=%i id=%s\n",handle,w,id.c_str());
		cGame::GetSingleton().mpScripting->LuaCall("GUIEventClick","is",handle,id.c_str());
		
		return true;
}

///cegui close callback, calls lua close with window handle
bool cSimpleGui::handleClose(const CEGUI::EventArgs& e){ PROFILE
		const CEGUI::WindowEventArgs *ew = static_cast<const CEGUI::WindowEventArgs *>(&e);
		CEGUI::Window *w = ew->window;
		const std::string & id = GetId(w);
		unsigned int handle = GetHandle(w);

		printf("handleClose: handle=%i window=%i id=%s\n",handle,w,id.c_str());
		cGame::GetSingleton().mpScripting->LuaCall("GUIEventClose","is",handle,id.c_str());
		
		CloseWindow(handle);
		
		return true;
}

void ReparseWindowSize(SimpleGuiNode *Node,float iX,float iY,float iW,float iH);

///cegui callback to handle a resize event, this will recalculate the simplegui xml tree node sizes
bool cSimpleGui::handleSized(const CEGUI::EventArgs& e){ PROFILE
		CEGUI::Rect rect = mpParent->getRect(CEGUI::Absolute);
		const CEGUI::WindowEventArgs *ew = static_cast<const CEGUI::WindowEventArgs *>(&e);
		CEGUI::Window *wnd = ew->window;
		const std::string & id = GetId(wnd);
		unsigned int handle = GetHandle(wnd);

		float w = wnd->getWidth();
		float h = wnd->getHeight();
	
		SimpleGuiNode *node = mlHandleSimpleGuiNode[handle];
	
		if(node){
			node->SetAttributeInt("w",w);
			node->SetAttributeInt("h",h);
			
			printf("handleResize: handle=%i window=%i id=%s w=%0.2f h=%0.2f\n",handle,wnd,id.c_str(),w,h);
			ReparseWindowSize(node,rect.getPosition().d_x,rect.getPosition().d_y,rect.getWidth(),rect.getHeight());
		} else {
			printf("handleResize: NO NODE FOUND handle=%i window=%i id=%s w=%0.2f h=%0.2f\n",handle,wnd,id.c_str(),w,h);
		}
		//cGame::GetSingleton().mpScripting->LuaCall("GUIEventClose","is",handle,id.c_str());
		
		//CloseWindow(handle);
		
		return true;
}

///converts node names (string) to node type constants
kSimpleGuiElement GetGuiElementFromName(const char *sName){ PROFILE
		ASSERT(sName);
		if(stricmp(sName,"window") == 0)return kSGUI_Window;
		else if(stricmp(sName,"text") == 0)return kSGUI_Text;
		else if(stricmp(sName,"rows") == 0)return kSGUI_Rows;
		else if(stricmp(sName,"row") == 0)return kSGUI_Row;
		else if(stricmp(sName,"cols") == 0)return kSGUI_Cols;
		else if(stricmp(sName,"col") == 0)return kSGUI_Col;
		else if(stricmp(sName,"button") == 0)return kSGUI_Button;
		else if(stricmp(sName,"edit") == 0)return kSGUI_Edit;
		else if(stricmp(sName,"multilineedit") == 0)return kSGUI_MultiLineEdit;
		else if(stricmp(sName,"checkbox") == 0)return kSGUI_Checkbox;
		else if(stricmp(sName,"scrollable") == 0)return kSGUI_Scrollable;
		else return kSGUI_Unknown;
}

///reads the content of a xml node, ie. <bla>blub</bla> shold return blub if the given node is bla
const char *GetNodeContent(TiXmlNode *Node){ PROFILE
		ASSERT(Node);
		for(TiXmlNode *Child = Node->FirstChild();Child;Child = Child->NextSibling()){
				if(Child->Type() == TiXmlNode::TEXT)return Child->ToText()->Value();
		}
		return 0;
}

///returns the value of a node's attribute or 0 if it dont exists
const char *GetNodeAttributeValue(TiXmlElement *Node,const char *sAttributeName){ PROFILE
		ASSERT(Node && sAttributeName);

		TiXmlAttribute* pAttrib=Node->FirstAttribute();
		while (pAttrib)
		{
				if(strcmp(pAttrib->Name(),sAttributeName) == 0){
						//printf("checking attrib: %s=%s\n",pAttrib->Name(),pAttrib->Value());
						return pAttrib->Value();
				}
				pAttrib=pAttrib->Next();
		}
		return 0;
}

///returns the value of a node's attribute, if it don't exists this function will descent to the first child and 
///try to find an attribute with the given name and so on
///only the first child element of every "layer" is checked
unsigned int GetNextNodeAttributeValueUInt(TiXmlElement *Node,const char *sAttributeName){ PROFILE
		ASSERT(Node && sAttributeName);

		TiXmlElement *p = Node;
		
		while(p){
				TiXmlAttribute* pAttrib=p->FirstAttribute();
				while (pAttrib)
				{
						if(strcmp(pAttrib->Name(),sAttributeName) == 0){
								//printf("found attrib: %s=%s\n",pAttrib->Name(),pAttrib->Value());
								const char * s=pAttrib->Value();
								if(s)return atoi(s);
								else return 0;
						}
						pAttrib=pAttrib->Next();
				}
				p = p->FirstChildElement();
		}
		return 0;
}


///returns the number of a childs with a given name
unsigned int CountChildElements(TiXmlNode *Node,const char *sName){ PROFILE
		ASSERT(Node);
		int count = 0;
		for(TiXmlNode *Child = Node->FirstChild();Child;Child = Child->NextSibling()){
				if(Child->Type() == TiXmlNode::ELEMENT)if(strcmp(Child->Value(),sName)==0)++count;
		}
		return count;
}


///collects all sName attributes of Node's children, only integer values are supported.
///the result will be stored in lHeight. unset nodes get the value -1
void ReadOutSizeOfChildren(const char *sName, SimpleGuiNode *Node,std::map<SimpleGuiNode *,float> &lHeight){ PROFILE
		lHeight.clear();
		for(SimpleGuiNode *Child = Node->mFirstChild;Child;Child = Child->mNext){
				//-1 if unset
				std::string name = std::string(sName);
				lHeight[Child] = Child->GetNextAttributeInt(name);
				printf("ReadOutSizeOfChildren: child=%i value=%0.2f\n",Child,lHeight[Child]);
		}
}

SimpleGuiNode *ParseSimpleGuiNode(SimpleGuiNode *parent,TiXmlNode *Node){ PROFILE
		if(!Node)return 0;
		int t = Node->Type();
		switch(t){
				case TiXmlNode::ELEMENT:{
						//printf("parse node %i with parent %i\n",Node,parent);
						SimpleGuiNode *my = new SimpleGuiNode(parent);
						my->ReadAttributesFromXMLElement(Node->ToElement());
						my->ReadNameFromXMLElement(Node->ToElement());
						for(TiXmlElement *Child = Node->FirstChildElement();Child;Child = Child->NextSiblingElement()){
								SimpleGuiNode *x = ParseSimpleGuiNode(my,Child);
								my->AddChild(x);
						}
						return my;
				}
				break;
				default:{
						return ParseSimpleGuiNode(parent,Node->FirstChildElement());
				}
				break;
		}
}

///common size code of the build window function
void BuildWindowCommonSize(CEGUI::Window *window,float iX, float iY, float iW, float iH){ PROFILE
		window->setMetricsMode(CEGUI::Absolute);
		window->setPosition(CEGUI::Point(iX, iY));
		window->setSize(CEGUI::Size(iW, iH));
}

///common window code of the build window function
void BuildWindowCommon(cSimpleGui *context, CEGUI::Window *window,SimpleGuiNode *Node,
		float iX, float iY, float iW, float iH, unsigned int iHandle){ PROFILE
		ASSERT(context && window && Node);
		
		Node->mWindow = window;
		BuildWindowCommonSize(window,iX,iY,iW,iH);
		if(Node->HasAttribute("id"))context->SetId(iHandle,Node->GetAttributeCstr("id"),window);
		if(Node->HasAttribute("text"))window->setText(Node->GetAttributeCstr("text"));
}

///common code of reparse window size code for rows and cols
void ReparseWindowSizeRowsCols(bool isRows, SimpleGuiNode *Node, const float iX,const float iY,const float iW,const float iH,const unsigned int iBorder){ PROFILE
		ASSERT(Node);

		//readout all sizes
		std::map<SimpleGuiNode *,float> lHeight;
		if(isRows)ReadOutSizeOfChildren("h",Node,lHeight);
		else ReadOutSizeOfChildren("w",Node,lHeight);
		//calc freespace and allocate it to the unset nodes
		float usedspace = 0;
		int countdefined = 0;
		int countundefined = 0;
		
		for(std::map<SimpleGuiNode *,float>::iterator it=lHeight.begin();it!=lHeight.end();++it){
				float x = (*it).second;
				if(x < 0.0f)++countundefined;
				else ++countdefined;
				usedspace += x;
		}
		
		int count = countdefined + countundefined;
		
		float freespace;
		if(isRows)freespace = iH-(count-1)*iBorder;
		else freespace = iW-(count-1)*iBorder;
		
		float d = (freespace - usedspace) / countundefined;
		//printf("defined=%i undefiend=%i usedspace=%i free=%i d=%i\n",countdefined,countundefined,usedspace,freespace,d);
		
		//TODO: if the summed up height is to high reduce all parts until fit
		float i;
		if(isRows)i = iY;
		else i = iX;
		
		for(SimpleGuiNode *Child = Node->mFirstChild;Child;Child = Child->mNext){
				printf("SGUI: this window has a child so parse it\n");
				float j = lHeight[Child];//GetNextNodeAttributeValueUInt(Child,"h");
				//printf("SGUI: generating row j=%i d=%i i=%i\n",j,d,i);
				if(j < 0.0f)j = d;
				CEGUI::Window *window;
				if(isRows)ReparseWindowSize(Child,iX,i,iW,j);
				else ReparseWindowSize(Child,i,iY,j,iH);
				i+=j+iBorder;
		}												
}

//common code fo rows and cols of the buildwindow function
CEGUI::Window *BuildWindowCommonRowsCols(bool isRows, cSimpleGui *context, SimpleGuiNode *Node,CEGUI::Window *ParentWindow,
		const float iX,const float iY,const float iW,const float iH,const unsigned int iHandle, const unsigned int iBorder){ PROFILE
		ASSERT(context && Node && ParentWindow);

		//readout all sizes
		std::map<SimpleGuiNode *,float> lHeight;
		if(isRows)ReadOutSizeOfChildren("h",Node,lHeight);
		else ReadOutSizeOfChildren("w",Node,lHeight);
		//calc freespace and allocate it to the unset nodes
		float usedspace = 0;
		int countdefined = 0;
		int countundefined = 0;
		
		for(std::map<SimpleGuiNode *,float>::iterator it=lHeight.begin();it!=lHeight.end();++it){
				float x = (*it).second;
				if(x < 0.0f)++countundefined;
				else ++countdefined;
				usedspace += x;
		}
		
		int count = countdefined + countundefined;
		
		float freespace;
		if(isRows)freespace = iH-(count-1)*iBorder;
		else freespace = iW-(count-1)*iBorder;
		
		float d = (freespace - usedspace) / countundefined;
		//printf("defined=%i undefiend=%i usedspace=%i free=%i d=%i\n",countdefined,countundefined,usedspace,freespace,d);
		
		//TODO: if the summed up height is to high reduce all parts until fit
		float i;
		if(isRows)i = iY;
		else i = iX;
		
		for(SimpleGuiNode *Child = Node->mFirstChild;Child;Child = Child->mNext){
				printf("SGUI: this window has a child so parse it\n");
				float j = lHeight[Child];//GetNextNodeAttributeValueUInt(Child,"h");
				//printf("SGUI: generating row j=%i d=%i i=%i\n",j,d,i);
				if(j < 0.0f)j = d;
				CEGUI::Window *window;
				if(isRows)window = context->BuildWindow(ParentWindow,Child,iX,i,iW,j,iHandle);
				else window = context->BuildWindow(ParentWindow,Child,i,iY,j,iH,iHandle);
				if(window)ParentWindow->addChildWindow(window);
				i+=j+iBorder;
		}												
		
		return ParentWindow;
}


///recalculates the sizes of the simplegui window tree
void ReparseWindowSize(SimpleGuiNode *Node,float iX,float iY,float iW,float iH){ PROFILE
	if(!Node)return;
	
	const char *s;
	TiXmlNode* pChild;
	TiXmlText* pText;

	static int iBorder = 5;
	
	printf("RPWS SGUI: parent window metrics: %0.2f,%0.2f,%0.2f,%0.2f\n",iX,iY,iW,iH);
	printf("RPWS SGUI: found %s\n",Node->msName.c_str());
	kSimpleGuiElement element = GetGuiElementFromName(Node->msName.c_str());
	if(Node->HasAttribute("w"))iW = Node->GetAttributeInt("w");
	if(Node->HasAttribute("h"))iH = Node->GetAttributeInt("h");
	if(Node->HasAttribute("x"))iX = Node->GetAttributeInt("x");
	if(Node->HasAttribute("y"))iY = Node->GetAttributeInt("y");
	printf("RPWS SGUI: window metrics: %0.2f,%0.2f,%0.2f,%0.2f\n",iX,iY,iW,iH);
											
	switch(element){
		case kSGUI_Window:
		case kSGUI_Scrollable:{
			try{
					BuildWindowCommonSize(Node->mWindow,iX,iY,iW,iH);
					CEGUI::Rect inner = Node->GetWindow()->getInnerRect();
					CEGUI::Point p = inner.getPosition()-Node->GetWindow()->getPosition();
					//CEGUI::Rect inner = Node->GetWindow()->getInnerRect();
					//CEGUI::Point p = inner.getPosition()-Node->mParent->GetWindow()->getPosition();

					//printf("INNER RECT: left=%0.2f top=%0.2f width=%0.2f height=%0.2f\n",inner.d_left,inner.d_top,inner.getWidth(),inner.getHeight());
					for(SimpleGuiNode *Child = Node->mFirstChild;Child;Child = Child->mNext){
							printf("RPWS SGUI: this window has a child so parse it\n");
							//ReparseWindowSize(Child,inner.d_left,inner.d_top,inner.getWidth(),inner.getHeight());
							ReparseWindowSize(Child,p.d_x+iBorder,p.d_y+iBorder,inner.getWidth()-2*iBorder,inner.getHeight()-2*iBorder);
					}												
			}
			catch(...){printf("RPWS SGUI: ERROR\n");return;}
		}
		break;
		case kSGUI_Button:
		case kSGUI_Text:
		case kSGUI_Edit:
		case kSGUI_Checkbox:
		case kSGUI_MultiLineEdit:
			//try{ ReparseWindowSize(Node,iX,iY,iW,iH); }
			try{ BuildWindowCommonSize(Node->mWindow,iX,iY,iW,iH); }
			catch(...){printf("RPWS SGUI: ERROR\n");return;}
		break;
						
		case kSGUI_Rows:
			try{ ReparseWindowSizeRowsCols(true, Node, iX, iY, iW, iH, iBorder); }
			catch(...){printf("RPWS SGUI: ERROR\n");return;}
		break;
		case kSGUI_Cols:
			try{ ReparseWindowSizeRowsCols(false, Node, iX, iY, iW, iH, iBorder); }
			catch(...){printf("RPWS SGUI: ERROR\n");return;}
		break;
		case kSGUI_Col:
		case kSGUI_Row:
			try{
				for(SimpleGuiNode *Child = Node->mFirstChild;Child;Child = Child->mNext){
						printf("RPWS SGUI: this window has a child so parse it\n");
						ReparseWindowSize(Child,iX,iY,iW,iH);
				}									
			}
			catch(...){printf("RPWS SGUI: ERROR\n");return;}
		break;
		default:
			printf("RPWS SGUI: found unknown\n");
			return;
		break;
	}
}


CEGUI::Window *cSimpleGui::BuildWindow(CEGUI::Window *ParentWindow, SimpleGuiNode *Node,
		float iX,float iY,float iW,float iH,const unsigned int iHandle){ PROFILE
		if(!mpWndMng)return 0;
		if(!Node)return 0;
		
		const char *s;
		TiXmlNode* pChild;
		TiXmlText* pText;

		static int iBorder = 5;
		
		printf("SGUI: parent window metrics: %0.2f,%0.2f,%0.2f,%0.2f\n",iX,iY,iW,iH);
		printf("SGUI: found %s\n",Node->msName.c_str());
		kSimpleGuiElement element = GetGuiElementFromName(Node->msName.c_str());
		if(Node->HasAttribute("w"))iW = Node->GetAttributeInt("w");
		if(Node->HasAttribute("h"))iH = Node->GetAttributeInt("h");
		if(Node->HasAttribute("x"))iX = Node->GetAttributeInt("x");
		if(Node->HasAttribute("y"))iY = Node->GetAttributeInt("y");
		printf("SGUI: window metrics: %0.2f,%0.2f,%0.2f,%0.2f\n",iX,iY,iW,iH);
												
		switch(element){
				case kSGUI_Scrollable:{
						try{
								CEGUI::ScrollablePane *parent = (CEGUI::ScrollablePane *)mpWndMng->createWindow("TaharezLook/ScrollablePane");
								BuildWindowCommon(this,parent,Node,iX,iY,iW,iH,iHandle);
								parent->setContentPaneAutoSized (true);
								
								CEGUI::Rect inner = parent->getInnerRect();
								CEGUI::Point p = inner.getPosition()-parent->getPosition();

								for(SimpleGuiNode *Child = Node->mFirstChild;Child;Child = Child->mNext){
										printf("SGUI: this window has a child so parse it\n");
										CEGUI::Window *window = BuildWindow(parent,Child,
											p.d_x+iBorder,p.d_y+iBorder,inner.getWidth()-2*iBorder,inner.getHeight()-2*iBorder,iHandle);
										if(window)parent->addChildWindow(window);
								}												
								
								return parent;
						}
						catch(...){printf("SGUI: ERROR\n");return 0;}
				}
				break;
				case kSGUI_Window:{
						try{
								CEGUI::FrameWindow *parent = (CEGUI::FrameWindow *)mpWndMng->createWindow("TaharezLook/FrameWindow");
								BuildWindowCommon(this,parent,Node,iX,iY,iW,iH,iHandle);
								parent->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, CEGUI::Event::Subscriber(&cSimpleGui::handleClose, this));
								parent->subscribeEvent(CEGUI::FrameWindow::EventSized, CEGUI::Event::Subscriber(&cSimpleGui::handleSized, this));
								
								//closeable=1/0: can the user close this window with the X?
								if(Node->HasAttribute("closeable"))parent->setCloseButtonEnabled(Node->GetAttributeBool("closeable"));
								CEGUI::Rect inner = parent->getInnerRect();
								CEGUI::Point p = inner.getPosition()-parent->getPosition();
								for(SimpleGuiNode *Child = Node->mFirstChild;Child;Child = Child->mNext){
										printf("SGUI: this window has a child so parse it\n");
										CEGUI::Window *window = BuildWindow(parent,Child,
											p.d_x+iBorder,p.d_y+iBorder,inner.getWidth()-2*iBorder,inner.getHeight()-2*iBorder,iHandle);
										if(window)parent->addChildWindow(window);
								}												
								
								return parent;
						}
						catch(...){printf("SGUI: ERROR\n");return 0;}
				}
				break;
				case kSGUI_Text:{
						try{
								CEGUI::StaticText *parent = (CEGUI::StaticText *)mpWndMng->createWindow("TaharezLook/StaticText");
								BuildWindowCommon(this,parent,Node,iX,iY,iW,iH,iHandle);
								return parent;
						}
						catch(...){printf("SGUI: ERROR\n");return 0;}
				}
				break;
								
				case kSGUI_Rows:
						try{
								return BuildWindowCommonRowsCols(true, this, Node, ParentWindow, iX, iY, iW, iH, iHandle, iBorder);
						}
						catch(...){printf("SGUI: ERROR\n");return 0;}
				break;
				case kSGUI_Cols:
						try{
								return BuildWindowCommonRowsCols(false, this, Node, ParentWindow, iX, iY, iW, iH, iHandle, iBorder);
						}
						catch(...){printf("SGUI: ERROR\n");return 0;}
				break;
				case kSGUI_Col:
				case kSGUI_Row:
						try{
								for(SimpleGuiNode *Child = Node->mFirstChild;Child;Child = Child->mNext){
										printf("SGUI: this window has a child so parse it\n");
										CEGUI::Window *window = BuildWindow(ParentWindow,Child,iX,iY,iW,iH,iHandle);
										if(window)ParentWindow->addChildWindow(window);
								}									
								return ParentWindow;
						}
						catch(...){printf("SGUI: ERROR\n");return 0;}
				break;
				case kSGUI_Button:
						try{
								CEGUI::StaticText *parent = (CEGUI::StaticText *)mpWndMng->createWindow("TaharezLook/Button");
								BuildWindowCommon(this,parent,Node,iX,iY,iW,iH,iHandle);
								parent->setHorizontalAlignment(CEGUI::HA_LEFT);
								parent->setVerticalAlignment(CEGUI::VA_TOP);
								parent->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&cSimpleGui::handleClick, this));
								mlWindowHandle[parent] = iHandle;
								return parent;
						}
						catch(...){printf("SGUI: ERROR\n");return 0;}
				break;
				case kSGUI_Edit:
						try{
								CEGUI::StaticText *parent = (CEGUI::StaticText *)mpWndMng->createWindow("TaharezLook/Editbox");
								BuildWindowCommon(this,parent,Node,iX,iY,iW,iH,iHandle);
								return parent;
						}
						catch(...){printf("SGUI: ERROR\n");return 0;}
				break;
				case kSGUI_MultiLineEdit:
						try{
								CEGUI::MultiLineEditbox *parent = (CEGUI::MultiLineEditbox *)mpWndMng->createWindow("TaharezLook/MultiLineEditbox");
								BuildWindowCommon(this,parent,Node,iX,iY,iW,iH,iHandle);
								//readonly=1/0: is this cute big box readonly?
								if(Node->HasAttribute("readonly"))parent->setReadOnly(Node->GetAttributeBool("readonly"));
								return parent;
						}
						catch(...){printf("SGUI: ERROR\n");return 0;}
				break;
				case kSGUI_Checkbox:
						try{
								CEGUI::Checkbox *parent = (CEGUI::Checkbox *)mpWndMng->createWindow("TaharezLook/Checkbox");
								BuildWindowCommon(this,parent,Node,iX,iY,iW,iH,iHandle);
								//checked=1/0: is this cute little box selected?
								if(Node->HasAttribute("checked"))parent->setSelected(Node->GetAttributeBool("checked"));
								return parent;
						}
						catch(...){printf("SGUI: ERROR\n");return 0;}
				break;
				default:
						printf("SGUI: found unknown\n");
						return 0;
				break;
		}
}

///returns the next unused window handle
unsigned int cSimpleGui::GetFreeHandle(){ PROFILE
		return miNextHandle++;
}

///generates a cegui + lua handling of buttons and things like this from an xml string
unsigned int cSimpleGui::Parse(const char *sXml){ PROFILE
		ASSERT(sXml);
		if(!mpWndMng && !mpParent)return 0;
		unsigned int handle = GetFreeHandle();
		TiXmlDocument Xml;
		Xml.Parse(sXml);
		CEGUI::Rect rect = mpParent->getRect(CEGUI::Absolute);
		SimpleGuiNode * node = ParseSimpleGuiNode(mpSimpleGuiRootNode,Xml.RootElement());
		printf("parsing finished\n");
		float w,h;
		node->GetPreferredSize(w,h,5);
		if(!node->HasAttribute("w"))node->SetAttributeInt("w",w);
		if(!node->HasAttribute("h"))node->SetAttributeInt("h",h);
		node->Print(1);
		CEGUI::Window *window = BuildWindow(mpParent,node,rect.getPosition().d_x,rect.getPosition().d_y,rect.getWidth(),rect.getHeight(),handle);
		printf("generated window %i\n",window);
		if(window){
				mpParent->addChildWindow(window);
				printf("SGUI: oki attach new window %i with handle %i\n",window,handle);
				mlHandleWindow[handle] = window;
				mlWindowHandle[window] = handle;
				mlHandleSimpleGuiNode[handle] = node;
				ReparseWindowSize(node,rect.getPosition().d_x,rect.getPosition().d_y,rect.getWidth(),rect.getHeight());
				return handle;
		} else {
				delete(node);
				return 0;
		}
}

cSimpleGui::cSimpleGui() : miNextHandle(1) { PROFILE

}

CEGUI::Window *cSimpleGui::GetWindow(unsigned int iHandle,const std::string &sId){ PROFILE
		ASSERT(iHandle > 0);
		if(mlHandleIdWindow.find(iHandle) == mlHandleIdWindow.end())return 0;
		else return mlHandleIdWindow[iHandle][sId];
}

const std::string &cSimpleGui::GetId(CEGUI::Window *Window){ PROFILE
		static std::string empty;
		ASSERT(Window);
		if(mlWindowId.find(Window) == mlWindowId.end())return empty;
		else return mlWindowId[Window];
}

void cSimpleGui::SetId(unsigned int iHandle,const char *sId,CEGUI::Window *Window){ PROFILE
		ASSERT(Window);
		std::string s = std::string(sId);
		mlHandleIdWindow[iHandle][s] = Window;
		mlWindowId[Window] = s;
}

unsigned int cSimpleGui::GetHandle(CEGUI::Window *Window){ PROFILE
		return mlWindowHandle[Window];
}

void cSimpleGui::SetWndManagerAndParent(CEGUI::WindowManager *WndMng, CEGUI::Window *Parent){ PROFILE
		ASSERT(WndMng && Parent);
		mpWndMng = WndMng;
		mpParent = Parent;
		CEGUI::Rect rect = mpParent->getRect(CEGUI::Absolute);
		mpSimpleGuiRootNode = new SimpleGuiNode(0,rect.getPosition().d_x,rect.getPosition().d_y,rect.getWidth(),rect.getHeight());
		mpSimpleGuiRootNode->mWindow = Parent;
}

cSimpleGui::~cSimpleGui(){ PROFILE
	if(mpSimpleGuiRootNode)delete mpSimpleGuiRootNode;
}

const char *cSimpleGui::GetText(unsigned int iHandle,const char *sId){ PROFILE
		CEGUI::Window *w = GetWindow(iHandle,std::string(sId));
		if(w) return w->getText().c_str();
		else return 0;
}

void cSimpleGui::SetText(unsigned int iHandle,const char *sId,const char *sText){ PROFILE
		CEGUI::Window *w = GetWindow(iHandle,std::string(sId));
		if(w)w->setText(sText);
}

void cSimpleGui::CloseWindow(unsigned int iHandle){ PROFILE
		CEGUI::Window *w = GetBaseWindow(iHandle);
		printf("CloseWindow: w=%i\n",w);
		mlHandleWindow.erase(iHandle);
		mlHandleSimpleGuiNode.erase(iHandle);
		mlWindowHandle.erase(w);
		mlWindowId.erase(w);
		mlHandleIdWindow.erase(iHandle);
		mpWndMng->destroyWindow(w);
}

///returns the cegui-parent-of-all-windows-in-the-windowtree-with-handle-iHandle window 
CEGUI::Window *cSimpleGui::GetBaseWindow(unsigned int iHandle){ PROFILE
		if(mlHandleWindow.find(iHandle) == mlHandleWindow.end())return 0;
		else return mlHandleWindow[iHandle];
}


void cSimpleGui::SetCheckboxSelected(unsigned int iHandle,const char *sId,bool bSelected){ PROFILE
		CEGUI::Window *w = GetWindow(iHandle,std::string(sId));
		if(w && w->getType().compare("TaharezLook/Checkbox")==0){
				CEGUI::Checkbox *cb = static_cast<CEGUI::Checkbox *>(w);
				return cb->setSelected(bSelected);
		};
}

bool cSimpleGui::GetCheckboxSelected(unsigned int iHandle,const char *sId){ PROFILE
		CEGUI::Window *w = GetWindow(iHandle,std::string(sId));
		if(w && w->getType().compare("TaharezLook/Checkbox")==0){
				CEGUI::Checkbox *cb = static_cast<CEGUI::Checkbox *>(w);
				return cb->isSelected();
		} else return false;
}

#if 0
// stuff removed from scripting.cpp

static int l_Client_GUIParse (lua_State *L) { PROFILE
    const char *sXml = luaL_checkstring(L, 1);
	unsigned int handle = cSimpleGui::GetSingleton().Parse(sXml);
	lua_pushnumber(L, handle);
	return 1;
}

static int l_Client_GUISetText (lua_State *L) { PROFILE
    unsigned int handle = luaL_checkint(L, 1);
    const char *id = luaL_checkstring(L, 2);
    const char *text = luaL_checkstring(L, 3);
	cSimpleGui::GetSingleton().SetText(handle,id,text);
	return 0;
}

static int l_Client_GUIGetText (lua_State *L) { PROFILE
    unsigned int handle = luaL_checkint(L, 1);
    const char *id = luaL_checkstring(L, 2);
	const char *text = cSimpleGui::GetSingleton().GetText(handle,id);
	lua_pushstring(L, text);
	return 1;
}

static int l_Client_GUICloseWindow (lua_State *L) { PROFILE
    unsigned int handle = luaL_checkint(L, 1);
	cSimpleGui::GetSingleton().CloseWindow(handle);
	return 0;
}

static int l_Client_GUISetCheckboxSelected (lua_State *L) { PROFILE
    unsigned int handle = luaL_checkint(L, 1);
    const char *id = luaL_checkstring(L, 2);
    bool selected = luaL_checkint(L, 3)>0;
	cSimpleGui::GetSingleton().SetCheckboxSelected(handle,id,selected);
	return 0;
}

static int l_Client_GUIGetCheckboxSelected (lua_State *L) { PROFILE
    unsigned int handle = luaL_checkint(L, 1);
    const char *id = luaL_checkstring(L, 2);
    bool selected = cSimpleGui::GetSingleton().GetCheckboxSelected(handle,id);
	lua_pushnumber(L, selected?1:0);
	return 1;
}

	lua_register(L,"Client_GUIParse",			l_Client_GUIParse);
	lua_register(L,"Client_GUISetText",			l_Client_GUISetText);
	lua_register(L,"Client_GUIGetText",			l_Client_GUIGetText);
	lua_register(L,"Client_GUICloseWindow",			l_Client_GUICloseWindow);
	lua_register(L,"Client_GUIGetCheckboxSelected",			l_Client_GUIGetCheckboxSelected);
	lua_register(L,"Client_GUISetCheckboxSelected",			l_Client_GUISetCheckboxSelected);


#endif

