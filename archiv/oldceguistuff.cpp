// from client.h

namespace CEGUI {
	class WindowManager;
	class OgreCEGUIRenderer;
	class System;
	class Window;
	class StaticText;
};


	CEGUI::WindowManager*		mWindowManager;
	CEGUI::OgreCEGUIRenderer*	mGUIRenderer;
	CEGUI::System*				mGUISystem;
	CEGUI::Window*				mSheet;
	CEGUI::StaticText*			mInfoText;


// from client.cpp


	// cegui
	if (1) {
		mGUIRenderer = new CEGUI::OgreCEGUIRenderer(mWindow, Ogre::RENDER_QUEUE_OVERLAY, true, 3000,mSceneMgr);
	//Ogre::ST_EXTERIOR_REAL_FAR);

		mGUISystem = new CEGUI::System(mGUIRenderer);

		CEGUI::Logger::getSingleton().setLoggingLevel(CEGUI::Informative);
		CEGUI::SchemeManager::getSingleton().loadScheme((CEGUI::utf8*)"TaharezLook.scheme");
		mGUISystem->setDefaultMouseCursor((CEGUI::utf8*)"TaharezLook", (CEGUI::utf8*)"MouseArrow");
		mGUISystem->setDefaultFont((CEGUI::utf8*)"Tahoma-12");

		mWindowManager = &CEGUI::WindowManager::getSingleton();

		mSheet = mWindowManager->createWindow((CEGUI::utf8*)"DefaultWindow", (CEGUI::utf8*)"root");
        //mSheet = CEGUI::WindowManager::getSingleton().loadWindowLayout((CEGUI::utf8*)"ogregui.layout");
		mGUISystem->setGUISheet(mSheet);

		//cSimpleGui::GetSingleton().SetWndManagerAndParent(mWindowManager,mSheet);

		//cSimpleGui::GetSingleton().Parse(sXml);
		//cSimpleGui::GetSingleton().Parse(sXml);

		mInfoText = (CEGUI::StaticText*)CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticText", (CEGUI::utf8*)"infoText");
		mSheet->addChildWindow(mInfoText);
		mInfoText->setPosition(CEGUI::Point(0.0f, 0.9f));
		mInfoText->setSize(CEGUI::Size(1.0f, 0.1f));
		mInfoText->setAlpha(0.7f);
		mInfoText->setHorizontalAlignment(CEGUI::HA_LEFT);
		mInfoText->setVerticalAlignment(CEGUI::VA_TOP);
		// init ogre ? scene ?

		class cCEGUIEventPump : public cListener { public:
			enum {
				kEventBinding_InputEvent,
				kEventBinding_EveryFrame,
			};
			virtual	void	Listener_Notify (cListenable* pTarget,const size_t eventcode = 0,const size_t param = 0,const size_t userdata = 0) {
				static int lastmousex = 0;
				static int lastmousey = 0;
				switch (userdata) {
					case kEventBinding_EveryFrame:	// mousemovement
						CEGUI::System::getSingleton().injectMouseMove(
								cInput::iMouse[0] - lastmousex,
								cInput::iMouse[1] - lastmousey);
						lastmousex = cInput::iMouse[0];
						lastmousey = cInput::iMouse[1];
					break;
					case kEventBinding_InputEvent:
					break;
				}
			}
		};
		cCEGUIEventPump* pump = new cCEGUIEventPump();
		cInput::GetSingleton().RegisterListener(pump,(int)cCEGUIEventPump::kEventBinding_InputEvent);
		cTimer::GetSingletonPtr()->RegisterFrameIntervalListener(pump,0,(int)cCEGUIEventPump::kEventBinding_EveryFrame);
		CEGUI::System::getSingleton().injectMousePosition(0,0);

		// CEGUI::Logger::getSingleton().setLoggingLevel(CEGUI::Informative); // add me ? found in olm
		/* eventhandling notes :
			see olm.cpp : setupEventHandlers()
			see olm.cpp : setupEnterExitEvents()
			see olm.cpp : setupLoadedLayoutHandlers()
			see olmframelistener.cpp : convertOgreButtonToCegui()
			see olmframelistener.cpp : mouseMoved() and following for injecting events into cegui
		*/
	}

	
	// clean up ogre ?

	/*
	if (mEditorGuiSheet) {
		CEGUI::WindowManager::getSingleton().destroyWindow(mEditorGuiSheet);
	}
	if (mGUISystem) {
		delete mGUISystem;
		mGUISystem = 0;
	}
	if(mGUIRenderer) {
		delete mGUIRenderer;
		mGUIRenderer = 0;
	}*/
	
		
		
	#include <OgreCEGUIRenderer.h>

	#include <CEGUI/CEGUIImageset.h>
	#include <CEGUI/CEGUISystem.h>
	#include <CEGUI/CEGUILogger.h>
	#include <CEGUI/CEGUISchemeManager.h>
	#include <CEGUI/CEGUIWindowManager.h>
	#include <CEGUI/CEGUIWindow.h>
	#include <CEGUI/CEGUIXMLParser.h>
	#include <CEGUI/CEGUIXMLHandler.h>

	#include <CEGUI/elements/CEGUICombobox.h>
	#include <CEGUI/elements/CEGUIListbox.h>
	#include <CEGUI/elements/CEGUIListboxTextItem.h>
	#include <CEGUI/elements/CEGUIPushButton.h>
	#include <CEGUI/elements/CEGUIScrollbar.h>
	#include <CEGUI/elements/CEGUIStaticImage.h>
	#include <CEGUI/elements/CEGUIStaticText.h>
	#include <CEGUI/elements/CEGUIEditbox.h>
	#include <CEGUI/elements/CEGUIMultiLineEditbox.h>
	#include <CEGUI/elements/CEGUIFrameWindow.h>




	//CEGUI::Scrollbar* mRed;
	//CEGUI::Scrollbar* mGreen;
	//CEGUI::Scrollbar* mBlue;
	//CEGUI::StaticImage* mPreview;
	//CEGUI::Window* mTip;
	//CEGUI::Listbox* mList;
	//CEGUI::Window* mEditBox;
	//typedef std::map<CEGUI::String, CEGUI::String> DescriptionMap;
	//DescriptionMap mDescriptionMap;

	//GuiFrameListener* mFrameListener;



	void addLine(CEGUI::Window *Window,const char *sLine){ PROFILE
			ASSERT(Window && sLine);
			CEGUI::String s = Window->getText();
			if(s.size()>0)s = s+"\n"+sLine;
			else s = sLine;
			Window->setText(s);
	}


	void cClient::ShowMessage	(const char *sText){ PROFILE
		if(mInfoText)addLine(mInfoText,sText);
	}
	
	// constructor ::
	//,mGUIRenderer(0)
	//,mGUISystem(0)
	//,mEditorGuiSheet(0)

	CEGUI::WindowManager *mWindowManager;
	CEGUI::OgreCEGUIRenderer* mGUIRenderer;
	CEGUI::System* mGUISystem;
	CEGUI::Window* mSheet;
	CEGUI::StaticText* infoText;
// from main.cpp


	#include <CEGUI/CEGUIImageset.h>
	#include <CEGUI/CEGUISystem.h>
	#include <CEGUI/CEGUILogger.h>
	#include <CEGUI/CEGUISchemeManager.h>
	#include <CEGUI/CEGUIWindowManager.h>
	#include <CEGUI/CEGUIWindow.h>
	#include <CEGUI/elements/CEGUICombobox.h>
	#include <CEGUI/elements/CEGUIListbox.h>
	#include <CEGUI/elements/CEGUIListboxTextItem.h>
	#include <CEGUI/elements/CEGUIPushButton.h>
	#include <CEGUI/elements/CEGUIScrollbar.h>

	#include <CEGUI/elements/CEGUIStaticImage.h>
	#include "OgreCEGUIRenderer.h"
	#include "OgreCEGUIResourceProvider.h"
	
	
// from scripting.cpp

	static int l_CEGUI_SetMouseCursor			(lua_State *L) { PROFILE CEGUI::System::getSingleton().setDefaultMouseCursor((CEGUI::utf8*)luaL_checkstring(L, 1), (CEGUI::utf8*)luaL_checkstring(L, 2));return 0; }
	static int l_CEGUI_InjectKeyDown			(lua_State *L) { PROFILE CEGUI::System::getSingleton().injectKeyDown(cInput::GetSingleton().KeyConvertOgreInv(luaL_checkint(L,1)));return 0; }
	static int l_CEGUI_InjectKeyUp				(lua_State *L) { PROFILE CEGUI::System::getSingleton().injectKeyUp(  cInput::GetSingleton().KeyConvertOgreInv(luaL_checkint(L,1)));return 0; }
	static int l_CEGUI_InjectChar				(lua_State *L) { PROFILE CEGUI::System::getSingleton().injectChar(luaL_checkint(L,1));return 0; }
	static int l_CEGUI_InjectMouseButtonDown	(lua_State *L) { PROFILE CEGUI::System::getSingleton().injectMouseButtonDown(RobKeyCode2CEGUIMouseButton(luaL_checkint(L,1)));return 0; }
	static int l_CEGUI_InjectMouseButtonUp		(lua_State *L) { PROFILE CEGUI::System::getSingleton().injectMouseButtonUp(  RobKeyCode2CEGUIMouseButton(luaL_checkint(L,1)));return 0; }


	CEGUI::MouseButton	RobKeyCode2CEGUIMouseButton (const int i) { PROFILE
		switch (i) {
			case cInput::kkey_mouse1:	return CEGUI::LeftButton;
			case cInput::kkey_mouse2:	return CEGUI::RightButton;
			case cInput::kkey_mouse3:	return CEGUI::MiddleButton;
			default : 					return CEGUI::NoButton;
		}
	}

	
	lua_register(L,"CEGUI_InjectKeyDown",	l_CEGUI_InjectKeyDown);
	lua_register(L,"CEGUI_InjectChar",		l_CEGUI_InjectChar);
	lua_register(L,"CEGUI_InjectKeyUp",		l_CEGUI_InjectKeyUp);
	lua_register(L,"CEGUI_InjectMouseButtonDown",	l_CEGUI_InjectMouseButtonDown);
	lua_register(L,"CEGUI_InjectMouseButtonUp",		l_CEGUI_InjectMouseButtonUp);
	lua_register(L,"CEGUI_SetMouseCursor",			l_CEGUI_SetMouseCursor);
	
// from makefile.am
	
INCLUDES = -I/usr/include @openal_CFLAGS@ @freealut_CFLAGS@ @lua50_CFLAGS@ @lualib50_CFLAGS@ @OGRE_CFLAGS@ @CEGUI_CFLAGS@ @vorbis_CFLAGS@ @vorbisfile_CFLAGS@

olm_LDFLAGS= -lSDL @openal_LIBS@ @freealut_LIBS@ @lua50_LIBS@ @lualib50_LIBS@ @OGRE_LIBS@ @CEGUI_LIBS@ -lCEGUIBase @vorbis_LIBS@ @vorbisfile_LIBS@