	lib.main.lua
		if (false) then
				wnd_name = GUICreateNameTeamDialog()
		end
		
	lib.client.lua ...	
	elseif (msgtype == MsgType("S_CreateTeam")) then
		local teamid = arg[1]
		table.insert(client.lTeam,teamid,{id=teamid})
		printf("new Team joined: id=%d\n",teamid)
		
		-- TODO : uses old gui system, search and destroy
		--if (wnd_name > 0) then
		--		local name = Client_GUIGetText(wnd_name,"name")
		--		Client_GUICloseWindow(wnd_name)
		--		wnd_name = GUICreateNameTeamDialog()
		--		Client_GUISetText(wnd_name,"name",name)
		--end
	end	
	
	lib.hud.lua
hud.buybutton.onMouseClick = function (self) 
	print("buy button clicked") 
	wnd_buy = Client_GUIParse("<window text='buy' closeable=1 x=100 y=100>" .. 
		"<rows>" ..
				"<row>" ..
						"<cols>" ..
							"<col><text text='Energy Blaster' /></col>" ..
							"<col><text text='500$' /></col>" ..
							"<col><button id=buy_0 text=buy /></col>" ..
							"<col><button id=sell_0 text=sell /></col>" ..
						"</cols>" ..
				"</row>" ..
				"<row>" ..
						"<cols>" ..
							"<col><text text='Rocket Launcher' /></col>" ..
							"<col><text text='800$' /></col>" ..
							"<col><button id=buy_1 text=buy /></col>" ..
							"<col><button id=sell_1 text=sell /></col>" ..
						"</cols>" ..
				"</row>" ..
				"<row>" ..
						"<cols>" ..
							"<col><text text='Plasma Blaster' /></col>" ..
							"<col><text text='1000$' /></col>" ..
							"<col><button id=buy_2 text=buy /></col>" ..
							"<col><button id=sell_2 text=sell /></col>" ..
						"</cols>" ..
				"</row>" ..
				"<row>" ..
					"<button id=exit text=exit />" ..
				"</row>" ..
		"</rows>" ..
		"</window>")
end

lib.gui.lua
function GUICreateNameTeamDialog ()
		local wnd
		
		local teams = "<row><button id=team_0 text='new team' /></row>"
		for k,v in pairs(client.lTeam) do
				teams = teams .. "<row><button id=team_" .. k .. " text='join team " .. k .. "' /></row>";
		end
		
		wnd = Client_GUIParse("<window text='choose name and team' closeable=0 x=100 y=100>" .. 
			"<rows>" ..
			"<row><text text='name:' /></row>" ..
			"<row><edit id=name /></row>" ..
			"<row><text text='team:' /></row>" ..
			teams ..
			"</rows>" ..
			"</window>")
		-- Client_GUICloseWindow(wnd_name)
		return wnd
end



-- simple gui button closed callback
function GUIEventClose (handle)
		print("GUIEventClose",handle)
		if (wnd_buy == handle) then
				wnd_buy = 0
		end
		if (wnd_name == handle) then
				wnd_name = 0
		end
end


-- simple gui button click callback
function GUIEventClick (handle,id) -- TODO : OBSOLETE ??????
		print("GUIEventClick",handle,id)
		if (handle == wnd_name) then
				local name = Client_GUIGetText(wnd_name,"name")
				print("+++++++++++++++++++++++",s)
				if (string.len(name) > 0) then
						printf("player set name: %s\n",name)
						local team
						local l = strsplit("_",id)
						team = l[2]
						Client_GUICloseWindow(wnd_name)
						wnd_name = 0
						hud.SetOwnName(s)
						client.SendMsgToServer(MsgType("C_SetName"),name)
						client.SendMsgToServer(MsgType("C_SetTeam"),tonumber(team))
				end
		end
		if (handle == wnd_buy) then
				if (id == "exit") then
						Client_GUICloseWindow(wnd_buy)
						wnd_buy = 0
				end
				local l = strsplit("_",id)
				local l_action = l[1]
				local l_id = l[2]
				print("action: ",l_action," id: ",l_id)
				if(l_action == "buy" and l_id == "0") then
						client.SendMsgToServer(MsgType("C_BuyWeapon"))
				end
				if(l_action == "sell" and l_id == "0") then
						client.SendMsgToServer(MsgType("C_SellWeapon"))
				end
		end
end


wnd_buy = 0
wnd_name = 0
