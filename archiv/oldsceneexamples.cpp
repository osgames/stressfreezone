
	if (0) {
		Real e = 20;
		GhoulPrimitive::OgreBeam* mybeam = new GhoulPrimitive::OgreBeam();

		mybeam->mbCapEnds = true;
		mybeam->mfCapLenStart = 50;	/// positive means outside [start;end], negative means within
		mybeam->mfCapLenEnd = 50;
		mybeam->mfCapTexCoordLen_Start = 0.3;
		mybeam->mfCapTexCoordLen_End = 0.3;
		mybeam->mfWidth = 20;
		mybeam->mvStart = Vector3(0,0,0);
		mybeam->mvEnd = Vector3(800,800,800);
		mybeam->setMaterial("bullet");
		//mybeam->setMaterial("ghoulgreen");
		SceneNode *linesNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("bullettest01");
		linesNode->attachObject(mybeam);
	}

	if (0) {
		SceneNode *trailNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("trailnode");
		trailNode->setPosition(0.0f,0.0f,0.0f);
		RibbonTrail *trail = mSceneMgr->createRibbonTrail("trail");
		trail->addNode(trailNode);
		trail->setTrailLength(500);
		trail->setMaxChainElements(100);
		trail->setMaterialName("Examples/LightRibbonTrail");
		//trail->setInitialColour(0,0.0f,1.0f,0.0f);
		//trail->setColourChange(0,0.0f,1.0f,0.0f,0.0f);
		//trail->setInitialWidth(0,0.5f);
		mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(trail);
	}
	
	
	// in step ... obsolete
	if(0){
		SceneNode *trail = mSceneMgr->getSceneNode("trailnode");
		if(trail){
			trail->translate(0.2f,0.0f,0.0f);
			Vector3 p = trail->getPosition();
			//printf("****************** TRAIL POSITION: (%.2f|%.2f|%.2f)\n",p[0],p[1],p[2]);
		}
	}
	
	
//const char *sXml = "<window w=200 h=100 x=10 y=10 onopen=\"Client_ShowMessage('hello world!')\" onclose=\"Client_ShowMessage('bye!')\"><text>please close me</text></window>";
const char *sXml = ""
"<window text='hello world!' w=200 h=400 x=10 y=10>"
"<rows>"
		"<row><text id=text text='please close me' /></row>"
		"<row>"
				"<cols>"
						"<col><button id=cancel text='cancel' /></col>"
						"<col><button text='nop' /></col>"
						"<col><button id=ok text='ok' /></col>"
				"</cols>"
		"</row>"
"</rows></window>";

	
	
	if (0)  {
		mRoot = new Root("plugins.cfg","display.cfg","log.txt");
		RenderSystemList *rsList = mRoot->getAvailableRenderers();
		int c=0;
		bool foundit = false;
		RenderSystem *selectedRenderSystem=0;
		while(c < (int) rsList->size()) {
			selectedRenderSystem = rsList->at(c);
			String rname = selectedRenderSystem->getName();
			if(rname.compare("OpenGL Rendering SubSystem")==0){
				foundit=true;
				break;
			}
			c++;
		}
		if(!foundit) return 0;
		mRoot->setRenderSystem(selectedRenderSystem);

		selectedRenderSystem->setConfigOption("Full Screen","No");
		selectedRenderSystem->setConfigOption("Video Mode","640 x 480 @ 32-bit colour");


		//retrieve the config option map
		ConfigOptionMap comap = selectedRenderSystem->getConfigOptions();

		//and now we need to run through all of it
		ConfigOptionMap::const_iterator start = comap.begin();
		ConfigOptionMap::const_iterator end = comap.end();
		while(start != end){
			String OptionName = start->first;
			String CurrentValue = start->second.currentValue;
			StringVector PossibleValues = start->second.possibleValues;
			int c=0;
			while (c < (int) PossibleValues.size()){
				String OneValue = PossibleValues.at(c);
				c++;
			}
			start++;
		}


		RenderWindow* mWindow;
		mWindow = mRoot->initialise(true, "SFZ");

		if (0) { // alternate
			// initialise root
			mRoot->initialise(false);
			// create main window
			RenderWindow *renderWindow = mRoot->createRenderWindow("Main",320,240,false);
		}
	}