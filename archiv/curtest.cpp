#if 0
#include <Ogre.h>
#include "DynamicLines.h"
#include "GhoulPrimitives.h"

using namespace Ogre;
void	CurTest		(SceneManager* mSceneMgr) {
	if (0) {
		GhoulPrimitive::OgreRenderableDrawer* mydrawer = new GhoulPrimitive::OgreRenderableDrawer();
		mydrawer->setMaterial("ghoulgreen");
		
		
		mydrawer->Prepare(RenderOperation::OT_TRIANGLE_LIST,4,6,true,false);
		
		Real e = 100;
		mydrawer->AddVertex(0,0,0,0,-1,0);
		mydrawer->AddVertex(e,0,0,0,-1,0);
		mydrawer->AddVertex(e,e,0,0,-1,0);
		mydrawer->AddVertex(0,e,0,0,-1,0);
		mydrawer->AddIndex(0);
		mydrawer->AddIndex(1);
		mydrawer->AddIndex(2);
		mydrawer->AddIndex(0);
		mydrawer->AddIndex(2);
		mydrawer->AddIndex(3);
		
		mydrawer->Finish();
		
		
		SceneNode *linesNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("bla");
		linesNode->attachObject(mydrawer);
	}
	if (0) {
		Real e = 20;
		GhoulPrimitive::OgreEllipsoid* mydrawer = new GhoulPrimitive::OgreEllipsoid(Vector3(-3*e,0,-5*e),Vector3(2*e,e,4*e));
		mydrawer->setMaterial("ghoulgreen");
		SceneNode *linesNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("ellipsoid1");
		linesNode->attachObject(mydrawer);
	}
	if (1) {
		Real e = 20;
		GhoulPrimitive::OgreCone* mydrawer = new GhoulPrimitive::OgreCone(Vector3(0,0,0),Vector3(e,e,e)*3.0,e,e,0.0,23,11);
		mydrawer->setMaterial("ghoulgreen");
		SceneNode *linesNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("cone1");
		linesNode->attachObject(mydrawer);
	}
	if (0) {
		GhoulPrimitive::OgreRenderableDrawer* mydrawer = new GhoulPrimitive::OgreRenderableDrawer();
		mydrawer->setMaterial("ghoulgreen");
		
		size_t i = 0;
		
		GhoulPrimitive::Loft myloft;
		Real e = 20;
		Real x = -3*e;
		Real z = -5*e;
		
		size_t segments = 23;
		Real circle3d[2*segments];
		GhoulPrimitive::Primitive::Circle(circle3d,segments,2,e);
		for (i=0;i<segments;++i)
			myloft.AddBasePoint(Vector3(0.0,circle3d[i*2+0],circle3d[i*2+1]));
		//for (i=0;i<segments;++i)
		//	myloft.AddBasePoint(Vector3(0.0,circle3d[(segments-1-i)*2+0],circle3d[(segments-1-i)*2+1]));
		myloft.mAutoBackSide = true;
		
		i=0;
		myloft.AddPathPoint(Vector3(x+3*i++*e,0.0,z),0,0,Vector3::UNIT_SCALE);
		myloft.AddPathPoint(Vector3(x+3*i++*e,1*e,z),0,0,Vector3::UNIT_SCALE,Quaternion(Degree(-30),Vector3::UNIT_Y));
		myloft.AddPathPoint(Vector3(x+3*i++*e,0.0,z),0,0,Vector3::UNIT_SCALE);
		myloft.AddPathPoint(Vector3(x+3*i++*e,0.0,z),0,0,0.1 * Vector3::UNIT_SCALE);
		myloft.Update(*mydrawer,true,false);
		
		SceneNode *linesNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("loft1");
		linesNode->attachObject(mydrawer);
	}
	if (0) {
		GhoulPrimitive::OgreRenderableDrawer* mydrawer = new GhoulPrimitive::OgreRenderableDrawer();
		
		Real source[] = {100*(-1),0,0, 100*(0),0,0, 100*(1),100,0, 100*(2),100,0};
		Real dest[3];
		Real t;
		int i;
		
		//i=0;lines->addPoint(source[i+0],source[i+1],source[i+2]);
		//i=3;lines->addPoint(source[i+0],source[i+1],source[i+2]);
		
		// kInterpolate_Constant , kInterpolate_Linear , kInterpolate_SmoothQuadric
		///size_t vertexCount = 0;
		//for (i=0;i<3;++i) for (t=0.0;t<1.0;t+=0.05) ++vertexCount;
		//mydrawer->Prepare(RenderOperation::OT_LINE_STRIP,vertexCount,0,false,false);
		mydrawer->Prepare(RenderOperation::OT_LINE_STRIP,4,4,false,false);
		
		/*
		for (i=0;i<3;++i) for (t=0.0;t<1.0;t+=0.05) {
			// (kReal* source,Real* dest,kReal t,size_t num,size_t startoff,size_t bufsize,eInterpolationMode mode,size_t stride) {
			//GhoulPrimitive::Primitive::InterpolatateFloatV(source,dest,t,3,3*i,3*4,GhoulPrimitive::kInterpolate_SmoothQuadric);
			//mydrawer->AddVertex(dest[0],dest[1],dest[2]);
			mydrawer->AddVertex(source[i*3+0],source[i*3+1],source[i*3+2]);
		}
		*/
		
		mydrawer->AddVertex(0,0,0);
		mydrawer->AddVertex(0,0,100);
		mydrawer->AddVertex(0,100,0);
		mydrawer->AddVertex(100,0,0);
		mydrawer->AddIndex(0);
		mydrawer->AddIndex(1);
		mydrawer->AddIndex(2);
		mydrawer->AddIndex(3);
		
		mydrawer->Finish();
		
		//i=6;lines->addPoint(source[i+0],source[i+1],source[i+2]);
		//i=9;lines->addPoint(source[i+0],source[i+1],source[i+2]);
		
		SceneNode *linesNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("lines2");
		linesNode->attachObject(mydrawer);
	}
		
}
#endif
