
function OLD_UNUSED () 
	local i
	
	--ServerSpawn(iLocationID,cObjTest,"ogrehead.mesh",0,0,0,100)
	--ServerSpawn(iLocationID,cObjTest,"razor.mesh",0,0,-300,100)
	
	--local ubership = ServerSpawn(iLocationID,cObjShip,"sphereglider/sphereglider.mesh",3000,0,0,200,sprintf("Ray\nUbership"),-1,5000)
	--local spacestation = ServerSpawn(iNebularLocID,cObjShip,"spacestation/spacestation.mesh",3000,3000,0,80,sprintf("Trader\nStation"),0,20000)
	
	
	-- RegisterStepper(ubership.AiShoot,ubership)
	--Bind("k",		function (state) if (state > 0) then print("\n\n\nkaboooom\n\n\n") ubership:ServerDamage(nil,10000) end end)

	if (false and gIsClient > 0) then
		e = 500
		GenerateBeamer(iLocationID,0,0,0, e,0,0)
		GenerateBeamer(iLocationID,0,0,0, 0,e,0)
		GenerateBeamer(iLocationID,0,0,0, 0,0,e)
		--GenerateBeamer(iLocationID,0,0,0, e,e,e)
		GenerateBeamer(iLocationID,e,e,e, -e,0,0)
		GenerateBeamer(iLocationID,e,e,e, 0,-e,0)
		GenerateBeamer(iLocationID,e,e,e, 0,0,-e)
	end
	
	
	if false then
		local bullet = ServerSpawn(iLocationID,cObjBullet,0,0,0,1,0,0,0,-1)
		bullet.mvVel = {0,0,0}
		bullet.iDeathTime = gMyTicks + 10000000
	end
	
	if false then
		local explo = ServerSpawn(iLocationID,cObjExplosion,0,0,0,300)
		explo.iCreateTime = gMyTicks + 10000000
	end

	-- ServerSpawn(iLocationID,cObjAsteroid,"rocket.mesh",0,100,800)
	
	if false then
		for i = 1,100 do
			local trail = ServerSpawn(iLocationID,cObjTrail,0+i*5,0+i*5,0+i*5,1,1000,100);
			trail.mvAccel = { Vector.random3(100) }
		end
	end

	--ServerSpawn(iLocationID,cObjTest,"ogrehead.mesh",0,0,0,50)
	--ServerSpawn(iLocationID,cObjTest,"sphereglider/sphereglider.mesh",0,0,-1200,200)
	
	
	if false then
		print("Creating nebula ships")
		local j
		for j = 1,6 do				
			local a = ServerSpawn(iNebularLocID,cObjAsteroid,math.random(0,3),Vector.random3(1*2000))
			a.mvVel = { Vector.random3(0*30) }
			--local x,y,z
			--x,y,z = Vector.random3(1*2000)
			--ServerSpawn(iNebularLocID,cObjShip,"sphereglider/sphereglider.mesh",x,y,z,200,sprintf("Nebelschiff"),0,5000)
		end
	end
end

-- only works if server and client are local
function GenerateBeamer (iLocationID,x,y,z,vx,vy,vz)
	local beamer = ServerSpawn(iLocationID,cObjBase)
	beamer.mvPos = {x,y,z}
	beamer.mqRot = {Quaternion.lookAt(vx,vy,vz)}
	beamer.gfx:SetBeam(0,0,0,0,0,500,60,50,50,0.3,0.3,"explosion")
end
