#include "prefix.h"
#include <time.h>
#include <SDL/SDL.h>
#include "shell.h"
#include "input.h"

// cShell::GetSingleton().SetProgramCaption("SFZ");
// cShell::GetSingleton().SetVideo(320,240,16,bFullscreen);
		
/*
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <SDL/SDL_image.h>
*/

// ****** ****** ****** cShell


size_t	cShell::miStartTime = 0;


void	cShell::Init			(const int iArgC, char **pszArgV) { PROFILE
	assert(!mbInitialized);
	mbAlive = true;
	mbInitialized = true;
	miArgC = iArgC;
	mpszArgV = pszArgV;

	// init random
	srand(time(NULL));

	// for sdl input hack see
	// http://localhost/staticogrewiki/u/s/i/Using_SDL_Input_5c21.html

    // init SDL library
    //if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_AUDIO) < 0) {
	//if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_AUDIO) < 0) {
	if (SDL_Init(SDL_INIT_TIMER) < 0) { // still need some form of timer which runs without full ogre init for dedicated server...
		printf("Couldn't initialize SDL: %s",SDL_GetError());
		exit(-1);
	}

	miStartTime = 0;
	// miStartTime = SDL_GetTicks();

	/*
	// init SDL OpenGL  todo: check if this is perhaps only for 16bpp?
    SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );
    SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 5 );
    SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );
    SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

	// init SDL Keys
	SDL_EnableUNICODE(1);
	*/
	//SDL_EnableKeyRepeat(200,30); // SDL_DEFAULT_REPEAT_DELAY,SDL_DEFAULT_REPEAT_INTERVAL

    // Clean up on exit
    atexit(SDL_Quit);
}

void	cShell::DeInit			() { PROFILE
	assert(mbInitialized);
	mbInitialized = false;
}

size_t	cShell::GetTicks		() { return SDL_GetTicks() - miStartTime; }

// EventLoopStep : one step in the eventloop, process all system events in queue, obsolete
//bool	cShell::EventLoopStep	() { }




#if 0
// code from /mnt/hda7/3dcode/archiv/joe3/project/source/shell/shell.cpp

#ifndef WIN32
	#include <sys/types.h>
	#include <dirent.h>
#endif

// returns a \n seperated list of all files in path
// "camouflage.png\ndata.txt\ndryforestground.jpg\ndryground.jpg\ndryleaves.jpg\n"
char*	cShell::ListFiles	(const char* path)
{ PROFILE
	cString pattern = path;
	cString result = "";
	pattern.Appendf("/*");
	// path without ending / : "data/maps"

	#ifdef WIN32

		WIN32_FIND_DATA finddata;
		HANDLE search = FindFirstFile(*pattern,&finddata);

		if (search == INVALID_HANDLE_VALUE) return 0;

		if ((finddata.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
			result.Appendf("%s\n",finddata.cFileName);

		while (FindNextFile(search,&finddata))
		{
			if ((finddata.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
				result.Appendf("%s\n",finddata.cFileName);
		}

		return strdup(*result);
	#else
		DIR *d;
		struct dirent *e;

		d = opendir(path);
		e = readdir(d);
		while ( e != NULL ) {
			if(e->d_type != DT_DIR)result.Appendf("%s\n",e->d_name);
			e = readdir(d);
		}
		closedir(d);

		return strdup(*result);
	#endif

}

// returns a \n seperated list of all dirs in path
// WARNING! also returns ../ and ./
// "..\n.\nmydir\nmydir2\n"
char*	cShell::ListDirs	(const char* path)
{ PROFILE
	cString pattern = path;
	cString result = "";
	pattern.Appendf("/*");
	// path without ending / : "data/maps"

	#ifdef WIN32

		WIN32_FIND_DATA finddata;
		HANDLE search = FindFirstFile(*pattern,&finddata);

		if (search == INVALID_HANDLE_VALUE) return 0;

		if ((finddata.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
			result.Appendf("%s\n",finddata.cFileName);

		while (FindNextFile(search,&finddata))
		{
			if ((finddata.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
				result.Appendf("%s\n",finddata.cFileName);
		}

		return strdup(*result);

	#endif// WIN32

	// TODO : implement me for linux !
}



#endif



#if 0
	// FROM HERE ON IS ANCIENT CODE, just kept as notes

	// SetVideo : set resoulution and fullscreen/windowed mode
	int		cShell::SetVideo		(int iCX,int iCY,int iBPP,bool bFullscreen) {
		SDL_Surface* screen;
		int iFlags;

		// if iBPP is 0, use current colordepth
		if (iBPP == 0)
		{
			const SDL_VideoInfo* info;
			info = SDL_GetVideoInfo();
			if (!info)
			{
				printf("Video query failed: %s",SDL_GetError());
				iBPP = 16;
			} else iBPP = info->vfmt->BitsPerPixel;
		}

		// fullscreen or windowed mode
		if (bFullscreen)
				iFlags = SDL_OPENGL | SDL_FULLSCREEN;
		else	iFlags = SDL_OPENGL | SDL_SWSURFACE;

		// set video mode
		screen = SDL_SetVideoMode(iCX, iCY, iBPP, iFlags);
		if (screen == NULL)
		{
			printf("Couldn't set %ix%ix%i video mode: %s",iCX,iCY,iBPP,SDL_GetError());
			return 0;
		}

		// store the current resolution in shell object
		iXRes = iCX;
		iYRes = iCY;

		return 1;
	}

	// SetProgramIcon : load an icon for window and taskbar
	void	cShell::SetProgramIcon		(char* szPath) {
		SDL_Surface* icon = IMG_Load(szPath);
		SDL_WM_SetIcon(icon,NULL);
		SDL_FreeSurface(icon);
	}

	// SetProgramCaption : set window title
	void	cShell::SetProgramCaption	(char* szCaption) {
		SDL_WM_SetCaption(szCaption,NULL);
	}

	// ShowCursor : show/hide cursor
	void	cShell::ShowCursor	(bool bVisible) {
		if (bVisible)
				SDL_ShowCursor(SDL_ENABLE);
		else	SDL_ShowCursor(SDL_DISABLE);
	}
#endif
