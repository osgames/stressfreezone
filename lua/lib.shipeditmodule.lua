-- handles modules in ship editor
-- see also lib.shipedit.lua

gShipPartTypes = CreateTypeList()
function RegisterShipPartType (...) return gShipPartTypes:RegisterType(unpack(arg)) end

gShipEditModules = {}
gShipEditModulesNextID = 1

gModulePrototype = {}
function gModulePrototype:Destroy () 
	if (gShipEditSelectedModule == self) then SelectShipModule(nil) end
	gShipEditModules[self.id] = nil
	if (self.gfx) then self.gfx:Destroy() self.gfx = nil end
end
function gModulePrototype:SetVisible	(bVisible) if (self.gfx) then self.gfx:SetVisible(bVisible) end end

function gModulePrototype:BBoxIntersectsPoint (x,y,z) return BBoxIntersectPoint(x,y,z,self:GetCachedGridBounds()) end

-- returns px,py,pz, qw,qx,qy,qz, mx,my,mz
function gModulePrototype:GetMirRotPosCombo ()
	local px,py,pz 		= self.gfx:GetDerivedPosition()
	local qw,qx,qy,qz 	= self.gfx:GetDerivedOrientation()
	local mx,my,mz 		= ScaleToMirror(self.gfx:GetScale())
	return px,py,pz, qw,qx,qy,qz, mx,my,mz
end

-- returns x1,y1,z1, x2,y2,z2  in absolute coords, rounded to grid positions, cached
function gModulePrototype:GetCachedGridBounds () 
	if (not self.cache_gridbounds) then self:UpdateGeomCache() end
	return unpack(self.cache_gridbounds) 
end						

-- local midx,midy,midz = module:GetCachedGridMiddle()
function gModulePrototype:GetCachedGridMiddle () 
	local minx,miny,minz, maxx,maxy,maxz = self:GetCachedGridBounds()
	return 0.5*(minx+maxx),0.5*(miny+maxy),0.5*(minz+maxz)
end

-- returns x1,y1,z1, x2,y2,z2  in absolute coords, rounded to grid positions
function gModulePrototype:GetGridBounds	()
	local sx,sy,sz 		= self.gfx:GetScale()
	local x,y,z 		= self.gfx:GetDerivedPosition()
	local qw,qx,qy,qz 	= self.gfx:GetDerivedOrientation()
	local t 			= self.shipparttype
	local x2,y2,z2 		= ApplyMirRotCombo(t.cx,t.cy,t.cz, qw,qx,qy,qz, sx,sy,sz) 
	return CorrectGridBounds(x,y,z, x+x2,y+y2,z+z2)
end


-- corrects bounds and rounds them back to the grid to avoid rounding errors
function CorrectGridBounds (x1,y1,z1, x2,y2,z2)
	local x1,y1,z1, x2,y2,z2 = CorrectBounds(x1,y1,z1, x2,y2,z2)
	return roundmultiple(x1,y1,z1, x2,y2,z2)
end





-- mx,my,mz should be 1 or -1,  keeps boundingbox
function gModulePrototype:MirrorInBounds	(mx,my,mz) 
	local x1,y1,z1,x2,y2,z2 = self:GetGridBounds()
	self:Mirror(mx,my,mz)
	local x3,y3,z3,x4,y4,z4 = self:GetGridBounds()
	self:Move(x1-x3,y1-y3,z1-z3)
end

function gModulePrototype:RotateAroundPoint (x,y,z, qw,qx,qy,qz)
	--local px,py,pz = self.gfx:GetDerivedPosition()
	--x,y,z = x-px,y-py,z-pz
	--local x2,y2,z2 = Quaternion.ApplyToVector(x,y,z, qw,qx,qy,qz) 
	--self:Move(roundmultiple(x-x2,y-y2,z-z2))
	
	--self:Rotate(qw,qx,qy,qz)
	--local x3,y3,z3,x4,y4,z4 = self:GetGridBounds()
	--self:Move(roundmultiple(x-x3,y-y3,z-z3))
	
	
	local x1,y1,z1,x2,y2,z2 = self:GetGridBounds()
	self:Rotate(qw,qx,qy,qz)
	local x3,y3,z3,x4,y4,z4 = self:GetGridBounds()
	self:Move(x1-x3,y1-y3,z1-z3)
end

-- mx,my,mz should be 1 or -1,  does NOT keep boundingbox
function gModulePrototype:Mirror	(mx,my,mz) 
	local sx,sy,sz = self.gfx:GetScale()
	self.gfx:SetScale(sx*mx,sy*my,sz*mz)
	self:UpdateGeomCache()
end
		
		
function gModulePrototype:Move	(addx,addy,addz) 
	local x,y,z = self.gfx:GetPosition()
	self.gfx:SetPosition(x+addx,y+addy,z+addz)
	self:UpdateGeomCache()
end

function gModulePrototype:Rotate (qw,qx,qy,qz)
	local aw,ax,ay,az = self.gfx:GetOrientation()
	self.gfx:SetOrientation(Quaternion.Mul(aw,ax,ay,az,qw,qx,qy,qz))
	self:UpdateGeomCache()
end

-- keeps transformed geometry data up to date to avoid having to calculate it in guess
function gModulePrototype:UpdateGeomCache ()
	local t = self.shipparttype
	self.cache_absgeom = GeomApplyScaleMirRotPosCombo(t.gfx_geom, t.cx,t.cy,t.cz,  self:GetMirRotPosCombo())
	self.cache_absgeom_nodiag = GeomGetAxisAlignedSides(self.cache_absgeom)
	self.cache_gridbounds = {self:GetGridBounds()}
end

-- return facenum,dist
function gModulePrototype:RayPick		(rx,ry,rz, rvx,rvy,rvz) 	
	local bhit,hitdist,facenum = self.gfx:RayPick(rx,ry,rz,rvx,rvy,rvz) 
	return bhit and facenum,bhit and hitdist
end

-- vPos as table {x,y,z}
-- qRot as table {w,x,y,z}
function SpawnShipEditModule (shipparttype_or_name_or_id,shippartskin_or_name_or_id,vPos,qRot,mx,my,mz)
	local module = CopyArray(gModulePrototype)
	
	-- id
	module.id = gShipEditModulesNextID
	gShipEditModulesNextID = gShipEditModulesNextID + 1
	gShipEditModules[module.id] = module
	
	-- determine type
	module.shipparttype = gShipPartTypes:Get(shipparttype_or_name_or_id)
	module.shippartskin = nil
	assert(module.shipparttype,"ShipPartType not found : "..tostring(shipparttype_or_name_or_id))

	-- gfx
	module.gfx = module.shipparttype:constructor()
	module.gfx:SetPosition(unpack(vPos))
	module.gfx:SetOrientation(unpack(qRot))
	module:Mirror(mx,my,mz)
	return module
end

