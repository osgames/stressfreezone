-- objects can have containers
-- e.g. a container for all cargo, 
-- and one container for each weaponslot...
-- the content of a container is an unsorted list, e.g. stuff that does not have a position
-- contents can be either individual object instances or pairs of objecttype+amount  , e.g. for cargo
-- warning : containers are NOT used for things like hangar and interior where objects/contents have positions, see location for that
-- todo : crew would also be managed by location, see lib.object.hangar.lua
-- container system is used for stuff like equipment- and weapon-slots,items,inventory,cargo..

--[[
		stuff inside a container is unsorted, each weapon slot is a single seperate container
		

		dragdrop from container to location : need position, container system is not suited for locations
		problem : container elements : pair(typeid,amount),objects
		
		location with pos stuff not via container ?
		
		
		slots/container id's important for netmessages, cargo also ?
		container
			move item between containers : each slot is container, 
			can move items between cargo container of ship a and ship b.
			hangar = container for fighters 
		what  (1 or amount)
			* cargo (normal and mission-related)
			* furniture ? crew ?
			* passengers
			* docked fighters
			* weapons
			* upgrades (hull,shield,engine)
			* equipment : stuff with "use" button
		slots/positions
			weapons : slots, furniture : positions, cargo : plain amount, or diablo-like-grid layout ?
		combine with savegame system ?
		use for resyncing with player
		TODO : AddWeapon : weapon infos zum client uebertragen -> equipment dialog
		equipment/container system so buying and equip/unequip work correctly (furniture cargo : spawn object)
		
	global containerlist with global containerids to avoid pairs (objid,containerid) when adressing
	object must destroy its containers on destruction
	server spawns containers with objects (containerlist in objecttype ? or on demand creation ?)
		cannot spawn on demand, otherwise client cannot request moving stuff to nonexistent containers 
		without requesting creation first : that would be very ugly
	client must be able to list containers associated with an object
	server must be able to determine the object associated with a container
	
	TODO : when an object-instance is added to the containercontents, the container should be notified when it is destroyed
	TODO : gObjectPrototype:AddWeapon updates equipment dialog
]]--




gContainerTypes = CreateTypeList()

function RegisterContainerType 	(...) return gContainerTypes:RegisterType(unpack(arg)) end

function GetContainerType (containertype_or_name_or_id) return gContainerTypes:Get(containertype_or_name_or_id) end

gContainersByID = {}
gNextContainerID = 1

gContainerPrototype = {}

function GetContainer			(container_or_id) 
	if (type(container_or_id) == "number") then return GetContainerByID(container_or_id) end
	return container_or_id
end

function GetContainerByID		(id) return gContainersByID[id] end

-- called by both client and server, id param can be left out if on server, will be auto-incremented
function SpawnContainer (containertype_or_name_or_id,object_or_id,id)
	local container = {}
	ArrayOverwrite(container,gContainerPrototype)
	
	-- determine type
	container.containertype = GetContainerType(containertype_or_name_or_id)
	if (not container.containertype) then print("warning, containertype not found:",containertype_or_name_or_id) end
	--print("SpawnContainer",containertype_or_name_or_id,object_or_id,id)
	
	-- id
	if (not id) then id = gNextContainerID gNextContainerID = gNextContainerID + 1 end
	container.id = id
	gContainersByID[container.id] = container
	
	-- init empty contents
	container.content = {}
	
	-- associate with object
	local obj = GetObject(object_or_id)
	if (obj) then
		container.obj = obj
		local containers_bytype = obj.containers_bytype
		if (not containers_bytype) then containers_bytype = {} obj.containers_bytype = containers_bytype end
		local containerlist = containers_bytype[container.containertype]
		if (not containerlist) then containerlist = {} containers_bytype[container.containertype] = containerlist end
		table.insert(containerlist,container)
		obj:AddToDestroyList(container)
	end
	
	-- broadcast spawn
	if (gbServerRunning) then for id,player in pairs(gServerPlayers) do container:ServerSendSpawn(player.con) end end
	
	NotifyListener("Hook_Container_Spawn",container)
	return container
end

--- sends spawn message
function gContainerPrototype:ServerSendSpawn (con) 
	SendNetMessage(con,kNetMessage_SpawnContainer,self.containertype.type_id,self.obj,self.id)			
end

gMessageTypeHandler_Client_Remote.kNetMessage_SpawnContainer = function (containertype_id,parentobj,new_id)
	SpawnContainer(containertype_id,parentobj,new_id)
end

function PlayerJoined_SendContainers(newplayer)
	-- send all containers
	for k,container in pairs(gContainersByID) do 
		container:ServerSendSpawn(newplayer.con)
		container:ServerSendCompleteContents(newplayer.con) -- send stuff within container
	end
end

function gContainerPrototype:GetType	()	return self.containertype	end
function gContainerPrototype:GetID		()	return self.id				end

function gContainerPrototype:Destroy ()
	if (gbServerRunning) then ServerBroadcastMessage(kNetMessage_DestroyContainer,self.id) end
	NotifyListener("Hook_Container_Destroy",self)
	gContainersByID[self.id] = nil -- delete from arr
end

gMessageTypeHandler_Client_Remote.kNetMessage_DestroyContainer = function (id)
	local container = GetContainer(id)
	if (container) then container:Destroy() end
end




-- ##### ##### ##### ##### ##### TypeContent


function gContainerPrototype:SetContainerTypeContent	(objecttype_or_name_or_id,amount) 
	local t = GetObjectType(objecttype_or_name_or_id) assert(t)
	self.content[t] = amount
	if (gbServerRunning) then ServerBroadcastMessage(kNetMessage_SetContainerTypeContent,self.id,t.type_id,amount) end
	NotifyListener("Hook_Object_SetContainerContent",self,t,amount)
end

gMessageTypeHandler_Client_Remote.kNetMessage_SetContainerTypeContent = function (containerid,typeid,amount)
	local container = GetContainer(containerid)
	if (container) then container:SetContainerTypeContent(typeid,amount) end
end

function gContainerPrototype:GetContainerTypeContent	(objecttype_or_name_or_id) 
	local t = GetObjectType(objecttype_or_name_or_id) assert(t)
	return self.content[t] or 0
end


function gContainerPrototype:GetFirstNonZero	() 
	for objtype,amount in pairs(self.content) do if (amount > 0) then return objtype,amount end end
end

--~ 				for objtype,amount in pairs(container.content) do 
--~ 					if (objtype.equipment_add_energyrate or 0) < 0 and amount>0 then
--~ 						return container,objtype
--~ 					end
--~ 				end


-- ##### ##### ##### ##### ##### rest

-- use num=3 if you want the third container of a specified type, defaults to 1.
function gObjectPrototype:GetContainerByType	(containertype_or_name_or_id,num)
	if (not self.containers_bytype) then return end
	local containers_bytype = self.containers_bytype[GetContainerType(containertype_or_name_or_id)]
	assert(containers_bytype)
	return containers_bytype[num or 1]
end

function gObjectPrototype:GetContainerListByType	() return self.containers_bytype end

function gObjectPrototype:ListContainersWithType	(containertype_or_name_or_id)
	if (not self.containers_bytype) then return end
	return self.containers_bytype[GetContainerType(containertype_or_name_or_id)]
end

-- typefieldname=nil : amount*1
-- typefieldname=typefieldname : amount*objtype[typefieldname]
function gContainerPrototype:GetSum (typefieldname) 
	local res = 0
	for objtype,amount in pairs(self.content) do res = res + amount * (typefieldname and (objtype[typefieldname] or 0) or 1) end
	return res
end

-- container_type_filter_fun can be nil to get all


function gObjectPrototype:GetContainerSum (typefieldname,container_type_filter_fun) 
	local res = 0
	for containertype,containerlist in pairs(self:GetContainerListByType() or {}) do 
		if ((not container_type_filter_fun) or container_type_filter_fun(containertype)) then
			for k,container in pairs(containerlist) do 
				res = res + container:GetSum(typefieldname)
			end
		end
	end
	return res
end

-- for objecttype,amount in pairs(x:GetContentByType() or {}) do 
function gContainerPrototype:GetContentByType		() return self.content end

-- returns objecttype,amount
function gContainerPrototype:GetFirstContentType	() 
	for objecttype,amount in pairs(self:GetContentByType() or {}) do 
		if (amount > 0) then return objecttype,amount end
	end
end


function gContainerPrototype:GetObject () return self.obj end

-- called when a new player joins the game, send all content
function gContainerPrototype:ServerSendCompleteContents (con) 
	assert(gbServerRunning)
	for objtype,amount in pairs(self.content) do 
		SendNetMessage(con,kNetMessage_SetContainerTypeContent,self.id,objtype.type_id,amount)
	end
end

--function ClientRequestContainerTransfer_ByType	(iContainerID_from,iContainerID_to,objtype,amount) end




