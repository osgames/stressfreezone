-- contacts the central masterserver, a simple php script with a mysql db to manage running games
-- see kMasterServer_* in config.lua.dist

-- ./sfz -masterservertest
function MasterServer_Test ()
	local motd = trim(MasterServer_GetMOTD())
	local gameid = MasterServer_AnnounceGame(kDefaultPort,"ghoulsblade's testgame","normal")
	Client_USleep(500)
	local gamelist = MasterServer_ListGames()
	print("motd",motd)
	print("gameid",gameid)
	print("keepalive",MasterServer_KeepAlive(gameid))
	for k,game in pairs(gamelist) do print("game",unpack(game)) end
	Crash()
end

function MasterServer_Get (command,params,bIgnoreReturnForSpeed) 
	return kMasterServer_Host and command and HTTPGetEx(kMasterServer_Host,kMasterServer_Port,command.."&"..URLEncodeArr(params or {}),bIgnoreReturnForSpeed) 
end

function MasterServer_GetMOTD		()					return MasterServer_Get(kMasterServer_Command_MOTD) end
function MasterServer_KeepAlive		(id)		
	local command = kMasterServer_Command_KeepAlive.."&"..URLEncodeArr({id=id})
	Threaded_HTTPRequest(kMasterServer_Host,kMasterServer_Port,command,false,
		function (answertext,bError) print("MasterServer_KeepAlive sent",answertext,bError) end ) 
--~ 	return MasterServer_Get(kMasterServer_Command_KeepAlive,{id=id},true) 
end
function MasterServer_AnnounceGame	(port,name,flags)	
	return MasterServer_Get(kMasterServer_Command_AnnounceGame,{port=port,name=name,flags=flags,version=gCurrentVersion}) end
function MasterServer_BugReport		(report,note)	
	return MasterServer_Get(kMasterServer_Command_BugReport,{report=report,note=note,version=gCurrentVersion,project="sfz"}) end

-- returns a list of games {{ip,port,name,flags,age},...}
function MasterServer_ListGames		()	
	local res = {}
	local gamelist = strsplit("\n",MasterServer_Get(kMasterServer_Command_ListGames) or "")
	for k,game in pairs(gamelist) do 
		local mygame = strsplit("\t",game)
		if (table.getn(mygame) > 2) then 
			table.insert(res,mygame) 
		end
	end
	return res
end


