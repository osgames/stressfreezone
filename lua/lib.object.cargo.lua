-- object methods related to cargo
-- cargo such as iron, ironore, ....

-- percentage in [0,1]
function gObjectPrototype:GetCargoPercentage	() return (self:GetMaxCargo() > 0) and math.min(1,self:GetCurCargo()/self:GetMaxCargo()) or 0 end

function gObjectPrototype:GetMaxCargo	()	return self:IsAlive() and self.objtype.cargo_max or 0 end
function gObjectPrototype:GetCurCargo	()	
	if (not self:IsAlive()) then return 0 end
	local cont = self:GetCargoContainer()
	return cont and cont:GetSum("weight") or 0
end

function gObjectPrototype:GetCargoSpaceLeft	() return math.max(0,self:GetMaxCargo() - self:GetCurCargo()) end

function gObjectPrototype:GetCargoContainer	() return self:GetContainerByType("containertype/cargo") end

function gObjectPrototype:GetCargoList	() 
	local cont = self:GetCargoContainer()
	return cont and cont:GetContentByType()
end

function gObjectPrototype:GetCargo	(objecttype_or_name_or_id)
	local cont = self:GetCargoContainer()
	return cont and cont:GetContainerTypeContent(objecttype_or_name_or_id) or 0
end

-- does not check cargo capacity, see AddCargoUntilFull for that
function gObjectPrototype:SetCargo	(objecttype_or_name_or_id,amount) 
	local cont = self:GetCargoContainer()
	if (cont) then 
		cont:SetContainerTypeContent(objecttype_or_name_or_id,amount) 
	else
		print("WARNING, SetCargo without container",objecttype_or_name_or_id,amount)
		print(_TRACEBACK())
	end
end




-- does not check bounds
-- amount_delta can be negative to substract cargo
-- amount_delta gets transfered from self to dest_obj
function gObjectPrototype:TransferCargo	(dest_obj,cargotype_name_or_id,amount_delta) 
	self:AddCargo(cargotype_name_or_id,-amount_delta)	
	dest_obj:AddCargo(cargotype_name_or_id,amount_delta)	
end

-- does not check bounds, see AddCargoUntilFull for that
-- amount_delta can be negative to substract cargo
function gObjectPrototype:AddCargo	(cargotype_name_or_id,amount_delta) 
	self:SetCargo(cargotype_name_or_id,self:GetCargo(cargotype_name_or_id) + amount_delta) 
end

-- returns the amount that did not fit, e.g. 0 if all fit, amount if there was no space left
function gObjectPrototype:AddCargoUntilFull	(cargotype_name_or_id,amount)	
	local cargotype = GetObjectType(cargotype_name_or_id) assert(cargotype)
	local amount_taken = math.min(math.floor(self:GetCargoSpaceLeft()/cargotype.weight),amount)
	self:AddCargo(cargotype,amount_taken)
	return amount-amount_taken
end

-- returns the amount that was taken
function gObjectPrototype:RemoveCargoUntilEmpty	(cargotype_name_or_id,amount)	
	local cargotype = GetObjectType(cargotype_name_or_id) assert(cargotype)
	local amount_inside = self:GetCargo(cargotype_name_or_id)
	local amount_taken = math.min(amount_inside,amount)
	self:AddCargo(cargotype,-amount_taken)
	return amount_taken
end

-- division by typelist : cost={["cargo/iron"]=50}
-- used to determine how many times you can afford an offer
function gObjectPrototype:CargoDivList (objtypelist) 
	local res
	for objtypename,amount in pairs(objtypelist) do
		local cur = math.max(0,math.floor(self:GetCargo(objtypename) / amount))
		res = math.min(res or cur,cur)
	end
	return res or 0
end

-- add/sub a typelist * times to cargo : cost={["cargo/iron"]=50}
-- times can be negative for substraction, defaults to 1
function gObjectPrototype:CargoAddList (objtypelist,times) 
	for objtypename,amount in pairs(objtypelist) do
		self:AddCargo(objtypename,amount*(times or 1))
	end
end




function OpenOwnCargoDialog () NotifyListener("Hook_Client_OpenOwnCargoDialog") end

-- jettison
function ClientRequestJettisonCargo (cargotype_or_name_or_id,amount) 
	local cargotype = GetObjectType(cargotype_or_name_or_id)
	ClientSendNetMessage(kNetMessage_RequestJettisonCargo,cargotype.type_id,amount) 
end

-- jettison, fAmount > 0
gMessageTypeHandler_Server.kNetMessage_RequestJettisonCargo = function (player,iCargoTypeID,fAmount) 
	local body = player.body
	if (not body) then return end
	local amount = math.max(0,math.min(body:GetCargo(iCargoTypeID),fAmount))
	-- todo : gfx/sound
	-- todo : add checks if possible do jettison
	local obj = SpawnObject("box",body:GetParentLocation(),body.mvPos,{Quaternion.random()})
	body:TransferCargo(obj,iCargoTypeID,amount)
end


