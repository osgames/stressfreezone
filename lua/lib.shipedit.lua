-- ship editor
-- handles ship-part-types
-- see also common.shipparttypes.lua
-- see also lib.shipeditmodule.lua


kShipEditorHaloWidth 	= 0.01 -- the width of the beamlines
kShipEditorSelectColor 	= {0,1,0,1} -- rgba, green
kShipEditorHitFaceOffset = 0.1 -- offset a little from normal to avoid rounding errors
kInitialShipEditThirdPersonDist = 10
gShipEditorTableCamSpeedFactor = 0.5 * gfDeg2Rad

gbShipEditorRunning = false

gLastPlacePosition = nil


gActiveShipEditTool = nil

--gShipEdit_StartBlockType = "shippart/ramp2"
--gShipEdit_StartBlockType = "shippart/pyramid2"
gShipEdit_StartBlockType = "shippart/box"
--gbShipEditorStartRot = {GetRot90(2,2)}

gActiveShipPartType = gShipEdit_StartBlockType
--gActiveShipPartType = "shippart/box"
--gActiveShipPartType = "shippart/ramp2"
--gActiveShipPartType = "shippart/pyramid2"
--gActiveShipPartType = false

kOldUnusedSkinName = ""



-- returns mindx,maxdx,mindy,maxdy,mindz,maxdz
function ExpandClickedPos (x,y,z,nx,ny,nz,clickedmodule)
	if (not NormalIsAxisAligned(nx,ny,nz)) then AddFadeLines("ExpandClickedPos : normal not axisaligned") return end
	local ax,ay,az,bx,by,bz
	nx,ny,nz = roundmultiple(nx,ny,nz)
	if (math.abs(nx) < 0.9) then
		ax,ay,az = roundmultiple(Vector.cross(nx,ny,nz,Vector.cross(nx,ny,nz,1,0,0)))
	else
		ax,ay,az = roundmultiple(Vector.cross(nx,ny,nz,Vector.cross(nx,ny,nz,0,1,0)))
	end
	bx,by,bz = roundmultiple(Vector.cross(nx,ny,nz,ax,ay,az))
	
	local amin,amax = 0,0
	local bmin,bmax = 0,0
	local e
	
	local midx,midy,midz = clickedmodule:GetCachedGridMiddle()
	while (FindModuleIntersectingPoint(midx+(amin-1)*ax,midy+(amin-1)*ay,midz+(amin-1)*az)) do amin = amin-1 end
	while (FindModuleIntersectingPoint(midx+(bmin-1)*bx,midy+(bmin-1)*by,midz+(bmin-1)*bz)) do bmin = bmin-1 end
	while (FindModuleIntersectingPoint(midx+(amax+1)*ax,midy+(amax+1)*ay,midz+(amax+1)*az)) do amax = amax+1 end
	while (FindModuleIntersectingPoint(midx+(bmax+1)*bx,midy+(bmax+1)*by,midz+(bmax+1)*bz)) do bmax = bmax+1 end
	
	
	mindx = math.min(ax*amin,bx*bmin,ax*amax,bx*bmax)
	maxdx = math.max(ax*amin,bx*bmin,ax*amax,bx*bmax)
	mindy = math.min(ay*amin,by*bmin,ay*amax,by*bmax)
	maxdy = math.max(ay*amin,by*bmin,ay*amax,by*bmax)
	mindz = math.min(az*amin,bz*bmin,az*amax,bz*bmax)
	maxdz = math.max(az*amin,bz*bmin,az*amax,bz*bmax)
--~ 	print(arrdump({"normal ",nx,ny,nz," ",ax,ay,az," ",bx,by,bz}))
--~ 	print(arrdump({"expansion ",mindx,maxdx," ",mindy,maxdy," ",mindz,maxdz}))
	return mindx,maxdx,mindy,maxdy,mindz,maxdz
end

gShipEditTools = {}
	gShipEditTools.Select 	= {material="editor_icon_select", fun=function (module,facenum,x,y,z,dist) SelectShipModule(module) end}
	gShipEditTools.Place	= {material="editor_icon_place", fun=function (module,facenum,x,y,z,dist) 
		if (gActiveShipPartType) then 
		
			local px,py,pz, qw,qx,qy,qz, mx,my,mz = GuessNewModulePosMirRot(gActiveShipPartType,module,facenum,x,y,z)
			if (not px) then return end -- no suitable position found
			
			-- extrude brush
			if (gKeyPressed[key_lcontrol]) then
				local nx,ny,nz = FaceGetNormal(module.gfx,facenum)
				local mindx,maxdx,mindy,maxdy,mindz,maxdz = ExpandClickedPos(x,y,z,nx,ny,nz,module)
				for ax = mindx,maxdx do 
				for ay = mindy,maxdy do 
				for az = mindz,maxdz do 
					if (ax ~= 0 or ay ~= 0 or az ~= 0) then
						local module = SpawnShipEditModule(gActiveShipPartType,kOldUnusedSkinName,{px+ax,py+ay,pz+az},{qw,qx,qy,qz},mx,my,mz)
						local midx,midy,midz = module:GetCachedGridMiddle()
						if (FindModuleIntersectingPoint(midx,midy,midz,nil,module)) then
							module:Destroy()
						end
					end
				end
				end
				end
			end
			
			-- simple line draw brush
			if (gKeyPressed[key_lshift] and gLastPlacePosition) then
				-- line brush start position
				local startx,starty,startz = gLastPlacePosition.x,gLastPlacePosition.y,gLastPlacePosition.z
				-- draw line
				while ( startx ~= px or starty ~= py or startz ~= pz ) do
					-- move position
					if startx > px then startx = startx - 1 end
					if startx < px then startx = startx + 1 end
					
					if starty > py then starty = starty - 1 end
					if starty < py then starty = starty + 1 end
					
					if startz > pz then startz = startz - 1 end
					if startz < pz then startz = startz + 1 end
					
					-- draw if there is no part
					local module = SpawnShipEditModule(gActiveShipPartType,kOldUnusedSkinName,{startx,starty,startz},{qw,qx,qy,qz},mx,my,mz)
					local midx,midy,midz = module:GetCachedGridMiddle()
					if (FindModuleIntersectingPoint(midx,midy,midz,nil,module)) then
						module:Destroy()
					end
				end
			end
			
			local newmodule = SpawnShipEditModule(gActiveShipPartType,kOldUnusedSkinName,{px,py,pz},{qw,qx,qy,qz},mx,my,mz)
			
			-- store position of the last time the place tools was used 
			gLastPlacePosition = {x=px,y=py,z=pz}
			
			SelectShipModule(newmodule)
		end
	end }
	gShipEditTools.MirrorX	= {material="editor_icon_mirror_x", fun=function (module,facenum,x,y,z,dist) 
		module:MirrorInBounds(-1,1,1)
		SelectShipModule(module)
	end }
	gShipEditTools.MirrorY	= {material="editor_icon_mirror_y", fun=function (module,facenum,x,y,z,dist) 
		module:MirrorInBounds(1,-1,1)
		SelectShipModule(module)
	end }
	gShipEditTools.MirrorZ	= {material="editor_icon_mirror_z", fun=function (module,facenum,x,y,z,dist) 
		module:MirrorInBounds(1,1,-1)
		SelectShipModule(module)
	end }
	
	
	gShipEditTools.Rot90X	= {material="editor_icon_rotate_x", fun=function (module,facenum,x,y,z,dist) 
		local ix,iy,iz = ShipEditGetGridPosInsideClickedModule(module,facenum,x,y,z)
		module:RotateAroundPoint(ix,iy,iz,GetRot90(1,0))
		SelectShipModule(module)
	end }
	gShipEditTools.Rot90Y	= {material="editor_icon_rotate_y", fun=function (module,facenum,x,y,z,dist) 
		local ix,iy,iz = ShipEditGetGridPosInsideClickedModule(module,facenum,x,y,z)
		module:RotateAroundPoint(ix,iy,iz,GetRot90(1,1))
		SelectShipModule(module)
	end }
	gShipEditTools.Rot90Z	= {material="editor_icon_rotate_z", fun=function (module,facenum,x,y,z,dist) 
		local ix,iy,iz = ShipEditGetGridPosInsideClickedModule(module,facenum,x,y,z)
		module:RotateAroundPoint(ix,iy,iz,GetRot90(1,2))
		SelectShipModule(module)
	end }
	gShipEditTools.CenterCam	= {material="editor_icon_base", fun=function (module,facenum,x,y,z,dist) 
		ShipEditCenterShipModule(module)
	end }
	
	--[[
	gShipEditTools.SideCompareDebug	= function (module,facenum,x,y,z,dist) 
		if (not gLastSideCompareDebugClick) then 
			gLastSideCompareDebugClick = {module,facenum}
		else
			local lastmodule,lastfacenum = unpack(gLastSideCompareDebugClick)
			gLastSideCompareDebugClick = nil
			
			local relgeom1 = lastmodule.shipparttype.gfx_geom
			local relgeom2 =     module.shipparttype.gfx_geom
			print("relside1",arrdump(GeomGetSide(relgeom1,GeomFaceNumToSideNum(relgeom1,lastfacenum))))
			print("relgeom2",arrdump(GeomGetSide(relgeom2,GeomFaceNumToSideNum(relgeom2,    facenum))))
			
			local t1 = lastmodule.shipparttype
			local t2 =     module.shipparttype
			local absgeom1 	= GeomApplyScaleMirRotPosCombo(t1.gfx_geom, t1.cx,t1.cy,t1.cz,	lastmodule:GetMirRotPosCombo())
			local absgeom2 	= GeomApplyScaleMirRotPosCombo(t2.gfx_geom, t2.cx,t2.cy,t2.cz,	    module:GetMirRotPosCombo())
			local absgeomside1 = GeomGetSide(absgeom1,GeomFaceNumToSideNum(absgeom1,lastfacenum))
			local absgeomside2 = GeomGetSide(absgeom2,GeomFaceNumToSideNum(absgeom2,    facenum))
			local score = ShipEditGetScoreForOverlappingGeomSides(absgeomside1,absgeomside2)
			print("SideCompareDebug",score)
		end
	end
	]]--



	
	
function ShipEditNewFile ()
	ShipEditRemoveAllModule()

	local module = SpawnShipEditModule(gShipEdit_StartBlockType,kOldUnusedSkinName,{0,0,0},{Quaternion.identity()},1,1,1)
	if (gbShipEditorStartRot) then module:Rotate(unpack(gbShipEditorStartRot)) end
	SelectShipModule(module)
	ShipEditCenterShipModule(module)
	
	NotifyListener("Hook_ShipEdit_NewFile")
end
	
	
function OpenShipPartChooserDialog ()
	if (gShipPartChooserDialog) then return end -- already started
	
	local myChooseShipEditTool = function (widget)
		gActiveShipEditTool = widget.tool
		gActiveShipPartType = widget.parttype and widget.parttype.type_name
		-- AddFadeLines(sprintf("active ship-part-type = %s",widget.parttype and widget.parttype.type_name or ""))
		-- todo : hilight button somehow
	end
	
	
	local rows = {
		{ {"ShipPartChooser:"} },
		{ {"#New",			ShipEditNewFile} },
		{ {"#Load",			ShipEditShowLoadDialog} },
		{ {"#Save",			ShipEditShowSaveDialog} },
		{ {"#QuickSave",	ShipEditQuickSave} },
	}
	
	local sorted_tool_keys = keys(gShipEditTools)
	
	for i,name in pairs(sorted_tool_keys) do
		local tool = gShipEditTools[name]
		table.insert(rows,{ {type="Button",	on_button_click=myChooseShipEditTool,tool=tool,text="#"..name} })
	end
	
	for i,parttype in pairs(ksort(gShipPartTypes.byname)) do if (parttype.type_parent) then -- don't show abstract baseclass
		table.insert(rows,{ {type="Button",	on_button_click=myChooseShipEditTool,parttype=parttype,tool=gShipEditTools.Place,text=parttype.type_name} })
	end end
	
	gShipPartChooserDialog = guimaker.MakeTableDlg(rows,-110,10,false,true)
end


function StartShipEditor ()
	gActiveShipEditTool = gShipEditTools.Place
	gbShipEditorRunning = true
	print("StartShipEditor")
	
	gShipEditLocation = CreateMenuBackgroundLocation()
	
	--assert(not gStarField)
	if (gStarField) then gStarField:Destroy() end
	gStarField = CreateRootGfx3D()
	gStarField:SetStarfield(gNumberOfStars,gStarsDist,gStarColorFactor,"starbase")
	local cam = GetMainCam()
	cam:SetNearClipDistance(gDefaultNearClip)
	cam:SetFarClipDistance(gDefaultFarClip) -- ogre defaul : 100000
	local amb = 0.2
	
	Client_ClearLights()
	
	Client_SetAmbientLight(amb,amb,amb,1)
	--gEditorLight = Client_AddDirectionalLight(-0.3,-0.5,-0.1)
	gEditorLight = Client_AddPointLight(cam:GetPos())
	
	gCamThirdPersonDist = kInitialShipEditThirdPersonDist
	
	ShipEditNewFile()
	if (gShipEdit_StartFile and file_exists(gShipEdit_StartFile)) then ShipEditLoadFromFile(gShipEdit_StartFile) end
	OpenShipPartChooserDialog()
	
	local myKeyDown = function (key,char)
		if (key == GetNamedKey("c")) then
			local module,facenum,x,y,z,dist = ShipEditMousePick()
			ShipEditCenterShipModule(module)
		end
		-- pipette to set the parttype currently under the mouse as selected tool
		if (key == GetNamedKey("space")) then
			local module,facenum,x,y,z,dist = ShipEditMousePick()
			if module and module.shipparttype and module.shipparttype.type_name then
				gActiveShipEditTool = gShipEditTools["Place"]
				gActiveShipPartType = module.shipparttype.type_name
			end
		end
		-- tool keybindings
		for k,o in pairs(glShipEditToolKeybinding) do
			if (key == GetNamedKey(o.key)) then
				gActiveShipEditTool = gShipEditTools[o.tool]	
				gActiveShipPartType = o.parttype
			end
		end
	end
	RegisterListener("keydown",myKeyDown)
	
	RegisterListener("mouse_left_click",function () ShipEditSingleClick() end)
	RegisterListener("mouse_right_down",function () ShipEditRightClick() end)

	NotifyListener("Hook_ShipEdit_Start")
end

function ShipEditCenterShipModule(module)
	if (not module) then return end
	local x1,y1,z1, x2,y2,z2 = module:GetGridBounds()
	gCamThirdPersonX,gCamThirdPersonY,gCamThirdPersonZ = (x1+x2)/2,(y1+y2)/2,(z1+z2)/2
	print("current center position:",gCamThirdPersonX,gCamThirdPersonY,gCamThirdPersonZ)
end

function SelectShipModule (module)
	if (gShipEditSelectBox) then gShipEditSelectBox:Destroy() gShipEditSelectBox = nil end
	gShipEditSelectedModule = module
	if (module) then
		local x1,y1,z1, x2,y2,z2 = module:GetGridBounds()
		gShipEditSelectBox = CreateRootGfx3D()
		SetBeamBox(gShipEditSelectBox,kShipEditorHaloWidth, x2-x1,y2-y1,z2-z1,unpack(kShipEditorSelectColor)) 
		gShipEditSelectBox:SetPosition(x1,y1,z1)
	end
end

-- return module,facenum,x,y,z,dist  : nil if not hit
function ShipEditMousePick ()
	local rx,ry,rz,rvx,rvy,rvz = GetMouseRay()
	local foundmodule,foundfacenum,founddist,x,y,z
	for k,module in pairs(gShipEditModules) do
		local facenum,dist = module:RayPick(rx,ry,rz, rvx,rvy,rvz) 
		if (dist and ((not founddist) or (dist < founddist))) then
			founddist = dist
			foundfacenum = facenum
			foundmodule = module
			x,y,z = (rx + dist*rvx),(ry + dist*rvy),(rz + dist*rvz)
		end
	end
	return foundmodule,foundfacenum,x,y,z,founddist
end



-- returns the center of the cell nearest to the clicked point
function ShipEditGetGridPosInsideClickedModule (module,facenum,x,y,z)
	--[[
	local nx,ny,nz = FaceGetNormal(module.gfx,facenum)
	local o = -kShipEditorHitFaceOffset -- offset to get inside
	local hx,hy,hz = x+o*nx,y+o*ny,z+o*nz   -- h for hint 
	return math.floor(hx),math.floor(hy),math.floor(hz)
	]]--
	local x3,y3,z3,x4,y4,z4 = module:GetGridBounds()
	local founddist
	for midx=x3+0.5,x4 do
	for midy=y3+0.5,y4 do
	for midz=z3+0.5,z4 do
		local dist = Vector.len(midx-x,midy-y,midz-z)
		if ((not founddist) or dist < founddist) then
			founddist = dist
			fx,fy,fz = math.floor(midx),math.floor(midy),math.floor(midz)
		end
	end
	end
	end
	return fx,fy,fz
end

function StepShipEditor ()
	if (not gbShipEditorRunning) then return end
	if (gShipEditLocation) then gShipEditLocation:StepAllObjects(gMyTicks,gSecondsSinceLastFrame) end
	StepTableCam(GetMainCam(),gKeyPressed[key_mouse1] and (not gui.bMouseBlocked),gShipEditorTableCamSpeedFactor)
	StepThirdPersonCam(GetMainCam(),gCamThirdPersonDist,gCamThirdPersonX,gCamThirdPersonY,gCamThirdPersonZ)
	
	-- todo : hilight sides on mouseover
	local module,facenum,x,y,z,dist = ShipEditMousePick()
	if (module) then 
		-- todo : display new-module-pos as half-transparent ghost (geom+size+ghostmaterial, or mesh + ghostmaterial)
		local x1,y1,z1, x2,y2,z2 = module:GetCachedGridBounds()
		local placetxt
		gEditorDebugMsg = ""
		if (gActiveShipPartType and false) then
			local px,py,pz, qw,qx,qy,qz, mx,my,mz = GuessNewModulePosMirRot(gActiveShipPartType,module,facenum,x,y,z)
			placetxt = px and arrdump({px,py,pz,GetMirRotComboName(qw,qx,qy,qz,mx,my,mz)})
		end
		local mousehittxt = sprintf("%4.1f,%4.1f,%4.1f ",round(x*10)/10,round(y*10)/10,round(z*10)/10)
		SetBottomLine(arrdump({mousehittxt,x1,y1,z1," ",x2,y2,z2," #",gEditorDebugMsg,"# ",placetxt}))
	else
		SetBottomLine("")
	end
	
	-- gShipPartTypes:Get(shipparttype_or_name_or_id)
	-- display currently selected tool and parttype
	if gActiveShipPartType ~= gDisplayingShipPartType or gActiveShipEditTool ~= gDisplayingShipEditTool then
		-- update parttype gfx
		gDisplayingShipPartType = gActiveShipPartType
		-- update tool icon
		gDisplayingShipEditTool = gActiveShipEditTool
		
		-- remove current parttype gfx
		if gDisplayingShipPartTypeGfx then 
			gDisplayingShipPartTypeGfx:Destroy() 
			gDisplayingShipPartTypeGfx = nil 
		end
		
		-- create tool icon dialog if unavailable
		if not gToolPlane then
			local dialog = guimaker.MakeSortedDialog()
			gToolPlane = guimaker.MakePlane (dialog,gDisplayingShipEditTool.material,16,16,64,64)
		else
			gToolPlane.gfx:SetMaterial(gDisplayingShipEditTool.material)
		end
		
		-- assign new parttype
		local parttype = gShipPartTypes:Get(gDisplayingShipPartType)
		if parttype then
			gDisplayingShipPartTypeGfx = parttype:constructor()
			local s = 0.25
			gDisplayingShipPartTypeGfx:SetScale(s,s,s)
		end
	end
	
	-- update light
	if gEditorLight then 
		--Client_SetLightDirection(gEditorLight,CamViewDirection(GetMainCam())) 
		Client_SetLightPosition(gEditorLight,GetMainCam():GetPos()) 
	end
	
	local px, py = 128, 80
	local vw,vh = GetViewportSize()
	local x,y,z,dx,dy,dz = GetScreenRay(px / vw, py / vh)
	
	-- update gfx position
	if gDisplayingShipPartTypeGfx then
		local d = 5
		gDisplayingShipPartTypeGfx:SetPosition(x + dx * d,y + dy * d,z + dz * d)

		local w1,x1,y1,z1 = Quaternion.fromAngleAxis(gTableCamAngleH,0,1,0)	
		local w2,x2,y2,z2 = Quaternion.fromAngleAxis(gTableCamAngleV,1,0,0)
		local w3,x3,y3,z3 = Quaternion.Mul(w1,x1,y1,z1, w2,x2,y2,z2)
		gDisplayingShipPartTypeGfx:SetOrientation(w3,x3,y3,z3)
	end
end

function ShipEditSingleClick ()  
	if (not gbShipEditorRunning) then return end
	local module,facenum,x,y,z,dist = ShipEditMousePick()
	if (module) then gActiveShipEditTool.fun(module,facenum,x,y,z,dist) end
end

function ShipEditRightClick ()  
	if (not gbShipEditorRunning) then return end
	local module,facenum,x,y,z = ShipEditMousePick()
	if (module) then module:Destroy() end
end


-- handles loading and saving of ships
-- todo : currently the ships are saved to lua scripts, this is insecure, move to xml later
-- see also lugre/lua/lib.filebrowser.lua

gLastShipEditSaveFileName = nil

function ShipEditShowLoadDialog ()
	FileBrowse_Load("Load Ship",kShipEditDefaultSavePath,ShipEditLoadFromFile)
end

function ShipEditQuickSave ()
	if (not gLastShipEditSaveFileName) then AddFadeLines("quicksave not possible, make a normal save first") return end	
	ShipEditSaveToFile(gLastShipEditSaveFileName)
end

function ShipEditShowSaveDialog ()
	FileBrowse_Save("Save Ship",kShipEditDefaultSavePath,gLastShipEditSaveFileName or kShipEditDefaultFileName,ShipEditSaveToFile)
end

------------------------- top level commands

function ShipEditRemoveAllModule ()
	for k,module in pairs(gShipEditModules) do module:Destroy() end 
	gShipEditModules = {}
end

function ShipEditLoadFromFile (filepath)
	gLastShipEditSaveFileName = filepath
	ShipEditRemoveAllModule()
	
	local modulelist = LoadShipType(filepath)
	
	for k,m in pairs(modulelist) do
		local newmodule = SpawnShipEditModule(m.typename,kOldUnusedSkinName,{m.px,m.py,m.pz},{m.qw,m.qx,m.qy,m.qz},m.mx,m.my,m.mz)
	end
end

function ShipEditSaveToFile (filepath)
	gLastShipEditSaveFileName = filename
	AddFadeLines(sprintf("ShipEditSaveToFile %s",filepath))
	local f = io.open(filepath,"w")
	if (f) then
		for k,module in pairs(gShipEditModules) do 
			local typename = module.shipparttype and module.shipparttype.type_name or ""
			local skinname = kOldUnusedSkinName
			local px,py,pz, qw,qx,qy,qz, mx,my,mz = module:GetMirRotPosCombo()
			f:write(sprintf("ShipLoad_Module(\"%s\",\"%s\", %f,%f,%f, %f,%f,%f,%f, %f,%f,%f)\n",typename, skinname, px,py,pz, qw,qx,qy,qz, mx,my,mz))
		end
		f:close()
	else
		AddFadeLines("save failed (maybe persmission denied?)")
	end
end


------------------------- load parts 

function LoadShipType (filepath) 
	glShipLoadModuleList = {}
	dofile(filepath)
	return glShipLoadModuleList
end

-- ship files call this function for each part of the ship to store all components in the global array glShipLoadModuleList
function ShipLoad_Module (typename, skinname, px,py,pz, qw,qx,qy,qz, mx,my,mz)
	table.insert(glShipLoadModuleList,{typename=typename, skinname=skinname, px=px,py=py,pz=pz, qw=qw,qx=qx,qy=qy,qz=qz, mx=mx,my=my,mz=mz})
end




