function PrintPath(path)
	for k,o in pairs(path) do
		print(k,o[1],o[2],o[3])
	end
end


function DrawPath (gfx,path)
	gfx:SetSimpleRenderable()
	gfx:RenderableBegin(table.getn(path),0,false,false,OT_LINE_STRIP)

	-- add geometry to renderable
	for k,p in pairs(path) do
		gfx:RenderableVertex(p[1],p[2],p[3])
	end

	-- finish, assign mat, and add to mesh
	gfx:RenderableEnd()
end


function DrawPathLoft (gfx,path,material)
	--gfx:SetSimpleRenderable()
	--gfx:RenderableBegin(table.getn(path),0,false,false,OT_LINE_STRIP)

	local base = Loft_Base_Init()
	
	local r = 0.25
	
	for a=0,360-1,360/7 do 
		Loft_Base_AddVertex(base,r * -math.sin(a*gfDeg2Rad),0, r * math.cos(a*gfDeg2Rad), a/360)
	end

	local loftpath = Loft_Path_Init()

	-- add points to loft path
	for k,p in pairs(path) do
		Loft_Path_AddVertex(loftpath,  p[1],p[2],p[3])
	end

	loftpath = Loft_SmoothPath(loftpath,3)
	
	MakeLoft(gfx,base,loftpath) 
	
	if material then gfx:SetMaterial(material) end
	
	-- finish, assign mat, and add to mesh
	--gfx:RenderableEnd()
end


-- generates a list of pathes as flower parts
-- rand : random generator
-- parts : number of stem pieces
-- steps : steps factor to generate stem
-- d : determins the global direction of the plant {x,y,z}
-- rd : determins the global random factor {x,y,z}
function GenerateFlowerPaths (rand,parts,steps,d,rd)
	local plant = {}
	
	-- first starting point
	local base = {0,0,0}
	
	for i = 1,parts-1 do
		--print("CURRENT BASE",base)
		local d = {
				rand:GetFloat()*d[1],
				rand:GetFloat()*d[2],
				rand:GetFloat()*d[3],
			}
		local rd = {
				rand:GetFloat()*rd[1],
				rand:GetFloat()*rd[2],
				rand:GetFloat()*rd[3],
			}
		--print("D",d[1],d[2],d[3])
		--print("RD",rd[1],rd[2],rd[3])
		--print("STEPS",steps)
		local p = GeneratePath(rand,base,d,rd,steps)
		--PrintPath(p)
		table.insert(plant,p)
		
		local newbasepath = rand:GetInt(1,i)
		local newbaseindex = rand:GetInt(1,steps)
		--print("NEW BASE",newbasepath,newbaseindex)
		base = {unpack(plant[newbasepath][newbaseindex])}
	end
	
	return plant
end


-- generates a list of points
-- rand : random generator
-- base : startpoint {x,y,z}
-- d : movement per step {x,y,z}
-- rd : random movement per step {x,y,z}
-- steps : number of steps, path contains steps + 1 points
function GeneratePath(rand,base,d,rd,steps)
	local l = {}
	table.insert(l,{unpack(base)})
	
	local p = {unpack(base)}
	
	for i = 1,steps do
		for dim = 1,3 do
			p[dim] = p[dim] + d[dim] + rand:GetFloat() * rd[dim]
		end
		table.insert(l,{unpack(p)})
	end
	
	return l
end


-- seed : seed for random generator
-- gfx : gfx to put the plant into
-- col : {r,g,b,a}
-- parts,steps,d,rd see GenerateFlowerPaths
function NewPlant(seed,gfx,col,parts,steps,d,rd)
	local mat = GetPlainColourMat(unpack(col))

	local r = CreateRandom(seed)

	parts = parts or 10
	steps = steps or 10
	d = d or {1,1,-2}
	rd = rd or {-2,-2,2}

	local p = GenerateFlowerPaths(r,parts,steps,d,rd)
	
	for k,o in pairs(p) do
		local child = gfx:CreateChild()
		DrawPathLoft(child,o,mat)
	end
	
	r:Destroy()
end


