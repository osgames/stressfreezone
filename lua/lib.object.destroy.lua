-- objects methods related to destruction 
-- for damaging see lib.object.hp.lua

-- objects can register themselves here, then something:Destroy() will be called when the object is destroyed
-- WARNING ! don't add anything to the list during destroy iteration !!!
function gObjectPrototype:AddToDestroyList		(something) 
	assert(not self.dead) 
	if (not self.destroylist) then self.destroylist = {} end
	self.destroylist[something] = true 
end
function gObjectPrototype:RemoveFromDestroyList	(something) self.destroylist[something] = nil end

-- callbacks can register themselves here, then callback(obj) will be called when the object is destroyed
-- WARNING ! don't add anything to the list during destroy iteration !!!
function gObjectPrototype:AddToDestroyNotifyList	(callback) 
	assert(not self.dead) 
	if (not self.destroynotifylist) then self.destroynotifylist = {} end
	self.destroynotifylist[callback] = true 
	return callback
end
function gObjectPrototype:RemoveFromDestroyNotifyList	(callback) self.destroynotifylist[callback] = nil end


gMessageTypeHandler_Client_Remote.kNetMessage_DestroyObject = function (obj) if (obj) then obj:Destroy() end end


function gObjectPrototype:Destroy ()
	assert(not self.dead)
	if (gbServerRunning) then ServerBroadcastMessage(kNetMessage_DestroyObject,self) end
	self:DestroyInterior()
	self.dead = true
	NotifyListener("Hook_Object_Destroy",self)
	--if (self.destroylist) then for destroyme,ignored in pairs(self.destroylist) do if destroyme and destroyme:IsAlive() then destroyme:Destroy() end end end
	if (self.destroylist) then for destroyme,ignored in pairs(self.destroylist) do destroyme:Destroy() end end
	if (self.destroynotifylist) then for callback,ignored in pairs(self.destroynotifylist) do callback(self) end end
	if (self.location) then self.location.childs[self.id] = nil end -- TODO : set location ? notify location ?
	gObjectsByID[self.id] = nil
	if (self.gfx) then self:SetGfx() self.gfx:Destroy() self.gfx = nil end
	
	-- unset player body on destroy
	if (gClientBody and gClientBody.id == self.id) then gClientBody = nil end
	
	-- unset player body if there is one
	if ( self.player ) then
		self.player.body = nil
	end
	
	self:_Destroy()
end
