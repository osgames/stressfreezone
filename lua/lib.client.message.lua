-- receives and handles messages on the client


gMessageTypeHandler_Client.kNetMessage_FireWeapon = function (ship,weapon_container_id,target) 
	local weapon_container = GetContainer(weapon_container_id)
	weapon_container.client_last_fired = gMyTicks
	NotifyListener("Hook_FireWeapon",ship,weapon_container,target)
end

gMessageTypeHandler_Client.kNetMessage_PlayerID = function (iMyID) 
	giClientPlayerID = iMyID 
    
    -- oki i know what ship i want to be so request it
    RequestBody()
end

gMessageTypeHandler_Client.kNetMessage_PlayerBody = function (iPlayerID,newbody) 
	-- print("got body",iPlayerID,newbody,(iPlayerID == giClientPlayerID) and "its me" or "someone else")
	gClientPlayerBodies[iPlayerID] = newbody
	if (iPlayerID == giClientPlayerID) then 
		--if (gClientBody) then gClientBody:SetVisible(true) end -- show old body
		local oldbody = gClientBody
		gClientBody = newbody
		if (gClientBody) then
			--gClientBody:SetVisible(false) -- hide body for first person cam
			--local qw,qx,qy,qz = unpack(gClientBody.mqRot)
			--ClientSetCamOrientation(qw,qx,qy,qz)
        end
		--BurnComputer()
		NotifyListener("Hook_Client_SetBody",newbody,oldbody)
		
		if (gCommandLineSwitches["-i"]) then ClientRequestViewShipInside() end
		if (gCommandLineSwitches["-p"]) then ClientRequestLandOnPlanet(ClientGetNearestObject(ObjFilter_IsBase)) end
	end
end

gMessageTypeHandler_Client.kNetMessage_ProtocolHeader = function (sNetVersion) gClientReceivedServerNetVersion = sNetVersion end

-- only used when udp is disabled
gMessageTypeHandler_Client.kNetMessage_TCPResyncs = function (data)
	gClientResyncReceiver:ReceiveResyncsFromFIFO(data)
	data:Destroy() -- release data fifo
end

gMessageTypeHandler_Client.kNetMessage_Chat = function (iParam,iFlags,sChatText)  
	AddFadeLines(sprintf("#%d : %s",iParam,sChatText))
end


