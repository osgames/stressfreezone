-- creates a mesh from a shipmodulelist
-- does some complex stuff to remove faces between adjacted modules
-- can generate an "inverted" mesh, displaying the interior of the ship
-- instead of many small meshes for all individual modules, a big mesh for the whole ship is generated, with submeshes group by material
-- see also lib.shipeditguess.lua
-- see also lib.shipvoxelgrid.lua

--[[
	LoadShipType (filepath) returns a module-list
		table.insert(res,{typename=typename, skinname=skinname, px=px,py=py,pz=pz, qw=qw,qx=qx,qy=qy,qz=qz, mx=mx,my=my,mz=mz})
		
	first : 
		only consider program generated geometry, 
	
		later : parttypes with meshes must provide interior and exterior mesh 
			and possibly more than one to remove interior walls
		
	local texcoords_ramp_0 = {
			{0,h, 0,1, 1,h},
			{0,h, 1,h, 0,1},
			{0,0, 0,h, 1,0, 1,h},
			{1,1, 0,1, 1,h, 0,h},
			{0,h, h,h, 0,1, h,1},
		} -- 2 triangles and 3 quads : sd_cockpit_mitte_1.png
		
	kGeomPyramid = {
		{0,0,0, 1,0,0, 0,1,0,}, -- base
		{0,0,0, 0,1,0, 0,0,1,},
		{0,0,0, 0,0,1, 1,0,0,},
		{0,0,1, 0,1,0, 1,0,0,},
		}
		
	shipparttype.gfx_geom=kGeomPyramid
	shipparttype.texcoords=texcoords_ramp_0
	shipparttype.gfx_mat="CockPitCubeMapTestRamp"
	shipparttype.,cx=1,cy=1,cz=1 , with cz might be 2 for some parts
	
	todo : recenter mesh !  or offset parameter
	calc bounds ?
]]--

-- returns an array of sides, each sides is an array with 9 or 12 floats, for the coordinates
-- e.g. return { {0,0,0, 1,0,0, 0,1,0,}, {0,0,0, 0,1,0, 0,0,1,}, ... }
function GetModuleAbsGeom (module)
	local m = module
	local t = m.shipparttype or gShipPartTypes:Get(module.typename)
	return GeomApplyScaleMirRotPosCombo(t.gfx_geom, t.cx,t.cy,t.cz, m.px,m.py,m.pz, m.qw,m.qx,m.qy,m.qz, m.mx,m.my,m.mz)
end

-- minx,miny,minz, maxx,maxy,maxz = GetModuleAbsBounds(module)
function GetModuleAbsBounds(module) return GetGeoBounds(GetModuleAbsGeom(module)) end

-- minx,miny,minz, maxx,maxy,maxz = GetGeoBounds(geom)
function GetGeoBounds (geom) 
	local minx,miny,minz, maxx,maxy,maxz
	for k,side in pairs(geom) do
		local x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4 = unpack(side)
		minx = math.min(minx or x1,x1,x2,x3,x4 or x1)
		miny = math.min(miny or y1,y1,y2,y3,y4 or y1)
		minz = math.min(minz or z1,z1,z2,z3,z4 or z1)
		maxx = math.max(maxx or x1,x1,x2,x3,x4 or x1)
		maxy = math.max(maxy or y1,y1,y2,y3,y4 or y1)
		maxz = math.max(maxz or z1,z1,z2,z3,z4 or z1)
	end
	return minx,miny,minz, maxx,maxy,maxz
end


-- gets points on both sides of the poly with a small distance (=0.1) to avoid rounding errors
-- xa,ya,za, xb,yb,zb = GetSidePointPair (x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4)
function GetSidePointPair (x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4)
	local nx,ny,nz = Vector.normalise(Vector.cross(x3-x1,y3-y1,z3-z1,x2-x1,y2-y1,z2-z1))
	local hx,hy,hz = nx*0.1,ny*0.1,nz*0.1
	local minx = math.min(x1,x2,x3,x4 or x1)
	local miny = math.min(y1,y2,y3,y4 or y1)
	local minz = math.min(z1,z2,z3,z4 or z1)
	local maxx = math.max(x1,x2,x3,x4 or x1)
	local maxy = math.max(y1,y2,y3,y4 or y1)
	local maxz = math.max(z1,z2,z3,z4 or z1)
	local midx = 0.5*minx + 0.5*maxx
	local midy = 0.5*miny + 0.5*maxy
	local midz = 0.5*minz + 0.5*maxz
	return midx+hx,midy+hy,midz+hz, midx-hx,midy-hy,midz-hz
end

-- true for boxes, false for pyramids, ramps etc
function TestIfModuleIsBox (module) return gShipPartTypes:Get(module.typename).bIsBox end
function TestIfGeomIsBox (geom) return TestIfAllGeomSidesAreAxisAligned(geom) end
function TestIfAllGeomSidesAreAxisAligned (geom) 
	for k,geomside in pairs(geom) do if (not GeomSideIsAxisAligned(geomside)) then return false end end
	return true
end


-- also recognises points like (1,0,0) as covered by line from (0,0,0) to (2,0,0)
function TestIfPointIsOnGeomLine (x,y,z, x1,y1,z1, x2,y2,z2)
	if (x == x1 and y == y1 and z == z1) then return true end
	if (x == x2 and y == y2 and z == z2) then return true end
	local dx = x2-x1
	local dy = y2-y1
	local dz = z2-z1
	local fx = (dx ~= 0) and ((x - x1)/dx) or 0
	local fy = (dy ~= 0) and ((y - y1)/dy) or 0
	local fz = (dz ~= 0) and ((z - z1)/dz) or 0
	local f
	if (dx == 0) then if (x ~= x1) then return false end else f = f or fx if (fx ~= f) then return false end end
	if (dy == 0) then if (y ~= y1) then return false end else f = f or fy if (fy ~= f) then return false end end
	if (dz == 0) then if (z ~= z1) then return false end else f = f or fz if (fz ~= f) then return false end end
	return f >= 0 and f <= 1
	--return false
end

assert(TestIfPointIsOnGeomLine(1,0,0, 0,0,0, 2,0,0),"TestIfPointIsOnGeomLine broken")

-- also recognises points like (1,0,0) as covered by line from (0,0,0) to (2,0,0)
-- does not test diagonal or point on surface
function TestIfGeomCoversPos (geom,x,y,z)
	for k,arr in pairs(geom) do 
		local x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4 = unpack(arr)
		if (TestIfPointIsOnGeomLine(x,y,z, x1,y1,z1, x2,y2,z2)) then return true end
		if (TestIfPointIsOnGeomLine(x,y,z, x1,y1,z1, x3,y3,z3)) then return true end
		if (x4) then 
			if (TestIfPointIsOnGeomLine(x,y,z, x4,y4,z4, x2,y2,z2)) then return true end
			if (TestIfPointIsOnGeomLine(x,y,z, x4,y4,z4, x3,y3,z3)) then return true end
		end
	end
	return false
end

-- bInvertFaces : true to render the interior of the ship
-- bFixHoles : set to true for outside, false for inside
-- bFixHoles : the border detection is not perfect, 
-- and it would sometimes be neccessary to not only set the visibility of sides, 
-- but also to generate custom geometry for fixing the holes correctly, e.g. when a pyramid sits on a box. 
-- we haven't implemented that exact stuff yet as it is rather complex.
-- our workaround is to cheat a bit in the voxelgrid about what is considered outside and what is inside
-- when the spaceship is viewed from the outside, more inner-walls are drawn to make the holes invisible
-- when the spaceship is viewed from the inside it is usually in a seperate scene with black background,
-- then no extra inner-walls are drawn as they would obscur the cool looking windows.
-- it might happen that when viewed from the inside at some places there is a small hole 
-- trough which the black background can be seen, but that is not so bad
function GenerateMeshFromModuleList (modulelist,bInvertFaces,bFixHoles,xoff,yoff,zoff)
	xoff = xoff or 0
	yoff = yoff or 0
	zoff = zoff or 0
	
	-- setup voxel grid for fast interior/exterior detection
	local shipVoxelGrid = ShipVoxelGrid_CreateFromModuleList(modulelist,bFixHoles)
	
	-- group modules by material
	local bymat = {}
	for k,m in pairs(modulelist) do
		local t = gShipPartTypes:Get(m.typename)
		local matname = t.gfx_mat
		local modulesublist = bymat[matname]
		if (not modulesublist) then modulesublist = {} bymat[matname] = modulesublist end
		table.insert(modulesublist,m)
	end
	
	local sMeshName = nil
	for matname,modulesublist in pairs(bymat) do
		
		local sidelist = {} -- { { absgeomside , texcoordset } , ... }
		
		for k2,m in pairs(modulesublist) do
			local t = gShipPartTypes:Get(m.typename)
			local texcoords = t.texcoords
			local absgeom = GetModuleAbsGeom(m)
			local bIsBox = TestIfModuleIsBox(m)
			for k,arr in pairs(absgeom) do 
				local x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4 = unpack(arr)
				if (bInvertFaces) then x2,y2,z2, x3,y3,z3 = x3,y3,z3, x2,y2,z2 end
				local bDrawn = TestIfSideIsBorder(absgeom,shipVoxelGrid, x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4)
				if (bDrawn) then 
					table.insert(sidelist,{arr,texcoords[k]}) 
				else
					if (bIsBox) then
						-- test if custom geometry for fixing holes needs to be generated
						local xa,ya,za, xb,yb,zb = GetSidePointPair (x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4)
						local a = ShipVoxelGrid_GetModuleAtPos(shipVoxelGrid,xa,ya,za)
						local b = ShipVoxelGrid_GetModuleAtPos(shipVoxelGrid,xb,yb,zb)
						local other = (a == m) and b or a
						if (other ~= m and (not TestIfModuleIsBox(other))) then 
							-- check if all 4 vertices of our box side are covered in other
							local other_absgeom = GetModuleAbsGeom(other)
							local covered_1 = TestIfGeomCoversPos(other_absgeom,x1,y1,z1)
							local covered_2 = TestIfGeomCoversPos(other_absgeom,x2,y2,z2)
							local covered_3 = TestIfGeomCoversPos(other_absgeom,x3,y3,z3)
							local covered_4 = TestIfGeomCoversPos(other_absgeom,x4,y4,z4)
							-- todo : for 1,2,3, : correct the set of texcoords used
							if (not covered_1) then
								table.insert(sidelist,{{x1,y1,z1, x3,y3,z3, x2,y2,z2},texcoords[k]}) 
							elseif (not covered_2) then
								table.insert(sidelist,{{x1,y1,z1, x4,y4,z4, x2,y2,z2},texcoords[k]}) 
							elseif (not covered_3) then
								table.insert(sidelist,{{x1,y1,z1, x3,y3,z3, x4,y4,z4},texcoords[k]}) 
							elseif (not covered_4) then
								table.insert(sidelist,{{x2,y2,z2, x3,y3,z3, x4,y4,z4},texcoords[k]}) 
							end
						end
					end
				end
			end
		end
		
		
		-- calc total vertex and index count in advance
		local vc,ic = 0,0
		for k,myside in pairs(sidelist) do
			local arr = myside[1]
			vc = vc + GeomSideGetVertexCount(arr)
			ic = ic + GeomSideGetIndexCount(arr)
		end
		
		if (vc > 0) then 
			local gfx = CreateGfx3D()
			gfx:SetSimpleRenderable()
			gfx:RenderableBegin(vc,ic,false,false,OT_TRIANGLE_LIST)
			vc = 0
			
			-- add geometry to renderable
			for k,myside in pairs(sidelist) do
				local arr = myside[1]
				local texcoordset = myside[2]
				
				local x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4 = unpack(arr)
				x1,y1,z1 = Vector.add(x1,y1,z1,xoff,yoff,zoff) 
				x2,y2,z2 = Vector.add(x2,y2,z2,xoff,yoff,zoff) 
				x3,y3,z3 = Vector.add(x3,y3,z3,xoff,yoff,zoff) 
				if (x4) then x4,y4,z4 = Vector.add(x4,y4,z4,xoff,yoff,zoff) end
				if (bInvertFaces) then x2,y2,z2, x3,y3,z3 = x3,y3,z3, x2,y2,z2 end
				if (GeomSideIsQuad(arr)) then 
					vc = DrawQuad(gfx,vc, x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4, unpack(texcoordset))
				else 
					vc = DrawTri(gfx,vc,  x1,y1,z1, x2,y2,z2, x3,y3,z3, unpack(texcoordset))
				end
			end
			
			-- finish, assign mat, and add to mesh
			gfx:RenderableEnd()
			gfx:SetMaterial(matname)
			if (not sMeshName) then
				sMeshName = gfx:RenderableConvertToMesh() -- first
			else
				gfx:RenderableAddToMesh(sMeshName)
			end
			gfx:Destroy()	
		end
	end
	return sMeshName
end

gShipOutsideMeshCache = {}
gShipInsideMeshCache = {}
function GetShipOutsideMesh_Cached (filepath)
	return gShipOutsideMeshCache[filepath] or WriteToCache(gShipOutsideMeshCache,filepath,GenerateMeshFromModuleList(LoadShipType(filepath),false,true))
end

function GetShipInsideMesh_Cached (filepath)
	return gShipInsideMeshCache[filepath] or WriteToCache(gShipInsideMeshCache,filepath,GenerateMeshFromModuleList(LoadShipType(filepath),true,false))
end

function LoadShipToGfx(gfx,filepath,bInside)
	gfx.childs = {}	-- TODO : removeme
	gfx:SetMesh(bInside and GetShipInsideMesh_Cached(filepath) or GetShipOutsideMesh_Cached(filepath))
	if (gUseCellShading) then
		local c = gfx:GetMeshSubEntityCount()
		for i=0,c-1 do 
			gfx:SetMeshSubEntityCustomParameter(i,1,	10,0,0,0) -- shininess
			gfx:SetMeshSubEntityCustomParameter(i,2,	1,1,1,1) -- diffuse
			gfx:SetMeshSubEntityCustomParameter(i,3,	1,1,1,1) -- specular
		end
	end
end
