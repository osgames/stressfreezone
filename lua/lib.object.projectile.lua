-- inits the projectile controller to approach the target using full acceleration
-- lifetime : time in seconds until the projectile explodes if its still alive
-- explodedistance : if projectile-target-distance is smaler than this BOOOOOOOM!
-- damagerange : every object nearer than this gets damage on explosion
-- damage : amount of damage inflicted
-- acceleration : just the acceleration
function Projectile_Init (projectile,target,ownerbody,weapontype)
	local t = weapontype
	assert(gbServerRunning,"projectile is a serverside feature!")
	if (projectile.controller) then return end -- already initialised
	
	if (t.bGuided) then
		projectile.controller = CreateObjectController() -- todo : memleak, release me
		projectile.controller.mfMaxAccel = t.acceleration
		projectile.controller.mbStupid = 1
		projectile.controller:SetApproachObject(target)
		projectile.controller.mfApproachMinDist = 0
		projectile:SetController(projectile.controller)
	end
	
	projectile.projectile_ownerbody = ownerbody
	projectile.projectile_target = target
	projectile.projectile_killtime = gMyTicks + (t.lifetime * 1000)
	projectile.projectile_weapontype = weapontype
	local myprojectile = projectile -- closure
	projectile.projectile_stepper = function () return Projectile_Step(myprojectile) end
	RegisterListener("Hook_MainStep",projectile.projectile_stepper)
end

function Projectile_Explode (projectile)
	if (projectile and projectile:IsAlive()) then
		local x,y,z = unpack(projectile.mvPos)
		local t = projectile.projectile_weapontype
		local ownerbody = projectile.projectile_ownerbody
		
		SpawnEffect("effect/hit",nil,{x,y,z},{1,0,0,0})
				
		-- search for objects to damage
		local l = GetObjectsInRange(projectile,t.damagerange,function(o) return o ~= projectile end)
		for k,v in pairs(l) do
			if v and v:IsAlive() then
				-- damage this one
				v:Damage(t.damage,x,y,z,ownerbody)
			end
		end
		
		projectile:Destroy()
	end
end

function Projectile_Step (projectile)
	if ((not projectile) or (not projectile:IsAlive())) then return true end
	local t = projectile.projectile_weapontype
	local boom = false
	
	-- set orientation
	local vx,vy,vz = unpack(projectile.mvVel)
	projectile.mqRot = { Quaternion.getRotation(0,0,-1,vx,vy,vz) }

	-- check target distance
	if (projectile.projectile_target and t.explodedistance > 0) then
		-- approach
		if (projectile.projectile_target:IsAlive()) then
			-- target still alive
			-- explode?
			if (projectile:GetDistToObject(projectile.projectile_target) < t.explodedistance) then
				boom = true
			end
		else
			-- target destroyed => just fly strait ahead
			projectile.projectile_target = nil
		end
	end
	
	-- todo : projectiles don't hit anything besides their target, but thats not a big problem since aiming is done by selecting target

	-- for fast projectile
	if (t.bLineCollision) then
		-- todo : precalulate own pow after movement  gSecondsSinceLastFrame 
		local rx,ry,rz		= unpack(projectile.mvPos)
		local rvx,rvy,rvz	= Vector.scale1(gSecondsSinceLastFrame,unpack(projectile.mvVel))
		local ownerbody = projectile.projectile_ownerbody
		
		local obj = projectile.projectile_target
		if (obj and obj:IsAlive()) then 
			local dist = obj:RayPick(rx,ry,rz,rvx,rvy,rvz)
			if (dist and dist < 1) then 
				local hx,hy,hz = Vector.addscaled(dist,rx,ry,rz,rvx,rvy,rvz)
				obj:Damage(t.damage,hx,hy,hz,ownerbody)
			end
		end
		
		--~ local pt = projectile.objtype
		--~ MapRayPick(rx,ry,rz,rvx,rvy,rvz,function (obj,dist,facenum) 
				--~ if (obj ~= ownerbody and obj ~= projectile and obj.objtype ~= pt and dist < 1) then 
					--~ print("Projectile_Step : line collision with ",obj.objtype.type_name)
					--~ local hx,hy,hz = Vector.addscaled(dist,rx,ry,rz,rvx,rvy,rvz)
					--~ obj:Damage(t.damage,hx,hy,hz,ownerbody)
				--~ end
			--~ end)
	end
	
	if (t.bSphereCollision) then
		-- spherecollision for slow projectiles
		print("todo : Projectile_Step:bSphereCollision")
	end
	
	
	-- check lifetime
	if (projectile.projectile_killtime < gMyTicks) then
		boom = true
	end

	-- should this one explode?
	if (boom) then
		Projectile_Explode(projectile)
		return true
	end
end
