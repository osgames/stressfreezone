-- object methods related to movement

function gObjectPrototype:GetMaxAccel	() return self.objtype.accel or 1 end
function gObjectPrototype:GetCurSpeed	() return Vector.len(unpack(self.mvVel)) end


-- x,y,z are in absolute coordinates, must be in [-1,1]
function gObjectPrototype:ManualThrustStep(x,y,z) 
	local e = self:GetMaxAccel()
	self.mvAccel = {x*e,y*e,z*e}
	self:MarkChanged()
end

-- ship decelleration
function gObjectPrototype:BreakStep ()
	local vx,vy,vz = unpack(self.mvVel)
	local vel = Vector.len(vx,vy,vz)
	local maxbreak = vel * 10
	local x,y,z = Vector.normalise(-vx,-vy,-vz)
	local e = math.min(maxbreak,self:GetMaxAccel()*3)
	self.mvAccel = {x*e,y*e,z*e}
	self:MarkChanged()
end


function gObjectPrototype:AutoOrientationStep ()
	local vx,vy,vz = unpack(self.mvVel)
	local vel = Vector.len(vx,vy,vz)
	if (vel > 2) then
		
		if (false) then
			-- try to keep up vector
			--local ax,ay,az = unpack(self.mvAccel)
			local dx,dy,dz = vx,vy,vz
			local qw,qx,qy,qz = Quaternion.getRotation(0,0,-1,dx,dy,dz)
			local ow,ox,oy,oz = unpack(self.mqRot)
			self.mqRot = { Quaternion.Slerp(qw,qx,qy,qz, ow,ox,oy,oz, 0.02) }
		else
			local turn_speed_factor = 10
			-- step1 : adjust heading to velocity 
			local qw,qx,qy,qz = unpack(self.mqRot)
			local hx,hy,hz = Quaternion.ApplyToVector(0,0,-1,qw,qx,qy,qz)  -- current heading/forward vector
			local rw,rx,ry,rz = Quaternion.getRotation(hx,hy,hz,vx,vy,vz)
			rw,rx,ry,rz = Quaternion.reduce(rw,rx,ry,rz,0.02 * turn_speed_factor) 
			qw,qx,qy,qz = Quaternion.normalise(Quaternion.Mul(rw,rx,ry,rz,qw,qx,qy,qz))
			
			-- step2 : adjust up vector
			local phx,phy,phz = Vector.project_on_plane(0,1,0,Vector.normalise(hx,hy,hz)) -- projected world up vector
			local ux,uy,uz = Quaternion.ApplyToVector(0,1,0,qw,qx,qy,qz)  -- current up-vector
			rw,rx,ry,rz = Quaternion.getRotation(ux,uy,uz,phx,phy,phz)
			
			rw,rx,ry,rz = Quaternion.reduce(rw,rx,ry,rz,0.05 * turn_speed_factor) 
			qw,qx,qy,qz = Quaternion.normalise(Quaternion.Mul(rw,rx,ry,rz,qw,qx,qy,qz))
			
			self.mqRot = { qw,qx,qy,qz }
		end
	end
	-- self:MarkChanged() -- this is usually set anyway
end

