-- targetting functions


-- returns a list of interesting targets for the player, e.g. ships, not asteroids
-- fun filter(obj) == true -> interesting target
function GetCustomTargetList (self,filter)
	local res = {}
	for k,loc in pairs(gLocationsByID) do 
		for k2,obj in pairs(loc.childs) do 
			if (filter(obj) and obj ~= self) then 
				table.insert(res,obj)
			end
		end
	end
	return res
end

-- returns a list of interesting targets for the player, e.g. ships, not asteroids
function GetTargetList (self)
	return GetCustomTargetList(self,function(obj) return obj.objtype.bIsShip end)
end

-- searches for targets with the given pattern in the name
function GetTargetByName (pattern)
	local res = {}
	for k,loc in pairs(gLocationsByID) do 
		for k2,obj in pairs(loc.childs) do 
			-- print("DEBUG",obj.objtype.type_name,pattern)
			if (string.find(obj.objtype.type_name,pattern)) then 
				table.insert(res,obj)
			end
		end
	end
	return res
end

function TargetList_GetFirst (targetlist)
	for k,obj in pairs(targetlist) do return obj end
end

function TargetList_GetLast (targetlist)
	local target
	for k,obj in pairs(targetlist) do target = obj end
	return target
end

function TargetList_GetNext (targetlist,cur)
	if (not cur) then return TargetList_GetFirst(targetlist) end
	local curfound = false
	for k,obj in pairs(targetlist) do
		if (curfound) then return obj end
		if (obj == cur) then curfound = true end
	end
	return TargetList_GetFirst(targetlist)
end

function TargetList_GetPrev (targetlist,cur)
	if (not cur) then return TargetList_GetLast(targetlist) end
	local prev
	for k,obj in pairs(targetlist) do
		if (obj == cur) then return prev or TargetList_GetLast(targetlist) end
		prev = obj
	end
end

function TargetList_Nearest (targetlist,self)
	if (not self) then return end
	if (not self:IsAlive()) then return end
	local founddist
	local target
	-- get nearest
	for k,obj in pairs(targetlist) do
		local curdist = obj:GetDistToObject(self)
		if ((not founddist) or founddist > curdist) then
			founddist = curdist
			target = obj
		end
	end
	return target
end

