-- TODO : obsolete, clean me up and reuse !

function gui.HideBuyButton () 
	gui.buybutton_dialog:SetVisible(false)
	gui.HideBuyMenu()
end

function gui.ShowBuyButton () 
	gui.buybutton_dialog:SetVisible(true)
end

function gui.ShowBuyMenu ()
	print("open buy menu")
	gui.buymenu_dialog:SetVisible(true)
end

function gui.HideBuyMenu ()
	gui.buymenu_dialog:SetVisible(false)
end

function guimaker.MakeBuyMenuRow (parent,itemname,price) 
	local b = 3
	local textcol = {1,1,1,1}
	local btnbackcol = {0.5,0.5,1,1}
	local charh = 20
	local topborder = charh+10
	local label_title = guimaker.MakeText(parent,0,0,"BuyMenu",charh,textcol)
	
	local w,h
	w,h = label_title.gfx:GetTextBounds()
	label_title.gfx:SetPos(parent.gfx:GetWidth()/2-w/2,b+parent.bT)
	
	local label1 = guimaker.MakeText(parent,b+parent.bL,	topborder+b+parent.bT,"0",charh,textcol)
	local label2 = guimaker.MakeText(parent,b+parent.bL+40,topborder+b+parent.bT,itemname,charh,textcol)
	local label3 = guimaker.MakeText(parent,b+parent.bL+120,topborder+b+parent.bT,"$"..price,charh,textcol)
	
	local buybtn = guimaker.MakeAutoScaledButton (parent,b+parent.bL+200,topborder+b+parent.bT-5,"Buy",charh/ 2,btnbackcol,textcol) 
	local sellbtn = guimaker.MakeAutoScaledButton (parent,b+parent.bL+230,topborder+b+parent.bT-5,"Sell",charh/ 2,btnbackcol,textcol) 
	
	local closebtn = guimaker.MakeAutoScaledButton (parent,0,0,"Close",charh/ 2,btnbackcol,textcol) 
	closebtn.gfx:SetPos(parent.gfx:GetWidth()-closebtn.gfx:GetWidth()-5,b+parent.bT-5)
	
	function closebtn:on_mouse_left_down () gui.HideBuyMenu() end
end

