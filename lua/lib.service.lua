-- manages services offered by stations and planets, e.g. repair, smelting(ore2metal), trade, missions,. ..
-- see also common.servicetypes.lua

gServiceTypes = CreateTypeList()

function RegisterServiceType 	(...) return gServiceTypes:RegisterType(unpack(arg)) end

function GetServiceType (servicetype_or_name_or_id) return gServiceTypes:Get(servicetype_or_name_or_id) end

-- request service from obj (usually a station or planet)
function ClientRequestService(obj,servicetype) ClientSendNetMessage(kNetMessage_Service,obj,servicetype.type_id) end

gMessageTypeHandler_Server.kNetMessage_Service = function (player,obj,iServiceTypeID,...) 
	local servicetype = GetServiceType(iServiceTypeID)   if (not servicetype) then return end
	if (servicetype.server_fun) then servicetype.server_fun(player,servicetype,obj,unpack(arg)) end
end

-- use this send messages generated during service to the player
function ServerServiceSendMessage(player,servicetype,obj,msg)
	ServerSendChatMessage(player.con,msg)
end

-- use this to tell the player that a service cannot be provided for whatever reason
function ServerServiceSendError(player,servicetype,obj,msg)
	ServerServiceSendMessage(player,servicetype,obj,msg)
	-- TODO : nice dialog on client instead of simple chat message ?
end
