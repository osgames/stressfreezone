-- object methods related to equipment
-- equipment : shield battery, accumulator, reactor, armor, engine->speed, afterburner -> energy
--[[
	* equipment system : schildbatterie, panzerplatten usw
		* location : containers/slots keeping object instances need an associated location, the object needs to exist somewhere
		* location : docked fighters and crew are object instances inside a location
		* location : each ship usually has one interior location
		* location : ship might also have an exterior location for sub-objects like weapons attached to the hull
		* cargo : cargo is just a type/amount list
		* transfer : (cargo,equip) between mothership and docked fighters (and nearby ships) should be possible
		* transfer : cargo2equip within ship should be possible
		* transfer : via dragrop : at first vertical slotlist for equipment, type+amount list for cargo
		* location : idea : a containertype for holding a location ? and all objects within can be listed ? 
			** container as highlevel interface for adding/removing stuff from this location, auto-detect pos for add
		* weapon : instances : weapon recharge, etc.
		* weapon : slots have a position 
		* weapon : +equipment later as energy consumptors ? with rate,maxcharge, etc...
			weapons : client needs to know which weapons are there, and their recharge times, e.g. by fire message.
				generic equipment property system similar to object properties ?
		* equipment slots, slotid, type, maxnumber
		* todo : all via property system ? might be possible if arrays are added ? [typename]=amount ?
		* idea : slots can hold more than one equipmentpart, but unsorted, e.g. 2 shield batteries
	* weapon and equip system client requirements 
		* list of cargo content
		* list of equipment slots with slottypes
		* request transfer between different cargo and equip lists (also cargo2cargo and equip2equip)
		* equipment instances in slots need state info, e.g. weapon recharge...  (weapons as objects ?)
	
	container = GetContainer(container_or_id)
	local container = self:GetContainerByType("containertype/cargo")
	local containerlist = obj:ListContainersWithType("slottype/shieldbattery")
	local containerlist = (obj:GetContainerListByType() or {})["slottype/shieldbattery"]
	container:GetSum("weight")
	container:GetContentByType()  for objecttype,amount in pairs(x:GetContentByType() or {}) do 
	container:GetContainerTypeContent(objecttype_or_name_or_id)
	container:SetContainerTypeContent(objecttype_or_name_or_id,amount)
	container:GetObject()
	container:GetType()
	container:GetID()
	local ct = GetContainerType (containertype_or_name_or_id)
	
	local containertypes_common = { ["containertype/cargo"]=1, ["slottype/weapon"]=6, ["slottype/shieldbattery"]=2, ["slottype/armor"]=2 }
	
]]--

function gObjectPrototype:GetActiveEquipmentSum	(objtype_name_or_id,filterfun)
	return self:GetContainerSum(objtype_name_or_id,function (t) return ContainerTypeFilter_ActiveEquipment(t) 
	and ((not filterfun) or filterfun(t)) end)
end
	
function ContainerTypeFilter_ActiveEquipment (t) return t.bActiveEquipment end

function OpenOwnEquipmentDialog () NotifyListener("Hook_Client_OpenOwnEquipmentDialog") end

function ClientRequestEquipCargo (cargotype_or_name_or_id,amount) 
	local cargotype = GetObjectType(cargotype_or_name_or_id)
	ClientSendNetMessage(kNetMessage_RequestEquipCargo,cargotype.type_id,amount) 
end

gMessageTypeHandler_Server.kNetMessage_RequestEquipCargo = function (player,iCargoTypeID,amount) 
	local t = GetObjectType(iCargoTypeID)
	if (not t) then return end
	local body = player.body
	if (not body) then return end
	local cur = body:GetCargo(iCargoTypeID)
	amount = math.max(0,math.min(cur,amount))
	--print("kNetMessage_RequestEquipCargo",player.id,iCargoTypeID,cur)
	if (amount <= 0 or (not body:CanEquip(t))) then return end
	for i = 1,amount do 
		local container = body:GetFreeEquipmentSlot(t.slottype)
		if (not container) then print("no free container of type ",t.slottype) return end
		body:SetCargo(t,cur-1)  -- remove from cargo hold
		body:Equip(t,container) 
	end -- install
end

function ClientRequestUnequip (iContainerID,iObjTypeID) ClientSendNetMessage(kNetMessage_RequestUnequip,iContainerID,iObjTypeID) end

-- todo : realise as container2container transfer
gMessageTypeHandler_Server.kNetMessage_RequestUnequip = function (player,iContainerID,iObjTypeID) 
	local body = player.body
	if (not body) then return end
	body:Unequip(iContainerID,iObjTypeID)
end

	
function gObjectPrototype:GetFreeEquipmentSlot	(slottype_or_name)	
	for containertype,containerlist in pairs(self:GetContainerListByType() or {}) do
		if (containertype == slottype_or_name or containertype.type_name == slottype_or_name) then
			for k,container in pairs(containerlist) do 
				local bSlotIsFree = true
				if (containertype.size and container:GetSum() >= containertype.size) then
					bSlotIsFree = false
				end
				if (bSlotIsFree) then return container end
			end
		end
	end
end

-- does not check if there is a free slot, use gObjectPrototype:GetFreeEquipmentSlot for that
function gObjectPrototype:CanEquip	(objtype_name_or_id)	
	local t = GetObjectType(objtype_name_or_id)
	return t and t.bIsEquipment
end


-- container_or_id can be nil -> autochoose
-- adds the type to cargo if it cannot be equipped
function gObjectPrototype:EquipOrAddToCargo	(objtype_name_or_id,container_or_id)	
	local t = GetObjectType(objtype_name_or_id)
	local bInstalled = false
	if (self:CanEquip(t)) then
		local container = GetContainer(container_or_id) or self:GetFreeEquipmentSlot(t.slottype)
		if (container) then 
			self:Equip(t,container)  
			bInstalled = true
		end
	end
	if (not bInstalled) then self:AddCargo(t,1) end -- add as cargo
	return bInstalled
end

function gObjectPrototype:Equip	(objtype_name_or_id,container_or_id)	
	local t = GetObjectType(objtype_name_or_id)
	assert(self:CanEquip(t))
	local container = GetContainer(container_or_id)
	assert(container,"need to specify a container/slot for equipping")
	local oldamount_container = container:GetContainerTypeContent(t)
	container:SetContainerTypeContent(t,oldamount_container+1)
	if (t.bIsWeapon) then container:AddWeapon(t) end
	-- this is server-only
end

function gObjectPrototype:Unequip (container_or_id,objecttype_or_name_or_id) 
	local container = GetContainer(container_or_id)
	if ((not container) or (container:GetObject() ~= self)) then return end
	local t = GetObjectType(objecttype_or_name_or_id)
	if (not t) then return end
	local oldamount_container = container:GetContainerTypeContent(t)
	if (oldamount_container <= 0) then return end
	container:SetContainerTypeContent(t,oldamount_container-1)
	self:AddCargo(t,1)
	if (t.bIsWeapon) then container:DelWeapon(t) end
	-- this is server-only
end



