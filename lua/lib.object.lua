-- handles objects and types..
-- see also common.objecttypes.lua
-- if this file gets too full, create subfiles like lib.object.*.lua 

gObjectTypes = CreateTypeList()
gNextObjectID = 1
gObjectsByID = {}
		
gObjectPrototype = {}   
dofile(libpath .. "lib.object.search.lua")
dofile(libpath .. "lib.object.destroy.lua")  
dofile(libpath .. "lib.object.pos.lua")  
dofile(libpath .. "lib.object.movement.lua")  
dofile(libpath .. "lib.object.damage.lua")  
dofile(libpath .. "lib.object.furniture.lua") 
dofile(libpath .. "lib.object.hangar.lua") 
dofile(libpath .. "lib.object.properties.lua") 
dofile(libpath .. "lib.object.container.lua") 
dofile(libpath .. "lib.object.cargo.lua") 
dofile(libpath .. "lib.object.equipment.lua") 
dofile(libpath .. "lib.object.weapon.lua") 
dofile(libpath .. "lib.object.projectile.lua") 

-- shorter is alive check for objects
function IsAlive(obj) return obj and obj:IsAlive() end

function GetObjectType (objtype_or_name_or_id) 
	local t = gObjectTypes:Get(objtype_or_name_or_id) 
	if (not t) then print("GetObjectType warning : type not found : ",objtype_or_name_or_id) end
	return t
end
function GetObjectTypeList (filterfun) return gObjectTypes:GetList(filterfun) end

function RegisterObjectType (...) return gObjectTypes:RegisterType(unpack(arg)) end

function GetObject			(obj_or_id)
	if (type(obj_or_id) == "table") then return obj_or_id end
	if (type(obj_or_id) == "number") then return gObjectsByID[obj_or_id] end
	return nil
end

-- called by both client and server, id param can be left out if on server, will be auto-incremented
-- TODO : don't set gfx if dedicated server ? (no dedicated server for now..)
-- vPos as table {x,y,z}
-- qRot as table {w,x,y,z}
-- id only neeeded on client
function SpawnObject (objtype_or_name_or_id,location_or_id,vPos,qRot,id)
	local obj = CreateObject()
	-- wrap some memberfunctions
	obj._Destroy = obj.Destroy -- keep c func
	obj._SetParentLocation = obj.SetParentLocation -- keep c func
	ArrayOverwrite(obj,gObjectPrototype)
	
	-- id
	if (not id) then id = gNextObjectID gNextObjectID = gNextObjectID + 1 end
	obj:SetID(id)
	obj.id = id
	gObjectsByID[id] = obj
	
	-- determine type
	obj.objtype = GetObjectType(objtype_or_name_or_id)
	assert(obj.objtype,"SpawnObject : objectype not found : "..tostring(objtype_or_name_or_id))
	--printdebug("spawn","spawn "..obj.objtype.type_name)
	
	-- initial pos
	obj.mvPos = vPos
	obj.mqRot = qRot
	local location = GetLocation(location_or_id)
	
	-- gfx
	obj.gfx = obj.objtype:constructor(location.gfx)
	obj:SetGfx(obj.gfx)
	
	-- set parent location (should be done AFTER gfx is created, as it triggers notify)
	obj:SetParentLocation(location,true) -- don't use obj.location before this line
	
	-- send message to network
	if (gbServerRunning) then for id,player in pairs(gServerPlayers) do obj:ServerSendSpawn(player.con) end end
	
	-- create containers
	if (gbServerRunning and obj.objtype.containertypes) then 
		for container_typename,amount in pairs(obj.objtype.containertypes) do
			for i=1,amount do SpawnContainer(container_typename,obj) end
		end
	end
	
	-- objecttype spawn callback
	if (obj.objtype.spawncallback) then obj.objtype.spawncallback(obj) end
	
	
	obj.mfBoundingRad = obj:GetMousePickRad() -- TODO : this is just a work-around, clean this up by fixing gfx3d:GetWorldAABB
	
	--[[
	if gbServerRunning and gOdeWorld then
		local radius = obj.objtype.phys_radius or 1
		local mass = obj.objtype.phys_mass or 1
		-- set ode body
		obj.odeobject = gOdeWorld:CreateObject(unpack(vPos))
		obj.odeobject:SetShapeSphere(radius,mass)
		obj.odeobject:AddForceAtRelPos(1000,1000,1000, 1000,1000,1000)
		obj:SetPhysicForm(obj.odeobject)
	end
	]]--
	
	NotifyListener("Hook_Object_SpawnObject",obj)
	
	return obj
end

--- sends kNetMessage_SpawnObject : iifffffffi : (objtypeid,locationid,vPos,qRot,objid)
function gObjectPrototype:ServerSendSpawn (con) 
	local x,y,z = unpack(self.mvPos)
	local qw,qx,qy,qz = unpack(self.mqRot)
	SendNetMessage(con,kNetMessage_SpawnObject,self.objtype.type_id,self.location.id,x,y,z,qw,qx,qy,qz,self.id)
end

gMessageTypeHandler_Client_Remote.kNetMessage_SpawnObject = function (objtype_id,parentloc_id,x,y,z,qw,qx,qy,qz,new_id) 
	SpawnObject(objtype_id,parentloc_id,{x,y,z},{qw,qx,qy,qz},new_id)
end




function gObjectPrototype:GetParentLocation () return self.location end
function gObjectPrototype:GetRootLocation () return self:GetParentLocation() end -- todo : adjust for hierarchy

-- bNoBroadCast : do not broadcast message to clients, only for initial setparentloc during objectspawn
function gObjectPrototype:SetParentLocation (location_or_id,bNoBroadCast)
	local newloc = GetLocation(location_or_id)
	local oldloc = self.location
	if (self.location) then self.location.childs[self.id] = nil end -- remove from old
	self.location = newloc
	if (self.location) then self.location.childs[self.id] = self end -- insert into new
	self:_SetParentLocation(self.location)
	NotifyListener("Hook_Object_SetParentLocation",self,oldloc,newloc)
	
	if (self.gfx) then self.gfx:SetParent(newloc.gfx) end
	
	-- broadcast to all players
	if (gbServerRunning and (not bNoBroadCast)) then 
		local x,y,z = unpack(self.mvPos)
		local qw,qx,qy,qz = unpack(self.mqRot)	
		ServerBroadcastMessage(kNetMessage_ObjectChangeLoc,self.id,newloc and newloc.id or 0,x,y,z,qw,qx,qy,qz) 
	end
end

function gObjectPrototype:SetVisible (bVisible) 
	if (self.gfx) then self.gfx:SetVisible(bVisible) end
end

-- TODO : don't use this yet, client must be able to receive resync-high-set from server spawn,worldsend,changeloc...
-- server only, increments resync-high... todo : send to all clients ? not when called from changeloc ?
function gObjectPrototype:ServerIncResyncHigh () 
	--self.miResyncCounterHigh = self.miResyncCounterHigh + 1
	--self:ResetResyncCounterLow()
	-- TODO : not yet implemented, especially on client
end


-- text for lists like in trade : cost={["cargo/iron"]=50}  -> "iron:50"
function GetObjTypeListText (objtypelist) 
	local res = ""
	for objtypename,amount in pairs(objtypelist) do
		if (res ~= "") then res = res .. "," end
		res = res .. sprintf("%s:%d",GetObjectType(objtypename).name,amount)
	end
	return res
end

-- text for lists like in trade : cost={["cargo/iron"]=50}  -> "50 iron"
function GetObjTypeListTextHuman (objtypelist) 
	local res = ""
	for objtypename,amount in pairs(objtypelist) do
		if (res ~= "") then res = res .. "," end
		res = res .. sprintf("%d %s",amount,GetObjectType(objtypename).name)
	end
	return res
end

function GetObjTypeListWeight (objtypelist) 
	local res = 0
	for objtypename,amount in pairs(objtypelist) do
		res = res + GetObjectType(objtypename).weight * amount
	end
	return res
end

-- mark as changed so resync messages are being sent
function gObjectPrototype:MarkChanged	() self.miLastChangeTime = gMyTicks end


-- returns x,y,z
function gObjectPrototype:GetRandomPositionAtDist (dist) return GetRandomPositionAtDist(dist,unpack(self.mvPos)) end

