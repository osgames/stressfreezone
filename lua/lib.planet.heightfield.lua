-- heightfields on planets

function MakePlane (parentgfx,size_xz,size_uv,matname)
	local gfx = parentgfx:CreateChild()
	gfx:SetSimpleRenderable()
	local iVertexCount = 4
	local iIndexCount = 6
	gfx:RenderableBegin(iVertexCount,iIndexCount,false,false,OT_TRIANGLE_LIST)
	local e = size_xz 
	local f = size_uv 
	gfx:RenderableVertex(-e,0,-e, 0,0)
	gfx:RenderableVertex( e,0,-e, f,0)
	gfx:RenderableVertex(-e,0, e, 0,f)
	gfx:RenderableVertex( e,0, e, f,f)
	gfx:RenderableIndex3(0,2,1)
	gfx:RenderableIndex3(1,2,3)
	gfx:RenderableEnd()
	gfx:SetMaterial(GetPlainTextureMat(matname))
	return gfx
end

function HF_GetLimit		(hf,x,y)	return hf[max(0,min(hf.cx,x))			..","..max(0,min(hf.cy,y))] end 
function HF_GetLimitFloor	(hf,x,y)	return hf[max(0,min(hf.cx,floor(x)))	..","..max(0,min(hf.cy,floor(y)))] end

function HF_Make (cx,cy,fun)
	local hf = {cx=cx,cy=cy}
	for x = 0,cx do 
	for y = 0,cy do hf[x..","..y] = fun(x,y) end end
	return hf
end

-- creates a bigger-scaled copy of hf2 (by factor s) , cx,cy = s*hf2.cx,s*hf2.cy
function HF_MakeScale	(hf_src,s)
	assert(s >= 1 and s == math.floor(s)) -- s must be integer
	return HF_Make(s*hf_src.cx,s*hf_src.cy,function (x,y) 
		local ix,iy = x/s,y/s -- the projection of x,y to the smaller hf_src, non-int !
		local x0,x1, y0,y1 = floor(ix),ceil(ix), floor(iy),ceil(iy) -- edge coordinates in hf_src
		local fx = (x0 == x1) and 0 or ((ix-x0)/(x1-x0)) -- x fraction
		local fy = (y0 == y1) and 0 or ((iy-y0)/(y1-y0)) -- y fraction
		return	((1.0-fx)*hf_src[x0..","..y0] + fx*hf_src[x1..","..y0])*(1.0-fy) +  -- linear interpolation between the 4 edges
				((1.0-fx)*hf_src[x0..","..y1] + fx*hf_src[x1..","..y1])*     fy
		end)
end

-- generates a random heightfield by adding a random value in [0,h] to every point, and adding a
function HF_MakeRandom		(cx,cy,h,a)		a = a or 0 return HF_Make(cx,cy,function () return h*math.random() + a end) end

-- makes a copy of hf_src, adding a random value in [0,h] to every point, and adding a
function HF_MakeRandomAdd	(hf_src,h,a)	a = a or 0 return HF_Make(hf_src.cx,hf_src.cy,function (x,y) return hf_src[x..","..y] + h*math.random() + a end) end


-- smoothing in 9x9 matrix : middle*f0 + up,left,right,top*f1 + diagonals*f2
function HF_MakeSmooth		(hf_src,f0,f1,f2)
	local isum = 1.0 / (f0 + 4*f1 + 4*f2)
	f0 = f0 * isum
	f1 = f1 * isum
	f2 = f2 * isum
	return HF_Make(hf_src.cx,hf_src.cy,function (x,y) 
		return	f0*(	HF_GetLimit(hf_src,x,y)		) + 
				f1*(	HF_GetLimit(hf_src,x+1,y)		+	
						HF_GetLimit(hf_src,x,y+1)		+	
						HF_GetLimit(hf_src,x-1,y)		+	
						HF_GetLimit(hf_src,x,y-1)		) + 
				f2*(	HF_GetLimit(hf_src,x+1,y+1)		+	
						HF_GetLimit(hf_src,x+1,y-1)		+	
						HF_GetLimit(hf_src,x-1,y+1)		+	
						HF_GetLimit(hf_src,x-1,y-1)		)
		end) 
end


-- writes a buffer of cx*cy floats : just the height values, row after row
function HF_ToFIFO_Height (hfdata, fifo) 
	-- local fifo = CreateFIFO()
	for y = 0,hfdata.cy do 
	for x = 0,hfdata.cx do fifo:PushF(hfdata[x..","..y]) end end
	return fifo
end

-- writes a buffer of cx*cy vertices x,y,z , row by row    
function HF_ToFIFO_Vertices (hfdata, fifo) 
	-- local fifo = CreateFIFO() -- 
	for y = 0,hfdata.cy do 
	for x = 0,hfdata.cx do fifo:PushF(x) fifo:PushF(y) fifo:PushF(hfdata[x..","..y]) end end
	return fifo
end

-- writes a buffer of (cx-1)*(cy-1)*6 vertices x,y,z, every 3 vertices are one triangle   
function HF_ToFIFO_Triangles (hfdata, fifo) 
	local HF_ODE_Vertex = function (x,y) fifo:PushF(x) fifo:PushF(y) fifo:PushF(hfdata[x..","..y]) end
	for x = 0,hfdata.cx-1 do  
	for y = 0,hfdata.cy-1 do 
		HF_ODE_Vertex(x  ,y  ) -- 0
		HF_ODE_Vertex(x  ,y+1) -- 2
		HF_ODE_Vertex(x+1,y  ) -- 1
		HF_ODE_Vertex(x+1,y  ) -- 1
		HF_ODE_Vertex(x  ,y+1) -- 2
		HF_ODE_Vertex(x+1,y+1) -- 3
	end end
	return fifo
end


function MakeRandomHeightField (parentgfx,matname,triangleFifo) 
	local cx,cy,s,h1,h2 = 32,32, 10, 80,5
	cx,cy = unpack(gTestHeightFieldResolution)
	local r = 4
	
	local hf0 = HF_MakeSmooth(HF_MakeRandom(cx/r,cy/r,h1,-0.5*h1),1,0.5,0.2)
	local hf1 = HF_MakeScale(hf0,r) 
	local hf2 = HF_MakeRandomAdd(hf1,h2,-0.5*h2)
	
	-- heightfield function
	local fun1 = function(x,y)
		x = x
		y = y
		if hf2[x..","..y] then
			return hf2[x..","..y]
		else
			return 0
		end
		-- return math.sin(x*y*0.02)*2+math.log(1+x*y)
	end
		
	local uvscale = 1.0/4.0
	local gfx = MakeHeightField(parentgfx,cx,cy,s,matname,uvscale,fun1)
	-- gfx:SetPosition(-0.5*s*cx,0,-0.5*s*cy)
	gfx:SetPosition(0,0,0)
	
	if triangleFifo then
		-- for ode : local fifo_triangles	= HF_ToFIFO_Triangles(hf2)
		HF_ToFIFO_Triangles(hf2, triangleFifo)
	end
	-- for ode : local fifo_height		= HF_ToFIFO_Height(hf2)
	-- for ode : local fifo_vertices	= HF_ToFIFO_Vertices(hf2)

	-- heightfield function
	local fun2 = function(x,y)
		return fun1(x / s,y / s)
		--[[
		if hf2[x..","..z] then
			return hf2[x..","..z]
		else
			return 0
		end
		]]--
	end
	
	return gfx, fun2, cx * s, cy * s, s, s
end

-- useful for heigthfields, assumes triangles : (z00,z10,z01)  (z11,z10,z01)
function InterpolateTriSquare (z00,z10,z01,z11,fx,fy) -- z00=left_top:fx=fy=0   z10=right_top:fx=1,fy=0
	local     m = fx *   0 + (1.0 - fx) *   1 -- y coordinate of the diagonal
	if (fx + fy < 1) then -- upper triangle with z00
		local t = fx * z10 + (1.0 - fx) * z00
		local b = fx * z10 + (1.0 - fx) * z01
		fy = (m > 0) and (fy / m) or 0
		return fy * b + (1.0 - fy) * t
	else -- lower triangle with z11
		local t = fx * z10 + (1.0 - fx) * z01
		local b = fx * z11 + (1.0 - fx) * z01
		fy = ((1 - m) > 0) and ((fy - m) / (1 - m)) or 0 
		return fy * b + (1.0 - fy) * t
	end
end

-- useful for heigthfields
function InterpolateSquare (z00,z10,z01,z11,fx,fy) -- z00=left_top:fx=fy=0   z10=right_top:fx=1,fy=0
	local t = fx * z10 + (1.0 - fx) * z00
	local b = fx * z11 + (1.0 - fx) * z01
	return fy * b + (1.0 - fy) * t
end


function MakeHeightField (parentgfx,cx,cy,s,matname,uvscale,fun)
	local HFGet		= function (x,y) return fun(x,y) end
	
	-- cache normals
	local cache_zn = {}
	local nx,ny,nz,z
	
	for x = 0,cx do 
	for y = 0,cy do 
		z = HFGet(x,y)
			if (x == 0) then		nx,ny,nz = 0,-1,0
		elseif (y == 0) then		nx,ny,nz = 0,-1,0
		elseif (x == cx) then		nx,ny,nz = 0,-1,0
		elseif (y == cy) then		nx,ny,nz = 0,-1,0
		else						nx,ny,nz = Vector.cross(	0,	HFGet(x,y+1)-HFGet(x,y-1), 2*s,
																2*s,HFGet(x+1,y)-HFGet(x-1,y), 0) 
		end
		cache_zn[x..","..y] = {z,Vector.normalise(nx,ny,nz)} 
	end end
	local HFGetZN	= function (x,y) return unpack(cache_zn[x..","..y]) end
	
	-- generate geometry
	local gfx = parentgfx:CreateChild() -- CreateGfx3D()
	gfx:SetSimpleRenderable()
	local iVertexCount	= (cx)*(cy)*4
	local iIndexCount	= (cx)*(cy)*6
	gfx:RenderableBegin(iVertexCount,iIndexCount,false,false,OT_TRIANGLE_LIST)
	-- x,y,z,nx,ny,nz,u,v,	r,g,b,a
	local HFVertex = function (x,y,u,v) 
		local z,nx,ny,nz = HFGetZN(x,y)
		gfx:RenderableVertex(s*x,z,s*y, nx,ny,nz, x*uvscale,y*uvscale) 
		-- print("VERTEX",s*x,z,s*y)
	end
	local vc = 0
	for x = 0,cx-1 do 
	for y = 0,cy-1 do
		HFVertex(x  ,y  )
		HFVertex(x+1,y  )
		HFVertex(x  ,y+1)
		HFVertex(x+1,y+1)
		gfx:RenderableIndex3(vc+0,vc+2,vc+1)
		gfx:RenderableIndex3(vc+1,vc+2,vc+3)
		vc = vc + 4
	end end
	gfx:RenderableEnd()
	gfx:SetMaterial(matname)
	
	function gfx:GetHeightAtPos (x,z)
		x = x/s z = z/s
		local l,t = floor(x),floor(z)  -- coords
		local z00,z10,z01,z11 = HFGet(l,t),HFGet(l+1,t),HFGet(l,t+1),HFGet(l+1,t+1)
		return z00 and z10 and z01 and z11 and InterpolateTriSquare(z00,z10,z01,z11,x-l,z-t)
	end
	
--~ 	local sMeshName = gfx:RenderableConvertToMesh() -- first
--~ 	gfx:Destroy()
--~ 	local gfx = parentgfx:CreateChild()
--~ 	gfx:SetMesh(sMeshName) 
	return gfx
end

--~ gPlanetBaseTex = "sd_metall_6.png"
gPlanetBaseTex = "ba_ccby_TiZeta_cem.jpg"
--~ function MakePlanetGround (loc) return MakePlane(loc.gfx,100,50,gPlanetBaseTex) end
function MakePlanetGround (loc) 
	local r,g,b = hex2rgb("#a19429")
	local gfx,fun,w,h = MakeRandomHeightField(loc.gfx,GetHuedMat(GetPlainTextureMat(gPlanetBaseTex),r,g,b))
	return gfx
end


