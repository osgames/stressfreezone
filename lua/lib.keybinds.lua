-- binds common keys
-- see also lugre/lua/lib.input.lua


kSFZCamMaxZoom = 600000
CamSetZoomLimit(0,kSFZCamMaxZoom)


UnbindAll()



--BindDown("m",function () print("a") asdasdasdads() end)

-- soemthing
BindDown("escape",	function () if (not gActiveEditText) then Terminate() else DeactivateCurEditText() end end)
BindDown("v",		function () if (not gActiveEditText) then AddFadeLines("screenshot") Client_TakeScreenshot(gScreenShotPrefix) end end)
BindDown("f12",		function () if (not gActiveEditText) then Client_TakeGridScreenshot(gScreenShotPrefix) end end)

BindDown("f9",		function () if (not gActiveEditText) then MissionTest() end end)
BindDown("f10",		function () if (not gActiveEditText) then RegeneratePlanetGround() end end)

BindDown("f1",		function () if (not gActiveEditText) then FadeLineToggleShowAll() end end)
BindDown("f2",		function () if (not gActiveEditText) then OpenOwnCargoDialog() end end)
BindDown("f3",		function () if (not gActiveEditText) then OpenOwnEquipmentDialog() end end)
BindDown("f4",		function () if (not gActiveEditText) then OpenOwnMissionDialog() end end)
BindDown("f5",		function () if (not gActiveEditText) then OpenFurnitureSaveDialog() end end)
BindDown("f6",		function () if (not gActiveEditText) then OpenFurnitureLoadDialog() end end)
BindDown("f7",		function () if (not gActiveEditText) then ClientRequestTeleportToMousePick() end end)
BindDown("f8",		function () if (not gActiveEditText) then 
		if (gbServerRunning) then 
			AddFadeLines("spawning group that attacks the player")
			local g = SpawnEnemyGroup({["shiptype/customship/xwing"]=1,["shiptype/pirate"]=2},10,0,0,0)
			g:Attack(gClientBody)
		end

	end end)

Bind("l",			function (state) if (not gActiveEditText) then SetLogoVisible(state > 0) end end)

BindDown("t",			function (state) if (not gActiveEditText) then 
	if gKeyPressed[key_lcontrol] then
		CallCurrentTarget()
	else
		TargetCurrentCall()
	end
end end)

-- chatline
BindDown("return",	function () ChatLine_ToggleActive() end)
BindDown("down",    function () if (gActiveEditText) then ChatLine_HistoryUpDown(-1) end end) 
BindDown("up",      function () if (gActiveEditText) then ChatLine_HistoryUpDown( 1) end end) 


--Bind("y",			function (state) if (not gActiveEditText) then if (state > 0) then CamChangeZoom( 1) end end end)
--Bind("x",			function (state) if (not gActiveEditText) then if (state > 0) then CamChangeZoom(-1) end end end)
Bind("wheelup",		function (state) if (not gActiveEditText) then if (state > 0) then CamChangeZoom( 1) end end end)
Bind("wheeldown",	function (state) if (not gActiveEditText) then if (state > 0) then CamChangeZoom(-1) end end end)

--[[
-- camera controlling
Bind("c",		function (state) if (not gActiveEditText) then if (state > 0) then gCurrentRenderer:ChangeCamMode() end end end)
Bind("x",		function (state) if (not gActiveEditText) then if (state > 0) then gCurrentRenderer:CamChangeZoom( 1) end end end)
Bind("y",		function (state) if (not gActiveEditText) then if (state > 0) then gCurrentRenderer:CamChangeZoom(-1) end end end)
]]--

function BindActionKeys () 
	
	-- todo : config
	gClientCtrlKeyMap[kCtrlKeyMask_Left]		= GetNamedKey("a")
	gClientCtrlKeyMap[kCtrlKeyMask_Right]		= GetNamedKey("d")
	gClientCtrlKeyMap[kCtrlKeyMask_Up]			= GetNamedKey("r")
	gClientCtrlKeyMap[kCtrlKeyMask_Down]		= GetNamedKey("f")
	gClientCtrlKeyMap[kCtrlKeyMask_Fwd]			= GetNamedKey("w")
	gClientCtrlKeyMap[kCtrlKeyMask_Back]		= GetNamedKey("s")
	gClientCtrlKeyMap[kCtrlKeyMask_Break]		= GetNamedKey("x")
	gClientCtrlKeyMap[kCtrlKeyMask_RollLeft]	= GetNamedKey("q")
	gClientCtrlKeyMap[kCtrlKeyMask_RollRight]	= GetNamedKey("e")
	gClientCtrlKeyMap[kCtrlKeyMask_FirePrimary]	= GetNamedKey("space")
	
	
	-- TODO : only if not in guimode
	--BindDown("tab",			function () if (not gActiveEditText) then ToggleGuiMode() end end)
	
	RegisterListener("mouse_left_click",function () ClientLeftClick() end)
	--BindDown("mouse1",		function () if (not gActiveEditText) then ClientLeftClick() end end) 
	BindDown("mouse2",		function () if (not gActiveEditText) then ClientRightClick() end end) 
	--BindDown("m",			function () if (not gActiveEditText) then ClientSendActionKey(kActionKey_FireMissile) end end) 
	BindDown("b", 			function () if (not gActiveEditText) then ClientSendActionKey(kActionKey_Debug01) end end) 
	BindDown("n", 			function () if (not gActiveEditText) then ClientSendActionKey(kActionKey_Debug02) end end) 
	BindDown("c", 			function () if (not gActiveEditText) then ClientChooseTarget_NearestHostile() end end) 
	BindDown("backspace", 	function () if (not gActiveEditText) then ClientChooseTarget_NoTarget() end end) 
	BindDown("tab",			function () if (not gActiveEditText) then ClientChooseTarget_Next() end end)
	BindDown("y",			function () if (not gActiveEditText) then ClientChooseTarget_Prev() end end)
	--BindDown("b",			function () if (not gActiveEditText) then print("campos",GetMainCam():GetPos()) gTableCamAngleH = 0 gTableCamAngleV = 90*gfDeg2Rad end end)

	-- fire all weapons on space and request a new body
	BindDown("space",		function () if (not gActiveEditText) then 
		for i = 1,10 do ClientSendActionKey(_G["kActionKey_FireSlot" .. i]) end
		RequestBody() 
	end end)

	-- assign key 1 to 10 to fire slots 1 to 10
	for i = 1,10 do 
		local slotnumber = i
		BindDown(tostring(slotnumber),function () if (not gActiveEditText) then ClientSendActionKey(_G["kActionKey_FireSlot" .. slotnumber]) end end)
	end

	--BindDown("space",		function () if (not gActiveEditText) then ClientSendActionKey(kActionKey_FirePrimary) RequestBody() end end)
	
	BindDown("a",			function () if (not gActiveEditText) then ClientSendActionKey(kActionKey_TurnLeft) end end)
	BindDown("d",			function () if (not gActiveEditText) then ClientSendActionKey(kActionKey_TurnRight) end end)
	BindDown("w",			function () if (not gActiveEditText) then ClientSendActionKey(kActionKey_WalkFwd) end end)
	BindDown("s",			function () if (not gActiveEditText) then ClientSendActionKey(kActionKey_WalkBack) end end)
	
	BindDown("left",		function () if (not gActiveEditText) then ClientSendActionKey(kActionKey_TurnLeft) end end)
	BindDown("right",		function () if (not gActiveEditText) then ClientSendActionKey(kActionKey_TurnRight) end end)
	BindDown("up",			function () if (not gActiveEditText) then ClientSendActionKey(kActionKey_WalkFwd) end end)
	BindDown("down",		function () if (not gActiveEditText) then ClientSendActionKey(kActionKey_WalkBack) end end)
	
	-- shortcuts
	BindDown("q",			function () if (not gActiveEditText) then if ( gClientTarget ) then ClientAutoPilot_Approach(gClientTarget) end end end)
end


-- shipedit : tool keybindings
glShipEditToolKeybinding = {
	{key="q",tool="MirrorX",parttype=nil},
	{key="w",tool="MirrorY",parttype=nil},
	{key="e",tool="MirrorZ",parttype=nil},
	{key="a",tool="Rot90X",parttype=nil},
	{key="s",tool="Rot90Y",parttype=nil},
	{key="d",tool="Rot90Z",parttype=nil},
	{key="1",tool="Place",parttype="shippart/box"},
	{key="2",tool="Place",parttype="shippart/ramp1"},
	{key="3",tool="Place",parttype="shippart/ramp2"},
	{key="4",tool="Place",parttype="shippart/pyramid1"},
	{key="5",tool="Place",parttype="shippart/pyramid2"},
}
