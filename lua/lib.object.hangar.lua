-- handles docking and launching fighters from mothership

function gObjectPrototype:IsSpaceSuit	()	return self.objtype.bIsSpaceSuit end
function gObjectPrototype:IsShip		()	return self.objtype.bIsShip end
function gObjectPrototype:HasHangar		()	return self.objtype.bHasHangar end



-- ##### ##### ##### ##### ##### 
-- ##### ##### ##### ##### ##### landing on planets
-- ##### ##### ##### ##### ##### 

function ClientIsOnPlanet 			() return gActiveLocation and gActiveLocation.loctype.bIsPlanet end
function ClientRequestLeavePlanet	() ClientSendNetMessage(kNetMessage_RequestLeavePlanet) end
function ClientRequestLandOnPlanet	(planet) ClientSendNetMessage(kNetMessage_RequestLandOnPlanet,planet) end
function gObjectPrototype:IsOnPlanet	() return self:GetRootLocation().loctype.bIsPlanet end
function gObjectPrototype:IsLandable	() return self.objtype.bHasSpacePort end

gMessageTypeHandler_Server.kNetMessage_RequestLeavePlanet = function (player)
	local body = player.body
	if (not body) then return end
	if (not body:IsAlive()) then return end
	local planet = body:GetRootLocation().obj_planet
	if (not planet) then return end
	
	local planet_parent_loc = planet:GetParentLocation()
	
	local x,y,z = Vector.random3(planet:GetMousePickRad()*4)
	body.mvPos = {Vector.add(x,y,z,unpack(planet.mvPos))}
	body.mqRot = {Quaternion.identity()}
	body:MarkChanged()
	body:SetParentLocation(planet_parent_loc)
end


gMessageTypeHandler_Server.kNetMessage_RequestLandOnPlanet = function (player,planet)
	local body = player.body
	if (not planet) then return end
	if (not body) then return end
	if (not body:IsAlive()) then return end
	
	local planetloc = planet:GetPlanetLocation()
	body.mvVel = {0,0,0}
	body.mvPos = {0,0,0}
	body.mqRot = {Quaternion.identity()}
	body:MarkChanged()
	body:SetParentLocation(planetloc)
--~ 	ServerBroadcastMessage(kNetMessage_LocationSetShipInterior,planetloc.id,oldbody.objtype.type_id)
--~ 	
--~ 	local x,y,z = ShipVoxelGrid_GetInteriorStartPos(oldbody:GetVoxelGrid())
--~ 	local newbody = SpawnObject("shiptype/spacesuit",planetloc,{x,y,z},{Quaternion.identity()})
--~ 	ServerPlayerSetBody(player,newbody)
--~ 	player.is_inside_ship = oldbody
end

function gObjectPrototype:GetPlanetLocation	() 
	if (not self.planet_loc) then 
		self.planet_loc = SpawnLocation("locationtype/onplanet") 
		self.planet_loc.obj_planet = self
	end
	return self.planet_loc 
end

-- ##### ##### ##### ##### ##### 
-- ##### ##### ##### ##### ##### view inside/outside
-- ##### ##### ##### ##### ##### 



function ClientIsInsideShip 			() return gActiveLocation and gActiveLocation.loctype.bIsInsideShip end
function ClientRequestViewShipInside	() ClientSendNetMessage(kNetMessage_RequestViewShipInside) end
function ClientRequestViewShipOutside	() ClientSendNetMessage(kNetMessage_RequestViewShipOutside) end

function gObjectPrototype:InsideShip_GetShip	() return self.player and self.player.is_inside_ship end -- TODO : quick hack
function gObjectPrototype:IsInsideShip 			() return self.player and self.player.is_inside_ship end -- TODO : quick hack
function gObjectPrototype:GetInteriorLocation	() 
	if (not self.interior_loc) then self.interior_loc = SpawnLocation("locationtype/shipinterior") end
	return self.interior_loc 
end

function gObjectPrototype:GetShipFilePath ()
	return self.objtype and self.objtype.gfx_shipfile and gShipDirPath .. self.objtype.gfx_shipfile
end

function gObjectPrototype:GetVoxelGrid ()
	if (not self.shipvoxelgrid) then 
		self.shipvoxelgrid = ShipVoxelGrid_CreateFromFilePath(self:GetShipFilePath(),false)
	end
	return self.shipvoxelgrid
end

-- throw everything outside when ship is destroyed
function gObjectPrototype:DestroyInterior ()
	if (not self.interior_loc) then return end
	for k,obj in pairs(self.interior_loc.childs) do 
		if (obj.player) then obj.player.is_inside_ship = nil end
		obj.mvPos = self.mvPos
		obj.mvVel = self.mvVel
		obj.mqRot = self.mqRot
		obj:SetParentLocation(self:GetParentLocation())
	end
end

gMessageTypeHandler_Server.kNetMessage_RequestViewShipInside = function (player)
	local oldbody = player.body
	if (not oldbody) then return end
	if (not oldbody:IsShip()) then return end
	if (oldbody:IsSpaceSuit()) then return end
	
	local interior = oldbody:GetInteriorLocation()
	ServerBroadcastMessage(kNetMessage_LocationSetShipInterior,interior.id,oldbody.objtype.type_id)
	
	local x,y,z = ShipVoxelGrid_GetInteriorStartPos(oldbody:GetVoxelGrid())
	local newbody = SpawnObject("shiptype/spacesuit",interior,{x,y,z},{Quaternion.identity()})
	ServerPlayerSetBody(player,newbody)
	player.is_inside_ship = oldbody
end

gMessageTypeHandler_Server.kNetMessage_RequestViewShipOutside = function (player)
	local oldbody = player.body
	if (not oldbody) then return end
	if (not player.is_inside_ship) then return end
	local newbody = player.is_inside_ship
	ServerPlayerSetBody(player,newbody)
	player.is_inside_ship = nil
	oldbody:Destroy()
end

gMessageTypeHandler_Client.kNetMessage_LocationSetShipInterior = function (locid,objtype_id)
	local loc = GetLocation(locid)
	local objtype = GetObjectType(objtype_id)
	if ((not loc) or (not objtype)) then return end
	if (loc.gfx_shipInterior) then loc.gfx_shipInterior:Destroy() loc.gfx_shipInterior = nil end
	loc.gfx_shipInterior = loc.gfx:CreateChild()
	LoadShipToGfx(loc.gfx_shipInterior,gShipDirPath .. objtype.gfx_shipfile,true)
end



-- ##### ##### ##### ##### ##### 
-- ##### ##### ##### ##### ##### fighter
-- ##### ##### ##### ##### ##### 



function ClientRequestLaunchFighter	()		ClientSendNetMessage(kNetMessage_RequestLaunchFighter) end
function ClientRequestDockToObject	(obj)	ClientSendNetMessage(kNetMessage_RequestDock,obj) end

gMessageTypeHandler_Server.kNetMessage_RequestLaunchFighter = function (player)
	local oldbody = player.body
	if (not oldbody) then return end
	if (not oldbody:HasHangar()) then return end
	
	if (oldbody.player == player) then oldbody.player = nil end
	
	local fighter = SpawnObject("shiptype/customship/fighter",oldbody:GetParentLocation(),oldbody.mvPos,oldbody.mqRot)
	fighter.mvVel = oldbody.mvVel
	fighter:MarkChanged()
	ServerPlayerSetBody(player,fighter)
end

gMessageTypeHandler_Server.kNetMessage_RequestDock = function (player,obj)
	if (not obj) then return end
	if (not obj:HasHangar()) then return end
	
	local oldbody = player.body
	if (not oldbody) then return end
	
 	ServerPlayerSetBody(player,obj)
	--oldbody:SetParentLocation(obj:GetInteriorLocation())
 	oldbody:Destroy() -- todo : add to hangar instead of destroying !
end



-- ##### ##### ##### ##### ##### 
-- ##### ##### ##### ##### ##### space suit 
-- ##### ##### ##### ##### ##### 



function CanExitObject	(obj,owner) return obj and obj:CanExitShip(owner) end
function CanEnterObject	(obj,owner) return obj and obj:CanEnterShip(owner) end

function gObjectPrototype:CanExitShip	(owner) return (not self:IsSpaceSuit()) end
function gObjectPrototype:CanEnterShip	(owner) return (not self.bIsPirate) and self:IsShip() end

function ClientRequestExitShip ()
	if (not CanExitObject(gClientBody,gClientOwner)) then return end
	ClientSendNetMessage(kNetMessage_RequestExitObject,gClientBody)
end

function ClientRequestEnterShip (obj)
	if (not CanEnterObject(obj,gClientOwner)) then return end
	ClientSendNetMessage(kNetMessage_RequestEnterObject,obj)
end

gMessageTypeHandler_Server.kNetMessage_RequestEnterObject = function (player,obj)
	if ((not obj) or (not player.body)) then return end
	if (not obj:IsShip()) then return end
	local oldbody = player.body
	if (not oldbody:IsSpaceSuit()) then return end
	if (obj:IsSpaceSuit()) then return end
	ServerPlayerSetBody(player,obj)
	oldbody:Destroy()
end

gMessageTypeHandler_Server.kNetMessage_RequestExitObject = function (player,obj)
	if ((not obj) or (not player.body)) then return end
	local oldbody = player.body
	if (oldbody:IsSpaceSuit()) then return end
	local newbody = SpawnObject("shiptype/spacesuit",oldbody:GetParentLocation(),oldbody.mvPos,oldbody.mqRot)
	newbody.mvVel = oldbody.mvVel
	newbody:MarkChanged()
	ServerPlayerSetBody(player,newbody)
end

