-- see also lib.object.hangar.lua

function ClientRequestTeleportToMousePick ()
--~ 	local loc = gActiveLocation
--~ 	
--~ 	loc.gfx_shipInterior = loc.gfx:CreateChild()
--~ 	LoadShipToGfx(loc.gfx_shipInterior,gShipDirPath .. objtype.gfx_shipfile,true
	local x,y,z = unpack(gClientBody.mvPos)
	y = y + 1
	ClientRequestTeleport(x,y,z) -- todo : currently only moves the player up one field
end

function ClientRequestTeleport (x,y,z) ClientSendNetMessage(kNetMessage_RequestTeleport,x,y,z) end

gMessageTypeHandler_Server.kNetMessage_RequestTeleport = function (player,x,y,z)
	-- todo : this is a cheat, enable only in debug mode or so
	local body = player.body
	if (not body) then return end 
	-- if (not body:IsInsideShip()) then return end
	body.mvPos = {x,y,z}
	body:MarkChanged()
end

function ClientRequestTurnFurniture (obj,degree)
	ClientSendNetMessage(kNetMessage_RequestTurnFurniture,obj,degree*gfDeg2Rad)
end

gMessageTypeHandler_Server.kNetMessage_RequestTurnFurniture = function (player,obj,ang)
	-- TODO : cheat protect : only turn furniture in same location
	if ((not obj) or (not obj.objtype.bFurniture)) then return end
	local body = player.body
	if ((not body) or (not body:IsInsideShip())) then return end
	local qw,qx,qy,qz = unpack(obj.mqRot)
	local aw,ax,ay,az = Quaternion.fromAngleAxis(ang,0,1,0)
	obj.mqRot = {Quaternion.Mul(aw,ax,ay,az,qw,qx,qy,qz)}
	obj:MarkChanged()
end

function InteriorSaveToFile (filepath)  
	AddFadeLines(sprintf("InteriorSaveToFile %s",filepath))
	local loc = gActiveLocation
	
	local f = io.open(filepath,"w")
	if (f) then
		for k,obj in pairs(loc:GetChilds()) do
			local typename = obj.objtype.type_name 
			local x,y,z = unpack(obj.mvPos)
			local qw,qx,qy,qz = unpack(obj.mqRot)
			f:write(sprintf("InteriorLoad_Object(\"%s\", %f,%f,%f, %f,%f,%f,%f)\n",typename, x,y,z, qw,qx,qy,qz))
		end
		f:close()
	else
		AddFadeLines("save failed (maybe persmission denied?)")
	end
end

function InteriorLoadFromFile (filepath) 
	AddFadeLines(sprintf("InteriorLoadFromFile %s",filepath))
	InteriorLoad_Object = function (typename, x,y,z, qw,qx,qy,qz) 
		ClientRequestPlaceFurnitureAbsolute(typename, x,y,z, qw,qx,qy,qz) 
	end
	dofile(filepath)
end

function OpenFurnitureSaveDialog ()
	if (not ClientIsInsideShip()) then return end
	FileBrowse_Save("Save Interior",kInteriorDefaultSavePath,kInteriorDefaultFileName,InteriorSaveToFile)
end

function OpenFurnitureLoadDialog () 
	FileBrowse_Load("Load Interior",kInteriorDefaultSavePath,InteriorLoadFromFile)
end

function ClientRequestPlaceFurnitureRelative (objtype,dx,dy,dz) 
	if (not gClientBody) then return end
	local qw,qx,qy,qz = unpack(gClientBody.mqRot)
	print("ClientRequestPlaceFurnitureRelative",dx,dy,dz)
	local fx,fy,fz = Quaternion.ApplyToVector(dx,dy,dz,qw,qx,qy,qz) 
	local x,y,z = unpack(gClientBody.mvPos)
	ClientSendNetMessage(kNetMessage_RequestFurniture,objtype.type_id,x+fx,y+fy,z+fz,qw,qx,qy,qz)
end

function ClientRequestPlaceFurnitureAbsolute (objtype_or_name_or_id,x,y,z,qw,qx,qy,qz) 
	ClientSendNetMessage(kNetMessage_RequestFurniture,GetObjectType(objtype_or_name_or_id).type_id,x,y,z,qw,qx,qy,qz)
end
	
gMessageTypeHandler_Server.kNetMessage_RequestFurniture = function (player,objtype_id,x,y,z,qw,qx,qy,qz)
	local objtype = GetObjectType(objtype_id)
	if (not objtype) then return end
	if (not objtype.bFurniture) then return end
	local body = player.body
	if ((not body) or (not body:IsInsideShip())) then return end
	local ship = body:InsideShip_GetShip()
	SpawnObject(objtype,ship:GetInteriorLocation(),{x,y,z},{qw,qx,qy,qz})
end
