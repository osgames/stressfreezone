-- handles lobby-chat, done via irc
-- see also lugre/lua/lib.irc.lua

gLobbyIRC = nil

RegisterListener("Hook_MainStep",function() if (gLobbyIRC) then gLobbyIRC:Step() end end)

function LobbyChat_Suggestion ()
	if (gNoLobbyChatSuggestion) then return end
	
	local rows = {
		{ {	"do you want to connect to the lobby chat ?\n"..
			"(ingame irc, "..kLobbyIRC_Chan.." on "..kLobbyIRC_Host..":"..kLobbyIRC_Port..")\n"..
			"you can chat by pressing return , type and press return again\n" } },
		{ { "no" , function (widget) widget.dialog:Destroy() end  } },
		{ { "YES" , function (widget) widget.dialog:Destroy() LobbyChat_Connect() end  } },
	}
	
	local vw,vh = GetViewportSize()
	local dialog = guimaker.MakeTableDlg(rows,vw/4,vh/2,false,true,gGuiDefaultStyleSet,"window")
end

function LobbyChat_Connect ()
	if (gLobbyIRC) then return end
	if (not kLobbyIRC_Host) then return end
	
	local username = gUserName.."_"..math.random(100,999)
	print("LobbyChat_Connect",kLobbyIRC_Host,kLobbyIRC_Port,username)
	gLobbyIRC = CreateIRCConnection(kLobbyIRC_Host,kLobbyIRC_Port,username)
	print("LobbyChat_Connect result:",gLobbyIRC)
	if (gLobbyIRC) then 
		gLobbyIRC.sfz_chan = kLobbyIRC_Chan
		gLobbyIRC.sfz_nick = username
		gLobbyIRC:JoinChannel(gLobbyIRC.sfz_chan)
		gLobbyIRC.OnMessage = function (self,sender,channel,message)
			AddFadeLines("[" .. channel .. "] " .. sender .. ": " .. message)
		end
	end
end

function LobbyChat_Disconnect (reason)
	if (not gLobbyIRC) then return end
	gLobbyIRC:Destroy(reason or "QUIT")
	gLobbyIRC = nil
end

function LobbyChat_Send (msg)
	if (gLobbyIRC) then 
		msg = msg or ""
		AddFadeLines("[" .. gLobbyIRC.sfz_chan .. "] " .. gLobbyIRC.sfz_nick .. ": " .. msg)
		gLobbyIRC:SendMessage(gLobbyIRC.sfz_chan,msg) 
	end
end

