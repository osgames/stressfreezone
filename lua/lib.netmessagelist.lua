-- defines message types used for networking

-- must start with sfz;
gNetVersionString = "sfz;protocolversion=1;"

--[[
unused so far...
kNetDataTypes = {}
kNetDataTypes["b"] = 1	-- b byte(uint8) 
kNetDataTypes["i"] = 2	-- i int(uint32) 
kNetDataTypes["f"] = 3	-- f float(4 byte) 
kNetDataTypes["s"] = 4	-- s string(len_uint32,text_ascii) 
kNetDataTypes["#"] = 5	-- # fifo(len_uint32,data_raw) 
kNetDataTypes["_"] = 6	-- _ variable argument count, each prefixed with 1 byte type

-- custom : "o" : game-object  (ship,asteroid,station...)
]]--

-- use FALSE instead of NIL here, nil in the middle confuses the variable argument syntax
RegisterNetMessageFormatWrapper("o","i",function (obj) return obj and obj.id or 0 end,  -- obj2id  
										function (objid) 
												if (objid == 0) then return false end
												local obj = GetObject(objid) 
												assert(type(obj) ~= "number")
												return obj or false
											end)	-- id2obj

RegisterNetMessageType("kNetMessage_ProtocolHeader"				,"s",kNetFirstByte)	-- sent by client and server directly after connection is established
RegisterNetMessageType("kNetMessage_Chat"						,"iis")			-- sent by server and client : param,flags,chattext (used for client and server, if from client then param = target else param = from)
RegisterNetMessageType("kNetMessage_Ping"						,"i")			-- sent by server and client : pingid

RegisterNetMessageType("kNetMessage_ObjectChangeLoc"			,"iifffffff")	-- sent by server : obj_id,new_loc_id,x,y,z,qw,qx,qy,qz  (change location)
RegisterNetMessageType("kNetMessage_SpawnLoc"					,"ii")			-- sent by server : locationtype_id,locid
RegisterNetMessageType("kNetMessage_DestroyLoc"					,"i")			-- sent by server : locid
RegisterNetMessageType("kNetMessage_LocationSetShipInterior"	,"ii")			-- sent by server : locid,objtype_id

-- RegisterNetMessageType("kNetMessage_SpawnMission"				,"osi")			-- sent by server : home,description,new_id
-- RegisterNetMessageType("kNetMessage_RequestAcceptMission"		,"i")			-- sent by server : missionid

RegisterNetMessageType("kNetMessage_UpdateMission"				,"ioossi.")		-- sent by server : mission_id, wheretogo_object, employer_object, description, name, state, (task_id, wheretogo_id, description, state)*
RegisterNetMessageType("kNetMessage_UpdateMissionState"			,"ii")			-- sent by server : mission_id, state
RegisterNetMessageType("kNetMessage_UpdateTaskState"			,"iii")			-- sent by server : mission_id, task_id, state

RegisterNetMessageType("kNetMessage_SpawnContainer"				,"ioi")			-- sent by server : containertype_id,parentobj,new_id
RegisterNetMessageType("kNetMessage_DestroyContainer"			,"i")			-- sent by server : containerid
RegisterNetMessageType("kNetMessage_SetContainerTypeContent"	,"iif")			-- sent by server : containerid,typeid,amount

RegisterNetMessageType("kNetMessage_FireWeapon"					,"oio")			-- sent by server : ship,weaponslotcontainer_id,target

RegisterNetMessageType("kNetMessage_SpawnObject"				,"iifffffffi")	-- sent by server : objtype_id,parentloc_id,x,y,z,qw,qx,qy,qz,new_id
RegisterNetMessageType("kNetMessage_DestroyObject"				,"o")			-- sent by server : destroyed object

RegisterNetMessageType("kNetMessage_SpawnEffect"				,"iofffffffi.")	-- sent by server : effecttype_id,parentobj,x,y,z,qw,qx,qy,qz,new_id
RegisterNetMessageType("kNetMessage_PlayerID"					,"i")			-- sent by server : tell player what id he has
RegisterNetMessageType("kNetMessage_PlayerCtrlKeys"				,"i")			-- sent by client : keymask
RegisterNetMessageType("kNetMessage_PlayerActionKey"			,"i")			-- sent by client : keymask
RegisterNetMessageType("kNetMessage_PlayerName"					,"is")			-- sent by server and client : playerid,playername. when send from client, playerid is ignored
RegisterNetMessageType("kNetMessage_PlayerBody"					,"io")			-- sent by server to all clients to assign player body : playerid,newbody
RegisterNetMessageType("kNetMessage_PlayerMouse"				,"iffff")		-- sent by client when udp is not available, timestamp,mouse_quaternion
RegisterNetMessageType("kNetMessage_TCPResyncs"					,"#")			-- sent by server, #=fifo , only used when udp is disabled
RegisterNetMessageType("kNetMessage_RequestSpawn"				,"i")			-- sent by client to request a body spawn with the given object type
RegisterNetMessageType("kNetMessage_SetTarget"					,"o")			-- sent by client to tell the server which target is currently selected, obj
RegisterNetMessageType("kNetMessage_AutoPilotApproach"			,"of")			-- sent by client : target, fDist
RegisterNetMessageType("kNetMessage_AutoPilotCancel"			,"")			-- sent by client
RegisterNetMessageType("kNetMessage_SetRelMove"					,"fff")			-- sent by client : player requests relative movement : vx,vy,vz
RegisterNetMessageType("kNetMessage_Service"					,"oi.")			-- sent by client and server : obj,iServiceTypeID,data

RegisterNetMessageType("kNetMessage_RequestJettisonCargo"		,"if")			-- sent by client : iCargoTypeID,fAmount
RegisterNetMessageType("kNetMessage_RequestEquipCargo"			,"if")			-- sent by client : iCargoTypeID
RegisterNetMessageType("kNetMessage_RequestUnequip"				,"ii")			-- sent by client : iContainerID,iObjTypeID
RegisterNetMessageType("kNetMessage_RequestEnterObject"			,"o")			-- sent by client : obj : entering spacecraft
RegisterNetMessageType("kNetMessage_RequestExitObject"			,"o")			-- sent by client : obj : exiting spacecraft
RegisterNetMessageType("kNetMessage_RequestDock"				,"o")			-- sent by client : obj
RegisterNetMessageType("kNetMessage_RequestLaunchFighter"		,"")			-- sent by client
RegisterNetMessageType("kNetMessage_RequestLeavePlanet"			,"")			-- sent by client
RegisterNetMessageType("kNetMessage_RequestLandOnPlanet"		,"o")			-- sent by client land on planet
RegisterNetMessageType("kNetMessage_RequestViewShipInside"		,"")			-- sent by client
RegisterNetMessageType("kNetMessage_RequestViewShipOutside"		,"")			-- sent by client
RegisterNetMessageType("kNetMessage_RequestFurniture"			,"ifffffff")	-- sent by client : objtype_id,x,y,z,qw,qx,qy,qz
RegisterNetMessageType("kNetMessage_RequestTurnFurniture"		,"of")			-- sent by client : obj,ang
RegisterNetMessageType("kNetMessage_RequestTrade"				,"oif")			-- sent by client : obj_trader,offerid,amount
RegisterNetMessageType("kNetMessage_RequestTeleport"			,"fff")			-- sent by client : x,y,z

RegisterNetMessageType("kNetMessage_SetProp_Dynamic_Number"		,"oifffff")		-- sent by server : obj,proptypeid,value,delta,fMin,fMax,fRate
RegisterNetMessageType("kNetMessage_SetProp_Static_Number"		,"oiff")		-- sent by server : obj,proptypeid,value,delta
RegisterNetMessageType("kNetMessage_SetProp_Static_String"		,"ois")			-- sent by server : obj,proptypeid,value

-- guildwars like calling of a target
RegisterNetMessageType("kNetMessage_CallTarget"		,"o")					-- sent by client : obj(target)
RegisterNetMessageType("kNetMessage_NotifyCallTarget"		,"ioo")					-- sent by server : playerid, obj(caller), obj(target)

-- send this first before anything else
function SendNetVersion (con) SendNetMessage(con,kNetMessage_ProtocolHeader,gNetVersionString) end


-- todo : custom netmessage type with variable argument count and size+type meta-infos ? would sure be nice later for scripting
