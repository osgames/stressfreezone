
gbClientRunning = false
gbClientIsRemote = false
gClientOwner = {id=23 } -- TODO : later owner obj
gClientBody = nil
gClientFireCounter = 0
gClientServerConnection = nil
gClientRecvFifo = CreateFIFO()
gMessageTypeHandler_Client = {} -- handlers for all clients, local+remote
gMessageTypeHandler_Client_Remote = {} -- handlers that are only for remote clients, e.g. skipped on local client
gClientUDPReceiveSocket = nil
giClientKeyMask = 0
gClientCtrlKeyMap = {}
gClientGuiMode = false
gClientTarget = nil
gClientObjectUnderMouse = nil
gbClientOwnBodyUnderMouse = false
gbClientMoveControls = false
gClient_ActiveLocation = nil
gLastSfzMousePickCycle = 1

kInitialClientThirdPersonDist = 80
gClientTableCamSpeedFactor = 0.5 * gfDeg2Rad
	
gClientPlayerBodies = {}

dofile(libpath .. "lib.client.message.lua")

function RequestBody (typename)
	if (gClientBody) then return end
    
    typename = typename or gClient_PreferredBodyTypeName
    -- request a player body
	if typename then
		local objtype = GetObjectType(typename)
		if gClientServerConnection then ClientSendNetMessage(kNetMessage_RequestSpawn,objtype.type_id) end
	else
		NotifyListener("Hook_Client_OpenSelectShipDialog")
		--printdebug("spawn","illegal body name requested: " .. typename)
	end
end

-- see also lib.target.lua
function ClientAutoPilot_Approach (obj) ClientSendNetMessage(kNetMessage_AutoPilotApproach,obj,kAutoPilotDefaultApproachDist) end

-- see also lib.target.lua
function ClientGetCustomTargetList (filter) return GetCustomTargetList(gClientBody,filter) end
function ClientGetTargetList () return GetTargetList(gClientBody) end
function ClientChooseTarget_Next  () ClientSetTarget(TargetList_GetNext(ClientGetTargetList(),gClientTarget)) end
function ClientChooseTarget_Prev  () ClientSetTarget(TargetList_GetPrev(ClientGetTargetList(),gClientTarget)) end
function ClientChooseTarget_NoTarget () ClientSetTarget(nil) end
function ClientChooseTarget_NearestHostile () ClientSetTarget(TargetList_Nearest(ClientGetTargetList(),gClientBody)) end
function ClientSetTarget (obj)
	gClientTarget = obj
	ClientSendNetMessage(kNetMessage_SetTarget,obj)
	NotifyListener("Hook_Client_SetTarget",gClientTarget)
end

function ClientChooseTarget_ByName (pattern) 
	ClientSetTarget(TargetList_Nearest(GetTargetByName(pattern),gClientBody)) 
end

function InitClientCtrls ()
	BindActionKeys()
end

function ToggleGuiMode ()
	print("ToggleGuiMode")
	gClientGuiMode = not gClientGuiMode
	-- TODO : mouse cursor ?
end

function StartClient_JoinLocal ()
	print("StartClient_JoinLocal")
	local con_master = NetLocalMaster()
	local con_slave = NetLocalSlave(con_master)
	ServerPlayerJoined(con_master,true)
	StartClient(con_slave,false)
end

function StartClient_JoinRemote (host,port)
	print("StartClient_JoinRemote",host,port)
	local con = NetConnect(host,port)
	if (con and con:IsConnected()) then
		AddFadeLines(sprintf("connection to server %s:%d established",tostring(host),tostring(port))) 
		StartClient(con,true)
		NotifyListener("Hook_Client_Connected",host,port)
	else
		AddFadeLines(sprintf("could not connect to server %s:%d",tostring(host),tostring(port)))
		StartMainMenu()		
	end
end

function StartClient (con,bIsRemote)
	InitClientCtrls()
	gbClientRunning = true
	gbClientIsRemote = bIsRemote
	gClientReceivedServerNetVersion = false
	gClientServerConnection = con 
	SendNetVersion(gClientServerConnection)
	
	if (gbClientIsRemote) then 
		gClientResyncReceiver = CreateResyncReceiver()
		SetGlobalResyncReceiver(gClientResyncReceiver) -- must be set before objects are created
		if (gEnableUDP) then gClientUDPReceiveSocket = Create_UDP_ReceiveSocket(giUDPClientRecvPort) end
	end
	
	gCamThirdPersonDist = kInitialClientThirdPersonDist
	
	RegisterListener("Hook_Object_Destroy",function (obj) if (obj == gClientTarget) then gClientTarget = nil end end)
	
	RegisterListener("mouse_left_down",function () gLastSfzMousePickCycle = gLastSfzMousePickCycle + 1 end)
		
	NotifyListener("Hook_Client_Start")
end

function ClientSendNetMessage	(iMsgTypeID,...)	SendNetMessage(gClientServerConnection,iMsgTypeID,unpack(arg)) end
function ClientSendChat			(text)				ClientSendNetMessage(kNetMessage_Chat,0,0,text) end
function ClientSendActionKey	(iActionKeyID)		ClientSendNetMessage(kNetMessage_PlayerActionKey,iActionKeyID) end

function StepClient ()
	if (not gbClientRunning) then return end
	
	-- todo : send only 30 times per second if not gbServerRunning 
	if (gbClientMoveControls) then ClientSendCurrentMouse() end
	
	-- skip input if gui is active
	if not GuiConsumesInput() then
		-- get keymask
		local keymask = 0
		gbClientMoveControls = false
		if not gDisableSteering then
			for mask,keycode in pairs(gClientCtrlKeyMap) do
				if (gKeyPressed[keycode]) then 
					--if (mask ~= kCtrlKeyMask_Break and
					--	mask ~= kCtrlKeyMask_FirePrimary) then gbClientMoveControls = true end
					if (mask ~= kCtrlKeyMask_Break) then gbClientMoveControls = true end
					keymask = keymask + mask 
				end
			end
		end
		if (giClientKeyMask ~= keymask) then
			giClientKeyMask = keymask
			ClientSendNetMessage(kNetMessage_PlayerCtrlKeys,giClientKeyMask)
		end
	end

	-- receive network messages from server
	gClientServerConnection:Pop(gClientRecvFifo)
	RecvNetMessages(gClientRecvFifo,function (msgtype,...) 
			local msgtypename = gNetMessageTypeName[msgtype]
			if (gMessageTypeHandler_Client[msgtypename]) then 
				gMessageTypeHandler_Client[msgtypename](unpack(arg))
			end
			if (gbClientIsRemote) then
				if (gMessageTypeHandler_Client_Remote[msgtypename]) then 
					gMessageTypeHandler_Client_Remote[msgtypename](unpack(arg))
				end
			end
			NotifyListener("Hook_Client_RecvNetMsg",msgtype,unpack(arg))
		end)
	
	-- receive udp data
	if (gbClientIsRemote and gEnableUDP) then 
		gClientResyncReceiver:ReceiveResyncs(gClientUDPReceiveSocket,gClientServerConnection:GetRemoteAddress())
	end
	
	-- step objects
	if (not gbServerRunning) then 
		-- todo : decrease precalc for objects that were just resynced from server
		for k,loc in pairs(gLocationsByID) do 
			loc:StepAllObjects(gMyTicks,gSecondsSinceLastFrame)
		end
	end
	
	-- keep cam position at playerbody, todo : interpolate server resyncs here ?
	if (gClientBody) then
		-- move cam to current body
		local x,y,z = unpack(gClientBody.mvPos)
		gCamThirdPersonX,gCamThirdPersonY,gCamThirdPersonZ = x,y,z
		--GetMainCam():SetPos(x,y,z)
		local vx,vy,vz = unpack(gClientBody.mvVel)
		local speed = Vector.len(vx,vy,vz)
		SetBottomLine(sprintf("pos=%0.2f,%0.2f,%0.2f speed=%0.2f  abs-speed=(%0.1f,%0.1f,%0.1f)",x,y,z,speed,vx,vy,vz))
		
		local curloc = gClientBody:GetParentLocation()
		if (gClient_ActiveLocation ~= curloc) then
			gClient_ActiveLocation = curloc
			gClient_ActiveLocation:ClientActivateLocationBackground()
		end
	else
		SetBottomLine("you are dead")
	end
	
	-- mousepicking
	gClientObjectUnderMouse = nil
	gbClientOwnBodyUnderMouse = false
	local widgetUnderMouse = GetWidgetUnderMouse()
	if (not widgetUnderMouse) then
		local mx,my = GetMousePos()
		
		if (gLastSfzMousePickX == mx and gLastSfzMousePickY == my) then
			-- see above : RegisterListener("mouse_left_down",function () gLastSfzMousePickCycle = gLastSfzMousePickCycle + 1 end)
		else 
			gLastSfzMousePickCycle = 1
		end
		gLastSfzMousePickX = mx
		gLastSfzMousePickY = my
		local e = 10 -- tolerance
		local x,y,z = GetMainCam():GetPos()
		local res = {} 
		for k,loc in pairs(gLocationsByID) do 
			-- (clickdist is only > 0 if not within core, but within tolerance)
			for id,dist in pairs(loc:Intersect2DRect(mx,my,mx,my,e)) do 
				local obj = GetObject(id)
				if (obj == gClientBody) then 
					gbClientOwnBodyUnderMouse = true
				else
					table.insert(res,{clickdist=dist,camdist=obj:GetDistToPos(x,y,z),obj=obj}) 
				end
			end
		end
		
		-- sort ascending by click distance, and if multiple have the same clickdist, prefer the closest one to the cam
		table.sort(res,function(a,b) 
			if (a.clickdist == b.clickdist) then return a.camdist < b.camdist end
			return a.clickdist < b.clickdist
		end)
	
		gLastSfzMousePickCycle = math.mod(gLastSfzMousePickCycle-1,table.getn(res))+1
		gClientObjectUnderMouse = res[gLastSfzMousePickCycle] and res[gLastSfzMousePickCycle].obj
				
		if (gClientObjectUnderMouse) then 
			local obj = gClientObjectUnderMouse
			SetBottomLine(arrdump({"mousepick",obj.id,obj.objtype.type_name}))
		end
	end
	NotifyListener("Hook_Client_MousePick",gClientObjectUnderMouse)
	SetToolTipSubject(GetWidgetUnderMouse() or gClientObjectUnderMouse)
	
	StepTableCam(GetMainCam(),gKeyPressed[key_mouse1] and (not gui.bMouseBlocked),gClientTableCamSpeedFactor)
	StepThirdPersonCam(GetMainCam(),gCamThirdPersonDist,gCamThirdPersonX,gCamThirdPersonY,gCamThirdPersonZ)
end

function ClientLeftClick ()
	if (GetWidgetUnderMouse() or GetLastMouseDownWidget()) then return end
	if (gClientObjectUnderMouse) then
		ClientSetTarget(gClientObjectUnderMouse)
	elseif (gbClientOwnBodyUnderMouse) then
		-- clicked self and nothing else
	else
		--ClientSetTarget(nil)
		--[[
		if (gClientBody) then
			local rx,ry,rz,rvx,rvy,rvz = GetMouseRay()
			local qw,qx,qy,qz = GetMainCam():GetRot()
			local mw,mx,my,mz = Quaternion.fromAngleAxis(-60*gfDeg2Rad,1,0,0) -- angle
			qw,qx,qy,qz = Quaternion.Mul(qw,qx,qy,qz,mw,mx,my,mz)
			local x,y,z = unpack(gClientBody.mvPos)
			local nx,ny,nz = Quaternion.ApplyToVector(0,0,1,qw,qx,qy,qz) -- direction nearly inverse to mouseray, used as plane normal
			nx,ny,nz = Vector.normalise(nx,ny,nz)
			local dist = PlaneRayPick(x,y,z,nx,ny,nz,rx,ry,rz,rvx,rvy,rvz)
			--print(arrdump({"PlaneRayPick(",x,y,z," ",nx,ny,nz," ",rx,ry,rz," ",rvx,rvy,rvz,")=",dist}))
			--print(arrdump({"raydir=",rvx,rvy,rvz,"  planenormal=",nx,ny,nz,"   dist=",dist}))
			
			if (dist) then
				local hx,hy,hz = (rx+dist*rvx),(ry+dist*rvy),(rz+dist*rvz)
				ClientSendNetMessage(kNetMessage_SetRelMove,hx-x,hy-y,hz-z)
				print(arrdump({"click-dist=",dist," hitpos=",hx,hy,hz}))
			end
		end
		]]-- working, but currently unused
	end
	NotifyListener("Hook_Client_LeftClick",gClientObjectUnderMouse)
end

function ClientRightClick ()
	if (GetWidgetUnderMouse()) then return end
	local menudata = {}
	NotifyListener("Hook_Client_BuildRightClickMenu",menudata,gClientObjectUnderMouse)
	ShowContextMenu(menudata,nil,nil,gGuiDefaultStyleSet)
end

function ClientSetCamOrientation (w,x,y,z)
	GetMainCam():SetRot(w,x,y,z)
end

function ClientSendCurrentMouse ()
	local tw,tx,ty,tz = 1,0,0,0 -- identity
	
	if (not gClientGuiMode) then
		tw,tx,ty,tz = CamGetMouseTurn(gSecondsSinceLastFrame * gMainMenuCamMouseSpeedFactor)
		TurnCam(tw,tx,ty,tz)
	end
	
	local roll =	1*gfDeg2Rad * (	(TestBit(giClientKeyMask,kCtrlKeyMask_RollLeft ) and 1 or 0) +
									(TestBit(giClientKeyMask,kCtrlKeyMask_RollRight) and -1 or 0) )
	if (roll ~= 0) then 
		local tw,tx,ty,tz = Quaternion.fromAngleAxis(roll,0,0,1)
		TurnCam(tw,tx,ty,tz) 
	end 
	
	gFirstClientMouseResyncTime = gFirstClientMouseResyncTime or gMyTicks
	local iMouseResyncTimeStamp = gMyTicks - gFirstClientMouseResyncTime
	
	local qw,qx,qy,qz = GetMainCam():GetRot()
	if (gbClientIsRemote) then 
		-- send network message
		if (gEnableUDP) then
			-- send via udp
			gSendFifo:Clear()
			FPush(gSendFifo,"iffff",iMouseResyncTimeStamp,qw,qx,qy,qz)
			gUDPSendSocket:Send(gClientServerConnection:GetRemoteAddress(),giUDPServerRecvPort,gSendFifo)
		else
			-- send via tcp
			ClientSendNetMessage(kNetMessage_PlayerMouse,iMouseResyncTimeStamp,qw,qx,qy,qz)
		end
	else
		-- local server, call method directly
		if (giClientPlayerID) then ServerSetPlayerMouseRot(giClientPlayerID,iMouseResyncTimeStamp,qw,qx,qy,qz) end
	end
end

-- call a target (like guildwars)
function CallCurrentTarget()
	if ( gClientTarget ) then
		ClientSendNetMessage(kNetMessage_CallTarget,gClientTarget)
	end	
end

function TargetCurrentCall()
	if ( gClientCall ) then
		ClientSetTarget(gClientCall)
	end
end

gMessageTypeHandler_Client.kNetMessage_NotifyCallTarget = function (playerid,obj_caller,obj_target)
	if (obj_caller and obj_target) then 
		gClientCall = obj_target
		AddFadeLines("player " .. playerid .. " calls " .. (obj_target:GetName() or "???"))
		NotifyListener("Hook_Client_SetCall",obj_target)
		-- print("DEBUG",playerid,obj_caller,obj_caller:GetName(),"calls",obj_target,obj_target:GetName())
	end
end


