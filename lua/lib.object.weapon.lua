-- object methods related to object weapons
-- weapontypes such as laser...
-- see also lib.object.equipment.lua
-- see also common.weapontypes.lua
-- todo : don't fire if friend would be damaged
-- todo : raypick only to detect if target would be hit,  if not wait for small interval, e.g. 200msec ! 

kWeaponSlotType = "slottype/weapon"


function gContainerPrototype:AddWeapon (weapontype_or_name_or_id) 
	self.weapon_type = GetObjectType(weapontype_or_name_or_id)
	self.weapon_nextfire = gMyTicks	
	-- todo : netmessage, link to equipment/container ? use container.data to store weapon infos ??
end

function gContainerPrototype:GetWeaponType() 
	local objtype = self:GetFirstContentType()
	return objtype
end

function gContainerPrototype:DelWeapon (weapontype_or_name_or_id) 
	assert(self.weapon_type == GetObjectType(weapontype_or_name_or_id))
	self.weapon_type = nil
end

function gContainerPrototype:GetWeaponSlotNumber ()
	local ship = self:GetObject()
	for slotnumber,container in pairs(ship and ship:GetWeaponSlotList() or {}) do
		if (container == self) then return slotnumber end
	end
end


function gObjectPrototype:GetWeaponSlotList ()
	local res = {}
	for containertype,containerlist in pairs(self:GetContainerListByType() or {}) do 
		if containertype.bIsWeaponSlot then
			for k,container in pairs(containerlist) do 
				table.insert(res,container)
			end
		end
	end
	return res
end
-- bIsWeaponSlot

function GetPlayerShipTarget (playership) return playership.player and playership.player.target end

function FirePrimaryWeapons	(ship,target)
	local weapons = ship:GetWeaponSlotList()
	for slotnumber,weapon in pairs(weapons) do FireWeaponSlot(ship,target,slotnumber) end
end

-- TODO replace me with something intelligent
-- at the moment it simply returns the ith weapon entry
function GetWeaponFromSlot (ship, slotnumber)
	local weapons = ship:GetWeaponSlotList()
	return weapons[slotnumber]
end



-- checks range and needed energy, returns true if fire possible
function FireCheckEnergyRange (weapon,ship,target)
	if (not weapon) or (not ship) or (not target) then return false end
	if (not weapon:GetWeaponType()) then return false end
	if (not ship:IsAlive()) or (not target:IsAlive()) then return false end
	
	local t = weapon.weapon_type
	
	if (not t.energy) or (not t.range) then return false end
	
	-- cooldown check
	if weapon.weapon_nextfire and weapon.weapon_nextfire > gMyTicks then return false end

	local dist = ship:GetDistToObject(target)

	-- range check
	if dist > t.range then return false end

	-- energy check
	if ship:GetEnergy() < t.energy then return false end

	return true
end

-- removes the ressources to fire, this does not check if possible so
-- only call this after a fire_possible_fun function
-- this handles weapon mark for cooldown
function FireAcquireEnergy (weapon,ship)
	assert(weapon,"this must contain a usefull value")
	assert(ship,"this must contain a usefull value")
	local t = weapon:GetWeaponType()
	weapon.weapon_nextfire  = gMyTicks + t.iFireInterval
	ship:ReduceEnergy(t.energy)
end

-- server-only
-- target can be nil
-- fires the weapon in the given slot
function FireWeaponSlot (ship,target,slotnumber) 
	
	local dx,dy,dz = 0,0,0
	dx = 0.5 * (1 + math.floor(slotnumber/2)) * ((math.mod(slotnumber,2) == 0) and 1 or -1)
	
	local weapon = GetWeaponFromSlot(ship,slotnumber)
	local t = weapon and weapon:GetWeaponType()

	local x,y,z = unpack(ship.mvPos)
	
	if weapon and t and 
		t.fire_possible_fun and 
		t.fire_acquire_res_fun and 
		t.fire_weapon_fun then 
		
		if t.fire_possible_fun(weapon,ship,target) then
			t.fire_acquire_res_fun(weapon,ship)
			
			ServerBroadcastMessage(kNetMessage_FireWeapon,ship,weapon.id,target)
			t.fire_weapon_fun(x+dx,y+dy,z+dz, ship,t,target,0,0,0) 
		end
	end
	
end


function ManualFireLaser (sx,sy,sz,ship,weapontype,target)
	if not target or not target:IsAlive() or not target.mvPos then return end

	local x2,y2,z2 = sx,sy,sz
	local x1,y1,z1
	local mq,mx,my,mz = 1,0,0,0
	-- local qw,qx,qy,qz = sqw,sqx,sqy,sqz
	
	x1,y1,z1 = unpack(target.mvPos)
	
	mq,mx,my,mz = Quaternion.getRotation(0,0,-1,
		Quaternion.ApplyToVector(x1-sx,y1-sy,z1-sz,Quaternion.inverse(1,0,0,0)))
	-- mq,mx,my,mz = ship:CalcRotationTo(x1,y1,z1) -- calc effect rotation
	
	local dist = Vector.len(x1-x2,y1-y2,z1-z2)

	-- ServerBroadcastMessage(kNetMessage_FireWeapon,ship,target,weapontype.type_id,weaponindex)
	
	-- local x = 0.5 * (1 + math.floor(weaponindex/2)) * ((math.mod(weaponindex,2) == 0) and 1 or -1)
	local beamlen = weapontype.range
	if (target and target:IsAlive()) then beamlen = dist end
	
	SpawnEffect(weapontype.effecttype,nil,{sx,sy,sz},{mq,mx,my,mz},nil,beamlen)
	
	local dx, dy, dz = x1 - x2, y1 - y2, z1 - z2
	if (target and target:IsAlive()) then -- target might have been destroyed during firing all weapons		
		local dist = target:RayPick(x2,y2,z2,dx,dy,dz)
		local hx,hy,hz
		if (dist) then hx,hy,hz = x2+dist*dx,y2+dist*dy,z2+dist*dz else hx,hy,hz = unpack(target.mvPos) end
		-- cause damage
		target:Damage(weapontype.damage,hx,hy,hz,ship)
	else
		-- no target : fire straight ahead and damage everything in the path (TODO : damage only nearest hit)
		MapRayPick(x2,y2,z2,dx,dy,dz,function (obj,dist,facenum) 
				if (obj ~= ship) then 
					local hx,hy,hz = x2+dist*dx,y2+dist*dy,z2+dist*dz
					obj:Damage(weapontype.damage,hx,hy,hz,ship)
				end
			end)
	end
end

function FireWeaponChain (sx,sy,sz, ship, weapontype, target, vx, vy, vz) 
	if not ship or not ship:IsAlive() or not ship.mvPos then return end
	if not weapontype.jump_weapontype then return end
	
	local chaintype = GetObjectType(weapontype.jump_weapontype)
	
	local breakChainOnKill = false
	if weapontype.break_chain_on_kill then breakChainOnKill = weapontype.break_chain_on_kill end
	
	local jumpsLeft = weapontype.jumps
	
	local usedTargets = {}
	local lastTarget = target
	local nextTarget = nil
	
	-- next chain step source (starts at the ship)
	local sx,sy,sz = unpack(ship.mvPos)
	
	while jumpsLeft > 0 do
		if lastTarget and lastTarget:IsAlive() then 
			-- mark target used
			usedTargets[lastTarget] = true
		
			-- find next target
			local k,v = GetRandomObjectInRange(lastTarget,chaintype.range,function(o) return (not usedTargets[o]) and (not o.player) end)
			if v then nextTarget = v else nextTarget = nil end
		
			-- fire
			if lastTarget and lastTarget:IsAlive() then
				local x,y,z = unpack(lastTarget.mvPos)
				chaintype.fire_weapon_fun(sx,sy,sz,ship,chaintype,lastTarget)
				sx,sy,sz = x,y,z
			end
			
			-- skip the rest if breakChainOnKill is true
			if breakChainOnKill and not lastTarget:IsAlive() then jumpsLeft = 1 end
		else
			nextTarget = nil
		end
		
		-- switch to next step
		jumpsLeft = jumpsLeft - 1
		lastTarget = nextTarget
	end
end

-- server-only
-- sx,sy,sz is absolute source position
-- vx,vy,vz initial velocity if possible
-- fires a laser beam
function FireWeaponLaser (sx,sy,sz, ship, weapontype, target, vx, vy, vz) 
	if not ship or not ship:IsAlive() or not ship.mvPos then return end
	
	local shipx,shipy,shipz = unpack(ship.mvPos) 
	ManualFireLaser (sx,sy,sz,ship,weapontype,target)
	
	-- jump to next targets
	if weapontype.jumps > 0 then
		local usedtargets = {}
		local lasttarget = target
		usedtargets[lasttarget] = true
	end
end


-- target can be nil, vx,vy,vz is initial velocity
function ManualFireProjectile (x,y,z,location,ownerbody,weapontype,target,vx,vy,vz) 
	-- center as source
	local rx = 0
	local ry = 0
	local rz = 0

	-- target might have been destroyed during firing all weapons
	if ((not target) or (not target:IsAlive())) then return end
	
	local t = weapontype
	
	-- fire projectile
	
	-- spawn rocket
	local projectile = SpawnObject(weapontype.projectile_type,location,{x,y,z},{1,0,0,0})
	Projectile_Init(projectile,target,ownerbody,weapontype)

	local dx,dy,dz = Vector.sub(target.mvPos[1],target.mvPos[2],target.mvPos[3],x,y,z)
	if (t.startspeed > 0) then -- aiming for moving targets
		local wx,wy,wz	= unpack(target.mvVel)
		local rvx,rvy,rvz = Vector.sub(vx,vy,vz,wx,wy,wz)
		local dirspeed	= Vector.dot(rvx,rvy,rvz,Vector.normalise(dx,dy,dz)) + t.startspeed
		if (dirspeed > 0) then -- speed in the direction of the enemy
			local disttime	= Vector.len(dx,dy,dz) / dirspeed -- approximate time until shot reaches target
			local x0,y0,z0	= unpack(target.mvPos)
			local x1,y1,z1	= Vector.addscaled(disttime,x0,y0,z0,Vector.sub(wx,wy,wz,vx,vy,vz))
			dx,dy,dz = Vector.sub(x1,y1,z1,x,y,z)
		end
	end
	dx,dy,dz = Vector.normalise_to_len(dx,dy,dz,t.startspeed)
	vx,vy,vz = Vector.add(vx,vy,vz,dx,dy,dz)
	projectile.mvVel = {vx,vy,vz}

	return projectile
end

-- server-only
-- sx,sy,sz is absolute source position
-- vx,vy,vz initial velocity if possible
-- fires a rocket and split it into multiple rockets after timeout (same target)
function FireWeaponProjectileSplit (sx,sy,sz, ship, weapontype, target, vx, vy, vz) 
	local p = FireWeaponProjectile(sx,sy,sz, ship,weapontype,target,vx,vy,vz)
	
	if p then
		local t = weapontype

		InvokeLater(t.splittime,function()
			if ((not target) or (not target:IsAlive()) or (not target.mvPos)) then return end
			if p and p:IsAlive() then
				local speed = Vector.len(unpack(p.mvVel))
				local pvx,pvy,pvz = Vector.normalise(unpack(p.mvVel))
				local sx,sy,sz = unpack(p.mvPos)
				
				-- some more or less usefull default values
				local splitcount = t.splitcount or 3
				local splitspread = t.splitspread or 2
				local splitprojectile_weapontype = t.splitprojectile_weapontype or nil
				
				-- calculate the different "nut" launch vectors
				
				local x,y,z
				local d = math.abs(Vector.dot(1,0,0,pvx,pvy,pvz))
				if d > 0.9 	then
					x,y,z = Vector.cross(0,1,0,pvx,pvy,pvz)
				else
					x,y,z = Vector.cross(1,0,0,pvx,pvy,pvz)
				end
				x,y,z = Vector.normalise(x,y,z)
				
				local ang = 360 / splitcount
				
				-- launch splitcount rockets
				for i = 0,splitcount-1 do
					local qw,qx,qy,qz = Quaternion.fromAngleAxis(tonumber(ang * i)*gfDeg2Rad,pvx,pvy,pvz)
					local wx,wy,wz = Quaternion.ApplyToVector(x,y,z,qw,qx,qy,qz)
					wx,wy,wz = Vector.normalise(pvx+wx*splitspread,pvy+wy*splitspread,pvz+wz*splitspread)
					
					if splitprojectile_weapontype then
						local t = GetObjectType(splitprojectile_weapontype)
						if t then 
							t.fire_weapon_fun(sx,sy,sz,ship,t,target,wx*speed,wy*speed,wz*speed)
						end
					end
				end
				
				p:Destroy()
			end
		end)
	end
end

-- server-only
-- sx,sy,sz is absolute source position
-- vx,vy,vz initial velocity if possible
-- fires a rocket and split it into multiple rockets after timeout (multiple targets)
function FireWeaponProjectileSplitMultiTarget (sx,sy,sz, ship, weapontype, target, vx, vy, vz) 
	local p = FireWeaponProjectile(sx,sy,sz, ship,weapontype,target,vx,vy,vz)
	
	if p then
		local t = weapontype

		InvokeLater(t.splittime,function()
			if ((not target) or (not target:IsAlive()) or (not target.mvPos)) then return end
			if p and p:IsAlive() then
				local speed = Vector.len(unpack(p.mvVel))
				local pvx,pvy,pvz = Vector.normalise(unpack(p.mvVel))
				local sx,sy,sz = unpack(p.mvPos)
				
				-- some more or less usefull default values
				local splitcount = t.splitcount or 3
				local splitspread = t.splitspread or 2
				local splitprojectile_weapontype = t.splitprojectile_weapontype or nil
				
				-- calculate the different "nut" launch vectors
				
				local x,y,z
				local d = math.abs(Vector.dot(1,0,0,pvx,pvy,pvz))
				if d > 0.9 	then
					x,y,z = Vector.cross(0,1,0,pvx,pvy,pvz)
				else
					x,y,z = Vector.cross(1,0,0,pvx,pvy,pvz)
				end
				x,y,z = Vector.normalise(x,y,z)
				
				local ang = 360 / splitcount
				
				-- launch splitcount rockets
				for i = 0,splitcount-1 do
					local qw,qx,qy,qz = Quaternion.fromAngleAxis(tonumber(ang * i)*gfDeg2Rad,pvx,pvy,pvz)
					local wx,wy,wz = Quaternion.ApplyToVector(x,y,z,qw,qx,qy,qz)
					wx,wy,wz = Vector.normalise(pvx+wx*splitspread,pvy+wy*splitspread,pvz+wz*splitspread)
					
					if splitprojectile_weapontype then
						local t = GetObjectType(splitprojectile_weapontype)
						if t then 
							local newtarget = target
							
							-- search for objects to damage and set a random new target
							local k,v = GetRandomObjectInRange(p,t.range,function(o) return o ~= p and (not o.player) end)
							if v then newtarget = v end
							
							t.fire_weapon_fun(sx,sy,sz,ship,t,newtarget,wx*speed,wy*speed,wz*speed)
						end
					end
				end
				
				p:Destroy()
			end
		end)
	end
end 

function FireWeaponProjectile (sx,sy,sz, ship, weapontype, target, vx, vy, vz)
	if not ship or not ship:IsAlive() or not ship.mvPos then return end
	if not target or not target:IsAlive() or not target.mvPos then return end

	local t = weapontype

	-- target might have been destroyed during firing all weapons
	if ((not target) or (not target:IsAlive())) then return end

	--if (weapon.weapon_nextfire and weapon.weapon_nextfire > gMyTicks) then return end
	
	--local cost_energy = t.energy
	--if (cost_energy and cost_energy <= 0) then cost_energy = false end
	--if (cost_energy and ship:GetEnergy() < cost_energy) then return end -- not enough energy
		
		-- fire projectile
		
		-- remove rocket from inventar
		
	--local cost_objtype_amount = 1
	--local cost_objtype = t.ammo_type
	--if ((not cost_objtype) and (not cost_energy)) then cost_objtype = t.projectile_type end -- use projectile by default
	--if (cost_objtype) then
		--local amount = ship:RemoveCargoUntilEmpty(cost_objtype,cost_objtype_amount)
		--if (amount < cost_objtype_amount) then 
			--if (amount > 0) then print("warning, bug : used up some ammo but couldn't fire because not enough there") end
			--return
		--end
	--end
	
	---- consume energy after ammo check
	--if (cost_energy) then ship:ReduceEnergy(cost_energy) end
		
	---- update fireinterval
	--weapon.weapon_nextfire  = gMyTicks + t.iFireInterval
	
	-- global source position
	-- local sx,sy,sz = ship:LocalToGlobal(rx,ry,rz)

	return ManualFireProjectile(sx,sy,sz,ship:GetParentLocation(),ship,t,target,unpack(ship.mvVel),vx,vy,vz) 
end
