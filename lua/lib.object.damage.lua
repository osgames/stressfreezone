-- object methods related to hit/hull-points


-- percentage in [0,1]
function gObjectPrototype:GetHullPercentage		() return self:GetHull()/self:GetHullMax() end

function gObjectPrototype:GetHullMax	()	return self:IsAlive() and 
	(math.max(1,(self.maxhp or self.objtype.hp or 0) + (self:GetActiveEquipmentSum("equipment_add_hullmax") or 0))) or 0 end
function gObjectPrototype:GetHull		()	return self:IsAlive() and self:GetProp("proptype/hull") or self:GetHullMax() end

function gObjectPrototype:SetHull		(value,delta) 
	self:SetProp("proptype/hull",math.max(0,value),delta) end

function gObjectPrototype:AddHull		(delta) 
	local hull_old = self:GetHull()
	local hull_new = math.max(0,math.min(self:GetHullMax(),hull_old+delta))
	delta = hull_new - hull_old
	self:SetHull(hull_new,delta)
	return delta
end

function gObjectPrototype:RepairHull	(rep) self:AddHull(math.max(0,rep)) end
function gObjectPrototype:DamageHull	(dmg) self:AddHull(-math.max(0,dmg)) end

-- server only
function gObjectPrototype:Damage (dmg,hx,hy,hz,attacker)
	local shield = self:GetShield()
	--print("gObjectPrototype:Damage",dmg,shield)
	local shielddamage = self:DamageShield(dmg)
	local hulldamage = dmg - shielddamage
	if (hulldamage > 0) then self:DamageHull(hulldamage) end
	
	--print("damaged object",self.id,"hp-left=",self.hp)
	if (self:GetHull() > 0) then 
		-- only damaged, not yet dead
		if (shielddamage	> 0) then SpawnEffect("effect/hit",self,{self:GlobalToLocal(hx,hy,hz)},{1,0,0,0}) end -- todo : different effect
		if (hulldamage		> 0) then SpawnEffect("effect/hit",self,{self:GlobalToLocal(hx,hy,hz)},{1,0,0,0}) end
		NotifyListener("Hook_Object_Damage",self,attacker,shielddamage,hulldamage) --- TODO : this is so far server only
	else
		-- kaboom, dead
		-- TODO : spawn corpse (with speed, rot, etc) and attach explosion to it ?
		if (attacker and self.objtype.loot) then
			-- give loot to attacker
			for cargotypename,amount in pairs(self.objtype.loot) do
				attacker:AddCargoUntilFull(cargotypename,amount)
			end
		end
		SpawnEffect("effect/die",nil,{hx,hy,hz},{1,0,0,0})
		self:Destroy() 
	end
end

-- object methods related to shields

-- percentage in [0,1]
function gObjectPrototype:GetShieldPercentage	() return (self:GetShieldMax() > 0) and self:GetShield()/self:GetShieldMax() or 0 end

function gObjectPrototype:HasShield		()	return self.objtype.shield_max ~= nil end
function gObjectPrototype:GetShield		()	return self:IsAlive() and self:GetProp("proptype/shield") or self:GetShieldMax() end
function gObjectPrototype:GetShieldRate	()	return self:IsAlive() and 
	((self.objtype.shield_rate or 0) + (self:GetActiveEquipmentSum("equipment_add_shieldrate") or 0)) or 0 end
function gObjectPrototype:GetShieldMax	()	return self:IsAlive() and 
	((self.objtype.shield_max or 0) + (self:GetActiveEquipmentSum("equipment_add_shieldmax") or 0)) or 0 end

function gObjectPrototype:SetShield		(value,delta)	
	self:SetProp("proptype/shield",math.max(0,value),delta or 0,0,self:GetShieldMax(),self:GetShieldRate())
end

RegisterListener("Hook_Object_SetContainerContent",function (container,objecttype,fCurAmount)
	local o = container:GetObject() 
	if (o and container:GetType().bIsSlot and o:HasShield()) then o:SetShield(o:GetShield()) end
	if (o and container:GetType().bIsSlot and o:HasEnergy()) then o:SetEnergy(o:GetEnergy()) end
	if (o and container:GetType().bIsSlot) then o:SetHull(o:GetHull()) end
end)

-- returns damage done to the shield
function gObjectPrototype:DamageShield	(dmg)
	if (not self:HasShield()) then return 0 end
	local shield_old = self:GetShield()
	local shield_dmg = math.max(0,math.min(shield_old,dmg))
	self:SetShield(shield_old-shield_dmg,-shield_dmg)
	return math.max(0,shield_dmg)
end


-- percentage in [0,1]
function gObjectPrototype:GetEnergyPercentage	() return (self:GetEnergyMax() > 0) and self:GetEnergy()/self:GetEnergyMax() or 0 end

function gObjectPrototype:HasEnergy		()	return self.objtype.energy_max ~= nil end
function gObjectPrototype:GetEnergy		()	return self:IsAlive() and self:GetProp("proptype/energy") or self:GetEnergyMax() end
function gObjectPrototype:GetEnergyRate	()	return self:IsAlive() and 
	((self.objtype.energy_rate or 0) + (self:GetActiveEquipmentSum("equipment_add_energyrate") or 0)) or 0 end
function gObjectPrototype:GetEnergyMax	()	return self:IsAlive() and 
	((self.objtype.energy_max or 0) + (self:GetActiveEquipmentSum("equipment_add_energymax") or 0)) or 0 end

function gObjectPrototype:SetEnergy		(value,delta)	
	self:SetProp("proptype/energy",math.max(0,value),delta or 0,0,self:GetEnergyMax(),self:GetEnergyRate())
end

-- returns reduction done to the energy
function gObjectPrototype:ReduceEnergy	(dmg)
	if (not self:HasEnergy()) then return 0 end
	local energy_old = self:GetEnergy()
	local energy_dmg = math.max(0,math.min(energy_old,dmg))
	self:SetEnergy(energy_old-energy_dmg,-energy_dmg)
	return math.max(0,energy_dmg)
end

