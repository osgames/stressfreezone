-- see also lugre/lua/lib.plugin.lua
--[[
	known hooks that can be used :
	(this list might not be up to date, it only servers as hint for getting started)
	NotifyListener("Hook_PluginsLoaded") -- called when all plugins have been loaded
	NotifyListener("Hook_PreLoad")
	NotifyListener("Hook_MainStep") 
	NotifyListener("Hook_HUDStep") -- updates special hud elements dependant on object positions that don't have auto-tracking
	NotifyListener("Hook_MainMenu_Buttons",rows)
	NotifyListener("Hook_MainMenu_Start") 
	NotifyListener("Hook_MainMenu_StartSinglePlayer")
	NotifyListener("Hook_MainMenu_StartMultiPlayerHost")
	NotifyListener("Hook_MainMenu_StartMultiPlayerJoin")
	NotifyListener("Hook_ShipEdit_NewFile")
	NotifyListener("Hook_ShipEdit_Start")
	NotifyListener("Hook_Server_Start",port)
	NotifyListener("Hook_Server_PlayerJoined",newplayer)
	NotifyListener("Hook_Server_RecvNetMsg",myplayer,msgtype,unpack(arg))
	NotifyListener("Hook_Server_SetBody",playerid,newbody)
	NotifyListener("Hook_Client_RecvNetMsg",msgtype,unpack(arg))
	NotifyListener("Hook_Client_Connected",host,port) -- only called for remote connection
	NotifyListener("Hook_Client_SetBody",newbody,oldbody)
	NotifyListener("Hook_Client_Start") -- called for local and remote clients
	NotifyListener("Hook_Client_SetTarget",obj)  -- client selects new target
	NotifyListener("Hook_Client_MousePick",obj)  -- might be used to change cursor
	NotifyListener("Hook_Client_BuildRightClickMenu",menudata,gClientObjectUnderMouse)
	NotifyListener("Hook_Client_LeftClick",obj)
	NotifyListener("Hook_Client_OpenOwnCargoDialog")
	NotifyListener("Hook_Client_OpenOwnEquipmentDialog")
	NotifyListener("Hook_Client_OpenBaseListDialog")
	NotifyListener("Hook_Object_SpawnObject",obj)
	NotifyListener("Hook_Object_Destroy",obj)
	NotifyListener("Hook_Object_Damage",obj,attacker,shielddamage,hulldamage) -- server only ?
	NotifyListener("Hook_Object_SetParentLocation",obj,oldloc,newloc) -- called after change
	NotifyListener("Hook_Object_SetContainerContent",container,objecttype,fCurAmount)
	NotifyListener("Hook_Object_SetProp",obj,proptype,value,...)
	NotifyListener("Hook_Effect_Spawn",effect)
	NotifyListener("Hook_Effect_Destroy",effect)
	NotifyListener("Hook_Location_Spawn",loc)
	NotifyListener("Hook_Location_Destroy",loc)
	NotifyListener("Hook_Container_Spawn",container)
	NotifyListener("Hook_Container_Destroy",container)
	NotifyListener("Hook_Client_OpenTradeDialog",obj_trader)
	NotifyListener("Hook_MissionUpdate",mission)
	NotifyListener("Hook_MissionStateUpdate",mission_id,state)
	NotifyListener("Hook_TaskStateUpdate",mission_id,task_id,state)
	NotifyListener("Hook_Client_OpenOwnMissionDialog")
	NotifyListener("Hook_Client_HUDShowWaypoint",object)
	NotifyListener("Hook_FireWeapon",ship,weapon_container,target)
]]--

function LoadPlugins_SFZ () LoadPlugins(gMainPluginDir) LoadPlugins(gPluginDir) end
