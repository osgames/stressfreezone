-- TODO : obsolete, clean me up and reuse !

hud = {} -- global hud object
hud.buttons = {} -- global hud object
hud.activebutton = nil
kHUDObjCat_None = 0
kHUDObjCat_Missile = 1
kHUDObjCat_Asteroid = 2
kHUDObjCat_Ship = 3

gEnableHUDElements = true

-- the current player body can always be accessed by client.playerbody
-- the name of a ship can be accessed by "o.infotext"
-- the HUDObjCat of an object can be accessed by "o.GetHUDObjCat()"
-- see also o.CanBeHitByShot(shotobj) , o.CanCollide(other) , o.team

hud.mbGUIMode = false
hud.mbGUIModeInit = true
function hud.ToggleGUIMode ()
	if (hud.mbGUIMode) then hud.SetGUIMode(false) else hud.SetGUIMode(true) end
end
function hud.SetGUIMode (mode)
	if (hud.mbGUIMode ~= mode or hud.mbGUIModeInit) then
		hud.mbGUIModeInit = false
		hud.mbGUIMode = mode
		Client_SetGUIMode(hud.mbGUIMode)
		if (hud.mbGUIMode) then
		--	CEGUI_SetMouseCursor("TaharezLook","MouseArrow")
		else
		--	CEGUI_SetMouseCursor("TaharezLook","MouseArrowFly")
		end
		-- todo : remember down keys and mousebuttons, and when changing modes, reset state of cegui
		-- todo : fade out windows ? make dialog boxes transparent ?
	end
end

gMyCredits = 0

-- called from lib.client.lua on msg S_SetCurBuyZone
function hud.SetCurBuyZone (buyzoneid)
	print("hud.SetCurBuyZone",buyzoneid)
	if (buyzoneid > 0) then
		gui.ShowBuyButton()
		-- hud.buybutton.mbVisible = true
		-- hud.fuelbutton.mbVisible = true
	else 
		gui.HideBuyButton()
		-- we are not in a buyzone
		-- hud.buybutton.mbVisible = false
		-- hud.fuelbutton.mbVisible = false
	end
end

-- called from lib.client.lua on msg S_SetCurCredits
function hud.SetCurCredits (credits)
	print("hud.SetCurCredits",credits)
	gMyCredits = credits
	if hud.creditstext then 
		hud.creditstext.gfx:SetText("$"..gMyCredits)
	end
end

-- called from lib.input.lua
function hud.KeyDown (key,char)
	if (hud.mbGUIMode) then
		if (key == key_mouse1 or key == key_mouse2 or key == key_mouse3) then
			--CEGUI_InjectMouseButtonDown(key)
		else
			--CEGUI_InjectKeyDown(key)
			--CEGUI_InjectChar(char)
		end
	end
end

-- called from lib.input.lua
function hud.KeyUp (key)
	if (hud.mbGUIMode) then
		if (key == key_mouse1 or key == key_mouse2 or key == key_mouse3) then
			--CEGUI_InjectMouseButtonUp(key)
		else
			--CEGUI_InjectKeyUp(key)
		end
	end
end


function MouseEnterHUDElement (uid)
	-- todo : change cegui cursor ?
	if (hud.mbGUIMode) then
		print("MouseEnterHUDElement",uid)
		local mybutton = hud.buttons[uid]
		if (mybutton and mybutton:IsAlive()) then
			if (mybutton.on_mouse_enter) then mybutton:on_mouse_enter() end
			hud.activebutton = mybutton
		end
	end
end

function MouseLeaveHUDElement (uid)
	print("MouseLeaveHUDElement",uid)
	local mybutton = hud.buttons[uid]
	if (mybutton and mybutton:IsAlive()) then
		if (hud.activebutton and mybutton.miUID == hud.activebutton.miUID) then hud.activebutton = nil end
		if (mybutton.on_mouse_leave) then mybutton:on_mouse_leave() end
	end
end

function hud.MouseButtonRight (state)
end
function hud.MouseButtonLeft (state)
	if (state > 0) then
		-- button down
		local mybutton = hud.activebutton
		if (mybutton and mybutton:IsAlive()) then
			if (mybutton.onMouseClick) then mybutton:onMouseClick() end
		end
	else 
		-- button up
		-- print("hud.click release")
	end
end

function CreateHUDElement2DWrapper () 
	local res = CreateHUDElement2D()
	res.gfx = res.mpGfx2D
	return res
end
				
function hud.CreatePanel (cx,cy,mat,parent) 
	local panel = CreateHUDElement2DWrapper()
	panel.gfx:InitCCPO(parent)
	--panel.gfx:SetClip(0,0,600,400)
	if (mat) then panel.gfx:SetMaterial(mat) end
	panel.gfx:SetDimensions(cx,cy)
	panel.mbVisible = true
	return panel
end

function hud.CreateText (cx,cy,parent,charh,font) 
	local panel = CreateHUDElement2DWrapper()
	panel.gfx:InitText(parent)
	panel.gfx:SetCharHeight(charh or 16)
	panel.gfx:SetFont(font or "TrebuchetMSBold")
	panel.gfx:SetDimensions(cx,cy)
	panel.mbVisible = true
	return panel
end


-- assumes that the mouseover effect is below the normal image
function hud.CreateButton (cx,cy,mat,u1,v1,u2,v2) 
	local button = hud.CreatePanel(cx,cy,mat)
	local v3 = v2+(v2-v1)
	button.gfx:SetUV(u1,v1,u2,v2)
	button.mbWatchMouse = true
	hud.buttons[button.miUID] = button
	button.on_mouse_enter = function (self) self.gfx:SetUV(u1,v2,u2,v3)	end
	button.on_mouse_leave = function (self) self.gfx:SetUV(u1,v1,u2,v2)	end
	return button
end

function hud.StartGame()
	if gEnableHUDElements then

		hud.starfield = CreateCamPosGfx3D()
		hud.starfield:SetStarfield(gNumberOfStars,gStarsDist,gStarColorFactor,"starbase")
		
		hud.speedlines = CreateCamPosGfx3D()
		hud.speedlines:SetSpeedLines(200,3000,gStarColorFactor,"speedlines",1,5)
		
		-- hud.radar = CreateCockpitGfx3D()
		-- hud.radar = CreateRootGfx3D()
		hud.radar = CreateCamPosGfx3D()
		hud.radar:SetRadar()
		-- hud.radar:SetPosition()
		
		hud.radialgrid = CreateRootGfx3D()
		hud.radialgrid:SetRadialGrid()
		
		hud.crosshair = hud.CreatePanel(128,128,"crosshair01")
		hud.crosshair.gfx:SetColours({1,0,0,1},{0,1,0,1},{0,0,1,1},{1,1,1,1})
		hud.crosshair.gfx:SetAlignment(kGfx2DAlign_Center,kGfx2DAlign_Center)
		hud.crosshair.gfx:SetPos(-64, -64)
		--hud.crosshair.mbWatchMouse = false
		--crosshair:SetScale(1, 1.0 / 1.3333)
		--crosshair:SetRotate(-0.1*math.pi)
		--crosshair:SetAlignment(kGfx2DAlign_Left,kGfx2DAlign_Top)
		--crosshair.miTurnMode = kTurnMode_Constant
		--crosshair.mfTurnBase = 0.01*math.pi
		
		hud.buybutton = hud.CreateButton(96,32,"hud_buttons",0.0,0.0,0.75,0.25)
		hud.buybutton.gfx:SetAlignment(kGfx2DAlign_Right,kGfx2DAlign_Top)
		hud.buybutton.gfx:SetPos(-96, 20)
		hud.buybutton.onMouseClick = function (self) 
			print("buy button clicked") 
			-- TODO : this buybutton isn used, seek and destroy
		end
		hud.buybutton.mbVisible = false
		
		hud.fuelbutton = hud.CreateButton(96,32,"hud_buttons",0.0,0.50,0.75,0.75)
		hud.fuelbutton.gfx:SetAlignment(kGfx2DAlign_Right,kGfx2DAlign_Top)
		hud.fuelbutton.gfx:SetPos(-96, 20+32)
		hud.fuelbutton.onMouseClick = function (self) print("fuel button clicked") end
		hud.fuelbutton.mbVisible = false
		
		-- how much "life" the player has left, top left of the screens
		hud.hullinfotextpanel = hud.CreatePanel(0,0)
		hud.hullinfotext = hud.CreateText(100,100,hud.hullinfotextpanel.gfx,64)
		hud.hullinfotext.gfx:SetColour({0,1,0,1})
		hud.hullinfotextpanel.gfx:SetAlignment(kGfx2DAlign_Left,kGfx2DAlign_Top)
		hud.hullinfotextpanel.gfx:SetPos(0,0)
		
		-- how much "speed" the player has
		hud.speedtextpanel = hud.CreatePanel(0,0)
		hud.speedtext = hud.CreateText(100,100,hud.speedtextpanel.gfx,16)
		hud.speedtext.gfx:SetColour({1,1,0,1})
		hud.speedtextpanel.gfx:SetAlignment(kGfx2DAlign_Right,kGfx2DAlign_Top)
		hud.speedtextpanel.gfx:SetPos(-100,15)
		hud.speedtext.miTextMode = kTextMode_AbsSpeed
		
		-- how much "speed" the player has
		hud.cdisttextpanel = hud.CreatePanel(0,0)
		hud.cdisttext = hud.CreateText(100,100,hud.cdisttextpanel.gfx,16)
		hud.cdisttext.gfx:SetColour({1,1,0,1})
		hud.cdisttextpanel.gfx:SetAlignment(kGfx2DAlign_Right,kGfx2DAlign_Top)
		hud.cdisttextpanel.gfx:SetPos(-100,30)

		-- how much "credits" the player has, bottom left of the screens
		hud.creditstextpanel = hud.CreatePanel(0,0)
		hud.creditstext = hud.CreateText(100,100,hud.creditstextpanel.gfx,32)
		hud.creditstext.gfx:SetColour({0,0,1,1})
		hud.creditstextpanel.gfx:SetAlignment(kGfx2DAlign_Left,kGfx2DAlign_Bottom)
		hud.creditstextpanel.gfx:SetPos(0,-80)
		
		hud.fpstextpanel = hud.CreatePanel(0,0)
		hud.fpstext = hud.CreateText(100,100,hud.fpstextpanel.gfx,16)
		hud.fpstextpanel.gfx:SetAlignment(kGfx2DAlign_Right,kGfx2DAlign_Top)
		hud.fpstextpanel.gfx:SetPos(-100,0)
		hud.fpstext.miTextMode = kTextMode_FPS
		hud.UpdateOwnHP()
		
		hud.SetGUIMode(false)
		-- Client_SetGUIMode(hud.mbGUIMode)
		
	end
end


function hud.ClientKill(o)
end

function hud.ClientSpawn(o)
	if gEnableHUDElements then
		if (o:GetHUDObjCat() == kHUDObjCat_Ship) then
			if false then
				o.marker = hud.CreatePanel(16,16)
				o.marker.gfx:SetColour(hud.GetTeamColor(o.team))
				--o.marker = hud.CreatePanel(16,16,"objmark_vorhalt")
				o.marker.mbPosModeObject_UpdateSize = true
				o.marker.mvParam = {-1,-1}
				o.marker.mvSizeParam = {0,0}
				o.marker.miPosMode = kPosMode_Object
				o.marker.mpObj1 = o
				-- o.hudelem = CreateHUDElement2DWrapper()
				-- self.objectmarker = CreateObjectMarker(self)
				-- self.objectmarker.gfx:SetText(self.infotext)
				-- self.objectmarker.gfx:SetColor(hud.GetTeamColor(team))
			end
			
			
			-- 4 edgemarkers
			if true then
				local marker_edge = {}
				local i
				for i = 0,3 do
					--local edge = hud.CreatePanel(16,16,"objmark_vorhalt")
					--local edge = hud.CreatePanel(16,16,"objmark_edge",o.marker.gfx)
					local edge = hud.CreatePanel(16,16,"objmark_edge")
					--edge:SetPos(-8,-8)
					edge.miPosMode = kPosMode_Object
					edge.gfx:SetColour(hud.GetTeamColor(o.team))
					edge.mpObj1 = o
					edge.mvBase = {-8,-8}
					marker_edge[i] = edge
				end
				
				local e = 0.8
				marker_edge[0].mvParam = {-e,-e}
				marker_edge[1].mvParam = {-e,e}
				marker_edge[2].mvParam = {e,-e}
				marker_edge[3].mvParam = {e,e}
				marker_edge[0].gfx:SetUV(0.0,0.0,1.0,1.0)
				marker_edge[1].gfx:SetUV(0.0,1.0,1.0,0.0)
				marker_edge[2].gfx:SetUV(1.0,0.0,0.0,1.0)
				marker_edge[3].gfx:SetUV(1.0,1.0,0.0,0.0)
				
				o.marker_edge = marker_edge
			end 
			
			-- infotext
			if true then
				o.infotextpanel = hud.CreatePanel(0,0)
				o.infotext_textarea = hud.CreateText(100,100,o.infotextpanel.gfx)
				--o.infotext_textarea.gfx:SetText(o.infotext)
				hud.UpdateHP(o)
				o.infotext_textarea.gfx:SetColour(hud.GetTeamColor(o.team))
				
				o.infotextpanel.miPosMode = kPosMode_Object
				o.infotextpanel.mpObj1 = o
				o.infotextpanel.mvBase = {8,-8}
				
				local e = 0.8
				o.infotextpanel.mvParam = {e,-e}
			end 
			
			
			-- distance text
			if true then
				o.disttextpanel = hud.CreatePanel(0,0)
				o.disttext_textarea = hud.CreateText(100,100,o.disttextpanel.gfx)
				o.disttext_textarea.gfx:SetText("")
				o.disttext_textarea.gfx:SetColour(hud.GetTeamColor(o.team))
				o.disttext_textarea.miTextMode = kTextMode_Dist
				o.disttext_textarea.mpObj1 = o
				
				o.disttextpanel.miPosMode = kPosMode_Object
				o.disttextpanel.mpObj1 = o
				o.disttextpanel.mvBase = {-8,8}
				
				local e = 0.8
				o.disttextpanel.mvParam = {-e,e}
			end 
			
			-- vorhalt marker
			if true then
				local vorhalt = hud.CreatePanel(32,32,"objmark_vorhalt")
				vorhalt.gfx:SetColour(hud.GetTeamColor(o.team))
				vorhalt.miPosMode = kPosMode_Aim
				vorhalt.mpObj1 = o
				vorhalt.mvBase = {-16,-16}
				vorhalt.mfParam = 8000 -- bullet speed for calc
				o.vorhalt = vorhalt
			end
			
			-- direction marker (shows where to turn when enemy is offscreen)
			if true then
				local dirmark = hud.CreatePanel(32,32,"objmark_vorhalt")
				dirmark.miPosMode = kPosMode_ObjectDir
				dirmark.gfx:SetColour(hud.GetTeamColor(o.team))
				dirmark.mpObj1 = o
				dirmark.mvParam = {0,0}
				dirmark.mvParam2 = {0,0}
				dirmark.mvBase = {-16,-16}
				o.dirmark = dirmark
			end
		end
	end
end

function hud.UpdateOwnHP () 
	if (hud.hullinfotext) then
		local o = client.playerbody
		if (o and o:IsAlive()) then
			local maxhp = o.iMaxHP
			if (maxhp <= 0) then maxhp = 1 end
			if (o.iHP > 0) then
				hud.hullinfotext.gfx:SetText(sprintf("%3.0f%%",100*o.iHP/maxhp))
			else
				hud.hullinfotext.gfx:SetText("")
			end
		else 
			hud.hullinfotext.gfx:SetText("")
		end
	end
end

function hud.UpdateHP (o) 
	local p = client.playerbody
	if (o and o:IsAlive() and p and p:IsAlive() and o.miID == p.miID) then hud.UpdateOwnHP() end
	if (o and o.infotext_textarea and o.infotext_textarea:IsAlive()) then
		-- o.infotext_textarea.gfx:SetText(o.infotext .. "\n" .. o.iHP .. "\n" .. o.iMaxHP)
		local maxhp = o.iMaxHP
		if (maxhp <= 0) then maxhp = 1 end
		o.infotext_textarea.gfx:SetText(sprintf("%s\n%3.0f%%",o.infotext,100*o.iHP/maxhp))
	end
end

function hud.NotifyDamage(o,amount)
	hud.UpdateHP(o)
end

function hud.SetOwnName(name)
	-- doesn't really matter
	-- client.playerbody.objectmarker.gfx:SetText(s)
end

--- body can be nil... but don't count on this being called when the player dies...
function hud.AssignBody(playerid,body,bIsSelf)
	if (bIsSelf) then
		if (body and body:IsAlive()) then
			hud.UpdateOwnHP()
			-- Client_SetPlayerShip(client.playerbody)
			-- client.playerbody.objectmarker.mbVisible = false
			if (body.marker) then body.marker.mbVisible = false end
			if (body.dirmark) then body.dirmark.mbVisible = false end
			if (body.vorhalt) then body.vorhalt.mbVisible = false end
			if (body.infotextpanel) then body.infotextpanel.mbVisible = false end
			if (body.disttextpanel) then body.disttextpanel.mbVisible = false end
			if (body.marker_edge) then
				body.marker_edge[0].mbVisible = false
				body.marker_edge[1].mbVisible = false
				body.marker_edge[2].mbVisible = false
				body.marker_edge[3].mbVisible = false
			end
		end
	end
end

function hud.GetTeamColor(team)
	if (team == 0) then			return {1,1,0,1}
	elseif (team == -1) then	return {1,0,0,1}
	else						return {0,0,1,1}
	end
end
