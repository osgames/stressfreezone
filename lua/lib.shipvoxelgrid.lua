-- see also lib.shipgfx.lua
-- don't confuse with lib.voxel.lua 
-- also used by lib.interior.walk.lua lib.shipgfx.lua 


function ShipVoxelGrid_CreateFromFilePath (filepath,bFixHoles)
	return ShipVoxelGrid_CreateFromModuleList(LoadShipType(filepath),bFixHoles)
end

function ShipVoxelGrid_IsRamp (shipVoxelGrid,x,y,z)  -- true if passable, but can also be above ground, false for ramps !
	local m = ShipVoxelGrid_GetModuleAtPos(shipVoxelGrid,x,y,z)
	if (not m) then return false end -- no ramps in the void outside of the ship
	if (TestIfModuleIsBox(m)) then return false end -- a box is not a ramp
	return true 
end

function ShipVoxelGrid_IsFreeInteriorPos (shipVoxelGrid,x,y,z)  -- true if passable, but can also be above ground, false for ramps !
	local m = ShipVoxelGrid_GetModuleAtPos(shipVoxelGrid,x,y,z)
	if ((not m) or (not TestIfModuleIsBox(m))) then return false end
	return true
end

function ShipVoxelGrid_CanStandAtPos (shipVoxelGrid,x,y,z) -- must be on ground
	if (not ShipVoxelGrid_IsFreeInteriorPos(shipVoxelGrid,x,y,z)) then return false end
	local ground = ShipVoxelGrid_GetModuleAtPos(shipVoxelGrid,x,y - 1,z)
	if (not ground) then return true end -- cannot stand in mid-air
	return false
end

-- x,y,z = ShipVoxelGrid_GetFallenPos(shipVoxelGrid,x,y,z)
function ShipVoxelGrid_GetFallenPos	(shipVoxelGrid,x,y,z)
	while ShipVoxelGrid_IsFreeInteriorPos(shipVoxelGrid,x,y-1,z) do y = y - 1 end
	return x,y,z
end

--  local x,y,z = ShipVoxelGrid_GetInteriorStartPos(shipVoxelGrid) 
function ShipVoxelGrid_GetInteriorStartPos (shipVoxelGrid) 
	local found
	local founddist
	local g = shipVoxelGrid
	local tz = g.minz -- math.floor(0.5*(g.minz+g.maxz))
	local tx = math.floor(0.5*(g.minx+g.maxx))
	local ty = math.floor(0.5*(g.miny+g.maxy))
	for x = g.minx,g.maxx do
	for y = g.miny,g.maxy do
	for z = g.minz,g.maxz do
		if (ShipVoxelGrid_CanStandAtPos(shipVoxelGrid,x,y,z)) then
			local dist = sqdist3(x,y,z,tx,ty,tz) 
			if ((not founddist) or founddist > dist) then
				founddist = dist
				found = {x+0.5,y+0.5,z+0.5,m}
			end
		end
	end
	end
	end
	return unpack(found)
end

function ShipVoxelGrid_GetModuleAtPos (shipVoxelGrid,x,y,z)
	return shipVoxelGrid[ShipVoxelGrid_GetCoordName(x,y,z)]
end

function ShipVoxelGrid_TestIfIsPositionCompletelyFilled (shipVoxelGrid,x,y,z)
	local m = ShipVoxelGrid_GetModuleAtPos(shipVoxelGrid,x,y,z)
	return m and TestIfModuleIsBox(absgeo)
end

function ShipVoxelGrid_GetCoordName (x,y,z) return math.floor(x).."|"..math.floor(y).."|"..math.floor(z) end



-- used for determining inside and outside
-- fills a 3dimensional array with pointers to the modules at all positions they overlap
function ShipVoxelGrid_CreateFromModuleList (modulelist,bFixHoles)
	local res = {}
	for k,m in pairs(modulelist) do
		local absgeo = GetModuleAbsGeom(m)
		local bAllGeomSidesAreAxisAligned = TestIfAllGeomSidesAreAxisAligned(absgeo)
		if (bAllGeomSidesAreAxisAligned or (not bFixHoles)) then 
			local minx,miny,minz, maxx,maxy,maxz = GetGeoBounds(absgeo)
			for x=minx+0.5,maxx do
			for y=miny+0.5,maxy do
			for z=minz+0.5,maxz do
				res[ShipVoxelGrid_GetCoordName(x,y,z)] = m
				res.minx = math.floor(math.min(res.minx or x,x))
				res.miny = math.floor(math.min(res.miny or y,y))
				res.minz = math.floor(math.min(res.minz or z,z))
				res.maxx = math.floor(math.max(res.maxx or x,x))
				res.maxy = math.floor(math.max(res.maxy or y,y))
				res.maxz = math.floor(math.max(res.maxz or z,z))
			end
			end
			end
		end
	end
	return res
end


-- returns true if either side of the poly is not inside another module
function TestIfSideIsBorder (own_module_absgeom,shipVoxelGrid,x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4)
	local nx,ny,nz = Vector.normalise(Vector.cross(x3-x1,y3-y1,z3-z1,x2-x1,y2-y1,z2-z1))
	if (not NormalIsAxisAligned(nx,ny,nz)) then return true end
	local xa,ya,za, xb,yb,zb = GetSidePointPair (x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4)
	if (not ShipVoxelGrid_GetModuleAtPos(shipVoxelGrid,xa,ya,za)) then return true end
	if (not ShipVoxelGrid_GetModuleAtPos(shipVoxelGrid,xb,yb,zb)) then return true end
	return false
end

