-- handling groups of ships

-- contains groups
-- TODO this array is used via [key] = nil and table.insert, this could cause problems?
glShipGroup = {}
GroupAIs = {}

-- group = {name, ships={ship,ship,ship,...}, homePos, state, ai}

-- ship group prototype (class)
gShipGroupPrototype = {}

function RegisterGroupAI(name, groupai )
	GroupAIs[name] = groupai
	--print ("DEBUG: Registed group AI \"" .. name .. "\"\n")
end

gShipGroupPrototype.SetAI = function (self, ai)

	local ainame = ai
	ai = GroupAIs[ai]

	if ai == nil then 
		ai = GroupAIs["Standard"] 
		print("Invalid AI: \"" .. ainame .. "\" using Standard AI\n") 
	end

	-- if the group already had an AI type, call the required deinit
	-- function
	
	if self.ai ~= nil then self.ai.Deinit(self) end

	-- Basic AI actions, can add more as needed

	self.ai     = ai
	self.Attack = ai.Attack
	self.Move   = ai.Move
	self.Idle   = ai.Idle

	for id,ship in pairs(self.ships) do
		ship.onDamage = ai.onDamage
		ship.onDeath  = ai.onDeath
	end

	-- init the new AI type
	ai.Init(self)

end

gShipGroupPrototype.SetState = function (self, newstate, goal)
	self.state = newstate
	self.goal = goal
end

-- kills the complete group (including every ship)
gShipGroupPrototype.Destroy = function (self)
	-- print("DEBUG","group destroy",countarr(self.ships))
	if self.onDestroy ~= nil then self:onDestroy() end

	for k,v in pairs(self.ships) do
		-- print("DEBUG",k,v)
		if v:IsAlive() then v:Destroy() end
	end

	if self.ai.Deinit then self.ai.Deinit(self) end

	self.mbRemoveGroupOnNextStep = true
end

-- sets the home position of the group
gShipGroupPrototype.SetHomePosition = function (self, x, y, z)
	self.homePos = {x,y,z}
end

-- returns the number if living ships, if zero the group is dead
gShipGroupPrototype.CountShipsAlive = function (self)
	local alive = 0

	for k,v in pairs(self.ships) do
		if v:IsAlive() then alive = alive + 1 end
	end

	return alive
end

-- is alive
gShipGroupPrototype.IsAlive = function (self)
	
	-- if there is atleast one ship left this group is alive
	for k,v in pairs(self.ships) do 
		if v:IsAlive() then return true end
	end

	return false
end

function SpawnShip ( shiptype, x, y, z)

	local ship = SpawnObject(shiptype, gMainLocation, {x,y,z}, {Quaternion.random()})
	ship:SetName(GenerateRandomName_Pilot())

	return ship
end

-- spawns a group of enemies distributed randomly around x,y,z (radius)
-- ships : {typename=amount,typename=amount,...}

function SpawnShipGroup (ships, x, y, z, radius, ai)

	if ai == nil then ai = "Standard" end

	local g = {}
	local shipid = 0;

	ArrayOverwrite(g, gShipGroupPrototype)	

	g.ships = {}
	g:SetHomePosition(x,y,z)
	
	-- spawn ships
	for typename,amount in pairs(ships) do
		 -- print("DEBUG",typename,amount)
		for i = 1,tonumber(amount) do
			local px,py,pz = Vector.random2( x - radius,y - radius,z - radius, x + radius,y + radius, z + radius )
			local ship = SpawnShip(typename, px, py, pz)
			ship.group = g

			g.ships[ship.id] = ship
			shipid = shipid + 1

		end
	end
	
	-- All groups start in the "Idle" state
	g:SetAI(ai)
	g:Idle()

	g.nextsid = shipid

	-- add to the global group list
	table.insert(glShipGroup, g)
	
	return g
end

gShipGroupPrototype.AddShips = function (self, ships, x, y, z, radius)

	if radius == nil then radius = 0 end
	local shipid = self.nextsid

	for typename,amount in pairs(ships) do
		
		-- print("DEBUG",typename,amount)
		
		for i = 1,tonumber(amount) do
			local px,py,pz = Vector.random2( x-radius,y-radius,z-radius,
			                                 x+radius,y+radius,z+radius )
							
			local ship = SpawnObject(typename, gMainLocation, {px,py,pz}, {Quaternion.random()})
			ship:SetName(GenerateRandomName_Pilot())
			ship.group = self

			ship.onDamage = self.ai.onDamage
			ship.onDeath  = self.ai.onDeath

			self.ships[ship.id] = ship
			
			shipid = shipid + 1
		end
	end

	self.nextsid = shipid

end


--
-- calls the stepper of every group
function ShipGroupStepAll ()
	for k,v in pairs(glShipGroup) do
		if v.mbRemoveGroupOnNextStep then
			glShipGroup[k] = nil
			-- print("DEBUG","removed group")
		else
			v:Step()
		end
	end
end


-- TODO: These functions should probably check is the object is a ship
--       alternatively, they could be made as part of the basic object
--       class

function ShipDeathHandler(ship)
	-- ensure this is a ship with a group
	if( ship.group == nil) then return end

	if ship.onDeath ~= nil then ship:onDeath() end

	-- print("DEBUG","ship destroy")

	-- remove dead ship
	ship.group.ships[ship.id] = nil
	
	-- remove the group if this was the last ship in the group
	if( countarr(ship.group.ships) == 0 ) then 
		ship.group:Destroy()
	end

	return
end

function ShipDamageHandler(ship, attacker, shielddamage, hulldamage)

	if ship.onDamage ~= nil then ship:onDamage(attacker, shielddamage, hulldamage) end
	return
end

RegisterListener("Hook_MainStep",ShipGroupStepAll)

RegisterListener("Hook_Object_Damage", ShipDamageHandler)
RegisterListener("Hook_Object_Destroy", ShipDeathHandler)
