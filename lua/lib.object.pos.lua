-- object methods related to object position and rotation
-- also for coordinate system translation and distance measurement



-- returns x,y,z : transforms a global position to a object-local coords
function gObjectPrototype:GlobalToLocal (x,y,z)
	local x0,y0,z0 = unpack(self.mvPos)
	return Quaternion.ApplyToVector(x-x0,y-y0,z-z0,Quaternion.inverse(unpack(self.mqRot)))
end

-- returns x,y,z : transforms a local-object position to global coords
function gObjectPrototype:LocalToGlobal (x,y,z) 
	local x0,y0,z0 = Quaternion.ApplyToVector(x,y,z,unpack(self.mqRot))
	return x0 + self.mvPos[1], y0 + self.mvPos[2], z0 + self.mvPos[3]
end

-- returns qw,qx,qy,qz : calculates the relative rotation to a position in global coords
function gObjectPrototype:CalcRotationTo (x,y,z) return Quaternion.getRotation(0,0,-1,self:GlobalToLocal(x,y,z)) end

function gObjectPrototype:GetDistToPos (x,y,z) 
	local x1,y1,z1 = unpack(self.mvPos)
	return Vector.len(x-x1,y-y1,z-z1) 
end

function gObjectPrototype:GetDistToObject (other) 
	return Vector.len(self:GetVectorToObject(other)) 
end

function gObjectPrototype:GetVectorToObject (other) 
	-- TODO handle object locations correctly
	assert(other,"obj:GetVectorToObject called without other object")
	assert(other:IsAlive(),"obj:GetVectorToObject called with dead other object")
	local x1,y1,z1 = unpack(self.mvPos)
	local x2,y2,z2 = unpack(other.mvPos)
	return x2-x1,y2-y1,z2-z1
end 


-- returns xpixel,ypixel,cx,cy
function gObjectPrototype:GetProjectedSizeAndPosInPixels()
	local x,y,z = unpack(self.mvPos)
	local r = self.mfBoundingRad
	local px,py,pz,cx,cy,cz = ProjectSizeAndPosEx(x,y,z,r)
	local vw,vh = GetViewportSize()
	px = ( px + 1.0)*0.5*vw
	py = (-py + 1.0)*0.5*vh
	cx = cx * vw*0.5
	cy = cy * vh*0.5
	local bIsBehindCam = cz >= 0

	local mvTrackClampMinX,mvTrackClampMinY = 0,0
	local mvTrackClampMaxX,mvTrackClampMaxY = vw,vh
	
	local nx = math.max(mvTrackClampMinX,math.min(mvTrackClampMaxX,px));
	local ny = math.max(mvTrackClampMinY,math.min(mvTrackClampMaxY,py));
	if (bIsBehindCam) then nx = (px > 0.5*vw) and mvTrackClampMinX or mvTrackClampMaxX end
	if (bIsBehindCam) then ny = (py > 0.5*vh) and mvTrackClampMinY or mvTrackClampMaxY end
	--~ 	bDidClamp = (nx != p.x) || (ny != p.y);
	px = nx
	py = ny
	return px,py,cx,cy
end
