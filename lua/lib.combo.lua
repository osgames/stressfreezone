-- object combo system (HARVEST x USING y GIVES y.bla z)

gComboCurFilePath = nil -- used for debug messages during parsing

kComboObjImage_ext = {"png","jpg","gif","tga"} -- todo, more ?
kComboObjMesh_ext = {"mesh"}

function Combo_SearchPath_ObjImage	(typename) return Combo_SearchPath(kComboObjImagePath,typename,kComboObjImage_ext) end
function Combo_SearchPath_ObjMesh	(typename) return Combo_SearchPath(kComboObjMeshPath ,typename,kComboObjMesh_ext) end


function Combo_SearchPath (folderpath,start,extlist) 
	start = start.."." -- search for name terminated by dot (either file extension or author/license info
	for k1,filename in pairs(dirlist(folderpath,false,true)) do
		if (beginswith(filename,start)) then 
			local ext = fileextension(filename)
			for k,ext2 in pairs(extlist) do if (ext == ext2) then return folderpath..filename end end
		end
	end
end

--~ kComboObjImagePath			= gWritablePath.."obj_image/"
--~ kComboObjMeshPath			= gWritablePath.."obj_mesh/"

--[[


a)	object("knife"):is("blade"):with({damage=5})
b)	object({"knife",type="blade",damage=5})

a)	object("bush"):is("plant"):with({woodamount=5})
b)	object({"bush",type="plant",woodamount=5})
c)	OBJECT bush IS plant WITH woodamount 5

a)	use("blade"):on("plant"):gives("firewood"):amount("woodamount")
b)	use({"blade",on="plant",gives={"firewood","woodamount"}})

obj("hammer"):is("impact"):with({damage=3})
obj("knife"):is("blade"):with({damage=5})
obj("bush"):is("plant"):with({woodamount=2})
obj("tree"):is("plant"):with({woodamount=10})

use("impact"):on("stone"):gives("rubble")
use("blade"):on("plant"):gives("firewood"):amount("woodamount")


]]--

function Combo_ParseFile (path) 
	gComboCurFilePath = path
	local linenumber = 0
	for line in io.lines(path) do 
		linenumber = linenumber + 1
		Combo_ParseLine(linenumber,line)
	end
end

-- takes strings like "a=1,b=2" and returns a table like {a=1,b=2} for them
function Combo_ParseParamList (paramlisttext)
	local res = {}
	for k1,propdef in pairs(strsplit(",",paramlisttext or "")) do 
		local k,v = unpack(strsplit("[ =]",propdef))
		k = trim(k) 
		if (k ~= "") then 
			v = v and trim(v) or true
			if (v == "true")	then	v = true end
			if (v == "false")	then	v = false end
			if (v == "nil")		then	v = nil end
			res[k] = v
		end
	end
	return res
end

function Combo_ParseLine (linenumber,line)
	line = trim(line)
	line = string.gsub(line,"#.*$","") -- remove comments
	if (line == "") then return end
	
	local loctxt = tostring(basename(gComboCurFilePath))..":"..linenumber..":" -- file:123:  useful for debug
	
	if (beginswith(line,"OBJECT")) then
		local					typename,parent,params 	= string.match(line,"OBJECT%s+(.*)%s+IS%s+(.*)%s+WITH%s+(.*)")
		if (not typename) then	typename,parent			= string.match(line,"OBJECT%s+(.*)%s+IS%s+(.*)") end
		params = Combo_ParseParamList(params)
		
		print("object:",typename,parent,vardump(params))
	end
	
	
	print(loctxt,line)
end

-- ./sfz -combotest
function ComboTest ()
	print("##### combotest")
	print("Combo_SearchPath_ObjImage",	Combo_SearchPath_ObjImage("flower"))
	print("Combo_SearchPath_ObjMesh",	Combo_SearchPath_ObjMesh("flower"))
	Combo_ParseFile(kComboDataPath)
	
	os.exit(0)
end


