gMusicFiles = {}

-- lists all lua files in kMusicDir and add them to the list
function StartMusic ()
	local arr_files = dirlist(kMusicDir,false,true)
	local sortedfilenames = {}
	for k,filename in pairs(arr_files) do 
		if (filename ~= ".." and filename ~= "." and filename ~= ".svn") then
			table.insert(sortedfilenames,kMusicDir..filename) 
		end
	end
	table.sort(sortedfilenames)
	gMusicFiles = sortedfilenames
	PlayRandomMusic()
end

function PlayRandomMusic ()
	local mytrack = gMusicFiles[math.random(table.getn(gMusicFiles))]
	if (mytrack) then print("starting music",mytrack) SoundPlayMusic(mytrack) end
end
