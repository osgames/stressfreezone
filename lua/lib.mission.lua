-- manages missions accepted by the players, e.g. bring some cargo somewhere
-- some infos (id,employer,description,tasks) about all accepted missions are also sent to the client
-- but the details (event scripts, etc) remain on the server, and are not needed by the client

gMissionsByID = {}
gNextMissionID = 1

kTaskState_Running = 0
kTaskState_Finished = 1
kTaskState_Failed = 2


-- ##################### mission task #############################

gTaskPrototype = {}
-- task description
gTaskPrototype.msDescription = "add task description here"
-- is this task finished or failed?
gTaskPrototype.mState = kTaskState_Running
-- object to mark a position or nil
gTaskPrototype.mWhereToGoNext = nil
-- this functions get called to check if the mission finished or failed
-- or timed events or whatever you want
function gTaskPrototype:Update () --[[ overwrite me ]] end
-- mission successfully finished
function gTaskPrototype:OnFinish () --[[ overwrite me ]] end
-- mission failed or canceled
function gTaskPrototype:OnFailure () --[[ overwrite me ]] end

-- ##################### mission #############################

gMissionPrototype = {}
gMissionPrototype.mlTask = nil
gMissionPrototype.msDescription = "add mission description here"
gMissionPrototype.msName = "add mission name here"
-- mission id
gMissionPrototype.miID = nil
-- current mission state
gMissionPrototype.mState = kTaskState_Running
-- employer object
gTaskPrototype.mEmployer = nil

-- this functions get called to check if the mission finished or failed
-- or timed events or whatever you want
function gMissionPrototype:Update () 
	local lastmissionstate = self.mState
	local needupdate = false
	
	for k,v in pairs(self.mlTask) do 
		local laststate = v.mState
		local lastWhereToGo = v.mWhereToGoNext
		
		-- update the task
		v:Update() 
		
		-- and look for state changes
		if ( laststate ~= v.mState ) then
			-- state changed so update needed
			needupdate = true
			
			-- call finished or failure callbacks
			if v.mState == kTaskState_Finished then
				v:OnFinish()
			elseif self.mState == kTaskState_Failed then
				v:OnFailure()
			end
			
			self:SendUpdateTaskState(k)
		end
		
		-- look for where to go changes
		if ( v.mWhereToGoNext ~= lastWhereToGo ) then needupdate = true end
		
	end
	
	-- change the mission state?
	self:UpdateState()
	if lastmissionstate ~= self.mState then
		needupdate = true
		
		-- call finished or failure callbacks
		if self.mState == kTaskState_Finished then
			self:OnFinish()
		elseif self.mState == kTaskState_Failed then
			self:OnFailure()
		end
		
		self:SendUpdateMissionState()
	end
	
	-- send mission changes
	if needupdate then self:SendMissionUpdate() end
end
-- mission successfully finished
function gMissionPrototype:OnFinish () --[[ overwrite me ]] end
-- mission failed or canceled
function gMissionPrototype:OnFailure () --[[ overwrite me ]] end

-- gets the next (hightest prio, lowest order) running task or nil
function gMissionPrototype:GetNextRunningTask ()
	local lowestorder = nil
	local task = nil
	
	for k,v in pairs(self.mlTask) do
		-- only check running tasks
		if v.mState == kTaskState_Running then
			if lowestorder then
				-- check for lower task
				if k < lowestorder then
					lowestorder = k
					task = v
				end
			else
				lowestorder = k
				task = v
			end 
		end
	end
	
	return task
end

-- gets the position where to go next
-- return : x,y,z or nil on error/no target
function gMissionPrototype:WhereToGoNext () 
	if self.mState == kTaskState_Running then
		local task = self:GetNextRunningTask()
		if task then 
			return task.mWhereToGoNext
		end
	end

	return nil
end
-- gets an array of task descriptions and the finished state
-- id determines the order of the tasks (lower ids first)
-- return : {id1={desc1,state1}, id2={desc2,state2},...}
function gMissionPrototype:GetTasks ()
	local l = {}
	for k,v in pairs(self.mlTask) do l[k] = {v.msDescription,v.mState} end
	return l
end

-- checks the tasks and decide if the mission failed/succeeded
-- this function changes the mState
-- default behavior : all tasks finished -> finished, >1 tasks failed -> failed
function gMissionPrototype:UpdateState () --[[ overwrite me ]]
	local allfinished = true
	
	for k,v in pairs(self.mlTask) do 
		if v.mState == kTaskState_Failed then
			-- complete failure
			self.mState = kTaskState_Failed
			return
		elseif v.mState == kTaskState_Running then
			allfinished = false
		end
	end
	
	if allfinished then
		-- all finished :)
		self.mState = kTaskState_Finished
	end
end

function gMissionPrototype:RemoveTask (id)
	self.mlTask[id] = nil
	self:SendMissionUpdate()
end

function gMissionPrototype:SetTask (id,task)
	self.mlTask[id] = task
	task.mMission = self
	self:SendMissionUpdate()
end

-- returns the number of the next task id
function gMissionPrototype:NextTaskID ()
	if not self.miNextTaskID then self.miNextTaskID = 1 end
	
	self.miNextTaskID = self.miNextTaskID + 1
	return self.miNextTaskID - 1
end

-- adds a task on the top of the task stack and return the task index
function gMissionPrototype:AddTask (task)
	-- get next task id
	local id = self:NextTaskID()
	-- init tasklist if needed
	if not self.mlTask then self.mlTask = {} end
	
	self.mlTask[id] = task
	task.mMission = self
	self:SendMissionUpdate()
	return id,task
end

-- sends the current mission (including tasks) to all the clients
function gMissionPrototype:SendMissionUpdate ()
	for id,player in pairs(gServerPlayers) do 
		-- send mission update to one player
		self:ServerSendMissionUpdate(player.con)
	end
end

-- sends a state update message
function gMissionPrototype:SendUpdateMissionState ()
	-- only send, if this is an accepted mission
	if self.miID then 
		for id,player in pairs(gServerPlayers) do 
			-- send mission update to one player
			SendNetMessage(player.con,kNetMessage_UpdateMissionState,self.miID,self.mState)					
		end		
	end
end

-- sends a state update message
function gMissionPrototype:SendUpdateTaskState (taskid)
	-- only send, if this is an accepted mission
	if self.miID then 
		for id,player in pairs(gServerPlayers) do 
			-- send mission update to one player
			SendNetMessage(player.con,kNetMessage_UpdateTaskState,self.miID,taskid,self.mlTask[taskid].mState)					
		end		
	end
end

-- sends the current mission (including tasks) to one connection/client
function gMissionPrototype:ServerSendMissionUpdate (con)
	-- only send, if this is an accepted mission
	if self.miID then 
		-- ("kNetMessage_UpdateMission","iosi.")
		-- sent by server : mission_id, employer_object, description, state, (task_id, description, state)*
		
		local l = {}
		
		-- add task parameter into one list
		for k,v in pairs(self.mlTask) do 
			table.insert(l,k)	-- id
			table.insert(l,v.mWhereToGoNext and v.mWhereToGoNext.id or 0)	-- id
			table.insert(l,v.msDescription)	-- desc
			table.insert(l,v.mState)	-- state
		end
		
		SendNetMessage(con,kNetMessage_UpdateMission,
			self.miID,self.mWhereToGoNext,self.mEmployer,self.msDescription,self.msName,self.mState,unpack(l))				
	end
end


-- ############################################################


gMessageTypeHandler_Client.kNetMessage_UpdateMissionState = function (mission_id,state)
	-- print("DEBUG","Hook_MissionStateUpdate",mission_id,state)
	NotifyListener("Hook_MissionStateUpdate",mission_id,state)
end

gMessageTypeHandler_Client.kNetMessage_UpdateTaskState = function (mission_id,task_id,state)
	-- print("DEBUG","Hook_TaskStateUpdate",mission_id,task_id,state)
	NotifyListener("Hook_TaskStateUpdate",mission_id,task_id,state)
end


gMessageTypeHandler_Client.kNetMessage_UpdateMission = function (id,wheretogo,employer,description,name,state,...)
	
	local l = {}

	-- group the arg list into 4pairs
	for i = 1,table.getn(arg),4 do
		table.insert(l,{unpackex(arg,i,i+3)})		
	end
	
	local mission = {}
	
	mission.id = id
	mission.wheretogo = GetObject(wheretogo)
	mission.employer = employer
	mission.description = description
	mission.name = name
	mission.state = state
	
	mission.tasks = {}
	for k,v in pairs(l) do
		mission.tasks[v[1]] = {id=v[1],wheretogo=GetObject(v[2]),description=v[3],state=v[4]}
	end
	
	NotifyListener("Hook_MissionUpdate",mission)
	
	-- print("DEBUG",vardump_rec(mission,nil,2))
end


function PlayerJoined_SendMissions(newplayer)
	-- send all missions
	for k,mission in pairs(gMissionsByID) do 
		mission:ServerSendMissionUpdate(newplayer.con)
	end
end


-- kMissionMaxAcceptDistance = 500
-- kMissionMaxFinishDistance = 500


function GetMission			(mission_or_id)
	if (type(mission_or_id) == "table") then return mission_or_id end
	if (type(mission_or_id) == "number") then return gMissionsByID[mission_or_id] end
	return nil
end



-- adds the mission to the currently open missions
-- and send the client the new mission
function AcceptMission (mission) 
	-- assign id,  needed for server/client communication
	local id = gNextMissionID gNextMissionID = gNextMissionID + 1
	mission.miID = id
	gMissionsByID[mission.miID] = mission
	
	-- broadcast spawn
	mission:SendMissionUpdate()
	
	return mission
end


function gObjectPrototype:ListMissions () return self.missions or {} end
function gObjectPrototype:HasMissions () return self.missions and notempty(self.missions) end


RegisterListener("Hook_MainStep",function ()
		for k,mission in pairs(gMissionsByID) do 
			mission:Update()
		end
	end)
	
	
function OpenOwnMissionDialog () NotifyListener("Hook_Client_OpenOwnMissionDialog") end
	
	
-- ############################################################


function CreateTask_Dummy (desc)
	local task = CopyArray(gTaskPrototype)
	
	task.Update = function(self)
		-- print("TASK update")
	end
	task.OnFailure = function(self)
		print("TASK failed")
	end
	task.OnFinish = function(self)
		print("TASK finished")		
	end
	
	task.msDescription = desc
	
	return task
end

function CreateTask_Timeout (desc,timeout)
	local task = CopyArray(gTaskPrototype)
	
	task.Update = function(self)
		if self.mState == kTaskState_Running then
			if gMyTicks >= self.miFinishTime then
				self.mState = kTaskState_Finished
			end
		end
	end
	task.OnFailure = function(self)
		print("TASK failed")
	end
	task.OnFinish = function(self)
		print("TASK finished")		
	end
	
	task.msDescription = desc
	
	task.miFinishTime = gMyTicks + timeout
	
	return task
end


function CreateTask_GotoPosition (desc,x,y,z,dist)
	local task = CopyArray(gTaskPrototype)

	task.msDescription = desc
	task.mWhereToGoNext = SpawnObject("waypoint",gMainLocation,{x,y,z},{1,0,0,0})
	
	task.Update = function(self)
		if self.mState == kTaskState_Running then
			for id,player in pairs(gServerPlayers) do 
				if player.body and player.body:GetDistToPos(x,y,z) < dist then
					self.mState = kTaskState_Finished
				end
			end		
		end
	end
	task.OnFailure = function(self)
		self:Cleanup()
		print("TASK failed")
	end
	task.OnFinish = function(self)
		self:Cleanup()
		print("TASK finished")		
	end
	task.Cleanup = function(self)
		if self.mWhereToGoNext then
			self.mWhereToGoNext:Destroy()
			self.mWhereToGoNext = nil
		end
	end
		
	return task
end

function CreateTask_Kill (desc,group, x,y,z,radius)
	local task = {}
	ArrayOverwrite(task,gTaskPrototype)

	task.msDescription = desc
	task.mGroup = SpawnEnemyGroup(group,x,y,z,radius)
	task.mWhereToGoNext = SpawnObject("waypoint",gMainLocation,{x,y,z},{1,0,0,0})

	task.Update = function(self)
		if ( self.mState == kTaskState_Running and not self.mGroup:IsAlive() ) then
			self.mState = kTaskState_Finished
		end
	end
	task.OnFailure = function(self)
		self:Cleanup()
		print("TASK failed")
	end
	task.OnFinish = function(self)
		self:Cleanup()
		print("TASK finished")		
	end
	task.Cleanup = function(self)
		if self.mGroup then
			self.mGroup:Destroy()
			self.mGroup = nil
		end
		if self.mWhereToGoNext then
			self.mWhereToGoNext:Destroy()
			self.mWhereToGoNext = nil
		end
	end
		
	return task
end

-- ############################################################



-- ############################################################

function MissionTest ()
	if not gbServerRunning then return end

	local m = CopyArray(gMissionPrototype)
	
	m.msName = "dummy mission"
	m.msDescription = "this is just a totally stupid dummy mission"
	m.OnFailure = function(self)
		print("FAILURE",self.miID)
	end
	m.OnFinish = function(self)
		print("FINISH",self.miID)
	end
	
	local id,task
	
	id,task = m:AddTask(CreateTask_GotoPosition("goto 200,200,200",200,200,200,100))
	id,task = m:AddTask(CreateTask_Kill("kill at 500,500,0",{["shiptype/customship/satellite"]=4},500,500,0,50))
	
	AcceptMission(m)
end



