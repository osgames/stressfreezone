-- handles the menu before starting the game

gMainMenuCamAngH =   0 * gfDeg2Rad
gMainMenuCamAngV =   0 * gfDeg2Rad
gMainMenuCamSpeedFactor = 6 -- 0.1
gMainMenuCamMouseSpeedFactor = 11 * gfDeg2Rad * gMainMenuCamSpeedFactor
gMainMenuCamAngHSpeed =  11 * gfDeg2Rad * gMainMenuCamSpeedFactor
gMainMenuCamAngVSpeed = - 3 * gfDeg2Rad * gMainMenuCamSpeedFactor
gMainMenuLocation = nil

function StartSinglePlayer()
	StartServer(kDefaultSystem)
	StartClient_JoinLocal()
	NotifyListener("Hook_MainMenu_StartSinglePlayer")
end

function StartMultiPlayerHost (bAnnounceOnline)
	StartServer(kDefaultSystem, kDefaultPort,bAnnounceOnline)
	StartClient_JoinLocal()
	NotifyListener("Hook_MainMenu_StartMultiPlayerHost")
end

function StartMultiPlayerJoin ()
	-- TODO : dialog for asking host and port
	StartClient_JoinRemote(kDefaultHost,kDefaultPort)
	NotifyListener("Hook_MainMenu_StartMultiPlayerJoin")
end

function StartMultiPlayerJoin_SpecificGame (host,port)
	AddFadeLines(sprintf("trying to join game : %s:%s",host or "",port or ""))
	StartClient_JoinRemote(host,port)
	NotifyListener("Hook_MainMenu_StartMultiPlayerJoin")
end

function StartMainMenu ()
	if (gMainMenuDialog) then return end -- already started
	
	local rows = {
		{ {type="Label",	text="MainMenu:"} }
	}

	--[[
	local fun = function (widget) MainMenuShowLoginDialog(widget.shard,widget.shardname) StopMainMenu() end
	for shardname,shard in pairs(gShardList) do
		--print(shardname,shard)
		table.insert(rows,{ {type="Button",	on_button_click=fun,shard=shard,shardname=shardname,text=GetShardButtonText(shard,shardname)} })
	end
	]]--
	
	table.insert(rows,{ {"SinglePlayer",		function () StopMainMenu() StartSinglePlayer() end } })
	table.insert(rows,{ {"MultiPlayer Host LAN",	function () StopMainMenu() StartMultiPlayerHost(false) end } })
	table.insert(rows,{ {"MultiPlayer Host Online",	function () StopMainMenu() StartMultiPlayerHost(true) end } })
	table.insert(rows,{ {"MultiPlayer Join",	function () StopMainMenu() StartMultiPlayerJoin() end } })
	table.insert(rows,{ {"List online games",	function () MainMenu_ShowGameList() end } })
	table.insert(rows,{ {"ShipEditor",			function () StopMainMenu() StartShipEditor() end } })
	
	NotifyListener("Hook_MainMenu_Buttons",rows)
	
	table.insert(rows,{ {"#gfx-config",function () 
		if (Client_ShowOgreConfig()) then
			DisplayNotice("please restart iris2 for the changes to take effekt")
			Exit() -- Terminate()  -- terminate softly is not enough, no frame can be drawn since ogre::root has to be killed for fullscreen switch
			-- todo : reinit ogre here ? might loose already loaded textures =(
		end	
		end } })
	table.insert(rows,{ {"#exit",function () Terminate() end } })
	
	SetLogoVisible(true)
	gMainMenuDialog = guimaker.MakeTableDlg(rows,10,10,false,true,gGuiDefaultStyleSet,"window")

	gMainMenuLocation = CreateMenuBackgroundLocation()
	gMainMenuLocation:ClientActivateLocationBackground()
	
	
	-- irc
	LobbyChat_Suggestion()
	
	
	-- local obj = SpawnObject("shiptype/customship/hagship",gMainMenuLocation,{0,0,0},{0,0,0,0})
	
	--MakeColourfulBeamCube()
	
	-- startup shortcuts
	if (not gMainMenuWasStartedBefore) then
		gMainMenuWasStartedBefore = true
		if (gCommandLineSwitches["-ss"]) then StopMainMenu() StartSinglePlayer() end 	-- start in Single Player
		if (gCommandLineSwitches["-sh"]) then StopMainMenu() StartMultiPlayerHost(true) end -- start in multiplayer host mode
		if (gCommandLineSwitches["-sj"]) then StopMainMenu() StartMultiPlayerJoin() end -- start in multiplayer join mode
		if (gCommandLineSwitches["-se"]) then StopMainMenu() StartShipEditor() end		-- start in Ship Editor
		if (gCommandLineSwitches["-sm"]) then StopMainMenu() StartMeshEditor(gCommandLineArguments[gCommandLineSwitches["-sm"]+1]) end		-- start in mesh Editor
		if (gCommandLineSwitches["-ge"]) then StopMainMenu() StartShipPartGeomEditor() end		-- start in mesh Editor
		if (gCommandLineSwitches["-ot"]) then StopMainMenu() StartOdeTest() end
		if (gCommandLineSwitches["-lab"]) then StopMainMenu() StartLaboratory() end
	end
	
	NotifyListener("Hook_MainMenu_Start")
end


function MainMenu_CloseGameList () if (gMainMenu_GameList) then gMainMenu_GameList:Destroy() gMainMenu_GameList = nil end end
function MainMenu_ShowGameList ()
	local x,y = 10,210
	if (gMainMenu_GameList) then  
		x = gMainMenu_GameList.rootwidget.gfx:GetDerivedLeft()
		y = gMainMenu_GameList.rootwidget.gfx:GetDerivedTop()
	end
	MainMenu_CloseGameList()
	
	-- http
	if (not gMasterServerLastMOTD) then
		gMasterServerLastMOTD = MasterServer_GetMOTD() or "could not contact masterserver"
		AddFadeLines("MessageOfTheDay : "..gMasterServerLastMOTD)
	end
	
	local rows = {}

	local gamelist = MasterServer_ListGames()
	local gamecount = table.getn(gamelist or {})
	table.insert(rows,{{sprintf("%d online games open",gamecount)},{"Refresh",MainMenu_ShowGameList},})
	for k,game in pairs(gamelist) do 
		local ip,port,name,flags,version,age,lastrefresh = unpack(game)
		local age_h = age and math.floor(age/3600) or 0
		local age_m = age and math.mod(math.floor(age/60),60) or 0
		local age_s = age and math.mod(age,60) or 0
		local agetxt = ((age_h>0)and(age_h.."h ")or"")..age_m.."m "..age_s.."s"
		
		table.insert(rows,{	
			{"Join",function () StopMainMenu() StartMultiPlayerJoin_SpecificGame(ip,port) end},
			{"  ip:"..(ip or "")},
			{"  port:"..(port or "")},
			{"  name:"..(name or "")},
			{"  flags:"..(flags or "")},
			{"  age:"..(agetxt or "")},
			})
	end
	
	gMainMenu_GameList = guimaker.MakeTableDlg(rows,x,y,false,true,gGuiDefaultStyleSet,"window")
end

function StopMainMenu ()
	for k,obj in pairs(gMainMenuLocation.childs) do obj:Destroy() end
	gMainMenuLocation.childs = {}
	gMainMenuLocation:Destroy() 
	gMainMenuLocation = nil
	
	SetLogoVisible(false)
	if (gMainMenuDialog) then gMainMenuDialog:Destroy() gMainMenuDialog = nil end
	MainMenu_CloseGameList()
end

function StepMainMenu ()
	if (not gMainMenuDialog) then return end
	gMainMenuLocation:StepAllObjects(gMyTicks,gSecondsSinceLastFrame)
	
--~ 	print("gSecondsSinceLastFrame",gSecondsSinceLastFrame,gMyTicks)
	-- turn cam based on mouse
	local tw,tx,ty,tz = CamGetMouseTurn(gSecondsSinceLastFrame * gMainMenuCamMouseSpeedFactor * (gKeyPressed[key_mouse2] and 1 or 0.1))
--~ 	local tw,tx,ty,tz = Quaternion.fromAngleAxis(-60*gfDeg2Rad*gSecondsSinceLastFrame,0,1,0)
	TurnCam(tw,tx,ty,tz)
end

