-- object methods related to searching for objects
-- like mouse-picking, ray-picking etc.
-- see also lib.location.lua

function ObjFilter_IsBase (obj) return obj.objtype.bHasSpacePort end

function ClientGetNearestObject (filterfun) return GetNearestObject(gClientBody,filterfun) end

function GetObjectList (filterfun) 
	local res = {}
	for k,loc in pairs(gLocationsByID) do loc:GetObjectList(filterfun,res) end
	return res
end


function GetNearestObject (ownbody,filterfun) 
	if (not ownbody) then return nil end
	local found = nil
	local founddist = false
	for k,loc in pairs(gLocationsByID) do 
		local obj,dist = loc:GetNearestObject(ownbody,filterfun) 
		if (obj) then if ((not found) or dist < founddist) then found = obj founddist = dist end end
	end
	return found
end

-- returns all objects with less or equal distance to object (filtered)
function GetObjectsInRange (object, distance, filterfun)
	if (not object) then return nil end
	return GetObjectList(function(o) 
		return (filterfun(o) and object:GetDistToObject(o) < distance) 
	end)
end

-- return one random object from the selected
function GetRandomObjectInRange (object, distance, filterfun)
	local l = GetObjectsInRange(object, distance, filterfun)
	local k,v = GetRandomTableElement(l)
	return k,v
end

-- calls MapRayPick for all locations, TODO : local coords
function MapRayPick (rx,ry,rz,rvx,rvy,rvz,fun) 
	for k,loc in pairs(gLocationsByID) do loc:MapRayPick(rx,ry,rz,rvx,rvy,rvz,fun) end
end


function gObjectPrototype:GetMousePickRad ()
	if (self.gfx.billboard_rad) then return self.gfx.billboard_rad * 0.75 end
	return self.objtype.gfx_meshrad or self.objtype.hitrad
end

-- returns hitdist (relative to raylen) or nil if not hit
function gObjectPrototype:RayPick (rx,ry,rz,rvx,rvy,rvz)
	--[[
	if (self.gfx.mesh) then
		if (true) then	
			local x1,y1,z1, x2,y2,z2 = self.gfx:GetWorldAABB() -- does not work somehow
			print("worldbounds",x1,y1,z1, x2,y2,z2)
			--x,y,z = x+0.5*(x1+x2),y+0.5*(y1+y2),z+0.5*(z1+z2)
			local x,y,z = 0.5*(x1+x2),0.5*(y1+y2),0.5*(z1+z2)
			local myrad = Vector.len(x2-x1,y2-y1,z2-z1) * 0.5
			-- local sx,sy,sz = gfx:GetScale()
			-- gfx:GetEntityBoundRad() * scale
		else
			local bhit,hitdist,facenum = self.gfx.mesh:RayPick(rx,ry,rz,rvx,rvy,rvz) -- mainly for mousepicking
			if (bhit) then return hitdist end
		end
	]]--
	if (self.gfx.billboard_rad) then
		local x,y,z = unpack(self.mvPos)
		local hitdist = SphereRayPick(x,y,z,self.gfx.billboard_rad * 0.5,rx,ry,rz,rvx,rvy,rvz)
		if (hitdist) then return hitdist end
	else
		local x,y,z = unpack(self.mvPos)
		local myrad = self.objtype.gfx_meshrad or self.objtype.hitrad
		local hitdist = SphereRayPick(x,y,z,myrad,rx,ry,rz,rvx,rvy,rvz)
		if (hitdist) then return hitdist end
	end 
end	


