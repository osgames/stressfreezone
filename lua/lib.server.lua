


kPlayerSpawnRadius = 2000
	
gLastResyncTimeStamp = 0
giNextPlayerID = 1
gbServerRunning = false
gServerListenerTCP = nil
gServerPlayers = {}
gMessageTypeHandler_Server = {}
gServerUDPReceiveSocket = nil
gTCPResyncSendFifo = CreateFIFO() -- only used if udp is disabled

dofile(libpath .. "lib.server.message.lua")

gMasterServerGameID = nil
gMasterServerNextKeepAlive = 0
gMasterServerKeepAliveInterval = 1000*19 -- every 20 seconds

gLastPingID = 0
gLastPingSendTime = 0
kPingInterval = 500 * 4
gLastValidPingReceivedTime = nil
gMasterServerKeepAliveInterval = 1000*19 -- every 20 seconds

gServerResyncSendInterval = 100
kServerResyncSendInterval_Min = 100
kServerResyncSendInterval_Max = 500
kServerResyncSendInterval_AdjustFactor = 1.2

function ServerPingStep ()
	if (gLastPingSendTime < gMyTicks - kPingInterval) then
		-- evaluate info from last ping
		if (gLastValidPingReceivedTime) then
			local lastpingdelay = gLastValidPingReceivedTime - gLastPingSendTime
			print("lastping_roundtripduration",math.floor(lastpingdelay))
			-- todo : if ping > threshold, slow down resync interval
		end
		
		-- send new ping
		gLastPingSendTime = gMyTicks
		gLastPingID = gLastPingID + 1
		gLastValidPingReceivedTime = nil
		ServerBroadcastMessage(kNetMessage_Ping,gLastPingID)
	end
end

gMessageTypeHandler_Client_Remote.kNetMessage_Ping = function (pingid) 
	ClientSendNetMessage(kNetMessage_Ping,pingid) 
end

gMessageTypeHandler_Server.kNetMessage_Ping = function (player,pingid) 
	print("server got ping answer",player.id,pingid,gLastPingID,(pingid == gLastPingID) and (gMyTicks - gLastPingSendTime))
	if (pingid == gLastPingID) then gLastValidPingReceivedTime = gMyTicks end
end

-- port can be false if no socket should be opened for listening
-- if port is nil, don't listen for incoming connections, used for singleplayer
function StartServer (system, port,bAnnounceOnline)
	-- create ode world
	if (CreateOdeWorld) then
		gOdeWorld = CreateOdeWorld(0.01)
		gOdeWorld:SetGravity(0,0,0)
	end	
	
	gbServerRunning = true
	gMainLocation = GetSystem(system):SpawnLocation() --CreateMenuBackgroundLocation()
	-- GenerateRandomMissions()
	
	print("StartServer",port and ("listening on port "..port) or "singleplayer")
	
	local timeout = 10 * 1000
	local starttime = Client_GetTicks()
	if (port) then
		-- try to bind to the socket
		repeat
			gServerListenerTCP = NetListen(port)
			if ( not gServerListenerTCP ) then Client_USleep(1 * 1000) end
		until gServerListenerTCP or Client_GetTicks() - starttime > timeout 
		
		if (gEnableUDP) then gServerUDPReceiveSocket = Create_UDP_ReceiveSocket(giUDPServerRecvPort) end

		if (bAnnounceOnline) then
			local porttxt = "TCP-port:" .. port
			if (gEnableUDP) then porttxt = porttxt .. " and UDP-port:" .. giUDPServerRecvPort end
			PlainMessageBox("Announcing the game in the internet...\n"..
							"please open "..porttxt.." in your firewall and router NAT configuration\n"..
							"so players can join your game")
							
			local gamename = gGameName
			
			-- announce game on masterserver
			gMasterServerGameID = MasterServer_AnnounceGame(port,gamename,"normal")
			
			-- announce game in IRC / lobbychat
			LobbyChat_Send("online game opened : "..gamename)
			
			-- keep the game alive in the online game list
			RegisterListener("Hook_MainStep",function ()
					if (gMasterServerNextKeepAlive < gMyTicks) then
						gMasterServerNextKeepAlive = gMyTicks + gMasterServerKeepAliveInterval
						if (gMasterServerGameID) then MasterServer_KeepAlive(gMasterServerGameID) end
					end
				end)
		end
	end
	
	NotifyListener("Hook_Server_Start",port)
end

function StepServer ()
	if (not gbServerRunning) then return end
	
	-- accept new players
	if (gServerListenerTCP) then
		while true do
			local newcon = gServerListenerTCP:PopAccepted()
			if (not newcon) then break end
			ServerPlayerJoined(newcon)
		end
	end
	
	-- ping
	ServerPingStep()
	
	-- receive udp data
	if (gServerUDPReceiveSocket) then
		while (true) do
			gUDPRecvFifo:Clear()
			local resultcode,remoteaddr = gServerUDPReceiveSocket:Receive(gUDPRecvFifo) 
			--print("server recv udp data",resultcode,remoteaddr,gUDPRecvFifo:Size())
			if (gUDPRecvFifo:Size() == 0) then break end
			local myplayer = GetPlayerFromRemoteAddr(remoteaddr) 
			if (gUDPRecvFifo:Size() >= 0) then 
				print("got udp data from client",myplayer and myplayer.id or -1,gUDPRecvFifo:Size())
			end
			--print("got udp data",remoteaddr,gUDPRecvFifo:Size())
			-- todo : can udp-from ip adresses be faked ? if so, send secret hash to player, that he sends with each udp message
			if (myplayer) then
				if (gUDPRecvFifo:Size() >= CalcNetMessageParamLength("iffff")) then
					local iMouseResyncTimeStamp,qw,qx,qy,qz = FPop(gUDPRecvFifo,"iffff") 
					if (myplayer.iMouseResyncTimeStamp <= iMouseResyncTimeStamp) then
						myplayer.iMouseResyncTimeStamp  = iMouseResyncTimeStamp
						ServerSetPlayerMouseRot(myplayer.id,iMouseResyncTimeStamp,qw,qx,qy,qz)
					end
				else
					print("player sent illegal udp message length",myplayer.id,gUDPRecvFifo:Size())
				end
			end
			--print("server received udp data !",resultcode,remoteaddr,gUDPRecvFifo:Size())
		end
	end
	
	-- step ode
	if (gOdeWorld) then gOdeWorld:Step() end

	-- step clients 
	for k,player in pairs(gServerPlayers) do
		local myplayer = player -- used in closure
		myplayer.con:Pop(myplayer.netRecvFifo)
		
		-- receive messages from clients
		RecvNetMessages(myplayer.netRecvFifo,function (msgtype,...) 
				local msgtypename = gNetMessageTypeName[msgtype]
				if (gMessageTypeHandler_Server[msgtypename]) then 
					gMessageTypeHandler_Server[msgtypename](myplayer,unpack(arg))
				end
				NotifyListener("Hook_Server_RecvNetMsg",myplayer,msgtype,unpack(arg))
			end)
			
		
		-- apply player keys
		local body = myplayer.body
		if (body and body:IsAlive() and (not CanWalk(body))) then 
			local x = 		(TestBit(myplayer.iKeyMask,kCtrlKeyMask_Left ) and -1 or 0) + (TestBit(myplayer.iKeyMask,kCtrlKeyMask_Right	) and 1 or 0)
			local y = 		(TestBit(myplayer.iKeyMask,kCtrlKeyMask_Down ) and -1 or 0) + (TestBit(myplayer.iKeyMask,kCtrlKeyMask_Up	) and 1 or 0)
			local z = 		(TestBit(myplayer.iKeyMask,kCtrlKeyMask_Fwd  ) and -1 or 0) + (TestBit(myplayer.iKeyMask,kCtrlKeyMask_Back	) and 1 or 0)
			local bBreak =	 TestBit(myplayer.iKeyMask,kCtrlKeyMask_Break)
			local bCancelAutoPilot = (x ~= 0) or (y ~= 0) or (z ~= 0) or bBreak
			
			local qw,qx,qy,qz = unpack(myplayer.qMouse)
			x,y,z = Quaternion.ApplyToVector(x,y,z,qw,qx,qy,qz) 
			
			if (bCancelAutoPilot) then AutoPilot_Cancel(body) end
			if (not body.autopilot_active) then 
				if (bBreak) then body:BreakStep() else body:ManualThrustStep(x,y,z) end
			end
			body:AutoOrientationStep()  
			
			-- fire weapons while player holds mousebutton pressed
			if (TestBit(myplayer.iKeyMask,kCtrlKeyMask_FirePrimary)) then  
				FirePrimaryWeapons(player.body,player.target)
			end
		end
	end

	-- step locations :  done near end so gfx has all the latest changes
	for k,loc in pairs(gLocationsByID) do 
		loc:StepAllObjects(gMyTicks,gSecondsSinceLastFrame,true)  -- this updates the lastchangetime for ships using autopilot
	end
	
	-- send resyncs to clients, should be done after objectstep to also resync changes from autopilot
	if (gMyTicks >= gLastResyncTimeStamp + gServerResyncSendInterval) then
		for k,player in pairs(gServerPlayers) do
			local myplayer = player -- used in closure
			if ((not myplayer.bIsLocal) and myplayer.con:IsConnected()) then
				if (not gEnableUDP) then gTCPResyncSendFifo:Clear() end
				
				local packetcounter = 0 -- counts how many resync packets have been sent this frame, might be used to adjust bandwidth
				for k,loc in pairs(gLocationsByID) do 
					local random_resync_prob = 0.005 -- resync for every object every once in a while, TODO : this controls bandwidth usage
					if (gEnableUDP) then
						-- send via udp
						packetcounter = packetcounter + loc:SendResyncs(gUDPSendSocket,myplayer.con:GetRemoteAddress(),giUDPClientRecvPort,gLastResyncTimeStamp,random_resync_prob)
					else
						-- collect in buffer for sending via tcp
						packetcounter = packetcounter + loc:StoreResyncs(gTCPResyncSendFifo,gLastResyncTimeStamp,random_resync_prob)
					end
				end
				if (not gEnableUDP) then 
					 -- send via tcp
					SendNetMessage(myplayer.con,kNetMessage_TCPResyncs,gTCPResyncSendFifo)
				end 
				--print("resyncs-sent : ",packetcounter,myplayer.con:GetRemoteAddress())
			end
		end
		gLastResyncTimeStamp = gMyTicks+1
	end
end


-- con = new connection
function ServerPlayerJoined (con,bIsLocal)
	SendNetVersion(con)
	assert(giNextPlayerID < 64000,"playerlimit reached") -- protect from buffer overflow ?
	local id = giNextPlayerID
	print("ServerPlayerJoined",con,id)
	local newplayer = {con=con,netRecvFifo=CreateFIFO(),id=id,iKeyMask=0,qMouse={1,0,0,0},iMouseResyncTimeStamp=0,bIsLocal=bIsLocal}
	gServerPlayers[id] = newplayer
	giNextPlayerID = giNextPlayerID + 1
	
	SendNetMessage(newplayer.con,kNetMessage_PlayerID,newplayer.id)
	
	-- send world to new player
	-- just send all locations as long as the game is simple
	for k,loc in pairs(gLocationsByID) do 
		loc:ServerSendSpawn(newplayer.con)
		
		-- send objects within location
		for k,object in pairs(loc.childs) do 
			object:ServerSendSpawn(newplayer.con) 
			object:ServerSendProps(newplayer.con) 
		end
	end
	
	-- send other global lists
	PlayerJoined_SendContainers(newplayer)
	PlayerJoined_SendMissions(newplayer)
	
	-- spawn player on connect (todo : choose spawnpoint/team/ship/... first?)
	-- SpawnPlayer(newplayer)
	
	NotifyListener("Hook_Server_PlayerJoined",newplayer)
end

function GetPlayerFromRemoteAddr (remoteaddr) 
	for id,player in pairs(gServerPlayers) do if (player.con:GetRemoteAddress() == remoteaddr) then return player end end
end

function ServerPlayerSetBody (player,newbody)
	local oldbody = player.body
	if (oldbody and oldbody.player == player) then oldbody.player = nil end
	player.body = newbody
	newbody.player = player
	-- notify all players of the new body for this player
	ServerBroadcastMessage(kNetMessage_PlayerBody,player.id,player.body)
	
	NotifyListener("Hook_Server_SetBody",player.id,player.body)
end

function SpawnPlayer(player, iBodyObjectTypeId) 
	local newbody = SpawnObject(iBodyObjectTypeId,gMainLocation,{Vector.random3(kPlayerSpawnRadius)},{Quaternion.identity()})
	ServerPlayerSetBody(player,newbody)
end


-- send a message to all players
function ServerBroadcastMessage (msgtype,...)
	assert(gbServerRunning)
	for k,player in pairs(gServerPlayers) do SendNetMessage(player.con,msgtype,unpack(arg)) end
end

-- use this for system-messages
function ServerSendChatMessage		(player,msg)	SendNetMessage(player.con,kNetMessage_Chat,0,0,msg) end
function ServerBroadcastChatMessage	(msg)			ServerBroadcastMessage(kNetMessage_Chat,0,0,msg) end

function ServerSetPlayerMouseRot (playerid,iMouseResyncTimeStamp,qw,qx,qy,qz)
	local myplayer = gServerPlayers[playerid]
	myplayer.qMouse = {qw,qx,qy,qz}
	-- todo : cache this in case the client is faster than the server ?
	-- todo : limit turn serverside for slow turning ships, or turn indirect body first and drag slow ships rot behind ?
	local body = myplayer.body
	--print("ServerSetPlayerMouseRot",playerid,iMouseResyncTimeStamp,tw,tx,ty,tz,body)
	if (body) then
		qw,qx,qy,qz = Quaternion.normalise(qw,qx,qy,qz)
		body.mqTurn = {1,0,0,0} -- todo : calc delta as per second, to enable other clients smoothing/prediction
		--body.mqRot = {qw,qx,qy,qz}
	end
end
