-- handle control-keys whose state is sent from client to the server

gCtrlKeyNames = {}
gNextCtrlKeyMask = 1

gActionKeyNames = {}
gNextActionKeyId = 1

function RegisterCtrlKey (name)
	local id = gNextCtrlKeyMask
	gNextCtrlKeyMask = gNextCtrlKeyMask*2
	_G[name] = id
	gCtrlKeyNames[id] = name
end

function RegisterActionKey (name)
	local id = gNextActionKeyId
	gNextActionKeyId = gNextActionKeyId + 1
	_G[name] = id
	gActionKeyNames[id] = name
end

RegisterCtrlKey("kCtrlKeyMask_Left")
RegisterCtrlKey("kCtrlKeyMask_Right")
RegisterCtrlKey("kCtrlKeyMask_Up")
RegisterCtrlKey("kCtrlKeyMask_Down")
RegisterCtrlKey("kCtrlKeyMask_Fwd")
RegisterCtrlKey("kCtrlKeyMask_Back")
RegisterCtrlKey("kCtrlKeyMask_Break")
RegisterCtrlKey("kCtrlKeyMask_RollLeft")
RegisterCtrlKey("kCtrlKeyMask_RollRight")
RegisterCtrlKey("kCtrlKeyMask_FirePrimary")

RegisterActionKey("kActionKey_Debug01")
RegisterActionKey("kActionKey_Debug02")

-- weapon slot activation
-- RegisterActionKey("kActionKey_FirePrimary") -- as hold and as action key, to ensure that also a short key hit is recognised
-- RegisterActionKey("kActionKey_FireMissile")
for i = 1,10 do RegisterActionKey("kActionKey_FireSlot" .. i) end

-- for tilebased controls while walking inside ship
RegisterActionKey("kActionKey_TurnLeft")
RegisterActionKey("kActionKey_TurnRight")
RegisterActionKey("kActionKey_WalkFwd")
RegisterActionKey("kActionKey_WalkBack")
-- ...

