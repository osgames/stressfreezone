-- mesh editor
-- for changing scale,rotate,position of newly imported meshes


kMeshEditorHaloWidth 	= 0.01 -- the width of the beamlines
kMeshEditorGridColor 	= {1,1,1,1} -- rgba, white
kMeshEditorHitFaceOffset = 0.1 -- offset a little from normal to avoid rounding errors
kInitialMeshEditorThirdPersonDist = 10
gMeshEditorTableCamSpeedFactor = 0.5 * gfDeg2Rad

gbMeshEditorRunning = false

gMeshEditor_MeshFilePath = kMeshEditStartPath

gMeshEditor_UndoStack = {}
gMeshEditor_ActiveUndoGroup = nil

function MeshEditor_GetMeshName () return gMeshEditor_MeshFilePath end
function MeshEditor_GetMeshFilePath () return gMeshEditor_MeshFilePath end
function MeshEditor_LoadFromFile (filepath)
	gMeshEditor_MeshFilePath = filepath
	MeshEditor_ReloadMesh()
end

function MeshEditor_ReloadMesh ()
	if (gMeshEditModel) then gMeshEditModel:Destroy() gMeshEditModel = nil end
	gMeshEditModel = CreateRootGfx3D()
	gMeshEditModel:SetMesh(MeshEditor_GetMeshName()) 
	gMeshEditModel:SetPosition(0,0,0)
	gMeshEditModel:SetScale(1,1,1)
	gMeshEditModel:SetNormaliseNormals(true)
end

function StartMeshEditor (filepath)
	gbMeshEditorRunning = true
	print("StartMeshEditor")
	
	gMeshEditLocation = CreateMenuBackgroundLocation()
	
	--assert(not gStarField)
	if (gStarField) then gStarField:Destroy() end
	gStarField = CreateRootGfx3D()
	gStarField:SetStarfield(gNumberOfStars,gStarsDist,gStarColorFactor,"starbase")
	local cam = GetMainCam()
	cam:SetNearClipDistance(gDefaultNearClip)
	cam:SetFarClipDistance(gDefaultFarClip) -- ogre defaul : 100000
	local amb = 0.2
	Client_ClearLights()
	Client_SetAmbientLight(amb,amb,amb,1)
	Client_AddDirectionalLight(-0.3,-0.5,-0.1)
	
	gCamThirdPersonDist = kInitialMeshEditorThirdPersonDist
	
	MeshEditor_LoadFromFile(filepath or gMeshEditor_MeshFilePath)
	
	MeshEditor_InitGrid()
	MeshEditor_AddReferenceModel("sd_spaceman.mesh",1,0,0,GetObjectType("shiptype/spacesuit").meshscale)
	MeshEditor_OpenToolsDialog()
	
--~ 	local myKeyDown = function (key,char) if (key == GetNamedKey("c")) then ... end end
--~ 	RegisterListener("keydown",myKeyDown)
	
--~ 	RegisterListener("mouse_left_click",function () MeshEditSingleClick() end)
--~ 	RegisterListener("mouse_right_down",function () MeshEditRightClick() end)
	
	RegisterListener("mouse_right_down",function () MeshEditor_OpenContextMenu() end)
	
	NotifyListener("Hook_MeshEdit_Start")
end

function MeshEditor_InitGrid ()
	local x1,y1,z1, x2,y2,z2 = 0,0,0, 1,1,1
	gMeshEditGrid = CreateRootGfx3D()
	SetBeamBox(gMeshEditGrid,kMeshEditorHaloWidth, x2-x1,y2-y1,z2-z1,unpack(kMeshEditorGridColor)) 
	gMeshEditGrid:SetPosition(x1-0.5,y1-0.5,z1-0.5)
end

function MeshEditor_AddReferenceModel (meshname,x,y,z,scale)
	local refmodel = CreateRootGfx3D()
	refmodel:SetMesh(meshname)
	refmodel:SetPosition(x,y,z)
	local s = scale or 1
	if (s ~= 1) then
		refmodel:SetScale(s,s,s)
		refmodel:SetNormaliseNormals(s ~= 1)
	end
	gMeshEditRefModels = gMeshEditRefModels or {}
	table.insert(gMeshEditRefModels,refmodel)
end

-- return module,facenum,x,y,z,dist  : nil if not hit
--~ function MeshEditMousePick ()
--~ 	local rx,ry,rz,rvx,rvy,rvz = GetMouseRay()
--~ 	local foundmodule,foundfacenum,founddist,x,y,z
--~ 	for k,module in pairs(gMeshEditModules) do
--~ 		local facenum,dist = module:RayPick(rx,ry,rz, rvx,rvy,rvz) 
--~ 		if (dist and ((not founddist) or (dist < founddist))) then
--~ 			founddist = dist
--~ 			foundfacenum = facenum
--~ 			foundmodule = module
--~ 			x,y,z = (rx + dist*rvx),(ry + dist*rvy),(rz + dist*rvz)
--~ 		end
--~ 	end
--~ 	return foundmodule,foundfacenum,x,y,z,founddist
--~ end

function StepMeshEditor ()
	if (not gbMeshEditorRunning) then return end
	if (gMeshEditLocation) then gMeshEditLocation:StepAllObjects(gMyTicks,gSecondsSinceLastFrame) end
	StepTableCam(GetMainCam(),gKeyPressed[key_mouse1] and (not gui.bMouseBlocked),gMeshEditorTableCamSpeedFactor)
	StepThirdPersonCam(GetMainCam(),gCamThirdPersonDist,gCamThirdPersonX,gCamThirdPersonY,gCamThirdPersonZ)
	
	-- todo : hilight sides on mouseover
--~ 	local module,facenum,x,y,z,dist = MeshEditMousePick()
--~ 	if (module) then 
--~ 		-- todo : display new-module-pos as half-transparent ghost (geom+size+ghostmaterial, or mesh + ghostmaterial)
--~ 		local x1,y1,z1, x2,y2,z2 = module:GetCachedGridBounds()
--~ 		local placetxt
--~ 		gEditorDebugMsg = ""
--~ 		if (gActiveShipPartType and false) then
--~ 			local px,py,pz, qw,qx,qy,qz, mx,my,mz = GuessNewModulePosMirRot(gActiveShipPartType,module,facenum,x,y,z)
--~ 			placetxt = px and arrdump({px,py,pz,GetMirRotComboName(qw,qx,qy,qz,mx,my,mz)})
--~ 		end
--~ 		local mousehittxt = sprintf("%4.1f,%4.1f,%4.1f ",round(x*10)/10,round(y*10)/10,round(z*10)/10)
--~ 		SetBottomLine(arrdump({mousehittxt,x1,y1,z1," ",x2,y2,z2," #",gEditorDebugMsg,"# ",placetxt}))
--~ 	else
--~ 		SetBottomLine("")
--~ 	end
	
end


function MeshEditor_Save () 
	local meshname = MeshEditor_GetMeshName()
	local filename = MeshEditor_GetMeshFilePath()
	local x1,y1,z1,x2,y2,z2 = MeshReadOutExactBounds(meshname)
	MeshSetBounds(meshname,x1,y1,z1,x2,y2,z2)
	MeshSetBoundRad(meshname,math.max(Vector.len(x1,y1,z1),Vector.len(x2,y2,z2)))
	ExportMesh(meshname,filename)
	AddFadeLines(sprintf("file saved : %s",filename or ""))
	MeshEditor_ReloadMesh()
end

function MeshEditor_ShowLoadDialog () 
	FileBrowse_Load("Load Mesh",kMeshEditDefaultSavePath,MeshEditor_LoadFromFile)
end

-- dx,dy,dz
function MeshEditor_GetSize () 
	local x1,y1,z1,x2,y2,z2 = MeshReadOutExactBounds(MeshEditor_GetMeshName())
	return Vector.sub(x2,y2,z2,x1,y1,z1)
end

-- mx,my,mz = MeshEditor_GetMid()
function MeshEditor_GetMid () 
	local x1,y1,z1,x2,y2,z2 = MeshReadOutExactBounds(MeshEditor_GetMeshName())
	return 0.5*(x1+x2),0.5*(y1+y2),0.5*(z1+z2)
end

-- x1,y1,z1 = MeshEditor_GetMin()
function MeshEditor_GetMin () local x1,y1,z1,x2,y2,z2 = MeshReadOutExactBounds(MeshEditor_GetMeshName()) return x1,y1,z1 end
-- x2,y2,z2 = MeshEditor_GetMax()
function MeshEditor_GetMax () local x1,y1,z1,x2,y2,z2 = MeshReadOutExactBounds(MeshEditor_GetMeshName()) return x2,y2,z2 end

function MeshEditor_ScaleUniform	(s)		MeshEditor_Scale(s,s,s) end
function MeshEditor_Scale	(sx,sy,sz)		
	TransformMesh(MeshEditor_GetMeshName(), 0,0,0, sx,sy,sz, 1,0,0,0) 
	MeshEditor_UndoStack_Add(0,0,0, 1.0/sx,1.0/sy,1.0/sz, 1,0,0,0)
end
function MeshEditor_Move	(x,y,z)			
	TransformMesh(MeshEditor_GetMeshName(), x,y,z, 1,1,1, 1,0,0,0) 
	MeshEditor_UndoStack_Add(-x,-y,-z, 1,1,1, 1,0,0,0)
end
function MeshEditor_Rot		(qw,qx,qy,qz)	
	TransformMesh(MeshEditor_GetMeshName(), 0,0,0, 1,1,1, qw,qx,qy,qz) 
	MeshEditor_UndoStack_Add(0,0,0, 1,1,1, Quaternion.inverse(qw,qx,qy,qz))
end

function MeshEditor_Resize	(maxsize)	MeshEditor_ScaleUniform(maxsize / math.max(MeshEditor_GetSize())) end
function MeshEditor_Center	()			local mx,my,mz = MeshEditor_GetMid() MeshEditor_Move(-mx,-my,-mz) end

function MeshEditor_UndoStack_Add ( x,y,z, sx,sy,sz, qw,qx,qy,qz )
	if (gMeshEditor_ActiveUndoGroup) then
		table.insert(gMeshEditor_ActiveUndoGroup, {x,y,z, sx,sy,sz, qw,qx,qy,qz})
	else
		table.insert(gMeshEditor_UndoStack, { {x,y,z, sx,sy,sz, qw,qx,qy,qz} })
	end
end

function MeshEditor_UndoGroup_Start ()
	assert(not gMeshEditor_ActiveUndoGroup)
	gMeshEditor_ActiveUndoGroup = {}
end

function MeshEditor_UndoGroup_End ()
	assert(gMeshEditor_ActiveUndoGroup)
	table.insert(gMeshEditor_UndoStack,gMeshEditor_ActiveUndoGroup)
	gMeshEditor_ActiveUndoGroup = nil
end

function MeshEditor_Undo ()
	local n = table.getn(gMeshEditor_UndoStack)
	if (n == 0) then AddFadeLines("no more undo steps possible") return end
	local lastgroup = table.remove(gMeshEditor_UndoStack,n) -- pop back from stack
	
	-- invert the transformations in reverse order
	local n = table.getn(lastgroup)
	for k=n,1,-1 do
		TransformMesh(MeshEditor_GetMeshName(), unpack(lastgroup[k])) 
	end
end


function MeshEditor_OpenContextMenu ()
	local menudata = {}
	local x1,y1,z1,x2,y2,z2 = MeshReadOutExactBounds(MeshEditor_GetMeshName())
	local mx,my,mz = MeshEditor_GetMid()
		
	table.insert(menudata,{ "MeshEditor" })
	ShowContextMenu(menudata,nil,nil,gGuiDefaultStyleSet)
end

function MeshEditor_OpenToolsDialog ()
	if (gMeshEditor_ToolsDialog) then return end -- already started
	
	local rows = {
		{ {type="Label",	text="Tools:"} },
		--{ {type="Button",	on_button_click=ShipEditShowLoadDialog,text="#Load"} },
		--{ {type="Button",	on_button_click=ShipEditShowSaveDialog,text="#Save"} },
	}
	
	table.insert(rows,{ {"Load",			function () MeshEditor_ShowLoadDialog() end }} )
	table.insert(rows,{ {"Save",			function () MeshEditor_Save() end }} )
	table.insert(rows,{ {"Undo",			function () MeshEditor_Undo() end }} )
	
	table.insert(rows,{ {"Unify",			function () MeshEditor_UndoGroup_Start() MeshEditor_Resize(1.0) MeshEditor_Center() MeshEditor_UndoGroup_End() end }} )
	
	table.insert(rows,{ {"Align Center", 	function () MeshEditor_Center()		end }} )
			
	table.insert(rows,{	{"Center:"},
						{"X", 		function () local mx,my,mz = MeshEditor_GetMid() MeshEditor_Move(-mx,0,0)		end },
						{"Y", 		function () local mx,my,mz = MeshEditor_GetMid() MeshEditor_Move(0,-my,0)		end },
						{"Z", 		function () local mx,my,mz = MeshEditor_GetMid() MeshEditor_Move(0,0,-mz)		end },	})
			
	table.insert(rows,{	{"Min:"},
						{"X",		function () local x1,y1,z1 = MeshEditor_GetMin() MeshEditor_Move(-0.5-x1,0,0)	end },
						{"Y",		function () local x1,y1,z1 = MeshEditor_GetMin() MeshEditor_Move(0,-0.5-y1,0)	end },
						{"Z",		function () local x1,y1,z1 = MeshEditor_GetMin() MeshEditor_Move(0,0,-0.5-z1)	end },	})
					
	table.insert(rows,{	{"Max:"},
						{"X",		function () local x2,y2,z2 = MeshEditor_GetMax() MeshEditor_Move(0.5-x2,0,0)	end },	
						{"Y",		function () local x2,y2,z2 = MeshEditor_GetMax() MeshEditor_Move(0,0.5-y2,0)	end },	
						{"Z",		function () local x2,y2,z2 = MeshEditor_GetMax() MeshEditor_Move(0,0,0.5-z2)	end },	})
						
	table.insert(rows,{	{"Rotate +90"},
						{"X",		function () MeshEditor_Rot(QuaternionFromString("x:90"))	end },	
						{"Y",		function () MeshEditor_Rot(QuaternionFromString("y:90"))	end },	
						{"Z",		function () MeshEditor_Rot(QuaternionFromString("z:90"))	end },	})
	table.insert(rows,{	{"Rotate -90"},
						{"X",		function () MeshEditor_Rot(QuaternionFromString("x:-90"))	end },	
						{"Y",		function () MeshEditor_Rot(QuaternionFromString("y:-90"))	end },	
						{"Z",		function () MeshEditor_Rot(QuaternionFromString("z:-90"))	end },	})
						
	
	
	
	
	local rows2 = {}
	table.insert(rows2,{{"Scale:"},	{"0.5", 	function () MeshEditor_ScaleUniform(0.5)	end },
									{"2.0", 	function () MeshEditor_ScaleUniform(2.0)	end },
									{"0.95", 	function () MeshEditor_ScaleUniform(0.95)	end },
									{"1.05", 	function () MeshEditor_ScaleUniform(1.05)	end }, })
						
	table.insert(rows2,{{"MoveX:"},	{"-0.01", 	function () MeshEditor_Move(-0.01,0,0)	end },
									{"+0.01", 	function () MeshEditor_Move( 0.01,0,0)	end },
									{"-0.1", 	function () MeshEditor_Move(-0.1,0,0)	end },
									{"+0.1", 	function () MeshEditor_Move( 0.1,0,0)	end },
									{"-1.0", 	function () MeshEditor_Move(-1.0,0,0)	end },
									{"+1.0", 	function () MeshEditor_Move( 1.0,0,0)	end }, })
									
	table.insert(rows2,{{"MoveY:"},	{"-0.01", 	function () MeshEditor_Move(0,-0.01,0)	end },
									{"+0.01", 	function () MeshEditor_Move(0, 0.01,0)	end },
									{"-0.1", 	function () MeshEditor_Move(0,-0.1,0)	end },
									{"+0.1", 	function () MeshEditor_Move(0, 0.1,0)	end },
									{"-1.0", 	function () MeshEditor_Move(0,-1.0,0)	end },
									{"+1.0", 	function () MeshEditor_Move(0, 1.0,0)	end }, })
									
	table.insert(rows2,{{"MoveZ:"},	{"-0.01", 	function () MeshEditor_Move(0,0,-0.01)	end },
									{"+0.01", 	function () MeshEditor_Move(0,0, 0.01)	end },
									{"-0.1", 	function () MeshEditor_Move(0,0,-0.1)	end },
									{"+0.1", 	function () MeshEditor_Move(0,0, 0.1)	end },
									{"-1.0", 	function () MeshEditor_Move(0,0,-1.0)	end },
									{"+1.0", 	function () MeshEditor_Move(0,0, 1.0)	end }, })
						
	gMeshEditor_ToolsDialog = guimaker.MakeTableDlg(rows,-150,10,false,true)
	gMeshEditor_ToolsDialog_Scale = guimaker.MakeTableDlg(rows2,10,10,false,true)
end
