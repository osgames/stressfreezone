-- trading

function ClientOpenTradeDialog (obj_trader)
	print("OpenTradeDialog",obj_trader)
	NotifyListener("Hook_Client_OpenTradeDialog",obj_trader)
end

function ClientRequestTrade	(obj_trader,offerid,amount)
	ClientSendNetMessage(kNetMessage_RequestTrade,obj_trader,offerid,amount)
end


gMessageTypeHandler_Server.kNetMessage_RequestTrade = function (player,obj_trader,offerid,amount)
	if (not obj_trader) then return end
	local tradelist = obj_trader.objtype.tradelist
	if (not tradelist) then return end
	local offerarr = tradelist[offerid]
	if (not offerarr) then return end
	local body = player.body
	if (not body) then return end
	
	-- todo : also limit buy/sell by free space
	local oldamount = amount
	local max_buy	= body:CargoDivList(offerarr.cost)
	local max_sell	= body:CargoDivList(offerarr.offer)
	amount = math.max(-max_sell,math.min(max_buy,amount))
	--print("server.RequestTrade",oldamount,max_buy,max_sell,amount)
	
	if (amount == 0) then return end
	body:CargoAddList(offerarr.offer,amount) -- amount is negative for sell
	body:CargoAddList(offerarr.cost,-amount)
end

