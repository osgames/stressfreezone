-- receives and handles messages on the server

gMessageTypeHandler_Server.kNetMessage_AutoPilotApproach = function (player, target, fDist) 
	AutoPilot_Approach(player.body,target,fDist) 
end

gMessageTypeHandler_Server.kNetMessage_AutoPilotCancel = function (player) 
	AutoPilot_Cancel(player.body) 
end

-- player request a body and wants to spawn with it somewhere
gMessageTypeHandler_Server.kNetMessage_RequestSpawn = function (player, iBodyId)
	-- player has no body or dead body?
	if (player.body and player.body:IsAlive()) then return end
	SpawnPlayer(player, iBodyId)
end

-- player marks an object as target
gMessageTypeHandler_Server.kNetMessage_SetTarget = function (player, targetobj)
	player.target = targetobj
end

gMessageTypeHandler_Server.kNetMessage_ProtocolHeader = function (player,sNetVersion) player.sNetVersion = sNetVersion end

gMessageTypeHandler_Server.kNetMessage_PlayerCtrlKeys = function (player,iKeyMask) 
	player.iKeyMask = iKeyMask
end

gMessageTypeHandler_Server.kNetMessage_SetRelMove = function (player,vx,vy,vz) 
	--SpawnEffect("effect/hit",nil,{x,y,z},{1,0,0,0})
	--if (not player.body) then return end
	--player.body.mvVel = {vx,vy,vz}
	-- TODO : temporarily deactivated, need more intuitive controls, and non-newtonian movement
end

gMessageTypeHandler_Server.kNetMessage_PlayerActionKey = function (player,iActionKeyID) 
	local t = {}
	
	t[kActionKey_TurnLeft]	= function() PlayerWalk_Turn(player,  90) end
	t[kActionKey_TurnRight]	= function() PlayerWalk_Turn(player, -90) end
	t[kActionKey_WalkFwd]	= function() PlayerWalk_Step(player,0,  1) end
	t[kActionKey_WalkBack]	= function() PlayerWalk_Step(player,0, -1) end
	
	for i = 1,10 do
		local slotnumber = i
		t[_G["kActionKey_FireSlot" .. slotnumber]] = function() 
			if (player.body) then 
				FireWeaponSlot(player.body,player.target,slotnumber) 
			end 
		end
	end
	
	t[kActionKey_Debug01] = function()
		if (player.body) then 
			for typename,amount in pairs(gCheatObjTypes) do player.body:AddCargo(typename,amount) end
			if (gCheatObjTypes_AllEquipment) then
				for k,objtype in pairs(GetObjectTypeList(function (t) return t.bIsEquipment end)) do
					player.body:AddCargo(objtype,1)
				end
			end
			
			--SpawnEffect("effect/die",player.body,{0,0,-40},{1,0,0,0})
			--SpawnEffect("effect/travel",player.body,{0,0,0},{1,0,0,0})
		end
	end
	
	t[kActionKey_Debug02] = function()
		if (player.body and player.body:IsAlive()) then 
			for i=1,20 do 
				SpawnObject("asteroid/asteroid2",gMainLocation,{player.body:GetRandomPositionAtDist(100)},{Quaternion.random()}) 
			end
		end
	end

	if t[iActionKeyID] then t[iActionKeyID]() end

	NotifyListener("Hook_Server_PlayerActionKey",player,iActionKeyID)
end

-- only used when udp is disabled
gMessageTypeHandler_Server.kNetMessage_PlayerMouse = function (player,iMouseResyncTimeStamp,qw,qx,qy,qz)  
	ServerSetPlayerMouseRot(player.id,iMouseResyncTimeStamp,qw,qx,qy,qz)
end

gMessageTypeHandler_Server.kNetMessage_Chat = function (player,iParam,iFlags,sChatText)  
	-- distribute to all players, or to target player/group
	ServerBroadcastMessage(kNetMessage_Chat,player.id,0,sChatText)
end

gMessageTypeHandler_Server.kNetMessage_CallTarget = function(player,obj_target)
	ServerBroadcastMessage(kNetMessage_NotifyCallTarget,player.id,player.body,obj_target)
end

