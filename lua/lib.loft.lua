

function LoftTest ()
	if (not gCommandLineSwitches["-lofttest"]) then return end
	local gfx = CreateRootGfx3D()
	local base = Loft_Base_Init()
--~ 	Loft_Base_AddVertex(base,-2,-1,0,	0.0)
--~ 	Loft_Base_AddVertex(base, 2,-1,0,	0.5)
--~ 	Loft_Base_AddVertex(base, 0, 1,0,	1.0)
	
	for a=0,360-1,120 do 
		Loft_Base_AddVertex(base,math.sin(a*gfDeg2Rad),math.cos(a*gfDeg2Rad),0,	a/360)
	end
	
	local path = Loft_Path_Init()
	local v = 0
	local vadd = 0.1
	Loft_Path_AddVertex(path,  0, 0, 0, 0,v, 0, 1,0,0,1) v=v+vadd
	Loft_Path_AddVertex(path,  0, 0,10, 0,v, 1, 0,0,1,1) v=v+vadd
	Loft_Path_AddVertex(path,  0,10,20, 0,v, 1, 0,0,1,1) v=v+vadd
	Loft_Path_AddVertex(path,  0,20,20, 0,v, 0, 0,0,1,1) v=v+vadd
	path = Loft_SmoothPath(path,3)
	MakeLoft(gfx,base,path) 
	gfx:SetMaterial(GetPlainColourMat(0,1,0))
end


function Loft_Base_Init			() 				return {} end
function Loft_Base_AddVertex	(base,x,y,z,u)	table.insert(base,{pos={x,y,z},u=u}) end
function Loft_Base_MakeLoop		(base)			table.insert(base,base[1]) end -- called automatically

function Loft_Path_Init			() return {} end
function Loft_Path_AddVertex	(path,x,y,z, ubias,v, scale, r,g,b,a)
	-- default parameter
	ubias = ubias or 1
	v = v or 0
	scale = scale or 1
	r = r or 1
	g = g or 1
	b = b or 1
	a = a or 1

	local p = {pos={x,y,z},ubias=ubias,v=v,scale=scale,col={r,g,b,a}}
	--print("loftpath add ",x,y,z, ubias,v, scale, r,g,b,a)
	table.insert(path,p) 
	return p
end	
	
	
-- will be called automatically by MakeLoft
function Loft_Path_AutoRot	(path) 
	assert(table.getn(path) >= 2,"too few points, call after completely filled")
	local pathlen = table.getn(path)
	local qw,qx,qy,qz = Quaternion.identity()
	for ip,vertex_path in ipairs(path) do
		local x,y,z = unpack(path[ip].pos)
		local xa,ya,za, xb,yb,zb
		if (ip > 1)			then xa,ya,za = unpack(path[ip-1].pos) else xa,ya,za = x,y,z end
		if (ip < pathlen)	then xb,yb,zb = unpack(path[ip+1].pos) else xb,yb,zb = x,y,z end
		local dxa,dya,dza = Vector.sub(x,y,z,xa,ya,za) 
		local dxb,dyb,dzb = Vector.sub(xb,yb,zb,x,y,z)
		local pw,px,py,pz = Quaternion.getRotation(dxa,dya,dza,dxb,dyb,dzb) 
		if (ip > 1) then qw,qx,qy,qz = unpack(path[ip-1].rot) end
		if (ip > 1 and ip < pathlen) then qw,qx,qy,qz = Quaternion.Mul(pw,px,py,pz, qw,qx,qy,qz) end
		vertex_path.rot = {Quaternion.normalise(qw,qx,qy,qz)}
	end
end

-- base = { vertex, vertex, ...}   
	-- vertex 
		-- pos={x,y,z} : z is usually zero
		-- u=0 : first texcoord
-- path = { vertex, vertex, ...}  
	-- vertex
		-- pos={x,y,z} pos
		-- ubias=0,v=0.2   ubias is added to u from base,  v is second texcoord
		-- scale=1  : single scale factor for x,y,z
		-- scale={x,y,z}  : x,y,z scale factors
		-- col={r,g,b,a}  : vertex-color
		-- rot={qw,qx,qy,qz}  : rotation... auto generated ?
-- base should be in x,y plane, is extruded along z-axis for path
function MakeLoft (gfx,base,path,filter) 
	Loft_Base_MakeLoop(base)
	local count_base = table.getn(base) assert(count_base >= 2)
	local count_path = table.getn(path) assert(count_path >= 2)
	local vc = count_base * count_path
	local ic = (count_path-1)*((count_base-1)*6)
	
	-- calculate rotation at path points if not manually specified
	if (not path[1].rot) then Loft_Path_AutoRot(path) end
	
	-- get middle of base
	local bx,by,bz = 0,0,0
	for ib,vertex_base in ipairs(base) do
		local x,y,z = unpack(vertex_base.pos)
		bx = bx + x / count_base
		by = by + y / count_base
		bz = bz + z / count_base
	end
	
	-- generate base normal
	for ib,vertex_base in ipairs(base) do
		local x,y,z,u = unpack(vertex_base.pos)
		vertex_base.normal = {Vector.normalise(x-bx,y-by,z-bz)}
	end
	
	gfx:SetSimpleRenderable()
	gfx:RenderableBegin(vc,ic,false,false,OT_TRIANGLE_LIST)
	
	-- generate loft vertices
	for ip,vertex_path in ipairs(path) do
		local r,g,b,a
		if (vertex_path.col) then r,g,b,a = unpack(vertex_path.col) end
		local qw,qx,qy,qz = unpack(vertex_path.rot) assert(vertex_path.rot,"MakeLoft:path must have rot, use Loft_Path_AutoRot")
		local sx,sy,sz = 1,1,1
		if (vertex_path.scale and type(vertex_path.scale) == "table") then sx,sy,sz = unpack(vertex_path.scale) end
		if (vertex_path.scale and type(vertex_path.scale) == "number") then sx = vertex_path.scale  sy=sx  sz=sx end
		
		for ib,vertex_base in ipairs(base) do
			local x,y,z = Vector.scale(sx,sy,sz, unpack(vertex_base.pos))
			local u,v = vertex_base.u+vertex_path.ubias,vertex_path.v
			local nx,ny,nz = unpack(vertex_base.normal)
			x,y,z		= Quaternion.ApplyToVector(x,y,z,		qw,qx,qy,qz)
			nx,ny,nz	= Quaternion.ApplyToVector(nx,ny,nz,	qw,qx,qy,qz)
			x,y,z		= Vector.add(x,y,z,unpack(vertex_path.pos))
			
			if (filter) then x,y,z,nx,ny,nz,u,v,r,g,b,a = filter(ip,ib, x,y,z,nx,ny,nz,u,v,r,g,b,a) end
			
			if (r) then
				gfx:RenderableVertex(x,y,z,nx,ny,nz,u,v,r,g,b,a)
			else
				gfx:RenderableVertex(x,y,z,nx,ny,nz,u,v)
			end
		end
	end
	
	-- generate loft indices : local ic = (count_path-1)*((count_base-1)*6)
	local iv = 0
	local d = count_base
	local maxindex = c
	for p = 1,count_path-1 do
		for b = 1,count_base-1 do
			local y0,y1 = b-1,b+1 - 1
			local x0,x1 = iv,iv+d
			gfx:RenderableIndex3(x0+y0,x1+y0,x0+y1)
			gfx:RenderableIndex3(x0+y1,x1+y0,x1+y1)
		end
		iv = iv + d
	end
	
	gfx:RenderableEnd()
end

-- the order is  q1  p1  p2  q2   , interpolation between p1 and p2
function InterpolateSmooth4 (t,q1,p1,p2,q2) 
	if (t < 0) then return p1 end
	if (t > 1) then return p2 end
	--~ 	//Q(t) = P1*(2t^3-3t^2+1) + R1*(t^3-2t^2+t) + P2*(-2t^3+3t^2) + R2*(t^3-t^2)
	--~ 	//Q(0) = P1*(1) + R1*(0) + P2*(0) + R2*(0)
	--~ 	//Q(1) = P1*(0) + R1*(0) + P2*(1) + R2*(0)
	--~ 	// hermit spline or something like that
	--~ 	// R1,R2 are tangent-vectors at P1,P2

	local t2 = t * t
	local t3 = t2 * t
	local t3m2 = t3 - t2
	local r1 = (p2 - q1) * 0.5
	local r2 = (q2 - p1) * 0.5
	return p1*(t3m2+t3m2-t2+1.0) + r1*(t3m2-t2+t) + p2*(-t3m2-t3m2+t2) + r2*(t3m2) 
end


-- returns smoothed path
function Loft_SmoothPath (path,steps)
	local newpath = Loft_Path_Init()
	local pathlen = table.getn(path)
	
	-- unpack scale
	for ip,v1 in ipairs(path) do if (type(v1.scale) == "number") then v1.scale = {v1.scale,v1.scale,v1.scale} end end
	
	-- insert interpolation since last vertex
	for ip,vertex in ipairs(path) do
		if (ip > 1) then 
			local v1 = path[math.max(1,ip-2)]
			local v2 = path[ip-1]
			local v3 = path[ip+0]
			local v4 = path[math.min(pathlen,ip+1)]
			for i=1,steps do 
				local t = i/(steps+1)
				local x		= InterpolateSmooth4(t,v1.pos[1],v2.pos[1],v3.pos[1],v4.pos[1])
				local y		= InterpolateSmooth4(t,v1.pos[2],v2.pos[2],v3.pos[2],v4.pos[2])
				local z		= InterpolateSmooth4(t,v1.pos[3],v2.pos[3],v3.pos[3],v4.pos[3])
				
				local ubias	= InterpolateSmooth4(t,v1.ubias,v2.ubias,v3.ubias,v4.ubias)
				local v		= InterpolateSmooth4(t,v1.v,v2.v,v3.v,v4.v)
				
				local sx	= InterpolateSmooth4(t,v1.scale[1],v2.scale[1],v3.scale[1],v4.scale[1])
				local sy	= InterpolateSmooth4(t,v1.scale[2],v2.scale[2],v3.scale[2],v4.scale[2])
				local sz	= InterpolateSmooth4(t,v1.scale[3],v2.scale[3],v3.scale[3],v4.scale[3])
				
				local r		= InterpolateSmooth4(t,v1.col[1],v2.col[1],v3.col[1],v4.col[1])
				local g		= InterpolateSmooth4(t,v1.col[2],v2.col[2],v3.col[2],v4.col[2])
				local b		= InterpolateSmooth4(t,v1.col[3],v2.col[3],v3.col[3],v4.col[3])
				local a		= InterpolateSmooth4(t,v1.col[4],v2.col[4],v3.col[4],v4.col[4])
				
				local np = Loft_Path_AddVertex(newpath, x,y,z, ubias,v, {sx,sy,sz}, r,g,b,a)
				if (v1.rot) then
					local qw,qx,qy,qz = unpack(p.rot)
					local pw,px,py,pz = unpack(q.rot)
					np.rot = QuaternionSlerp(qw,qx,qy,qz, pw,px,py,pz, t)
				end
			end
		end 
		table.insert(newpath,vertex)
	end
	return newpath
end


