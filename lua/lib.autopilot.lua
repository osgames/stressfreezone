kAutoPilot_StopEpsilon = 1 -- full stop cheat if velocity is below this
kAutoPilotDefaultApproachDist = 200

function AutoPilot_Init (ship)
	assert(gbServerRunning,"autopilot is a serverside feature!")
	if (ship.controller) then return end -- already initialised
	ship.controller = CreateObjectController() -- todo : memleak, release me
	ship.controller.mfMaxAccel = ship.objtype.accel
	local myship = ship -- closure
	ship.autopilot_stepper = function () return AutoPilot_Step(myship) end
	RegisterListener("Hook_MainStep",ship.autopilot_stepper)
end

-- tries to keep specified distance to target, can be used to follow the target
function AutoPilot_Approach (ship,target,dist)
	AutoPilot_Follow(ship,target,dist,true)
end

-- tries to keep specified distance to target, can be used to follow the target
function AutoPilot_Follow(ship,target,dist,stop_on_reached)
	--print("AutoPilot_Approach",ship)
	if ((not ship) or (not ship:IsAlive())) then return end
	AutoPilot_Init(ship)
	ship.controller:SetApproachObject(target)
	ship.controller.mfApproachMinDist = dist or kAutoPilotDefaultApproachDist
	ship:SetController(ship.controller)
	ship.autopilot_active = true
	ship.autopilot_stop_if_dist_reached = stop_if_reached
end

function AutoPilot_GoTo (ship,dist,x,y,z)
	--print("AutoPilot_Approach",ship)
	if ((not ship) or (not ship:IsAlive())) then return end
	AutoPilot_Init(ship)
	ship.controller:SetApproachPosition(x,y,z)
	ship.controller.mfApproachMinDist = dist or kAutoPilotDefaultApproachDist
	ship:SetController(ship.controller)
	ship.autopilot_active = true
	ship.autopilot_stop_if_dist_reached = true
end

-- active autopilot for stopping the ships movement after reaching the target
function AutoPilot_Stop (ship)
	--print("AutoPilot_Stop",ship)
	if ((not ship) or (not ship:IsAlive())) then return end
	AutoPilot_Init(ship)
	ship:SetController(nil)
	ship.autopilot_active = true
end

function AutoPilot_Cancel (ship)
	--print("AutoPilot_Cancel",ship)
	if ((not ship) or (not ship:IsAlive())) then return end
	if (not ship.autopilot_active) then return end
	ship.autopilot_active = false
	ship:SetController(nil)
end

function AutoPilot_Step (ship)
	if ((not ship) or (not ship:IsAlive())) then return true end
	if (not ship.autopilot_active) then return end -- currently inactive
	if (ship.controller) then
		-- approach
		if (ship.controller:IsTargetAlive()) then
			-- target still alive
			if (ship.autopilot_stop_if_dist_reached and 
				ship.controller:DistanceToTarget(unpack(ship.mvPos)) < ship.controller.mfApproachMinDist) then
				AutoPilot_Stop(ship)
			end
		else
			-- target destroyed => stop
			AutoPilot_Stop(ship)
		end
	else	
		-- stop pilot
		if (ship:GetCurSpeed() < kAutoPilot_StopEpsilon) then
			print("stop pilot finished")
			ship.mvVel = {0,0,0}
			ship.mvAccel = {0,0,0}
			AutoPilot_Cancel(ship)
		else
			ship:BreakStep()
		end
	end
end
