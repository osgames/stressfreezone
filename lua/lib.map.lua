-- TODO : obsoloete, clean me up and reuse =)
gMap = {}
gActiveLocation = false

-- server only
function ServerLoadMap (filepath) 
	print("Loading Map",filepath)
	-- dofile(objpath .. "list.object.lua")
	gDefaultLocation = nil
	dofile(filepath)
	MapMain() -- from file
end

-- temporary location debugging
--Bind("f1",		function (state) if (state > 0) then ClientChangeToLocation(gDefaultLocation) end end)
--Bind("f2",		function (state) if (state > 0) then ClientChangeToIndexedLocation(2) end end)

-- server only
function ServerSpawnMap (map)
	for ignored,arr in pairs(map) do
		local loc = ServerCreateLocation(arr.name)
		local iLocationID = loc.miID
		for k,v in pairs(arr) do loc[k] = v end
		
		-- asteroid count
		if (arr.asteroid_count) then for i = 1,arr.asteroid_count do
			local a = ServerSpawn(iLocationID,cObjAsteroid,math.random(0,3),Vector.random3(arr.asteroid_spread))
			a.mvVel = { Vector.random3(arr.asteroid_speed) }
		end end
		
		-- default location
		if (arr.isdefault == 1) then gDefaultLocation = loc end
		
		-- trader
		if (arr.station_trader) then
			local x,y,z = 0,0,0
			--local spacestation = ServerSpawn(iLocationID,cObjShip,"spacestation/spacestation.mesh",x,y,z,80,arr.station_trader,0,20000)
			local spacestation = ServerSpawn(iLocationID,cObjShip,"TraderBillboard",x,y,z,80,arr.station_trader,0,20000)
			spacestation.mass = 100000000 -- space stations are heavy
			local buyzone = ServerSpawn(iLocationID,cObjBuyZone,x,y,z)
		end
		
		-- pirates
		if (arr.pirates_count) then for i = 1,arr.pirates_count do
			server.SpawnPirate(loc)
		end end
	end
	-- def
	if (gDefaultLocation == nil) then
		gDefaultLocation = gLocations[1]
	end
end

function ClientChangeToIndexedLocation (i) 
	ClientChangeToLocation(gLocations[i])
end

function ClientChangeToLocation (loc) 
	if (loc and client.playerbody and client.playerbody:IsAlive()) then
		-- todo : server / client division, send messages over network
		if (loc ~= gActiveLocation) then
			if (gActiveLocation) then gActiveLocation.rootgfx:SetParent() end
			gActiveLocation = loc
			gActiveLocation.rootgfx:SetRootAsParent()
			if (client.playerbody) then
				local oldloc = gLocations[client.playerbody:GetLocationID()]
				local newloc = gActiveLocation
				oldloc.objects[client.playerbody.miID] = nil -- do this everytime SetLocationID is used
				newloc.objects[client.playerbody.miID] = client.playerbody
				client.playerbody:SetLocationID(gActiveLocation.miID)
				client.playerbody.miLifeCounter = client.playerbody.miLifeCounter + 1
				client.playerbody.mvVel = { 0,0,0 } -- todo : view and speed towards system center
				client.playerbody.mvPos = { Vector.random3(1*2000) }
				local x,y,z
				x,y,z = unpack(client.playerbody.mvPos)
				--print("playerpos = ",x,y,z)
				client.playerbody.mqRot = { Quaternion.lookAt(-x,-y,-z) }
				--client.playerbody.mqRot = { Quaternion.random() }
				-- TODO : NEED TO RESET CAM, to force cam onto ship-rotation
				Client_ForceCamRot(unpack(client.playerbody.mqRot))
			end
			
			Client_SetSkybox(loc.skybox)
			Client_SetBackCol(loc.fog_r,loc.fog_g,loc.fog_b,loc.fog_a)
			Client_SetFog(loc.fog_mode,loc.fog_r,loc.fog_g,loc.fog_b,loc.fog_a,loc.fog_exp,loc.fog_linearStart,loc.fog_linearEnd)
		end
	else
		print("ClientChangeToLocation failed, invalid loc")
	end
end
