--###############################
--###        PATHS            ###
--###############################

function GetWritableDir ()
	if (UseHomeWritable()) then return os.getenv("HOME").."/.sfz" end
	return gMainWorkingDir.."writable"
end

gMainWorkingDir 			= (GetMainWorkingDir and GetMainWorkingDir() or "./")
lugreluapath 				= file_exists(gMainWorkingDir.."mylugre/lua/lugre.lua") and (gMainWorkingDir.."mylugre/lua/") or (GetLugreDir().."/lua/")
gDataPath 					= GetDataDir().."/" -- gMainWorkingDir.."data/"
libpath 					= GetLuaDir().."/" -- gMainWorkingDir.."lua/"
gWritablePath 				= GetWritableDir().."/"  -- gMainWorkingDir.."writable/"
kVersionFilePath 			= gDataPath.."VERSION"
kVersionWriteEnablerPath 	= gMainWorkingDir.."VERSION_WRITE_ENABLE" -- only for devs, if this file exists the version file is written
kMusicDir 					= gDataPath.."music/"
kSoundDir 					= gDataPath.."sound/"
gConfigPathFallback			= gDataPath..		"config.lua.dist"
gConfigPath 				= gWritablePath..	"config.lua"
kSystemDir 					= gWritablePath.."system/"
gShipDirPath				= gWritablePath.."myships/"
gMainPluginDir 				= gMainWorkingDir.."plugins/"
gPluginDir 					= gWritablePath.."plugins/"
kInteriorDefaultSavePath 	= gWritablePath.."myinterior/"
kObjTypePreviewDir			= gWritablePath.."previewcache/"
kComboDataPath				= gWritablePath.."combos.txt"
kComboObjImagePath			= gWritablePath.."obj_image/"
kComboObjMeshPath			= gWritablePath.."obj_mesh/"
kShipEditDefaultSavePath 	= gShipDirPath
kMeshEditDefaultSavePath 	= gDataPath.."model/"

print("gMainWorkingDir",gMainWorkingDir)
print("gWritablePath",gWritablePath)
print("gDataPath",gDataPath)


gScreenShotPrefix 			= gMainWorkingDir.."screenshots/"
gShipEdit_StartFile 		= gShipDirPath.."ghoulship.lua"
kMeshEditStartPath			= gDataPath.."model/ke_gpl_walllamp.mesh"  
kInteriorDefaultFileName 	= "myinterior.lua"
kShipEditDefaultFileName 	= "myship.lua"


--###############################
--###        CONSTANTS        ###
--###############################

gSecondsSinceLastFrame = 0
gCurrentVersion = nil
gCurrentVersionWrite = false

-- gStarColorFactor = 0  -- really gray stars
-- gStarColorFactor = 1   -- really colorful stars
gStarColorFactor = 0.5 -- somewhat colorful stars
gNumberOfStars = 10000 
gStarsDist = 9000 
gDefaultNearClip = 0.2 
gDefaultFarClip = 22000  -- ogre default : 100000 
gMyFrameCounter = 0

gUDPSendSocket = nil

--###############################
--###     OTHER LUA FILES     ###
--###############################

-- utils first
print("MainWorkingDir",gMainWorkingDir)
print("lugreluapath",lugreluapath)
dofile(lugreluapath .. "lugre.lua")
lugre_include_libs(lugreluapath)
dofile(libpath .. "lib.keybinds.lua")
dofile(libpath .. "lib.client.lua")
dofile(libpath .. "lib.server.lua") -- needed early so others can register themselves in gMessageTypeHandler_Server
dofile(libpath .. "lib.mainmenu.lua")
dofile(libpath .. "lib.netmessagelist.lua")
dofile(libpath .. "lib.target.lua")
dofile(libpath .. "lib.effect.lua")
dofile(libpath .. "lib.service.lua")
dofile(libpath .. "lib.object.lua")
dofile(libpath .. "lib.location.lua")
dofile(libpath .. "lib.meshedit.lua")
dofile(libpath .. "lib.shipedit.lua")
dofile(libpath .. "lib.shipeditmodule.lua")
dofile(libpath .. "lib.shipeditguess.lua")
dofile(libpath .. "lib.keys.lua")
dofile(libpath .. "lib.autopilot.lua")
dofile(libpath .. "lib.shipgfx.lua")
dofile(libpath .. "lib.shipvoxelgrid.lua")
dofile(libpath .. "lib.walk.lua")
dofile(libpath .. "lib.system.lua")
dofile(libpath .. "lib.mission.lua")
dofile(libpath .. "lib.plugin.lua")
dofile(libpath .. "lib.trade.lua")
dofile(libpath .. "lib.loft.lua")
dofile(libpath .. "lib.music.lua")
dofile(libpath .. "lib.randomname.lua")
dofile(libpath .. "lib.masterserver.lua")
dofile(libpath .. "lib.lobbychat.lua")
dofile(libpath .. "lib.enemygroup.lua")
dofile(libpath .. "lib.planet.heightfield.lua")
dofile(libpath .. "lib.combo.lua")
dofile(libpath .. "lib.flower.lua")

dofile(libpath .. "testarea.lua")
--dofile(libpath .. "lib.loading.lua")
LugreActivateGlobalVarChecking()

--###############################
--###        CONFIG           ###
--###############################

dofile(gConfigPathFallback)
if (file_exists(gConfigPath)) then
	-- execute local config
	dofile(gConfigPath)
else
	-- no local config file, create empty
	local fp = io.open(gConfigPath,"w")
	assert(fp,"can't write "..gConfigPath)
	fp:write("-- this is your local config file, here you can override the options from "..gConfigPathFallback.."\n")
	fp:close()
end

--###############################
--###     GUI STYLES          ###
--###############################
gGuiDefaultStyleSet = "ray"
glGuiMakerStyleSet["sfz"] = {
window = {
		material="ui_window",size=64,x=0,y=0,
		cx1=11,cy1=11,cx2=10,cy2=10,cx3=11,cy3=11,
		dx=0,dy=32,border=11,clipborder=11
	},
button = {
		material="ui_window",size=64,x=32,y=0,
		cx1=5,cy1=5,cx2=22,cy2=22,cx3=5,cy3=5,
		dx=0,dy=32,border=5,clipborder=5
	},
border = {
		material="ui_window",size=64,x=32,y=0,
		cx1=5,cy1=5,cx2=22,cy2=22,cx3=5,cy3=5,
		dx=0,dy=32,border=5,clipborder=5
	},
default = {
		material="ui_window",size=64,x=32,y=0,
		cx1=5,cy1=5,cx2=22,cy2=22,cx3=5,cy3=5,
		dx=0,dy=32,border=5,clipborder=5
	},
}
glGuiMakerStyleSet["ray"] = {
default = {
		material="ui_ray_border",size=32,x=0,y=0,
		cx1=12,cy1=12,cx2=8,cy2=8,cx3=12,cy3=12,
		dx=0,dy=0,border=12,clipborder=2
	},
window = {
		material="ui_ray_window",size=128,x=0,y=0,
		cx1=18,cy1=28,cx2=104,cy2=86,cx3=6,cy3=14,
		dx=0,dy=0,
		border_l=18,border_r=6,border_t=28,border_b=14,
		clipborder_l=7,clipborder_r=7,clipborder_t=29,clipborder_b=15
	},
button = {
		material="ui_ray_button",size=128,x=0,y=0,
		cx1=8,cy1=8,cx2=112,cy2=9,cx3=8,cy3=8,
		dx=0,dy=32,border=8,
		clipborder_l=8,clipborder_r=5,
		clipborder_t=3,clipborder_b=3
	},
}

--###############################
--##  OGRE RESOURCE LOCATIONS  ##
--###############################

function CollectOgreResLocs	()
	OgreAddResLoc(gDataPath.."OgreCore.zip"			,"Zip","Bootstrap")
	OgreAddResLoc(gDataPath.."."					,"FileSystem","General")
	OgreAddResLoc(gDataPath.."billboard"			,"FileSystem","General")
	OgreAddResLoc(gDataPath.."base"					,"FileSystem","General")
	OgreAddResLoc(gDataPath.."icon"					,"FileSystem","General")
	OgreAddResLoc(gDataPath.."texture"				,"FileSystem","General")
	OgreAddResLoc(gDataPath.."model"				,"FileSystem","General")
	OgreAddResLoc(gDataPath.."particles/materials"	,"FileSystem","General")
	OgreAddResLoc(gDataPath.."particles/particles"	,"FileSystem","General")
	OgreAddResLoc(gDataPath.."particles/textures"	,"FileSystem","General")
	OgreAddResLoc(gDataPath.."planetmap"			,"FileSystem","General")
	OgreAddResLoc(gDataPath.."planetmap/freeorion"	,"FileSystem","General")
	OgreAddResLoc(gDataPath.."skybox"				,"FileSystem","General")
	OgreAddResLoc(gDataPath.."skybox/planet"		,"FileSystem","General")
	OgreAddResLoc(gDataPath.."skybox/space"			,"FileSystem","General")
	OgreAddResLoc(gDataPath.."skybox/space2"		,"FileSystem","General")
	
	OgreAddResLoc(kObjTypePreviewDir.."."			,"FileSystem","General")
	OgreInitResLocs()
end

--###############################
--###        FUNCTIONS        ###
--###############################

--- called from c right before Main() for every commandline argument
gCommandLineArguments = {}
gCommandLineSwitches = {}
function CommandLineArgument (i,s) gCommandLineArguments[i] = s gCommandLineSwitches[s] = i end

function HandleCommandLine	() 
	--if (gCommandLineArguments[1] == "-g") then
		--DoSomething(gCommandLineArguments[2])
	--end
end

function CheckVersion ()
	gCurrentVersionWrite = file_exists(kVersionWriteEnablerPath)
	if (gCurrentVersionWrite) then
		gCurrentVersion = os.time()
		local fp = io.open(kVersionFilePath,"w")
		fp:write(gCurrentVersion.."\n")
		fp:close()
		print("version written : ",gCurrentVersion)
	end	
	local fp = io.open(kVersionFilePath,"r")
	assert(fp,"couldn't read "..kVersionFilePath)
	gCurrentVersion = TrimNewLines(fp:read())
	fp:close()
	
	print("version-timestamp",gCurrentVersion)
	--Crash()
end

--- main function, when it returns, the program ends
function Main ()
	if (gCommandLineSwitches["-combotest"]) then ComboTest() end
	
	local luaversion = string.sub(_VERSION, 5, 7)
	print("Lua version : "..luaversion)
	if (gEnableUDP) then gUDPSendSocket = Create_UDP_SendSocket() end
	
	gUserName = gUserName or os.getenv("USERNAME") or os.getenv("USER")
	if ((not gUserName) or gUserName == "root") then gUserName = "anonymous" end
	gGameName = gGameName or gUserName.."'s game"
	print("username",gUserName,"game name",gGameName)

	if (gCommandLineSwitches["-masterservertest"]) then MasterServer_Test() end
	
	CheckVersion()
	--[[
	local s = CreateSoundSystem("fmod",44100)
	
	s:SetDistanceFactor(0.25)
	
	s:SetListenerPosition(100,-50,0)
	s:SetListenerVelocity(0,0,0)
	
	local a = s:CreateSoundSource3D(100,-50,0,"horse.ogg")
	local b = s:CreateSoundSource3D(15,0,0,"jaguar.ogg")
	local c = s:CreateSoundSource3D(15,0,0,"seal.ogg")
	
	a:SetMinMaxDistance(5,100)
	b:SetMinMaxDistance(5,100)
	c:SetMinMaxDistance(5,100)
	
	a:Play()
	b:Play()
	c:Play()
	
	local running = true
	
	while running do
		s:Step()
		
		running = false
		if a:IsPlaying() then running = true end
		if b:IsPlaying() then running = true end
		if c:IsPlaying() then running = true end
	end
	
	Crash()
	]]--
	
	HandleCommandLine()
	
	gMyTicks = Client_GetTicks()
	
	LoadPlugins_SFZ()
	NotifyListener("Hook_PluginsLoaded")
	LoadSystems()
	NotifyListener("Hook_SystemsLoaded")
	
	--LoadingProfile("initializing Ogre",true)
	--gPreOgreTime = gLoadingProfileLastTime
	print("lugre_detect_ogre_plugin_path()",lugre_detect_ogre_plugin_path())
	if (not InitOgre("SFZ",gOgrePluginPath or lugre_detect_ogre_plugin_path(),gWritablePath)) then Exit() end
	CollectOgreResLocs()
	
	
	if (gCommandLineSwitches["-tritest"]) then 
		-- custom geometry for testing combatjuans problem
		local gfx = CreateRootGfx3D()
		gfx:SetSimpleRenderable()
		local iVertexCount = 3
		local iIndexCount = 3
		gfx:RenderableBegin(iVertexCount,iIndexCount,false,false,OT_TRIANGLE_LIST)
		--gfx:RenderableVertex(x,y,z)
		gfx:RenderableVertex(0,0,0)
		gfx:RenderableVertex(1000,0,0)
		gfx:RenderableVertex(0,1000,0)
		gfx:RenderableIndex3(0,1,2)
		gfx:RenderableEnd()
	end


	SetCursor(GetPlainTextureMat("cursor.png",true),16,16)

	-- set global shadow settings
	if (gEnableShadows) then
		--OgreSetShadowTexturePixelFormat(PF_FLOAT32_R) --??
		--OgreSetShadowTextureSize(512)
		--OgreSetShadowTextureCasterMaterial
		--OgreSetShadowTextureReceiverMaterial
		--OgreSetShadowTextureSelfShadow(false)
		OgreShadowTechnique(gShadowTechnique)
		--OgreSetShadowFarDistance(100.0)
	end
	
	-- TODO : show loading screen
	-- SetLoadingBackground()
	Client_RenderOneFrame() -- first frame rendered with ogre, needed for init of viewport size
	ChatLine_Init()
	
	-- set global shadow settings
	--OgreShadowTechnique(gShadowTechnique)
	
	SoundInit("any",22050*2)
	
	-- SoundPlayMusic(gDataPath.."sound/test.ogg")
	
	NotifyListener("Hook_PreLoad")
	
	
	
	if (gCommandLineSwitches["-export"]) then
		local i = gCommandLineSwitches["-export"]
		local src = gCommandLineArguments[i+1]
		local dst = gCommandLineArguments[i+2]
		print("export ship as mesh",src,dst)
		local meshname = GetShipOutsideMesh_Cached(src)
		ExportMesh(meshname,dst)
		print("done")
		os.exit(0)
	end
	
	
	
	LoftTest()
	StartMainMenu()
	StartMusic()
	
	-- parse body request parameter
	-- gClient_PreferredBodyTypeName = gClient_PreferredBodyTypeName or "shiptype/customship/ghoulship"
	if gCommandLineSwitches["-body"] then
		local x = gCommandLineArguments[gCommandLineSwitches["-body"] + 1]
		if ( x and GetObjectType(x) ) then
			gClient_PreferredBodyTypeName = x
		end
	end
	print("preferred body name",gClient_PreferredBodyTypeName)
	
	
	-- mainloop
	while (Client_IsAlive()) do MainStep() end
	
	SoundDone()
end

-- called every frame, after all timer-steppers, see Step() in lib.time.lua
function MainStep ()	
	if (gMainWindowSizeDirty) then UpdateMainWindowSize() end
	
	LugreStep()
	
	NotifyListener("Hook_MainStep") -- should called before physstep, so object position changes affect the gfx correctly
	
	--NetStep()
	
	StepMainMenu()
	StepShipEditor()
	StepMeshEditor()
	StepLaboratory()
	
	NetReadAndWrite() -- recv
	StepServer() -- physstep
	NetReadAndWrite() -- recv and send
	StepClient() -- physstep
	NetReadAndWrite() -- send
	
	NotifyListener("Hook_HUDStep") -- updates special hud elements dependant on object positions that don't have auto-tracking
	
	InputStep() -- generate mouse_left_drag_* and mouse_left_click_single events 
	GUIStep() -- generate mouse_enter, mouse_leave events (might adjust cursor -> before CursorStep)
	ToolTipStep() -- needs mouse_enter, should be after GUIStep
	
	CursorStep() -- update cursor gfx pos, should be directly before frame is drawn
	if (not gCommandLineSwitches["-nodraw"]) then Client_RenderOneFrame() end
	
	--SoundStep()
	Client_USleep(1) -- just 1 millisecond, but gives other processes a chance to do something
	if (gCommandLineSwitches["-maxfps"]) then 
		gMaxFPS_Sleep = gMaxFPS_Sleep or 1000 / tonumber(gCommandLineArguments[gCommandLineSwitches["-maxfps"]+1])
		Client_USleep(gMaxFPS_Sleep) 
	end
	--Client_USleep(1000/20) -- max 20 fps
end




--[[
dofile(libpath .. "lib.obj.lua")
dofile(libpath .. "lib.hud.lua")
dofile(libpath .. "lib.map.lua")
dofile(libpath .. "lib.travel.lua")
dofile(libpath .. "lib.buymenu.lua")
-- called from c BEFORE join of any local players
function OldStartGame ()
	if (gIsServer > 0) then
		Server_SetMaxResyncsPerSecond(0) -- 0 = unlimited
		ServerLoadMap(mappath .. "testmap.lua")
		server.StartGame()
	end
	if (gIsClient > 0) then
		Client_SetMaxFPS(0) -- 0 = unlimited , 30 for local network testing with 2 apps running on the same maschine
		Client_SetMouseSensitivity(3.0)
		Client_SetInvertMouse(0) -- 0 = ghouly mode, 1 = hagish-mode ;)
		
		hud.StartGame()
		gui.StartGame()
		ClientChangeToLocation(gDefaultLocation)
	end
end
]]--


function SendChat (text)
	ClientSendChat(text)
	LobbyChat_Send(text)
end

-- called from c, WARNING ! also called during window creation, just use this to set a flag for later !
function NotifyMainWindowResized (w,h) gMainWindowSizeDirty = true end
function UpdateMainWindowSize ()
	gMainWindowSizeDirty = false
	-- set aspect ratio
	-- TODO : right&center alignment hud elements
	local vp = GetMainViewport()
	GetMainCam():SetAspectRatio(vp:GetActualWidth() / vp:GetActualHeight())
end

function MouseEnterHUDElement () end -- obsolete : HUDElement2D.cpp
function MouseLeaveHUDElement () end -- obsolete : HUDElement2D.cpp

