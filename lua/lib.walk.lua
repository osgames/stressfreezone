-- see also lib.object.hangar.lua

function gObjectPrototype:GetInteriorStuffAtPos (x,y,z)
	if (not self.interior_loc) then return end
	for k,obj in pairs(self.interior_loc.childs) do 
		-- TODO : implement me
		assert(false,"NOT YET IMPLEMENTED")
	end
end

function gObjectPrototype:IsOnPlanetGround () 
	if (not self:IsOnPlanet()) then return end
	return true
end

function CanWalk (body) 
	if (not body) then return false end
	if (body:IsInsideShip())		then return true end
	if (body:IsOnPlanetGround())	then return true end
end	

function PlayerWalk_Turn (player,degrees)
	local body = player.body
	if (not CanWalk(body)) then return end
	local qw,qx,qy,qz = unpack(body.mqRot)
	local aw,ax,ay,az = Quaternion.fromAngleAxis( degrees*gfDeg2Rad,0,1,0)
	body.mqRot = {Quaternion.Mul(aw,ax,ay,az,qw,qx,qy,qz)}
	body:MarkChanged()
end

-- 0,1 = 1 step forward
function PlayerWalk_Step	(player,dx,dy)
	local body = player.body
	if (not CanWalk(body)) then return end
	
	local qw,qx,qy,qz = unpack(body.mqRot)
	local fx,fy,fz = Quaternion.ApplyToVector(dx,0,-dy,qw,qx,qy,qz) 
	local x,y,z = unpack(body.mvPos)
	x,y,z = x+fx,y+fy,z+fz
	
	local ship = body:InsideShip_GetShip()
	if (ship) then 
		local shipVoxelGrid = ship:GetVoxelGrid() 
		if (ShipVoxelGrid_IsRamp(shipVoxelGrid,x,y,z)) then y = y + 1 end
		if (not ShipVoxelGrid_IsFreeInteriorPos(shipVoxelGrid,x,y,z)) then return end
		x,y,z = ShipVoxelGrid_GetFallenPos(shipVoxelGrid,x,y,z) -- fall down
	elseif (body:IsOnPlanetGround()) then 
		local planetloc = body:GetRootLocation()
		local planetobj = planetloc.obj_planet
		-- z = heighfield get pos (x,y)
		-- gPlanetGround -- TODO : this is client-side, use something that also works on both client and server
		
		local groundy = gPlanetGround:GetHeightAtPos(x,z) or y
		-- y = max(y - 1,groundy)
		y = groundy + 0.5
	end
	
	body.mvPos = {x,y,z}
	body:MarkChanged()
end	
