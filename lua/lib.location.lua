-- handles location stuff
-- see also lib.object.search.lua
 
 
gLocationTypes = CreateTypeList()

function RegisterLocationType 	(...) return gLocationTypes:RegisterType(unpack(arg)) end

gLocationsByID = {}
gNextLocationID = 1

gLocationPrototype = {}

function GetLocation			(location_or_id) 
	if (type(location_or_id) == "number") then return GetLocationByID(location_or_id) end
	return location_or_id
end

function GetLocationByID		(id) return gLocationsByID[id] end

-- called by both client and server, id param can be left out if on server, will be auto-incremented
-- TODO : vPos,qRot,parent ?
function SpawnLocation (loctype_or_name_or_id,id)
	local loc = CreateLocation() -- c++ function
	loc._Destroy = loc.Destroy
	ArrayOverwrite(loc,gLocationPrototype)
	
	-- determine type
	loc.loctype = gLocationTypes:Get(loctype_or_name_or_id)
	if (not loc.loctype) then print("warning, locationtype not found:",loctype_or_name_or_id) end
	
	-- id
	if (not id) then id = gNextLocationID gNextLocationID = gNextLocationID + 1 end
	loc.id = id
	gLocationsByID[loc.id] = loc
	
	-- create scenenode
	loc.gfx = CreateGfx3D()
	print("spawnloc creategfx = ",loc.gfx)
	
	-- init empty childlist
	loc.childs = {}
	
	-- broadcast spawn
	if (gbServerRunning) then for id,player in pairs(gServerPlayers) do loc:ServerSendSpawn(player.con) end end
	
	NotifyListener("Hook_Location_Spawn",loc)
	return loc
end

--- sends kNetMessage_SpawnLoc : ii : (loctype_id,new_id)
function gLocationPrototype:ServerSendSpawn (con) 
	SendNetMessage(con,kNetMessage_SpawnLoc,self.loctype.type_id,self.id)			
end

gMessageTypeHandler_Client_Remote.kNetMessage_SpawnLoc = function (loctype_id,new_id)
	SpawnLocation(loctype_id,new_id)
end

function gLocationPrototype:Destroy ()
	if (gbServerRunning) then ServerBroadcastMessage(kNetMessage_DestroyLoc,self.id) end
	NotifyListener("Hook_Location_Destroy",self)
	gLocationsByID[self.id] = nil -- delete from arr
	self:_Destroy()
	-- do not automatically destroy mainobject, might be kept, for example fleet-leader
end

gMessageTypeHandler_Client_Remote.kNetMessage_DestroyLoc = function (id)
	local loc = GetLocation(id)
	if (loc) then loc:Destroy() end
end

gMessageTypeHandler_Client_Remote.kNetMessage_ObjectChangeLoc = function (obj_id,new_loc_id,x,y,z,qw,qx,qy,qz)
	local obj = GetObject(obj_id)
	local loc = GetLocation(new_loc_id)
	if (not obj) then return end
	obj.mvPos = {x,y,z}
	obj.mqRot = {qw,qx,qy,qz}
	obj:SetParentLocation(loc) -- this triggers notify messages
end






gActiveLocation = nil
function gLocationPrototype:ClientActivateLocationBackground()
	-- remove old activeloc from scenegraph
	if (gActiveLocation) then
		-- TODO 
		gActiveLocation.gfx:SetParent()
	end
	-- add new activeloc to scenegraph
	gActiveLocation = self
	gActiveLocation.gfx:SetRootAsParent()

	-- set nice stuff in the background
	local cam = GetMainCam()
	--cam:SetFOVy(gfDeg2Rad*45)
	cam:SetNearClipDistance(gDefaultNearClip)
	cam:SetFarClipDistance(gDefaultFarClip)
	--cam:SetProjectionType(0) -- perspective
	Client_SetSkybox(self.loctype.skybox)
	local r,g,b,a 
	if (self.loctype.skycolor) then r,g,b,a = unpack(self.loctype.skycolor) end
	GetMainViewport():SetBackCol(r or 0,g or 0,b or 0,a or 0)
	-- TODO : later : Client_SetFog , ...
	
	-- setup basic lighting
	Client_ClearLights()
	Client_SetAmbientLight(0.4, 0.4, 0.4, 1)
	if (self.loctype.bDirectionalLight) then
		-- Client_AddDirectionalLight(0.9,0.7,-1)
		Client_AddDirectionalLight(-0.3,-0.5,-0.1) 
		-- Client_AddDirectionalLight(-0.7,-0.1,-0.1) 
	else
		Client_AddPointLight(0,0,0)
	end
	
	-- starfield
	if (gStarField) then gStarField:Destroy() gStarField = nil end
	if (self.loctype.bStarField) then
		gStarField = CreateRootGfx3D()
		gStarField:SetForcePosCam(GetMainCam())
		gStarField:SetStarfield(gNumberOfStars,gStarsDist,gStarColorFactor,"starbase")
	end
	
	-- planet ground
	RegeneratePlanetGround()
end

function RegeneratePlanetGround ()
	if (gPlanetGround) then gPlanetGround:Destroy() gPlanetGround = nil end
	if (gActiveLocation.loctype.bIsPlanet) then gPlanetGround = MakePlanetGround(gActiveLocation) end
end

-- adds all objects accepted by the filterfun to list
function gLocationPrototype:GetObjectList(filterfun,list)
	for k,obj in pairs(self.childs) do if (filterfun(obj)) then table.insert(list,obj) end end
end

function gLocationPrototype:GetNearestObject(ownbody,filterfun)
	local found = nil
	local founddist = nil
	for k,obj in pairs(self.childs) do 
		if (filterfun(obj)) then
			local dist = obj:GetDistToObject(ownbody) 
			if ((not found) or dist < founddist) then found = obj founddist = dist end
		end 
	end
	return found,founddist
end

-- calls fun(obj,hitdist) for all childs that were hit by the ray, dist is relative to raylen
-- TODO : location-local coordinate system ? or just change ray
function gLocationPrototype:MapRayPick (rx,ry,rz,rvx,rvy,rvz,fun)
	local res = {}
	for k,obj in pairs(self.childs) do 
		local dist = obj:RayPick(rx,ry,rz,rvx,rvy,rvz) 
		if (dist) then res[obj] = dist end
	end
	-- don't execute during iteration, so fun can savely create new objects, e.g. spawn debris on destruction
	for obj,dist in pairs(res) do fun(obj,dist) end
end


-- for k,obj in pairs(loc:GetChilds()) do ...
function gLocationPrototype:GetChilds() return self.childs end

-- creates and returns a location with all sorts of random junk inside
function CreateMenuBackgroundLocation ()
	local adist = 2000
	local bdist = 4000
	local stuff = {
--~ 		{ objtype="asteroid/asteroid1",	count=10,	posrad=adist,turnspeed=180.0*gfDeg2Rad },
--~ 		{ objtype="asteroid/asteroid2",	count=10,	posrad=adist,turnspeed=180.0*gfDeg2Rad },
--~ 		{ objtype="asteroid/asteroid3",	count=10,	posrad=adist,turnspeed=180.0*gfDeg2Rad },
--~ 		{ objtype="asteroid/asteroid4",	count=10,	posrad=adist,turnspeed=180.0*gfDeg2Rad },
--~ 		{ objtype="planet/xotus2",		count=2,	posrad=bdist,turnspeed=30.0*gfDeg2Rad },
--~ 		{ objtype="planet/xotus3",		count=2,	posrad=bdist,turnspeed=30.0*gfDeg2Rad },
--~ 		{ objtype="planet/xotus4",		count=2,	posrad=bdist,turnspeed=30.0*gfDeg2Rad },
--~ 		{ objtype="planet/xotus5",		count=2,	posrad=bdist,turnspeed=30.0*gfDeg2Rad },
--~ 		{ objtype="station/station1",	count=2,	posrad=bdist,turnspeed=30.0*gfDeg2Rad }, --3d, big
--~ 		{ objtype="station/station2",	count=2,	posrad=bdist,turnspeed=30.0*gfDeg2Rad },
--~ 		{ objtype="station/station3",	count=2,	posrad=bdist,turnspeed=30.0*gfDeg2Rad },
--~ 		{ objtype="station/station4",	count=2,	posrad=bdist,turnspeed=30.0*gfDeg2Rad },
--~ 		{ objtype="station/station5",	count=2,	posrad=bdist,turnspeed=30.0*gfDeg2Rad },
--~ 		{ objtype="sat/sat1",			count=2,	posrad=bdist,turnspeed=30.0*gfDeg2Rad },
--~ 		{ objtype="sat/sat2",			count=11,	posrad=bdist,turnspeed=30.0*gfDeg2Rad }, -- todo : orbit around planet ?
--~ 		{ objtype="sun",				count=1,	posrad=bdist,turnspeed=30.0*gfDeg2Rad }, -- todo : planets orbiting around me ?
--~ 		{ objtype="wormhole",			count=1,	posrad=bdist,turnspeed=30.0*gfDeg2Rad },
--~ 		{ objtype="box",				count=1,	posrad=bdist,turnspeed=30.0*gfDeg2Rad }, -- 3d, small
	}
	
	-- todo : just for testing, don't do that here
	local loc = SpawnLocation("locationtype/default")
	for k,stuff in pairs(stuff) do
		for i = 1,stuff.count do
			local obj = SpawnObject(stuff.objtype,loc,{Vector.random3(stuff.posrad)},{Quaternion.random()})
			obj.mqTurn = {Quaternion.random((2*math.random()-1)*stuff.turnspeed) } -- random turn speed, max speed = 180 degrees/sec = 1.0*pi
		end
	end
	
	--[[
	for i = 1,100 do
		local eff = SpawnEffect("effect/particle",nil,{Vector.random3(10)},{Quaternion.random()})
	end
	]]--
	
	return loc
end
