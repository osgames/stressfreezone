-- handles intelligent guessing positions/rotation/mirror of newly placed ship-parts




-- x,y,z = clicked pos
-- returns x,y,z, qw,qx,qy,qz, mx,my,mz
-- mx,my,mz are 1 or -1, to be applied using newmodule:Mirror(mx,my,mz) afterwards
function GuessNewModulePosMirRot (parttype,oldmodule,oldfacenum,x,y,z)
	local nx,ny,nz = FaceGetNormal(oldmodule.gfx,oldfacenum)
	nx,ny,nz = Quaternion.ApplyToVector(nx,ny,nz,oldmodule.gfx:GetDerivedOrientation())  -- correct normal rotation
	if (not NormalIsAxisAligned(nx,ny,nz)) then return end -- attaching to non-axis-aligned sides is not allowed
	nx,ny,nz = round(nx),round(ny),round(nz)
	gShipEditGuessNormalCache = {} -- clear cache
	
	-- check if part with the same geomety was picked and use simple mirroring in that case
	local tn = gShipPartTypes:Get(parttype)
	local to = oldmodule.shipparttype
	if (tn.gfx_geom == to.gfx_geom and tn.cx == to.cx and tn.cy == to.cy and tn.cz == to.cz) then
		-- cx,cy,cz compare because  ramp1 + ramp2, etc  will look bad in some situation
		
		-- mirror bbox of oldmodule using normal
		local mx,my,mz 				= AxisAlignedNormalToMirror(nx,ny,nz)
		local x1,y1,z1, x2,y2,z2 	= oldmodule:GetCachedGridBounds()
		x1,y1,z1 					= MirrorPoint(x1,y1,z1,mx,my,mz,x,y,z)
		x2,y2,z2 					= MirrorPoint(x2,y2,z2,mx,my,mz,x,y,z)
		x1,y1,z1, x2,y2,z2 			= CorrectGridBounds(x1,y1,z1, x2,y2,z2)
		
		-- calculate bbox of new module for position 0, to calc real position
		local qw,qx,qy,qz 			= oldmodule.gfx:GetDerivedOrientation() -- rotation for new module 
		local omx,omy,omz 			= ScaleToMirror(oldmodule.gfx:GetScale()) -- mirror of old module
		local nx2,ny2,nz2			= Quaternion.ApplyToVector(nx,ny,nz,Quaternion.inverse(qw,qx,qy,qz)) 
		local mx2,my2,mz2 			= AxisAlignedNormalToMirror(nx2,ny2,nz2)-- rot must be used to calc new mirror
		local nmx,nmy,nmz			= omx*mx2,omy*my2,omz*mz2 				-- mirror for new module
		local x3,y3,z3, x4,y4,z4	= CorrectGridBounds(0,0,0,ApplyMirRotCombo(tn.cx,tn.cy,tn.cz, qw,qx,qy,qz, nmx,nmy,nmz))
		
		--gEditorDebugMsg = arrdump({"target bbox",x1,y1,z1, x2,y2,z2})
		--gEditorDebugMsg = gEditorDebugMsg.." _ "..arrdump({"new-rel bbox",x3,y3,z3, x4,y4,z4})
		return x1-x3,y1-y3,z1-z3, qw,qx,qy,qz, nmx,nmy,nmz
	end
	
	--if (true) then return end
	--print("GuessNewModulePosMirRot : boom",tn.gfx_geom,to.gfx_geom,tn.cx,to.cx,tn.cy,to.cy,tn.cz,to.cz)
	
	-- setup vars
	local time_guess_start = Client_GetTicks()
	local o = kShipEditorHitFaceOffset -- offset to get outside
	local hx,hy,hz = x+o*nx,y+o*ny,z+o*nz   -- h for hint 
	local ix,iy,iz = math.floor(hx),math.floor(hy),math.floor(hz) -- must be "inside"
	local cmax = math.max(tn.cx,tn.cy,tn.cz) -- maximal side length of new module
	
	-- list all combos
	local foundscore,foundres
	local foundcount = 0
	
	local nearbymodules = FindModulesIntersectingBBox(	ix-cmax-1,iy-cmax-1,iz-cmax-1, 
														ix+cmax+1,iy+cmax+1,iz+cmax+1)
	
	for myMirRotComboName,myMirRotCombo in pairs(gAllMirRotCombos) do
		local qw,qx,qy,qz, mx,my,mz = unpack(myMirRotCombo)
		
		-- calc relative bounds for this combo
		local x1,y1,z1, x2,y2,z2 = CorrectGridBounds(0,0,0,ApplyMirRotCombo(tn.cx,tn.cy,tn.cz, qw,qx,qy,qz, mx,my,mz))
		
		-- list all interesting positions
		for px=ix-cmax,ix+cmax do
		for py=iy-cmax,iy+cmax do
		for pz=iz-cmax,iz+cmax do
			--if (foundres) then break end -- TODO : remove me, just for debug
			
			-- calc absolute bounds for this combo
			local x3,y3,z3, x4,y4,z4 = px+x1,py+y1,pz+z1, px+x2,py+y2,pz+z2
			
			-- check if hint-point is inside bbox
			if (x3 <= hx and hx <= x4 and
				y3 <= hy and hy <= y4 and
				z3 <= hz and hz <= z4 and isempty(FindModulesIntersectingBBox(x3,y3,z3, x4,y4,z4,nearbymodules))) then
				-- todo : discard those having collisions with other modules in gShipEditModules
				-- todo : method to get absolute coords of side of module  (RelToAbs(x,y,z))
				
				local score = 0
				
				-- score for side overlaps
				local absgeom = GeomApplyScaleMirRotPosCombo(tn.gfx_geom, tn.cx,tn.cy,tn.cz, px,py,pz, qw,qx,qy,qz, mx,my,mz)
				score = score + ShipEditGetScoreForOverlappingGeom(absgeom,nearbymodules)
				--if (score > 0) then print("score ",score) end
				
				-- choose the best according to score based on 
				if ((not foundscore) or foundscore < score) then
					--print("found score,p,rotmir",score, px,py,pz, qw,qx,qy,qz, mx,my,mz)
					foundscore = score
					foundres = {px,py,pz, qw,qx,qy,qz, mx,my,mz}
					break
				end
				foundcount = foundcount + 1
			end
		end
		end
		end
	end
	print(arrdump({"guess took",Client_GetTicks()-time_guess_start,"ticks"}))
	
	gEditorDebugMsg = arrdump({"foundcount",foundcount})
	if (foundres) then return unpack(foundres) end
end

-- rounds to grid coordinates
-- returns an array of sides, each sides is an array with 9 or 12 floats, for the coordinates
-- e.g. return { {0,0,0, 1,0,0, 0,1,0,}, {0,0,0, 0,1,0, 0,0,1,}, ... }
function GeomApplyScaleMirRotPosCombo (geom, sx,sy,sz, px,py,pz, qw,qx,qy,qz, mx,my,mz)
	local res = {}
	local bInvertCulling = mx*my*mz < 0 -- *sx*sy*sz    invert culling if mirrored an odd amount of times (sign=-1)
	for k,geomside in pairs(geom) do
		local x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4 = unpack(geomside)
		if (x1) then x1,y1,z1 = ApplyMirRotCombo(sx*x1,sy*y1,sz*z1, qw,qx,qy,qz, mx,my,mz) x1,y1,z1 = round(px+x1),round(py+y1),round(pz+z1) end
		if (x2) then x2,y2,z2 = ApplyMirRotCombo(sx*x2,sy*y2,sz*z2, qw,qx,qy,qz, mx,my,mz) x2,y2,z2 = round(px+x2),round(py+y2),round(pz+z2) end
		if (x3) then x3,y3,z3 = ApplyMirRotCombo(sx*x3,sy*y3,sz*z3, qw,qx,qy,qz, mx,my,mz) x3,y3,z3 = round(px+x3),round(py+y3),round(pz+z3) end
		if (x4) then x4,y4,z4 = ApplyMirRotCombo(sx*x4,sy*y4,sz*z4, qw,qx,qy,qz, mx,my,mz) x4,y4,z4 = round(px+x4),round(py+y4),round(pz+z4) end
		if (bInvertCulling) then
			res[k] = {x1,y1,z1, x3,y3,z3, x2,y2,z2, x4,y4,z4}
		else	
			res[k] = {x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4}
		end
	end
	return res
end

function FindModulesIntersectingBBox(x3,y3,z3, x4,y4,z4, modulelist)
	local res = {}
	for k,module in pairs(modulelist or gShipEditModules) do
		local minx,miny,minz, maxx,maxy,maxz = module:GetCachedGridBounds()
		local bIsIn = false
		for x=x3+0.5,x4 do
			for y=y3+0.5,y4 do
				for z=z3+0.5,z4 do
					if (BBoxIntersectPoint(x,y,z, minx,miny,minz, maxx,maxy,maxz)) then bIsIn = true break end
				end
				if (bIsIn) then break end
			end
			if (bIsIn) then break end
		end
		if (bIsIn) then table.insert(res,module) end
	end
	return res
end

function FindModuleIntersectingPoint(x,y,z, modulelist,skipmodule)
	for k,module in pairs(modulelist or gShipEditModules) do
		if (module ~= skipmodule) then
			local minx,miny,minz, maxx,maxy,maxz = module:GetCachedGridBounds()
			if (BBoxIntersectPoint(x,y,z, minx,miny,minz, maxx,maxy,maxz)) then 
--~ 				print(arrdump({"FindModuleIntersectingPoint",x,y,z," ",minx,miny,minz," ",maxx,maxy,maxz}))
				return module 
			end
		end
	end
--~ 	print(arrdump({"FindModuleIntersectingPoint",x,y,z," found nothing"}))
end

function ShipEditGetScoreForOverlappingGeom(absgeom1,nearbymodules)
	local res = 0
	local absgeom1 = GeomGetAxisAlignedSides(absgeom1) -- only axis aligned sides are interesting here
	local minx,miny,minz,maxx,maxy,maxz
	for k1,geomside in pairs(absgeom1) do
		local x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4 = roundmultiple(unpack(geomside))
		minx,maxx = math.min(minx or x1,x1,x2,x3,x4 or x1),math.max(maxx or x1,x1,x2,x3,x4 or x1)
		miny,maxy = math.min(miny or y1,y1,y2,y3,y4 or y1),math.max(maxy or y1,y1,y2,y3,y4 or y1)
		minz,maxz = math.min(minz or z1,z1,z2,z3,z4 or z1),math.max(maxz or z1,z1,z2,z3,z4 or z1)
	end
	local modulelist = FindModulesIntersectingBBox(minx-1,miny-1,minz-1,maxx+1,maxy+1,maxz+1,nearbymodules)
	
	--print("FindModulesIntersectingBBox candidates:",table.getn(modulelist),"/",table.getn(nearbymodules))
	
	--print("ShipEditGetScoreForOverlappingGeom",table.getn(nearbymodules))
	for k1,module in pairs(modulelist) do
		local absgeom2 = module.cache_absgeom_nodiag
		for k2,absgeomside1 in pairs(absgeom1) do 
		for k3,absgeomside2 in pairs(absgeom2) do 
			res = res + ShipEditGetScoreForOverlappingGeomSides(absgeomside1,absgeomside2) 
		end
		end
	end
	return res
end

function ShipEditGetScoreForOverlappingGeomSides (absgeomside1,absgeomside2)
	local ncache1 = gShipEditGuessNormalCache[absgeomside1]
	local ncache2 = gShipEditGuessNormalCache[absgeomside2]
	local nx1,ny1,nz1
	local nx2,ny2,nz2
	if (ncache1) then 
		nx1,ny1,nz1 = unpack(ncache1)
	else
		nx1,ny1,nz1 = roundmultiple(GeomSideGetNormal(absgeomside1))
		gShipEditGuessNormalCache[absgeomside1] = {nx1,ny1,nz1}
	end
	if (ncache2) then 
		nx2,ny2,nz2 = unpack(ncache2)
	else
		nx2,ny2,nz2 = roundmultiple(GeomSideGetNormal(absgeomside2))
		gShipEditGuessNormalCache[absgeomside2] = {nx2,ny2,nz2}
	end
	
	-- check if sides are parallel
	if ((math.abs(nx1) ~= math.abs(nx2)) or
		(math.abs(ny1) ~= math.abs(ny2)) or
		(math.abs(nz1) ~= math.abs(nz2))) then return 0 end
	
	local x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4 = roundmultiple(unpack(absgeomside1))
	local x5,y5,z5, x6,y6,z6, x7,y7,z7, x8,y8,z8 = roundmultiple(unpack(absgeomside2))
	
	-- get the 2d bounding boxes of the sides (projected onto the best axis aligned plane)
	local mina1,minb1,maxa1,maxb1,c1 -- c is the vector orthogonal to the plane they lie in, a,b are planecoordinates
	local mina2,minb2,maxa2,maxb2,c2
	
	if (math.abs(nx1) > 0.5) then
		-- both faces lie in the yz plane
		c1,c2 = x1,x5
	elseif (math.abs(ny1) > 0.5) then                                         
		-- both faces lie in the xz plane                                     
		c1,c2 = y1,y5                                          
	else                                                                      
		-- both faces lie in the xy plane                                     
		c1,c2 = z1,z5                                           
	end
	
	-- check distance along normal, only zero is good for comparison
	if (c1 ~= c2) then return 0 end
	--print(" o c1,c2",c1,c2)
	
	
	if (math.abs(nx1) > 0.5) then
		-- both faces lie in the yz plane
		mina1,maxa1 = math.min(y1,y2,y3,y4 or y1),math.max(y1,y2,y3,y4 or y1)
		minb1,maxb1 = math.min(z1,z2,z3,z4 or z1),math.max(z1,z2,z3,z4 or z1)
		mina2,maxa2 = math.min(y5,y6,y7,y8 or y5),math.max(y5,y6,y7,y8 or y5)
		minb2,maxb2 = math.min(z5,z6,z7,z8 or z5),math.max(z5,z6,z7,z8 or z5)
	elseif (math.abs(ny1) > 0.5) then                                         
		-- both faces lie in the xz plane                               
		mina1,maxa1 = math.min(x1,x2,x3,x4 or x1),math.max(x1,x2,x3,x4 or x1)
		minb1,maxb1 = math.min(z1,z2,z3,z4 or z1),math.max(z1,z2,z3,z4 or z1)
		mina2,maxa2 = math.min(x5,x6,x7,x8 or x5),math.max(x5,x6,x7,x8 or x5)
		minb2,maxb2 = math.min(z5,z6,z7,z8 or z5),math.max(z5,z6,z7,z8 or z5)
	else                                                                      
		-- both faces lie in the xy plane                         
		mina1,maxa1 = math.min(y1,y2,y3,y4 or y1),math.max(y1,y2,y3,y4 or y1)
		minb1,maxb1 = math.min(x1,x2,x3,x4 or x1),math.max(x1,x2,x3,x4 or x1)
		mina2,maxa2 = math.min(y5,y6,y7,y8 or y5),math.max(y5,y6,y7,y8 or y5)
		minb2,maxb2 = math.min(x5,x6,x7,x8 or x5),math.max(x5,x6,x7,x8 or x5)
	end
	
	
	
	--print(" o geo1",x1,y1,z1, x2,y2,z2, x3,y3,z3, x4,y4,z4)
	--print(" o geo2",x5,y5,z5, x6,y6,z6, x7,y7,z7, x8,y8,z8)
	--print(arrdump({"  face with same normal and pos",nx1,ny1,nz1," ",mina1,maxa1,minb1,maxb1," ",mina2,maxa2,minb2,maxb2}))
	--  face with same normal and pos,0,0,-1, ,0,1,0,1, ,0,1,-1,0
	
	-- 2d bbox must overlap for comparison
	if (maxa1 <= mina2 or 
		maxb1 <= minb2 or
		maxa2 <= mina1 or 
		maxb2 <= minb1) then return 0 end
	
	
	-- calc the bounding rect of the intersection
	local imina,imaxa = math.max(mina1,mina2), math.min(maxa1,maxa2)
	local iminb,imaxb = math.max(minb1,minb2), math.min(maxb1,maxb2)
	
	-- a big area for the bounding rect is good, e.g. two big faces overlapping are better than two small faces overlapping
	local intersectionarea = (imaxa-imina)*(imaxb-iminb)
	if (intersectionarea <= 0) then return 0 end
	
	-- but it is bad if the overlapping area is only part of the area of the individual faces
	local area1 = (mina1-maxa1)*(minb1-maxb1)
	local area2 = (mina2-maxa2)*(minb2-maxb2)
	
	local score = intersectionarea * (intersectionarea / (area1 or 1)) * (intersectionarea / (area2 or 1))
	
	-- give a little bonus for overlapping vertices
	local vertexbonus = 0.001
	if (Vector.compare(x1,y1,z1, x5,y5,z5)) then score = score + vertexbonus end
	if (Vector.compare(x1,y1,z1, x6,y6,z6)) then score = score + vertexbonus end
	if (Vector.compare(x1,y1,z1, x7,y7,z7)) then score = score + vertexbonus end
	if (Vector.compare(x1,y1,z1, x8,y8,z8)) then score = score + vertexbonus end
	
	if (Vector.compare(x2,y2,z2, x5,y5,z5)) then score = score + vertexbonus end
	if (Vector.compare(x2,y2,z2, x6,y6,z6)) then score = score + vertexbonus end
	if (Vector.compare(x2,y2,z2, x7,y7,z7)) then score = score + vertexbonus end
	if (Vector.compare(x2,y2,z2, x8,y8,z8)) then score = score + vertexbonus end
	
	if (Vector.compare(x3,y3,z3, x5,y5,z5)) then score = score + vertexbonus end
	if (Vector.compare(x3,y3,z3, x6,y6,z6)) then score = score + vertexbonus end
	if (Vector.compare(x3,y3,z3, x7,y7,z7)) then score = score + vertexbonus end
	if (Vector.compare(x3,y3,z3, x8,y8,z8)) then score = score + vertexbonus end
	
	if (Vector.compare(x4,y4,z4, x5,y5,z5)) then score = score + vertexbonus end
	if (Vector.compare(x4,y4,z4, x6,y6,z6)) then score = score + vertexbonus end
	if (Vector.compare(x4,y4,z4, x7,y7,z7)) then score = score + vertexbonus end
	if (Vector.compare(x4,y4,z4, x8,y8,z8)) then score = score + vertexbonus end
	
	return score
	-- todo : check exact overlap using a few ray hit tests	? probably not necessary
end


