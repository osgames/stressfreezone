-- objects can have properties with float/int and string values, 
-- number properties can be dynamic, this means they are moving/changing, like shields regenerating or energy charging
-- string : name
-- float : static : hull
-- float : dynamic : shield,energy
-- int(internally as float) : faction_id (e.g. groups like pirates, traders, police..) , state,flags... (crippled..)
-- PropTypes could be used to store allowed values, hud infos, debug names...
-- delta is for visual effects like numbers popping out for damage done

gPropTypes = CreateTypeList()

function RegisterPropType 	(...) return gPropTypes:RegisterType(unpack(arg)) end

function GetPropType 		(proptype_or_name_or_id) return gPropTypes:Get(proptype_or_name_or_id) end

-- ##### ##### ##### ##### ##### common

function gObjectPrototype:SetName (sName) self:SetProp("proptype/name",sName) end
function gObjectPrototype:GetName (sName) return self:GetProp("proptype/name") end

function gObjectPrototype:GetTypeAndNameText ()
	local typename = self.objtype.name
	local name = self:GetName()
	return (typename and name and (typename.." : "..name)) or typename or name or ""
end

-- ##### ##### ##### ##### ##### misc

function gObjectPrototype:InitProp () if (not self.prop) then self.prop = {} end end

-- transmit all current properties to a newly joined player
function gObjectPrototype:ServerSendProps (con)
	if (self.prop) then
		for t,value in pairs(self.prop) do
			if (type(value) == "table") then 
				local o = value
				SendNetMessage(con,kNetMessage_SetProp_Dynamic_Number,self,t.type_id,self:GetProp(t),0,o.fMin,o.fMax,o.fRate)
			elseif (type(value) == "string") then
				SendNetMessage(con,kNetMessage_SetProp_Static_String,self,t.type_id,value)
			else
				SendNetMessage(con,kNetMessage_SetProp_Static_Number,self,t.type_id,value,0)
			end
		end
	end
end

function gObjectPrototype:GetProp	(proptype_or_name_or_id)
	if (not self.prop) then return end
	local t = GetPropType(proptype_or_name_or_id)
	local o = self.prop[t]
	if (type(o) == "table") then 
		return math.max(o.fMin,math.min(o.fMax,o.start_value + o.fRate * 0.001 * (gMyTicks - o.start_timestamp))) 
	end 
	return o
end

-- overloaded function supporting different parameter sets for convenience
function gObjectPrototype:SetProp	(proptype_or_name_or_id,value,delta,fMin,fMax,fRate)
	if (type(value) == "string") then 
		self:SetProp_Static_String(proptype_or_name_or_id,value) 
	elseif (fMin) then 
		self:SetProp_Dynamic_Number(proptype_or_name_or_id,value,delta,fMin,fMax,fRate) 
	else
		self:SetProp_Static_Number(proptype_or_name_or_id,value,delta) 
	end
end

-- ##### ##### ##### ##### ##### SetProp_Dynamic_Number

function gObjectPrototype:SetProp_Dynamic_Number (proptype_or_name_or_id,value,delta,fMin,fMax,fRate) 
	local t = GetPropType(proptype_or_name_or_id)
	self:InitProp()
	self.prop[t] = {start_value=value,fMin=fMin,fMax=fMax,fRate=fRate,start_timestamp=gMyTicks}
	NotifyListener("Hook_Object_SetProp",self,t,value,delta,fMin,fMax,fRate)
	if (gbServerRunning) then ServerBroadcastMessage(kNetMessage_SetProp_Dynamic_Number,self,t.type_id,value,delta,fMin,fMax,fRate) end
end

gMessageTypeHandler_Client_Remote.kNetMessage_SetProp_Dynamic_Number = function (obj,proptypeid,value,delta,fMin,fMax,fRate)
	if (obj) then obj:SetProp_Dynamic_Number(proptypeid,value,delta,fMin,fMax,fRate) end
end

-- ##### ##### ##### ##### ##### SetProp_Static_Number

function gObjectPrototype:SetProp_Static_Number (proptype_or_name_or_id,value,delta) 
	local t = GetPropType(proptype_or_name_or_id)
	self:InitProp()
	self.prop[t] = value
	NotifyListener("Hook_Object_SetProp",self,t,value,delta)
	if (gbServerRunning) then ServerBroadcastMessage(kNetMessage_SetProp_Static_Number,self,t.type_id,value,delta or 0) end
end

gMessageTypeHandler_Client_Remote.kNetMessage_SetProp_Static_Number = function (obj,proptypeid,value)
	if (obj) then obj:SetProp_Static_Number(proptypeid,value) end
end

-- ##### ##### ##### ##### ##### SetProp_Static_String

function gObjectPrototype:SetProp_Static_String (proptype_or_name_or_id,value) 
	local t = GetPropType(proptype_or_name_or_id)
	self:InitProp()
	self.prop[t] = value
	NotifyListener("Hook_Object_SetProp",self,t,value)
	if (gbServerRunning) then ServerBroadcastMessage(kNetMessage_SetProp_Static_String,self,t.type_id,value) end
end

gMessageTypeHandler_Client_Remote.kNetMessage_SetProp_Static_String = function (obj,proptypeid,value)
	if (obj) then obj:SetProp_Static_String(proptypeid,value) end
end

