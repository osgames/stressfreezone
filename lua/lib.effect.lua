-- effects such as beams, sparks, maybe even sounds
-- see also common.effecttypes.lua

gEffectTypes = CreateTypeList()

function RegisterEffectType 	(...) return gEffectTypes:RegisterType(unpack(arg)) end

gEffectNextID = 1
gEffectsByID = {}

function GetEffect			(effect_or_id)
	if (type(effect_or_id) == "number") then return gEffectsByID[effect_or_id] end
	return effect_or_id
end

function SpawnEffect (effecttype_or_name_or_id,parentobj_or_id,vPos,qRot,id,...)
	local effect = CopyArray(gEffectPrototype)
	
	-- id
	if (gbServerRunning) then assert(id == nil,"vararg should start after id=nil") id = gEffectNextID gEffectNextID = gEffectNextID + 1 else id = id or 0 end
	effect.id = id
	gEffectsByID[id] = effect
	
	-- determine type
	effect.effecttype = gEffectTypes:Get(effecttype_or_name_or_id)
	if (not effect.effecttype) then print("warning, effecttype not found:",effecttype_or_name_or_id) end
	
	printdebug("spawn","spawn "..effect.effecttype.type_name)
	
	-- initial pos
	effect.obj = GetObject(parentobj_or_id)
	if (effect.obj) then effect.obj:AddToDestroyList(effect) end
	
	-- gfx
	effect.pos = vPos
	effect.rot = qRot
	--print("SpawnEffect",effecttype_or_name_or_id,effect.effecttype and effect.effecttype.type_name)
	if (effect.effecttype and effect.effecttype.constructor) then
		local parentgfx = effect.obj and effect.obj.gfx
		effect.gfx = effect.effecttype:constructor(parentgfx,effect,unpack(arg))
		if (effect.gfx) then 
			effect.gfx:SetPosition(unpack(vPos)) 
			effect.gfx:SetOrientation(unpack(qRot)) 
		end
	end
	
	-- sound
	if effect.effecttype and effect.effecttype.sound then
		local x,y,z = unpack(effect.pos)
		-- adjust position due to parent present (
		if effect.obj then 
			x,y,z = Quaternion.ApplyToVector(x,y,z,unpack(effect.obj.mqRot)) 
			x,y,z = Vector.add(x,y,z,unpack(effect.obj.mvPos)) 
		end
		SoundPlayEffect(x,y,z,effect.effecttype.sound)
	end
	
	-- send create message to network
	if (gbServerRunning) then for id,player in pairs(gServerPlayers) do effect:ServerSendSpawn(player.con,unpack(arg)) end end
	
	NotifyListener("Hook_Effect_Spawn",effect)
	return effect
end

gEffectPrototype = {}

function gEffectPrototype:Destroy ()
	NotifyListener("Hook_Effect_Destroy",self)
	gEffectsByID[self.id] = nil
	if (self.obj) then self.obj:RemoveFromDestroyList(self) end
	if (self.gfx) then self.gfx:Destroy() self.gfx = nil end
end

--- sends spawn message
function gEffectPrototype:ServerSendSpawn (con,...) 
	local x,y,z = unpack(self.pos)
	local qw,qx,qy,qz = unpack(self.rot)
	SendNetMessage(con,kNetMessage_SpawnEffect,self.effecttype.type_id,self.obj,x,y,z,qw,qx,qy,qz,self.id,unpack(arg))			
end

gMessageTypeHandler_Client_Remote.kNetMessage_SpawnEffect = function (type_id,parentobj,x,y,z,qw,qx,qy,qz,new_id,...)
	SpawnEffect(type_id,parentobj,{x,y,z},{qw,qx,qy,qz},new_id,unpack(arg))
end

