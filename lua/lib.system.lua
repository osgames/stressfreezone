-- loads the systems in kSystemDir/*.lua
-- universe dir with multiple subdirs for different campaigns/mods ? maybe each can have a plugins dir ?

gSystems = {}
gSystemPrototype = {}

function RegisterSystem (name) 
	local s = {}
	ArrayOverwrite(s,gSystemPrototype)
	s.name = name
	s.filepath = gCurSystemFilePath
	s.connections = {}
	s.suns = {}
	s.planets = {}
	gSystems[name] = s
	return s
end


function gSystemPrototype:SpawnLocation	() 
	local loc = SpawnLocation("locationtype/default")
	
	local au = 4000
	
	if (false) then
	
	-- spawn suns
	if (not gCommandLineSwitches["-lofttest"]) then 
		--TODO : for k,o in pairs(self.suns)	do o:Spawn(loc) end
		for k,o in pairs(self.suns) do
			local obj = SpawnObject(o.objtype or "sun",loc,{0,0,0},{Quaternion.random()})
			obj.mqTurn = {Quaternion.random( (2*math.random()-1) * (30.0*gfDeg2Rad) ) }
		end
	end

	-- spawn planets
	--TODO : for k,o in pairs(self.planets)	do o:Spawn(loc) end
	for k,o in pairs(self.planets) do
		
		local orad = o.orad * au
		local planettype = o.planettype
		
		local a = math.random()
		
		local obj = SpawnObject(o.objtype or "planet",loc,{orad * math.cos(2*math.pi*a),0,orad * math.sin(2*math.pi*a)},{Quaternion.random()})
		obj.mqTurn = {Quaternion.random( (2*math.random()-1) * (10.0*gfDeg2Rad) ) }
		obj:SetName(o.name)
		
		-- place a astroid circle in the planet orbit
		for i = 0,50 do
			local d = 10
			
			a = math.random()
			
			local dx = math.random() * 2
			local dy = math.random() * 2
			local dz = math.random() * 2

			local t = "asteroid/asteroid" .. math.random(1,5)
			
			local obj = SpawnObject(t,loc,
				{	dx + orad * math.cos(2*math.pi*a),
					dy,
					dz + orad * math.sin(2*math.pi*a)
				},{Quaternion.random()})
			obj.mqTurn = {Quaternion.random( (2*math.random()-1) * (90.0*gfDeg2Rad) ) }
		end
	end
	end
		
	--TODO : stations ? asteroid belts ? maybe later ships(pirates,traders,police,bounty-hunters..) ?
	
	return loc
end

function gSystemPrototype:AddConnection		(other_system_name) self.connections[other_system_name] = true end

function gSystemPrototype:AddConnections	(...) 
	for k,v in ipairs(arg) do self:AddConnection(v) end
end

function gSystemPrototype:AddSun			(objtype,name,stellarclass)  
	local sun = {name=name,stellarclass=stellarclass,objtype=objtype}
	table.insert(self.suns,sun)
end

--[[
	name
	planettype,
	diameter : Equatorial diameter[a]	
	mass : Mass[a]	
	orad : Orbital radius (AU)	
	operiod : Orbital period (years)	
	inclin : Inclination to Sun's equator (�)	
	eccen : Orbital eccentricity	
	rotperiod : Rotation period (days)
	mooncount,	
	atmosphere
]]--
function gSystemPrototype:AddPlanetEx		(objtype,name,planettype,diameter,mass,orad,operiod,inclin,eccen,rotperiod,mooncount,atmosphere)  
	local planet = {name=name,planettype=planettype,diameter=diameter,mass=mass,orad=orad,operiod=operiod,
		inclin=inclin,eccen=eccen,rotperiod=rotperiod,mooncount=mooncount,atmosphere=atmosphere,objtype=objtype}
	table.insert(self.planets,planet)
end

-- lists all lua files in kSystemDir and executes them
function LoadSystems ()
	local arr_files = dirlist(kSystemDir,false,true)
	local sortedfilenames = {}
	for k,filename in pairs(arr_files) do table.insert(sortedfilenames,filename) end
	table.sort(sortedfilenames)
	
	for k,filename in pairs(sortedfilenames) do if (filename ~= ".." and filename ~= "." and filename ~= ".svn") then
		local path = kSystemDir..filename
		gCurSystemFilePath = path
		print("loading system ",path)
		dofile(path)
	end end
	
	CreateSystemConnections()
	NotifyListener("Hook_SystemsLoaded")
end

function GetSystem (systemname) return gSystems[systemname] end

--- makes all connections bidirectional
function CreateSystemConnections ()
	for k,s in pairs(gSystems) do	
		for other_system_name,ign in pairs(s.connections) do 
			local other_system = GetSystem(other_system_name)
			assert(other_system,"broken system link "..s.name.."("..s.filepath..") to "..other_system_name)
			other_system:AddConnection(s.name)
		end
	end
end
