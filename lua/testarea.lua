gbLabRunnnig = false


giSeed = 0
gPlantGfx = nil


function StartLaboratory ()
	print("StartLaboratory")
		
	gbLabRunnnig = true
	
	gLabLocation = SpawnLocation("locationtype/default")
	
	if (gStarField) then gStarField:Destroy() end
	gStarField = CreateRootGfx3D()
	gStarField:SetStarfield(gNumberOfStars,gStarsDist,gStarColorFactor,"starbase")

	local cam = GetMainCam()
	cam:SetNearClipDistance(gDefaultNearClip)
	cam:SetFarClipDistance(gDefaultFarClip) -- ogre defaul : 100000

	local amb = 0.2

	Client_ClearLights()
	Client_SetAmbientLight(amb,amb,amb,1)
	Client_AddDirectionalLight(-0.3,-0.5,-0.1)
	
	gCamThirdPersonDist = 100
	gCamThirdPersonX,gCamThirdPersonY,gCamThirdPersonZ = 0,0,0
	gLaborTableCamSpeedFactor = 0.5 * gfDeg2Rad
		
	local myKeyDown = function (key,char)
		if (key == GetNamedKey("c")) then
		end
	end

	RegisterListener("keydown",myKeyDown)
	
	RegisterListener("mouse_left_click",function () LabSingleClick() end)
	RegisterListener("mouse_right_down",function () LabRightClick() end)
	
	NotifyListener("Hook_Lab_Start")
	
	-- local p = GeneratePath({0,0,0},{0.5,0,2},{0.5,1.0,0.1},5)
	
	if gPlantGfx then
		gPlantGfx:Destroy()
	end
	gPlantGfx = CreateRootGfx3D()
	NewPlant(giSeed,gPlantGfx)

	for i = 1,25 do
		local s = RndNameGenerate()
		print("name",s)
	end
	
	if (CreateOdeWorld) then
		gOdeWorld = CreateOdeWorld(0.01)
		gOdeWorld:SetGravity(0,0,0)
	
		o1 = gOdeWorld:CreateObject(5,5,0)
		-- o1:SetShapeSphere(1,1)
		o1:SetShapeBox(1,1,1,1)
		o2 = gOdeWorld:CreateObject(-5,-5,0)
		-- o2:SetShapeSphere(1,1)
		o2:SetShapeBox(1,1,1,1)

		local s = 1

		o1.gfx = CreateRootGfx3D()
		o1.gfx:SetMesh("fridge.mesh")
		o1.gfx:SetScale(s,s,s)
		o1.gfx:SetPosition(0,0,0)

		o2.gfx = CreateRootGfx3D()
		o2.gfx:SetMesh("fridge.mesh")
		o2.gfx:SetScale(s,s,s)
		o2.gfx:SetPosition(0,0,0)

		o1:AddForce(0,0,100)
		o2:AddForce(100,100,0)
	end

	--[[
	irc = CreateIRCConnection("zwischenwelt.org",7777,"lala123")
	irc:JoinChannel("#zocken")
	irc:SendMessage("#zocken","hy guys!")
	irc.OnMessage = function (self,sender,channel,message)
		AddFadeLines("[" .. channel .. "] " .. sender .. ": " .. message)
	end
	]]--
end

function ForceToPoint(o,x,y,z)
	ox,oy,oz = o:GetPosition()
	local dx = x - ox
	local dy = y - oy
	local dz = z - oz
	local l = math.sqrt(dx*dx + dy*dy + dz*dz)
	local f = 1 -- * l
	o:AddForce(dx*f,dy*f,dz*f)
end

function StepLaboratory ()
	if not gbLabRunnnig then return end
	
	if (gLabLocation) then gLabLocation:StepAllObjects(gMyTicks,gSecondsSinceLastFrame) end
	StepTableCam(GetMainCam(),gKeyPressed[key_mouse1] and (not gui.bMouseBlocked),gLaborTableCamSpeedFactor)
	StepThirdPersonCam(GetMainCam(),gCamThirdPersonDist,gCamThirdPersonX,gCamThirdPersonY,gCamThirdPersonZ)
	
	SetBottomLine("")
	
	if (gOdeWorld) then gOdeWorld:Step() end
	
	local zx,zy,zz = 0,0,0
	
	local x1,y1,z1,w1
	local x2,y2,z2,w2
	
	x1,y1,z1 = o1:GetPosition()
	x2,y2,z2 = o2:GetPosition()

	--ForceToPoint(o1,x2,y2,z2)
	--ForceToPoint(o2,x1,y1,z1)
	
	ForceToPoint(o1,zx,zy,zz)
	ForceToPoint(o2,zx,zy,zz)

	o1.gfx:SetPosition(x1,y1,z1)
	o2.gfx:SetPosition(x2,y2,z2)
	
	x1,y1,z1,w1 = o1:GetRotation()
	o1.gfx:SetOrientation(w1,x1,y1,z1)
	x2,y2,z2,w2 = o2:GetRotation()
	o2.gfx:SetOrientation(w2,x2,y2,z2)
	
	if irc then irc:Step() end
end

function LabSingleClick ()

end

function LabRightClick ()
	giSeed = giSeed + 1

	if gPlantGfx then
		gPlantGfx:Destroy()
	end
	gPlantGfx = CreateRootGfx3D()
	NewPlant(giSeed,gPlantGfx)

end

